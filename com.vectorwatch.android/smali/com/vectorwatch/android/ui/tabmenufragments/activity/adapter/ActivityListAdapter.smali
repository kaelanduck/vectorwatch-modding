.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;
.super Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
.source "ActivityListAdapter.java"

# interfaces
.implements Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;,
        Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;
    }
.end annotation


# static fields
.field private static final ADJUSTED_CALORIES:I = 0x1

.field private static final ADJUSTED_DISTANCE:I = 0x2

.field private static final ADJUSTED_STEPS:I = 0x0

.field private static final AVERAGE_STEP_RATE:F = 2.25f

.field private static final CURR_SLEEP_TIME:I = 0x2

.field private static final DEFAULT_CALORIES:I = 0x9c4

.field private static final DEFAULT_DISTANCE:I = 0x5

.field private static final DEFAULT_SLEEP_TIME:I = 0x8

.field private static final DEFAULT_STEPS:I = 0x2710

.field private static final EXPORT_DATA_URL:Ljava/lang/String; = "https://developer.vectorwatch.com/platform/export/activity"

.field private static final MAX_CALORIES_ROTATION:I = 0x1f4

.field private static final MAX_DISTANCE_ROTATION:I = 0xa

.field private static final MAX_SLEEP_TIME:I = 0xc

.field private static final MAX_SLEEP_TIME_ROTATION:I = 0xc

.field private static final MAX_STEPS_ROTATION:I = 0x2710

.field private static final METERS_TO_KM_COEFFICIENT:I = 0x3e8

.field private static final METERS_TO_MILES_COEFFICIENT:I = 0x649

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

.field private distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

.field private mActionMode:Landroid/support/v7/view/ActionMode;

.field private mActivity:Landroid/app/Activity;

.field private mActivityAlertsButton:Landroid/widget/Button;

.field private mActivityContainerView:Landroid/view/View;

.field private mActivityLayout:Landroid/widget/RelativeLayout;

.field private mArrowDownImageView:Landroid/widget/ImageView;

.field private mBadgeView:Landroid/widget/TextView;

.field mCaloriesBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

.field mCaloriesCenterTV:Landroid/widget/TextView;

.field mCaloriesLL:Landroid/widget/LinearLayout;

.field mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

.field mCaloriesNeedle:Landroid/widget/ImageView;

.field mCaloriesTV:Landroid/widget/TextView;

.field mCaloriesTVLabel:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

.field private mCurrentCaloriesGoal:F

.field private mCurrentDistanceGoal:F

.field private mCurrentEditMode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field private mCurrentSleepGoal:F

.field private mCurrentStepsGoal:F

.field mDistanceBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

.field mDistanceCenterTV:Landroid/widget/TextView;

.field mDistanceLL:Landroid/widget/LinearLayout;

.field mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

.field mDistanceNeedle:Landroid/widget/ImageView;

.field mDistanceTV:Landroid/widget/TextView;

.field mDistanceTVLabel:Landroid/widget/TextView;

.field private mExportDataLink:Landroid/widget/TextView;

.field private mGraphFAB:Lcom/software/shell/fab/ActionButton;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLinkStateImage:Landroid/widget/TextView;

.field private mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

.field private mProgressCaloriesGoal:I

.field private mProgressDistanceGoal:F

.field private mProgressSleepGoal:F

.field private mProgressStepsGoal:I

.field private mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

.field private mScrollDummyView:Landroid/view/View;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mShareButton:Landroid/widget/Button;

.field private mShareLayout:Landroid/widget/RelativeLayout;

.field mSleepBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

.field mSleepCenterTV:Landroid/widget/TextView;

.field mSleepLL:Landroid/widget/LinearLayout;

.field mSleepModel:Lcom/vectorwatch/android/models/QuadrantModel;

.field mSleepNeedle:Landroid/widget/ImageView;

.field private mSleepQuality:I

.field mSleepTV:Landroid/widget/TextView;

.field mSleepTVLabel:Landroid/widget/TextView;

.field private mSomethingToSave:Z

.field mStepsBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

.field mStepsCenterTV:Landroid/widget/TextView;

.field mStepsLL:Landroid/widget/LinearLayout;

.field mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

.field mStepsNeedle:Landroid/widget/ImageView;

.field mStepsTV:Landroid/widget/TextView;

.field mStepsTVLabel:Landroid/widget/TextView;

.field mValueCalories:Landroid/widget/TextView;

.field mValueCaloriesMax:Landroid/widget/TextView;

.field mValueDistance:Landroid/widget/TextView;

.field mValueDistanceMax:Landroid/widget/TextView;

.field mValueSleep:Landroid/widget/TextView;

.field mValueSleepMax:Landroid/widget/TextView;

.field mValueSteps:Landroid/widget/TextView;

.field mValueStepsMax:Landroid/widget/TextView;

.field private mView:Landroid/view/View;

.field public final rowOne:I
    .annotation build Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        initMethod = true
        layout = 0x7f030024
        views = {
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f1000cb
                name = "title"
                type = Landroid/widget/TextView;
            .end subannotation
        }
    .end annotation
.end field

.field private sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

.field private stepsQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/support/v7/view/ActionMode;Landroid/support/v7/view/ActionMode$Callback;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "mActionMode"    # Landroid/support/v7/view/ActionMode;
    .param p3, "mContextModeCallback"    # Landroid/support/v7/view/ActionMode$Callback;

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    const/4 v4, 0x0

    .line 208
    invoke-direct {p0, p1}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;-><init>(Landroid/content/Context;)V

    .line 124
    const/high16 v1, 0x41000000    # 8.0f

    iput v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    .line 175
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentEditMode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    .line 186
    iput-boolean v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    .line 189
    const/4 v1, -0x1

    iput v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepQuality:I

    .line 194
    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->rowOne:I

    .line 209
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    .line 210
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivity:Landroid/app/Activity;

    .line 211
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 212
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 213
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    .line 215
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 216
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    .line 217
    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-nez v1, :cond_2

    .line 218
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    .line 219
    .local v0, "steps":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/vectorwatch/android/utils/Helpers;->getMaxValueForField(Landroid/content/Context;I)F

    move-result v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/vectorwatch/android/utils/Helpers;->getMaxValueForField(Landroid/content/Context;I)F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    sub-float/2addr v1, v2

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 223
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v1

    invoke-direct {p0, v4, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->adjustGoals(IF)V

    .line 226
    .end local v0    # "steps":F
    :cond_2
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    return v0
.end method

.method static synthetic access$1000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mShareButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1100()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/support/v7/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActionMode:Landroid/support/v7/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    return v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    return v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    return v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    return v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    return v0
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    return v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressSleepGoal:F

    return v0
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    return-object v0
.end method

.method private adjustGoals(IF)V
    .locals 26
    .param p1, "adjustedField"    # I
    .param p2, "adjustedValue"    # F

    .prologue
    .line 1274
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v10

    .line 1276
    .local v10, "height":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v4

    .line 1277
    .local v4, "bmr":D
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v15

    if-nez v15, :cond_0

    const-wide v8, 0x3fda8f5c28f5c28fL    # 0.415

    .line 1278
    .local v8, "distanceFactor":D
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v15

    int-to-double v0, v15

    move-wide/from16 v20, v0

    mul-double v16, v8, v20

    .line 1279
    .local v16, "stride":D
    const-wide/high16 v20, 0x4002000000000000L    # 2.25

    mul-double v20, v20, v16

    const-wide/high16 v22, 0x4059000000000000L    # 100.0

    div-double v20, v20, v22

    const-wide v22, 0x400ccccccccccccdL    # 3.6

    mul-double v20, v20, v22

    const-wide/high16 v22, 0x3fe4000000000000L    # 0.625

    mul-double v12, v20, v22

    .line 1280
    .local v12, "speed":D
    const-wide v20, 0x3f654c985f06f694L    # 0.0026

    const-wide/high16 v22, 0x4010000000000000L    # 4.0

    move-wide/from16 v0, v22

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v22

    mul-double v20, v20, v22

    const-wide v22, 0x3fb23a29c779a6b5L    # 0.0712

    const-wide/high16 v24, 0x4008000000000000L    # 3.0

    move-wide/from16 v0, v24

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    mul-double v22, v22, v24

    sub-double v20, v20, v22

    const-wide v22, 0x3fe3cfaacd9e83e4L    # 0.6191

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    move-wide/from16 v0, v24

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    mul-double v22, v22, v24

    add-double v20, v20, v22

    const-wide v22, 0x3fd56872b020c49cL    # 0.3345

    mul-double v22, v22, v12

    sub-double v20, v20, v22

    const-wide v22, 0x3ff0b780346dc5d6L    # 1.0448

    add-double v2, v20, v22

    .line 1283
    .local v2, "MET":D
    packed-switch p1, :pswitch_data_0

    .line 1313
    :goto_1
    return-void

    .line 1277
    .end local v2    # "MET":D
    .end local v8    # "distanceFactor":D
    .end local v12    # "speed":D
    .end local v16    # "stride":D
    :cond_0
    const-wide v8, 0x3fda6e978d4fdf3bL    # 0.413

    goto :goto_0

    .line 1285
    .restart local v2    # "MET":D
    .restart local v8    # "distanceFactor":D
    .restart local v12    # "speed":D
    .restart local v16    # "stride":D
    :pswitch_0
    move/from16 v0, p2

    float-to-double v0, v0

    move-wide/from16 v20, v0

    mul-double v20, v20, v8

    int-to-double v0, v10

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    const-wide v22, 0x40f86a0000000000L    # 100000.0

    div-double v20, v20, v22

    const-wide v22, 0x408f400000000000L    # 1000.0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v15, v0

    sget-object v20, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v15, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1287
    const/high16 v15, 0x40100000    # 2.25f

    div-float v15, p2, v15

    const/high16 v20, 0x42700000    # 60.0f

    div-float v15, v15, v20

    float-to-double v0, v15

    move-wide/from16 v18, v0

    .line 1289
    .local v18, "walkingMinutes":D
    const-wide/high16 v20, 0x4038000000000000L    # 24.0

    div-double v20, v4, v20

    mul-double v20, v20, v2

    mul-double v20, v20, v18

    const-wide/high16 v22, 0x404e000000000000L    # 60.0

    div-double v6, v20, v22

    .line 1290
    .local v6, "calsBurned":D
    const-wide/high16 v20, 0x4038000000000000L    # 24.0

    const-wide/high16 v22, 0x404e000000000000L    # 60.0

    div-double v22, v18, v22

    sub-double v20, v20, v22

    mul-double v20, v20, v4

    const-wide/high16 v22, 0x4038000000000000L    # 24.0

    div-double v20, v20, v22

    add-double v20, v20, v6

    move-wide/from16 v0, v20

    double-to-float v15, v0

    sget-object v20, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v15, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    goto :goto_1

    .line 1294
    .end local v6    # "calsBurned":D
    .end local v18    # "walkingMinutes":D
    :pswitch_1
    const-wide v20, 0x4096800000000000L    # 1440.0

    move/from16 v0, p2

    float-to-double v0, v0

    move-wide/from16 v22, v0

    sub-double v22, v22, v4

    mul-double v20, v20, v22

    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    sub-double v22, v2, v22

    mul-double v22, v22, v4

    div-double v18, v20, v22

    .line 1295
    .restart local v18    # "walkingMinutes":D
    const-wide/high16 v20, 0x4002000000000000L    # 2.25

    mul-double v20, v20, v18

    const-wide/high16 v22, 0x404e000000000000L    # 60.0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v11, v0

    .line 1297
    .local v11, "steps":F
    sget-object v15, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v11, v15, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1298
    float-to-double v0, v11

    move-wide/from16 v20, v0

    mul-double v20, v20, v8

    int-to-double v0, v10

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    const-wide v22, 0x40f86a0000000000L    # 100000.0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v15, v0

    const/high16 v20, 0x447a0000    # 1000.0f

    mul-float v15, v15, v20

    sget-object v20, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v15, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1302
    .end local v11    # "steps":F
    .end local v18    # "walkingMinutes":D
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 1303
    const v15, 0x44c92000    # 1609.0f

    mul-float v15, v15, p2

    const/high16 v20, 0x447a0000    # 1000.0f

    div-float p2, v15, v20

    .line 1305
    :cond_1
    const v15, 0x47c35000    # 100000.0f

    mul-float v15, v15, p2

    float-to-double v0, v15

    move-wide/from16 v20, v0

    int-to-double v0, v10

    move-wide/from16 v22, v0

    mul-double v22, v22, v8

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v14, v0

    .line 1306
    .local v14, "stepsDistance":F
    sget-object v15, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v14, v15, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1307
    const/high16 v15, 0x40100000    # 2.25f

    div-float v15, v14, v15

    const/high16 v20, 0x42700000    # 60.0f

    div-float v15, v15, v20

    float-to-double v0, v15

    move-wide/from16 v18, v0

    .line 1308
    .restart local v18    # "walkingMinutes":D
    const-wide/high16 v20, 0x4038000000000000L    # 24.0

    div-double v20, v4, v20

    mul-double v20, v20, v2

    mul-double v20, v20, v18

    const-wide/high16 v22, 0x404e000000000000L    # 60.0

    div-double v6, v20, v22

    .line 1309
    .restart local v6    # "calsBurned":D
    const-wide/high16 v20, 0x4038000000000000L    # 24.0

    const-wide/high16 v22, 0x404e000000000000L    # 60.0

    div-double v22, v18, v22

    sub-double v20, v20, v22

    mul-double v20, v20, v4

    const-wide/high16 v22, 0x4038000000000000L    # 24.0

    div-double v20, v20, v22

    add-double v20, v20, v6

    move-wide/from16 v0, v20

    double-to-float v15, v0

    sget-object v20, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v15, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private calculateDefaultGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;)I
    .locals 3
    .param p1, "goalType"    # Lcom/vectorwatch/android/utils/Constants$GoalType;

    .prologue
    .line 667
    const/4 v0, 0x0

    .line 669
    .local v0, "defaultValue":I
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$12;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoalType:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/Constants$GoalType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 683
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->log:Lorg/slf4j/Logger;

    const-string v2, "Should not get here. Check type of goal set in ActivityFragment."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 686
    :goto_0
    return v0

    .line 671
    :pswitch_0
    const/16 v0, 0x2710

    .line 672
    goto :goto_0

    .line 674
    :pswitch_1
    const/16 v0, 0x9c4

    .line 675
    goto :goto_0

    .line 677
    :pswitch_2
    const/4 v0, 0x5

    .line 678
    goto :goto_0

    .line 680
    :pswitch_3
    const/16 v0, 0x8

    .line 681
    goto :goto_0

    .line 669
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private convertDistanceToKm(I)F
    .locals 6
    .param p1, "distance"    # I

    .prologue
    .line 609
    int-to-double v2, p1

    const-wide v4, 0x40f86a0000000000L    # 100000.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 610
    .local v0, "result":F
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 611
    .local v1, "resultInt":I
    int-to-double v2, v1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 612
    return v0
.end method

.method private convertDistanceToMiles(I)F
    .locals 6
    .param p1, "distance"    # I

    .prologue
    .line 622
    int-to-float v2, p1

    const v3, 0x481d2980    # 160934.0f

    div-float v0, v2, v3

    .line 623
    .local v0, "result":F
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 624
    .local v1, "resultAux":I
    int-to-double v2, v1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 626
    return v0
.end method

.method private convertFromMetersToKm(I)F
    .locals 6
    .param p1, "meters"    # I

    .prologue
    .line 723
    int-to-double v2, p1

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 724
    .local v0, "result":F
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 725
    .local v1, "resultInt":I
    int-to-double v2, v1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 726
    return v0
.end method

.method private convertFromMetersToMiles(I)F
    .locals 6
    .param p1, "meters"    # I

    .prologue
    .line 730
    int-to-double v2, p1

    const-wide/high16 v4, 0x4099000000000000L    # 1600.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 731
    .local v0, "result":F
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 732
    .local v1, "resultInt":I
    int-to-double v2, v1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 733
    return v0
.end method

.method private convertSleep(I)F
    .locals 6
    .param p1, "quartersOfHour"    # I

    .prologue
    .line 715
    mul-int/lit8 v2, p1, 0xf

    .line 716
    .local v2, "minutes":I
    div-int/lit8 v1, v2, 0x3c

    .line 717
    .local v1, "fullHours":I
    rem-int/lit8 v0, v2, 0x3c

    .line 718
    .local v0, "extraMinutes":I
    mul-int/lit8 v4, v1, 0x64

    add-int/2addr v4, v0

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v3, v4, v5

    .line 719
    .local v3, "result":F
    return v3
.end method

.method private getActivityEntriesForCrtDay()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1074
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11}, Landroid/text/format/Time;-><init>()V

    .line 1075
    .local v11, "now":Landroid/text/format/Time;
    invoke-virtual {v11}, Landroid/text/format/Time;->setToNow()V

    .line 1076
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v14, v2

    .line 1078
    .local v14, "timeUnix":I
    iget v1, v11, Landroid/text/format/Time;->hour:I

    mul-int/lit8 v1, v1, 0x3c

    mul-int/lit8 v1, v1, 0x3c

    sub-int v1, v14, v1

    iget v2, v11, Landroid/text/format/Time;->minute:I

    mul-int/lit8 v2, v2, 0x3c

    sub-int/2addr v1, v2

    iget v2, v11, Landroid/text/format/Time;->second:I

    sub-int v12, v1, v2

    .line 1082
    .local v12, "startOfDay":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1085
    .local v6, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1086
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ACTIVITY:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "distance"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "calories"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "timestamp"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "timestamp >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1091
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1096
    :cond_0
    const-string v1, "value"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 1097
    .local v13, "steps":I
    const-string v1, "distance"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1098
    .local v9, "distance":I
    const-string v1, "calories"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1100
    .local v7, "calories":I
    new-instance v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;

    const/4 v1, 0x0

    invoke-direct {v10, p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$1;)V

    .line 1101
    .local v10, "entry":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;
    iput v7, v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;->calories:I

    .line 1102
    iput v13, v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;->steps:I

    .line 1103
    int-to-float v1, v9

    iput v1, v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;->distance:F

    .line 1108
    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1110
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1112
    .end local v7    # "calories":I
    .end local v9    # "distance":I
    .end local v10    # "entry":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;
    .end local v13    # "steps":I
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1114
    return-object v6
.end method

.method private handleSyncToWatch()V
    .locals 2

    .prologue
    .line 740
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->getActiveGoals(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendGoals(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 741
    return-void
.end method


# virtual methods
.method public bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;I)V
    .locals 0
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 606
    return-void
.end method

.method public changeEditMode(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V
    .locals 59
    .param p1, "mode"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    .prologue
    .line 1157
    if-eqz p1, :cond_0

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_0

    const/16 v30, 0x1

    .line 1158
    .local v30, "setStepsGoal":Z
    :goto_0
    if-eqz p1, :cond_1

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_1

    const/16 v44, 0x1

    .line 1159
    .local v44, "setDistanceGoal":Z
    :goto_1
    if-eqz p1, :cond_2

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->SLEEP:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    const/16 v16, 0x1

    .line 1160
    .local v16, "setSleepGoal":Z
    :goto_2
    if-eqz p1, :cond_3

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_3

    const/16 v58, 0x1

    .line 1162
    .local v58, "setCaloriesGoal":Z
    :goto_3
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1163
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090063

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x5dc

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1214
    :goto_4
    return-void

    .line 1157
    .end local v16    # "setSleepGoal":Z
    .end local v30    # "setStepsGoal":Z
    .end local v44    # "setDistanceGoal":Z
    .end local v58    # "setCaloriesGoal":Z
    :cond_0
    const/16 v30, 0x0

    goto :goto_0

    .line 1158
    .restart local v30    # "setStepsGoal":Z
    :cond_1
    const/16 v44, 0x0

    goto :goto_1

    .line 1159
    .restart local v44    # "setDistanceGoal":Z
    :cond_2
    const/16 v16, 0x0

    goto :goto_2

    .line 1160
    .restart local v16    # "setSleepGoal":Z
    :cond_3
    const/16 v58, 0x0

    goto :goto_3

    .line 1168
    .restart local v58    # "setCaloriesGoal":Z
    :cond_4
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_5

    .line 1169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivity:Landroid/app/Activity;

    check-cast v2, Lcom/vectorwatch/android/ui/BaseActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/BaseActivity;->startSupportActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 1170
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v2, :cond_6

    .line 1171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActionMode:Landroid/support/v7/view/ActionMode;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0900b6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 1174
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepNeedle:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    move/from16 v19, v0

    const/16 v20, 0xc

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepLL:Landroid/widget/LinearLayout;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTV:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepCenterTV:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTVLabel:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSleepMax:Landroid/widget/TextView;

    new-instance v2, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepNeedle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTV:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepModel:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v8, 0x0

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/view/View;Lcom/vectorwatch/android/models/QuadrantModel;ZLcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f09013d

    .line 1178
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v3, p0

    move-object v4, v14

    move-object/from16 v5, v17

    move-object/from16 v6, v18

    move/from16 v7, v19

    move/from16 v8, v20

    move-object/from16 v9, v21

    move-object v14, v2

    .line 1174
    invoke-virtual/range {v3 .. v16}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setActivityWidget(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;Landroid/widget/ImageView;FILandroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;Ljava/lang/String;Z)V

    .line 1179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->stepsQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsNeedle:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    move/from16 v21, v0

    const/16 v22, 0x2710

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsLL:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsCenterTV:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTVLabel:Landroid/widget/TextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueStepsMax:Landroid/widget/TextView;

    move-object/from16 v27, v0

    new-instance v2, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsNeedle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v8, 0x1

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/view/View;Lcom/vectorwatch/android/models/QuadrantModel;ZLcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    const-string v29, ""

    move-object/from16 v17, p0

    move-object/from16 v28, v2

    invoke-virtual/range {v17 .. v30}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setActivityWidget(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;Landroid/widget/ImageView;FILandroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;Ljava/lang/String;Z)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceNeedle:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    move/from16 v35, v0

    const/16 v36, 0xa

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceLL:Landroid/widget/LinearLayout;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceCenterTV:Landroid/widget/TextView;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTVLabel:Landroid/widget/TextView;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistanceMax:Landroid/widget/TextView;

    move-object/from16 v41, v0

    new-instance v2, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceNeedle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v8, 0x1

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/view/View;Lcom/vectorwatch/android/models/QuadrantModel;ZLcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    .line 1190
    invoke-static {v3}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f090146

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v43

    :goto_5
    move-object/from16 v31, p0

    move-object/from16 v42, v2

    .line 1186
    invoke-virtual/range {v31 .. v44}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setActivityWidget(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;Landroid/widget/ImageView;FILandroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;Ljava/lang/String;Z)V

    .line 1193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v46, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    move-object/from16 v47, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesNeedle:Landroid/widget/ImageView;

    move-object/from16 v48, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    move/from16 v49, v0

    const/16 v50, 0x1f4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesLL:Landroid/widget/LinearLayout;

    move-object/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesCenterTV:Landroid/widget/TextView;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTVLabel:Landroid/widget/TextView;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCaloriesMax:Landroid/widget/TextView;

    move-object/from16 v55, v0

    new-instance v2, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesNeedle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v8, 0x1

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/view/View;Lcom/vectorwatch/android/models/QuadrantModel;ZLcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    const-string v57, ""

    move-object/from16 v45, p0

    move-object/from16 v56, v2

    invoke-virtual/range {v45 .. v58}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setActivityWidget(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;Landroid/widget/ImageView;FILandroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;Ljava/lang/String;Z)V

    .line 1203
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    if-eqz v2, :cond_8

    .line 1204
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->saveGoals()V

    .line 1205
    new-instance v2, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->handleActivityTotalsUpdateEvent(Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;)V

    .line 1207
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    .line 1210
    :cond_8
    if-eqz p1, :cond_9

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_9

    .line 1211
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    .line 1213
    :cond_9
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentEditMode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    goto/16 :goto_4

    .line 1190
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0901ac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v43

    goto/16 :goto_5
.end method

.method public computeCurrentStatusForGoals()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1037
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->getActivityEntriesForCrtDay()Ljava/util/List;

    move-result-object v1

    .line 1039
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;>;"
    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    .line 1040
    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    .line 1041
    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    .line 1042
    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressSleepGoal:F

    .line 1043
    const/4 v0, 0x0

    .line 1045
    .local v0, "distanceReceived":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;

    .line 1046
    .local v2, "entry":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;
    int-to-float v4, v0

    iget v5, v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;->distance:F

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 1047
    iget v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    iget v5, v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;->steps:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    .line 1048
    iget v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    iget v5, v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;->calories:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    goto :goto_0

    .line 1052
    .end local v2    # "entry":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$ActivityEntry;
    :cond_0
    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    const v4, 0x186a0

    div-int/2addr v3, v4

    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    .line 1054
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->convertDistanceToKm(I)F

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    .line 1057
    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInProgressForCalories(Landroid/content/Context;)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    .line 1058
    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInProgressForDistance(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    .line 1059
    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInProgressForSteps(Landroid/content/Context;)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    .line 1062
    const/high16 v3, 0x40000000    # 2.0f

    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressSleepGoal:F

    .line 1065
    return-void
.end method

.method public getAndSetExistingGoalValues()V
    .locals 15

    .prologue
    const/high16 v14, -0x40800000    # -1.0f

    .line 943
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v3

    .line 944
    .local v3, "activeStepsGoal":F
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    .line 945
    .local v0, "activeCaloriesGoal":F
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v1

    .line 946
    .local v1, "activeDistanceGoal":F
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v2

    .line 950
    .local v2, "activeSleepGoal":F
    const/4 v7, 0x0

    .line 953
    .local v7, "isDoingDefaultGoalSetup":Z
    cmpl-float v10, v3, v14

    if-nez v10, :cond_0

    .line 954
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {p0, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->calculateDefaultGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;)I

    move-result v10

    int-to-float v3, v10

    .line 957
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 958
    const/4 v7, 0x1

    .line 960
    :cond_0
    iput v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    .line 961
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueStepsMax:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 964
    cmpl-float v10, v0, v14

    if-nez v10, :cond_1

    .line 965
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {p0, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->calculateDefaultGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;)I

    move-result v10

    int-to-float v0, v10

    .line 968
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 969
    const/4 v7, 0x1

    .line 971
    :cond_1
    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    .line 972
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 973
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCaloriesMax:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 979
    :goto_0
    cmpl-float v10, v1, v14

    if-nez v10, :cond_2

    .line 980
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {p0, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->calculateDefaultGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;)I

    move-result v10

    mul-int/lit16 v10, v10, 0x3e8

    int-to-float v1, v10

    .line 983
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 984
    const/4 v7, 0x1

    .line 988
    :cond_2
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 989
    const/high16 v10, 0x447a0000    # 1000.0f

    div-float v10, v1, v10

    iput v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 990
    const/16 v4, 0x3e8

    .line 995
    .local v4, "coeficient":I
    :goto_1
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v6

    .line 996
    .local v6, "format":Ljava/text/DecimalFormat;
    const-string v10, "#0.00"

    invoke-virtual {v6, v10}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 997
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 998
    sget-object v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->log:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTIVITY: IS METRIC = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1000
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v11, 0x7f090146

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1002
    .local v8, "unitSystem":Ljava/lang/String;
    :goto_2
    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-nez v10, :cond_8

    const-string v9, "0 "

    .line 1005
    .local v9, "value":Ljava/lang/String;
    :goto_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1007
    .local v5, "distanceGoalMax":Ljava/lang/String;
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistanceMax:Landroid/widget/TextView;

    invoke-virtual {v10, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1013
    .end local v5    # "distanceGoalMax":Ljava/lang/String;
    .end local v8    # "unitSystem":Ljava/lang/String;
    .end local v9    # "value":Ljava/lang/String;
    :goto_4
    cmpl-float v10, v2, v14

    if-nez v10, :cond_3

    .line 1014
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {p0, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->calculateDefaultGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;)I

    move-result v10

    int-to-float v2, v10

    .line 1017
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1018
    const/4 v7, 0x1

    .line 1020
    :cond_3
    float-to-int v10, v2

    int-to-float v10, v10

    iput v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    .line 1021
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSleepMax:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v12, v2

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v13, 0x7f09013d

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1023
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    int-to-float v11, v4

    div-float v11, v1, v11

    float-to-double v12, v11

    invoke-virtual {v6, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1024
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTV:Landroid/widget/TextView;

    iget v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    invoke-static {v11}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    iget v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    invoke-static {v11}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1026
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    iget v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    invoke-static {v11}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1028
    if-eqz v7, :cond_4

    .line 1029
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->handleSyncToWatch()V

    .line 1031
    :cond_4
    return-void

    .line 975
    .end local v4    # "coeficient":I
    .end local v6    # "format":Ljava/text/DecimalFormat;
    :cond_5
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCaloriesMax:Landroid/widget/TextView;

    const-string v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 992
    :cond_6
    const v10, 0x44c92000    # 1609.0f

    div-float v10, v1, v10

    iput v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 993
    const/16 v4, 0x649

    .restart local v4    # "coeficient":I
    goto/16 :goto_1

    .line 1000
    .restart local v6    # "format":Ljava/text/DecimalFormat;
    :cond_7
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v11, 0x7f0901ac

    .line 1001
    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    .line 1002
    .restart local v8    # "unitSystem":Ljava/lang/String;
    :cond_8
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    int-to-float v11, v4

    div-float v11, v1, v11

    float-to-double v12, v11

    invoke-virtual {v6, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3

    .line 1009
    .end local v8    # "unitSystem":Ljava/lang/String;
    :cond_9
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistanceMax:Landroid/widget/TextView;

    const-string v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public getHeaderId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 298
    int-to-long v0, p1

    return-wide v0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 245
    if-nez p1, :cond_2

    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->context:Landroid/content/Context;

    invoke-static {v8}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 246
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030067

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 247
    .local v7, "view":Landroid/view/View;
    const v8, 0x7f1001fa

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityAlertsButton:Landroid/widget/Button;

    .line 249
    const v8, 0x7f1001fb

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    .line 250
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x40400000    # 3.0f

    invoke-static {v9, v10}, Lcom/github/lzyzsd/circleprogress/Utils;->sp2px(Landroid/content/res/Resources;F)F

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 251
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x106000c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 253
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->context:Landroid/content/Context;

    const/16 v9, 0x8

    invoke-static {v8, v9}, Lcom/github/lzyzsd/circleprogress/Utils;->dpToPx(Landroid/content/Context;I)I

    move-result v4

    .line 254
    .local v4, "r":I
    const/16 v8, 0x8

    new-array v3, v8, [F

    const/4 v8, 0x0

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x1

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x2

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x3

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x4

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x5

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x6

    int-to-float v9, v4

    aput v9, v3, v8

    const/4 v8, 0x7

    int-to-float v9, v4

    aput v9, v3, v8

    .line 256
    .local v3, "outerR":[F
    new-instance v6, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v6, v3, v8, v9}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    .line 257
    .local v6, "rr":Landroid/graphics/drawable/shapes/RoundRectShape;
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v2, v6}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 258
    .local v2, "drawable":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0f00b4

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 260
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 262
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityAlertsButton:Landroid/widget/Button;

    new-instance v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$1;

    invoke-direct {v9, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 271
    .local v5, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllActivityAlerts(Lio/realm/Realm;)Lio/realm/RealmResults;

    move-result-object v0

    .line 272
    .local v0, "activityAlertRealmList":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;>;"
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsCalories(Landroid/content/Context;)I

    move-result v1

    .line 274
    .local v1, "calories":I
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/realm/RealmResults;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 275
    :cond_0
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityAlertsButton:Landroid/widget/Button;

    const v9, 0x7f02013b

    const/4 v10, 0x0

    const v11, 0x7f020063

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 276
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityAlertsButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->context:Landroid/content/Context;

    invoke-static {v9, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil;->getActivityMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    :goto_0
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 292
    .end local v0    # "activityAlertRealmList":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;>;"
    .end local v1    # "calories":I
    .end local v2    # "drawable":Landroid/graphics/drawable/ShapeDrawable;
    .end local v3    # "outerR":[F
    .end local v4    # "r":I
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v6    # "rr":Landroid/graphics/drawable/shapes/RoundRectShape;
    .end local v7    # "view":Landroid/view/View;
    :goto_1
    return-object v7

    .line 279
    .restart local v0    # "activityAlertRealmList":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;>;"
    .restart local v1    # "calories":I
    .restart local v2    # "drawable":Landroid/graphics/drawable/ShapeDrawable;
    .restart local v3    # "outerR":[F
    .restart local v4    # "r":I
    .restart local v5    # "realm":Lio/realm/Realm;
    .restart local v6    # "rr":Landroid/graphics/drawable/shapes/RoundRectShape;
    .restart local v7    # "view":Landroid/view/View;
    :cond_1
    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityAlertsButton:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v8

    check-cast v8, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-static {v8}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil;->getIconResourceForActivityAlert(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;)I

    move-result v8

    const/4 v10, 0x0

    const v11, 0x7f020063

    const/4 v12, 0x0

    invoke-virtual {v9, v8, v10, v11, v12}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 280
    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityAlertsButton:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v8

    check-cast v8, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v8}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lio/realm/RealmResults;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mBadgeView:Landroid/widget/TextView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 290
    .end local v0    # "activityAlertRealmList":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;>;"
    .end local v1    # "calories":I
    .end local v2    # "drawable":Landroid/graphics/drawable/ShapeDrawable;
    .end local v3    # "outerR":[F
    .end local v4    # "r":I
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v6    # "rr":Landroid/graphics/drawable/shapes/RoundRectShape;
    .end local v7    # "view":Landroid/view/View;
    :cond_2
    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 291
    .local v7, "view":Landroid/widget/TextView;
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHeight(I)V

    goto :goto_1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 240
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public handleActivityTotalsUpdateEvent(Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 691
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityContainerView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsSteps(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    .line 693
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsSleep(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->convertSleep(I)F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressSleepGoal:F

    .line 694
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 695
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsDistance(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->convertFromMetersToKm(I)F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    .line 699
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsCalories(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    .line 703
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressSleepGoal:F

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressStepsGoal:I

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressCaloriesGoal:I

    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->viewSetUp(FIIF)V

    .line 704
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityContainerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 706
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->invalidate()V

    .line 707
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->postInvalidate()V

    .line 708
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->stepsQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->postInvalidate()V

    .line 709
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->invalidate()V

    .line 711
    :cond_0
    return-void

    .line 697
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsDistance(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->convertFromMetersToMiles(I)F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressDistanceGoal:F

    goto :goto_0
.end method

.method public handleSleepQualityEvent(Lcom/vectorwatch/android/events/SleepQualityEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/SleepQualityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 652
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SLEEP QUALITY: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/SleepQualityEvent;->getSleepQuality()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 653
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/SleepQualityEvent;->getSleepQuality()I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepQuality:I

    .line 655
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepQuality:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepQuality:I

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 658
    :cond_0
    return-void
.end method

.method public initPersonalInfoDependantViews(Z)V
    .locals 5
    .param p1, "hasPersonalInfo"    # Z

    .prologue
    const v4, 0x7f0f00b2

    const/4 v3, 0x0

    .line 1118
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {p0, v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setSpokeView(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)V

    .line 1119
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {p0, v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setSpokeView(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)V

    .line 1121
    if-nez p1, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 1123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 1125
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCalories:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1126
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistance:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1127
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCaloriesMax:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1128
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistanceMax:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1130
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTVLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1131
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTVLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1133
    :cond_0
    return-void
.end method

.method public initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 11
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v8, 0x7f1000ca

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 303
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityContainerView:Landroid/view/View;

    .line 304
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityContainerView:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityLayout:Landroid/widget/RelativeLayout;

    .line 305
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityContainerView:Landroid/view/View;

    const v7, 0x7f1000f5

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mShareLayout:Landroid/widget/RelativeLayout;

    .line 307
    const v6, 0x7f1000e6

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    .line 308
    const v6, 0x7f1000d2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->stepsQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    .line 309
    const v6, 0x7f1000da

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    .line 310
    const v6, 0x7f1000f0

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    .line 312
    const v6, 0x7f1000e5

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepNeedle:Landroid/widget/ImageView;

    .line 313
    const v6, 0x7f1000ef

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceNeedle:Landroid/widget/ImageView;

    .line 314
    const v6, 0x7f1000d9

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesNeedle:Landroid/widget/ImageView;

    .line 315
    const v6, 0x7f1000d1

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsNeedle:Landroid/widget/ImageView;

    .line 317
    const v6, 0x7f1000e4

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .line 318
    const v6, 0x7f1000ee

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .line 319
    const v6, 0x7f1000d0

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .line 320
    const v6, 0x7f1000d8

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .line 322
    const v6, 0x7f1000d4

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsLL:Landroid/widget/LinearLayout;

    .line 323
    const v6, 0x7f1000dc

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesLL:Landroid/widget/LinearLayout;

    .line 324
    const v6, 0x7f1000e8

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepLL:Landroid/widget/LinearLayout;

    .line 325
    const v6, 0x7f1000f2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceLL:Landroid/widget/LinearLayout;

    .line 327
    const v6, 0x7f1000cb

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    .line 328
    const v6, 0x7f1000df

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTV:Landroid/widget/TextView;

    .line 329
    const v6, 0x7f1000e1

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    .line 330
    const v6, 0x7f1000cd

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    .line 332
    const v6, 0x7f1000cc

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTVLabel:Landroid/widget/TextView;

    .line 333
    const v6, 0x7f1000e0

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTVLabel:Landroid/widget/TextView;

    .line 334
    const v6, 0x7f1000e2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTVLabel:Landroid/widget/TextView;

    .line 335
    const v6, 0x7f1000ce

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTVLabel:Landroid/widget/TextView;

    .line 337
    const v6, 0x7f1000f1

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceCenterTV:Landroid/widget/TextView;

    .line 338
    const v6, 0x7f1000e7

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepCenterTV:Landroid/widget/TextView;

    .line 339
    const v6, 0x7f1000db

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesCenterTV:Landroid/widget/TextView;

    .line 340
    const v6, 0x7f1000d3

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsCenterTV:Landroid/widget/TextView;

    .line 342
    const v6, 0x7f1000d5

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSteps:Landroid/widget/TextView;

    .line 343
    const v6, 0x7f1000f3

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistance:Landroid/widget/TextView;

    .line 344
    const v6, 0x7f1000dd

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCalories:Landroid/widget/TextView;

    .line 345
    const v6, 0x7f1000e9

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSleep:Landroid/widget/TextView;

    .line 347
    const v6, 0x7f1000ea

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSleepMax:Landroid/widget/TextView;

    .line 348
    const v6, 0x7f1000d6

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueStepsMax:Landroid/widget/TextView;

    .line 349
    const v6, 0x7f1000de

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCaloriesMax:Landroid/widget/TextView;

    .line 350
    const v6, 0x7f1000f4

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistanceMax:Landroid/widget/TextView;

    .line 352
    const v6, 0x7f1001b8

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mScrollDummyView:Landroid/view/View;

    .line 353
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityLayout:Landroid/widget/RelativeLayout;

    .line 354
    const v6, 0x7f100073

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ScrollView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mScrollView:Landroid/widget/ScrollView;

    .line 355
    const v6, 0x7f1001b7

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mArrowDownImageView:Landroid/widget/ImageView;

    .line 356
    const v6, 0x7f1000f6

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mShareButton:Landroid/widget/Button;

    .line 358
    const v6, 0x7f1000f8

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 359
    const v6, 0x7f1000eb

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .line 361
    const v6, 0x7f1000f7

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mExportDataLink:Landroid/widget/TextView;

    .line 362
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mExportDataLink:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mExportDataLink:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 364
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mExportDataLink:Landroid/widget/TextView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$2;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 373
    iget v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepQuality:I

    if-lez v6, :cond_0

    .line 374
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iget v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepQuality:I

    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 377
    :cond_0
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 378
    .local v0, "display":Landroid/view/Display;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 379
    .local v5, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 381
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivityLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 383
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v6}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    .line 384
    .local v2, "hasMenuKey":Z
    const/4 v6, 0x4

    invoke-static {v6}, Landroid/view/KeyCharacterMap;->deviceHasKey(I)Z

    move-result v1

    .line 387
    .local v1, "hasBackKey":Z
    if-nez v2, :cond_1

    if-nez v1, :cond_1

    .line 391
    iget v6, v5, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const/high16 v8, 0x43480000    # 200.0f

    invoke-static {v7, v8}, Lcom/github/lzyzsd/circleprogress/Utils;->pxFromDp(Landroid/content/Context;F)F

    move-result v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 396
    :goto_0
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mShareButton:Landroid/widget/Button;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450
    iput-boolean v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSomethingToSave:Z

    .line 451
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v3

    .line 452
    .local v3, "hasPersonalInfo":Z
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {p0, v6, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setSpokeView(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)V

    .line 453
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsBack:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {p0, v6, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->setSpokeView(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)V

    .line 454
    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->initPersonalInfoDependantViews(Z)V

    .line 457
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsCenterTV:Landroid/widget/TextView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$4;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesCenterTV:Landroid/widget/TextView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$5;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 491
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepCenterTV:Landroid/widget/TextView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 508
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceCenterTV:Landroid/widget/TextView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$7;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$7;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->stepsQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$8;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$8;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 558
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$10;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$10;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 565
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$11;

    invoke-direct {v7, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$11;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V

    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 593
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->getAndSetExistingGoalValues()V

    .line 594
    new-instance v6, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;

    invoke-direct {v6}, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;-><init>()V

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->handleActivityTotalsUpdateEvent(Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;)V

    .line 596
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 597
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mShareLayout:Landroid/widget/RelativeLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 601
    :goto_1
    return-void

    .line 393
    .end local v3    # "hasPersonalInfo":Z
    :cond_1
    iget v6, v5, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const/high16 v8, 0x43180000    # 152.0f

    invoke-static {v7, v8}, Lcom/github/lzyzsd/circleprogress/Utils;->pxFromDp(Landroid/content/Context;F)F

    move-result v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 599
    .restart local v3    # "hasPersonalInfo":Z
    :cond_2
    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mShareLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public registerEvent()V
    .locals 1

    .prologue
    .line 630
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 631
    return-void
.end method

.method public saveGoals()V
    .locals 14

    .prologue
    .line 818
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat(Ljava/util/Locale;)Ljava/text/DecimalFormat;

    move-result-object v9

    .line 819
    .local v9, "format":Ljava/text/DecimalFormat;
    const-string v0, "#0.00"

    invoke-virtual {v9, v0}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 826
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 827
    .local v11, "newDistanceGoal":Ljava/lang/Float;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    .line 828
    .local v13, "newStepsGoal":Ljava/lang/Float;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    .line 829
    .local v10, "newCaloriesGoal":Ljava/lang/Float;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 837
    .local v12, "newSleepGoal":Ljava/lang/Float;
    :goto_0
    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 839
    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    .line 840
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    float-to-int v1, v1

    div-int/lit16 v1, v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 842
    const/4 v0, 0x0

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->adjustGoals(IF)V

    .line 844
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    .line 845
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    const/high16 v2, 0x43fa0000    # 500.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 846
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 848
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    float-to-int v7, v0

    .line 850
    .local v7, "distanceGoal":I
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 851
    div-int/lit16 v0, v7, 0x3e8

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 852
    const/16 v6, 0x3e8

    .line 857
    .local v6, "coeficient":I
    :goto_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 858
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    int-to-float v2, v7

    int-to-float v3, v6

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v9, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 860
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    .line 861
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 863
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STEPS_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v3, 0x0

    check-cast v3, Ljava/lang/String;

    .line 864
    invoke-virtual {v13}, Ljava/lang/Float;->longValue()J

    move-result-wide v4

    .line 863
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    .line 867
    .end local v6    # "coeficient":I
    .end local v7    # "distanceGoal":I
    :cond_0
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 868
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 869
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    .line 870
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    float-to-int v1, v1

    div-int/lit16 v1, v1, 0x1f4

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 872
    const/4 v0, 0x1

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->adjustGoals(IF)V

    .line 874
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    float-to-int v7, v0

    .line 876
    .restart local v7    # "distanceGoal":I
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 877
    div-int/lit16 v0, v7, 0x3e8

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 878
    const/16 v6, 0x3e8

    .line 883
    .restart local v6    # "coeficient":I
    :goto_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 884
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceTV:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    int-to-float v2, v7

    int-to-float v3, v6

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v9, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 886
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    .line 887
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    const v2, 0x461c4000    # 10000.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 888
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 890
    const-string v0, "VALUE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 894
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_CALORIES_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v3, 0x0

    check-cast v3, Ljava/lang/String;

    .line 895
    invoke-virtual {v10}, Ljava/lang/Float;->longValue()J

    move-result-wide v4

    .line 894
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    .line 898
    .end local v6    # "coeficient":I
    .end local v7    # "distanceGoal":I
    :cond_1
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 899
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 901
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 906
    :goto_3
    invoke-virtual {v11}, Ljava/lang/Float;->intValue()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 907
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 909
    const/4 v0, 0x2

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->adjustGoals(IF)V

    .line 911
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    .line 912
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    const v2, 0x461c4000    # 10000.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 913
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsTV:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 915
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    .line 916
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    const/high16 v2, 0x43fa0000    # 500.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 917
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesTV:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 919
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_DISTANCE_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v3, 0x0

    check-cast v3, Ljava/lang/String;

    .line 920
    invoke-virtual {v11}, Ljava/lang/Float;->longValue()J

    move-result-wide v4

    .line 919
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    .line 923
    :cond_2
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 924
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 925
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    .line 926
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepModel:Lcom/vectorwatch/android/models/QuadrantModel;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0xc

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 928
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SLEEP_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v3, 0x0

    check-cast v3, Ljava/lang/String;

    .line 929
    invoke-virtual {v12}, Ljava/lang/Float;->longValue()J

    move-result-wide v4

    .line 928
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    .line 932
    :cond_3
    const-string v0, "flag_changed_goals"

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 935
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->handleSyncToWatch()V

    .line 936
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 937
    return-void

    .line 830
    .end local v10    # "newCaloriesGoal":Ljava/lang/Float;
    .end local v11    # "newDistanceGoal":Ljava/lang/Float;
    .end local v12    # "newSleepGoal":Ljava/lang/Float;
    .end local v13    # "newStepsGoal":Ljava/lang/Float;
    :catch_0
    move-exception v8

    .line 831
    .local v8, "e":Ljava/text/ParseException;
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 832
    .restart local v11    # "newDistanceGoal":Ljava/lang/Float;
    const v0, 0x461c4000    # 10000.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    .line 833
    .restart local v13    # "newStepsGoal":Ljava/lang/Float;
    const v0, 0x455ac000    # 3500.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    .line 834
    .restart local v10    # "newCaloriesGoal":Ljava/lang/Float;
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    .restart local v12    # "newSleepGoal":Ljava/lang/Float;
    goto/16 :goto_0

    .line 854
    .end local v8    # "e":Ljava/text/ParseException;
    .restart local v7    # "distanceGoal":I
    :cond_4
    div-int/lit16 v0, v7, 0x649

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 855
    const/16 v6, 0x649

    .restart local v6    # "coeficient":I
    goto/16 :goto_1

    .line 880
    .end local v6    # "coeficient":I
    :cond_5
    div-int/lit16 v0, v7, 0x649

    int-to-float v0, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    .line 881
    const/16 v6, 0x649

    .restart local v6    # "coeficient":I
    goto/16 :goto_2

    .line 904
    .end local v6    # "coeficient":I
    .end local v7    # "distanceGoal":I
    :cond_6
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const v1, 0x44c92000    # 1609.0f

    mul-float/2addr v0, v1

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    goto/16 :goto_3
.end method

.method public setActivityWidget(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;Landroid/widget/ImageView;FILandroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "backgroundWheel"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;
    .param p2, "quadrantView"    # Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;
    .param p3, "needle"    # Landroid/widget/ImageView;
    .param p4, "goal"    # F
    .param p5, "maxValue"    # I
    .param p6, "labels"    # Landroid/widget/LinearLayout;
    .param p7, "goalTV"    # Landroid/widget/TextView;
    .param p8, "labelCenterTV"    # Landroid/widget/TextView;
    .param p9, "tvLabel"    # Landroid/widget/TextView;
    .param p10, "labelMax"    # Landroid/widget/TextView;
    .param p11, "listener"    # Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;
    .param p12, "units"    # Ljava/lang/String;
    .param p13, "setGoal"    # Z

    .prologue
    .line 1220
    if-eqz p13, :cond_0

    .line 1224
    const/4 v2, 0x4

    invoke-virtual {p2, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setVisibility(I)V

    .line 1226
    const/4 v2, 0x0

    invoke-virtual {p3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1228
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, p4

    int-to-float v3, p5

    div-float/2addr v2, v3

    const v3, 0x40666666    # 3.6f

    mul-float/2addr v2, v3

    invoke-virtual {p3, v2}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1230
    const/4 v2, 0x4

    invoke-virtual {p6, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1232
    const/4 v2, 0x0

    invoke-virtual {p8, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1233
    const v2, 0x7f090224

    invoke-virtual {p8, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1235
    const/4 v2, 0x0

    invoke-virtual {p7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1237
    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1239
    move-object/from16 v0, p11

    invoke-virtual {p1, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1241
    const/4 v2, 0x4

    invoke-virtual {p9, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1270
    :goto_0
    return-void

    .line 1246
    :cond_0
    if-eqz p9, :cond_1

    .line 1247
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p9, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1250
    :cond_1
    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    cmpl-float v2, p4, v2

    if-nez v2, :cond_2

    .line 1251
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p10, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1256
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setVisibility(I)V

    .line 1258
    const/4 v2, 0x4

    invoke-virtual {p3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1260
    const/4 v2, 0x0

    invoke-virtual {p6, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1262
    const/4 v2, 0x4

    invoke-virtual {p8, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1264
    const/4 v2, 0x4

    invoke-virtual {p7, v2}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1265
    :catch_0
    move-exception v1

    .line 1266
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setActivityWidget: exception  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1253
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p10, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public setSleptTime(F)V
    .locals 4
    .param p1, "sleptTime"    # F

    .prologue
    .line 749
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    const/high16 v1, 0x41400000    # 12.0f

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 750
    return-void
.end method

.method public setSpokeView(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)V
    .locals 8
    .param p1, "view"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;
    .param p2, "enabled"    # Z

    .prologue
    const/16 v7, 0x28

    const/4 v6, 0x0

    const/high16 v5, 0x41000000    # 8.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1136
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v4, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 1137
    .local v1, "dip":F
    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    .line 1138
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1141
    .local v0, "batteryColor":I
    :goto_0
    const/16 v2, 0x3c

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokesNo(I)V

    .line 1142
    mul-float v2, v1, v4

    float-to-int v2, v2

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBStrokeWidth(I)V

    .line 1143
    mul-float v2, v1, v4

    float-to-int v2, v2

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAStrokeWidth(I)V

    .line 1144
    const/16 v2, 0x64

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAPercent(I)V

    .line 1145
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBPercent(I)V

    .line 1146
    invoke-virtual {p1, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBColor(I)V

    .line 1147
    invoke-virtual {p1, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAColor(I)V

    .line 1148
    invoke-virtual {p1, v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setDrawAllBSpokes(Z)V

    .line 1149
    invoke-virtual {p1, v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setClickable(Z)V

    .line 1150
    mul-float v2, v1, v5

    float-to-int v2, v2

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBPadding(I)V

    .line 1151
    mul-float v2, v1, v5

    float-to-int v2, v2

    invoke-virtual {p1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAPadding(I)V

    .line 1152
    invoke-virtual {p1, v7}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeARadiusPercent(I)V

    .line 1153
    invoke-virtual {p1, v7}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBRadiusPercent(I)V

    .line 1154
    return-void

    .line 1138
    .end local v0    # "batteryColor":I
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    .line 1139
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public unregisterEvent()V
    .locals 1

    .prologue
    .line 634
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 635
    return-void
.end method

.method public viewSetUp(FIIF)V
    .locals 10
    .param p1, "crtValueSleep"    # F
    .param p2, "crtValueSteps"    # I
    .param p3, "crtValueCalories"    # I
    .param p4, "crtValueDistance"    # F

    .prologue
    .line 755
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    if-nez v0, :cond_0

    .line 756
    new-instance v0, Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    int-to-float v3, p2

    const/16 v4, 0x2710

    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    float-to-int v5, v5

    div-int/lit16 v5, v5, 0x2710

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/models/QuadrantModel;-><init>(FFFII)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    .line 760
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepModel:Lcom/vectorwatch/android/models/QuadrantModel;

    if-nez v0, :cond_1

    .line 761
    new-instance v0, Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    const/16 v4, 0xc

    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    float-to-int v3, v3

    div-int/lit8 v5, v3, 0xc

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/models/QuadrantModel;-><init>(FFFII)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepModel:Lcom/vectorwatch/android/models/QuadrantModel;

    .line 765
    :goto_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    if-nez v0, :cond_2

    .line 766
    new-instance v0, Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    const/16 v4, 0xa

    iget v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    float-to-int v3, v3

    div-int/lit8 v5, v3, 0xa

    move v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/models/QuadrantModel;-><init>(FFFII)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    .line 770
    :goto_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    if-nez v0, :cond_3

    .line 771
    new-instance v0, Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    int-to-float v3, p3

    const/16 v4, 0x1f4

    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    float-to-int v5, v5

    div-int/lit16 v5, v5, 0x1f4

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/models/QuadrantModel;-><init>(FFFII)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    .line 778
    :goto_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->sleepQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentSleepGoal:F

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 779
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->stepsQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    int-to-float v1, p2

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentStepsGoal:F

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 780
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 781
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    int-to-float v1, p3

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 782
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    iget v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, p4, v1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 788
    :goto_4
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p1

    float-to-double v0, v0

    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    sub-double v8, v0, v2

    .line 789
    .local v8, "minutes":D
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    cmpg-double v0, v8, v0

    if-gez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    double-to-int v1, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 790
    .local v7, "minutesString":Ljava/lang/String;
    :goto_5
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_6

    .line 791
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSleep:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0901c0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 795
    :goto_6
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSteps:Landroid/widget/TextView;

    int-to-float v1, p2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 796
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 797
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCalories:Landroid/widget/TextView;

    int-to-float v1, p3

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 798
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f090146

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 800
    .local v6, "distanceInd":Ljava/lang/String;
    :goto_7
    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-nez v0, :cond_8

    .line 801
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistance:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 810
    .end local v6    # "distanceInd":Ljava/lang/String;
    :goto_8
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->getAndSetExistingGoalValues()V

    .line 811
    return-void

    .line 758
    .end local v7    # "minutesString":Ljava/lang/String;
    .end local v8    # "minutes":D
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mStepsModel:Lcom/vectorwatch/android/models/QuadrantModel;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setCurentValue(F)V

    goto/16 :goto_0

    .line 763
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mSleepModel:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/QuadrantModel;->setCurentValue(F)V

    goto/16 :goto_1

    .line 768
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mDistanceModel:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/models/QuadrantModel;->setCurentValue(F)V

    goto/16 :goto_2

    .line 774
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCaloriesModel:Lcom/vectorwatch/android/models/QuadrantModel;

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/QuadrantModel;->setCurentValue(F)V

    goto/16 :goto_3

    .line 784
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->caloriesQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentCaloriesGoal:F

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    .line 785
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->distanceQuadrantView:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mCurrentDistanceGoal:F

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setValues(FFI)V

    goto/16 :goto_4

    .line 789
    .restart local v8    # "minutes":D
    :cond_5
    double-to-int v0, v8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_5

    .line 793
    .restart local v7    # "minutesString":Ljava/lang/String;
    :cond_6
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueSleep:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, p1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 798
    :cond_7
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f0901ac

    .line 799
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_7

    .line 803
    .restart local v6    # "distanceInd":Ljava/lang/String;
    :cond_8
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistance:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 806
    .end local v6    # "distanceInd":Ljava/lang/String;
    :cond_9
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueCalories:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 807
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->mValueDistance:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8
.end method
