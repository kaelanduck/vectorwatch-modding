.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;
.super Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
.source "NotificationAlertListAdapter.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterBinder;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mActivityAlertsList:Lio/realm/RealmResults;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmResults",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field public final row:I
    .annotation build Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        initMethod = false
        layout = 0x7f03009c
        views = {
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f1001fa
                name = "alert"
                type = Landroid/widget/Button;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100238
                name = "dateTag"
                type = Landroid/widget/TextView;
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lio/realm/RealmResults;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lio/realm/RealmResults",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, "activityAlertsList":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;>;"
    invoke-direct {p0, p1}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;-><init>(Landroid/content/Context;)V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->row:I

    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mActivityAlertsList:Lio/realm/RealmResults;

    .line 55
    return-void
.end method


# virtual methods
.method public bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;I)V
    .locals 7
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 74
    iget-object v1, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;->alert:Landroid/widget/Button;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v0, p2}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil;->getIconResourceForActivityAlert(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;)I

    move-result v0

    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 75
    iget-object v1, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;->alert:Landroid/widget/Button;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v0, p2}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 78
    .local v2, "now":J
    iget-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;->dateTag:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v6, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;->dateTag:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v0, p2}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->getTimestamp()J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    const/4 v0, 0x5

    .line 59
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v1}, Lio/realm/RealmResults;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v0}, Lio/realm/RealmResults;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 69
    const-wide/16 v0, 0x0

    return-wide v0
.end method
