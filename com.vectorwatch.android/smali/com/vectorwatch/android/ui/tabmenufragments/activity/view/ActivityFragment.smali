.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
.super Landroid/app/Fragment;
.source "ActivityFragment.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field public mActionMode:Landroid/support/v7/view/ActionMode;

.field private mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

.field private mActivityListView:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

.field public mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

.field private mGraphFAB:Lcom/software/shell/fab/ActionButton;

.field private mLinkStateImage:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 52
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    return-object v0
.end method

.method public static newInstance()Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;-><init>()V

    .line 94
    .local v0, "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    return-object v0
.end method


# virtual methods
.method public handleActivityTabSelectedEvent(Lcom/vectorwatch/android/events/ActivityTabSelectedEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/vectorwatch/android/events/ActivityTabSelectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 199
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTIVITY FRAGMENT - visible = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->isVisible()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " added = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->isAdded()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " detached"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 200
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->isDetached()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is removing = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->isRemoving()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 199
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 202
    const-string v2, "flag_show_sync_to_fit_popup"

    const/4 v3, 0x1

    .line 203
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 202
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 204
    .local v1, "showSyncToGoogleFit":Z
    if-eqz v1, :cond_0

    .line 205
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 207
    .local v0, "dialog":Landroid/app/AlertDialog;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0c00c5

    invoke-direct {v2, v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f090060

    .line 209
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f090296

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$4;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)V

    .line 210
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901bb

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)V

    .line 219
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 225
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 227
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    :cond_0
    return-void
.end method

.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 192
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 195
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 257
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 258
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    .line 262
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 269
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->setUpLinkLostState()V

    .line 251
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 253
    :cond_0
    return-void
.end method

.method public handleTabSelectedEvent(Lcom/vectorwatch/android/events/TabSelectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/TabSelectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/TabSelectedEvent;->getTabIndex()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->changeEditMode(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    .line 160
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 165
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 101
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    const v1, 0x7f030055

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 120
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f1001b6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListView:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    .line 121
    const v1, 0x7f1001b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 122
    const v1, 0x7f1001b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/software/shell/fab/ActionButton;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    .line 124
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    invoke-direct {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;-><init>(Landroid/app/Activity;Landroid/support/v7/view/ActionMode;Landroid/support/v7/view/ActionMode$Callback;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 125
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListView:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-virtual {v1, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setAdapter(Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V

    .line 127
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->setUpLinkLostState()V

    .line 129
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    sget-object v2, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    .line 130
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    const v2, 0x7f0200d5

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 131
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setButtonColor(I)V

    .line 132
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    .line 133
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setButtonColorPressed(I)V

    .line 134
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    const/high16 v2, 0x40600000    # 3.5f

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setShadowYOffset(F)V

    .line 135
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setShadowXOffset(F)V

    .line 136
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setShadowColor(I)V

    .line 137
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-static {v2, v3}, Lcom/github/lzyzsd/circleprogress/Utils;->pxFromDp(Landroid/content/Context;F)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setImageSize(F)V

    .line 138
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mGraphFAB:Lcom/software/shell/fab/ActionButton;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)V

    invoke-virtual {v1, v2}, Lcom/software/shell/fab/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 186
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->unregisterEvent()V

    .line 188
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 171
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->setUpLinkLostState()V

    .line 172
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->registerEvent()V

    .line 173
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActivityListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->notifyDataSetChanged()V

    .line 174
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 175
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 106
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->log:Lorg/slf4j/Logger;

    const-string v1, "ACTIVITY FRAGMENT - On start."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 112
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 153
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 154
    return-void
.end method

.method public setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 233
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    .line 237
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 179
    invoke-super {p0, p1}, Landroid/app/Fragment;->setUserVisibleHint(Z)V

    .line 180
    return-void
.end method
