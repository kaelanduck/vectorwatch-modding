.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil;
.super Ljava/lang/Object;
.source "ActivityAlertUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;
    }
.end annotation


# static fields
.field public static final NUMBER_OF_VISIBLE_ALERTS:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method

.method public static getActivityMessage(Landroid/content/Context;I)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "calories"    # I

    .prologue
    .line 30
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v6

    double-to-int v1, v6

    .line 31
    .local v1, "bmr":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 32
    .local v2, "cal":Ljava/util/Calendar;
    const/16 v6, 0xb

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    int-to-float v6, v6

    const/16 v7, 0xc

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x42700000    # 60.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    const/high16 v7, 0x41c00000    # 24.0f

    div-float v4, v6, v7

    .line 34
    .local v4, "pd":F
    mul-int/lit16 v6, p1, 0x3e8

    int-to-float v6, v6

    int-to-float v7, v1

    mul-float/2addr v7, v4

    div-float/2addr v6, v7

    float-to-int v0, v6

    .line 36
    .local v0, "activityFactor":I
    const/16 v6, 0x55f

    if-ge v0, v6, :cond_0

    .line 37
    const v6, 0x7f09020c

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 48
    .local v5, "state":Ljava/lang/String;
    :goto_0
    const v6, 0x7f09009c

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "message":Ljava/lang/String;
    return-object v3

    .line 38
    .end local v3    # "message":Ljava/lang/String;
    .end local v5    # "state":Ljava/lang/String;
    :cond_0
    const/16 v6, 0x60e

    if-ge v0, v6, :cond_1

    .line 39
    const v6, 0x7f090197

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "state":Ljava/lang/String;
    goto :goto_0

    .line 40
    .end local v5    # "state":Ljava/lang/String;
    :cond_1
    const/16 v6, 0x6bd

    if-ge v0, v6, :cond_2

    .line 41
    const v6, 0x7f090129

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "state":Ljava/lang/String;
    goto :goto_0

    .line 42
    .end local v5    # "state":Ljava/lang/String;
    :cond_2
    const/16 v6, 0x76c

    if-ge v0, v6, :cond_3

    .line 43
    const v6, 0x7f090051

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "state":Ljava/lang/String;
    goto :goto_0

    .line 45
    .end local v5    # "state":Ljava/lang/String;
    :cond_3
    const v6, 0x7f09013a

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "state":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getActivityTypeForString(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;
    .locals 1
    .param p0, "resourceType"    # Ljava/lang/String;

    .prologue
    .line 97
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 120
    :goto_0
    return-object v0

    .line 99
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 101
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 103
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 105
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 106
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 107
    :cond_4
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 108
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 109
    :cond_5
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 110
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 111
    :cond_6
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 112
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 113
    :cond_7
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 114
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto :goto_0

    .line 115
    :cond_8
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 116
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto/16 :goto_0

    .line 117
    :cond_9
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 118
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto/16 :goto_0

    .line 120
    :cond_a
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    goto/16 :goto_0
.end method

.method public static getIconResourceForActivityAlert(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;)I
    .locals 1
    .param p0, "activityAlert"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->getActivityType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil;->getActivityTypeForString(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->getResourceId()I

    move-result v0

    return v0
.end method
