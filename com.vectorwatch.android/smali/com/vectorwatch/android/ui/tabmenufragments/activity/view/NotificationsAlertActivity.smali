.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "NotificationsAlertActivity.java"


# instance fields
.field private count:I

.field private initialTime:J

.field private mActivityAlertsList:Lio/realm/RealmResults;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmResults",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            ">;"
        }
    .end annotation
.end field

.field mBtnClear:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10010e
        }
    .end annotation
.end field

.field mBtnClearAll:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10010f
        }
    .end annotation
.end field

.field mEmptyText:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100110
        }
    .end annotation
.end field

.field mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10010d
        }
    .end annotation
.end field

.field private mNotificationAlertListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;

.field private mRealm:Lio/realm/Realm;

.field mainLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10010c
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearAllButtonClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10010f
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0}, Lio/realm/Realm;->beginTransaction()V

    .line 119
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v0}, Lio/realm/RealmResults;->deleteAllFromRealm()Z

    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0}, Lio/realm/Realm;->commitTransaction()V

    .line 121
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->finish()V

    .line 122
    return-void
.end method

.method public onClearButtonClicked()V
    .locals 9
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10010e
        }
    .end annotation

    .prologue
    const/4 v3, 0x5

    const/4 v8, 0x0

    .line 86
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 87
    .local v0, "currentTime":J
    iget-wide v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->initialTime:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 88
    iput-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->initialTime:J

    .line 91
    :cond_0
    iget-wide v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->initialTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3a98

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 92
    iget v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->count:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->count:I

    .line 98
    :goto_0
    iget v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->count:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 99
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClearAll:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 102
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    invoke-virtual {v4}, Lio/realm/Realm;->beginTransaction()V

    .line 104
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v4}, Lio/realm/RealmResults;->size()I

    move-result v4

    if-le v4, v3, :cond_3

    .line 106
    .local v3, "size":I
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v3, :cond_4

    .line 107
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v4, v8}, Lio/realm/RealmResults;->deleteFromRealm(I)V

    .line 106
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 94
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_2
    const/4 v4, 0x1

    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->count:I

    .line 95
    iput-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->initialTime:J

    goto :goto_0

    .line 104
    :cond_3
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v4}, Lio/realm/RealmResults;->size()I

    move-result v3

    goto :goto_1

    .line 109
    .restart local v2    # "i":I
    .restart local v3    # "size":I
    :cond_4
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    invoke-virtual {v4}, Lio/realm/Realm;->commitTransaction()V

    .line 110
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mNotificationAlertListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;->notifyDataSetChanged()V

    .line 111
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v4}, Lio/realm/RealmResults;->size()I

    move-result v4

    if-nez v4, :cond_5

    .line 112
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClear:Landroid/widget/Button;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 114
    :cond_5
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, -0x1

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->requestWindowFeature(I)Z

    .line 54
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f030029

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->setContentView(I)V

    .line 56
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 57
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 59
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    .line 60
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllActivityAlerts(Lio/realm/Realm;)Lio/realm/RealmResults;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    .line 61
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;-><init>(Landroid/content/Context;Lio/realm/RealmResults;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mNotificationAlertListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;

    .line 62
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mNotificationAlertListAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mActivityAlertsList:Lio/realm/RealmResults;

    invoke-virtual {v0}, Lio/realm/RealmResults;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClear:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 70
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->count:I

    .line 71
    return-void
.end method

.method public onMainViewClicked()V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10010c
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->onBackPressed()V

    .line 82
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onStop()V

    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0}, Lio/realm/Realm;->close()V

    .line 77
    return-void
.end method
