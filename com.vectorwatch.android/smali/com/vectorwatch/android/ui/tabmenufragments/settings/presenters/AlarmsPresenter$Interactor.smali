.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;
.super Ljava/lang/Object;
.source "AlarmsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Interactor"
.end annotation


# virtual methods
.method public abstract notifyAlarmLimitStatus(Z)V
.end method

.method public abstract sendAlarms(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;)V"
        }
    .end annotation
.end method
