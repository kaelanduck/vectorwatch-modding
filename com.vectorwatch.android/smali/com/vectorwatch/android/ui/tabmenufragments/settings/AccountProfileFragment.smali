.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
.super Landroid/app/Fragment;
.source "AccountProfileFragment.java"


# static fields
.field private static final COUNT_MAX_NAME_CHARACTERS:I = 0xf

.field private static final NAME_MAX_CHARACTERS_COUNT:I = 0xf

.field public static final TIME_FORMAT_12H:I = 0x1

.field public static final TIME_FORMAT_24H:I = 0x0

.field public static final UNIT_SYSTEM_FORMAT_IMPERIAL:I = 0x1

.field public static final UNIT_SYSTEM_FORMAT_METRIC:I = 0x0

.field public static final UNSET_VALUE:I = -0x1

.field private static final VALUE_24_HOURS_FORMAT:Ljava/lang/String; = "24:00"

.field private static final VALUE_AM_PM_FORMAT:Ljava/lang/String; = "AM/PM"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mCallbackManager:Lcom/facebook/CallbackManager;

.field private mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

.field private mContext:Landroid/content/Context;

.field private mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

.field private mFacebookStatus:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private mLinkStateImage:Landroid/widget/TextView;

.field private mTempChoiceTimeFormat:I

.field private mTempChoiceUnitSystem:I

.field private mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private syncSettingsNeeded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->syncSettingsNeeded:Z

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->handleNameChanged(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 53
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTempChoiceTimeFormat:I

    return v0
.end method

.method static synthetic access$302(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTempChoiceTimeFormat:I

    return p1
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getDefaultChoiceForTimeFormat()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->handleTimeFormatSelected(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 53
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTempChoiceUnitSystem:I

    return v0
.end method

.method static synthetic access$602(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTempChoiceUnitSystem:I

    return p1
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getDefaultChoiceForUnitSystem()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->handleUnitSystemFormatSelected(I)V

    return-void
.end method

.method private getDefaultChoiceForTimeFormat()I
    .locals 3

    .prologue
    .line 599
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTimeFormatPreference(Landroid/content/Context;)I

    move-result v1

    .line 600
    .local v1, "savedPreferenceTimeFormat":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 602
    .local v0, "defaultChoiceTimeFormat":I
    :goto_0
    return v0

    .end local v0    # "defaultChoiceTimeFormat":I
    :cond_0
    move v0, v1

    .line 600
    goto :goto_0
.end method

.method private getDefaultChoiceForUnitSystem()I
    .locals 3

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getUnitSystemFormatPreference(Landroid/content/Context;)I

    move-result v1

    .line 614
    .local v1, "savedPreferenceUnitSystem":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 616
    .local v0, "defaultChoiceUnitSystem":I
    :goto_0
    return v0

    .end local v0    # "defaultChoiceUnitSystem":I
    :cond_0
    move v0, v1

    .line 614
    goto :goto_0
.end method

.method private handleNameChanged(Ljava/lang/String;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0xf

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 483
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 484
    .local v0, "oldName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    :goto_0
    return-void

    .line 490
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 491
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 492
    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 495
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setGreetingUserName(Ljava/lang/String;Landroid/content/Context;)V

    .line 496
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 497
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 498
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f090205

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 500
    iput-boolean v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->syncSettingsNeeded:Z

    .line 509
    :goto_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 510
    const-string v1, "flag_changed_activity_profile"

    .line 511
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 510
    invoke-static {v1, v6, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_0

    .line 502
    :cond_2
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setGreetingUserName(Ljava/lang/String;Landroid/content/Context;)V

    .line 504
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 505
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 506
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f090206

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private handleTimeFormatSelected(I)V
    .locals 4
    .param p1, "timeFormat"    # I

    .prologue
    const/4 v3, 0x1

    .line 515
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTimeFormatPreference(Landroid/content/Context;)I

    move-result v0

    .line 517
    .local v0, "oldTimeFormat":I
    if-ne v0, p1, :cond_0

    .line 531
    :goto_0
    return-void

    .line 522
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTimeFormatPreference(ILandroid/content/Context;)V

    .line 524
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->updateTimeFormatValueField(I)V

    .line 526
    iput-boolean v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->syncSettingsNeeded:Z

    .line 528
    const-string v1, "flag_changed_activity_profile"

    .line 529
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 528
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 530
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleUnitSystemFormatSelected(I)V
    .locals 4
    .param p1, "unitSystem"    # I

    .prologue
    const/4 v3, 0x1

    .line 534
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getUnitSystemFormatPreference(Landroid/content/Context;)I

    move-result v0

    .line 536
    .local v0, "oldUnitSystemFormat":I
    if-ne v0, p1, :cond_0

    .line 550
    :goto_0
    return-void

    .line 541
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setUnitSystemFormatPreference(ILandroid/content/Context;)V

    .line 543
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->updateUnitSystemFormatValueField(I)V

    .line 545
    iput-boolean v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->syncSettingsNeeded:Z

    .line 547
    const-string v1, "flag_changed_activity_profile"

    .line 548
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 547
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 549
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private initialTextSetUp()V
    .locals 0

    .prologue
    .line 358
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpConnectionInfoAndStatus()V

    .line 359
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpUserGivenName()V

    .line 360
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpTimeFormat()V

    .line 361
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpUnitSystemFormat()V

    .line 362
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpLanguage()V

    .line 363
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpFacebook()V

    .line 364
    return-void
.end method

.method public static newInstance(Z)Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    .locals 3
    .param p0, "share"    # Z

    .prologue
    .line 83
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 84
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "share"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 86
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;-><init>()V

    .line 87
    .local v1, "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setArguments(Landroid/os/Bundle;)V

    .line 88
    return-object v1
.end method

.method private sendSyncSettingsCommandToWatch()V
    .locals 1

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 589
    return-void
.end method

.method private setUpConnectionInfoAndStatus()V
    .locals 6

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 368
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;->setVisibility(I)V

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAccountAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "accountAddress":Ljava/lang/String;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLoggedInStatus(Landroid/content/Context;)Z

    move-result v1

    .line 375
    .local v1, "isLoggedIn":Z
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 377
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f090201

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;->setTitle(Ljava/lang/String;)V

    .line 378
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    invoke-virtual {v3, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;->setSummary(Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f090202

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;->setTitle(Ljava/lang/String;)V

    .line 383
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f090203

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;->setSummary(Ljava/lang/String;)V

    .line 385
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 387
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private setUpFacebook()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 474
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookStatus:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

    invoke-virtual {v0, v1}, Lcom/facebook/login/widget/LoginButton;->setVisibility(I)V

    .line 480
    :goto_0
    return-void

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookStatus:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f090124

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setUpLanguage()V
    .locals 4

    .prologue
    .line 441
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090200

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 442
    const-string v1, "pref_watch_language_selected"

    const v2, 0x7f1000fb

    .line 443
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 442
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 445
    .local v0, "checkedId":I
    packed-switch v0, :pswitch_data_0

    .line 471
    :goto_0
    return-void

    .line 447
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09018c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 450
    :pswitch_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090191

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 453
    :pswitch_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09018a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 456
    :pswitch_3
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09018e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :pswitch_4
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09018d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :pswitch_5
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090192

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 465
    :pswitch_6
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09018f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 468
    :pswitch_7
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09018b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x7f1000fa
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 629
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 630
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 632
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    .line 633
    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 635
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 641
    :cond_0
    :goto_0
    return-void

    .line 638
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 639
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setUpTimeFormat()V
    .locals 4

    .prologue
    .line 405
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTimeFormatPreference(Landroid/content/Context;)I

    move-result v0

    .line 407
    .local v0, "value":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f09025a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 408
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 409
    packed-switch v0, :pswitch_data_0

    .line 420
    :goto_0
    return-void

    .line 411
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    const-string v2, "AM/PM"

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 414
    :pswitch_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    const-string v2, "24:00"

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 418
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setUpUnitSystemFormat()V
    .locals 4

    .prologue
    .line 423
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getUnitSystemFormatPreference(Landroid/content/Context;)I

    move-result v0

    .line 425
    .local v0, "value":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090279

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 426
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 427
    packed-switch v0, :pswitch_data_0

    .line 438
    :goto_0
    return-void

    .line 429
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090204

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :pswitch_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0901ff

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 436
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setUpUserGivenName()V
    .locals 4

    .prologue
    .line 394
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0901fe

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 395
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 398
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 402
    :goto_0
    return-void

    .line 400
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateTimeFormatValueField(I)V
    .locals 3
    .param p1, "timeFormat"    # I

    .prologue
    .line 553
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 554
    packed-switch p1, :pswitch_data_0

    .line 565
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 566
    return-void

    .line 556
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    const-string v1, "24:00"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 559
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    const-string v1, "AM/PM"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f090207

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 554
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateUnitSystemFormatValueField(I)V
    .locals 3
    .param p1, "unitSystem"    # I

    .prologue
    .line 569
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 570
    packed-switch p1, :pswitch_data_0

    .line 581
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 582
    return-void

    .line 572
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f090204

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 575
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0901ff

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f090207

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 570
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 686
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 687
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 689
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 645
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 646
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 647
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 649
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    .line 650
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 651
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 652
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 659
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 656
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 657
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 663
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 664
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpLinkLostState()V

    .line 665
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 667
    :cond_0
    return-void
.end method

.method public handleLoginStateChangedEvent(Lcom/vectorwatch/android/events/LoginStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LoginStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 621
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpConnectionInfoAndStatus()V

    .line 622
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;->invalidate()V

    .line 623
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 693
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 694
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mCallbackManager:Lcom/facebook/CallbackManager;

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/CallbackManager;->onActivityResult(IILandroid/content/Intent;)Z

    .line 695
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mContext:Landroid/content/Context;

    .line 95
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 96
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 106
    const v2, 0x7f0300a5

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 107
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f1001b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 108
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpLinkLostState()V

    .line 110
    const v2, 0x7f100284

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mConnectionInfo:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsInfoElementView;

    .line 111
    const v2, 0x7f100285

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 112
    const v2, 0x7f100286

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 113
    const v2, 0x7f100287

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 114
    const v2, 0x7f100289

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookStatus:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 115
    const v2, 0x7f10028a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/login/widget/LoginButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

    .line 116
    const v2, 0x7f100288

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 118
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/login/widget/LoginButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v0, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "publish_actions"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

    invoke-virtual {v2, v0}, Lcom/facebook/login/widget/LoginButton;->setPublishPermissions(Ljava/util/List;)V

    .line 132
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

    invoke-virtual {v2, p0}, Lcom/facebook/login/widget/LoginButton;->setFragment(Landroid/app/Fragment;)V

    .line 134
    invoke-static {}, Lcom/facebook/CallbackManager$Factory;->create()Lcom/facebook/CallbackManager;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mCallbackManager:Lcom/facebook/CallbackManager;

    .line 137
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mFacebookLoginButton:Lcom/facebook/login/widget/LoginButton;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mCallbackManager:Lcom/facebook/CallbackManager;

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/facebook/login/widget/LoginButton;->registerCallback(Lcom/facebook/CallbackManager;Lcom/facebook/FacebookCallback;)V

    .line 161
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->initialTextSetUp()V

    .line 163
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mChangeName:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mTimeFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mUnitSystemFormat:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$5;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->mLanguage:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$6;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 101
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 102
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 671
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 672
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->syncSettingsNeeded:Z

    if-eqz v0, :cond_0

    .line 673
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->sendSyncSettingsCommandToWatch()V

    .line 674
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->syncSettingsNeeded:Z

    .line 676
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 680
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 681
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->setUpLanguage()V

    .line 682
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 349
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 351
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "share"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    const v0, 0x7f090126

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xbb8

    .line 353
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 352
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 355
    :cond_0
    return-void
.end method
