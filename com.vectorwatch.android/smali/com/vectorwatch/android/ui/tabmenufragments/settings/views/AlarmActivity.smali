.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "AlarmActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;


# static fields
.field public static final ALARM_ID_NOT_SET:J = -0x1L

.field public static final EXTRA_ALARM_ID:Ljava/lang/String; = "extra_alarm_position"

.field public static final MAX_ALARMS_ALLOWED:I = 0x5

.field public static final TIME_OF_DAY_AM:I = -0x1

.field public static final TIME_OF_DAY_PM:I = 0x1

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

.field mAlarmList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100296
        }
    .end annotation
.end field

.field mAlert:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100297
        }
    .end annotation
.end field

.field mLinkStateImage:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001b5
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method private actionAdd()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;->createAlarmPressed()V

    .line 174
    return-void
.end method

.method private setUpLinkLostState(Z)V
    .locals 3
    .param p1, "isConnected"    # Z

    .prologue
    const/4 v2, 0x0

    .line 195
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    .line 199
    invoke-static {v0, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setup()V
    .locals 2

    .prologue
    .line 156
    const v0, 0x7f09014d

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->setTitle(I)V

    .line 158
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    .line 159
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;->setLinkLostStatus()V

    .line 161
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlarmList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlarmList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 163
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    .line 164
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlarmList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 166
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;->getAllAlarms()V

    .line 167
    return-void
.end method

.method private updateScreen(Z)V
    .locals 3
    .param p1, "alarmsAvailable"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 182
    if-eqz p1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlert:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlarmList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 189
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlert:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAlarmList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "alertMessage"    # Ljava/lang/String;
    .param p2, "timeout"    # I

    .prologue
    .line 104
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 105
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 211
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 212
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    .line 216
    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 223
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onAlarmClicked(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;->onAlarmClicked(J)V

    .line 130
    return-void
.end method

.method public onAlarmStatusChanged(JBZ)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "enabled"    # B
    .param p4, "isActive"    # Z

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;->onAlarmStatusChanged(JBZ)V

    .line 135
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 62
    const v0, 0x7f0300a7

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->setContentView(I)V

    .line 63
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 65
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->setup()V

    .line 67
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->updateLinkLostStatus(Z)V

    .line 68
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 73
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f110009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 74
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 80
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 81
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;->onStop()V

    .line 83
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 138
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 143
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->finish()V

    .line 147
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 140
    :pswitch_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->actionAdd()V

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x7f1002ee
        :pswitch_0
    .end packed-switch
.end method

.method public startCreateAlarmMode()V
    .locals 4

    .prologue
    .line 122
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_alarm_position"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 124
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->startActivity(Landroid/content/Intent;)V

    .line 125
    return-void
.end method

.method public startEditAlarmMode(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 97
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_alarm_position"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->startActivity(Landroid/content/Intent;)V

    .line 100
    return-void
.end method

.method public updateAlarm(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->triggerUpdateList()V

    .line 110
    return-void
.end method

.method public updateAlarmsList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 90
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-virtual {v2, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->updateList(Ljava/util/List;)V

    .line 91
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    if-ne v2, v0, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->updateScreen(Z)V

    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->triggerUpdateList()V

    .line 93
    return-void

    :cond_0
    move v2, v1

    .line 91
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public updateLinkLostStatus(Z)V
    .locals 1
    .param p1, "isWatchConnected"    # Z

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->setUpLinkLostState(Z)V

    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 118
    :cond_0
    return-void
.end method
