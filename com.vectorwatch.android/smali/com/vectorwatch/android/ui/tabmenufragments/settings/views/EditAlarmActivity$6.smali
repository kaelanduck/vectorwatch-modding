.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;
.super Ljava/lang/Object;
.source "EditAlarmActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->editAlarmRepeatPatternAllowed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

.field final synthetic val$selectedOptions:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;->val$selectedOptions:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I
    .param p3, "isChecked"    # Z

    .prologue
    .line 215
    if-eqz p3, :cond_1

    .line 217
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;->val$selectedOptions:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;->val$selectedOptions:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;->val$selectedOptions:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
