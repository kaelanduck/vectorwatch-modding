.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;
.super Ljava/lang/Object;
.source "AlarmsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract createAlarmPressed()V
.end method

.method public abstract getAllAlarms()V
.end method

.method public abstract onAlarmClicked(J)V
.end method

.method public abstract onAlarmStatusChanged(JBZ)V
.end method

.method public abstract onStop()V
.end method

.method public abstract setLinkLostStatus()V
.end method
