.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;
.super Landroid/widget/RelativeLayout;
.source "SystemNotificationEnablerView.java"


# static fields
.field public static final NOTIFICATION_RESULT_CODE:I = 0x4d2

.field public static TITLE:Ljava/lang/String;

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mSwitch:Landroid/widget/Switch;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    const v0, 0x7f0900be

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->TITLE:Ljava/lang/String;

    .line 42
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->init(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const v0, 0x7f0900be

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->TITLE:Ljava/lang/String;

    .line 48
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->init(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$200()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->onSharedPreferenceChanged(Ljava/lang/String;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    .line 54
    const-string v1, "layout_inflater"

    .line 55
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 56
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030079

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 58
    const v1, 0x7f100226

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mLayout:Landroid/widget/RelativeLayout;

    .line 59
    const v1, 0x7f100228

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mSwitch:Landroid/widget/Switch;

    .line 60
    const v1, 0x7f100227

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mTitle:Landroid/widget/TextView;

    .line 62
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mTitle:Landroid/widget/TextView;

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->TITLE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mSwitch:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSystemNotification(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 67
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mSwitch:Landroid/widget/Switch;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 90
    return-void
.end method

.method private onSharedPreferenceChanged(Ljava/lang/String;)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 96
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSharedPreferencesFile(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 98
    .local v3, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v4, "system_not"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 99
    const/4 v4, 0x0

    invoke-interface {v3, p1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 100
    .local v1, "notificationsEnabled":Ljava/lang/Boolean;
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/vectorwatch/android/service/nls/NLService;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .local v2, "service":Landroid/content/Intent;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 102
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 103
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const/16 v5, 0x4d2

    invoke-virtual {v4, v0, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 112
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with new value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 114
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 120
    .end local v1    # "notificationsEnabled":Ljava/lang/Boolean;
    .end local v2    # "service":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 117
    .restart local v1    # "notificationsEnabled":Ljava/lang/Boolean;
    .restart local v2    # "service":Landroid/content/Intent;
    :cond_2
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    return v0
.end method

.method public updateIsChecked()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mSwitch:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSystemNotification(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 128
    return-void
.end method
