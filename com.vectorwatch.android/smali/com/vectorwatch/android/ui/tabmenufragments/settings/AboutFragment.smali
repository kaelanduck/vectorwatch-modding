.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;
.super Landroid/app/Fragment;
.source "AboutFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mAddDummySteps:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100098
        }
    .end annotation
.end field

.field mConnectBle:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10009b
        }
    .end annotation
.end field

.field mCountWatchCrashes:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100092
        }
    .end annotation
.end field

.field mDisplayRealmActivityDb:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10009c
        }
    .end annotation
.end field

.field mEraseDb:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10009a
        }
    .end annotation
.end field

.field private mEtUri:Landroid/widget/EditText;

.field mReconnect:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100094
        }
    .end annotation
.end field

.field mRemoveTokenButton:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10009f
        }
    .end annotation
.end field

.field mReqLocalization:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100097
        }
    .end annotation
.end field

.field mReqSynRealmActivity:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10009d
        }
    .end annotation
.end field

.field mReqSyncToFit:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100099
        }
    .end annotation
.end field

.field mSendLocalization:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100096
        }
    .end annotation
.end field

.field mStopBleButton:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100093
        }
    .end annotation
.end field

.field mSwitchOfflineMode:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10009e
        }
    .end annotation
.end field

.field private rbDE:Landroid/widget/RadioButton;

.field private rbDev:Landroid/widget/RadioButton;

.field private rbEN:Landroid/widget/RadioButton;

.field private rbES:Landroid/widget/RadioButton;

.field private rbFR:Landroid/widget/RadioButton;

.field private rbProduction:Landroid/widget/RadioButton;

.field private rbRO:Landroid/widget/RadioButton;

.field private rbStage:Landroid/widget/RadioButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEtUri:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private static previousEntryInDatabase(ILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z
    .locals 9
    .param p0, "columnValue"    # I
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "table"    # Landroid/net/Uri;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 94
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 95
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-array v2, v7, [Ljava/lang/String;

    aput-object p1, v2, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 97
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 103
    :goto_0
    return v1

    .line 102
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 103
    goto :goto_0
.end method


# virtual methods
.method public displayRealmActivityDb()V
    .locals 8
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10009c
        }
    .end annotation

    .prologue
    .line 173
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v2

    .line 174
    .local v2, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v1

    .line 176
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 177
    .local v0, "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "REALM ACTIVITY: IN REALM RIGHT NOW = Start time stamp = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " steps count = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 178
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getVal()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "dist = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getDist()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cals = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getCal()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "DIRTY (cloud, steps, cals, dist) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 179
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->isDirtyCloud()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->isDirtySteps()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 180
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->isDirtyCalories()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->isDirtyDistance()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 177
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 183
    .end local v0    # "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_0
    invoke-virtual {v2}, Lio/realm/Realm;->close()V

    .line 184
    return-void
.end method

.method public eraseDb()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10009a
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ACTIVITY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 169
    return-void
.end method

.method public handleCrashLogsReceivedEvent(Lcom/vectorwatch/android/events/CrashLogsReceivedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/CrashLogsReceivedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 337
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mCountWatchCrashes:Landroid/widget/Button;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getCounterWatchCrashLogs(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mCountWatchCrashes:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->invalidate()V

    .line 339
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v2, 0x1

    .line 368
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 409
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 370
    :pswitch_1
    if-eqz p2, :cond_0

    .line 371
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEtUri:Landroid/widget/EditText;

    const-string v1, "https://endpoint.vector.watch"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 375
    :pswitch_2
    if-eqz p2, :cond_0

    .line 376
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEtUri:Landroid/widget/EditText;

    const-string v1, "https://api.vector.watch"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 380
    :pswitch_3
    if-eqz p2, :cond_0

    .line 381
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEtUri:Landroid/widget/EditText;

    const-string v1, "http://52.18.220.232:8080"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 385
    :pswitch_4
    if-eqz p2, :cond_0

    .line 386
    const-string v0, "en"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 387
    const-string v0, "en"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 391
    :pswitch_5
    if-eqz p2, :cond_0

    .line 392
    const-string v0, "de"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 393
    const-string v0, "de"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 397
    :pswitch_6
    if-eqz p2, :cond_0

    .line 398
    const-string v0, "es"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 399
    const-string v0, "es"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 403
    :pswitch_7
    if-eqz p2, :cond_0

    .line 404
    const-string v0, "fr"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 405
    const-string v0, "fr"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 368
    nop

    :pswitch_data_0
    .packed-switch 0x7f10018d
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 110
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    const v1, 0x7f03001a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 115
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 117
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mCountWatchCrashes:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getCounterWatchCrashLogs(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 119
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 131
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 132
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 125
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 126
    return-void
.end method

.method public removeTokenButton()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10009f
        }
    .end annotation

    .prologue
    .line 344
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$4;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    .line 363
    .local v0, "timer":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 364
    return-void
.end method

.method public reqLocalizationFile()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100097
        }
    .end annotation

    .prologue
    .line 203
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v2, 0x25

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->requestFile(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)V

    .line 205
    return-void
.end method

.method public reqSyncRealmActivityToCloud()V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10009d
        }
    .end annotation

    .prologue
    .line 189
    return-void
.end method

.method public reqSyncToFit()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100099
        }
    .end annotation

    .prologue
    .line 193
    const-string v2, "flag_sync_to_google_fit"

    const/4 v3, 0x0

    .line 194
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 195
    .local v1, "isSyncToGoogleFitEnabled":Z
    if-eqz v1, :cond_0

    .line 196
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/vectorwatch/android/service/FitnessClientService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 197
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 199
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public sendBleConnectCommand()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10009b
        }
    .end annotation

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->connect(Landroid/content/Context;)V

    .line 313
    return-void
.end method

.method public sendBleReconnectCommand()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100094
        }
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->reconnect(Landroid/content/Context;)V

    .line 323
    return-void
.end method

.method public sendBleStopCommand()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100093
        }
    .end annotation

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->stop(Landroid/content/Context;)V

    .line 318
    return-void
.end method

.method public sendLocalizationFile()V
    .locals 16
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100096
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v11

    .line 138
    .local v11, "assetManager":Landroid/content/res/AssetManager;
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Here"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 140
    :try_start_0
    new-instance v12, Ljava/io/BufferedInputStream;

    const-string v1, "localization/locale_en_UK.compressed"

    invoke-virtual {v11, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v12, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 141
    .local v12, "bis":Ljava/io/BufferedInputStream;
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 145
    .local v13, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {v12}, Ljava/io/BufferedInputStream;->read()I

    move-result v15

    .local v15, "theByte":I
    const/4 v1, -0x1

    if-eq v15, v1, :cond_0

    .line 146
    invoke-virtual {v13, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    .end local v12    # "bis":Ljava/io/BufferedInputStream;
    .end local v13    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v15    # "theByte":I
    :catch_0
    move-exception v14

    .line 160
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 161
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Error"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 163
    .end local v14    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 149
    .restart local v12    # "bis":Ljava/io/BufferedInputStream;
    .restart local v13    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v15    # "theByte":I
    :cond_0
    :try_start_1
    invoke-virtual {v12}, Ljava/io/BufferedInputStream;->close()V

    .line 150
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 152
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 154
    .local v3, "result":[B
    new-instance v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/4 v2, 0x0

    array-length v4, v3

    int-to-short v4, v4

    array-length v5, v3

    int-to-short v5, v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;-><init>(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I[BSSZZ)V

    .line 156
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v4

    const-string v5, "localization/locale_en_UK.compressed"

    const/4 v6, 0x0

    sget-object v7, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    array-length v1, v3

    int-to-short v8, v1

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-interface/range {v4 .. v10}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public switchBaseUrl()V
    .locals 7
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100092
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 209
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 211
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f100190

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEtUri:Landroid/widget/EditText;

    .line 212
    const v2, 0x7f10018d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbProduction:Landroid/widget/RadioButton;

    .line 213
    const v2, 0x7f10018e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbStage:Landroid/widget/RadioButton;

    .line 214
    const v2, 0x7f10018f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDev:Landroid/widget/RadioButton;

    .line 216
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEtUri:Landroid/widget/EditText;

    const-string v3, "https://endpoint.vector.watch"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbStage:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 218
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbProduction:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 219
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDev:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 221
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v2

    const-string v3, "https://endpoint.vector.watch"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbProduction:Landroid/widget/RadioButton;

    invoke-virtual {v2, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 223
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbStage:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 224
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDev:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 235
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c00c5

    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 236
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v2, "Change cloud uri"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 237
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901d8

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    .line 238
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09008a

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$1;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    .line 261
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 267
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 268
    return-void

    .line 225
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v2

    const-string v3, "http://52.18.220.232:8080"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 226
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbProduction:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 227
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbStage:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 228
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDev:Landroid/widget/RadioButton;

    invoke-virtual {v2, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbProduction:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 231
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbStage:Landroid/widget/RadioButton;

    invoke-virtual {v2, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 232
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDev:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method

.method public switchLanguage()V
    .locals 6
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100095
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 272
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003e

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 274
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f100191

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbEN:Landroid/widget/RadioButton;

    .line 275
    const v2, 0x7f100192

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDE:Landroid/widget/RadioButton;

    .line 276
    const v2, 0x7f100193

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbES:Landroid/widget/RadioButton;

    .line 277
    const v2, 0x7f100194

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbFR:Landroid/widget/RadioButton;

    .line 278
    const v2, 0x7f100195

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbRO:Landroid/widget/RadioButton;

    .line 280
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getLocaleString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "en"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 281
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbEN:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 292
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbEN:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 293
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDE:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 294
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbES:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 295
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbFR:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 296
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbRO:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 298
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c00c5

    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 299
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v2, "Change app language"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 300
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901d8

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    .line 301
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 307
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 308
    return-void

    .line 282
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getLocaleString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "de"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 283
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbDE:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getLocaleString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "es"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 285
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbES:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 286
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getLocaleString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fr"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 287
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbFR:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 289
    :cond_3
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->rbRO:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method public switchOfflineMode()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10009e
        }
    .end annotation

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setOfflineModeStatus(Landroid/content/Context;Z)V

    .line 329
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->switchToOfflineMode(Landroid/content/Context;)V

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setOfflineModeStatus(Landroid/content/Context;Z)V

    goto :goto_0
.end method
