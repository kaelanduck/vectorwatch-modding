.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;
.super Ljava/lang/Object;
.source "EditAlarmActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->editAlarmNameAllowed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

.field final synthetic val$mainValueInputField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 179
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "alarmName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mEditAlarmName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_0
    return-void
.end method
