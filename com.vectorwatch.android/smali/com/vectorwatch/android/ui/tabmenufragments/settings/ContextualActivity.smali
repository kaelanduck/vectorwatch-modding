.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "ContextualActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-direct {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;-><init>()V

    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 22
    const v0, 0x7f09015a

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualActivity;->setTitle(I)V

    .line 23
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 25
    sget-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_CONTEXTUAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {p0, v0, v1, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 28
    return-void
.end method
