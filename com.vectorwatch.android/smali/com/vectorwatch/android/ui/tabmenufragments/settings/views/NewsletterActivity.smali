.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "NewsletterActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mAppSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10010a
        }
    .end annotation
.end field

.field mNewsSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10010b
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$View;

.field mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000f8
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$View;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$View;

    return-object v0
.end method

.method private setupAppSwitch()V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mAppSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const v1, 0x7f0901b8

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pref_news_app"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mAppSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 61
    return-void
.end method

.method private setupNewsSwitch()V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mNewsSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const v1, 0x7f0901b9

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pref_news_news"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mNewsSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$2;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "alertDuration"    # I

    .prologue
    .line 137
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 138
    return-void
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 127
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->setContentView(I)V

    .line 41
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 43
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$View;

    .line 45
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->setupAppSwitch()V

    .line 46
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->setupNewsSwitch()V

    .line 47
    return-void
.end method

.method public resetAppSwitch()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 82
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mAppSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 83
    const-string v1, "pref_news_app"

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 85
    .local v0, "isEnable":Z
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mAppSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setSwitch(Z)V

    .line 86
    const-string v1, "pref_news_app"

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 87
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mAppSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$3;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 93
    return-void

    :cond_0
    move v1, v3

    .line 85
    goto :goto_0

    :cond_1
    move v2, v3

    .line 86
    goto :goto_1
.end method

.method public resetNewsSwitch()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 100
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mNewsSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 101
    const-string v1, "pref_news_news"

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 103
    .local v0, "isEnable":Z
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mNewsSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setSwitch(Z)V

    .line 104
    const-string v1, "pref_news_news"

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 105
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mNewsSwitch:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$4;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 111
    return-void

    :cond_0
    move v1, v3

    .line 103
    goto :goto_0

    :cond_1
    move v2, v3

    .line 104
    goto :goto_1
.end method

.method public showProgress()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 119
    return-void
.end method
