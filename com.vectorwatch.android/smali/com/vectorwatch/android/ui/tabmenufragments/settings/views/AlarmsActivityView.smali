.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;
.super Ljava/lang/Object;
.source "AlarmsActivityView.java"


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;I)V
.end method

.method public abstract startCreateAlarmMode()V
.end method

.method public abstract startEditAlarmMode(J)V
.end method

.method public abstract updateAlarm(J)V
.end method

.method public abstract updateAlarmsList(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateLinkLostStatus(Z)V
.end method
