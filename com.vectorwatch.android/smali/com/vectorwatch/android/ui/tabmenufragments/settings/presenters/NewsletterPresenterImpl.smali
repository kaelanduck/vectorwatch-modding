.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;
.super Ljava/lang/Object;
.source "NewsletterPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;

.field private mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mContext:Landroid/content/Context;

    .line 23
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;

    .line 24
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    .line 25
    return-void
.end method


# virtual methods
.method public onCheckNewsletterAppInfoChanged(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;->changeNewsletterOptionsApp(Z)V

    .line 60
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->showProgress()V

    .line 61
    return-void
.end method

.method public onCheckNewsletterNewsChanged(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;->changeNewsletterOptionsNews(Z)V

    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->showProgress()V

    .line 72
    return-void
.end method

.method public onUpdateNewsletterFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 42
    const-string v0, "MOBILE_APP_NEWSLETTER_LIST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->resetAppSwitch()V

    .line 48
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->hideProgress()V

    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f090221

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->displayAlert(Ljava/lang/String;I)V

    .line 50
    return-void

    .line 44
    :cond_1
    const-string v0, "MOBILE_APP_MARKETING_NEWSLETTER_LIST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->resetNewsSwitch()V

    goto :goto_0
.end method

.method public onUpdateNewsletterSuccess()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenterImpl;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterView;->hideProgress()V

    .line 33
    return-void
.end method
