.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "AboutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 14
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-direct {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;-><init>()V

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 21
    const v0, 0x7f0901fc

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutActivity;->setTitle(I)V

    .line 22
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 23
    return-void
.end method
