.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;
.super Landroid/app/Fragment;
.source "ActivityInfoFragment.java"


# static fields
.field public static final GENDER_FEMALE:I = 0x0

.field public static final GENDER_MALE:I = 0x1

.field private static final MAX_HEIGHT:I = 0xfe

.field private static final MAX_WEIGHT:I = 0xfe

.field private static final MIN_HEIGHT:I = 0x3c

.field private static final MIN_WEIGHT:I = 0xf

.field public static final UNSET_VALUE:I = -0x1

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mActivityReminder:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100293
        }
    .end annotation
.end field

.field private mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field mAlertsSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100290
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field mGoalAchievementAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100291
        }
    .end annotation
.end field

.field mGoalAlmostAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100292
        }
    .end annotation
.end field

.field private mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private mLinkStateImage:Landroid/widget/TextView;

.field mNewsletterSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100294
        }
    .end annotation
.end field

.field private mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mTempChoiceActivityLevel:I

.field private mTempChoiceGender:I

.field mWeeklyNewsletter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100295
        }
    .end annotation
.end field

.field private mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

.field private syncSettingsNeeded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 54
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mTempChoiceGender:I

    return v0
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mTempChoiceGender:I

    return p1
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getDefaultChoiceForGender()I

    move-result v0

    return v0
.end method

.method static synthetic access$200()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->handleGenderSelected(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->handleMetricHeightInputReceived(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;II)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->handleImperialHeightInputReceived(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->handleWeightInputReceived(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->goToMain()V

    return-void
.end method

.method private getDefaultChoiceForGender()I
    .locals 3

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v1

    .line 544
    .local v1, "savedPreferenceGender":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 545
    .local v0, "defaultChoiceGender":I
    :goto_0
    return v0

    .end local v0    # "defaultChoiceGender":I
    :cond_0
    move v0, v1

    .line 544
    goto :goto_0
.end method

.method private goToMain()V
    .locals 3

    .prologue
    .line 529
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 530
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 531
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 532
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 533
    return-void
.end method

.method private handleAgeInputReceived(J)V
    .locals 5
    .param p1, "dateOfBirthInMillis"    # J

    .prologue
    const/4 v4, 0x1

    .line 730
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v0

    .line 731
    .local v0, "oldDateOfBirth":J
    cmp-long v2, v0, p1

    if-nez v2, :cond_0

    .line 746
    :goto_0
    return-void

    .line 736
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, p2, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoDateOfBirth(JLandroid/content/Context;)V

    .line 739
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateAgeValueField(J)V

    .line 741
    iput-boolean v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    .line 743
    const-string v2, "flag_changed_activity_info"

    .line 744
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 743
    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 745
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleGenderSelected(I)V
    .locals 4
    .param p1, "gender"    # I

    .prologue
    const/4 v3, 0x1

    .line 708
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v0

    .line 709
    .local v0, "oldValueGender":I
    if-ne v0, p1, :cond_0

    .line 724
    :goto_0
    return-void

    .line 714
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoGender(ILandroid/content/Context;)V

    .line 717
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateGenderValueField(I)V

    .line 719
    iput-boolean v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    .line 721
    const-string v1, "flag_changed_activity_info"

    .line 722
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 721
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 723
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleImperialHeightInputReceived(II)V
    .locals 5
    .param p1, "feet"    # I
    .param p2, "inches"    # I

    .prologue
    const/4 v4, 0x1

    .line 785
    invoke-static {p1, p2}, Lcom/vectorwatch/android/utils/Helpers;->convertHeightFromImperialToMetric(II)I

    move-result v0

    .line 787
    .local v0, "heightInCm":I
    const/16 v2, 0x3c

    if-le v0, v2, :cond_0

    const/16 v2, 0xfe

    if-lt v0, v2, :cond_1

    .line 788
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090104

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 808
    :goto_0
    return-void

    .line 792
    :cond_1
    const-string v2, "activity_info_height_feet"

    .line 793
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 792
    invoke-static {v2, p1, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 794
    const-string v2, "activity_info_height_inches"

    .line 795
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 794
    invoke-static {v2, p2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 797
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoHeight(ILandroid/content/Context;)V

    .line 800
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateHeightValueField(II)V

    .line 802
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v1

    .line 803
    .local v1, "oldValueHeight":I
    if-eq v1, v0, :cond_2

    .line 805
    iput-boolean v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    .line 807
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleMetricHeightInputReceived(I)V
    .locals 7
    .param p1, "height"    # I

    .prologue
    const/4 v6, 0x1

    .line 752
    const/16 v4, 0x3c

    if-le p1, v4, :cond_0

    const/16 v4, 0xfe

    if-lt p1, v4, :cond_2

    .line 753
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f090105

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 782
    :cond_1
    :goto_0
    return-void

    .line 758
    :cond_2
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->convertHeightFromMetricToImperial(I)F

    move-result v0

    .line 759
    .local v0, "feet":F
    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->getIntNumberOfFeet(F)I

    move-result v1

    .line 760
    .local v1, "feetRounded":I
    int-to-float v4, v1

    sub-float v4, v0, v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->getNoInchesFromNoFeet(F)I

    move-result v2

    .line 761
    .local v2, "inches":I
    const-string v4, "activity_info_height_feet"

    .line 762
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 761
    invoke-static {v4, v1, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 763
    const-string v4, "activity_info_height_inches"

    .line 764
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 763
    invoke-static {v4, v2, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 766
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v3

    .line 767
    .local v3, "oldValueHeight":I
    if-eq v3, p1, :cond_1

    .line 772
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoHeight(ILandroid/content/Context;)V

    .line 775
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateHeightValueField(I)V

    .line 777
    iput-boolean v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    .line 779
    const-string v4, "flag_changed_activity_info"

    .line 780
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 779
    invoke-static {v4, v6, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 781
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleWeightInputReceived(I)V
    .locals 7
    .param p1, "weightInSetFormat"    # I

    .prologue
    const/4 v6, 0x1

    .line 816
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    .line 817
    .local v0, "isMetric":Z
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v1

    .line 820
    .local v1, "oldValueWeight":I
    if-eqz v0, :cond_1

    move v2, p1

    .line 821
    .local v2, "weightInKg":I
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->convertWeightFromMetricToImperial(I)I

    move-result v3

    .line 823
    .local v3, "weightInLbs":I
    :goto_1
    const/16 v4, 0xf

    if-lt v2, v4, :cond_0

    const/16 v4, 0xfe

    if-le v2, v4, :cond_4

    .line 824
    :cond_0
    if-eqz v0, :cond_3

    .line 825
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f090108

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 846
    :goto_2
    return-void

    .line 820
    .end local v2    # "weightInKg":I
    .end local v3    # "weightInLbs":I
    :cond_1
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->convertWeightFromImperialToMetric(I)I

    move-result v2

    goto :goto_0

    .restart local v2    # "weightInKg":I
    :cond_2
    move v3, p1

    .line 821
    goto :goto_1

    .line 827
    .restart local v3    # "weightInLbs":I
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f090109

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 832
    :cond_4
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoWeight(ILandroid/content/Context;)V

    .line 833
    const-string v4, "activity_info_weight_lbs"

    .line 834
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 833
    invoke-static {v4, v3, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 837
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateWeightValueField(I)V

    .line 839
    if-eq v1, v2, :cond_5

    .line 841
    iput-boolean v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    .line 842
    const-string v4, "flag_changed_activity_info"

    .line 843
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 842
    invoke-static {v4, v6, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 845
    :cond_5
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_2
.end method

.method private labelsSetUp()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 552
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f09012e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 553
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090074

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 554
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090137

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 555
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090292

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setLabel(Ljava/lang/String;)V

    .line 558
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09026b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 559
    const-string v1, "flag_sync_to_google_fit"

    const/4 v2, 0x0

    .line 560
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 559
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 561
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 562
    if-eqz v0, :cond_1

    .line 563
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090240

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 568
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mNewsletterSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v2, 0x7f090164

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 569
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAlertsSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v2, 0x7f090166

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 571
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAchievementAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090131

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pref_goal_achievement"

    invoke-virtual {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 573
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAlmostAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090132

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pref_goal_almost"

    invoke-virtual {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 575
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mActivityReminder:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090052

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pref_activity_reminder"

    invoke-virtual {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 577
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeeklyNewsletter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090291

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pref_activity_newsletter"

    invoke-virtual {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 580
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mNewsletterSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setVisibility(I)V

    .line 582
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeeklyNewsletter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setVisibility(I)V

    .line 584
    :cond_0
    return-void

    .line 565
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09023f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private listenerSetUp()V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAchievementAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 475
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAlmostAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$7;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$7;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 493
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mActivityReminder:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$8;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$8;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 511
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeeklyNewsletter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$9;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$9;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 526
    return-void
.end method

.method private sendSyncSettingsCommandToWatch()V
    .locals 1

    .prologue
    .line 852
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 853
    return-void
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 864
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 865
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 867
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    .line 868
    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 870
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 876
    :cond_0
    :goto_0
    return-void

    .line 873
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 874
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateAgeValueField(J)V
    .locals 5
    .param p1, "value"    # J

    .prologue
    .line 649
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 650
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 651
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 653
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "MMM dd, yyyy"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 657
    .end local v0    # "calendar":Ljava/util/Calendar;
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 658
    return-void

    .line 655
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateGenderValueField(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 628
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 629
    packed-switch p1, :pswitch_data_0

    .line 640
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 641
    return-void

    .line 631
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f09012b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 634
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0901a3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f090207

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 629
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateHeightValueField(I)V
    .locals 7
    .param p1, "value"    # I

    .prologue
    .line 666
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 667
    int-to-float v1, p1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float v0, v1, v2

    .line 668
    .local v0, "heightInMeters":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.2f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " M"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 672
    .end local v0    # "heightInMeters":F
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 673
    return-void

    .line 670
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateHeightValueField(II)V
    .locals 3
    .param p1, "feet"    # I
    .param p2, "inches"    # I

    .prologue
    const/4 v0, -0x1

    .line 676
    if-eq p1, v0, :cond_0

    if-eq p2, v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 681
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 682
    return-void

    .line 679
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f090207

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateWeightValueField(I)V
    .locals 5
    .param p1, "valueInSetFormat"    # I

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v0

    .line 692
    .local v0, "isMetric":Z
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    .line 693
    if-eqz v0, :cond_0

    .line 694
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f090145

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    .line 701
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->invalidate()V

    .line 702
    return-void

    .line 696
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f090194

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 699
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f090207

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private valuesSetUp()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 593
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v4

    .line 594
    .local v4, "value":I
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateGenderValueField(I)V

    .line 597
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v0

    .line 598
    .local v0, "dob":J
    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateAgeValueField(J)V

    .line 601
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 602
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v4

    .line 603
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateHeightValueField(I)V

    .line 613
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 614
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v4

    .line 619
    :goto_1
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateWeightValueField(I)V

    .line 620
    return-void

    .line 605
    :cond_0
    const-string v5, "activity_info_height_feet"

    .line 606
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 605
    invoke-static {v5, v7, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v2

    .line 607
    .local v2, "feet":I
    const-string v5, "activity_info_height_inches"

    .line 608
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 607
    invoke-static {v5, v7, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v3

    .line 609
    .local v3, "inches":I
    invoke-direct {p0, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->updateHeightValueField(II)V

    goto :goto_0

    .line 616
    .end local v2    # "feet":I
    .end local v3    # "inches":I
    :cond_1
    const-string v5, "activity_info_weight_lbs"

    .line 617
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 616
    invoke-static {v5, v7, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v4

    goto :goto_1
.end method


# virtual methods
.method public handleAgeChangedEvent(Lcom/vectorwatch/android/events/AgeChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/AgeChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 857
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AgeChangedEvent;->getDateOfBirthInMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->handleAgeInputReceived(J)V

    .line 858
    return-void
.end method

.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 922
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 923
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 925
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 880
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 881
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 882
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 884
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    .line 885
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 886
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 887
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 890
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 891
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 892
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 898
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 899
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->setUpLinkLostState()V

    .line 900
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 902
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mContext:Landroid/content/Context;

    .line 96
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    const v1, 0x7f0300a6

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 103
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 105
    const v1, 0x7f1001b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 106
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->setUpLinkLostState()V

    .line 108
    const v1, 0x7f10028b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 109
    const v1, 0x7f10028c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 110
    const v1, 0x7f10028d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 111
    const v1, 0x7f10028e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    .line 112
    const v1, 0x7f10028f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 115
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->labelsSetUp()V

    .line 116
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->valuesSetUp()V

    .line 117
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->listenerSetUp()V

    .line 119
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGender:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mHeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeight:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 421
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mSyncToGoogleFit:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$5;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 450
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 915
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 916
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 917
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 918
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 906
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 907
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    if-eqz v0, :cond_0

    .line 908
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->sendSyncSettingsCommandToWatch()V

    .line 909
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->syncSettingsNeeded:Z

    .line 911
    :cond_0
    return-void
.end method
