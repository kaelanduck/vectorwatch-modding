.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;
.super Ljava/lang/Object;
.source "EditAlarmActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->displayDeleteAlarmPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J
    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->deleteAlarm(J)V

    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->finish()V

    .line 122
    return-void
.end method
