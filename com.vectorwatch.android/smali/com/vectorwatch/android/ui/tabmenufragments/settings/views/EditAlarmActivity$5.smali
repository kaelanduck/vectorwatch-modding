.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;
.super Ljava/lang/Object;
.source "EditAlarmActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->editAlarmRepeatPatternAllowed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

.field final synthetic val$selectedOptions:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;->val$selectedOptions:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;->val$selectedOptions:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->repeatPatternSelected(Ljava/util/List;)V

    .line 228
    return-void
.end method
