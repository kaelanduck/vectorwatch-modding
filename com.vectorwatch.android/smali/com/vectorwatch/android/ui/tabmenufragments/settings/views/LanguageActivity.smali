.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "LanguageActivity.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field protected mDefault:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000fa
        }
    .end annotation
.end field

.field protected mDutch:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100101
        }
    .end annotation
.end field

.field protected mEnglish:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000fb
        }
    .end annotation
.end field

.field protected mFrench:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000fe
        }
    .end annotation
.end field

.field protected mGerman:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000fd
        }
    .end annotation
.end field

.field protected mRadioGroup:Landroid/widget/RadioGroup;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000f9
        }
    .end annotation
.end field

.field protected mRomanian:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000fc
        }
    .end annotation
.end field

.field protected mSpanish:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000ff
        }
    .end annotation
.end field

.field protected mTurkish:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100100
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->handleOptionSelected(I)V

    return-void
.end method

.method private handleOptionSelected(I)V
    .locals 5
    .param p1, "checkedId"    # I

    .prologue
    const/4 v4, 0x1

    .line 115
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xbb8

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 194
    :goto_0
    return-void

    .line 121
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 180
    :pswitch_0
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle default"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 183
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f1000fa

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 186
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v0, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 187
    .local v0, "current":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "currentLanguage":Ljava/lang/String;
    invoke-static {v1, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 190
    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 191
    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 123
    .end local v0    # "current":Ljava/util/Locale;
    .end local v1    # "currentLanguage":Ljava/lang/String;
    :pswitch_1
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle en"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 124
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f1000fb

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 126
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 127
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 128
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 131
    :pswitch_2
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle ro"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 132
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f1000fc

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 134
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 135
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 136
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 139
    :pswitch_3
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle de"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 140
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f1000fd

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 142
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 143
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 144
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 147
    :pswitch_4
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle fr"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 148
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f1000fe

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 150
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 151
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 152
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 155
    :pswitch_5
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle es"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 156
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f1000ff

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 158
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 159
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 160
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 163
    :pswitch_6
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle tr"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 164
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f100100

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 166
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 167
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 168
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 171
    :pswitch_7
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "Language: handle nl"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 172
    const-string v2, "pref_watch_language_selected"

    const v3, 0x7f100101

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 175
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v4}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 176
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V

    .line 177
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->DUTCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x7f1000fa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private setSelectedOption()V
    .locals 4

    .prologue
    .line 102
    const-string v1, "pref_watch_language_selected"

    const v2, 0x7f1000fb

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 105
    .local v0, "checkedId":I
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Language: set selected option to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 107
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 63
    const v1, 0x7f090190

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->setTitle(I)V

    .line 65
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 66
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 67
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 70
    :cond_0
    const v1, 0x7f030025

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->setContentView(I)V

    .line 71
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 73
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mDutch:Landroid/widget/RadioButton;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09018f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090193

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->setSelectedOption()V

    .line 79
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 85
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 90
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 91
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 97
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->setSelectedOption()V

    .line 98
    return-void
.end method
