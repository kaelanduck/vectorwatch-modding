.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "AlarmsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;,
        Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAlarms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->mListener:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->mAlarms:Ljava/util/List;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->mListener:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->mAlarms:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 55
    int-to-long v0, p1

    return-wide v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->onBindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->mAlarms:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->bind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;)V

    .line 51
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030069

    const/4 v3, 0x0

    .line 43
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public triggerUpdateList()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->notifyDataSetChanged()V

    .line 141
    return-void
.end method

.method public updateList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->log:Lorg/slf4j/Logger;

    const-string v1, "ALARMS - updated alarms list."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->mAlarms:Ljava/util/List;

    .line 68
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->notifyDataSetChanged()V

    .line 69
    return-void
.end method
