.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "EditAlarmActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAlarmId:J

.field private mAlarmIsEnabled:B

.field mDelete:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002c4
        }
    .end annotation
.end field

.field mEditAlarmName:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002c1
        }
    .end annotation
.end field

.field mLinkState:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001b5
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

.field mRepeatAlarm:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002c3
        }
    .end annotation
.end field

.field mTimePicker:Landroid/widget/TimePicker;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002bf
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    return-object v0
.end method

.method private saveAlarm(Ljava/lang/String;IIIBJ)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "timeOfDay"    # I
    .param p5, "enabled"    # B
    .param p6, "id"    # J

    .prologue
    .line 329
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide v6, p6

    invoke-interface/range {v0 .. v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->saveAlarm(Ljava/lang/String;IIIBJ)V

    .line 330
    return-void
.end method

.method private setAlarmData(Ljava/lang/String;IIBJLjava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "enabledStatus"    # B
    .param p5, "id"    # J
    .param p7, "repeatPatternText"    # Ljava/lang/String;

    .prologue
    .line 302
    if-eqz p1, :cond_0

    .line 304
    :goto_0
    iput-wide p5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J

    .line 305
    iput-byte p4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    .line 308
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 309
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 312
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mEditAlarmName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mRepeatAlarm:Landroid/widget/TextView;

    invoke-virtual {v0, p7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    return-void

    .line 302
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09009d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private setUpLinkLostState(Z)V
    .locals 2
    .param p1, "isWatchConnected"    # Z

    .prologue
    .line 336
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setup(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 257
    const v3, 0x7f090174

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setTitle(I)V

    .line 259
    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;

    invoke-direct {v3, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;)V

    iput-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    .line 260
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->setLinkLostStatus()V

    .line 262
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->is24hFormat(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 264
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 265
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 266
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v3, "extra_alarm_position"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 268
    .local v0, "alarmId":J
    sget-object v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ALARMS - Setup edit alarm started with id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 269
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 270
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    .line 271
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setupNewAlarm()V

    .line 277
    .end local v0    # "alarmId":J
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 273
    .restart local v0    # "alarmId":J
    .restart local v2    # "bundle":Landroid/os/Bundle;
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setupExistingAlarm(J)V

    goto :goto_0
.end method

.method private setupExistingAlarm(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 292
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mDelete:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->getAlarmWithId(J)V

    .line 295
    return-void
.end method

.method private setupNewAlarm()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mDelete:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J

    .line 287
    invoke-static {v2, v2}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    iput-byte v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    .line 288
    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "alertMessage"    # Ljava/lang/String;
    .param p2, "timeout"    # I

    .prologue
    .line 134
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 135
    return-void
.end method

.method public displayDeleteAlarmPopup()V
    .locals 4

    .prologue
    .line 114
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c00c5

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 116
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f090057

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0901d8

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    .line 117
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09008a

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    .line 124
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 130
    return-void
.end method

.method public editAlarmNameAllowed()V
    .locals 5

    .prologue
    .line 171
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 172
    .local v1, "mainValueInputField":Landroid/widget/EditText;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 174
    new-instance v2, Landroid/app/AlertDialog$Builder;

    const v3, 0x7f0c00c5

    invoke-direct {v2, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f09014b

    .line 175
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 176
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0900af

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;

    invoke-direct {v4, p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Landroid/widget/EditText;)V

    .line 177
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09008a

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    .line 187
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 191
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 193
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 195
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 196
    return-void
.end method

.method public editAlarmRepeatPatternAllowed()V
    .locals 6

    .prologue
    .line 201
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    iget-byte v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    invoke-interface {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->populateRepeatSelection(B)Ljava/util/ArrayList;

    move-result-object v2

    .line 202
    .local v2, "selectedOptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const v3, 0x7f0c00c5

    invoke-direct {v0, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 205
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    iget-byte v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    invoke-interface {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->populateCheckedRepeatItems(B)[Z

    move-result-object v1

    .line 207
    .local v1, "checkedItems":[Z
    const v3, 0x7f090172

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0004

    new-instance v5, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;

    invoke-direct {v5, p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Ljava/util/List;)V

    .line 210
    invoke-virtual {v3, v4, v1, v5}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems(I[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0901d8

    new-instance v5, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;

    invoke-direct {v5, p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Ljava/util/List;)V

    .line 224
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 231
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 232
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 61
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 63
    const v1, 0x7f0300ad

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setContentView(I)V

    .line 64
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 66
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setup(Landroid/content/Intent;)V

    .line 69
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 74
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f11000a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 75
    const/4 v1, 0x1

    return v1
.end method

.method public onDeleteAlarm()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1002c4
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->onDeleteAlarmOptionSelected()V

    .line 249
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 94
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->onStop()V

    .line 96
    return-void
.end method

.method public onEditNameClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1002c1
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->onEditNameClicked()V

    .line 239
    return-void
.end method

.method public onEditRepeatPatternClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1002c3
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->onEditRepeatPatternClicked()V

    .line 244
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 79
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 84
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->finish()V

    .line 88
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 81
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;->saveAlarmButtonPressed()V

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x7f1002ee
        :pswitch_0
    .end packed-switch
.end method

.method public populateAlarm(Ljava/lang/String;IIBJLjava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "enabledStatus"    # B
    .param p5, "id"    # J
    .param p7, "repeatPatternText"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-direct/range {p0 .. p7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setAlarmData(Ljava/lang/String;IIBJLjava/lang/String;)V

    .line 103
    return-void
.end method

.method public saveAlarmAllowed()V
    .locals 12

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mEditAlarmName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 153
    .local v2, "hour":I
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 154
    .local v3, "min":I
    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getTimeOfDay(I)I

    move-result v4

    .line 157
    .local v4, "timeOfDay":I
    iget-byte v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    .line 160
    .local v5, "enabled":B
    const-wide/16 v6, -0x1

    .line 161
    .local v6, "id":J
    iget-wide v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-ltz v0, :cond_0

    .line 162
    iget-wide v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmId:J

    :cond_0
    move-object v0, p0

    .line 165
    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->saveAlarm(Ljava/lang/String;IIIBJ)V

    .line 166
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->finish()V

    .line 167
    return-void

    .line 149
    .end local v2    # "hour":I
    .end local v3    # "min":I
    .end local v4    # "timeOfDay":I
    .end local v5    # "enabled":B
    .end local v6    # "id":J
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v8, 0x7f09009d

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setRepeatPattern(BLjava/lang/String;)V
    .locals 1
    .param p1, "repeatPattern"    # B
    .param p2, "repeatPatternText"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-byte p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mAlarmIsEnabled:B

    .line 108
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mRepeatAlarm:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method

.method public updateLinkLostStatus(Z)V
    .locals 1
    .param p1, "isWatchConnected"    # Z

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->setUpLinkLostState(Z)V

    .line 141
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 143
    :cond_0
    return-void
.end method
