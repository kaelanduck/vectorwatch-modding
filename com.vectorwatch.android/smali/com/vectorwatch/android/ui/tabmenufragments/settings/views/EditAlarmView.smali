.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;
.super Ljava/lang/Object;
.source "EditAlarmView.java"


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;I)V
.end method

.method public abstract displayDeleteAlarmPopup()V
.end method

.method public abstract editAlarmNameAllowed()V
.end method

.method public abstract editAlarmRepeatPatternAllowed()V
.end method

.method public abstract populateAlarm(Ljava/lang/String;IIBJLjava/lang/String;)V
.end method

.method public abstract saveAlarmAllowed()V
.end method

.method public abstract setRepeatPattern(BLjava/lang/String;)V
.end method

.method public abstract updateLinkLostStatus(Z)V
.end method
