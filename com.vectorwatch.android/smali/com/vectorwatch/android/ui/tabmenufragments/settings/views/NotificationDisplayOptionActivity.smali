.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "NotificationDisplayOptionActivity.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mOptionAlert:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002bd
        }
    .end annotation
.end field

.field mOptionContent:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002be
        }
    .end annotation
.end field

.field mOptionOff:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1002bc
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method private setRadioButtons(I)V
    .locals 2
    .param p1, "option"    # I

    .prologue
    const/4 v1, 0x1

    .line 190
    packed-switch p1, :pswitch_data_0

    .line 201
    :goto_0
    return-void

    .line 192
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionContent:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 195
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionAlert:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 198
    :pswitch_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionOff:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setSelectedOption()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 131
    const/4 v1, 0x0

    .line 133
    .local v1, "doWatchSync":Z
    const-string v4, "pref_notification_display"

    invoke-static {v4, v8, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v3

    .line 137
    .local v3, "previousSetOption":I
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v2

    .line 138
    .local v2, "isNotificationAccessAllowed":Z
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOTIFICATIONS DISPLAY: Resume with permissionEnabled = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 141
    if-nez v2, :cond_2

    .line 142
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "NOTIFICATIONS DISPLAY: current option = 2"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 144
    invoke-direct {p0, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setRadioButtons(I)V

    .line 147
    if-eqz v3, :cond_0

    .line 148
    const/4 v1, 0x1

    .line 149
    const-string v4, "pref_notification_display"

    invoke-static {v4, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 162
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 163
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    .line 165
    :cond_1
    return-void

    .line 154
    :cond_2
    const-string v4, "pref_notification_display"

    invoke-static {v4, v8, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 157
    .local v0, "currentOption":I
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setRadioButtons(I)V

    .line 159
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOTIFICATIONS DISPLAY: currentOption = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private treatActivateAndroidNotifications()Z
    .locals 3

    .prologue
    .line 174
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 175
    .local v0, "isNotificationAccessAllowed":Z
    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 178
    const/4 v1, 0x1

    .line 181
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public handleLanguageChangedEvent(Lcom/vectorwatch/android/events/LanguageChangedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageChangedEvent;

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setSelectedOption()V

    .line 206
    return-void
.end method

.method public onAlertButtonClicked()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1002bd
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->treatActivateAndroidNotifications()Z

    move-result v0

    .line 100
    .local v0, "showedActivateAndroidNotifications":Z
    if-nez v0, :cond_0

    .line 104
    const-string v1, "pref_notification_display"

    const/4 v2, 0x2

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 107
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onContentButtonClicked()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1002be
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->treatActivateAndroidNotifications()Z

    move-result v0

    .line 119
    .local v0, "showedActivateAndroidNotifications":Z
    if-nez v0, :cond_0

    .line 123
    const-string v1, "pref_notification_display"

    const/4 v2, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 126
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 44
    const v1, 0x7f090165

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setTitle(I)V

    .line 46
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 47
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 48
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 51
    :cond_0
    const v1, 0x7f0300ac

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setContentView(I)V

    .line 52
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 55
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setSelectedOption()V

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 61
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public onOffButtonClicked()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1002bc
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->treatActivateAndroidNotifications()Z

    move-result v0

    .line 81
    .local v0, "showedActivateAndroidNotifications":Z
    if-nez v0, :cond_0

    .line 85
    const-string v1, "pref_notification_display"

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 88
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 68
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->setSelectedOption()V

    .line 69
    return-void
.end method
