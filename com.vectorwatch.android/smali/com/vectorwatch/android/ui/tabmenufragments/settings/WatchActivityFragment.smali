.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;
.super Landroid/app/Fragment;
.source "WatchActivityFragment.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# static fields
.field private static BACKLIGH_TIMEOUT_MAX:I

.field private static BACKLIGH_TIMEOUT_MIN:I

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mBatterySection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001de
        }
    .end annotation
.end field

.field private mCurrentSelectedIntensity:I

.field mDurationBacklightText:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001da
        }
    .end annotation
.end field

.field mLinkLostIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001dc
        }
    .end annotation
.end field

.field mLinkLostNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001dd
        }
    .end annotation
.end field

.field mLinkSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001db
        }
    .end annotation
.end field

.field mLinkStateImage:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001b5
        }
    .end annotation
.end field

.field mLowBatteryIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001df
        }
    .end annotation
.end field

.field mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001e0
        }
    .end annotation
.end field

.field mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d8
        }
    .end annotation
.end field

.field mSeekBar:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d9
        }
    .end annotation
.end field

.field private values:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->log:Lorg/slf4j/Logger;

    .line 44
    const/4 v0, 0x4

    sput v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->BACKLIGH_TIMEOUT_MAX:I

    .line 45
    const/4 v0, 0x0

    sput v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->BACKLIGH_TIMEOUT_MIN:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mCurrentSelectedIntensity:I

    .line 71
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .prologue
    .line 42
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mCurrentSelectedIntensity:I

    return v0
.end method

.method static synthetic access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mCurrentSelectedIntensity:I

    return p1
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->saveBacklightIntensityPreference(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->saveDisplaySecondHand(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->values:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->saveBacklightDurationPreference(I)V

    return-void
.end method

.method private backlightSelectedIntensitySetUp()V
    .locals 4

    .prologue
    .line 329
    const-string v1, "pref_backlight_intensity"

    const/4 v2, 0x1

    .line 330
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 329
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 332
    .local v0, "intensity":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 333
    return-void
.end method

.method private backlightSelectedTimeoutSetUp()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 313
    const-string v1, "pref_backlight_duration"

    .line 314
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 313
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 316
    .local v0, "timeout":I
    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 317
    const/4 v0, 0x1

    .line 320
    :cond_0
    if-eq v0, v3, :cond_1

    .line 321
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mDurationBacklightText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->values:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    :goto_0
    return-void

    .line 323
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mDurationBacklightText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090223

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private mLinkLostSetup()V
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const v1, 0x7f09022f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pref_link_lost_icon"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 198
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const v1, 0x7f090230

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pref_link_lost_notification"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 201
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$5;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 223
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 244
    return-void
.end method

.method private mLowBatterySetup()V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mBatterySection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v1, 0x7f0901a0

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const v1, 0x7f09022f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pref_low_battery_icon"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 146
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    const v1, 0x7f090230

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pref_low_battery_notification"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setUpElements(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 149
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 171
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$4;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setOnCheckChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 192
    return-void
.end method

.method private saveBacklightDurationPreference(I)V
    .locals 4
    .param p1, "timeout"    # I

    .prologue
    .line 343
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7d0

    .line 345
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 344
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 356
    :goto_0
    return-void

    .line 349
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BACKLIGHT - TIMEOUT - set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 350
    const-string v1, "pref_backlight_duration"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 352
    const-string v1, "pref_backlight_duration"

    const/4 v2, 0x2

    .line 353
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v1

    .line 352
    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->getWatchTimeoutDuration(I)I

    move-result v0

    .line 354
    .local v0, "timeoutInSeconds":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightTimeout(Landroid/content/Context;I)Ljava/util/UUID;

    .line 355
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private saveBacklightIntensityPreference(I)V
    .locals 6
    .param p1, "intensity"    # I

    .prologue
    .line 364
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BACKLIGHT INTENSITY - selected intensity = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 365
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x7d0

    .line 367
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 366
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 370
    const-string v2, "pref_backlight_intensity"

    const/4 v3, 0x1

    .line 371
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 370
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v1

    .line 373
    .local v1, "previousIntensity":I
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 384
    .end local v1    # "previousIntensity":I
    :goto_0
    return-void

    .line 377
    :cond_0
    const-string v2, "pref_backlight_intensity"

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, p1, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 379
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BACKLIGHT - INTENSITY - set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    sget v5, Lcom/vectorwatch/android/utils/Constants;->BACKLIGHT_INTENSITY_STEP_SIZE:I

    mul-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 381
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getIntensityPercentage(Landroid/content/Context;)I

    move-result v0

    .line 382
    .local v0, "intensityPercentage":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightIntensity(Landroid/content/Context;I)Ljava/util/UUID;

    .line 383
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private saveDisplaySecondHand(Z)V
    .locals 4
    .param p1, "isChecked"    # Z

    .prologue
    .line 392
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7d0

    .line 394
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 393
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 397
    const-string v1, "pref_watch_second_hand"

    const/4 v2, 0x1

    .line 398
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 397
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 400
    .local v0, "displaySecondHand":Z
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 410
    .end local v0    # "displaySecondHand":Z
    :goto_0
    return-void

    .line 404
    :cond_0
    const-string v1, "pref_watch_second_hand"

    .line 405
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 404
    invoke-static {v1, p1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 407
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WATCH SECOND HAND - set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 408
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendDisplaySecondHand(Landroid/content/Context;Z)Ljava/util/UUID;

    .line 409
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private secondHandSetup()V
    .locals 4

    .prologue
    .line 303
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090270

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 304
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090245

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 306
    const-string v1, "pref_watch_second_hand"

    const/4 v2, 0x1

    .line 307
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 306
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 309
    .local v0, "displaySecondHand":Z
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 310
    return-void
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 416
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 417
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    .line 420
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showBacklightDurationPicker()V
    .locals 7

    .prologue
    .line 254
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0c00c5

    invoke-direct {v0, v4, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 255
    .local v0, "dialog":Landroid/app/Dialog;
    const-string v4, ""

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 256
    const v4, 0x7f03004f

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 257
    const v4, 0x7f1001a8

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 258
    .local v2, "setButton":Landroid/widget/Button;
    const v4, 0x7f1001a7

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/NumberPicker;

    .line 259
    .local v1, "numberPicker":Landroid/widget/NumberPicker;
    const/high16 v4, 0x60000

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setDescendantFocusability(I)V

    .line 260
    sget v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->BACKLIGH_TIMEOUT_MAX:I

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 261
    sget v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->BACKLIGH_TIMEOUT_MIN:I

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 262
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->values:[Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    .line 263
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    .line 264
    invoke-virtual {v1, p0}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 266
    const-string v4, "pref_backlight_duration"

    const/4 v5, 0x2

    .line 267
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 266
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v3

    .line 268
    .local v3, "value":I
    invoke-virtual {v1, v3}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 269
    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;

    invoke-direct {v4, p0, v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;Landroid/widget/NumberPicker;Landroid/app/Dialog;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 283
    return-void
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 458
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 459
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 461
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 432
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 433
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 434
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    .line 437
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 439
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 443
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 444
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 450
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 451
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->setUpLinkLostState()V

    .line 452
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 454
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const v5, 0x7f09021b

    .line 84
    const v1, 0x7f030063

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 87
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->setUpLinkLostState()V

    .line 89
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "5 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 90
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "20 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "30 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 91
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->values:[Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSeekBar:Landroid/widget/SeekBar;

    sget v2, Lcom/vectorwatch/android/utils/Constants;->BACKLIGHT_INTENSITY_MAX:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 95
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 121
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 128
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 249
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 250
    return-void
.end method

.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .locals 0
    .param p1, "picker"    # Landroid/widget/NumberPicker;
    .param p2, "oldVal"    # I
    .param p3, "newVal"    # I

    .prologue
    .line 299
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 135
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->secondHandSetup()V

    .line 136
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->backlightSelectedTimeoutSetUp()V

    .line 137
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->backlightSelectedIntensitySetUp()V

    .line 138
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostSetup()V

    .line 139
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatterySetup()V

    .line 140
    return-void
.end method

.method public setBackLightDuration()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001da
        }
    .end annotation

    .prologue
    .line 288
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7d0

    .line 290
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 289
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 295
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->showBacklightDurationPicker()V

    goto :goto_0
.end method
