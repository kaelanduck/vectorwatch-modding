.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;
.super Landroid/app/Fragment;
.source "AlertsAndNotificationsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;

.field private mApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AppInfoModel;",
            ">;"
        }
    .end annotation
.end field

.field private mAppsView:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mEnableNotifications:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

.field private mLinkStateImage:Landroid/widget/TextView;

.field private mList:Landroid/widget/ListView;

.field private mMirrorAsyncTask:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

.field private mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 168
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

# for accessing mDismissFromPhone
.method static synthetic access$001(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorAsyncTask:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    return-object v0
.end method

.method static synthetic access$1000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;
    .param p1, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorAsyncTask:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->updateAppsState(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;

    return-object v0
.end method

.method static synthetic access$602(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;
    .param p1, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;

    return-object p1
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mApps:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mAppsView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private instantiateAppsList()V
    .locals 3

    .prologue
    .line 198
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mAppsView:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 199
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 200
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 240
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 241
    return-void
.end method

.method private setDisplayNotificationOption()V
    .locals 3

    .prologue
    .line 111
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: setting listener on enable notifications."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mEnableNotifications:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;->setLabel(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mEnableNotifications:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    return-void
.end method

.method private setMirrorPhoneState()V
    .locals 4

    .prologue
    .line 127
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090176

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090175

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 131
    const-string v1, "flag_mirror_phone"

    const/4 v2, 0x1

    .line 132
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 131
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 134
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 137
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 153
    return-void
.end method

.method private setDismissFromPhoneState()V
    .locals 4

    .prologue
    .line 127
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902c5 #string:label_settings_alerts_dismiss_phone_title

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902c6 #string:label_settings_alerts_dismiss_phone_summary

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 131
    const-string v1, "flag_dismiss_from_phone"

    const/4 v2, 0x1

    .line 132
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 131
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 134
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 137
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$4;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 153
    return-void
.end method

.method private setUpLinkLostState()V
    .locals 2

    .prologue
    .line 282
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateAppsState(Z)V
    .locals 1
    .param p1, "isSetToMirrorPhone"    # Z

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mApps:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mApps:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mApps:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->updateEnableStatusForApps(Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private updateEnableStatusForApps(Ljava/util/List;Z)V
    .locals 7
    .param p2, "isChecked"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AppInfoModel;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AppInfoModel;>;"
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "app_not_all"

    const-class v5, Ljava/util/List;

    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 250
    .local v2, "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/AppInfoModel;

    .line 251
    .local v0, "app":Lcom/vectorwatch/android/models/AppInfoModel;
    if-eqz v0, :cond_0

    .line 252
    iget-object v3, v0, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mContext:Landroid/content/Context;

    invoke-static {v3, v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 253
    if-eqz v2, :cond_3

    .line 254
    if-eqz p2, :cond_1

    .line 255
    iget-object v3, v0, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 256
    iget-object v3, v0, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 259
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 260
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, v0, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 261
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 259
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 266
    .end local v1    # "i":I
    :cond_3
    if-eqz p2, :cond_0

    .line 267
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .restart local v2    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, v0, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 273
    .end local v0    # "app":Lcom/vectorwatch/android/models/AppInfoModel;
    :cond_4
    if-eqz v2, :cond_5

    .line 274
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "app_not_all"

    invoke-virtual {v3, v4, v2}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 276
    :cond_5
    return-void
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 320
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 321
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 323
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 298
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 305
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setUpLinkLostState()V

    .line 306
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 308
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mContext:Landroid/content/Context;

    .line 70
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater1"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "layout_inflater"

    .line 86
    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 88
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300a8

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 90
    .local v1, "view":Landroid/widget/RelativeLayout;
    const v2, 0x7f1001b5

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 91
    const v2, 0x7f10029c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mList:Landroid/widget/ListView;

    .line 92
    const v2, 0x7f10029a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    # instance the dismiss from phone button
    const v2, 0x7f1002ef #id:dismiss_from_phone
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;
    move-result-object v2
    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mDismissFromPhone:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 93
    const v2, 0x7f1000f8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 94
    const v2, 0x7f100298

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mAppsView:Landroid/widget/LinearLayout;

    .line 95
    const v2, 0x7f100299

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mEnableNotifications:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

    .line 97
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mApps:Ljava/util/List;

    .line 99
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setUpLinkLostState()V

    .line 100
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setDisplayNotificationOption()V

    .line 101
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->instantiateAppsList()V

    .line 102
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setMirrorPhoneState()V

    .line 103
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setDismissFromPhoneState()V

    .line 104
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorAsyncTask:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->mMirrorAsyncTask:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->cancel(Z)Z

    .line 80
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 81
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 312
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 313
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSystemNotification(ZLandroid/content/Context;)V

    .line 316
    :cond_0
    return-void
.end method
