.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;
.super Landroid/app/Fragment;
.source "ContextualFragment.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mContext:Landroid/content/Context;

.field private mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mLinkStateImage:Landroid/widget/TextView;

.field private mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

.field private syncSettingsNeeded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->syncSettingsNeeded:Z

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->setUpLinkLostState()V

    return-void
.end method

.method static synthetic access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->syncSettingsNeeded:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->sendSyncSettingsCommandToWatch()V

    return-void
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    return-object v0
.end method

.method private sendSyncSettingsCommandToWatch()V
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 291
    return-void
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 306
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    .line 310
    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private summarySetUp()V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090243

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09023e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090241

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090246

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090244

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090242

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    const v1, 0x7f090095

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSummary(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method private titleSetUp()V
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09026e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09026a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090269

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09026c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090274

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09026f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09026d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    const v1, 0x7f090096

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setTitle(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method private valuesSetUp()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 111
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualMorningFaceSettings(Landroid/content/Context;)Z

    move-result v0

    .line 112
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 113
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 138
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoSleepSettings(Landroid/content/Context;)Z

    move-result v0

    .line 139
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 140
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 161
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoDiscreetSettings(Landroid/content/Context;)Z

    move-result v0

    .line 162
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 163
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$3;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 178
    const-string v1, "drop_notification_option"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 180
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 181
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$4;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 200
    const-string v1, "settings_contextual_tentative_meetings"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 202
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 203
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 225
    const-string v1, "prefs_settings_contextual_notifications"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 227
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 228
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$6;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 243
    const-string v1, "glance_mode"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 245
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 246
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 264
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setVisibility(I)V

    .line 267
    const-string v1, "prefs_offline_badge"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 269
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 270
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 283
    :cond_0
    return-void
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 351
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 322
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 323
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    .line 327
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 328
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 329
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 333
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 334
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 340
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->setUpLinkLostState()V

    .line 342
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 344
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mContext:Landroid/content/Context;

    .line 53
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    const v1, 0x7f0300aa

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f10029f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mMorningFaceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 67
    const v1, 0x7f1002a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoSleepDetectView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 68
    const v1, 0x7f1002a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mAutoDiscreetModeView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 69
    const v1, 0x7f1002a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mFlickToDismissView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 70
    const v1, 0x7f1001b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 71
    const v1, 0x7f1002a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mTentativeMeetingsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 72
    const v1, 0x7f1002a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mNotificationsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 73
    const v1, 0x7f1002a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mGlanceView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 74
    const v1, 0x7f1002a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->mOfflineBadge:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 76
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->setUpLinkLostState()V

    .line 79
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->titleSetUp()V

    .line 80
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->summarySetUp()V

    .line 81
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->valuesSetUp()V

    .line 83
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 59
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 295
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 296
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->syncSettingsNeeded:Z

    if-eqz v0, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->sendSyncSettingsCommandToWatch()V

    .line 298
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->syncSettingsNeeded:Z

    .line 300
    :cond_0
    return-void
.end method
