.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;
.super Ljava/lang/Object;
.source "EditAlarmPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract deleteAlarm(J)V
.end method

.method public abstract getAlarmWithId(J)V
.end method

.method public abstract onDeleteAlarmOptionSelected()V
.end method

.method public abstract onEditNameClicked()V
.end method

.method public abstract onEditRepeatPatternClicked()V
.end method

.method public abstract onStop()V
.end method

.method public abstract populateCheckedRepeatItems(B)[Z
.end method

.method public abstract populateRepeatSelection(B)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract repeatPatternSelected(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract saveAlarm(Ljava/lang/String;IIIBJ)V
.end method

.method public abstract saveAlarmButtonPressed()V
.end method

.method public abstract setLinkLostStatus()V
.end method
