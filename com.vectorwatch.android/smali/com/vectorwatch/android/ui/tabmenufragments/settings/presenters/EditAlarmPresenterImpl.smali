.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;
.super Ljava/lang/Object;
.source "EditAlarmPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;


# static fields
.field private static final COUNT_DAYS_OF_WEEK:B = 0x7t

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;

.field private mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    .line 35
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;

    .line 36
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    .line 38
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 39
    return-void
.end method

.method private getRepeatPattern(Ljava/util/List;)B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)B"
        }
    .end annotation

    .prologue
    .local p1, "repeatOptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 172
    invoke-static {v3, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v1

    .line 174
    .local v1, "repeat":B
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 183
    .end local v1    # "repeat":B
    .local v2, "repeat":B
    :goto_0
    return v2

    .line 178
    .end local v2    # "repeat":B
    .restart local v1    # "repeat":B
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 179
    .local v0, "opt":Ljava/lang/Integer;
    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    shl-int/2addr v4, v5

    or-int/2addr v4, v1

    int-to-byte v1, v4

    .line 180
    goto :goto_1

    .line 182
    .end local v0    # "opt":Ljava/lang/Integer;
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ALARMS - repeat set to - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move v2, v1

    .line 183
    .end local v1    # "repeat":B
    .restart local v2    # "repeat":B
    goto :goto_0
.end method

.method private getRepeatPatternText(B)Ljava/lang/String;
    .locals 4
    .param p1, "repeatPattern"    # B

    .prologue
    const/4 v3, 0x1

    .line 238
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 241
    .local v1, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 242
    .local v0, "opt":B
    if-eqz v0, :cond_0

    .line 243
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 248
    if-eqz v0, :cond_1

    .line 249
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 254
    if-eqz v0, :cond_2

    .line 255
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_2
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 260
    if-eqz v0, :cond_3

    .line 261
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    :cond_3
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 266
    if-eqz v0, :cond_4

    .line 267
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_4
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 272
    if-eqz v0, :cond_5

    .line 273
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    :cond_5
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    int-to-byte v0, v2

    .line 278
    if-eqz v0, :cond_6

    .line 279
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_6
    invoke-direct {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->getRepeatPatternText(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getRepeatPatternText(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "repeatOptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v7, 0x1

    .line 193
    const-string v3, ""

    .line 194
    .local v3, "text":Ljava/lang/String;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 195
    .local v2, "res":Landroid/content/res/Resources;
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 196
    :cond_0
    const v4, 0x7f09020f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 228
    :goto_0
    return-object v4

    .line 199
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->values()[Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    move-result-object v1

    .line 200
    .local v1, "options":[Lcom/vectorwatch/android/utils/Constants$RepeatOption;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 201
    .local v0, "opt":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 203
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_2

    const-string v4, ", "

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f090211

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 204
    goto :goto_1

    .line 203
    :cond_2
    const-string v4, ""

    goto :goto_2

    .line 206
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_3

    const-string v4, ", "

    :goto_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f09020e

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 207
    goto :goto_1

    .line 206
    :cond_3
    const-string v4, ""

    goto :goto_3

    .line 209
    :pswitch_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_4

    const-string v4, ", "

    :goto_4
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f090213

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 210
    goto/16 :goto_1

    .line 209
    :cond_4
    const-string v4, ""

    goto :goto_4

    .line 212
    :pswitch_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_5

    const-string v4, ", "

    :goto_5
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f090214

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 213
    goto/16 :goto_1

    .line 212
    :cond_5
    const-string v4, ""

    goto :goto_5

    .line 215
    :pswitch_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_6

    const-string v4, ", "

    :goto_6
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f090212

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 216
    goto/16 :goto_1

    .line 215
    :cond_6
    const-string v4, ""

    goto :goto_6

    .line 218
    :pswitch_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_7

    const-string v4, ", "

    :goto_7
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f09020d

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 219
    goto/16 :goto_1

    .line 218
    :cond_7
    const-string v4, ""

    goto :goto_7

    .line 221
    :pswitch_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_8

    const-string v4, ", "

    :goto_8
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f090210

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_8
    const-string v4, ""

    goto :goto_8

    .line 227
    .end local v0    # "opt":Ljava/lang/Integer;
    :cond_9
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ALARMS - repeat pattern text = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move-object v4, v3

    .line 228
    goto/16 :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public deleteAlarm(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 51
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ALARMS - Delete alarm id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;->delete(J)V

    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/AlarmHelpers;->syncAlarms(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public getAlarmWithId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;->getAlarmWithId(J)V

    .line 60
    return-void
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->updateLinkLostStatus(Z)V

    .line 161
    return-void
.end method

.method public onDeleteAlarmOptionSelected()V
    .locals 3

    .prologue
    .line 71
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->displayAlert(Ljava/lang/String;I)V

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->displayDeleteAlarmPopup()V

    goto :goto_0
.end method

.method public onEditNameClicked()V
    .locals 3

    .prologue
    .line 100
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->displayAlert(Ljava/lang/String;I)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->editAlarmNameAllowed()V

    goto :goto_0
.end method

.method public onEditRepeatPatternClicked()V
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->displayAlert(Ljava/lang/String;I)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->editAlarmRepeatPatternAllowed()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;->onStop()V

    .line 95
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 96
    return-void
.end method

.method public populateCheckedRepeatItems(B)[Z
    .locals 6
    .param p1, "pattern"    # B

    .prologue
    const/4 v5, 0x7

    .line 118
    new-array v0, v5, [Z

    .line 119
    .local v0, "checkedList":[Z
    const/4 v1, 0x1

    .local v1, "i":B
    :goto_0
    if-gt v1, v5, :cond_1

    .line 120
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ALARMS - size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " element = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 121
    invoke-static {p1, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    add-int/lit8 v2, v1, -0x1

    const/4 v3, 0x1

    aput-boolean v3, v0, v2

    .line 119
    :goto_1
    add-int/lit8 v2, v1, 0x1

    int-to-byte v1, v2

    goto :goto_0

    .line 125
    :cond_0
    add-int/lit8 v2, v1, -0x1

    const/4 v3, 0x0

    aput-boolean v3, v0, v2

    goto :goto_1

    .line 129
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "ALARMS - populate check"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 131
    return-object v0
.end method

.method public populateRepeatSelection(B)Ljava/util/ArrayList;
    .locals 4
    .param p1, "pattern"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x1

    .local v0, "i":B
    :goto_0
    const/4 v2, 0x7

    if-gt v0, v2, :cond_1

    .line 139
    invoke-static {p1, v0}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    add-int/lit8 v2, v0, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_0
    add-int/lit8 v2, v0, 0x1

    int-to-byte v0, v2

    goto :goto_0

    .line 145
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "ALARMS - populate list"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 147
    return-object v1
.end method

.method public repeatPatternSelected(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->getRepeatPattern(Ljava/util/List;)B

    move-result v0

    .line 65
    .local v0, "repeatPattern":B
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->getRepeatPatternText(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "repeatPatternText":Ljava/lang/String;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-interface {v2, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->setRepeatPattern(BLjava/lang/String;)V

    .line 67
    return-void
.end method

.method public saveAlarm(Ljava/lang/String;IIIBJ)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "timeOfDay"    # I
    .param p5, "enabled"    # B
    .param p6, "id"    # J

    .prologue
    .line 44
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p5

    move-wide v6, p6

    invoke-interface/range {v1 .. v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;->saveAlarm(Ljava/lang/String;IIBJ)V

    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/AlarmHelpers;->syncAlarms(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public saveAlarmButtonPressed()V
    .locals 3

    .prologue
    .line 85
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->displayAlert(Ljava/lang/String;I)V

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->saveAlarmAllowed()V

    goto :goto_0
.end method

.method public sendAlarm(Ljava/lang/String;IIBJ)V
    .locals 9
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "enabledStatus"    # B
    .param p5, "id"    # J

    .prologue
    .line 152
    invoke-direct {p0, p4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->getRepeatPatternText(B)Ljava/lang/String;

    move-result-object v8

    .line 153
    .local v8, "repeatPatternText":Ljava/lang/String;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-interface/range {v1 .. v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->populateAlarm(Ljava/lang/String;IIBJLjava/lang/String;)V

    .line 154
    return-void
.end method

.method public setLinkLostStatus()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmView;->updateLinkLostStatus(Z)V

    .line 81
    return-void
.end method
