.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;
.super Ljava/lang/Object;
.source "AlarmsPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;

.field private mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mContext:Landroid/content/Context;

    .line 40
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;

    .line 42
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 43
    return-void
.end method


# virtual methods
.method public createAlarmPressed()V
    .locals 3

    .prologue
    .line 90
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->displayAlert(Ljava/lang/String;I)V

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "ALARMS - Trying to add new alarms. Checking for limit first."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;->createAlarmPressed()V

    goto :goto_0
.end method

.method public getAllAlarms()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;->getAllAlarms()V

    .line 49
    return-void
.end method

.method public handleAlarmCreatedEvent(Lcom/vectorwatch/android/events/AlarmCreatedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/AlarmCreatedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->getAllAlarms()V

    .line 137
    return-void
.end method

.method public handleAlarmDeletedEvent(Lcom/vectorwatch/android/events/AlarmDeletedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/AlarmDeletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->getAllAlarms()V

    .line 127
    return-void
.end method

.method public handleAlarmSyncFromCloudEvent(Lcom/vectorwatch/android/events/AlarmSyncFromCloudEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/AlarmSyncFromCloudEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->getAllAlarms()V

    .line 147
    return-void
.end method

.method public handleAlarmUpdatedEvent(Lcom/vectorwatch/android/events/AlarmUpdatedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/AlarmUpdatedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->getAllAlarms()V

    .line 132
    return-void
.end method

.method public handleAlarmsRefreshedByWatchEvent(Lcom/vectorwatch/android/events/AlarmsRefreshedByWatchEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/AlarmsRefreshedByWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->getAllAlarms()V

    .line 142
    return-void
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->updateLinkLostStatus(Z)V

    .line 152
    return-void
.end method

.method public notifyAlarmLimitStatus(Z)V
    .locals 3
    .param p1, "isLimitReached"    # Z

    .prologue
    .line 110
    if-eqz p1, :cond_0

    .line 111
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->displayAlert(Ljava/lang/String;I)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->startCreateAlarmMode()V

    goto :goto_0
.end method

.method public onAlarmClicked(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 54
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->displayAlert(Ljava/lang/String;I)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->startEditAlarmMode(J)V

    goto :goto_0
.end method

.method public onAlarmStatusChanged(JBZ)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "enabledValue"    # B
    .param p4, "isActive"    # Z

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    if-eqz p4, :cond_0

    .line 65
    invoke-static {p3, v1}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result p3

    .line 70
    :goto_0
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ALARMS - ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Enabled value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->updateAlarmStatus(JB)V

    .line 81
    :goto_1
    return-void

    .line 67
    :cond_0
    invoke-static {p3, v1}, Lcom/vectorwatch/android/utils/Helpers;->clearBit(BB)B

    move-result p3

    goto :goto_0

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->displayAlert(Ljava/lang/String;I)V

    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->updateAlarm(J)V

    goto :goto_1
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;->onStop()V

    .line 104
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public sendAlarms(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->updateAlarmsList(Ljava/util/List;)V

    .line 121
    return-void
.end method

.method public setLinkLostStatus()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmsActivityView;->updateLinkLostStatus(Z)V

    .line 86
    return-void
.end method

.method public updateAlarmStatus(JB)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "enabledValue"    # B

    .prologue
    .line 156
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "ALARMS - update alarm status and sync it."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;

    invoke-interface {v0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;->updateAlarmStatus(JB)V

    .line 159
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenterImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/AlarmHelpers;->syncAlarms(Landroid/content/Context;)V

    .line 160
    return-void
.end method
