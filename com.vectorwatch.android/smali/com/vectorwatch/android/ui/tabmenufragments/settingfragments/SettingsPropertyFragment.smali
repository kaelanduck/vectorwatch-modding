.class public abstract Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
.super Landroid/app/Fragment;
.source "SettingsPropertyFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;
    }
.end annotation


# static fields
.field static final CONTROLLER_ALLOW_CUSTOM_VALUE:Ljava/lang/String; = "c_allow_custom"

.field static final CONTROLLER_TYPE:Ljava/lang/String; = "c_type"


# instance fields
.field private asYouType:Z

.field private isDynamic:Z

.field protected mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

.field private minChars:I

.field private sDummyCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->isDynamic:Z

    .line 19
    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->asYouType:Z

    .line 20
    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->minChars:I

    .line 69
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->sDummyCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    return-void
.end method


# virtual methods
.method public getAsYouType()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->asYouType:Z

    return v0
.end method

.method public getIsDynamic()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->isDynamic:Z

    return v0
.end method

.method public getMinChars()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->minChars:I

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 99
    instance-of v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "Activity must implement fragment\'s callbacks."

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    .line 104
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->sDummyCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    .line 110
    return-void
.end method

.method public abstract onOptionsReceived()V
.end method

.method public setAsYouType(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->asYouType:Z

    .line 28
    return-void
.end method

.method public setIsDynamic(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->isDynamic:Z

    .line 24
    return-void
.end method

.method public setMinChars(I)V
    .locals 0
    .param p1, "minChars"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->minChars:I

    .line 32
    return-void
.end method
