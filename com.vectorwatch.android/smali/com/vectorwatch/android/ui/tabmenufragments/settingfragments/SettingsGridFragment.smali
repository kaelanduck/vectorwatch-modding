.class public Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;
.super Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
.source "SettingsGridFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;
    }
.end annotation


# instance fields
.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation
.end field

.field private mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

.field private mGridView:Landroid/widget/GridView;

.field private mHintLabel:Landroid/widget/TextView;

.field private mKeyName:Ljava/lang/String;

.field private progressBar:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;-><init>()V

    .line 133
    return-void
.end method

.method private loadMainGridAdapter()V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0300b1

    const v3, 0x1020014

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 90
    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;
    .locals 3
    .param p0, "keyName"    # Ljava/lang/String;

    .prologue
    .line 26
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "c_type"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;-><init>()V

    .line 29
    .local v1, "settingsGridFragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v1
.end method

.method private reloadDataOnAdapter()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mData:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->clear()V

    .line 108
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mData:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->addAll(Ljava/util/Collection;)V

    .line 110
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->getIsDynamic()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->progressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mKeyName:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->reloadDataOnAdapter()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onAttach(Landroid/app/Activity;)V

    .line 45
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "c_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mKeyName:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mKeyName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->getValues(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mData:Ljava/util/List;

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    const v1, 0x7f0300b0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f1002cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridView:Landroid/widget/GridView;

    .line 67
    const v1, 0x7f1002c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mHintLabel:Landroid/widget/TextView;

    .line 68
    const v1, 0x7f1000f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->progressBar:Landroid/view/View;

    .line 69
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onDestroy()V

    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->clear()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    .line 123
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onDestroyView()V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridView:Landroid/widget/GridView;

    .line 116
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mKeyName:Ljava/lang/String;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    invoke-virtual {v2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    .line 128
    invoke-virtual {v3, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getValue()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    .line 129
    invoke-virtual {v4, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getType()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mGridAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;

    .line 130
    invoke-virtual {v5, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .line 127
    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V

    .line 131
    return-void
.end method

.method public onOptionsReceived()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->progressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mKeyName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->getValues(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mData:Ljava/util/List;

    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->continueOnEmptyOption()V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->reloadDataOnAdapter()V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 75
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mKeyName:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->getHint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "hint":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mHintLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->mHintLabel:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->loadMainGridAdapter()V

    .line 82
    return-void
.end method
