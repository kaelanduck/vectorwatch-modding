.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;
.super Ljava/lang/Object;
.source "SettingsPropertyFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract continueOnEmptyOption()V
.end method

.method public abstract getHint(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getValues(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation
.end method

.method public abstract requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V
.end method

.method public abstract select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V
.end method
