.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StoreItemDetailsRatingsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;


# instance fields
.field private mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;

.field mEmptyText:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100156
        }
    .end annotation
.end field

.field mFabAdd:Lcom/software/shell/fab/ActionButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100157
        }
    .end annotation
.end field

.field mItemRatingListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100155
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;

.field mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000f8
        }
    .end annotation
.end field

.field private mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

.field private mStoreElement:Lcom/vectorwatch/android/models/StoreElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public addRatingButton()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100157
        }
    .end annotation

    .prologue
    .line 122
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "item"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 124
    const-string v1, "item_rating"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 125
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 126
    return-void
.end method

.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 101
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 102
    return-void
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mItemRatingListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 91
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 131
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 132
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    const-string v1, "rating_result_data"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getRating()F

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StoreElement;->setRating(F)V

    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;->loadRatings(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 137
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v3, 0x3fc00000    # 1.5f

    .line 48
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f030034

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->setContentView(I)V

    .line 51
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 53
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mItemRatingListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    .line 60
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    const v1, 0x7f020135

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColor(I)V

    .line 62
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColorPressed(I)V

    .line 63
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v3}, Lcom/software/shell/fab/ActionButton;->setShadowYOffset(F)V

    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v3}, Lcom/software/shell/fab/ActionButton;->setShadowXOffset(F)V

    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowColor(I)V

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->JUMP_TO_DOWN:Lcom/software/shell/fab/ActionButton$Animations;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setHideAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mFabAdd:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->JUMP_FROM_DOWN:Lcom/software/shell/fab/ActionButton$Animations;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShowAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    .line 71
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;

    .line 72
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;->loadRatings(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 73
    return-void
.end method

.method public onRatingsLoaded(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;

    .line 112
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mItemRatingListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 113
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 114
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->isOwnRating()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    .line 113
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    return-void
.end method

.method public showProgress()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;->mItemRatingListView:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 82
    return-void
.end method
