.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;
.super Ljava/lang/Object;
.source "StorePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Interactor"
.end annotation


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;)V
.end method

.method public abstract onStoreQueryFailure(Lretrofit/RetrofitError;)V
.end method

.method public abstract onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V
.end method
