.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;
.super Ljava/lang/Object;
.source "StoreAddRatingView.java"


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;I)V
.end method

.method public abstract hideProgress()V
.end method

.method public abstract onRatingAdded(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V
.end method

.method public abstract onRatingsLoaded(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract showProgress()V
.end method
