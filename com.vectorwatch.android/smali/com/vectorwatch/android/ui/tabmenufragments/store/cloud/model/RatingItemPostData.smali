.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;
.super Ljava/lang/Object;
.source "RatingItemPostData.java"


# instance fields
.field private rating:F


# direct methods
.method public constructor <init>(F)V
    .locals 0
    .param p1, "rating"    # F

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;->rating:F

    .line 11
    return-void
.end method


# virtual methods
.method public getRating()F
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;->rating:F

    return v0
.end method

.method public setRating(F)V
    .locals 0
    .param p1, "rating"    # F

    .prologue
    .line 18
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;->rating:F

    .line 19
    return-void
.end method
