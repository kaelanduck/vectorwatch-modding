.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StoreItemDetailsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;


# instance fields
.field private log:Lorg/slf4j/Logger;

.field mActionButton:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100150
        }
    .end annotation
.end field

.field mAppDescription:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10014f
        }
    .end annotation
.end field

.field mAppName:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10014e
        }
    .end annotation
.end field

.field mExtendedDescription:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100151
        }
    .end annotation
.end field

.field mImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10014c
        }
    .end annotation
.end field

.field mImgInstalled:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10014d
        }
    .end annotation
.end field

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;

.field mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000f8
        }
    .end annotation
.end field

.field mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000eb
        }
    .end annotation
.end field

.field mRatingLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100153
        }
    .end annotation
.end field

.field mReportLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100154
        }
    .end annotation
.end field

.field mReviewsLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100152
        }
    .end annotation
.end field

.field private mStoreElement:Lcom/vectorwatch/android/models/StoreElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 39
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 198
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 199
    return-void
.end method

.method public onActionClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100150
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;->onActionClick(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 128
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 203
    invoke-super {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 204
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 205
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    const-string v1, "rating_result_data"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getRating()F

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 209
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x8

    .line 68
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v2, 0x7f030033

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->setContentView(I)V

    .line 70
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 71
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "item"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/StoreElement;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    .line 72
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mAppName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mAppDescription:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StoreElement;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 78
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    mul-int/lit16 v1, v2, 0x400

    .line 79
    .local v1, "availableMemoryInBytes":I
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$1;

    div-int/lit8 v3, v1, 0xa

    invoke-direct {v2, p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;I)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mMemoryCache:Landroid/util/LruCache;

    .line 86
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mMemoryCache:Landroid/util/LruCache;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getExtendedDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mExtendedDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    :goto_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StoreElement;->getRating()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 104
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-direct {v2, p0, p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;Lcom/vectorwatch/android/models/StoreElement;)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;

    .line 106
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mReportLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 108
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mReviewsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 109
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 111
    :cond_0
    return-void

    .line 89
    :cond_1
    invoke-static {p0}, Lcom/bumptech/glide/Glide;->with(Landroid/support/v4/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    .line 90
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImageView:Landroid/widget/ImageView;

    .line 92
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    goto :goto_0

    .line 98
    :cond_2
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mExtendedDescription:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mExtendedDescription:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0900a5

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StoreElement;->getExtendedDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 122
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;->onDestroy()V

    .line 123
    return-void
.end method

.method public onRatingLayoutClick()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100153
        }
    .end annotation

    .prologue
    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "item"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 141
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 142
    return-void
.end method

.method public onReportAbuseClick()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100154
        }
    .end annotation

    .prologue
    .line 146
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "item"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 148
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 149
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;->updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 117
    return-void
.end method

.method public onReviewsClick()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100152
        }
    .end annotation

    .prologue
    .line 132
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "item"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 134
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 135
    return-void
.end method

.method public showDownloadButton()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 184
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    const v1, 0x7f0200ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 185
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImgInstalled:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 188
    return-void
.end method

.method public showDownloadProgress(Z)V
    .locals 3
    .param p1, "isInstalled"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 158
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 161
    if-eqz p1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImgInstalled:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImgInstalled:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showRemoveButton()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 173
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    const v1, 0x7f0200e8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 174
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImgInstalled:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 177
    return-void
.end method
