.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StoreActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

.field private mAppsArrayList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation
.end field

.field private mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field mEmptyText:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100110
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;

.field mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000f8
        }
    .end annotation
.end field

.field mSearchFAB:Lcom/software/shell/fab/ActionButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d6
        }
    .end annotation
.end field

.field mStoreAppsListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d7
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)Lcom/vectorwatch/android/models/cloud/AppsOption;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;

    return-object v0
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delay"    # I

    .prologue
    .line 209
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 210
    return-void
.end method

.method public goToSearch()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001d6
        }
    .end annotation

    .prologue
    .line 214
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->startActivity(Landroid/content/Intent;)V

    .line 216
    return-void
.end method

.method public handleAppInstallInterrupted(Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 184
    :cond_0
    return-void
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 142
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v0, 0x7f030062

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->setContentView(I)V

    .line 68
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 71
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "app_option"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    .line 87
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const v1, 0x7f02013c

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 88
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColor(I)V

    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColorPressed(I)V

    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/high16 v1, 0x40600000    # 3.5f

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowYOffset(F)V

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowXOffset(F)V

    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowColor(I)V

    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v3}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->JUMP_TO_DOWN:Lcom/software/shell/fab/ActionButton$Animations;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setHideAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->JUMP_FROM_DOWN:Lcom/software/shell/fab/ActionButton$Animations;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShowAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    .line 99
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-interface {v0, v3, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;->loadFacesInList(ILcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mStoreAppsListView:Landroid/widget/ListView;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$2;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)V

    invoke-direct {v1, p0, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;Lcom/vectorwatch/android/ui/store/OnEndListCallback;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 115
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    .line 118
    :cond_0
    return-void

    .line 73
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 74
    const v0, 0x7f09024d

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 77
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 78
    const v0, 0x7f09024e

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 81
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 82
    const v0, 0x7f09024f

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;->onDestroy()V

    .line 131
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 132
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 126
    :cond_0
    return-void
.end method

.method public populateFacesListView(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;

    .prologue
    .line 151
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getStoreElements()Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    .line 152
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-nez v0, :cond_3

    .line 153
    new-instance v0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    const v3, 0x7f030085

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)V

    const-string v5, "store_watchfaces"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;ILcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .line 161
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mStoreAppsListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mStoreAppsListView:Landroid/widget/ListView;

    new-instance v1, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mStoreAppsListView:Landroid/widget/ListView;

    invoke-direct {v1, p0, v2, v3}, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 164
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mEmptyText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    invoke-static {p0}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatus(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mEmptyText:Landroid/widget/TextView;

    const v1, 0x7f09010f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->addItems(Ljava/util/List;)V

    goto :goto_0

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mEmptyText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsArrayList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->addItems(Ljava/util/List;)V

    goto :goto_0
.end method

.method public refreshList(Lcom/vectorwatch/android/events/AppListReloadEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppListReloadEvent;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_1

    .line 189
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->onOperationFinished(Ljava/lang/String;)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 194
    :cond_1
    return-void
.end method

.method public refreshStreamList(Lcom/vectorwatch/android/events/StreamListReloadEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamListReloadEvent;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_0

    .line 199
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamListReloadEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamListReloadEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamListReloadEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->onOperationFinished(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {p0}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->setAllStreams(Ljava/util/List;)V

    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 205
    :cond_0
    return-void
.end method

.method public showProgress()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 137
    return-void
.end method
