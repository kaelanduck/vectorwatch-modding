.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;
.super Ljava/lang/Object;
.source "StoreItemDetailsRatingsPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractor;

.field private mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    .line 25
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractor;

    .line 26
    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;->hideProgress()V

    .line 48
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;->displayAlert(Ljava/lang/String;I)V

    .line 49
    return-void
.end method

.method public loadRatings(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 1
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractor;->loadRatingsList(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;->showProgress()V

    .line 60
    return-void
.end method

.method public onRatingsLoaded(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;->onRatingsLoaded(Ljava/util/List;)V

    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenterImpl;->mStoreItemDetailsRatingsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsRatingsView;->hideProgress()V

    .line 37
    return-void
.end method
