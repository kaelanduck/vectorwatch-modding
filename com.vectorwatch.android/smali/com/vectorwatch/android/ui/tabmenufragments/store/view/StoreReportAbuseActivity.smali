.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StoreReportAbuseActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;


# instance fields
.field mCommentEdit:Landroid/widget/EditText;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10014b
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$View;

.field private mStoreElement:Lcom/vectorwatch/android/models/StoreElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 68
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 69
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f030035

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->setContentView(I)V

    .line 32
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 34
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    .line 36
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$View;

    .line 37
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 42
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f110005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 43
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 48
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 57
    :goto_0
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 50
    :sswitch_0
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    .line 51
    const/4 v0, 0x1

    goto :goto_1

    .line 54
    :sswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mCommentEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$View;->reportAbuse(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1002ec -> :sswitch_1
    .end sparse-switch
.end method
