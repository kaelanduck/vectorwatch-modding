.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;
.super Ljava/lang/Object;
.source "StoreReportAbusePresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractor;

.field private mStoreReportAbuseView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;->mContext:Landroid/content/Context;

    .line 21
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;->mStoreReportAbuseView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;

    .line 22
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractor;

    .line 23
    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;->mStoreReportAbuseView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseView;->displayAlert(Ljava/lang/String;I)V

    .line 34
    return-void
.end method

.method public reportAbuse(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;)V
    .locals 1
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractor;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractor;->reportAbuse(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;)V

    .line 45
    return-void
.end method
