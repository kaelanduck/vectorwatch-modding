.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StoreAddRatingActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;


# instance fields
.field mCommentEdit:Landroid/widget/EditText;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10014b
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;

.field mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000f8
        }
    .end annotation
.end field

.field mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000eb
        }
    .end annotation
.end field

.field private mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

.field private mStoreElement:Lcom/vectorwatch/android/models/StoreElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 114
    invoke-static {p1, p2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 115
    return-void
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 104
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->setContentView(I)V

    .line 47
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 49
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    .line 50
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "item_rating"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 54
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;

    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    if-eqz v0, :cond_1

    .line 57
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mCommentEdit:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getComment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getRatingValue()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 63
    :goto_1
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getComment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;->loadOwnRating(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 68
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f110003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 69
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 149
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 150
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 151
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 74
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 87
    :goto_0
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 76
    :sswitch_0
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    .line 77
    const/4 v0, 0x1

    goto :goto_1

    .line 80
    :sswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mCommentEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->getRating()I

    move-result v3

    int-to-float v3, v3

    invoke-interface {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;->addRating(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;F)V

    .line 82
    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_REVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SENT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v0, 0x0

    check-cast v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {p0, v1, v2, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1002eb -> :sswitch_1
    .end sparse-switch
.end method

.method public onRatingAdded(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V
    .locals 3
    .param p1, "postData"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    .prologue
    .line 124
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 125
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "rating_result_data"

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;->getRating()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 126
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->setResult(ILandroid/content/Intent;)V

    .line 127
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->finish()V

    .line 128
    return-void
.end method

.method public onRatingsLoaded(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "ratingItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 138
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->isOwnRating()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    .line 140
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getRatingValue()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 141
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mCommentEdit:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mRatingItem:Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :cond_0
    return-void

    .line 137
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public showProgress()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 96
    return-void
.end method
