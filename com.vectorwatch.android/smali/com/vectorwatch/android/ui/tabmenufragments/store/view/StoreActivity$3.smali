.class Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$3;
.super Ljava/lang/Object;
.source "StoreActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->populateFacesListView(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSaveAction(Lcom/vectorwatch/android/models/StoreElement;I)V
    .locals 2
    .param p1, "item"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "positionInList"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;)Lcom/vectorwatch/android/models/cloud/AppsOption;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;->onSaveAction(Lcom/vectorwatch/android/models/StoreElement;ILcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 158
    return-void
.end method
