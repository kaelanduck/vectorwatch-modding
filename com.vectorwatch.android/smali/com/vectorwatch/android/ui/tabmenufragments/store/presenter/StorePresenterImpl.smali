.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;
.super Ljava/lang/Object;
.source "StorePresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mStoreAppsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;

.field private mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeAppsView"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    .line 41
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;

    .line 42
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 43
    return-void
.end method


# virtual methods
.method public displayAlert(Ljava/lang/String;)V
    .locals 2
    .param p1, "alert"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    const/16 v1, 0x5dc

    invoke-interface {v0, p1, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->displayAlert(Ljava/lang/String;I)V

    .line 122
    :cond_0
    return-void
.end method

.method public handleAppInstallInterruptedEvent(Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->hideProgress()V

    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->handleAppInstallInterrupted(Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;)V

    .line 94
    :cond_0
    return-void
.end method

.method public handleAppListReloadEvent(Lcom/vectorwatch/android/events/AppListReloadEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->hideProgress()V

    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->refreshList(Lcom/vectorwatch/android/events/AppListReloadEvent;)V

    .line 102
    :cond_0
    return-void
.end method

.method public handleDownloadInProgressEvent(Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->hideProgress()V

    .line 83
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->displayAlert(Ljava/lang/String;I)V

    .line 86
    :cond_0
    return-void
.end method

.method public handleStreamListReloadEvent(Lcom/vectorwatch/android/events/StreamListReloadEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->hideProgress()V

    .line 108
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->refreshStreamList(Lcom/vectorwatch/android/events/StreamListReloadEvent;)V

    .line 110
    :cond_0
    return-void
.end method

.method public loadFacesInList(ILcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 1
    .param p1, "page"    # I
    .param p2, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->showProgress()V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;->loadFacesInList(ILcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 65
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method public onSaveAction(Lcom/vectorwatch/android/models/StoreElement;ILcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 1
    .param p1, "item"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "positionInList"    # I
    .param p3, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;

    invoke-interface {v0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;->onSaveAction(Lcom/vectorwatch/android/models/StoreElement;ILcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 77
    return-void
.end method

.method public onStoreQueryFailure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->hideProgress()V

    .line 149
    :cond_0
    return-void
.end method

.method public onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 2
    .param p1, "storeQuery"    # Lcom/vectorwatch/android/models/StoreQuery;
    .param p2, "response"    # Lretrofit/client/Response;
    .param p3, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    .line 133
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->hideProgress()V

    .line 135
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenterImpl;->mStoreAppsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;

    new-instance v1, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;

    invoke-direct {v1, p3, p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;-><init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Lcom/vectorwatch/android/models/StoreQuery;)V

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;->populateFacesListView(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V

    .line 137
    :cond_0
    return-void
.end method
