.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;
.super Ljava/lang/Object;
.source "StoreAddRatingPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;

.field private mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mContext:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    .line 26
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;

    .line 27
    return-void
.end method


# virtual methods
.method public addRating(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;F)V
    .locals 1
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "comment"    # Ljava/lang/String;
    .param p3, "rating"    # F

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;->showProgress()V

    .line 82
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;

    invoke-interface {v0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;->addRating(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;F)V

    .line 83
    return-void
.end method

.method public displayAlert(Ljava/lang/String;I)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;->hideProgress()V

    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;->displayAlert(Ljava/lang/String;I)V

    .line 60
    return-void
.end method

.method public loadOwnRating(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 1
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;->loadOwnRating(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 70
    return-void
.end method

.method public onRatingAdded(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V
    .locals 1
    .param p1, "ratingAddPostData"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;->hideProgress()V

    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;->onRatingAdded(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V

    .line 48
    return-void
.end method

.method public onRatingsLoaded(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "ratingItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenterImpl;->mStoreAddRatingView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreAddRatingView;->onRatingsLoaded(Ljava/util/List;)V

    .line 37
    return-void
.end method
