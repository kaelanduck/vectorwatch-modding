.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;
.super Ljava/lang/Object;
.source "StoreItemDetailsPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

.field private mStoreItemDetailsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;

.field private mStoreItemDetailsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mItemView"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;
    .param p3, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;

    .line 27
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;

    .line 28
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mContext:Landroid/content/Context;

    .line 29
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    .line 30
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 31
    return-void
.end method


# virtual methods
.method public handleAppListReloadEvent(Lcom/vectorwatch/android/events/AppListReloadEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 97
    return-void
.end method

.method public handleStreamListReloadEvent(Lcom/vectorwatch/android/events/StreamListReloadEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreElement:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 105
    return-void
.end method

.method public onActionClick(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 1
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;->onActionClick(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 51
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public onErrorReceived(Ljava/lang/String;I)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delayTime"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;->displayAlert(Ljava/lang/String;I)V

    .line 89
    return-void
.end method

.method public onViewUpdated(ZZ)V
    .locals 1
    .param p1, "isInstalled"    # Z
    .param p2, "isTransmitted"    # Z

    .prologue
    .line 69
    if-eqz p2, :cond_0

    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;->showDownloadProgress(Z)V

    .line 78
    :goto_0
    return-void

    .line 72
    :cond_0
    if-eqz p1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;->showRemoveButton()V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsView;->showDownloadButton()V

    goto :goto_0
.end method

.method public updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 1
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenterImpl;->mStoreItemDetailsInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;->updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 41
    return-void
.end method
