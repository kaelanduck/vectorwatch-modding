.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
.super Landroid/app/Fragment;
.source "StoreMainFragment.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final CLOUD_CALLS_TO_SHOW_STORE:I

.field private mCloudCallsCount:I

.field mLinkStateImage:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001b5
        }
    .end annotation
.end field

.field mListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d5
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;

.field mSearchFAB:Lcom/software/shell/fab/ActionButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d6
        }
    .end annotation
.end field

.field private mSlideList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;"
        }
    .end annotation
.end field

.field private mStorePlacesAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;

.field private mStorePlacesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;",
            ">;"
        }
    .end annotation
.end field

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 60
    const/4 v0, 0x4

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->CLOUD_CALLS_TO_SHOW_STORE:I

    .line 63
    return-void
.end method

.method public static newInstance()Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;-><init>()V

    .line 67
    .local v0, "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    return-object v0
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 269
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    .line 273
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private trySetupList()V
    .locals 4

    .prologue
    .line 182
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 183
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSlideList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    const/4 v1, 0x0

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    invoke-direct {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 186
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSlideList:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesAdapter:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 189
    :cond_1
    return-void
.end method


# virtual methods
.method public goToSearch()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001d6
        }
    .end annotation

    .prologue
    .line 313
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 314
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->startActivity(Landroid/content/Intent;)V

    .line 315
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 319
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 320
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    .line 324
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 330
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 331
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChanged(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 253
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->setUpLinkLostState()V

    .line 254
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 256
    :cond_0
    return-void
.end method

.method public hideProgress()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public hideSlider()V
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    .line 247
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSlideList:Ljava/util/List;

    .line 248
    return-void
.end method

.method public navigateToApps()V
    .locals 3

    .prologue
    .line 136
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "app_option"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->startActivity(Landroid/content/Intent;)V

    .line 139
    return-void
.end method

.method public navigateToStreams()V
    .locals 3

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "app_option"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 145
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->startActivity(Landroid/content/Intent;)V

    .line 146
    return-void
.end method

.method public navigateToWatchfaces()V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "app_option"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 152
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->startActivity(Landroid/content/Intent;)V

    .line 153
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    const v0, 0x7f030060

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mView:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mView:Landroid/view/View;

    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 82
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;

    .line 83
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;->onDestroy()V

    .line 119
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 120
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 121
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 285
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move v0, p3

    .line 286
    .local v0, "pos":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSlideList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 287
    add-int/lit8 v0, v0, -0x1

    .line 289
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 309
    :goto_0
    return-void

    .line 291
    :pswitch_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->navigateToWatchfaces()V

    .line 292
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_WATCHFACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_0

    .line 297
    :pswitch_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->navigateToStreams()V

    .line 298
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_STREAMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_0

    .line 304
    :pswitch_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->navigateToApps()V

    .line 305
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_APPLICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_0

    .line 289
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRecommendedOfflineSlide(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V
    .locals 1
    .param p1, "slide"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;

    .prologue
    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 261
    .local v0, "slideList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSlideList:Ljava/util/List;

    .line 263
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 113
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->setUpLinkLostState()V

    .line 114
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 90
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->setUpLinkLostState()V

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;->getStorePlaces()V

    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const v1, 0x7f02013c

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 98
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColor(I)V

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColorPressed(I)V

    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/high16 v1, 0x40600000    # 3.5f

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowYOffset(F)V

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowXOffset(F)V

    .line 102
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowColor(I)V

    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    .line 105
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    .line 108
    :cond_0
    return-void
.end method

.method public populateImageList(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
    .locals 9
    .param p1, "event"    # Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 199
    iget v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    .line 200
    if-nez p1, :cond_0

    .line 201
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->trySetupList()V

    .line 242
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getStoreElements()Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    .line 207
    .local v0, "elem1":Lcom/vectorwatch/android/models/StoreElement;
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getStoreElements()Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/StoreElement;

    .line 208
    .local v1, "elem2":Lcom/vectorwatch/android/models/StoreElement;
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getStoreElements()Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/StoreElement;

    .line 209
    .local v2, "elem3":Lcom/vectorwatch/android/models/StoreElement;
    sget-object v4, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$1;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getOption()Lcom/vectorwatch/android/models/cloud/AppsOption;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/cloud/AppsOption;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 241
    :goto_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->trySetupList()V

    goto :goto_0

    .line 211
    :pswitch_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    .line 212
    .local v3, "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl1(Ljava/lang/String;)V

    .line 213
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl2(Ljava/lang/String;)V

    .line 214
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl3(Ljava/lang/String;)V

    .line 215
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 216
    :cond_1
    invoke-virtual {v3, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setNewInStore(Z)V

    .line 218
    :cond_2
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    invoke-interface {v4, v7, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 221
    .end local v3    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    :pswitch_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    .line 222
    .restart local v3    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl1(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl2(Ljava/lang/String;)V

    .line 224
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl3(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 226
    :cond_3
    invoke-virtual {v3, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setNewInStore(Z)V

    .line 228
    :cond_4
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    invoke-interface {v4, v6, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 231
    .end local v3    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    :pswitch_2
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    .line 232
    .restart local v3    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl1(Ljava/lang/String;)V

    .line 233
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl2(Ljava/lang/String;)V

    .line 234
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl3(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 236
    :cond_5
    invoke-virtual {v3, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setNewInStore(Z)V

    .line 238
    :cond_6
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    invoke-interface {v4, v8, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 209
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public populateSlideshowViewPager(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;)V
    .locals 1
    .param p1, "recommendQuery"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;

    .prologue
    .line 162
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;->getSlides()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSlideList:Ljava/util/List;

    .line 163
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    .line 164
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->trySetupList()V

    .line 165
    return-void
.end method

.method public populateStorePlaces(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "storePlaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mStorePlacesList:Ljava/util/List;

    .line 175
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const/4 v0, 0x4

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mCloudCallsCount:I

    .line 177
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->trySetupList()V

    .line 179
    :cond_0
    return-void
.end method

.method public showProgress()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method
