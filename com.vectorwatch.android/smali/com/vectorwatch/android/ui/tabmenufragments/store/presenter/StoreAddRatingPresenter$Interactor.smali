.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;
.super Ljava/lang/Object;
.source "StoreAddRatingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Interactor"
.end annotation


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;I)V
.end method

.method public abstract onRatingAdded(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V
.end method

.method public abstract onRatingsLoaded(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation
.end method
