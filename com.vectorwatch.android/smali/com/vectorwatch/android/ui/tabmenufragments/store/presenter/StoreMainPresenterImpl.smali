.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;
.super Ljava/lang/Object;
.source "StoreMainPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$View;
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mStoreMainInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractor;

.field private mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeView"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    .line 31
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractor;

    .line 32
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mContext:Landroid/content/Context;

    .line 34
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getStorePlaces()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainInteractor:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractor;

    invoke-interface {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractor;->getStorePlaces()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->populateStorePlaces(Ljava/util/List;)V

    .line 45
    :cond_0
    return-void
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->handleLinkStateChanged(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V

    .line 122
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    .line 53
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method public onRecommendedFail(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->hideProgress()V

    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->hideSlider()V

    .line 81
    :cond_0
    return-void
.end method

.method public onRecommendedSuccess(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;Lretrofit/client/Response;)V
    .locals 1
    .param p1, "serverResponse"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->hideProgress()V

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->populateSlideshowViewPager(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;)V

    .line 68
    :cond_0
    return-void
.end method

.method public onStoreQueryFailure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->populateImageList(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V

    .line 107
    :cond_0
    return-void
.end method

.method public onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 2
    .param p1, "storeQuery"    # Lcom/vectorwatch/android/models/StoreQuery;
    .param p2, "response"    # Lretrofit/client/Response;
    .param p3, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    new-instance v1, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;

    invoke-direct {v1, p3, p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;-><init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Lcom/vectorwatch/android/models/StoreQuery;)V

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->populateImageList(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V

    .line 95
    :cond_0
    return-void
.end method

.method public recommendedOfflineSlide(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V
    .locals 1
    .param p1, "slide"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenterImpl;->mStoreMainView:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;->onRecommendedOfflineSlide(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V

    .line 112
    return-void
.end method
