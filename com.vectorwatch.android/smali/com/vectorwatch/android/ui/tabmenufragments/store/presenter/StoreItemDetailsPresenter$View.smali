.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$View;
.super Ljava/lang/Object;
.source "StoreItemDetailsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract onActionClick(Lcom/vectorwatch/android/models/StoreElement;)V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V
.end method
