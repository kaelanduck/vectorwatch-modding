.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreView;
.super Ljava/lang/Object;
.source "StoreView.java"


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;I)V
.end method

.method public abstract handleAppInstallInterrupted(Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;)V
.end method

.method public abstract hideProgress()V
.end method

.method public abstract populateFacesListView(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
.end method

.method public abstract refreshList(Lcom/vectorwatch/android/events/AppListReloadEvent;)V
.end method

.method public abstract refreshStreamList(Lcom/vectorwatch/android/events/StreamListReloadEvent;)V
.end method

.method public abstract showProgress()V
.end method
