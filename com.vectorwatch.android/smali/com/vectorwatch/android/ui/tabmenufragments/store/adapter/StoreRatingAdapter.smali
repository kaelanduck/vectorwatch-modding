.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;
.super Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
.source "StoreRatingAdapter.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterBinder;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDateFormat:Ljava/text/SimpleDateFormat;

.field private mTimeFormat:Ljava/text/SimpleDateFormat;

.field public final ratings:I
    .annotation build Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        layout = 0x7f030083
        views = {
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100236
                name = "from"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10014b
                name = "comment"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f1000eb
                name = "rating"
                type = Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100238
                name = "date"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100237
                name = "time"
                type = Landroid/widget/TextView;
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p2, "storeRatingList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    invoke-direct {p0, p1}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->ratings:I

    .line 58
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mData:Ljava/util/List;

    .line 59
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mContext:Landroid/content/Context;

    .line 60
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mTimeFormat:Ljava/text/SimpleDateFormat;

    .line 61
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy/MM/dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mDateFormat:Ljava/text/SimpleDateFormat;

    .line 62
    return-void
.end method


# virtual methods
.method public bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;I)V
    .locals 4
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 86
    invoke-virtual {p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    move-result-object v1

    .line 87
    .local v1, "item":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->from:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->rating:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getRatingValue()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 89
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->comment:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :goto_0
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getCreationDate()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 97
    .local v0, "date":Ljava/util/Date;
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->date:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->time:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mTimeFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    return-void

    .line 92
    .end local v0    # "date":Ljava/util/Date;
    :cond_0
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->comment:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mData:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapter;->mData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->getId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method
