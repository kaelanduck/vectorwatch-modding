.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;
.super Ljava/lang/Object;
.source "RatingItem.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private comment:Ljava/lang/String;

.field private creationDate:J

.field private id:I

.field private name:Ljava/lang/String;

.field private ownRating:Z

.field private ratingValue:F


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;FZJ)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "comment"    # Ljava/lang/String;
    .param p4, "ratingValue"    # F
    .param p5, "ownRating"    # Z
    .param p6, "creationDate"    # J

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->id:I

    .line 17
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->name:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->comment:Ljava/lang/String;

    .line 19
    iput p4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->ratingValue:F

    .line 20
    iput-boolean p5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->ownRating:Z

    .line 21
    iput-wide p6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->creationDate:J

    .line 22
    return-void
.end method


# virtual methods
.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCreationDate()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->creationDate:J

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->id:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRatingValue()F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->ratingValue:F

    return v0
.end method

.method public isOwnRating()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->ownRating:Z

    return v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->comment:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setCreationDate(J)V
    .locals 1
    .param p1, "creationDate"    # J

    .prologue
    .line 69
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->creationDate:J

    .line 70
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->id:I

    .line 30
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->name:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setOwnRating(Z)V
    .locals 0
    .param p1, "ownRating"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->ownRating:Z

    .line 62
    return-void
.end method

.method public setRatingValue(F)V
    .locals 0
    .param p1, "ratingValue"    # F

    .prologue
    .line 53
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;->ratingValue:F

    .line 54
    return-void
.end method
