.class public Lcom/vectorwatch/android/ui/SetupWatchActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "SetupWatchActivity.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mButtonRetry:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100149
        }
    .end annotation
.end field

.field mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100145
        }
    .end annotation
.end field

.field private mHandlerReset:Landroid/os/Handler;

.field mInfoText:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100143
        }
    .end annotation
.end field

.field private mInstalledAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mRunnableReset:Ljava/lang/Runnable;

.field mUpdateProgress:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100147
        }
    .end annotation
.end field

.field mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100148
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/vectorwatch/android/ui/SetupWatchActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/SetupWatchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SetupWatchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/SetupWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SetupWatchActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->setupAppsCompleted()V

    return-void
.end method

.method private getListOfIds(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    .local p1, "fullAppsInOrder":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    if-nez p1, :cond_1

    .line 461
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 470
    :cond_0
    return-object v1

    .line 464
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 466
    .local v1, "appIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 467
    .local v0, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    iget-object v3, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private goToMain()V
    .locals 2

    .prologue
    .line 223
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 224
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 225
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 226
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->finish()V

    .line 227
    return-void
.end method

.method private resetViews()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 233
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInfoText:Landroid/widget/TextView;

    const v1, 0x7f09022a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 235
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 238
    return-void
.end method

.method private saveAppToDb()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 290
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 292
    .local v0, "appDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    new-instance v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 293
    .local v1, "summary":Lcom/vectorwatch/android/models/CloudElementSummary;
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 294
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 295
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setIsStream(Z)V

    .line 297
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 298
    iget v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->rating:F

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 299
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 300
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 302
    sget-object v2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SETUP: install = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 304
    sget-object v2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SETUP APPS: [setup] saved app = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 305
    sget v2, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    invoke-static {v0, v2, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->saveAppToDatabase(Lcom/vectorwatch/android/models/DownloadedAppDetails;ILandroid/content/Context;)V

    .line 306
    return-void
.end method

.method private setupAppsCompleted()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 372
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getListOfIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 374
    .local v2, "installedAppsIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->syncAppOrder(Ljava/util/List;)V

    .line 376
    invoke-static {v2, p0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updatePositionsForApps(Ljava/util/List;Landroid/content/Context;)V

    .line 378
    sput v4, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    .line 379
    sput v4, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 380
    const-string v3, "flag_sync_system_apps"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 382
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 384
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-static {v3, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->appContainsDefaultStreams(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 385
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-static {v3, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->setDefaultStreamsOnDefaultApp(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)V

    .line 382
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 388
    .end local v1    # "i":I
    :cond_1
    const-string v3, "flag_sync_defaults_to_cloud"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 394
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vectorwatch/android/managers/StreamsManager;->triggerRestoreStreams(Landroid/content/Context;)V

    .line 399
    :cond_2
    const-string v3, "flag_sync_defaults_to_cloud"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 401
    const-string v3, "flag_sync_system_apps"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 405
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 406
    const v3, 0x7f090093

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "VectorWatch"

    const v5, 0x7f0200e6

    const-class v6, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v3, v4, v5, p0, v6}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    .line 413
    :goto_1
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->checkCalendarEventsForSync(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 414
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getCalendarEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 415
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 420
    .end local v0    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 421
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->goToMain()V

    .line 422
    return-void

    .line 409
    :cond_4
    const v3, 0x7f0900aa

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "VectorWatch"

    const v5, 0x7f0200ea

    const-class v6, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v3, v4, v5, p0, v6}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1
.end method

.method private syncAppOrder(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 435
    .local p1, "appIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 436
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "APP INSTALL MANAGER - SETUP no apps to install."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 450
    :goto_0
    return-void

    .line 440
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "APP INSTALL MANAGER - trigger sync app order after setup complete"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 442
    const-string v1, ""

    .line 443
    .local v1, "listApps":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 444
    .local v0, "appId":Ljava/lang/Integer;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 445
    goto :goto_1

    .line 447
    .end local v0    # "appId":Ljava/lang/Integer;
    :cond_2
    sget-object v2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "APP INSTALL MANAGER - \n Apps in order = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 449
    invoke-static {p0, p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAppsOrder(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    goto :goto_0
.end method

.method private triggerErrorScreen(Ljava/lang/String;)V
    .locals 5
    .param p1, "errorText"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 244
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 245
    if-eqz p1, :cond_0

    .line 246
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v3}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    sput v4, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    .line 254
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private triggerUpdatesWithCloud()V
    .locals 3

    .prologue
    .line 425
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 426
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 427
    return-void
.end method

.method private updateProgressUi()V
    .locals 4

    .prologue
    .line 260
    sget v1, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    mul-int/lit8 v1, v1, 0x64

    sget v2, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    div-int v0, v1, v2

    .line 261
    .local v0, "progress":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v0}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setProgress(I)V

    .line 263
    return-void
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 213
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 215
    :cond_0
    return-void
.end method

.method public handleInstallAppEvent(Lcom/vectorwatch/android/events/InstallAppEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/vectorwatch/android/events/InstallAppEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 145
    sget-object v1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SETUP: Received install app event / app type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/InstallAppEvent;->getInstallAppType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mHandlerReset:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mRunnableReset:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 148
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/InstallAppEvent;->getInstallAppType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 173
    :goto_0
    return-void

    .line 150
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 151
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v4}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 152
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    .line 155
    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getListOfIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 157
    .local v0, "appIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->syncAppOrder(Ljava/util/List;)V

    .line 158
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->initProgress()V

    goto :goto_0

    .line 161
    .end local v0    # "appIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :pswitch_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 162
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v4}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 163
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->updateProgress()V

    goto :goto_0

    .line 167
    :pswitch_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/InstallAppEvent;->getInstallErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :pswitch_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerSkipAppInstall()V

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public handleInternetConnectionEnabledEvent(Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public handleOfflineInstallCompleteEvent(Lcom/vectorwatch/android/events/OfflineInstallCompleteEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/OfflineInstallCompleteEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 184
    sput v2, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    .line 185
    sput v2, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 189
    const-string v1, "flag_sync_defaults_to_cloud"

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 191
    const-string v1, "flag_sync_system_apps"

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 194
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->checkCalendarEventsForSync(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getCalendarEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 196
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 201
    .end local v0    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 202
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->goToMain()V

    .line 203
    return-void
.end method

.method public handleReceiveAppEvent(Lcom/vectorwatch/android/events/ReceivedAppOrderEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/ReceivedAppOrderEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mHandlerReset:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mRunnableReset:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 179
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/ReceivedAppOrderEvent;->getData()[B

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->getAppListOffline([B)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->installAppsToDb(Landroid/content/Context;Ljava/util/List;)V

    .line 180
    return-void
.end method

.method public initProgress()V
    .locals 4

    .prologue
    .line 269
    sget v1, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    if-lez v1, :cond_1

    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 270
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 271
    sget v1, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    mul-int/lit8 v1, v1, 0x64

    sget v2, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    div-int v0, v1, v2

    .line 272
    .local v0, "progress":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v0}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setProgress(I)V

    .line 275
    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->saveAppToDb()V

    .line 282
    .end local v0    # "progress":I
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090116

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    .line 280
    sget-object v1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "Init progress failed list null or total 0"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->moveTaskToBack(Z)Z

    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x8

    .line 69
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v0, 0x7f030030

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->setContentView(I)V

    .line 71
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 72
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mHandlerReset:Landroid/os/Handler;

    .line 77
    new-instance v0, Lcom/vectorwatch/android/ui/SetupWatchActivity$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity$1;-><init>(Lcom/vectorwatch/android/ui/SetupWatchActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mRunnableReset:Ljava/lang/Runnable;

    .line 86
    sget-boolean v0, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    .line 93
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 99
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    const v0, 0x7f090093

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "VectorWatch"

    const v2, 0x7f0200e6

    const-class v3, Lcom/vectorwatch/android/ui/SetupWatchActivity;

    invoke-static {v0, v1, v2, p0, v3}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    .line 107
    :goto_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mHandlerReset:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mRunnableReset:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 109
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->getAppOrder(Landroid/content/Context;)Ljava/util/UUID;

    .line 112
    :cond_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mButtonRetry:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInfoText:Landroid/widget/TextView;

    const v1, 0x7f09022a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 103
    :cond_2
    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "VectorWatch"

    const v2, 0x7f0200ea

    const-class v3, Lcom/vectorwatch/android/ui/SetupWatchActivity;

    invoke-static {v0, v1, v2, p0, v3}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 117
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 118
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 119
    return-void
.end method

.method public retryButton()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100149
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 130
    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 131
    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 132
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mHandlerReset:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mRunnableReset:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 133
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->resetViews()V

    .line 134
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->getAppOrder(Landroid/content/Context;)Ljava/util/UUID;

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerUpdatesWithCloud()V

    goto :goto_0
.end method

.method public triggerSkipAppInstall()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 313
    sget v0, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    if-lez v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    .line 314
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 315
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->updateProgressUi()V

    .line 317
    sget-object v1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SETUP: Skipped app = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 318
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->hardDeleteAppByUuid(Ljava/lang/String;Landroid/content/Context;)V

    .line 319
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 320
    sget v0, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 328
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 329
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->saveAppToDb()V

    .line 338
    :goto_0
    return-void

    .line 322
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "Init progress failed list null or total 0"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 331
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/ui/SetupWatchActivity$2;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity$2;-><init>(Lcom/vectorwatch/android/ui/SetupWatchActivity;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public updateProgress()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 344
    sget v0, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    if-lez v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    .line 345
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 346
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->updateProgressUi()V

    .line 348
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInstalledAppList:Ljava/util/List;

    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 350
    sget v0, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 356
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 357
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->saveAppToDb()V

    .line 366
    :goto_0
    return-void

    .line 352
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/ui/SetupWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "Init progress failed list null or total 0"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 353
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/SetupWatchActivity;->triggerErrorScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/ui/SetupWatchActivity$3;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/SetupWatchActivity$3;-><init>(Lcom/vectorwatch/android/ui/SetupWatchActivity;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
