.class public Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;
.super Landroid/app/Fragment;
.source "StoreSearchActivityFragment.java"


# static fields
.field public static final EXTRA_ACTION_UPDATE_AUTH_CREDENTIALS:Ljava/lang/String; = "updateAuthCredentials"

.field public static final EXTRA_KEY_ACTION:Ljava/lang/String; = "action"

.field public static final EXTRA_KEY_APP_ID:Ljava/lang/String; = "mAppId"

.field private static final REQUEST_CODE_APP_AUTHENTICATION:I = 0x66

.field private static final REQUEST_CODE_APP_SETTINGS:I = 0x65

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private emptyText:Landroid/widget/TextView;

.field private isPaused:Z

.field private isUpdatingAppCredentials:Z

.field private isUpdatingAppId:I

.field private mAccountManager:Landroid/accounts/AccountManager;

.field public mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

.field mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

.field mStoreWatchfacesListView:Landroid/widget/ListView;

.field public mWatchFaceArrayList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation
.end field

.field private progressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->isUpdatingAppCredentials:Z

    .line 54
    iput v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->isUpdatingAppId:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mWatchFaceArrayList:Ljava/util/List;

    .line 65
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->isPaused:Z

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->progressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    return-object v0
.end method


# virtual methods
.method public handleAppListReloadEvent(Lcom/vectorwatch/android/events/AppListReloadEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_1

    .line 212
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->onOperationFinished(Ljava/lang/String;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 217
    :cond_1
    return-void
.end method

.method public handleLoadedStoreElementsEvent(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
    .locals 7
    .param p1, "event"    # Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->log:Lorg/slf4j/Logger;

    const-string v1, "Handle loaded store elements."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getOption()Lcom/vectorwatch/android/models/cloud/AppsOption;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->ALL:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne v0, v1, :cond_2

    .line 129
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->getStoreElements()Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mWatchFaceArrayList:Ljava/util/List;

    .line 130
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v6

    .line 131
    .local v6, "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_0

    sget v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentPage:I

    if-nez v0, :cond_4

    .line 132
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mWatchFaceArrayList:Ljava/util/List;

    const v3, 0x7f030086

    new-instance v4, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;

    invoke-direct {v4, p0, v6}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;-><init>(Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;Lcom/vectorwatch/android/utils/AppInstallManager;)V

    const-string v5, "store_search"

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;ILcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .line 193
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 194
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    new-instance v1, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    invoke-direct {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mWatchFaceArrayList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mWatchFaceArrayList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->emptyText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatus(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->emptyText:Landroid/widget/TextView;

    const v1, 0x7f09010f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    .end local v6    # "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    :cond_2
    :goto_0
    return-void

    .line 201
    .restart local v6    # "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->emptyText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 204
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mWatchFaceArrayList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->addItems(Ljava/util/List;)V

    goto :goto_0
.end method

.method public handleStreamListReloadEvent(Lcom/vectorwatch/android/events/StreamListReloadEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    if-eqz v0, :cond_1

    .line 222
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamListReloadEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamListReloadEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->onOperationFinished(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->setAllStreams(Ljava/util/List;)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 228
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mAccountManager:Landroid/accounts/AccountManager;

    .line 76
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 89
    const v1, 0x7f030062

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 90
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 82
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 121
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setProgressBar(Landroid/view/View;)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->isPaused:Z

    .line 123
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 114
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->progressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setProgressBar(Landroid/view/View;)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->isPaused:Z

    .line 116
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 96
    const v0, 0x7f1001d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    .line 97
    const v0, 0x7f1000f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->progressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 98
    const v0, 0x7f100110

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->emptyText:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    new-instance v1, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mListViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    invoke-direct {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mStoreWatchfacesListView:Landroid/widget/ListView;

    new-instance v1, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;

    new-instance v2, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;-><init>(Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;)V

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;-><init>(Lcom/vectorwatch/android/ui/store/OnEndListCallback;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 109
    return-void
.end method
