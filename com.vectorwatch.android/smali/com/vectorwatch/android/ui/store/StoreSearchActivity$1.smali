.class Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;
.super Ljava/lang/Object;
.source "StoreSearchActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

.field final synthetic val$searchItem:Landroid/view/MenuItem;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->val$searchItem:Landroid/view/MenuItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 8
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 57
    sput v6, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentPage:I

    .line 58
    sput-object p1, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentQuery:Ljava/lang/String;

    .line 60
    iget-object v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STORE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SEARCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentQuery:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentQuery:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v2, v3, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    # getter for: Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->access$000(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;)Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    # getter for: Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->access$000(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;)Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->cancel(Z)Z

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    new-instance v1, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    sget-object v2, Lcom/vectorwatch/android/models/cloud/AppsOption;->ALL:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    const/4 v4, 0x0

    sget-object v5, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentQuery:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;-><init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Landroid/app/Activity;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Ljava/lang/String;)V

    # setter for: Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->access$002(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;)Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    .line 68
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    # getter for: Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->access$000(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;)Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    :cond_1
    return v7

    .line 60
    :cond_2
    const-string v0, "null"

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;->val$searchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    .line 78
    const/4 v0, 0x0

    return v0
.end method
