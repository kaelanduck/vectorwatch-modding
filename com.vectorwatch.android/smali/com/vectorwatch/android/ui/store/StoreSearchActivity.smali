.class public Lcom/vectorwatch/android/ui/store/StoreSearchActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StoreSearchActivity.java"


# static fields
.field public static mCurrentPage:I

.field public static mCurrentQuery:Ljava/lang/String;


# instance fields
.field private mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

.field private mMenu:Landroid/view/Menu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;)Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;)Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/store/StoreSearchActivity;
    .param p1, "x1"    # Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    return-object p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f030036

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->setContentView(I)V

    .line 32
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 33
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v5

    const v6, 0x7f110007

    invoke-virtual {v5, v6, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mMenu:Landroid/view/Menu;

    .line 41
    const v5, 0x7f1002ed

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 42
    .local v2, "searchItem":Landroid/view/MenuItem;
    invoke-static {v2}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/SearchView;

    .line 43
    .local v4, "searchView":Landroid/support/v7/widget/SearchView;
    const v5, 0x104000c

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 45
    const-string v5, "search"

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchManager;

    .line 46
    .local v3, "searchManager":Landroid/app/SearchManager;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    .line 48
    .local v0, "info":Landroid/app/SearchableInfo;
    invoke-virtual {v4, v0}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 50
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 53
    new-instance v1, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity$1;-><init>(Lcom/vectorwatch/android/ui/store/StoreSearchActivity;Landroid/view/MenuItem;)V

    .line 82
    .local v1, "queryTextListener":Landroid/support/v7/widget/SearchView$OnQueryTextListener;
    invoke-virtual {v4, v1}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 83
    invoke-static {v2}, Landroid/support/v4/view/MenuItemCompat;->expandActionView(Landroid/view/MenuItem;)Z

    .line 85
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v5

    return v5
.end method

.method protected onStop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onStop()V

    .line 91
    invoke-virtual {p0, v0, v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->overridePendingTransition(II)V

    .line 92
    return-void
.end method
