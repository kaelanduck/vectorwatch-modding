.class public interface abstract Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;
.super Ljava/lang/Object;
.source "ChartPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract fetchData()V
.end method

.method public abstract getDataInterval()I
.end method

.method public abstract getDataType()I
.end method

.method public abstract getLimitFormatText()Ljava/text/DecimalFormat;
.end method

.method public abstract onButtonLeft()V
.end method

.method public abstract onButtonRight()V
.end method

.method public abstract onChartTypeChange(I)V
.end method

.method public abstract onStop()V
.end method

.method public abstract onTabSelected(Ljava/lang/Integer;)V
.end method
