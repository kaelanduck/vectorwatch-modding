.class public interface abstract Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;
.super Ljava/lang/Object;
.source "ChartPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Interactor"
.end annotation


# virtual methods
.method public abstract getDataInterval()I
.end method

.method public abstract getDataType()I
.end method

.method public abstract getNumberOfPagesScrolled()I
.end method

.method public abstract onLoadFinished(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;)V
.end method

.method public abstract setInterval(Ljava/util/Calendar;JJ)V
.end method

.method public abstract setNoData()V
.end method

.method public abstract setNoPersonalData()V
.end method

.method public abstract setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V
.end method
