.class public Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;
.super Ljava/lang/Object;
.source "ChartPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;
.implements Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;


# instance fields
.field private mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

.field private mContext:Landroid/content/Context;

.field private mInteractor:Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;

.field private mNumberOfPagesScrolled:I

.field private mSelectedDataInterval:I

.field private mSelectedDataType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/chart/view/ChartView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartsView"    # Lcom/vectorwatch/android/ui/chart/view/ChartView;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mContext:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    .line 33
    new-instance v0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;

    invoke-direct {v0, p1, p0}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    .line 35
    return-void
.end method


# virtual methods
.method public fetchData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->showProgress()V

    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;

    const/4 v1, 0x1

    invoke-interface {v0, v1, v2, v2}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;->fetchData(ZLjava/util/List;Ljava/util/List;)V

    .line 44
    return-void
.end method

.method public getDataInterval()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    return v0
.end method

.method public getLimitFormatText()Ljava/text/DecimalFormat;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 83
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v0

    .line 84
    .local v0, "format":Ljava/text/DecimalFormat;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setGroupingSize(I)V

    .line 85
    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    .line 86
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    if-nez v1, :cond_0

    .line 87
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 96
    :goto_0
    return-object v0

    .line 88
    :cond_0
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    if-ne v1, v2, :cond_1

    .line 89
    const-string v1, "#0.00"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_1
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 91
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_2
    const-string v1, "#00"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getNumberOfPagesScrolled()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    return v0
.end method

.method public onButtonLeft()V
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    .line 64
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->fetchData()V

    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->updateArrowsView(I)V

    .line 66
    return-void
.end method

.method public onButtonRight()V
    .locals 2

    .prologue
    .line 53
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    if-lez v0, :cond_0

    .line 54
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->fetchData()V

    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->updateArrowsView(I)V

    .line 59
    return-void
.end method

.method public onChartTypeChange(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    .line 71
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->fetchData()V

    .line 72
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;>;"
    .local p2, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->hideProgress()V

    .line 103
    const/4 v5, 0x0

    .line 104
    .local v5, "average":F
    const/4 v6, 0x0

    .line 105
    .local v6, "count":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_3

    .line 106
    invoke-virtual {p2, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 108
    .local v8, "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v8}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v0

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_1

    .line 109
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 111
    :cond_0
    invoke-virtual {v8}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 112
    invoke-virtual {v8}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v0

    add-float/2addr v5, v0

    .line 113
    add-int/lit8 v6, v6, 0x1

    .line 105
    :cond_1
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v8}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 117
    invoke-virtual {v8}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v0

    add-float/2addr v5, v0

    .line 118
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 123
    .end local v8    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_3
    if-lez v6, :cond_4

    .line 124
    int-to-float v0, v6

    div-float/2addr v5, v0

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    iget v3, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->onDataFetched(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;IIF)V

    .line 127
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mInteractor:Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;->onStop()V

    .line 49
    return-void
.end method

.method public onTabSelected(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "interval"    # Ljava/lang/Integer;

    .prologue
    .line 76
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    .line 78
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->fetchData()V

    .line 79
    return-void
.end method

.method public setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;)V
    .locals 6
    .param p1, "formater"    # Ljava/text/SimpleDateFormat;
    .param p2, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    iget v3, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    iget v5, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;III)V

    .line 138
    return-void
.end method

.method public setInterval(Ljava/util/Calendar;JJ)V
    .locals 8
    .param p1, "calendar"    # Ljava/util/Calendar;
    .param p2, "fStartDate"    # J
    .param p4, "fEndDate"    # J

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    iget v6, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    iget v7, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mNumberOfPagesScrolled:I

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v7}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->setInterval(Ljava/util/Calendar;JJII)V

    .line 163
    return-void
.end method

.method public setNoData()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->setNoData()V

    .line 171
    return-void
.end method

.method public setNoPersonalData()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->setNoPersonalData()V

    .line 191
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->hideProgress()V

    .line 192
    return-void
.end method

.method public setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "format"    # Ljava/text/DecimalFormat;
    .param p2, "finalValue"    # F
    .param p3, "distanceInd2"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mChartsView:Lcom/vectorwatch/android/ui/chart/view/ChartView;

    iget v5, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataInterval:I

    iget v6, p0, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;->mSelectedDataType:I

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/ui/chart/view/ChartView;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;II)V

    .line 151
    return-void
.end method
