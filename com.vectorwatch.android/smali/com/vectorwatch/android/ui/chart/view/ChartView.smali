.class public interface abstract Lcom/vectorwatch/android/ui/chart/view/ChartView;
.super Ljava/lang/Object;
.source "ChartView.java"


# virtual methods
.method public abstract displayAlert(Ljava/lang/String;I)V
.end method

.method public abstract hideProgress()V
.end method

.method public abstract onDataFetched(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;IIF)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;IIF)V"
        }
    .end annotation
.end method

.method public abstract setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;III)V
.end method

.method public abstract setInterval(Ljava/util/Calendar;JJII)V
.end method

.method public abstract setNoData()V
.end method

.method public abstract setNoPersonalData()V
.end method

.method public abstract setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract showProgress()V
.end method

.method public abstract updateArrowsView(I)V
.end method
