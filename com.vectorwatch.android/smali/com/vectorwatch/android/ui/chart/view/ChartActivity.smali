.class public Lcom/vectorwatch/android/ui/chart/view/ChartActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "ChartActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/chart/view/ChartView;
.implements Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/support/design/widget/TabLayout$OnTabSelectedListener;


# static fields
.field private static final log:Lorg/slf4j/Logger;

.field private static sCaloriesBaseline:D

.field private static sHasBase:Z


# instance fields
.field private final MONTH_INTERVAL_COUNT:I

.field private final WEEK_INTERVAL_COUNT:I

.field private final YEAR_INTERVAL_COUNT:I

.field private avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

.field private baseLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

.field centerDate:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000a7
        }
    .end annotation
.end field

.field private currentApiVersion:I

.field private final flags:I

.field final formatAMPM:Ljava/text/SimpleDateFormat;

.field private goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

.field leftDate:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000a6
        }
    .end annotation
.end field

.field mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000aa
        }
    .end annotation
.end field

.field mBtPageLeft:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000af
        }
    .end annotation
.end field

.field mBtPageRight:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000b0
        }
    .end annotation
.end field

.field final mDateFormatter:Ljava/text/SimpleDateFormat;

.field private mIsInForegroundMode:Z

.field private mIsSensorDelay:Z

.field mNoDataTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000b1
        }
    .end annotation
.end field

.field private mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

.field private mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

.field private mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

.field mTabLayout:Landroid/support/design/widget/TabLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000a9
        }
    .end annotation
.end field

.field radioButtons:[Landroid/widget/RadioButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000b2,
            0x7f1000b4,
            0x7f1000b3,
            0x7f1000b5
        }
    .end annotation
.end field

.field rightDate:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000a8
        }
    .end annotation
.end field

.field txtTotalInfo:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1000ac
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 67
    invoke-static {}, Ljava/text/SimpleDateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mDateFormatter:Ljava/text/SimpleDateFormat;

    .line 68
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm aa"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->formatAMPM:Ljava/text/SimpleDateFormat;

    .line 69
    const/16 v0, 0x1706

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->flags:I

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsSensorDelay:Z

    .line 103
    const/16 v0, 0x8

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->WEEK_INTERVAL_COUNT:I

    .line 104
    const/16 v0, 0x1e

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->MONTH_INTERVAL_COUNT:I

    .line 105
    const/16 v0, 0xc

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->YEAR_INTERVAL_COUNT:I

    return-void
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/chart/view/ChartActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsSensorDelay:Z

    return p1
.end method

.method private addAvgLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V
    .locals 2
    .param p1, "avgLineLimit"    # Lcom/github/mikephil/charting/components/LimitLine;
    .param p2, "leftAxis"    # Lcom/github/mikephil/charting/components/YAxis;

    .prologue
    const/4 v1, -0x1

    .line 968
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 969
    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 970
    sget-object v0, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 971
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/components/LimitLine;->setTextSize(F)V

    .line 972
    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/components/LimitLine;->setTextColor(I)V

    .line 974
    invoke-virtual {p2, p1}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 975
    return-void
.end method

.method private addBaseLineLimit(FLjava/text/DecimalFormat;Lcom/github/mikephil/charting/components/YAxis;)V
    .locals 5
    .param p1, "baseLineValue"    # F
    .param p2, "formatF"    # Ljava/text/DecimalFormat;
    .param p3, "leftAxis"    # Lcom/github/mikephil/charting/components/YAxis;

    .prologue
    const v4, -0x333334

    .line 950
    sget-boolean v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sHasBase:Z

    if-eqz v1, :cond_0

    .line 951
    new-instance v0, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090072

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    float-to-double v2, p1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    .line 952
    .local v0, "baseLineLimit":Lcom/github/mikephil/charting/components/LimitLine;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 953
    invoke-virtual {v0, v4}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 954
    sget-object v1, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->RIGHT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 955
    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/LimitLine;->setTextSize(F)V

    .line 956
    invoke-virtual {v0, v4}, Lcom/github/mikephil/charting/components/LimitLine;->setTextColor(I)V

    .line 957
    invoke-virtual {p3, v0}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 959
    .end local v0    # "baseLineLimit":Lcom/github/mikephil/charting/components/LimitLine;
    :cond_0
    return-void
.end method

.method private addGoalLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V
    .locals 4
    .param p1, "goalLineLimit"    # Lcom/github/mikephil/charting/components/LimitLine;
    .param p2, "leftAxis"    # Lcom/github/mikephil/charting/components/YAxis;

    .prologue
    const/4 v3, -0x1

    const/high16 v2, 0x41200000    # 10.0f

    .line 930
    if-eqz p1, :cond_0

    .line 931
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 932
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 933
    const/high16 v0, 0x41700000    # 15.0f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v1}, Lcom/github/mikephil/charting/components/LimitLine;->enableDashedLine(FFF)V

    .line 934
    sget-object v0, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 935
    invoke-virtual {p1, v2}, Lcom/github/mikephil/charting/components/LimitLine;->setTextSize(F)V

    .line 936
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/components/LimitLine;->setTextColor(I)V

    .line 938
    invoke-virtual {p2, p1}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 940
    :cond_0
    return-void
.end method

.method private fillMissingData(Landroid/util/SparseArray;I)Landroid/util/SparseArray;
    .locals 20
    .param p2, "mSelectedDataInterval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;I)",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    const/4 v12, 0x1

    .line 417
    .local v12, "notOk":Z
    :cond_0
    if-eqz v12, :cond_c

    .line 418
    const/4 v12, 0x0

    .line 419
    const/4 v15, 0x1

    move/from16 v0, p2

    if-ne v0, v15, :cond_4

    .line 420
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v3, v15, :cond_2

    .line 421
    add-int/lit8 v15, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v18

    sub-long v16, v16, v18

    const-wide/32 v18, 0xa15c980

    cmp-long v15, v16, v18

    if-lez v15, :cond_3

    .line 422
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v8, v15, -0x1

    .local v8, "k":I
    :goto_1
    if-le v8, v3, :cond_1

    .line 423
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 424
    .local v13, "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v15

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 425
    add-int/lit8 v15, v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 422
    add-int/lit8 v8, v8, -0x1

    goto :goto_1

    .line 428
    .end local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_1
    new-instance v9, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    add-int/lit8 v15, v3, 0x1

    int-to-float v15, v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v9, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 429
    .local v9, "newPoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 430
    add-int/lit8 v15, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v9}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 432
    const/4 v12, 0x1

    .line 437
    .end local v8    # "k":I
    .end local v9    # "newPoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_2
    if-nez v12, :cond_0

    .line 438
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 439
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 440
    .local v2, "dataSize":I
    const/4 v3, 0x0

    :goto_2
    rsub-int/lit8 v15, v2, 0x8

    if-ge v3, v15, :cond_0

    .line 441
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 442
    .local v14, "referencePoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    new-instance v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v15

    int-to-float v0, v3

    move/from16 v16, v0

    add-float v15, v15, v16

    const/high16 v16, 0x3f800000    # 1.0f

    add-float v15, v15, v16

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v13, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 443
    .restart local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v13, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 444
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/2addr v15, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 440
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 420
    .end local v2    # "dataSize":I
    .end local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v14    # "referencePoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 448
    .end local v3    # "i":I
    :cond_4
    const/4 v15, 0x2

    move/from16 v0, p2

    if-ne v0, v15, :cond_8

    .line 449
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v3, v15, :cond_6

    .line 450
    add-int/lit8 v15, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v18

    sub-long v16, v16, v18

    const-wide/32 v18, 0xa15c980

    cmp-long v15, v16, v18

    if-lez v15, :cond_7

    .line 451
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v8, v15, -0x1

    .restart local v8    # "k":I
    :goto_4
    if-le v8, v3, :cond_5

    .line 452
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 453
    .restart local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v15

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 454
    add-int/lit8 v15, v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 451
    add-int/lit8 v8, v8, -0x1

    goto :goto_4

    .line 457
    .end local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_5
    new-instance v9, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    add-int/lit8 v15, v3, 0x1

    int-to-float v15, v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v9, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 458
    .restart local v9    # "newPoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 459
    add-int/lit8 v15, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v9}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 461
    const/4 v12, 0x1

    .line 466
    .end local v8    # "k":I
    .end local v9    # "newPoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_6
    if-nez v12, :cond_0

    .line 467
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    const/16 v16, 0x1e

    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 468
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 469
    .restart local v2    # "dataSize":I
    const/4 v3, 0x0

    :goto_5
    rsub-int/lit8 v15, v2, 0x1e

    if-ge v3, v15, :cond_0

    .line 470
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 471
    .restart local v14    # "referencePoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    new-instance v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v15

    int-to-float v0, v3

    move/from16 v16, v0

    add-float v15, v15, v16

    const/high16 v16, 0x3f800000    # 1.0f

    add-float v15, v15, v16

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v13, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 472
    .restart local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v13, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 473
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/2addr v15, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 469
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 449
    .end local v2    # "dataSize":I
    .end local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v14    # "referencePoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 477
    .end local v3    # "i":I
    :cond_8
    const/4 v15, 0x3

    move/from16 v0, p2

    if-ne v0, v15, :cond_0

    .line 478
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_6
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v3, v15, :cond_a

    .line 479
    add-int/lit8 v15, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v18

    sub-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 480
    .local v4, "diff":J
    const-wide v6, 0xa9f1dc00L

    .line 481
    .local v6, "intervalValue":J
    cmp-long v15, v4, v6

    if-lez v15, :cond_b

    .line 482
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v8, v15, -0x1

    .restart local v8    # "k":I
    :goto_7
    if-le v8, v3, :cond_9

    .line 483
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 484
    .restart local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v15

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 485
    add-int/lit8 v15, v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 482
    add-int/lit8 v8, v8, -0x1

    goto :goto_7

    .line 488
    .end local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_9
    new-instance v9, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    add-int/lit8 v15, v3, 0x1

    int-to-float v15, v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v9, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 489
    .restart local v9    # "newPoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    const-wide v18, 0x9a7ec800L

    add-long v10, v16, v18

    .line 490
    .local v10, "newTimestamp":J
    invoke-virtual {v9, v10, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 491
    add-int/lit8 v15, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v9}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 493
    const/4 v12, 0x1

    .line 498
    .end local v4    # "diff":J
    .end local v6    # "intervalValue":J
    .end local v8    # "k":I
    .end local v9    # "newPoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v10    # "newTimestamp":J
    :cond_a
    if-nez v12, :cond_0

    .line 499
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    const/16 v16, 0xc

    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 500
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 501
    .restart local v2    # "dataSize":I
    const/4 v3, 0x0

    :goto_8
    rsub-int/lit8 v15, v2, 0xc

    if-ge v3, v15, :cond_0

    .line 502
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 503
    .restart local v14    # "referencePoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    new-instance v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v15

    int-to-float v0, v3

    move/from16 v16, v0

    add-float v15, v15, v16

    const/high16 v16, 0x3f800000    # 1.0f

    add-float v15, v15, v16

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v13, v15, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 504
    .restart local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v16

    const-wide v18, 0x9a7ec800L

    add-long v16, v16, v18

    const-wide/32 v18, 0x5265c00

    add-long v10, v16, v18

    .line 506
    .restart local v10    # "newTimestamp":J
    invoke-virtual {v13, v10, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 507
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v15

    add-int/2addr v15, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 501
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 478
    .end local v2    # "dataSize":I
    .end local v10    # "newTimestamp":J
    .end local v13    # "point":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v14    # "referencePoint":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .restart local v4    # "diff":J
    .restart local v6    # "intervalValue":J
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    .line 514
    .end local v3    # "i":I
    .end local v4    # "diff":J
    .end local v6    # "intervalValue":J
    :cond_c
    return-object p1
.end method

.method private getDataLabels(Landroid/util/SparseArray;II)Ljava/util/List;
    .locals 12
    .param p2, "mSelectedDataInterval"    # I
    .param p3, "mSelectedDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;II)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/AxisValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 876
    .local p1, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 878
    .local v6, "xAxisValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/AxisValue;>;"
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    .line 879
    .local v5, "tz":Ljava/util/TimeZone;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 880
    .local v2, "now":Ljava/util/Date;
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    .line 884
    .local v3, "offsetFromUtc":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-ge v0, v7, :cond_3

    .line 885
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 886
    .local v4, "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mDateFormatter:Ljava/text/SimpleDateFormat;

    new-instance v8, Ljava/util/Date;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 888
    .local v1, "label":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 889
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->is24hFormat(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 890
    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->formatAMPM:Ljava/text/SimpleDateFormat;

    new-instance v8, Ljava/util/Date;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 892
    :cond_0
    const/4 v7, 0x3

    if-ne p3, v7, :cond_1

    .line 893
    new-instance v7, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v8

    invoke-direct {v7, v8}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;-><init>(F)V

    invoke-virtual {v7, v1}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->setLabel(Ljava/lang/String;)Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 884
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 895
    :cond_1
    new-instance v7, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v8

    invoke-direct {v7, v8}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;-><init>(F)V

    invoke-virtual {v7, v1}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->setLabel(Ljava/lang/String;)Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 898
    :cond_2
    new-instance v7, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v8

    invoke-direct {v7, v8}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;-><init>(F)V

    invoke-virtual {v7, v1}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->setLabel(Ljava/lang/String;)Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 902
    .end local v1    # "label":Ljava/lang/String;
    .end local v4    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_3
    return-object v6
.end method

.method private init(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 754
    invoke-static {}, Lcom/vectorwatch/android/utils/MenuConfig;->getInstance()Lcom/vectorwatch/android/utils/MenuConfig;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vectorwatch/android/utils/MenuConfig;->getChartsTabMenu(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 755
    .local v1, "menuData":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 756
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v5}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v5

    aget-object v6, v1, v0

    invoke-virtual {v5, v6}, Landroid/support/design/widget/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v5

    invoke-static {v0}, Lcom/vectorwatch/android/ui/chart/util/ChartUtil;->getInterval(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/design/widget/TabLayout$Tab;->setTag(Ljava/lang/Object;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 755
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 759
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageRight:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 760
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 761
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v3, p0}, Landroid/support/design/widget/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 762
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    array-length v6, v5

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_1

    aget-object v2, v5, v3

    .line 763
    .local v2, "radioButton":Landroid/widget/RadioButton;
    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 762
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 765
    .end local v2    # "radioButton":Landroid/widget/RadioButton;
    :cond_1
    if-nez p1, :cond_2

    .line 766
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    aget-object v3, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 769
    :cond_2
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->initChart()V

    .line 770
    return-void
.end method

.method private initChart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 773
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Ljava/lang/String;)V

    .line 774
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/LineChart;->getLegend()Lcom/github/mikephil/charting/components/Legend;

    move-result-object v1

    new-array v2, v4, [I

    new-array v3, v4, [Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/github/mikephil/charting/components/Legend;->setCustom([I[Ljava/lang/String;)V

    .line 776
    new-instance v0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;

    const v1, 0x7f030047

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-direct {v0, p0, v1, v2}, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;-><init>(Landroid/content/Context;ILcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;)V

    .line 779
    .local v0, "mv":Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1, v0}, Lcom/github/mikephil/charting/charts/LineChart;->setMarkerView(Lcom/github/mikephil/charting/components/MarkerView;)V

    .line 781
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/charts/LineChart;->setPinchZoom(Z)V

    .line 782
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/charts/LineChart;->setDoubleTapToZoomEnabled(Z)V

    .line 783
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/charts/LineChart;->setScaleXEnabled(Z)V

    .line 784
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/charts/LineChart;->setScaleYEnabled(Z)V

    .line 785
    return-void
.end method

.method private initChartAxis()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 813
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v1

    .line 814
    .local v1, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/components/XAxis;->setTextSize(F)V

    .line 815
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/components/XAxis;->setTextColor(I)V

    .line 816
    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/components/XAxis;->setDrawGridLines(Z)V

    .line 817
    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/components/XAxis;->setDrawAxisLine(Z)V

    .line 818
    sget-object v2, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTTOM:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 820
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v0

    .line 821
    .local v0, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/YAxis;->setTextColor(I)V

    .line 822
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 823
    invoke-virtual {v0, v4}, Lcom/github/mikephil/charting/components/YAxis;->setGranularityEnabled(Z)V

    .line 824
    return-void
.end method

.method private initDataSet(Lcom/github/mikephil/charting/data/LineDataSet;)V
    .locals 5
    .param p1, "set"    # Lcom/github/mikephil/charting/data/LineDataSet;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const v2, 0x7f0f00b2

    .line 794
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 795
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 796
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighLightColor(I)V

    .line 797
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 798
    invoke-virtual {p1, v4}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 799
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCubic(Z)V

    .line 800
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 801
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 802
    invoke-virtual {p1, v4}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 803
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextColor(I)V

    .line 804
    const v1, 0x7f0200d1

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 805
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setFillDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 806
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawFilled(Z)V

    .line 807
    return-void
.end method

.method private setListenersEnable(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 982
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 983
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    aget-object v2, v1, v0

    if-eqz p1, :cond_0

    move-object v1, p0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 982
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 983
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 985
    :cond_1
    return-void
.end method

.method private setSleepChartLimits(Lcom/github/mikephil/charting/components/YAxis;)V
    .locals 13
    .param p1, "leftAxis"    # Lcom/github/mikephil/charting/components/YAxis;

    .prologue
    const v12, 0x7f090233

    const/4 v11, 0x0

    const/high16 v10, 0x41700000    # 15.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, -0x1

    .line 832
    invoke-virtual {p1}, Lcom/github/mikephil/charting/components/YAxis;->removeAllLimitLines()V

    .line 833
    new-instance v2, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f09009a

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v12}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v10, v5}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    .line 834
    .local v2, "deep":Lcom/github/mikephil/charting/components/LimitLine;
    invoke-virtual {v2, v9}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 835
    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 836
    sget-object v5, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual {v2, v5}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 837
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 838
    invoke-virtual {v2, v10}, Lcom/github/mikephil/charting/components/LimitLine;->setTextSize(F)V

    .line 839
    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setTextColor(I)V

    .line 840
    new-instance v3, Lcom/github/mikephil/charting/components/LimitLine;

    const/high16 v5, 0x42340000    # 45.0f

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f090196

    invoke-virtual {p0, v7}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v12}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    .line 841
    .local v3, "light":Lcom/github/mikephil/charting/components/LimitLine;
    invoke-virtual {v3, v9}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 842
    invoke-virtual {v3, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 843
    sget-object v5, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual {v3, v5}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 844
    invoke-virtual {v3, v11}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 845
    invoke-virtual {v3, v10}, Lcom/github/mikephil/charting/components/LimitLine;->setTextSize(F)V

    .line 846
    invoke-virtual {v3, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setTextColor(I)V

    .line 847
    new-instance v0, Lcom/github/mikephil/charting/components/LimitLine;

    const/high16 v5, 0x42960000    # 75.0f

    const v6, 0x7f09006e

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    .line 848
    .local v0, "awake":Lcom/github/mikephil/charting/components/LimitLine;
    invoke-virtual {v0, v9}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 849
    invoke-virtual {v0, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 850
    sget-object v5, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual {v0, v5}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 851
    invoke-virtual {v0, v11}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 852
    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/components/LimitLine;->setTextSize(F)V

    .line 853
    invoke-virtual {v0, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setTextColor(I)V

    .line 854
    new-instance v1, Lcom/github/mikephil/charting/components/LimitLine;

    const/high16 v5, 0x41f00000    # 30.0f

    const-string v6, ""

    invoke-direct {v1, v5, v6}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    .line 855
    .local v1, "bottom":Lcom/github/mikephil/charting/components/LimitLine;
    invoke-virtual {v1, v9}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 856
    invoke-virtual {v1, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 857
    new-instance v4, Lcom/github/mikephil/charting/components/LimitLine;

    const/high16 v5, 0x42700000    # 60.0f

    const-string v6, ""

    invoke-direct {v4, v5, v6}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    .line 858
    .local v4, "top":Lcom/github/mikephil/charting/components/LimitLine;
    invoke-virtual {v4, v9}, Lcom/github/mikephil/charting/components/LimitLine;->setLineWidth(F)V

    .line 859
    invoke-virtual {v4, v8}, Lcom/github/mikephil/charting/components/LimitLine;->setLineColor(I)V

    .line 860
    invoke-virtual {p1, v2}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 861
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 862
    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 863
    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 864
    invoke-virtual {p1, v4}, Lcom/github/mikephil/charting/components/YAxis;->addLimitLine(Lcom/github/mikephil/charting/components/LimitLine;)V

    .line 865
    return-void
.end method

.method private setUiHelperDataSet(Lcom/github/mikephil/charting/data/LineDataSet;)V
    .locals 4
    .param p1, "set2"    # Lcom/github/mikephil/charting/data/LineDataSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const v1, 0x7f0f00a4

    .line 912
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 913
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 914
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighLightColor(I)V

    .line 915
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 916
    invoke-virtual {p1, v2}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 917
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCubic(Z)V

    .line 918
    invoke-virtual {p1, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 919
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 920
    invoke-virtual {p1, v2}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 921
    return-void
.end method


# virtual methods
.method public buttonLeft()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1000af
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->onButtonLeft()V

    .line 182
    return-void
.end method

.method public buttonRight()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1000b0
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->onButtonRight()V

    .line 176
    return-void
.end method

.method public displayAlert(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "delay"    # I

    .prologue
    .line 228
    return-void
.end method

.method public hideProgress()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->dismiss()V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    .line 211
    :cond_0
    return-void
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 978
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsInForegroundMode:Z

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "compoundButton"    # Landroid/widget/CompoundButton;
    .param p2, "b"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 548
    invoke-direct {p0, v3}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setListenersEnable(Z)V

    .line 549
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 550
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 549
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 553
    :cond_0
    const/4 v1, 0x0

    .line 554
    .local v1, "type":I
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 572
    :goto_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v2, v1}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->onChartTypeChange(I)V

    .line 573
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setListenersEnable(Z)V

    .line 574
    return-void

    .line 556
    :pswitch_0
    const/4 v1, 0x0

    .line 557
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    aget-object v2, v2, v3

    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 560
    :pswitch_1
    const/4 v1, 0x1

    .line 561
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    aget-object v2, v2, v4

    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 564
    :pswitch_2
    const/4 v1, 0x2

    .line 565
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 568
    :pswitch_3
    const/4 v1, 0x3

    .line 569
    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 554
    :pswitch_data_0
    .packed-switch 0x7f1000b2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x400

    const/4 v2, 0x0

    .line 109
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 111
    const v1, 0x7f03001e

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setContentView(I)V

    .line 112
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 114
    new-instance v1, Lcom/vectorwatch/android/ui/helper/OrientationManager;

    const/4 v3, 0x3

    invoke-direct {v1, p0, v3, p0}, Lcom/vectorwatch/android/ui/helper/OrientationManager;-><init>(Landroid/content/Context;ILcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    .line 115
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/helper/OrientationManager;->enable()V

    .line 117
    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_OPENED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, v3, v4, v1}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "sensor_delay"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsSensorDelay:Z

    .line 121
    iget-boolean v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsSensorDelay:Z

    if-eqz v1, :cond_0

    .line 122
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$1;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;)V

    const-wide/16 v4, 0x1770

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 130
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->currentApiVersion:I

    .line 133
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->currentApiVersion:I

    const/16 v3, 0x13

    if-lt v1, v3, :cond_1

    .line 134
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x1706

    invoke-virtual {v1, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 138
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 139
    .local v0, "decorView":Landroid/view/View;
    new-instance v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$2;

    invoke-direct {v1, p0, v0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$2;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 149
    .end local v0    # "decorView":Landroid/view/View;
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v4

    sput-wide v4, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sCaloriesBaseline:D

    .line 150
    sget-wide v4, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sCaloriesBaseline:D

    const-wide v6, 0x408f400000000000L    # 1000.0

    cmpl-double v1, v4, v6

    if-lez v1, :cond_2

    sget-wide v4, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sCaloriesBaseline:D

    const-wide v6, 0x40ab580000000000L    # 3500.0

    cmpg-double v1, v4, v6

    if-gez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sHasBase:Z

    .line 151
    new-instance v1, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;

    invoke-direct {v1, p0, p0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenterImpl;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/chart/view/ChartView;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    .line 152
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->init(Landroid/os/Bundle;)V

    .line 153
    return-void

    :cond_2
    move v1, v2

    .line 150
    goto :goto_0
.end method

.method public onDataFetched(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;IIF)V
    .locals 26
    .param p3, "mSelectedDataInterval"    # I
    .param p4, "mSelectedDataType"    # I
    .param p5, "average"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;IIF)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;>;"
    .local p2, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    if-eqz p3, :cond_0

    .line 240
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->fillMissingData(Landroid/util/SparseArray;I)Landroid/util/SparseArray;

    move-result-object p2

    .line 243
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mNoDataTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setVisibility(I)V

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mDateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v21, v0

    invoke-static/range {p3 .. p3}, Lcom/vectorwatch/android/ui/chart/util/ChartUtil;->getDataFormat(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 246
    if-eqz p2, :cond_4

    .line 247
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getDataLabels(Landroid/util/SparseArray;II)Ljava/util/List;

    move-result-object v16

    .line 249
    .local v16, "xAxisValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/AxisValue;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v4, "chartLineData":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_2

    .line 252
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 253
    .local v13, "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    sget-object v21, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->log:Lorg/slf4j/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "max y = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getYValue()F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v21

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-ltz v21, :cond_1

    .line 257
    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 261
    .end local v13    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_2
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 262
    .local v20, "yVals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .local v17, "xValsSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_3

    .line 265
    new-instance v8, Lcom/github/mikephil/charting/data/Entry;

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v21

    move/from16 v0, v21

    invoke-direct {v8, v0, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    .line 266
    .local v8, "e":Lcom/github/mikephil/charting/data/Entry;
    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getTimestamp()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/github/mikephil/charting/data/Entry;->setData(Ljava/lang/Object;)V

    .line 267
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->getLabelAsChars()[C

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 271
    .end local v8    # "e":Lcom/github/mikephil/charting/data/Entry;
    :cond_3
    new-instance v14, Lcom/github/mikephil/charting/data/LineDataSet;

    const-string v21, "DataSet 1"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v14, v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 272
    .local v14, "set1":Lcom/github/mikephil/charting/data/LineDataSet;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->initDataSet(Lcom/github/mikephil/charting/data/LineDataSet;)V

    .line 274
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 276
    .local v7, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->initChartAxis()V

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v11

    .line 279
    .local v11, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getLimitFormatText()Ljava/text/DecimalFormat;

    move-result-object v9

    .line 280
    .local v9, "formatF":Ljava/text/DecimalFormat;
    packed-switch p3, :pswitch_data_0

    .line 406
    :goto_2
    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    new-instance v6, Lcom/github/mikephil/charting/data/LineData;

    move-object/from16 v0, v17

    invoke-direct {v6, v0, v7}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 408
    .local v6, "dataNew":Lcom/github/mikephil/charting/data/LineData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    move-object/from16 v21, v0

    const/16 v22, 0x3e8

    sget-object v23, Lcom/github/mikephil/charting/animation/Easing$EasingOption;->EaseInOutCubic:Lcom/github/mikephil/charting/animation/Easing$EasingOption;

    invoke-virtual/range {v21 .. v23}, Lcom/github/mikephil/charting/charts/LineChart;->animateY(ILcom/github/mikephil/charting/animation/Easing$EasingOption;)V

    .line 412
    .end local v4    # "chartLineData":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    .end local v6    # "dataNew":Lcom/github/mikephil/charting/data/LineData;
    .end local v7    # "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    .end local v9    # "formatF":Ljava/text/DecimalFormat;
    .end local v10    # "i":I
    .end local v11    # "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    .end local v14    # "set1":Lcom/github/mikephil/charting/data/LineDataSet;
    .end local v16    # "xAxisValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/AxisValue;>;"
    .end local v17    # "xValsSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v20    # "yVals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_4
    return-void

    .line 282
    .restart local v4    # "chartLineData":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    .restart local v7    # "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    .restart local v9    # "formatF":Ljava/text/DecimalFormat;
    .restart local v10    # "i":I
    .restart local v11    # "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    .restart local v14    # "set1":Lcom/github/mikephil/charting/data/LineDataSet;
    .restart local v16    # "xAxisValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/AxisValue;>;"
    .restart local v17    # "xValsSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v20    # "yVals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :pswitch_0
    const/16 v21, 0x3

    move/from16 v0, p4

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 283
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setSleepChartLimits(Lcom/github/mikephil/charting/components/YAxis;)V

    goto :goto_2

    .line 284
    :cond_5
    if-eqz p4, :cond_6

    const/16 v21, 0x1

    move/from16 v0, p4

    move/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 285
    :cond_6
    invoke-virtual {v11}, Lcom/github/mikephil/charting/components/YAxis;->removeAllLimitLines()V

    .line 286
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f09006d

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move/from16 v0, p5

    float-to-double v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move/from16 v1, p5

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addAvgLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    goto/16 :goto_2

    .line 289
    :cond_7
    invoke-virtual {v11}, Lcom/github/mikephil/charting/components/YAxis;->removeAllLimitLines()V

    .line 290
    sget-wide v22, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sCaloriesBaseline:D

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x42c80000    # 100.0f

    div-float v21, v21, v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1, v9, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addBaseLineLimit(FLjava/text/DecimalFormat;Lcom/github/mikephil/charting/components/YAxis;)V

    goto/16 :goto_2

    .line 296
    :pswitch_1
    const/16 v21, 0x3

    move/from16 v0, p4

    move/from16 v1, v21

    if-ne v0, v1, :cond_a

    .line 297
    invoke-virtual {v11}, Lcom/github/mikephil/charting/components/YAxis;->removeAllLimitLines()V

    .line 298
    move/from16 v0, p5

    float-to-double v0, v0

    move-wide/from16 v22, v0

    move/from16 v0, p5

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->floor(D)D

    move-result-wide v24

    sub-double v22, v22, v24

    const-wide/high16 v24, 0x404e000000000000L    # 60.0

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v12, v0

    .line 299
    .local v12, "minutes":F
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f09006d

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move/from16 v0, p5

    float-to-double v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    float-to-double v0, v12

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move/from16 v1, p5

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addAvgLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 302
    sget-object v21, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    .line 303
    .local v5, "currentGoal":F
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f090134

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    float-to-double v0, v5

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v5, v1}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addGoalLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 306
    cmpg-float v21, p5, v5

    if-gez v21, :cond_8

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 314
    :goto_3
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 315
    .local v19, "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .local v18, "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_9

    .line 319
    new-instance v21, Lcom/github/mikephil/charting/data/Entry;

    const/high16 v22, 0x41200000    # 10.0f

    mul-float v22, v22, v5

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    add-float v22, v22, v5

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->getLabelAsChars()[C

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 310
    .end local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    goto :goto_3

    .line 324
    .restart local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_9
    new-instance v15, Lcom/github/mikephil/charting/data/LineDataSet;

    const-string v21, "DataSet 2"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 325
    .local v15, "set2":Lcom/github/mikephil/charting/data/LineDataSet;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setUiHelperDataSet(Lcom/github/mikephil/charting/data/LineDataSet;)V

    .line 326
    invoke-interface {v7, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 327
    .end local v5    # "currentGoal":F
    .end local v12    # "minutes":F
    .end local v15    # "set2":Lcom/github/mikephil/charting/data/LineDataSet;
    .end local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_a
    if-eqz p4, :cond_b

    const/16 v21, 0x1

    move/from16 v0, p4

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    .line 328
    :cond_b
    invoke-virtual {v11}, Lcom/github/mikephil/charting/components/YAxis;->removeAllLimitLines()V

    .line 329
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f09006d

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move/from16 v0, p5

    float-to-double v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move/from16 v1, p5

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addAvgLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 333
    if-nez p4, :cond_c

    .line 334
    sget-object v21, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    .line 345
    .restart local v5    # "currentGoal":F
    :goto_5
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f090134

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    float-to-double v0, v5

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v5, v1}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addGoalLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 348
    cmpg-float v21, p5, v5

    if-gez v21, :cond_e

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 356
    :goto_6
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 357
    .restart local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 360
    .restart local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    :goto_7
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_f

    .line 361
    new-instance v21, Lcom/github/mikephil/charting/data/Entry;

    const/high16 v22, 0x41200000    # 10.0f

    mul-float v22, v22, v5

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    add-float v22, v22, v5

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->getLabelAsChars()[C

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 336
    .end local v5    # "currentGoal":F
    .end local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_c
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 337
    sget-object v21, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v21

    const/high16 v22, 0x447a0000    # 1000.0f

    div-float v5, v21, v22

    .restart local v5    # "currentGoal":F
    goto/16 :goto_5

    .line 340
    .end local v5    # "currentGoal":F
    :cond_d
    sget-object v21, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    .line 341
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v21

    const/high16 v22, 0x42c80000    # 100.0f

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-long v0, v0

    move-wide/from16 v22, v0

    .line 340
    invoke-static/range {v22 .. v23}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToMiles(J)F

    move-result v5

    .restart local v5    # "currentGoal":F
    goto/16 :goto_5

    .line 352
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    goto/16 :goto_6

    .line 366
    .restart local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_f
    new-instance v15, Lcom/github/mikephil/charting/data/LineDataSet;

    const-string v21, "DataSet 2"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 367
    .restart local v15    # "set2":Lcom/github/mikephil/charting/data/LineDataSet;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setUiHelperDataSet(Lcom/github/mikephil/charting/data/LineDataSet;)V

    .line 368
    invoke-interface {v7, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 371
    .end local v5    # "currentGoal":F
    .end local v15    # "set2":Lcom/github/mikephil/charting/data/LineDataSet;
    .end local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_10
    invoke-virtual {v11}, Lcom/github/mikephil/charting/components/YAxis;->removeAllLimitLines()V

    .line 372
    sget-wide v22, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->sCaloriesBaseline:D

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1, v9, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addBaseLineLimit(FLjava/text/DecimalFormat;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 374
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f09006d

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move/from16 v0, p5

    float-to-double v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move/from16 v1, p5

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addAvgLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 377
    sget-object v21, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    .line 378
    .restart local v5    # "currentGoal":F
    new-instance v21, Lcom/github/mikephil/charting/components/LimitLine;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const v23, 0x7f090134

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    float-to-double v0, v5

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v5, v1}, Lcom/github/mikephil/charting/components/LimitLine;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->addGoalLineLimit(Lcom/github/mikephil/charting/components/LimitLine;Lcom/github/mikephil/charting/components/YAxis;)V

    .line 381
    cmpg-float v21, p5, v5

    if-gez v21, :cond_11

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 389
    :goto_8
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .restart local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 393
    .restart local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    :goto_9
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_12

    .line 394
    new-instance v21, Lcom/github/mikephil/charting/data/Entry;

    const/high16 v22, 0x41200000    # 10.0f

    mul-float v22, v22, v5

    const/high16 v23, 0x42c80000    # 100.0f

    div-float v22, v22, v23

    add-float v22, v22, v5

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->getLabelAsChars()[C

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 385
    .end local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->goalLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_BOTTOM:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->avgLineLimit:Lcom/github/mikephil/charting/components/LimitLine;

    move-object/from16 v21, v0

    sget-object v22, Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;->LEFT_TOP:Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;

    invoke-virtual/range {v21 .. v22}, Lcom/github/mikephil/charting/components/LimitLine;->setLabelPosition(Lcom/github/mikephil/charting/components/LimitLine$LimitLabelPosition;)V

    goto :goto_8

    .line 399
    .restart local v18    # "xValsSet2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "yVal2s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    :cond_12
    new-instance v15, Lcom/github/mikephil/charting/data/LineDataSet;

    const-string v21, "DataSet 2"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 400
    .restart local v15    # "set2":Lcom/github/mikephil/charting/data/LineDataSet;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setUiHelperDataSet(Lcom/github/mikephil/charting/data/LineDataSet;)V

    .line 401
    invoke-interface {v7, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 280
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onOrientationChange(Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;)V
    .locals 2
    .param p1, "screenOrientation"    # Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .prologue
    .line 519
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsSensorDelay:Z

    if-eqz v0, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->isInForeground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    sget-object v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$6;->$SwitchMap$com$vectorwatch$android$ui$helper$OrientationManager$ScreenOrientation:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 526
    :pswitch_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->finish()V

    goto :goto_0

    .line 523
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onPause()V

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsInForegroundMode:Z

    .line 165
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mIsInForegroundMode:Z

    .line 159
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onStop()V

    .line 170
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->onStop()V

    .line 171
    return-void
.end method

.method public onTabReselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .locals 0
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .prologue
    .line 590
    return-void
.end method

.method public onTabSelected(Landroid/support/design/widget/TabLayout$Tab;)V
    .locals 2
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .prologue
    .line 578
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageRight:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 579
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-interface {v1, v0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->onTabSelected(Ljava/lang/Integer;)V

    .line 580
    return-void
.end method

.method public onTabUnselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .locals 0
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .prologue
    .line 585
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 534
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onWindowFocusChanged(Z)V

    .line 535
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->currentApiVersion:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 536
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x1706

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 544
    :cond_0
    return-void
.end method

.method public setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;III)V
    .locals 7
    .param p1, "formater"    # Ljava/text/SimpleDateFormat;
    .param p2, "cal"    # Ljava/util/Calendar;
    .param p3, "mSelectedDataInterval"    # I
    .param p4, "mSelectedPageBefore0"    # I
    .param p5, "mSelectedDataType"    # I

    .prologue
    .line 595
    new-instance v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;

    move-object v1, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;IIILjava/util/Calendar;Ljava/text/SimpleDateFormat;)V

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 645
    return-void
.end method

.method public setInterval(Ljava/util/Calendar;JJII)V
    .locals 10
    .param p1, "mCalendar"    # Ljava/util/Calendar;
    .param p2, "fStartDate"    # J
    .param p4, "fEndDate"    # J
    .param p6, "mSelectedDataInterval"    # I
    .param p7, "mSelectedPageBefore0"    # I

    .prologue
    .line 698
    new-instance v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;

    move-object v2, p0

    move/from16 v3, p6

    move-wide v4, p2

    move-wide v6, p4

    move/from16 v8, p7

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;IJJILjava/util/Calendar;)V

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 751
    return-void
.end method

.method public setNoData()V
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->hideProgress()V

    .line 216
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->clear()V

    .line 217
    return-void
.end method

.method public setNoPersonalData()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mNoDataTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->clear()V

    .line 223
    return-void
.end method

.method public setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "format"    # Ljava/text/DecimalFormat;
    .param p2, "finalValue"    # F
    .param p3, "distanceInd2"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "mSelectedDataInterval"    # I
    .param p6, "mSelectedDataType"    # I

    .prologue
    .line 649
    new-instance v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;

    move-object v1, p0

    move v2, p6

    move v3, p2

    move-object v4, p4

    move-object v5, p1

    move-object v6, p3

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;IFLjava/lang/String;Ljava/text/DecimalFormat;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 694
    return-void
.end method

.method public showProgress()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    .line 188
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->setCancelable(Z)V

    .line 189
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mSpokeProgressDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->show()V

    .line 191
    :cond_0
    return-void
.end method

.method public updateArrowsView(I)V
    .locals 2
    .param p1, "selectedPage"    # I

    .prologue
    const/4 v1, 0x0

    .line 196
    if-nez p1, :cond_0

    .line 197
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageRight:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
