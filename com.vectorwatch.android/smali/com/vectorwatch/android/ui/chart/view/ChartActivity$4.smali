.class Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;
.super Ljava/lang/Object;
.source "ChartActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

.field final synthetic val$distanceInd2:Ljava/lang/String;

.field final synthetic val$finalValue:F

.field final synthetic val$format:Ljava/text/DecimalFormat;

.field final synthetic val$mSelectedDataInterval:I

.field final synthetic val$mSelectedDataType:I

.field final synthetic val$message:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;IFLjava/lang/String;Ljava/text/DecimalFormat;Ljava/lang/String;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    .prologue
    .line 649
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iput p2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataType:I

    iput p3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    iput-object p4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    iput-object p5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iput-object p6, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$distanceInd2:Ljava/lang/String;

    iput p7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataInterval:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 652
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 653
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_1

    .line 654
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 655
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v2, v1

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    mul-double/2addr v2, v4

    double-to-float v0, v2

    .line 656
    .local v0, "minutes":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    float-to-double v4, v0

    .line 657
    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$distanceInd2:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 656
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    .end local v0    # "minutes":F
    :cond_0
    :goto_0
    return-void

    .line 659
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 662
    :cond_2
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataInterval:I

    if-nez v1, :cond_4

    .line 663
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 664
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$distanceInd2:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 665
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 667
    :cond_3
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$distanceInd2:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 670
    :cond_4
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_8

    .line 671
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 672
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataType:I

    if-nez v1, :cond_5

    .line 673
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    const-string v2, "#0"

    invoke-virtual {v1, v2}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 674
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    const v4, 0x7f090235

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 675
    :cond_5
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 676
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    const-string v2, "#0.00"

    invoke-virtual {v1, v2}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 677
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 678
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    const v4, 0x7f090146

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 680
    :cond_6
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    const v4, 0x7f0901ab

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 683
    :cond_7
    iget v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$mSelectedDataType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 684
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    const-string v2, "#0"

    invoke-virtual {v1, v2}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 685
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$format:Ljava/text/DecimalFormat;

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->val$finalValue:F

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    const v4, 0x7f090088

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 688
    :cond_8
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$4;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method
