.class Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;
.super Ljava/lang/Object;
.source "ChartActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setInterval(Ljava/util/Calendar;JJII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

.field final synthetic val$fEndDate:J

.field final synthetic val$fStartDate:J

.field final synthetic val$mCalendar:Ljava/util/Calendar;

.field final synthetic val$mSelectedDataInterval:I

.field final synthetic val$mSelectedPageBefore0:I


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;IJJILjava/util/Calendar;)V
    .locals 1
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    .prologue
    .line 698
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iput p2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedDataInterval:I

    iput-wide p3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fStartDate:J

    iput-wide p5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    iput p7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedPageBefore0:I

    iput-object p8, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mCalendar:Ljava/util/Calendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 701
    iget v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedDataInterval:I

    packed-switch v9, :pswitch_data_0

    .line 749
    :goto_0
    return-void

    .line 703
    :pswitch_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v9, "dd MMM"

    invoke-direct {v3, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 704
    .local v3, "formater":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    .line 705
    .local v8, "tz":Ljava/util/TimeZone;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 706
    .local v4, "now":Ljava/util/Date;
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v5

    .line 707
    .local v5, "offsetFromUtc":I
    new-instance v1, Ljava/util/Date;

    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fStartDate:J

    invoke-direct {v1, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 708
    .local v1, "date":Ljava/util/Date;
    new-instance v2, Ljava/util/Date;

    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    int-to-long v12, v5

    sub-long/2addr v10, v12

    invoke-direct {v2, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 709
    .local v2, "date2":Ljava/util/Date;
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v9, v9, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->centerDate:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 711
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 712
    .local v0, "cal":Ljava/util/Calendar;
    const-string v9, "UTC"

    invoke-static {v9}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 713
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 714
    const/16 v9, 0xb

    const/16 v10, 0x18

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->set(II)V

    .line 715
    const/16 v9, 0xc

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->set(II)V

    .line 716
    const/16 v9, 0xd

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->set(II)V

    .line 717
    const/4 v9, 0x3

    iget v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedPageBefore0:I

    mul-int/lit8 v10, v10, -0x1

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->add(II)V

    .line 718
    const/4 v9, 0x7

    const/4 v10, 0x2

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->set(II)V

    .line 720
    iget v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedPageBefore0:I

    if-nez v9, :cond_0

    .line 721
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x7

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    mul-int/lit16 v9, v9, 0xe10

    mul-int/lit16 v9, v9, 0x3e8

    mul-int/lit8 v9, v9, 0x18

    int-to-long v6, v9

    .line 722
    .local v6, "timeToAdd":J
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    const-wide/32 v12, 0x1ee62800

    sub-long/2addr v10, v12

    sub-long/2addr v10, v6

    invoke-virtual {v1, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 723
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    const-wide/32 v12, 0x5265c00

    add-long/2addr v10, v12

    sub-long/2addr v10, v6

    invoke-virtual {v2, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 728
    .end local v6    # "timeToAdd":J
    :goto_1
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v9, v9, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->leftDate:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 729
    iget v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedPageBefore0:I

    add-int/lit8 v9, v9, -0x1

    if-nez v9, :cond_1

    .line 730
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x7

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    mul-int/lit16 v9, v9, 0xe10

    mul-int/lit16 v9, v9, 0x3e8

    mul-int/lit8 v9, v9, 0x18

    int-to-long v6, v9

    .line 731
    .restart local v6    # "timeToAdd":J
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fStartDate:J

    const-wide/32 v12, 0x240c8400

    add-long/2addr v10, v12

    add-long/2addr v10, v6

    invoke-virtual {v1, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 732
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    const-wide/32 v12, 0x240c8400

    add-long/2addr v10, v12

    add-long/2addr v10, v6

    invoke-virtual {v2, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 733
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v9, v9, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 740
    .end local v6    # "timeToAdd":J
    :goto_2
    iget v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$mSelectedPageBefore0:I

    if-nez v9, :cond_2

    .line 741
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v9, v9, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 725
    :cond_0
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    const-wide/32 v12, 0x48190800

    sub-long/2addr v10, v12

    invoke-virtual {v1, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 726
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    const-wide/32 v12, 0x240c8400

    sub-long/2addr v10, v12

    invoke-virtual {v2, v10, v11}, Ljava/util/Date;->setTime(J)V

    goto/16 :goto_1

    .line 735
    :cond_1
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fStartDate:J

    const-wide/32 v12, 0x240c8400

    add-long/2addr v10, v12

    invoke-virtual {v1, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 736
    iget-wide v10, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->val$fEndDate:J

    const-wide/32 v12, 0x240c8400

    add-long/2addr v10, v12

    invoke-virtual {v2, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 737
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v9, v9, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 743
    :cond_2
    iget-object v9, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$5;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v9, v9, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 701
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
