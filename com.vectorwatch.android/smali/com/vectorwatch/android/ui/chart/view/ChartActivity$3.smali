.class Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;
.super Ljava/lang/Object;
.source "ChartActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

.field final synthetic val$cal:Ljava/util/Calendar;

.field final synthetic val$formater:Ljava/text/SimpleDateFormat;

.field final synthetic val$mSelectedDataInterval:I

.field final synthetic val$mSelectedDataType:I

.field final synthetic val$mSelectedPageBefore0:I


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;IIILjava/util/Calendar;Ljava/text/SimpleDateFormat;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    .prologue
    .line 595
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iput p2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedDataInterval:I

    iput p3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedPageBefore0:I

    iput p4, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedDataType:I

    iput-object p5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    iput-object p6, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 598
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedDataInterval:I

    packed-switch v0, :pswitch_data_0

    .line 643
    :goto_0
    :pswitch_0
    return-void

    .line 600
    :pswitch_1
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedPageBefore0:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedDataType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 601
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/4 v1, -0x1

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->add(II)V

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->centerDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 605
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->add(II)V

    .line 606
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/4 v1, -0x2

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->add(II)V

    .line 608
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->leftDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 609
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedPageBefore0:I

    if-nez v0, :cond_1

    .line 610
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 612
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 616
    :pswitch_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 617
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->centerDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 618
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/16 v1, -0xa

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->add(II)V

    .line 619
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->leftDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 620
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/4 v1, 0x2

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 621
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 623
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedPageBefore0:I

    if-nez v0, :cond_2

    .line 624
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 626
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 630
    :pswitch_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/4 v1, -0x7

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->add(II)V

    .line 631
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->centerDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 632
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v0, v3, v3}, Ljava/util/Calendar;->add(II)V

    .line 633
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    const/4 v1, -0x2

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 635
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->leftDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$formater:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 636
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->val$mSelectedPageBefore0:I

    if-nez v0, :cond_3

    .line 637
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 639
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$3;->this$0:Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 598
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
