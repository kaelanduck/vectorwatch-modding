.class public Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;
.super Lcom/github/mikephil/charting/components/MarkerView;
.source "ChartMarkerView.java"


# instance fields
.field final formatAMPM:Ljava/text/SimpleDateFormat;

.field private mContext:Landroid/content/Context;

.field final mDateFormatter:Ljava/text/SimpleDateFormat;

.field private mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

.field private tvContent:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResource"    # I
    .param p3, "presenter"    # Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/components/MarkerView;-><init>(Landroid/content/Context;I)V

    .line 24
    invoke-static {}, Ljava/text/SimpleDateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mDateFormatter:Ljava/text/SimpleDateFormat;

    .line 25
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm aa"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->formatAMPM:Ljava/text/SimpleDateFormat;

    .line 33
    const v0, 0x7f10019e

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    .line 34
    iput-object p3, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    .line 35
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method


# virtual methods
.method public getXOffset(F)I
    .locals 1
    .param p1, "xpos"    # F

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    return v0
.end method

.method public getYOffset(F)I
    .locals 1
    .param p1, "ypos"    # F

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->getHeight()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public refreshContent(Lcom/github/mikephil/charting/data/Entry;Lcom/github/mikephil/charting/highlight/Highlight;)V
    .locals 10
    .param p1, "e"    # Lcom/github/mikephil/charting/data/Entry;
    .param p2, "highlight"    # Lcom/github/mikephil/charting/highlight/Highlight;

    .prologue
    .line 42
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mDateFormatter:Ljava/text/SimpleDateFormat;

    iget-object v6, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v6}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataInterval()I

    move-result v6

    invoke-static {v6}, Lcom/vectorwatch/android/ui/chart/util/ChartUtil;->getDataFormat(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 43
    iget-object v6, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mDateFormatter:Ljava/text/SimpleDateFormat;

    new-instance v7, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "label":Ljava/lang/String;
    instance-of v5, p1, Lcom/github/mikephil/charting/data/CandleEntry;

    if-eqz v5, :cond_1

    move-object v0, p1

    .line 47
    check-cast v0, Lcom/github/mikephil/charting/data/CandleEntry;

    .line 48
    .local v0, "ce":Lcom/github/mikephil/charting/data/CandleEntry;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/CandleEntry;->getHigh()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    .end local v0    # "ce":Lcom/github/mikephil/charting/data/CandleEntry;
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataInterval()I

    move-result v5

    if-nez v5, :cond_2

    .line 51
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->is24hFormat(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 52
    iget-object v6, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->formatAMPM:Ljava/text/SimpleDateFormat;

    new-instance v7, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 56
    :cond_2
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataType()I

    move-result v5

    if-nez v5, :cond_3

    .line 57
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f090235

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 59
    :cond_3
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 60
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 61
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f090146

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 63
    :cond_4
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f0901ac

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 65
    :cond_5
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 66
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/github/mikephil/charting/utils/Utils;->formatNumber(FIZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f090087

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 67
    :cond_6
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataType()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 68
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$View;->getDataInterval()I

    move-result v5

    if-nez v5, :cond_9

    .line 69
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v5

    const/high16 v6, 0x41f00000    # 30.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_7

    .line 70
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f09009b

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 71
    :cond_7
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v5

    const/high16 v6, 0x42700000    # 60.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_8

    .line 72
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f090198

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 74
    :cond_8
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f09006e

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 77
    :cond_9
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v1, v6

    .line 78
    .local v1, "hours":I
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v5

    int-to-float v6, v1

    sub-float v4, v5, v6

    .line 79
    .local v4, "minsFloat":F
    const/high16 v5, 0x42700000    # 60.0f

    mul-float/2addr v5, v4

    float-to-int v3, v5

    .line 80
    .local v3, "mins":I
    const/16 v5, 0xa

    if-ge v3, v5, :cond_a

    .line 81
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f09013d

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 83
    :cond_a
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->tvContent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/chart/view/ChartMarkerView;->mContext:Landroid/content/Context;

    const v8, 0x7f09013d

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
