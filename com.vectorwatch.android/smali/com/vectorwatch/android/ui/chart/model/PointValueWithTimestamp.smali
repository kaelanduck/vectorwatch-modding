.class public Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
.super Lcom/vectorwatch/android/ui/chart/model/PointValue;
.source "PointValueWithTimestamp.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/chart/model/IPoolObject;


# instance fields
.field private mTimestamp:J

.field private mYValue:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/chart/model/PointValue;-><init>(FF)V

    .line 9
    return-void
.end method


# virtual methods
.method public getTimestamp()J
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->mTimestamp:J

    return-wide v0
.end method

.method public getYValue()F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->mYValue:F

    return v0
.end method

.method public invalidateState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->mTimestamp:J

    .line 32
    iput v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->mYValue:F

    .line 33
    invoke-virtual {p0, v2, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 34
    return-void
.end method

.method public setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .locals 1
    .param p1, "mTimestamp"    # J

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->mTimestamp:J

    .line 17
    return-object p0
.end method

.method public setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .locals 0
    .param p1, "mYValue"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->mYValue:F

    .line 26
    return-object p0
.end method
