.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
.super Lio/realm/RealmObject;
.source "CloudActivityDay.java"

# interfaces
.implements Lio/realm/CloudActivityDayRealmProxyInterface;


# instance fields
.field private activityType:Ljava/lang/String;

.field private avgAmpl:I

.field private avgPeriod:I

.field private cal:I

.field private dirtyCalories:Z

.field private dirtyCloud:Z

.field private dirtyDistance:Z

.field private dirtySteps:Z

.field private dist:I

.field private effTime:I

.field private offset:I

.field private timestamp:J
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private timezone:I

.field private val:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public getActivityType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$activityType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAvgAmpl()I
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$avgAmpl()I

    move-result v0

    return v0
.end method

.method public getAvgPeriod()I
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$avgPeriod()I

    move-result v0

    return v0
.end method

.method public getCal()I
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$cal()I

    move-result v0

    return v0
.end method

.method public getDist()I
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$dist()I

    move-result v0

    return v0
.end method

.method public getEffTime()I
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$effTime()I

    move-result v0

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$offset()I

    move-result v0

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$timestamp()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTimezone()I
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$timezone()I

    move-result v0

    return v0
.end method

.method public getVal()I
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$val()I

    move-result v0

    return v0
.end method

.method public isDirtyCalories()Z
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$dirtyCalories()Z

    move-result v0

    return v0
.end method

.method public isDirtyCloud()Z
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$dirtyCloud()Z

    move-result v0

    return v0
.end method

.method public isDirtyDistance()Z
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$dirtyDistance()Z

    move-result v0

    return v0
.end method

.method public isDirtySteps()Z
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmGet$dirtySteps()Z

    move-result v0

    return v0
.end method

.method public realmGet$activityType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->activityType:Ljava/lang/String;

    return-object v0
.end method

.method public realmGet$avgAmpl()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->avgAmpl:I

    return v0
.end method

.method public realmGet$avgPeriod()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->avgPeriod:I

    return v0
.end method

.method public realmGet$cal()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->cal:I

    return v0
.end method

.method public realmGet$dirtyCalories()Z
    .locals 1

    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtyCalories:Z

    return v0
.end method

.method public realmGet$dirtyCloud()Z
    .locals 1

    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtyCloud:Z

    return v0
.end method

.method public realmGet$dirtyDistance()Z
    .locals 1

    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtyDistance:Z

    return v0
.end method

.method public realmGet$dirtySteps()Z
    .locals 1

    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtySteps:Z

    return v0
.end method

.method public realmGet$dist()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dist:I

    return v0
.end method

.method public realmGet$effTime()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->effTime:I

    return v0
.end method

.method public realmGet$offset()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->offset:I

    return v0
.end method

.method public realmGet$timestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->timestamp:J

    return-wide v0
.end method

.method public realmGet$timezone()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->timezone:I

    return v0
.end method

.method public realmGet$val()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->val:I

    return v0
.end method

.method public realmSet$activityType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->activityType:Ljava/lang/String;

    return-void
.end method

.method public realmSet$avgAmpl(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->avgAmpl:I

    return-void
.end method

.method public realmSet$avgPeriod(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->avgPeriod:I

    return-void
.end method

.method public realmSet$cal(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->cal:I

    return-void
.end method

.method public realmSet$dirtyCalories(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtyCalories:Z

    return-void
.end method

.method public realmSet$dirtyCloud(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtyCloud:Z

    return-void
.end method

.method public realmSet$dirtyDistance(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtyDistance:Z

    return-void
.end method

.method public realmSet$dirtySteps(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dirtySteps:Z

    return-void
.end method

.method public realmSet$dist(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->dist:I

    return-void
.end method

.method public realmSet$effTime(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->effTime:I

    return-void
.end method

.method public realmSet$offset(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->offset:I

    return-void
.end method

.method public realmSet$timestamp(J)V
    .locals 1

    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->timestamp:J

    return-void
.end method

.method public realmSet$timezone(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->timezone:I

    return-void
.end method

.method public realmSet$val(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->val:I

    return-void
.end method

.method public setActivityType(Ljava/lang/String;)V
    .locals 0
    .param p1, "activityType"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$activityType(Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public setAvgAmpl(I)V
    .locals 0
    .param p1, "avgAmpl"    # I

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$avgAmpl(I)V

    .line 43
    return-void
.end method

.method public setAvgPeriod(I)V
    .locals 0
    .param p1, "avgPeriod"    # I

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$avgPeriod(I)V

    .line 51
    return-void
.end method

.method public setCal(I)V
    .locals 0
    .param p1, "cal"    # I

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$cal(I)V

    .line 59
    return-void
.end method

.method public setDirtyCalories(Z)V
    .locals 0
    .param p1, "dirtyCalories"    # Z

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$dirtyCalories(Z)V

    .line 115
    return-void
.end method

.method public setDirtyCloud(Z)V
    .locals 0
    .param p1, "dirtyCloud"    # Z

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$dirtyCloud(Z)V

    .line 131
    return-void
.end method

.method public setDirtyDistance(Z)V
    .locals 0
    .param p1, "dirtyDistance"    # Z

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$dirtyDistance(Z)V

    .line 123
    return-void
.end method

.method public setDirtySteps(Z)V
    .locals 0
    .param p1, "dirtySteps"    # Z

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$dirtySteps(Z)V

    .line 107
    return-void
.end method

.method public setDist(I)V
    .locals 0
    .param p1, "dist"    # I

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$dist(I)V

    .line 67
    return-void
.end method

.method public setEffTime(I)V
    .locals 0
    .param p1, "effTime"    # I

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$effTime(I)V

    .line 35
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$offset(I)V

    .line 91
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 98
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$timestamp(J)V

    .line 99
    return-void
.end method

.method public setTimezone(I)V
    .locals 0
    .param p1, "timezone"    # I

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$timezone(I)V

    .line 75
    return-void
.end method

.method public setVal(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->realmSet$val(I)V

    .line 83
    return-void
.end method
