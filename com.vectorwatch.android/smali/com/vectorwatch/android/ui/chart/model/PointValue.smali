.class public Lcom/vectorwatch/android/ui/chart/model/PointValue;
.super Ljava/lang/Object;
.source "PointValue.java"


# instance fields
.field private diffX:F

.field private diffY:F

.field private label:[C

.field private originX:F

.field private originY:F

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p0, v0, v0}, Lcom/vectorwatch/android/ui/chart/model/PointValue;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 20
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/chart/model/PointValue;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/ui/chart/model/PointValue;)V
    .locals 2
    .param p1, "pointValue"    # Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iget v0, p1, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    iget v1, p1, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValue;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 28
    iget-object v0, p1, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    if-ne p0, p1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    .line 96
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 98
    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 100
    .local v0, "that":Lcom/vectorwatch/android/ui/chart/model/PointValue;
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 101
    :cond_4
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 102
    :cond_5
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_0

    .line 103
    :cond_6
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 104
    :cond_7
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_8

    move v1, v2

    goto :goto_0

    .line 105
    :cond_8
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_9

    move v1, v2

    goto :goto_0

    .line 106
    :cond_9
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 37
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    add-float/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValue;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 38
    return-void
.end method

.method public getLabel()[C
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    return-object v0
.end method

.method public getLabelAsChars()[C
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    return-object v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 113
    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 114
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 115
    mul-int/lit8 v3, v0, 0x1f

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 116
    mul-int/lit8 v3, v0, 0x1f

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 117
    mul-int/lit8 v3, v0, 0x1f

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 118
    mul-int/lit8 v3, v0, 0x1f

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 119
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([C)I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 120
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 113
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 114
    goto :goto_1

    :cond_3
    move v2, v1

    .line 115
    goto :goto_2

    :cond_4
    move v2, v1

    .line 116
    goto :goto_3

    :cond_5
    move v2, v1

    .line 117
    goto :goto_4

    :cond_6
    move v2, v1

    .line 118
    goto :goto_5
.end method

.method public set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v0, 0x0

    .line 41
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    .line 42
    iput p2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    .line 43
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    .line 44
    iput p2, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    .line 45
    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    .line 46
    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    .line 47
    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/vectorwatch/android/ui/chart/model/PointValue;
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    .line 75
    return-object p0
.end method

.method public setLabel([C)Lcom/vectorwatch/android/ui/chart/model/PointValue;
    .locals 0
    .param p1, "label"    # [C
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 84
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->label:[C

    .line 85
    return-object p0
.end method

.method public setTarget(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;
    .locals 2
    .param p1, "targetX"    # F
    .param p2, "targetY"    # F

    .prologue
    .line 54
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValue;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 55
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    sub-float v0, p1, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    .line 56
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    sub-float v0, p2, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    .line 57
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PointValue [x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(F)V
    .locals 2
    .param p1, "scale"    # F

    .prologue
    .line 32
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originX:F

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffX:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->x:F

    .line 33
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->originY:F

    iget v1, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->diffY:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/PointValue;->y:F

    .line 34
    return-void
.end method
