.class public Lcom/vectorwatch/android/ui/chart/model/AxisValue;
.super Ljava/lang/Object;
.source "AxisValue.java"


# instance fields
.field private label:[C

.field private value:F


# direct methods
.method public constructor <init>(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->setValue(F)Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    .line 15
    return-void
.end method

.method public constructor <init>(F[C)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "label"    # [C
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    .line 20
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/ui/chart/model/AxisValue;)V
    .locals 1
    .param p1, "axisValue"    # Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iget v0, p1, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    iput v0, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    .line 25
    iget-object v0, p1, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p0, p1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    .line 70
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 72
    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;

    .line 74
    .local v0, "axisValue":Lcom/vectorwatch/android/ui/chart/model/AxisValue;
    iget v3, v0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    iget v4, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 75
    :cond_4
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getLabel()[C
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    return-object v0
.end method

.method public getLabelAsChars()[C
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 82
    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 83
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([C)I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 84
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 82
    goto :goto_0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/vectorwatch/android/ui/chart/model/AxisValue;
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    .line 49
    return-object p0
.end method

.method public setLabel([C)Lcom/vectorwatch/android/ui/chart/model/AxisValue;
    .locals 0
    .param p1, "label"    # [C
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->label:[C

    .line 64
    return-object p0
.end method

.method public setValue(F)Lcom/vectorwatch/android/ui/chart/model/AxisValue;
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/AxisValue;->value:F

    .line 34
    return-object p0
.end method
