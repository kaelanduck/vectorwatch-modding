.class public interface abstract Lcom/vectorwatch/android/ui/chart/util/ChartUtil$DataInterval;
.super Ljava/lang/Object;
.source "ChartUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/chart/util/ChartUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DataInterval"
.end annotation


# static fields
.field public static final MONTH:I = 0x2

.field public static final TODAY:I = 0x0

.field public static final WEEK:I = 0x1

.field public static final YEAR:I = 0x3
