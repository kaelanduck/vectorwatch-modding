.class public Lcom/vectorwatch/android/ui/chart/util/ChartUtil;
.super Ljava/lang/Object;
.source "ChartUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/chart/util/ChartUtil$DataInterval;,
        Lcom/vectorwatch/android/ui/chart/util/ChartUtil$DataType;
    }
.end annotation


# static fields
.field public static final DAY_INTERVAL:I = 0x15180

.field public static final MAX_CALORIES_BASE:I = 0xdac

.field public static final MIN_CALORIES_BASE:I = 0x3e8

.field public static final MIN_NUM_ENTRIES:I = 0x5a

.field public static final MIN_NUM_ENTRIES_MONTH:I = 0x49d4

.field public static final MIN_NUM_ENTRIES_WEEK:I = 0x276

.field public static final MIN_NUM_ENTRIES_YEAR:I = 0x375f0

.field public static final MONTH_INTERVAL:I = 0x278d00

.field public static final QUARTER_HOUR_INTERVAL:I = 0x384

.field public static final WEEK_INTERVAL:I = 0xa8c00

.field public static final WEEK_INTERVAL_CONSTANT:I = 0x7

.field public static final YEAR_INTERVAL:I = 0x1e13380


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static getDataFormat(I)Ljava/lang/String;
    .locals 1
    .param p0, "mSelectedDataInterval"    # I

    .prologue
    .line 52
    packed-switch p0, :pswitch_data_0

    .line 61
    const-string v0, "MMM"

    :goto_0
    return-object v0

    .line 54
    :pswitch_0
    const-string v0, "HH:mm"

    goto :goto_0

    .line 57
    :pswitch_1
    const-string v0, "d MMM"

    goto :goto_0

    .line 59
    :pswitch_2
    const-string v0, "MMM"

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getInterval(I)I
    .locals 1
    .param p0, "tabIndex"    # I

    .prologue
    .line 38
    packed-switch p0, :pswitch_data_0

    .line 46
    const/4 v0, 0x3

    :goto_0
    return v0

    .line 40
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 44
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
