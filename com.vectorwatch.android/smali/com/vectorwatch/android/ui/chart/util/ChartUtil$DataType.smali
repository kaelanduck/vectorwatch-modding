.class public interface abstract Lcom/vectorwatch/android/ui/chart/util/ChartUtil$DataType;
.super Ljava/lang/Object;
.source "ChartUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/chart/util/ChartUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DataType"
.end annotation


# static fields
.field public static final CALORIES:I = 0x2

.field public static final DISTANCE:I = 0x1

.field public static final SLEEP:I = 0x3

.field public static final STEPS:I
