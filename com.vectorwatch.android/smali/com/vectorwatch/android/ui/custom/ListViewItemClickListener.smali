.class public Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;
.super Ljava/lang/Object;
.source "ListViewItemClickListener.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field listView:Landroid/widget/ListView;

.field listViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;Landroid/widget/ListView;)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "adapter"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;
    .param p3, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;->listViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .line 25
    iput-object p3, p0, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;->listView:Landroid/widget/ListView;

    .line 26
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;->listViewAdapter:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v2, p3}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->getItem(I)Lcom/vectorwatch/android/models/StoreElement;

    move-result-object v0

    .line 32
    .local v0, "element":Lcom/vectorwatch/android/models/StoreElement;
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;->mContext:Landroid/content/Context;

    const-class v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "item"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 34
    iget-object v2, p0, Lcom/vectorwatch/android/ui/custom/ListViewItemClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 35
    return-void
.end method
