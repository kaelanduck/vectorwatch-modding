.class public Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;
.super Landroid/app/IntentService;
.source "RemotePushNotificationsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/RemotePushNotificationsInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RegistrationIntentService"
.end annotation


# static fields
.field private static final PLATFORM_APPLICATION_ARN:Ljava/lang/String; = "arn:aws:sns:eu-west-1:966642756382:app/GCM/VectorWatch-Android"

.field private static final TAG:Ljava/lang/String; = "RegIntentService"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "RegIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private createPlatformEndpoint(Ljava/lang/String;)V
    .locals 6
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->getSNSClient()Lcom/amazonaws/services/sns/AmazonSNSClient;

    move-result-object v0

    .line 123
    .local v0, "client":Lcom/amazonaws/services/sns/AmazonSNSClient;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "RegistrationIntentService: Create platform endpoint call."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 125
    new-instance v1, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;

    invoke-direct {v1}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;-><init>()V

    .line 126
    .local v1, "platformEndpointRequest":Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    invoke-virtual {v1, p1}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->setToken(Ljava/lang/String;)V

    .line 127
    const-string v3, "arn:aws:sns:eu-west-1:966642756382:app/GCM/VectorWatch-Android"

    invoke-virtual {v1, v3}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->setPlatformApplicationArn(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createPlatformEndpoint(Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;

    move-result-object v2

    .line 130
    .local v2, "result":Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RegistrationIntentService: Endpoint ARN: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;->getEndpointArn()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v2}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;->getEndpointArn()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->setEndpointArn(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {p0, v3}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$100(Landroid/content/Context;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method private getEndpointAttributes(Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .param p1, "endpointArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->getSNSClient()Lcom/amazonaws/services/sns/AmazonSNSClient;

    move-result-object v0

    .line 143
    .local v0, "client":Lcom/amazonaws/services/sns/AmazonSNSClient;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "RegistrationIntentService: Get endpoint attributes call."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 145
    new-instance v1, Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;

    invoke-direct {v1}, Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;-><init>()V

    .line 146
    .local v1, "endpointAttributesRequest":Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;
    invoke-virtual {v1, p1}, Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;->setEndpointArn(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v0, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->getEndpointAttributes(Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;)Lcom/amazonaws/services/sns/model/GetEndpointAttributesResult;

    move-result-object v2

    .line 149
    .local v2, "result":Lcom/amazonaws/services/sns/model/GetEndpointAttributesResult;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "RegistrationIntentService: Get endpoint attributes call result received."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v2}, Lcom/amazonaws/services/sns/model/GetEndpointAttributesResult;->getAttributes()Ljava/util/Map;

    move-result-object v3

    return-object v3
.end method

.method private getSNSClient()Lcom/amazonaws/services/sns/AmazonSNSClient;
    .locals 4

    .prologue
    .line 108
    new-instance v0, Lcom/amazonaws/auth/BasicAWSCredentials;

    const-string v2, "AKIAJBWG5LATBC6SV7FA"

    const-string v3, "ArzF3u+2nXiwbdVkkOcamOUhUYLuMt0XzDeEsbVw"

    invoke-direct {v0, v2, v3}, Lcom/amazonaws/auth/BasicAWSCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .local v0, "awsCredentials":Lcom/amazonaws/auth/AWSCredentials;
    new-instance v1, Lcom/amazonaws/services/sns/AmazonSNSClient;

    invoke-direct {v1, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentials;)V

    .line 110
    .local v1, "pushClient":Lcom/amazonaws/services/sns/AmazonSNSClient;
    sget-object v2, Lcom/amazonaws/regions/Regions;->EU_WEST_1:Lcom/amazonaws/regions/Regions;

    invoke-static {v2}, Lcom/amazonaws/regions/Region;->getRegion(Lcom/amazonaws/regions/Regions;)Lcom/amazonaws/regions/Region;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amazonaws/services/sns/AmazonSNSClient;->setRegion(Lcom/amazonaws/regions/Region;)V

    .line 112
    return-object v1
.end method

.method private setEndpointAttributes(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p1, "endpointArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->getSNSClient()Lcom/amazonaws/services/sns/AmazonSNSClient;

    move-result-object v0

    .line 163
    .local v0, "client":Lcom/amazonaws/services/sns/AmazonSNSClient;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "RegistrationIntentService: Set endpoint attributes call."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 165
    new-instance v1, Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;

    invoke-direct {v1}, Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;-><init>()V

    .line 166
    .local v1, "setEndpointAttributesRequest":Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;
    invoke-virtual {v1, p1}, Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;->setEndpointArn(Ljava/lang/String;)V

    .line 167
    invoke-virtual {v1, p2}, Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;->setAttributes(Ljava/util/Map;)V

    .line 169
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "RegistrationIntentService: Set endpoint attributes call result received."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 171
    invoke-virtual {v0, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->setEndpointAttributes(Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;)V

    .line 172
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 62
    :try_start_0
    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->getInstance()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/firebase/iid/FirebaseInstanceId;->getToken()Ljava/lang/String;

    move-result-object v4

    .line 63
    .local v4, "token":Ljava/lang/String;
    invoke-static {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "endpointArn":Ljava/lang/String;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RegistrationIntentService: endpoint = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 66
    if-nez v4, :cond_0

    .line 67
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v5

    const-string v6, "RegistrationIntentService: The GCM token is null. This should never happen!"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 105
    .end local v1    # "endpointArn":Ljava/lang/String;
    .end local v4    # "token":Ljava/lang/String;
    :goto_0
    return-void

    .line 71
    .restart local v1    # "endpointArn":Ljava/lang/String;
    .restart local v4    # "token":Ljava/lang/String;
    :cond_0
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RegistrationIntentService: GCM token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 73
    if-nez v1, :cond_1

    .line 74
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->createPlatformEndpoint(Ljava/lang/String;)V

    .line 75
    invoke-static {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 78
    :cond_1
    invoke-direct {p0, v1}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->getEndpointAttributes(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 80
    .local v3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "Token"

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "Enabled"

    .line 81
    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 82
    :cond_2
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->createPlatformEndpoint(Ljava/lang/String;)V

    .line 94
    :cond_3
    :goto_1
    invoke-static {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->syncEndpointArnToCloud(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/amazonaws/AmazonServiceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/amazonaws/AmazonClientException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 95
    .end local v1    # "endpointArn":Ljava/lang/String;
    .end local v3    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "token":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 96
    .local v2, "exception":Lcom/amazonaws/AmazonServiceException;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RegistrationIntentService: AmazonServiceException! "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/amazonaws/AmazonServiceException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v2}, Lcom/amazonaws/AmazonServiceException;->printStackTrace()V

    goto :goto_0

    .line 84
    .end local v2    # "exception":Lcom/amazonaws/AmazonServiceException;
    .restart local v1    # "endpointArn":Ljava/lang/String;
    .restart local v3    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "token":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v5, "Token"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "Enabled"

    .line 85
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "false"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 86
    :cond_5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 87
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "Token"

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    const-string v5, "Enabled"

    const-string v6, "true"

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-direct {p0, v1, v0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;->setEndpointAttributes(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_1
    .catch Lcom/amazonaws/AmazonServiceException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/amazonaws/AmazonClientException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 98
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "endpointArn":Ljava/lang/String;
    .end local v3    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "token":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 99
    .local v2, "exception":Lcom/amazonaws/AmazonClientException;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RegistrationIntentService: AmazonClientException! "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/amazonaws/AmazonClientException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v2}, Lcom/amazonaws/AmazonClientException;->printStackTrace()V

    goto/16 :goto_0

    .line 101
    .end local v2    # "exception":Lcom/amazonaws/AmazonClientException;
    :catch_2
    move-exception v2

    .line 102
    .local v2, "exception":Ljava/lang/Exception;
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RegistrationIntentService: General exception! "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method
