.class Lcom/vectorwatch/android/VectorApplication$9;
.super Ljava/lang/Object;
.source "VectorApplication.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/VectorApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/VectorApplication;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/VectorApplication;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/VectorApplication;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/vectorwatch/android/VectorApplication$9;->this$0:Lcom/vectorwatch/android/VectorApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 240
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentActivity(Landroid/app/Activity;)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 241
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 272
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 257
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 250
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentActivity(Landroid/app/Activity;)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 251
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->onResume()V

    .line 252
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 267
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 246
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 262
    return-void
.end method
