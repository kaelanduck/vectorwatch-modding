.class public final Lcom/vectorwatch/android/R$raw;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "raw"
.end annotation


# static fields
.field public static final gtm_analytics:I = 0x7f070000

.field public static final joda_africa_abidjan:I = 0x7f070001

.field public static final joda_africa_accra:I = 0x7f070002

.field public static final joda_africa_addis_ababa:I = 0x7f070003

.field public static final joda_africa_algiers:I = 0x7f070004

.field public static final joda_africa_asmara:I = 0x7f070005

.field public static final joda_africa_asmera:I = 0x7f070006

.field public static final joda_africa_bamako:I = 0x7f070007

.field public static final joda_africa_bangui:I = 0x7f070008

.field public static final joda_africa_banjul:I = 0x7f070009

.field public static final joda_africa_bissau:I = 0x7f07000a

.field public static final joda_africa_blantyre:I = 0x7f07000b

.field public static final joda_africa_brazzaville:I = 0x7f07000c

.field public static final joda_africa_bujumbura:I = 0x7f07000d

.field public static final joda_africa_cairo:I = 0x7f07000e

.field public static final joda_africa_casablanca:I = 0x7f07000f

.field public static final joda_africa_ceuta:I = 0x7f070010

.field public static final joda_africa_conakry:I = 0x7f070011

.field public static final joda_africa_dakar:I = 0x7f070012

.field public static final joda_africa_dar_es_salaam:I = 0x7f070013

.field public static final joda_africa_djibouti:I = 0x7f070014

.field public static final joda_africa_douala:I = 0x7f070015

.field public static final joda_africa_el_aaiun:I = 0x7f070016

.field public static final joda_africa_freetown:I = 0x7f070017

.field public static final joda_africa_gaborone:I = 0x7f070018

.field public static final joda_africa_harare:I = 0x7f070019

.field public static final joda_africa_johannesburg:I = 0x7f07001a

.field public static final joda_africa_juba:I = 0x7f07001b

.field public static final joda_africa_kampala:I = 0x7f07001c

.field public static final joda_africa_khartoum:I = 0x7f07001d

.field public static final joda_africa_kigali:I = 0x7f07001e

.field public static final joda_africa_kinshasa:I = 0x7f07001f

.field public static final joda_africa_lagos:I = 0x7f070020

.field public static final joda_africa_libreville:I = 0x7f070021

.field public static final joda_africa_lome:I = 0x7f070022

.field public static final joda_africa_luanda:I = 0x7f070023

.field public static final joda_africa_lubumbashi:I = 0x7f070024

.field public static final joda_africa_lusaka:I = 0x7f070025

.field public static final joda_africa_malabo:I = 0x7f070026

.field public static final joda_africa_maputo:I = 0x7f070027

.field public static final joda_africa_maseru:I = 0x7f070028

.field public static final joda_africa_mbabane:I = 0x7f070029

.field public static final joda_africa_mogadishu:I = 0x7f07002a

.field public static final joda_africa_monrovia:I = 0x7f07002b

.field public static final joda_africa_nairobi:I = 0x7f07002c

.field public static final joda_africa_ndjamena:I = 0x7f07002d

.field public static final joda_africa_niamey:I = 0x7f07002e

.field public static final joda_africa_nouakchott:I = 0x7f07002f

.field public static final joda_africa_ouagadougou:I = 0x7f070030

.field public static final joda_africa_porto_novo:I = 0x7f070031

.field public static final joda_africa_sao_tome:I = 0x7f070032

.field public static final joda_africa_timbuktu:I = 0x7f070033

.field public static final joda_africa_tripoli:I = 0x7f070034

.field public static final joda_africa_tunis:I = 0x7f070035

.field public static final joda_africa_windhoek:I = 0x7f070036

.field public static final joda_america_adak:I = 0x7f070037

.field public static final joda_america_anchorage:I = 0x7f070038

.field public static final joda_america_anguilla:I = 0x7f070039

.field public static final joda_america_antigua:I = 0x7f07003a

.field public static final joda_america_araguaina:I = 0x7f07003b

.field public static final joda_america_argentina_buenos_aires:I = 0x7f07003c

.field public static final joda_america_argentina_catamarca:I = 0x7f07003d

.field public static final joda_america_argentina_comodrivadavia:I = 0x7f07003e

.field public static final joda_america_argentina_cordoba:I = 0x7f07003f

.field public static final joda_america_argentina_jujuy:I = 0x7f070040

.field public static final joda_america_argentina_la_rioja:I = 0x7f070041

.field public static final joda_america_argentina_mendoza:I = 0x7f070042

.field public static final joda_america_argentina_rio_gallegos:I = 0x7f070043

.field public static final joda_america_argentina_salta:I = 0x7f070044

.field public static final joda_america_argentina_san_juan:I = 0x7f070045

.field public static final joda_america_argentina_san_luis:I = 0x7f070046

.field public static final joda_america_argentina_tucuman:I = 0x7f070047

.field public static final joda_america_argentina_ushuaia:I = 0x7f070048

.field public static final joda_america_aruba:I = 0x7f070049

.field public static final joda_america_asuncion:I = 0x7f07004a

.field public static final joda_america_atikokan:I = 0x7f07004b

.field public static final joda_america_bahia:I = 0x7f07004c

.field public static final joda_america_bahia_banderas:I = 0x7f07004d

.field public static final joda_america_barbados:I = 0x7f07004e

.field public static final joda_america_belem:I = 0x7f07004f

.field public static final joda_america_belize:I = 0x7f070050

.field public static final joda_america_blanc_sablon:I = 0x7f070051

.field public static final joda_america_boa_vista:I = 0x7f070052

.field public static final joda_america_bogota:I = 0x7f070053

.field public static final joda_america_boise:I = 0x7f070054

.field public static final joda_america_cambridge_bay:I = 0x7f070055

.field public static final joda_america_campo_grande:I = 0x7f070056

.field public static final joda_america_cancun:I = 0x7f070057

.field public static final joda_america_caracas:I = 0x7f070058

.field public static final joda_america_cayenne:I = 0x7f070059

.field public static final joda_america_cayman:I = 0x7f07005a

.field public static final joda_america_chicago:I = 0x7f07005b

.field public static final joda_america_chihuahua:I = 0x7f07005c

.field public static final joda_america_coral_harbour:I = 0x7f07005d

.field public static final joda_america_costa_rica:I = 0x7f07005e

.field public static final joda_america_creston:I = 0x7f07005f

.field public static final joda_america_cuiaba:I = 0x7f070060

.field public static final joda_america_curacao:I = 0x7f070061

.field public static final joda_america_danmarkshavn:I = 0x7f070062

.field public static final joda_america_dawson:I = 0x7f070063

.field public static final joda_america_dawson_creek:I = 0x7f070064

.field public static final joda_america_denver:I = 0x7f070065

.field public static final joda_america_detroit:I = 0x7f070066

.field public static final joda_america_dominica:I = 0x7f070067

.field public static final joda_america_edmonton:I = 0x7f070068

.field public static final joda_america_eirunepe:I = 0x7f070069

.field public static final joda_america_el_salvador:I = 0x7f07006a

.field public static final joda_america_ensenada:I = 0x7f07006b

.field public static final joda_america_fort_nelson:I = 0x7f07006c

.field public static final joda_america_fortaleza:I = 0x7f07006d

.field public static final joda_america_glace_bay:I = 0x7f07006e

.field public static final joda_america_godthab:I = 0x7f07006f

.field public static final joda_america_goose_bay:I = 0x7f070070

.field public static final joda_america_grand_turk:I = 0x7f070071

.field public static final joda_america_grenada:I = 0x7f070072

.field public static final joda_america_guadeloupe:I = 0x7f070073

.field public static final joda_america_guatemala:I = 0x7f070074

.field public static final joda_america_guayaquil:I = 0x7f070075

.field public static final joda_america_guyana:I = 0x7f070076

.field public static final joda_america_halifax:I = 0x7f070077

.field public static final joda_america_havana:I = 0x7f070078

.field public static final joda_america_hermosillo:I = 0x7f070079

.field public static final joda_america_indiana_indianapolis:I = 0x7f07007a

.field public static final joda_america_indiana_knox:I = 0x7f07007b

.field public static final joda_america_indiana_marengo:I = 0x7f07007c

.field public static final joda_america_indiana_petersburg:I = 0x7f07007d

.field public static final joda_america_indiana_tell_city:I = 0x7f07007e

.field public static final joda_america_indiana_vevay:I = 0x7f07007f

.field public static final joda_america_indiana_vincennes:I = 0x7f070080

.field public static final joda_america_indiana_winamac:I = 0x7f070081

.field public static final joda_america_inuvik:I = 0x7f070082

.field public static final joda_america_iqaluit:I = 0x7f070083

.field public static final joda_america_jamaica:I = 0x7f070084

.field public static final joda_america_juneau:I = 0x7f070085

.field public static final joda_america_kentucky_louisville:I = 0x7f070086

.field public static final joda_america_kentucky_monticello:I = 0x7f070087

.field public static final joda_america_kralendijk:I = 0x7f070088

.field public static final joda_america_la_paz:I = 0x7f070089

.field public static final joda_america_lima:I = 0x7f07008a

.field public static final joda_america_los_angeles:I = 0x7f07008b

.field public static final joda_america_lower_princes:I = 0x7f07008c

.field public static final joda_america_maceio:I = 0x7f07008d

.field public static final joda_america_managua:I = 0x7f07008e

.field public static final joda_america_manaus:I = 0x7f07008f

.field public static final joda_america_marigot:I = 0x7f070090

.field public static final joda_america_martinique:I = 0x7f070091

.field public static final joda_america_matamoros:I = 0x7f070092

.field public static final joda_america_mazatlan:I = 0x7f070093

.field public static final joda_america_menominee:I = 0x7f070094

.field public static final joda_america_merida:I = 0x7f070095

.field public static final joda_america_metlakatla:I = 0x7f070096

.field public static final joda_america_mexico_city:I = 0x7f070097

.field public static final joda_america_miquelon:I = 0x7f070098

.field public static final joda_america_moncton:I = 0x7f070099

.field public static final joda_america_monterrey:I = 0x7f07009a

.field public static final joda_america_montevideo:I = 0x7f07009b

.field public static final joda_america_montreal:I = 0x7f07009c

.field public static final joda_america_montserrat:I = 0x7f07009d

.field public static final joda_america_nassau:I = 0x7f07009e

.field public static final joda_america_new_york:I = 0x7f07009f

.field public static final joda_america_nipigon:I = 0x7f0700a0

.field public static final joda_america_nome:I = 0x7f0700a1

.field public static final joda_america_noronha:I = 0x7f0700a2

.field public static final joda_america_north_dakota_beulah:I = 0x7f0700a3

.field public static final joda_america_north_dakota_center:I = 0x7f0700a4

.field public static final joda_america_north_dakota_new_salem:I = 0x7f0700a5

.field public static final joda_america_ojinaga:I = 0x7f0700a6

.field public static final joda_america_panama:I = 0x7f0700a7

.field public static final joda_america_pangnirtung:I = 0x7f0700a8

.field public static final joda_america_paramaribo:I = 0x7f0700a9

.field public static final joda_america_phoenix:I = 0x7f0700aa

.field public static final joda_america_port_au_prince:I = 0x7f0700ab

.field public static final joda_america_port_of_spain:I = 0x7f0700ac

.field public static final joda_america_porto_velho:I = 0x7f0700ad

.field public static final joda_america_puerto_rico:I = 0x7f0700ae

.field public static final joda_america_rainy_river:I = 0x7f0700af

.field public static final joda_america_rankin_inlet:I = 0x7f0700b0

.field public static final joda_america_recife:I = 0x7f0700b1

.field public static final joda_america_regina:I = 0x7f0700b2

.field public static final joda_america_resolute:I = 0x7f0700b3

.field public static final joda_america_rio_branco:I = 0x7f0700b4

.field public static final joda_america_rosario:I = 0x7f0700b5

.field public static final joda_america_santarem:I = 0x7f0700b6

.field public static final joda_america_santiago:I = 0x7f0700b7

.field public static final joda_america_santo_domingo:I = 0x7f0700b8

.field public static final joda_america_sao_paulo:I = 0x7f0700b9

.field public static final joda_america_scoresbysund:I = 0x7f0700ba

.field public static final joda_america_sitka:I = 0x7f0700bb

.field public static final joda_america_st_barthelemy:I = 0x7f0700bc

.field public static final joda_america_st_johns:I = 0x7f0700bd

.field public static final joda_america_st_kitts:I = 0x7f0700be

.field public static final joda_america_st_lucia:I = 0x7f0700bf

.field public static final joda_america_st_thomas:I = 0x7f0700c0

.field public static final joda_america_st_vincent:I = 0x7f0700c1

.field public static final joda_america_swift_current:I = 0x7f0700c2

.field public static final joda_america_tegucigalpa:I = 0x7f0700c3

.field public static final joda_america_thule:I = 0x7f0700c4

.field public static final joda_america_thunder_bay:I = 0x7f0700c5

.field public static final joda_america_tijuana:I = 0x7f0700c6

.field public static final joda_america_toronto:I = 0x7f0700c7

.field public static final joda_america_tortola:I = 0x7f0700c8

.field public static final joda_america_vancouver:I = 0x7f0700c9

.field public static final joda_america_whitehorse:I = 0x7f0700ca

.field public static final joda_america_winnipeg:I = 0x7f0700cb

.field public static final joda_america_yakutat:I = 0x7f0700cc

.field public static final joda_america_yellowknife:I = 0x7f0700cd

.field public static final joda_antarctica_casey:I = 0x7f0700ce

.field public static final joda_antarctica_davis:I = 0x7f0700cf

.field public static final joda_antarctica_dumontdurville:I = 0x7f0700d0

.field public static final joda_antarctica_macquarie:I = 0x7f0700d1

.field public static final joda_antarctica_mawson:I = 0x7f0700d2

.field public static final joda_antarctica_mcmurdo:I = 0x7f0700d3

.field public static final joda_antarctica_palmer:I = 0x7f0700d4

.field public static final joda_antarctica_rothera:I = 0x7f0700d5

.field public static final joda_antarctica_south_pole:I = 0x7f0700d6

.field public static final joda_antarctica_syowa:I = 0x7f0700d7

.field public static final joda_antarctica_troll:I = 0x7f0700d8

.field public static final joda_antarctica_vostok:I = 0x7f0700d9

.field public static final joda_arctic_longyearbyen:I = 0x7f0700da

.field public static final joda_asia_aden:I = 0x7f0700db

.field public static final joda_asia_almaty:I = 0x7f0700dc

.field public static final joda_asia_amman:I = 0x7f0700dd

.field public static final joda_asia_anadyr:I = 0x7f0700de

.field public static final joda_asia_aqtau:I = 0x7f0700df

.field public static final joda_asia_aqtobe:I = 0x7f0700e0

.field public static final joda_asia_ashgabat:I = 0x7f0700e1

.field public static final joda_asia_baghdad:I = 0x7f0700e2

.field public static final joda_asia_bahrain:I = 0x7f0700e3

.field public static final joda_asia_baku:I = 0x7f0700e4

.field public static final joda_asia_bangkok:I = 0x7f0700e5

.field public static final joda_asia_beirut:I = 0x7f0700e6

.field public static final joda_asia_bishkek:I = 0x7f0700e7

.field public static final joda_asia_brunei:I = 0x7f0700e8

.field public static final joda_asia_chita:I = 0x7f0700e9

.field public static final joda_asia_choibalsan:I = 0x7f0700ea

.field public static final joda_asia_chongqing:I = 0x7f0700eb

.field public static final joda_asia_chungking:I = 0x7f0700ec

.field public static final joda_asia_colombo:I = 0x7f0700ed

.field public static final joda_asia_damascus:I = 0x7f0700ee

.field public static final joda_asia_dhaka:I = 0x7f0700ef

.field public static final joda_asia_dili:I = 0x7f0700f0

.field public static final joda_asia_dubai:I = 0x7f0700f1

.field public static final joda_asia_dushanbe:I = 0x7f0700f2

.field public static final joda_asia_gaza:I = 0x7f0700f3

.field public static final joda_asia_hanoi:I = 0x7f0700f4

.field public static final joda_asia_harbin:I = 0x7f0700f5

.field public static final joda_asia_hebron:I = 0x7f0700f6

.field public static final joda_asia_ho_chi_minh:I = 0x7f0700f7

.field public static final joda_asia_hong_kong:I = 0x7f0700f8

.field public static final joda_asia_hovd:I = 0x7f0700f9

.field public static final joda_asia_irkutsk:I = 0x7f0700fa

.field public static final joda_asia_istanbul:I = 0x7f0700fb

.field public static final joda_asia_jakarta:I = 0x7f0700fc

.field public static final joda_asia_jayapura:I = 0x7f0700fd

.field public static final joda_asia_jerusalem:I = 0x7f0700fe

.field public static final joda_asia_kabul:I = 0x7f0700ff

.field public static final joda_asia_kamchatka:I = 0x7f070100

.field public static final joda_asia_karachi:I = 0x7f070101

.field public static final joda_asia_kashgar:I = 0x7f070102

.field public static final joda_asia_kathmandu:I = 0x7f070103

.field public static final joda_asia_khandyga:I = 0x7f070104

.field public static final joda_asia_kolkata:I = 0x7f070105

.field public static final joda_asia_krasnoyarsk:I = 0x7f070106

.field public static final joda_asia_kuala_lumpur:I = 0x7f070107

.field public static final joda_asia_kuching:I = 0x7f070108

.field public static final joda_asia_kuwait:I = 0x7f070109

.field public static final joda_asia_macau:I = 0x7f07010a

.field public static final joda_asia_magadan:I = 0x7f07010b

.field public static final joda_asia_makassar:I = 0x7f07010c

.field public static final joda_asia_manila:I = 0x7f07010d

.field public static final joda_asia_muscat:I = 0x7f07010e

.field public static final joda_asia_nicosia:I = 0x7f07010f

.field public static final joda_asia_novokuznetsk:I = 0x7f070110

.field public static final joda_asia_novosibirsk:I = 0x7f070111

.field public static final joda_asia_omsk:I = 0x7f070112

.field public static final joda_asia_oral:I = 0x7f070113

.field public static final joda_asia_phnom_penh:I = 0x7f070114

.field public static final joda_asia_pontianak:I = 0x7f070115

.field public static final joda_asia_pyongyang:I = 0x7f070116

.field public static final joda_asia_qatar:I = 0x7f070117

.field public static final joda_asia_qyzylorda:I = 0x7f070118

.field public static final joda_asia_rangoon:I = 0x7f070119

.field public static final joda_asia_riyadh:I = 0x7f07011a

.field public static final joda_asia_sakhalin:I = 0x7f07011b

.field public static final joda_asia_samarkand:I = 0x7f07011c

.field public static final joda_asia_seoul:I = 0x7f07011d

.field public static final joda_asia_shanghai:I = 0x7f07011e

.field public static final joda_asia_singapore:I = 0x7f07011f

.field public static final joda_asia_srednekolymsk:I = 0x7f070120

.field public static final joda_asia_taipei:I = 0x7f070121

.field public static final joda_asia_tashkent:I = 0x7f070122

.field public static final joda_asia_tbilisi:I = 0x7f070123

.field public static final joda_asia_tehran:I = 0x7f070124

.field public static final joda_asia_tel_aviv:I = 0x7f070125

.field public static final joda_asia_thimphu:I = 0x7f070126

.field public static final joda_asia_tokyo:I = 0x7f070127

.field public static final joda_asia_ulaanbaatar:I = 0x7f070128

.field public static final joda_asia_urumqi:I = 0x7f070129

.field public static final joda_asia_ust_nera:I = 0x7f07012a

.field public static final joda_asia_vientiane:I = 0x7f07012b

.field public static final joda_asia_vladivostok:I = 0x7f07012c

.field public static final joda_asia_yakutsk:I = 0x7f07012d

.field public static final joda_asia_yekaterinburg:I = 0x7f07012e

.field public static final joda_asia_yerevan:I = 0x7f07012f

.field public static final joda_atlantic_azores:I = 0x7f070130

.field public static final joda_atlantic_bermuda:I = 0x7f070131

.field public static final joda_atlantic_canary:I = 0x7f070132

.field public static final joda_atlantic_cape_verde:I = 0x7f070133

.field public static final joda_atlantic_faroe:I = 0x7f070134

.field public static final joda_atlantic_jan_mayen:I = 0x7f070135

.field public static final joda_atlantic_madeira:I = 0x7f070136

.field public static final joda_atlantic_reykjavik:I = 0x7f070137

.field public static final joda_atlantic_south_georgia:I = 0x7f070138

.field public static final joda_atlantic_st_helena:I = 0x7f070139

.field public static final joda_atlantic_stanley:I = 0x7f07013a

.field public static final joda_australia_adelaide:I = 0x7f07013b

.field public static final joda_australia_brisbane:I = 0x7f07013c

.field public static final joda_australia_broken_hill:I = 0x7f07013d

.field public static final joda_australia_currie:I = 0x7f07013e

.field public static final joda_australia_darwin:I = 0x7f07013f

.field public static final joda_australia_eucla:I = 0x7f070140

.field public static final joda_australia_hobart:I = 0x7f070141

.field public static final joda_australia_lindeman:I = 0x7f070142

.field public static final joda_australia_lord_howe:I = 0x7f070143

.field public static final joda_australia_melbourne:I = 0x7f070144

.field public static final joda_australia_perth:I = 0x7f070145

.field public static final joda_australia_sydney:I = 0x7f070146

.field public static final joda_cet:I = 0x7f070147

.field public static final joda_cst6cdt:I = 0x7f070148

.field public static final joda_eet:I = 0x7f070149

.field public static final joda_est:I = 0x7f07014a

.field public static final joda_est5edt:I = 0x7f07014b

.field public static final joda_etc_gmt:I = 0x7f07014c

.field public static final joda_etc_gmt_1:I = 0x7f07014d

.field public static final joda_etc_gmt_10:I = 0x7f07014e

.field public static final joda_etc_gmt_11:I = 0x7f07014f

.field public static final joda_etc_gmt_12:I = 0x7f070150

.field public static final joda_etc_gmt_13:I = 0x7f070151

.field public static final joda_etc_gmt_14:I = 0x7f070152

.field public static final joda_etc_gmt_2:I = 0x7f070153

.field public static final joda_etc_gmt_3:I = 0x7f070154

.field public static final joda_etc_gmt_4:I = 0x7f070155

.field public static final joda_etc_gmt_5:I = 0x7f070156

.field public static final joda_etc_gmt_6:I = 0x7f070157

.field public static final joda_etc_gmt_7:I = 0x7f070158

.field public static final joda_etc_gmt_8:I = 0x7f070159

.field public static final joda_etc_gmt_9:I = 0x7f07015a

.field public static final joda_etc_gmtplus1:I = 0x7f07015b

.field public static final joda_etc_gmtplus10:I = 0x7f07015c

.field public static final joda_etc_gmtplus11:I = 0x7f07015d

.field public static final joda_etc_gmtplus12:I = 0x7f07015e

.field public static final joda_etc_gmtplus2:I = 0x7f07015f

.field public static final joda_etc_gmtplus3:I = 0x7f070160

.field public static final joda_etc_gmtplus4:I = 0x7f070161

.field public static final joda_etc_gmtplus5:I = 0x7f070162

.field public static final joda_etc_gmtplus6:I = 0x7f070163

.field public static final joda_etc_gmtplus7:I = 0x7f070164

.field public static final joda_etc_gmtplus8:I = 0x7f070165

.field public static final joda_etc_gmtplus9:I = 0x7f070166

.field public static final joda_etc_uct:I = 0x7f070167

.field public static final joda_etc_utc:I = 0x7f070168

.field public static final joda_europe_amsterdam:I = 0x7f070169

.field public static final joda_europe_andorra:I = 0x7f07016a

.field public static final joda_europe_athens:I = 0x7f07016b

.field public static final joda_europe_belfast:I = 0x7f07016c

.field public static final joda_europe_belgrade:I = 0x7f07016d

.field public static final joda_europe_berlin:I = 0x7f07016e

.field public static final joda_europe_bratislava:I = 0x7f07016f

.field public static final joda_europe_brussels:I = 0x7f070170

.field public static final joda_europe_bucharest:I = 0x7f070171

.field public static final joda_europe_budapest:I = 0x7f070172

.field public static final joda_europe_busingen:I = 0x7f070173

.field public static final joda_europe_chisinau:I = 0x7f070174

.field public static final joda_europe_copenhagen:I = 0x7f070175

.field public static final joda_europe_dublin:I = 0x7f070176

.field public static final joda_europe_gibraltar:I = 0x7f070177

.field public static final joda_europe_guernsey:I = 0x7f070178

.field public static final joda_europe_helsinki:I = 0x7f070179

.field public static final joda_europe_isle_of_man:I = 0x7f07017a

.field public static final joda_europe_istanbul:I = 0x7f07017b

.field public static final joda_europe_jersey:I = 0x7f07017c

.field public static final joda_europe_kaliningrad:I = 0x7f07017d

.field public static final joda_europe_kiev:I = 0x7f07017e

.field public static final joda_europe_lisbon:I = 0x7f07017f

.field public static final joda_europe_ljubljana:I = 0x7f070180

.field public static final joda_europe_london:I = 0x7f070181

.field public static final joda_europe_luxembourg:I = 0x7f070182

.field public static final joda_europe_madrid:I = 0x7f070183

.field public static final joda_europe_malta:I = 0x7f070184

.field public static final joda_europe_mariehamn:I = 0x7f070185

.field public static final joda_europe_minsk:I = 0x7f070186

.field public static final joda_europe_monaco:I = 0x7f070187

.field public static final joda_europe_moscow:I = 0x7f070188

.field public static final joda_europe_nicosia:I = 0x7f070189

.field public static final joda_europe_oslo:I = 0x7f07018a

.field public static final joda_europe_paris:I = 0x7f07018b

.field public static final joda_europe_podgorica:I = 0x7f07018c

.field public static final joda_europe_prague:I = 0x7f07018d

.field public static final joda_europe_riga:I = 0x7f07018e

.field public static final joda_europe_rome:I = 0x7f07018f

.field public static final joda_europe_samara:I = 0x7f070190

.field public static final joda_europe_san_marino:I = 0x7f070191

.field public static final joda_europe_sarajevo:I = 0x7f070192

.field public static final joda_europe_simferopol:I = 0x7f070193

.field public static final joda_europe_skopje:I = 0x7f070194

.field public static final joda_europe_sofia:I = 0x7f070195

.field public static final joda_europe_stockholm:I = 0x7f070196

.field public static final joda_europe_tallinn:I = 0x7f070197

.field public static final joda_europe_tirane:I = 0x7f070198

.field public static final joda_europe_tiraspol:I = 0x7f070199

.field public static final joda_europe_uzhgorod:I = 0x7f07019a

.field public static final joda_europe_vaduz:I = 0x7f07019b

.field public static final joda_europe_vatican:I = 0x7f07019c

.field public static final joda_europe_vienna:I = 0x7f07019d

.field public static final joda_europe_vilnius:I = 0x7f07019e

.field public static final joda_europe_volgograd:I = 0x7f07019f

.field public static final joda_europe_warsaw:I = 0x7f0701a0

.field public static final joda_europe_zagreb:I = 0x7f0701a1

.field public static final joda_europe_zaporozhye:I = 0x7f0701a2

.field public static final joda_europe_zurich:I = 0x7f0701a3

.field public static final joda_hst:I = 0x7f0701a4

.field public static final joda_indian_antananarivo:I = 0x7f0701a5

.field public static final joda_indian_chagos:I = 0x7f0701a6

.field public static final joda_indian_christmas:I = 0x7f0701a7

.field public static final joda_indian_cocos:I = 0x7f0701a8

.field public static final joda_indian_comoro:I = 0x7f0701a9

.field public static final joda_indian_kerguelen:I = 0x7f0701aa

.field public static final joda_indian_mahe:I = 0x7f0701ab

.field public static final joda_indian_maldives:I = 0x7f0701ac

.field public static final joda_indian_mauritius:I = 0x7f0701ad

.field public static final joda_indian_mayotte:I = 0x7f0701ae

.field public static final joda_indian_reunion:I = 0x7f0701af

.field public static final joda_keep:I = 0x7f0701b0

.field public static final joda_met:I = 0x7f0701b1

.field public static final joda_mst:I = 0x7f0701b2

.field public static final joda_mst7mdt:I = 0x7f0701b3

.field public static final joda_pacific_apia:I = 0x7f0701b4

.field public static final joda_pacific_auckland:I = 0x7f0701b5

.field public static final joda_pacific_bougainville:I = 0x7f0701b6

.field public static final joda_pacific_chatham:I = 0x7f0701b7

.field public static final joda_pacific_chuuk:I = 0x7f0701b8

.field public static final joda_pacific_easter:I = 0x7f0701b9

.field public static final joda_pacific_efate:I = 0x7f0701ba

.field public static final joda_pacific_enderbury:I = 0x7f0701bb

.field public static final joda_pacific_fakaofo:I = 0x7f0701bc

.field public static final joda_pacific_fiji:I = 0x7f0701bd

.field public static final joda_pacific_funafuti:I = 0x7f0701be

.field public static final joda_pacific_galapagos:I = 0x7f0701bf

.field public static final joda_pacific_gambier:I = 0x7f0701c0

.field public static final joda_pacific_guadalcanal:I = 0x7f0701c1

.field public static final joda_pacific_guam:I = 0x7f0701c2

.field public static final joda_pacific_honolulu:I = 0x7f0701c3

.field public static final joda_pacific_johnston:I = 0x7f0701c4

.field public static final joda_pacific_kiritimati:I = 0x7f0701c5

.field public static final joda_pacific_kosrae:I = 0x7f0701c6

.field public static final joda_pacific_kwajalein:I = 0x7f0701c7

.field public static final joda_pacific_majuro:I = 0x7f0701c8

.field public static final joda_pacific_marquesas:I = 0x7f0701c9

.field public static final joda_pacific_midway:I = 0x7f0701ca

.field public static final joda_pacific_nauru:I = 0x7f0701cb

.field public static final joda_pacific_niue:I = 0x7f0701cc

.field public static final joda_pacific_norfolk:I = 0x7f0701cd

.field public static final joda_pacific_noumea:I = 0x7f0701ce

.field public static final joda_pacific_pago_pago:I = 0x7f0701cf

.field public static final joda_pacific_palau:I = 0x7f0701d0

.field public static final joda_pacific_pitcairn:I = 0x7f0701d1

.field public static final joda_pacific_pohnpei:I = 0x7f0701d2

.field public static final joda_pacific_port_moresby:I = 0x7f0701d3

.field public static final joda_pacific_rarotonga:I = 0x7f0701d4

.field public static final joda_pacific_saipan:I = 0x7f0701d5

.field public static final joda_pacific_tahiti:I = 0x7f0701d6

.field public static final joda_pacific_tarawa:I = 0x7f0701d7

.field public static final joda_pacific_tongatapu:I = 0x7f0701d8

.field public static final joda_pacific_wake:I = 0x7f0701d9

.field public static final joda_pacific_wallis:I = 0x7f0701da

.field public static final joda_pst8pdt:I = 0x7f0701db

.field public static final joda_wet:I = 0x7f0701dc

.field public static final joda_zoneinfomap:I = 0x7f0701dd

.field public static final navigation:I = 0x7f0701de

.field public static final notifications:I = 0x7f0701df

.field public static final stream:I = 0x7f0701e0

.field public static final tibetan_bell:I = 0x7f0701e1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
