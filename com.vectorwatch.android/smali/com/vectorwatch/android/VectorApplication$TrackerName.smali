.class public final enum Lcom/vectorwatch/android/VectorApplication$TrackerName;
.super Ljava/lang/Enum;
.source "VectorApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/VectorApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrackerName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/VectorApplication$TrackerName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/VectorApplication$TrackerName;

.field public static final enum ANDROID_APP_TRACKER:Lcom/vectorwatch/android/VectorApplication$TrackerName;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 389
    new-instance v0, Lcom/vectorwatch/android/VectorApplication$TrackerName;

    const-string v1, "ANDROID_APP_TRACKER"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/VectorApplication$TrackerName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/VectorApplication$TrackerName;->ANDROID_APP_TRACKER:Lcom/vectorwatch/android/VectorApplication$TrackerName;

    .line 388
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vectorwatch/android/VectorApplication$TrackerName;

    sget-object v1, Lcom/vectorwatch/android/VectorApplication$TrackerName;->ANDROID_APP_TRACKER:Lcom/vectorwatch/android/VectorApplication$TrackerName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/vectorwatch/android/VectorApplication$TrackerName;->$VALUES:[Lcom/vectorwatch/android/VectorApplication$TrackerName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 388
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/VectorApplication$TrackerName;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 388
    const-class v0, Lcom/vectorwatch/android/VectorApplication$TrackerName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication$TrackerName;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/VectorApplication$TrackerName;
    .locals 1

    .prologue
    .line 388
    sget-object v0, Lcom/vectorwatch/android/VectorApplication$TrackerName;->$VALUES:[Lcom/vectorwatch/android/VectorApplication$TrackerName;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/VectorApplication$TrackerName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/VectorApplication$TrackerName;

    return-object v0
.end method
