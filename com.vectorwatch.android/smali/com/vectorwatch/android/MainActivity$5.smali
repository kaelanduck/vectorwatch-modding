.class Lcom/vectorwatch/android/MainActivity$5;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/MainActivity;->buildFitnessClient()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/MainActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 790
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity$5;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 5
    .param p1, "result"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    const/4 v4, 0x0

    .line 793
    # getter for: Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/MainActivity;->access$300()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FITNESS_API: Google Play services connection failed. Cause: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 794
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 793
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 796
    const-string v1, "flag_sync_to_google_fit"

    iget-object v2, p0, Lcom/vectorwatch/android/MainActivity$5;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 799
    .local v0, "isSetToSync":Z
    if-eqz v0, :cond_0

    .line 800
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$5;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090110

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    iget-object v3, p0, Lcom/vectorwatch/android/MainActivity$5;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 802
    const-string v1, "flag_sync_to_google_fit"

    iget-object v2, p0, Lcom/vectorwatch/android/MainActivity$5;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 804
    const/4 v1, 0x0

    sput-object v1, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 806
    :cond_0
    return-void
.end method
