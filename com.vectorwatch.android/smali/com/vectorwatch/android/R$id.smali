.class public final Lcom/vectorwatch/android/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BIG:I = 0x7f10001d

.field public static final DEFAULT:I = 0x7f10001e

.field public static final MINI:I = 0x7f10001f

.field public static final accountPassword:I = 0x7f100103

.field public static final accountUsername:I = 0x7f100102

.field public static final account_details:I = 0x7f100284

.field public static final account_profile:I = 0x7f1001c3

.field public static final action0:I = 0x7f100240

.field public static final actionButton:I = 0x7f10023a

.field public static final action_add:I = 0x7f1002ee

.field public static final action_bar:I = 0x7f100081

.field public static final action_bar_activity_content:I = 0x7f100000

.field public static final action_bar_container:I = 0x7f100080

.field public static final action_bar_root:I = 0x7f10007c

.field public static final action_bar_spinner:I = 0x7f100001

.field public static final action_bar_subtitle:I = 0x7f100065

.field public static final action_bar_title:I = 0x7f100064

.field public static final action_button:I = 0x7f100150

.field public static final action_context_bar:I = 0x7f100082

.field public static final action_divider:I = 0x7f100244

.field public static final action_menu_divider:I = 0x7f100002

.field public static final action_menu_presenter:I = 0x7f100003

.field public static final action_mode_bar:I = 0x7f10007e

.field public static final action_mode_bar_stub:I = 0x7f10007d

.field public static final action_mode_close_button:I = 0x7f100066

.field public static final action_settings:I = 0x7f1002ea

.field public static final actions_list_view:I = 0x7f1001d5

.field public static final activityAlertsButton:I = 0x7f1001fa

.field public static final activityInfoAge:I = 0x7f10028c

.field public static final activityInfoGender:I = 0x7f10028b

.field public static final activityInfoHeight:I = 0x7f10028d

.field public static final activityInfoWeight:I = 0x7f10028e

.field public static final activityListView:I = 0x7f1001b6

.field public static final activityQuadrantsLayout:I = 0x7f1000ca

.field public static final activityTopLayout:I = 0x7f1001e1

.field public static final activity_alerts:I = 0x7f100290

.field public static final activity_chooser_view_content:I = 0x7f100067

.field public static final activity_info:I = 0x7f1001c4

.field public static final activity_info_sync_to_google_fit:I = 0x7f10028f

.field public static final activity_notifications_alert:I = 0x7f10010c

.field public static final activity_reminder:I = 0x7f100293

.field public static final addDummySteps:I = 0x7f100098

.field public static final address:I = 0x7f1001a6

.field public static final adjust_height:I = 0x7f100039

.field public static final adjust_width:I = 0x7f10003a

.field public static final advanced_button:I = 0x7f100279

.field public static final alarmCreateButton:I = 0x7f1000a2

.field public static final alarmEditNameText:I = 0x7f1001af

.field public static final alarmEditSummaryText:I = 0x7f1001ae

.field public static final alarmEditTimePicker:I = 0x7f1001ad

.field public static final alarmListView:I = 0x7f1000a3

.field public static final alarmTimePicker:I = 0x7f1000a1

.field public static final alarm_time:I = 0x7f100189

.field public static final alarm_title:I = 0x7f10018a

.field public static final alarm_toggle:I = 0x7f10018b

.field public static final alarms:I = 0x7f1001c9

.field public static final alarmsLayout:I = 0x7f1000a0

.field public static final alarms_alert:I = 0x7f100297

.field public static final alarms_list:I = 0x7f100296

.field public static final alertText:I = 0x7f100196

.field public static final alertTitle:I = 0x7f100071

.field public static final alerts:I = 0x7f1001c6

.field public static final alreadyMember:I = 0x7f100134

.field public static final always:I = 0x7f10003e

.field public static final appNotificationsSection:I = 0x7f10017e

.field public static final app_switch:I = 0x7f10010a

.field public static final appsListView:I = 0x7f1001ba

.field public static final appsNotifItemLayout:I = 0x7f10017f

.field public static final arrowDown:I = 0x7f1001b7

.field public static final attention_icon:I = 0x7f100230

.field public static final auto:I = 0x7f100046

.field public static final automatic:I = 0x7f10005f

.field public static final back:I = 0x7f100185

.field public static final backlight_duration_text:I = 0x7f1001da

.field public static final badgeView:I = 0x7f1001fb

.field public static final baseInfoText:I = 0x7f1000ae

.field public static final batteryButton:I = 0x7f1001b2

.field public static final batteryImg:I = 0x7f1001e5

.field public static final batteryIndicator:I = 0x7f1001e4

.field public static final battery_icon:I = 0x7f1001df

.field public static final battery_level_text:I = 0x7f100273

.field public static final battery_notification:I = 0x7f1001e0

.field public static final beginning:I = 0x7f100038

.field public static final bonding_device:I = 0x7f100272

.field public static final both:I = 0x7f10004b

.field public static final bottom:I = 0x7f100027

.field public static final box_count:I = 0x7f10005c

.field public static final breadcrumbs_text:I = 0x7f10015a

.field public static final btClearPlaceholder:I = 0x7f100224

.field public static final btConnect:I = 0x7f1000be

.field public static final btFbLogin:I = 0x7f10011e

.field public static final btLogin:I = 0x7f100128

.field public static final btLoginCancel:I = 0x7f100127

.field public static final btPageLeft:I = 0x7f1000af

.field public static final btPageRight:I = 0x7f1000b0

.field public static final btRecoverPass:I = 0x7f100129

.field public static final btRegister:I = 0x7f10012f

.field public static final btRegisterCancel:I = 0x7f10012e

.field public static final btSkip:I = 0x7f100123

.field public static final btTblstLink:I = 0x7f1000c1

.field public static final btTour:I = 0x7f1000c0

.field public static final btVectorLogin:I = 0x7f100122

.field public static final btVectorRegister:I = 0x7f100120

.field public static final btnClear:I = 0x7f10010e

.field public static final btnClearAll:I = 0x7f10010f

.field public static final btnGetStarted:I = 0x7f100116

.field public static final btnLanguageSet:I = 0x7f100095

.field public static final btnPrev:I = 0x7f10011a

.field public static final btnSkip:I = 0x7f10011b

.field public static final btnUpdate:I = 0x7f10026f

.field public static final btnUpdateKernel:I = 0x7f1002d3

.field public static final btn_change_speed_high:I = 0x7f10026d

.field public static final btn_change_speed_low:I = 0x7f10026e

.field public static final btn_clear_cmd_queue:I = 0x7f100283

.field public static final btn_nest_activity:I = 0x7f100280

.field public static final btn_sync_activity:I = 0x7f100281

.field public static final btn_tests:I = 0x7f100282

.field public static final button:I = 0x7f10005d

.field public static final button1:I = 0x7f1001a8

.field public static final buttonPanel:I = 0x7f100077

.field public static final button_attach_file_for_bug:I = 0x7f10013c

.field public static final button_close_gatt:I = 0x7f10026b

.field public static final button_forget_watch:I = 0x7f10026a

.field public static final button_report_bug:I = 0x7f100277

.field public static final button_send_bug_report:I = 0x7f10013e

.field public static final button_stop_service:I = 0x7f10026c

.field public static final callNotifItemLayout:I = 0x7f100176

.field public static final calligraphy_tag_id:I = 0x7f100004

.field public static final caloriesBack:I = 0x7f1000d8

.field public static final caloriesCenterTV:I = 0x7f1000db

.field public static final caloriesGoalText:I = 0x7f1000c6

.field public static final caloriesLL:I = 0x7f1000dc

.field public static final caloriesNeedle:I = 0x7f1000d9

.field public static final caloriesQuadrantLayout:I = 0x7f1000d7

.field public static final caloriesQuadrantView:I = 0x7f1000da

.field public static final caloriesTVLabel:I = 0x7f1000ce

.field public static final caloriesValueTV:I = 0x7f1000cd

.field public static final cancel_action:I = 0x7f100241

.field public static final cbAgreeToTerms:I = 0x7f100130

.field public static final cbRegisterForUpdates:I = 0x7f100132

.field public static final center:I = 0x7f100028

.field public static final centerDate:I = 0x7f1000a7

.field public static final center_horizontal:I = 0x7f100029

.field public static final center_vertical:I = 0x7f10002a

.field public static final chart:I = 0x7f1000ad

.field public static final chart1:I = 0x7f1000aa

.field public static final checkbox:I = 0x7f100079

.field public static final checkbox_use_login_mail:I = 0x7f100137

.field public static final choice:I = 0x7f10004c

.field public static final chronometer:I = 0x7f100246

.field public static final clip_horizontal:I = 0x7f10002b

.field public static final clip_vertical:I = 0x7f10002c

.field public static final collapseActionView:I = 0x7f10003f

.field public static final com_facebook_body_frame:I = 0x7f100199

.field public static final com_facebook_button_xout:I = 0x7f10019b

.field public static final com_facebook_fragment_container:I = 0x7f100197

.field public static final com_facebook_login_activity_progress_bar:I = 0x7f100198

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f10019d

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f10019c

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f10019a

.field public static final comment_text:I = 0x7f10014b

.field public static final connectButton:I = 0x7f100268

.field public static final connect_ble:I = 0x7f10009b

.field public static final connect_button:I = 0x7f100276

.field public static final connected_device:I = 0x7f100270

.field public static final connected_status:I = 0x7f100271

.field public static final container:I = 0x7f1000c9

.field public static final containerButtons:I = 0x7f10011c

.field public static final containerFacePlaceholders:I = 0x7f1001f6

.field public static final containerFeatures:I = 0x7f1000ab

.field public static final containerFragments:I = 0x7f100159

.field public static final containerLogin:I = 0x7f100124

.field public static final containerRegister:I = 0x7f10012a

.field public static final contentLayout:I = 0x7f1001f7

.field public static final contentPanel:I = 0x7f100072

.field public static final contextual:I = 0x7f1001c7

.field public static final contextualAutoDiscreetMode:I = 0x7f1002a1

.field public static final contextualAutoSleep:I = 0x7f1002a0

.field public static final contextualElementLayout:I = 0x7f100217

.field public static final contextualFlickToDismiss:I = 0x7f1002a2

.field public static final contextualGlance:I = 0x7f1002a5

.field public static final contextualMorningFace:I = 0x7f10029f

.field public static final contextualNotifications:I = 0x7f1002a4

.field public static final contextualOfflineBadge:I = 0x7f1002a6

.field public static final contextualTentativeMeetings:I = 0x7f1002a3

.field public static final crashLogs:I = 0x7f100092

.field public static final current_cool_temp:I = 0x7f100256

.field public static final current_heat_temp:I = 0x7f100259

.field public static final current_temp:I = 0x7f1002c7

.field public static final custom:I = 0x7f100076

.field public static final customPanel:I = 0x7f100075

.field public static final cwac_cam2_gallery:I = 0x7f1001a4

.field public static final cwac_cam2_ok:I = 0x7f1002e9

.field public static final cwac_cam2_picture:I = 0x7f1001a0

.field public static final cwac_cam2_preview_stack:I = 0x7f10019f

.field public static final cwac_cam2_retry:I = 0x7f1002e8

.field public static final cwac_cam2_settings:I = 0x7f1001a1

.field public static final cwac_cam2_settings_camera:I = 0x7f1001a3

.field public static final cwac_cam2_switch_camera:I = 0x7f1001a2

.field public static final cwac_cam2_zoom:I = 0x7f1001a5

.field public static final dark:I = 0x7f100047

.field public static final date:I = 0x7f100238

.field public static final de:I = 0x7f100192

.field public static final decor_content_parent:I = 0x7f10007f

.field public static final default_activity_button:I = 0x7f10006a

.field public static final descriptionTextView:I = 0x7f1000ec

.field public static final description_text_view:I = 0x7f10014f

.field public static final dev:I = 0x7f10018f

.field public static final dialog:I = 0x7f100049

.field public static final disableHome:I = 0x7f100016

.field public static final disconnectButton:I = 0x7f100269

.field public static final dismiss:I = 0x7f10004d

.field public static final display_always:I = 0x7f100060

.field public static final display_realm_activity:I = 0x7f10009c

.field public static final distanceBack:I = 0x7f1000ee

.field public static final distanceCenterTV:I = 0x7f1000f1

.field public static final distanceLL:I = 0x7f1000f2

.field public static final distanceNeedle:I = 0x7f1000ef

.field public static final distanceQuadrantLayout:I = 0x7f1000ed

.field public static final distanceQuadrantView:I = 0x7f1000f0

.field public static final distanceTVLabel:I = 0x7f1000e2

.field public static final distanceValueTV:I = 0x7f1000e1

.field public static final divider:I = 0x7f1002ce

.field public static final dividerRight:I = 0x7f1002cd

.field public static final down:I = 0x7f100037

.field public static final downloadProgress:I = 0x7f100166

.field public static final downloadinfo_text_view:I = 0x7f10023c

.field public static final dropdown:I = 0x7f10004a

.field public static final dummyView:I = 0x7f100113

.field public static final editGoalTitle:I = 0x7f1001b0

.field public static final editGoalsButton:I = 0x7f1000c7

.field public static final editInputGoal:I = 0x7f1001b1

.field public static final edit_alarm_name:I = 0x7f1002c1

.field public static final edit_query:I = 0x7f100083

.field public static final edit_text_bug_details:I = 0x7f10013a

.field public static final edit_text_bug_summary:I = 0x7f100135

.field public static final edit_text_other_mail:I = 0x7f100139

.field public static final empty1:I = 0x7f1002ac

.field public static final empty2:I = 0x7f1002ae

.field public static final empty3:I = 0x7f1002b4

.field public static final empty4:I = 0x7f1002b8

.field public static final empty5:I = 0x7f1002ba

.field public static final emptyText:I = 0x7f100110

.field public static final empty_database:I = 0x7f10027c

.field public static final empty_text:I = 0x7f100156

.field public static final en:I = 0x7f100191

.field public static final enable_notifications:I = 0x7f100299

.field public static final end:I = 0x7f10002d

.field public static final end_padder:I = 0x7f10024b

.field public static final enterAlways:I = 0x7f100021

.field public static final enterAlwaysCollapsed:I = 0x7f100022

.field public static final eraseDb:I = 0x7f10009a

.field public static final es:I = 0x7f100193

.field public static final etPlaceholder:I = 0x7f100225

.field public static final etUri:I = 0x7f100190

.field public static final exitUntilCollapsed:I = 0x7f100023

.field public static final expand_activities_button:I = 0x7f100068

.field public static final expanded_menu:I = 0x7f100078

.field public static final extended_description:I = 0x7f100151

.field public static final fab_add:I = 0x7f100157

.field public static final fab_battery:I = 0x7f1001e3

.field public static final fab_graph:I = 0x7f1001b9

.field public static final fab_label:I = 0x7f100005

.field public static final fab_search:I = 0x7f1001d6

.field public static final fab_vibrations:I = 0x7f1001e7

.field public static final facebook_login_button:I = 0x7f10028a

.field public static final facebook_status:I = 0x7f100289

.field public static final feetHintTextView:I = 0x7f1001aa

.field public static final feetValueEditText:I = 0x7f1001a9

.field public static final fill:I = 0x7f10002e

.field public static final fill_horizontal:I = 0x7f10002f

.field public static final fill_vertical:I = 0x7f100030

.field public static final firstLayout:I = 0x7f100112

.field public static final fixed:I = 0x7f10004f

.field public static final fr:I = 0x7f100194

.field public static final fragment:I = 0x7f100158

.field public static final frameLayoutNotifications:I = 0x7f1001e8

.field public static final from:I = 0x7f100235

.field public static final from_text:I = 0x7f100236

.field public static final front:I = 0x7f100188

.field public static final getRssi:I = 0x7f100265

.field public static final go_to_offline:I = 0x7f10009e

.field public static final goal_achievement_alert:I = 0x7f100291

.field public static final goal_almost_alert:I = 0x7f100292

.field public static final graph:I = 0x7f1001c1

.field public static final gridView:I = 0x7f1002cc

.field public static final group:I = 0x7f10018c

.field public static final group_languages:I = 0x7f1000f9

.field public static final hint_label:I = 0x7f1002c9

.field public static final home:I = 0x7f100006

.field public static final homeAsUp:I = 0x7f100017

.field public static final horizontalScrollView:I = 0x7f1001e2

.field public static final hybrid:I = 0x7f10003b

.field public static final icon:I = 0x7f10006c

.field public static final icon_only:I = 0x7f100043

.field public static final ifRoom:I = 0x7f100040

.field public static final image:I = 0x7f100069

.field public static final imageButtonBack:I = 0x7f100091

.field public static final imageLayout:I = 0x7f1000b8

.field public static final imageView:I = 0x7f1001ec

.field public static final imageViewLogo:I = 0x7f100090

.field public static final image_place_1:I = 0x7f10025d

.field public static final image_place_2:I = 0x7f100261

.field public static final image_place_3:I = 0x7f100260

.field public static final image_view:I = 0x7f10014c

.field public static final imageview_recommended:I = 0x7f1002e4

.field public static final imgConnectBkg:I = 0x7f1000b9

.field public static final imgFbLogin:I = 0x7f10011d

.field public static final imgInstalled:I = 0x7f10023b

.field public static final imgLogo:I = 0x7f100114

.field public static final imgStreamImage:I = 0x7f10015b

.field public static final imgVectorLogin:I = 0x7f100121

.field public static final imgVectorRegister:I = 0x7f10011f

.field public static final imgWatchFaceDetails:I = 0x7f1001f5

.field public static final img_installed:I = 0x7f10014d

.field public static final inchesHintTextView:I = 0x7f1001ac

.field public static final inchesValueEditText:I = 0x7f1001ab

.field public static final info:I = 0x7f10024a

.field public static final info_screen_layout:I = 0x7f100142

.field public static final info_text:I = 0x7f1001fe

.field public static final inline:I = 0x7f10005e

.field public static final itemAppsImage:I = 0x7f100180

.field public static final itemAppsNotifText:I = 0x7f100181

.field public static final itemAppsSummaryText:I = 0x7f100182

.field public static final itemCallImage:I = 0x7f100177

.field public static final itemSmsImage:I = 0x7f10017b

.field public static final itemSmsNotifText:I = 0x7f10017c

.field public static final itemSmsSummaryText:I = 0x7f10017d

.field public static final itemSystemNotifImage:I = 0x7f100172

.field public static final item_rating_list_view:I = 0x7f100155

.field public static final item_touch_helper_previous_elevation:I = 0x7f100007

.field public static final label_alarm_name:I = 0x7f1002c0

.field public static final label_delete:I = 0x7f1002c4

.field public static final label_image:I = 0x7f10020d

.field public static final label_repeat:I = 0x7f1002c2

.field public static final label_text:I = 0x7f10020e

.field public static final landingCentralLeft:I = 0x7f1001f0

.field public static final landingCentralRight:I = 0x7f1001f1

.field public static final landingDelete:I = 0x7f1001ea

.field public static final landingLeft:I = 0x7f1001eb

.field public static final landingRight:I = 0x7f1001f2

.field public static final language:I = 0x7f100288

.field public static final large:I = 0x7f100062

.field public static final layout:I = 0x7f1002a7

.field public static final layout_attach_file:I = 0x7f10013b

.field public static final layout_use_login_mail:I = 0x7f100136

.field public static final lbConnect:I = 0x7f1000bb

.field public static final lbFound:I = 0x7f1000bd

.field public static final lbInfo:I = 0x7f1000bf

.field public static final lbSearching:I = 0x7f1000ba

.field public static final lbWatchConnected:I = 0x7f1000bc

.field public static final left:I = 0x7f100031

.field public static final leftDate:I = 0x7f1000a6

.field public static final leftTimeValue:I = 0x7f100164

.field public static final leftTimezone:I = 0x7f10015e

.field public static final leftZoneName:I = 0x7f100161

.field public static final legalTermsLink:I = 0x7f100131

.field public static final legalTermsWebView:I = 0x7f100234

.field public static final light:I = 0x7f100048

.field public static final line1:I = 0x7f100245

.field public static final line3:I = 0x7f100248

.field public static final linkLostIndicator:I = 0x7f10029d

.field public static final link_lost:I = 0x7f1001b5

.field public static final link_lost_icon:I = 0x7f1001dc

.field public static final link_lost_notification:I = 0x7f1001dd

.field public static final list:I = 0x7f10029c

.field public static final listFoundDevices:I = 0x7f1000c4

.field public static final listMode:I = 0x7f100013

.field public static final listView:I = 0x7f10010d

.field public static final list_found_devices:I = 0x7f100141

.field public static final list_item:I = 0x7f10006b

.field public static final localTimeValue:I = 0x7f100163

.field public static final localTimezone:I = 0x7f10015d

.field public static final localZoneName:I = 0x7f100160

.field public static final lockerAppDescription:I = 0x7f100203

.field public static final lockerAppImage:I = 0x7f100201

.field public static final lockerAppLayout:I = 0x7f100200

.field public static final lockerAppListView:I = 0x7f1000a4

.field public static final lockerAppName:I = 0x7f100202

.field public static final lockerAppRatingBar:I = 0x7f100204

.field public static final locker_button:I = 0x7f10027e

.field public static final login_button:I = 0x7f100274

.field public static final logo_image:I = 0x7f1001fc

.field public static final logo_vector_text:I = 0x7f1001fd

.field public static final logout:I = 0x7f1001d1

.field public static final mDownloadingProgress:I = 0x7f100145

.field public static final mDownloadingProgressBar:I = 0x7f10016b

.field public static final mWaitingProgress:I = 0x7f100148

.field public static final mainWebView:I = 0x7f100111

.field public static final main_activity_main_layout_view:I = 0x7f100106

.field public static final main_container:I = 0x7f100109

.field public static final marquee:I = 0x7f100035

.field public static final media_actions:I = 0x7f100243

.field public static final media_controller_bottom_area:I = 0x7f1002d8

.field public static final media_controller_bottom_root:I = 0x7f1002d7

.field public static final media_controller_brightness:I = 0x7f1002dd

.field public static final media_controller_controls:I = 0x7f1002dc

.field public static final media_controller_controls_root:I = 0x7f1002e3

.field public static final media_controller_gestures_area:I = 0x7f1002d6

.field public static final media_controller_loading_view:I = 0x7f1002d4

.field public static final media_controller_next:I = 0x7f1002e0

.field public static final media_controller_pause:I = 0x7f1002df

.field public static final media_controller_previous:I = 0x7f1002de

.field public static final media_controller_progress:I = 0x7f1002da

.field public static final media_controller_root:I = 0x7f1002d5

.field public static final media_controller_time:I = 0x7f1002db

.field public static final media_controller_time_current:I = 0x7f1002d9

.field public static final media_controller_touch_root:I = 0x7f1002e2

.field public static final media_controller_volume:I = 0x7f1002e1

.field public static final menu_search:I = 0x7f1002ed

.field public static final messenger_send_button:I = 0x7f10023f

.field public static final middle:I = 0x7f100036

.field public static final mini:I = 0x7f100020

.field public static final mirror_phone:I = 0x7f10029a

.field public static final multiply:I = 0x7f100054

.field public static final name:I = 0x7f100133

.field public static final nameTextView:I = 0x7f100239

.field public static final name_text_view:I = 0x7f10014e

.field public static final navLayout:I = 0x7f100119

.field public static final never:I = 0x7f100041

.field public static final never_display:I = 0x7f100061

.field public static final new_layout:I = 0x7f10023d

.field public static final news_switch:I = 0x7f10010b

.field public static final newsletter:I = 0x7f1001cc

.field public static final noDataSetText:I = 0x7f1000b1

.field public static final none:I = 0x7f100018

.field public static final normal:I = 0x7f100014

.field public static final notification_button:I = 0x7f100278

.field public static final notificationsButton:I = 0x7f1001b3

.field public static final notificationsEnabler:I = 0x7f1001e9

.field public static final numberPicker1:I = 0x7f1001a7

.field public static final open_graph:I = 0x7f100059

.field public static final option_alert:I = 0x7f1002bd

.field public static final option_content:I = 0x7f1002be

.field public static final option_de:I = 0x7f1000fd

.field public static final option_default:I = 0x7f1000fa

.field public static final option_en:I = 0x7f1000fb

.field public static final option_es:I = 0x7f1000ff

.field public static final option_fr:I = 0x7f1000fe

.field public static final option_nl:I = 0x7f100101

.field public static final option_off:I = 0x7f1002bc

.field public static final option_ro:I = 0x7f1000fc

.field public static final option_tr:I = 0x7f100100

.field public static final options_list:I = 0x7f1002cb

.field public static final page:I = 0x7f10005a

.field public static final pair_button:I = 0x7f100275

.field public static final parallax:I = 0x7f100025

.field public static final parentPanel:I = 0x7f10006e

.field public static final phoneNotifSection:I = 0x7f100175

.field public static final picture_activity:I = 0x7f1002e6

.field public static final pin:I = 0x7f100026

.field public static final place_autocomplete_clear_button:I = 0x7f10024e

.field public static final place_autocomplete_powered_by_google:I = 0x7f100250

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f100252

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f100253

.field public static final place_autocomplete_progress:I = 0x7f100251

.field public static final place_autocomplete_search_button:I = 0x7f10024c

.field public static final place_autocomplete_search_input:I = 0x7f10024d

.field public static final place_autocomplete_separator:I = 0x7f10024f

.field public static final placeholder_name:I = 0x7f100231

.field public static final play_gesture_controller:I = 0x7f1001bb

.field public static final play_gesture_horizontal_seekbar:I = 0x7f1001bc

.field public static final play_gesture_vertical_seekbar:I = 0x7f1001bd

.field public static final play_video_controller:I = 0x7f1001bf

.field public static final play_video_loading:I = 0x7f1001c0

.field public static final play_video_texture:I = 0x7f1001be

.field public static final prb_child_tag_id:I = 0x7f100008

.field public static final preferenceNotificationEnablerLayout:I = 0x7f10020a

.field public static final preferenceNotificationEnablerSwitch:I = 0x7f10020c

.field public static final preferenceNotificationEnablerTitle:I = 0x7f10020b

.field public static final preferencesLayout:I = 0x7f10016f

.field public static final production:I = 0x7f10018d

.field public static final progress:I = 0x7f1000c2

.field public static final progressBarCircular:I = 0x7f1001f8

.field public static final progress_bar:I = 0x7f1000f8

.field public static final progress_circular:I = 0x7f100009

.field public static final progress_dialog_search_devices:I = 0x7f100140

.field public static final progress_horizontal:I = 0x7f10000a

.field public static final radio:I = 0x7f10007b

.field public static final radio1:I = 0x7f1000b2

.field public static final radio2:I = 0x7f1000b4

.field public static final radio3:I = 0x7f1000b3

.field public static final radio4:I = 0x7f1000b5

.field public static final range_control:I = 0x7f100254

.field public static final rating_bar:I = 0x7f1000eb

.field public static final rating_layout:I = 0x7f100153

.field public static final reconnectService:I = 0x7f100094

.field public static final recoverPassWebView:I = 0x7f10025b

.field public static final relativel:I = 0x7f10022e

.field public static final relativelayoutCenter:I = 0x7f1001b4

.field public static final remove_token_button:I = 0x7f10009f

.field public static final repet_option_selected:I = 0x7f1002c3

.field public static final report_abuse_layout:I = 0x7f100154

.field public static final requestLocalizationFile:I = 0x7f100097

.field public static final requestSyncStepToFit:I = 0x7f100099

.field public static final reset_settings:I = 0x7f1001d0

.field public static final retry:I = 0x7f100149

.field public static final retry_button:I = 0x7f1001ff

.field public static final reveal:I = 0x7f10004e

.field public static final reviews_layout:I = 0x7f100152

.field public static final right:I = 0x7f100032

.field public static final rightDate:I = 0x7f1000a8

.field public static final rightTimeValue:I = 0x7f100165

.field public static final rightTimezone:I = 0x7f10015f

.field public static final rightZoneName:I = 0x7f100162

.field public static final ro:I = 0x7f100195

.field public static final rssi_value_textview:I = 0x7f100264

.field public static final satellite:I = 0x7f10003c

.field public static final saveGoalsButton:I = 0x7f1000c8

.field public static final save_rating:I = 0x7f1002eb

.field public static final screen:I = 0x7f100055

.field public static final scroll:I = 0x7f100024

.field public static final scrollDummyView:I = 0x7f1001b8

.field public static final scrollView:I = 0x7f100073

.field public static final scrollable:I = 0x7f100050

.field public static final search_badge:I = 0x7f100085

.field public static final search_bar:I = 0x7f100084

.field public static final search_button:I = 0x7f100086

.field public static final search_close_btn:I = 0x7f10008b

.field public static final search_edit_frame:I = 0x7f100087

.field public static final search_field:I = 0x7f1002ca

.field public static final search_go_btn:I = 0x7f10008d

.field public static final search_mag_icon:I = 0x7f100088

.field public static final search_plate:I = 0x7f100089

.field public static final search_src_text:I = 0x7f10008a

.field public static final search_voice_btn:I = 0x7f10008e

.field public static final secondHand:I = 0x7f1001d8

.field public static final section_1:I = 0x7f1001c2

.field public static final section_2:I = 0x7f1001c5

.field public static final section_3:I = 0x7f1001ca

.field public static final section_4:I = 0x7f1001ce

.field public static final section_battery:I = 0x7f1001de

.field public static final section_link:I = 0x7f1001db

.field public static final section_newsletter:I = 0x7f100294

.field public static final seekBar:I = 0x7f1001d9

.field public static final select_dialog_listview:I = 0x7f10008f

.field public static final sendDummyData:I = 0x7f100266

.field public static final sendLocalizationFile:I = 0x7f100096

.field public static final send_report:I = 0x7f1002ec

.field public static final sep:I = 0x7f1000b7

.field public static final sepCenterLeft:I = 0x7f1001ef

.field public static final sepCenterRight:I = 0x7f1001ee

.field public static final sepCenterVertical:I = 0x7f1001ed

.field public static final settingsAlertsAppSectionSeparatorApps:I = 0x7f10029b

.field public static final settingsInfoElementLayout:I = 0x7f10020f

.field public static final settingsInfoElementSummary:I = 0x7f100211

.field public static final settingsInfoElementTitle:I = 0x7f100210

.field public static final settingsSimpleElementLabel:I = 0x7f100213

.field public static final settingsSimpleElementLayout:I = 0x7f100212

.field public static final settingsSimpleToggleLabel:I = 0x7f100214

.field public static final settingsSimpleToggleSwitch:I = 0x7f100216

.field public static final settingsValueElementLabel:I = 0x7f10021c

.field public static final settingsValueElementLayout:I = 0x7f10021a

.field public static final settingsValueElementValue:I = 0x7f10021b

.field public static final settings_notifications_layout:I = 0x7f100298

.field public static final shareButton:I = 0x7f1000f6

.field public static final shareLayout:I = 0x7f1000f5

.field public static final shortcut:I = 0x7f10007a

.field public static final showCustom:I = 0x7f100019

.field public static final showHome:I = 0x7f10001a

.field public static final showTitle:I = 0x7f10001b

.field public static final signIn:I = 0x7f100104

.field public static final signUp:I = 0x7f100105

.field public static final single_control:I = 0x7f1002c5

.field public static final skipButton:I = 0x7f10016e

.field public static final sleepBack:I = 0x7f1000e4

.field public static final sleepCenterTV:I = 0x7f1000e7

.field public static final sleepLL:I = 0x7f1000e8

.field public static final sleepNeedle:I = 0x7f1000e5

.field public static final sleepQuadrantLayout:I = 0x7f1000e3

.field public static final sleepQuadrantView:I = 0x7f1000e6

.field public static final sleepTVLabel:I = 0x7f1000e0

.field public static final sleepValueTV:I = 0x7f1000df

.field public static final small:I = 0x7f100063

.field public static final smsNotifItemLayout:I = 0x7f10017a

.field public static final snackbar_action:I = 0x7f100233

.field public static final snackbar_text:I = 0x7f100232

.field public static final split_action_bar:I = 0x7f10000b

.field public static final src_atop:I = 0x7f100056

.field public static final src_in:I = 0x7f100057

.field public static final src_over:I = 0x7f100058

.field public static final stage:I = 0x7f10018e

.field public static final standard:I = 0x7f100044

.field public static final start:I = 0x7f100033

.field public static final status_bar_latest_event_content:I = 0x7f100242

.field public static final stepsBack:I = 0x7f1000d0

.field public static final stepsCenterTV:I = 0x7f1000d3

.field public static final stepsGoalText:I = 0x7f1000c5

.field public static final stepsLL:I = 0x7f1000d4

.field public static final stepsNeedle:I = 0x7f1000d1

.field public static final stepsQuadrantLayout:I = 0x7f1000cf

.field public static final stepsQuadrantView:I = 0x7f1000d2

.field public static final stepsTVLabel:I = 0x7f1000cc

.field public static final stepsValueTV:I = 0x7f1000cb

.field public static final steps_button:I = 0x7f10027a

.field public static final stopBleService:I = 0x7f100093

.field public static final storeAppDescription:I = 0x7f100220

.field public static final storeAppImage:I = 0x7f10021e

.field public static final storeAppLayout:I = 0x7f10021d

.field public static final storeAppName:I = 0x7f10021f

.field public static final storeAppRatingBar:I = 0x7f100221

.field public static final storeAppsListView:I = 0x7f1001d3

.field public static final storeFeaturedListView:I = 0x7f1001d4

.field public static final storeText:I = 0x7f1001e6

.field public static final storeWatchfacesListView:I = 0x7f1001d7

.field public static final store_button:I = 0x7f10027d

.field public static final stream_image:I = 0x7f100223

.field public static final submit_area:I = 0x7f10008c

.field public static final summary:I = 0x7f100205

.field public static final summaryToggleElementSummary:I = 0x7f100219

.field public static final summaryToggleElementTitle:I = 0x7f100218

.field public static final summaryToggleOnOffSwitch:I = 0x7f100215

.field public static final summary_store_place:I = 0x7f10025f

.field public static final support:I = 0x7f1001cb

.field public static final supportedAppCheckImage:I = 0x7f100208

.field public static final supportedAppIcon:I = 0x7f100209

.field public static final supportedAppLayout:I = 0x7f100207

.field public static final supportedAppSummaryText:I = 0x7f100179

.field public static final supportedAppTitleText:I = 0x7f100178

.field public static final supportedAppsList:I = 0x7f100184

.field public static final supportedAppsSection:I = 0x7f100183

.field public static final supportedSystemNotifSummaryText:I = 0x7f100174

.field public static final supportedSystemNotifTitleText:I = 0x7f100173

.field public static final swipe_button1:I = 0x7f100186

.field public static final swipe_button2:I = 0x7f100187

.field public static final switchToggle:I = 0x7f10029e

.field public static final syncButton:I = 0x7f100267

.field public static final sync_button:I = 0x7f10027b

.field public static final sync_realm_activity_to_cloud:I = 0x7f10009d

.field public static final systemNotifLayout:I = 0x7f100171

.field public static final systemNotifSection:I = 0x7f100170

.field public static final systemNotificationEnablerLayout:I = 0x7f100226

.field public static final systemNotificationEnablerSwitch:I = 0x7f100228

.field public static final systemNotificationEnablerTitle:I = 0x7f100227

.field public static final tabImage:I = 0x7f1002d0

.field public static final tabLayout:I = 0x7f1000a9

.field public static final tabMode:I = 0x7f100015

.field public static final tabRelative:I = 0x7f1002cf

.field public static final tabText:I = 0x7f1002d2

.field public static final tab_layout:I = 0x7f100107

.field public static final tab_tab_holder:I = 0x7f1002d1

.field public static final tag_face_element_id:I = 0x7f10000c

.field public static final tag_face_placeholder_text_id:I = 0x7f10000d

.field public static final tag_stream_delete_button_id:I = 0x7f10000e

.field public static final tag_stream_full:I = 0x7f10000f

.field public static final tag_stream_id:I = 0x7f100010

.field public static final take_picture:I = 0x7f1002e5

.field public static final temp_cool_down:I = 0x7f100257

.field public static final temp_cool_up:I = 0x7f100255

.field public static final temp_down:I = 0x7f1002c8

.field public static final temp_heat_down:I = 0x7f10025a

.field public static final temp_heat_up:I = 0x7f100258

.field public static final temp_up:I = 0x7f1002c6

.field public static final terrain:I = 0x7f10003d

.field public static final text:I = 0x7f100249

.field public static final text2:I = 0x7f100247

.field public static final textAccountProfile:I = 0x7f1002a8

.field public static final textActivityInfo:I = 0x7f1002a9

.field public static final textAlerts:I = 0x7f1002aa

.field public static final textContextual:I = 0x7f1002ab

.field public static final textDeveloperMode:I = 0x7f1002b9

.field public static final textFactoryReset:I = 0x7f1002b1

.field public static final textFaq:I = 0x7f1002b6

.field public static final textPairWatch:I = 0x7f1002af

.field public static final textResetDatabase:I = 0x7f1002b2

.field public static final textSpacerNoButtons:I = 0x7f100074

.field public static final textSupport:I = 0x7f1002b7

.field public static final textTroubleshoothing:I = 0x7f1002b5

.field public static final textUnpair:I = 0x7f1002b3

.field public static final textUpdateWatch:I = 0x7f1002b0

.field public static final textVectorTour:I = 0x7f1002ad

.field public static final textVersion:I = 0x7f1002bb

.field public static final text_attach_file_for_bug:I = 0x7f10013d

.field public static final text_layout:I = 0x7f10025c

.field public static final text_use_login_mail:I = 0x7f100138

.field public static final time:I = 0x7f100237

.field public static final timeLayout:I = 0x7f1000a5

.field public static final time_format:I = 0x7f100286

.field public static final time_picker:I = 0x7f1002bf

.field public static final timezoneElementLayout:I = 0x7f100229

.field public static final timezoneName:I = 0x7f10022a

.field public static final timezonesLayout:I = 0x7f10015c

.field public static final title:I = 0x7f10006d

.field public static final title_store_place:I = 0x7f10025e

.field public static final title_template:I = 0x7f100070

.field public static final toggle:I = 0x7f100206

.field public static final toolbar:I = 0x7f100108

.field public static final top:I = 0x7f100034

.field public static final topPanel:I = 0x7f10006f

.field public static final totalInfoTxt:I = 0x7f1000ac

.field public static final tour:I = 0x7f1001cd

.field public static final triangle:I = 0x7f100052

.field public static final tvContent:I = 0x7f10019e

.field public static final txLoginPassword:I = 0x7f100126

.field public static final txLoginUsername:I = 0x7f100125

.field public static final txRegisterName:I = 0x7f10012b

.field public static final txRegisterPassword:I = 0x7f10012d

.field public static final txRegisterUsername:I = 0x7f10012c

.field public static final txtExport:I = 0x7f1000f7

.field public static final txtSelectWatch:I = 0x7f1000c3

.field public static final txtWelcome:I = 0x7f100115

.field public static final typeText:I = 0x7f10023e

.field public static final underline:I = 0x7f100053

.field public static final unit_system:I = 0x7f100287

.field public static final unknown:I = 0x7f10005b

.field public static final up:I = 0x7f100011

.field public static final updateTextScroll:I = 0x7f100168

.field public static final update_watch:I = 0x7f1001cf

.field public static final useLogo:I = 0x7f10001c

.field public static final user_name:I = 0x7f100285

.field public static final valueCalories:I = 0x7f1000dd

.field public static final valueCaloriesMax:I = 0x7f1000de

.field public static final valueDistance:I = 0x7f1000f3

.field public static final valueDistanceMax:I = 0x7f1000f4

.field public static final valueSleep:I = 0x7f1000e9

.field public static final valueSleepMax:I = 0x7f1000ea

.field public static final valueSteps:I = 0x7f1000d5

.field public static final valueStepsMax:I = 0x7f1000d6

.field public static final version:I = 0x7f1001d2

.field public static final videoLayout:I = 0x7f100117

.field public static final video_activity:I = 0x7f1002e7

.field public static final video_view:I = 0x7f100118

.field public static final viewOverlay:I = 0x7f1001f4

.field public static final viewPager:I = 0x7f1001f3

.field public static final view_offset_helper:I = 0x7f100012

.field public static final view_page_indicator:I = 0x7f100263

.field public static final view_watchface:I = 0x7f10022f

.field public static final viewpager:I = 0x7f10014a

.field public static final viewpager_recommended:I = 0x7f100262

.field public static final watch:I = 0x7f1001c8

.field public static final watchAppDescription:I = 0x7f10022d

.field public static final watchAppImage:I = 0x7f10022b

.field public static final watchAppLayout:I = 0x7f100222

.field public static final watchAppListView:I = 0x7f1000b6

.field public static final watchAppName:I = 0x7f10022c

.field public static final watchFacesListView:I = 0x7f1001f9

.field public static final watch_apps_button:I = 0x7f10027f

.field public static final watch_image:I = 0x7f10013f

.field public static final watch_update_action_button:I = 0x7f10016d

.field public static final watch_update_build_number_text:I = 0x7f100167

.field public static final watch_update_completed_percentage:I = 0x7f100147

.field public static final watch_update_extra_details_text:I = 0x7f10016c

.field public static final watch_update_logo_image:I = 0x7f100144

.field public static final watch_update_progress_status_text:I = 0x7f10016a

.field public static final watch_update_update_details_text:I = 0x7f100169

.field public static final watch_update_vector_image:I = 0x7f100146

.field public static final watch_update_version_status_text:I = 0x7f100143

.field public static final weekly_newsletter:I = 0x7f100295

.field public static final wide:I = 0x7f100045

.field public static final withText:I = 0x7f100042

.field public static final wrap_content:I = 0x7f100051


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
