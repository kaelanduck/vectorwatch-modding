.class public Lcom/vectorwatch/android/managers/StreamsManager;
.super Ljava/lang/Object;
.source "StreamsManager.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/vectorwatch/android/managers/StreamsManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static clearStreamsFromUninstalledApp(ILandroid/content/Context;)V
    .locals 12
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    .line 534
    invoke-static {p0, p1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v5

    .line 535
    .local v5, "placeholders":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Element;

    .line 537
    .local v4, "placeholder":Lcom/vectorwatch/android/models/Element;
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {p0, v11, v8, p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    .line 538
    .local v0, "activeStream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v0, :cond_0

    .line 540
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 539
    invoke-static {v0, p0, v11, v8, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveStreamChannelLabel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 542
    .local v6, "streamChannelLabel":Ljava/lang/String;
    sget-object v8, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clearing streams for uninstall - stream name = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " app = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " element = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 543
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 542
    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 545
    const/4 v3, 0x0

    .line 546
    .local v3, "faceId":I
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 548
    .local v2, "elementId":I
    new-instance v1, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 549
    .local v1, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-virtual {v1, v6}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUniqueLabel(Ljava/lang/String;)V

    .line 551
    invoke-static {v0, p0, v3, v2, p1}, Lcom/vectorwatch/android/managers/StreamsManager;->requestUnsubscribeFromChannel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)V

    goto :goto_0

    .line 554
    .end local v0    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    .end local v1    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v2    # "elementId":I
    .end local v3    # "faceId":I
    .end local v4    # "placeholder":Lcom/vectorwatch/android/models/Element;
    .end local v6    # "streamChannelLabel":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 9
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 91
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v4

    .line 92
    .local v4, "stream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v4, :cond_0

    .line 93
    sget-object v6, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Stream remove from db - name = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 99
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getPushUpdateRoutesAssociatedToStream(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 101
    .local v3, "routesList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 102
    .local v2, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    new-instance v1, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    .line 103
    .local v1, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getAppId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 104
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getWatchfaceId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-byte v7, v7

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 105
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getFieldId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-byte v7, v7

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 106
    sget-object v7, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 107
    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 109
    invoke-static {v1, p1}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    goto :goto_0

    .line 95
    .end local v1    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .end local v2    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    .end local v3    # "routesList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    :cond_0
    sget-object v6, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Stream is null: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 124
    :cond_1
    :goto_1
    return v5

    .line 112
    .restart local v3    # "routesList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    :cond_2
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->markStreamForRemoval(Ljava/lang/String;Landroid/content/Context;)V

    .line 113
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 114
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->hardDeleteStream(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 116
    .local v0, "deleted":I
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v6

    new-instance v7, Lcom/vectorwatch/android/events/StreamDeletedEvent;

    invoke-direct {v7, v4}, Lcom/vectorwatch/android/events/StreamDeletedEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 121
    .end local v0    # "deleted":I
    :goto_2
    if-eqz v4, :cond_1

    .line 122
    const/4 v5, 0x1

    goto :goto_1

    .line 118
    :cond_3
    invoke-static {p1}, Lcom/vectorwatch/android/managers/StreamsManager;->notifyCloud(Landroid/content/Context;)V

    goto :goto_2
.end method

.method public static getActiveLiveStreamsForAppAndWatchface(Landroid/content/Context;II)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613
    invoke-static {p0, p1, p2}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveLiveStreamsForAppAndWatchface(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getActiveStreamsByUuid(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveStreams(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static getAppPrivateSetUpData(Ljava/lang/String;III)Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .locals 3
    .param p0, "elementType"    # Ljava/lang/String;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "elementId"    # I

    .prologue
    .line 458
    const-string v0, "\u0000"

    .line 460
    .local v0, "defaultValue":Ljava/lang/String;
    new-instance v1, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    .line 461
    .local v1, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {v1, p1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 462
    int-to-byte v2, p2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 463
    int-to-byte v2, p3

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 465
    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->LONG_TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 466
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_LONG_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 471
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 472
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setValue(Ljava/lang/String;)V

    .line 474
    return-object v1

    .line 467
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 468
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    goto :goto_0
.end method

.method public static getLastUpdates(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 772
    invoke-static {p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getLastUpdatedValues(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 773
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    return-object v0
.end method

.method public static getSavedStreams(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 578
    invoke-static {p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getUniqueAvailableStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 579
    .local v0, "streamsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    return-object v0
.end method

.method private static getStandaloneSetUpData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .locals 3
    .param p0, "appId"    # I
    .param p1, "watchfaceId"    # I
    .param p2, "elementId"    # I

    .prologue
    .line 479
    const-string v0, "\u0000"

    .line 481
    .local v0, "defaultValue":Ljava/lang/String;
    new-instance v1, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    .line 482
    .local v1, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {v1, p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 483
    int-to-byte v2, p1

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 484
    int-to-byte v2, p2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 485
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 486
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 487
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setValue(Ljava/lang/String;)V

    .line 489
    return-object v1
.end method

.method public static getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;
    .locals 1
    .param p0, "appId"    # I
    .param p1, "faceId"    # I
    .param p2, "elementId"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 605
    invoke-static {p0, p1, p2, p3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveStream(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    return-object v0
.end method

.method public static getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;
    .locals 1
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    return-object v0
.end method

.method private static getStreamWithUuidFromList(Ljava/lang/String;Ljava/util/List;)Lcom/vectorwatch/android/models/Stream;
    .locals 4
    .param p0, "streamUuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;)",
            "Lcom/vectorwatch/android/models/Stream;"
        }
    .end annotation

    .prologue
    .local p1, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    const/4 v1, 0x0

    .line 724
    if-nez p1, :cond_0

    move-object v0, v1

    .line 735
    :goto_0
    return-object v0

    .line 728
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream;

    .line 729
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .end local v0    # "stream":Lcom/vectorwatch/android/models/Stream;
    :cond_2
    move-object v0, v1

    .line 735
    goto :goto_0
.end method

.method public static getUpdates(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 767
    invoke-static {p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getUpdatedValues(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 768
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    return-object v0
.end method

.method public static getUpdatesForLiveStream(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/Integer;
    .param p2, "watchfaceId"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752
    invoke-static {p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getUpdatedValues(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 753
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 755
    .local v1, "removeStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 756
    .local v2, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getAppId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getWatchfaceId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 757
    :cond_1
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 761
    .end local v2    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 763
    return-object v0
.end method

.method public static getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .locals 2
    .param p0, "appId"    # I
    .param p1, "watchfaceId"    # I
    .param p2, "elementId"    # I

    .prologue
    .line 494
    new-instance v0, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    .line 495
    .local v0, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 496
    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 497
    int-to-byte v1, p2

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 498
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 499
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 501
    return-object v0
.end method

.method private static getWatchSourceSetUpDataForLocalStreams(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;III)Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .locals 7
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;
    .param p2, "appId"    # I
    .param p3, "watchfaceId"    # I
    .param p4, "elementId"    # I

    .prologue
    const/4 v1, 0x0

    .line 383
    if-nez p0, :cond_1

    .line 384
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v5, "Trying to set up a stream on the watch but the stream is null."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 453
    :cond_0
    :goto_0
    return-object v1

    .line 390
    :cond_1
    if-eqz p1, :cond_3

    .line 391
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 392
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 394
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/settings/Setting;

    .line 413
    .local v2, "streamSettings":Lcom/vectorwatch/android/models/settings/Setting;
    :goto_1
    iget-object v4, v2, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 418
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v2, Lcom/vectorwatch/android/models/settings/Setting;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 419
    .local v0, "defaultValue":Ljava/lang/String;
    iget-object v3, v2, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    .line 421
    .local v3, "type":Ljava/lang/String;
    new-instance v1, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    .line 422
    .local v1, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {v1, p2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 423
    int-to-byte v4, p3

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 424
    int-to-byte v4, p4

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 426
    sget-object v4, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->SYSTEM_VARS:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->getVal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 427
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SYSTEM_VARS:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 428
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stream - type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SYSTEM_VARS:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 429
    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$ElementType;->getVal()I

    move-result v6

    int-to-byte v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 428
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 448
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    int-to-byte v4, v4

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 450
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stream - default val = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 451
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setValue(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 396
    .end local v0    # "defaultValue":Ljava/lang/String;
    .end local v1    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .end local v2    # "streamSettings":Lcom/vectorwatch/android/models/settings/Setting;
    .end local v3    # "type":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v5, "Trying to set up a stream with channel settings non null, yet userSettings field not correct."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 401
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getDefaults()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 402
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getDefaults()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 403
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getDefaults()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 405
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getDefaults()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/settings/Setting;

    .restart local v2    # "streamSettings":Lcom/vectorwatch/android/models/settings/Setting;
    goto/16 :goto_1

    .line 407
    .end local v2    # "streamSettings":Lcom/vectorwatch/android/models/settings/Setting;
    :cond_4
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v5, "Trying to set up a stream with channel settings non null, yet userSettings field not correct."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 430
    .restart local v0    # "defaultValue":Ljava/lang/String;
    .restart local v1    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .restart local v2    # "streamSettings":Lcom/vectorwatch/android/models/settings/Setting;
    .restart local v3    # "type":Ljava/lang/String;
    :cond_5
    sget-object v4, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->DIGITAL_TIME:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->getVal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 431
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DIGITAL_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 432
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stream - type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DIGITAL_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 433
    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$ElementType;->getVal()I

    move-result v6

    int-to-byte v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 432
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 434
    :cond_6
    sget-object v4, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->TIMEZONE:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->getVal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 435
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TIMEZONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 436
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stream - type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TIMEZONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 437
    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$ElementType;->getVal()I

    move-result v6

    int-to-byte v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 436
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 438
    :cond_7
    sget-object v4, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->LIVE_STREAM:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->getVal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 439
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 440
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stream - type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 441
    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$ElementType;->getVal()I

    move-result v6

    int-to-byte v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 440
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 443
    :cond_8
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 444
    sget-object v4, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error! No stream type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private static isStreamChannelInUse(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 2
    .param p0, "streamChannelLabel"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 643
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamChannelActiveCount(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 645
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 646
    const/4 v1, 0x1

    .line 649
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isStreamSaved(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 583
    invoke-static {p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getUniqueAvailableStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 585
    .local v1, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream;

    .line 586
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 587
    const/4 v2, 0x1

    .line 591
    .end local v0    # "stream":Lcom/vectorwatch/android/models/Stream;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isStreamSaved(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;)Z
    .locals 3
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 595
    .local p2, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream;

    .line 596
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 597
    const/4 v1, 0x1

    .line 601
    .end local v0    # "stream":Lcom/vectorwatch/android/models/Stream;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static markAsUnmodified(IIILandroid/content/Context;)V
    .locals 0
    .param p0, "appId"    # I
    .param p1, "faceId"    # I
    .param p2, "elementId"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 777
    invoke-static {p0, p1, p2, p3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->cleanDirtyFieldForPlacement(IIILandroid/content/Context;)V

    .line 778
    return-void
.end method

.method private static notifyCloud(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 573
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 574
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 575
    return-void
.end method

.method public static onPostExecuteSubscribeStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILcom/vectorwatch/android/models/StreamValueModel;Landroid/content/Context;)V
    .locals 12
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;
    .param p2, "appId"    # I
    .param p3, "faceId"    # I
    .param p4, "elementId"    # I
    .param p5, "modelData"    # Lcom/vectorwatch/android/models/StreamValueModel;
    .param p6, "context"    # Landroid/content/Context;

    .prologue
    .line 277
    new-instance v10, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v10}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    .line 278
    .local v10, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {v10, p2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 279
    int-to-byte v2, p3

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 280
    move/from16 v0, p4

    int-to-byte v2, v0

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 281
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 282
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 284
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isLiveStream(Lcom/vectorwatch/android/models/Stream;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    const v2, 0x7f0901c0

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setValue(Ljava/lang/String;)V

    .line 286
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 288
    const v2, 0x7f09019b

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xbb8

    move-object/from16 v0, p6

    invoke-static {v2, v3, v0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    :cond_0
    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p6

    .line 292
    invoke-static/range {v2 .. v7}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    .line 296
    move-object/from16 v0, p6

    invoke-static {v10, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    .line 298
    if-eqz p5, :cond_1

    invoke-virtual/range {p5 .. p5}, Lcom/vectorwatch/android/models/StreamValueModel;->getPayload()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 299
    new-instance v8, Lcom/google/gson/Gson;

    invoke-direct {v8}, Lcom/google/gson/Gson;-><init>()V

    .line 301
    .local v8, "gson":Lcom/google/gson/Gson;
    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 302
    .local v11, "streamUpdates":Ljava/lang/String;
    move-object/from16 v0, p6

    invoke-static {v11, v0}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->pushNotification(Ljava/lang/String;Landroid/content/Context;)V

    .line 313
    .end local v8    # "gson":Lcom/google/gson/Gson;
    .end local v11    # "streamUpdates":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v2

    const-string v3, "21F1FC164BE69DA33A39BDE17402A120"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    const-string v2, "update_phone_battery"

    const-wide/16 v4, -0x1

    move-object/from16 v0, p6

    invoke-static {v2, v4, v5, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 317
    new-instance v9, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/android/scheduler/BatteryAlarm;

    move-object/from16 v0, p6

    invoke-direct {v9, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    .local v9, "intent":Landroid/content/Intent;
    const-string v2, "manuallyFired"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 319
    move-object/from16 v0, p6

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 320
    sget-object v2, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BATTERY ALARM]: Registering intent for stream "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " channel label"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 321
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getLabelText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 320
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 323
    .end local v9    # "intent":Landroid/content/Intent;
    :cond_2
    return-void
.end method

.method private static placeDefaultStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIILandroid/content/Context;)V
    .locals 7
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;
    .param p2, "elementType"    # Ljava/lang/String;
    .param p3, "appId"    # I
    .param p4, "watchfaceId"    # I
    .param p5, "elementId"    # I
    .param p6, "context"    # Landroid/content/Context;

    .prologue
    .line 340
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 341
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Trying to place a null stream or a stream with a null name or type on a face."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 378
    :cond_1
    :goto_0
    return-void

    .line 345
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Default Stream add to face - stream name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stream type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 346
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stream uuid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " channel setting (label) = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 347
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 345
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 348
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 350
    invoke-static {p0, p1, p3, p4, p5}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchSourceSetUpDataForLocalStreams(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v6

    .line 353
    .local v6, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    if-eqz v6, :cond_1

    .line 358
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Debug - place default stream (simple) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " app = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " face = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " element = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 361
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    .line 363
    invoke-static {v6, p6}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 365
    .end local v6    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 366
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 367
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    .line 369
    invoke-static {p3, p4, p5}, Lcom/vectorwatch/android/managers/StreamsManager;->getStandaloneSetUpData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v6

    .line 370
    .restart local v6    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-static {v6, p6}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 371
    .end local v6    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_5
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 372
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_6
    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 373
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    .line 375
    invoke-static {p2, p3, p4, p5}, Lcom/vectorwatch/android/managers/StreamsManager;->getAppPrivateSetUpData(Ljava/lang/String;III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v6

    .line 376
    .restart local v6    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-static {v6, p6}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public static placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V
    .locals 8
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;
    .param p2, "appUuid"    # Ljava/lang/String;
    .param p3, "appId"    # I
    .param p4, "watchfaceId"    # I
    .param p5, "elementId"    # I
    .param p6, "isSubscribed"    # Z
    .param p7, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 140
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Trying to place a null stream or a stream with a null name or type on a face."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 263
    :cond_1
    :goto_0
    return-void

    .line 144
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stream add to face - stream name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stream type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 145
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isSubscribed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 144
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    invoke-static {p0, p1, p3, p4, p5}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchSourceSetUpDataForLocalStreams(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v6

    .line 150
    .local v6, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    if-eqz v6, :cond_1

    .line 155
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Debug - place default stream (simple) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " app = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " face = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " element = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p7

    .line 158
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    .line 160
    invoke-static {v6, p7}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    .line 163
    .end local v6    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 164
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p7

    .line 166
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    .line 169
    :cond_5
    if-nez p6, :cond_1

    .line 170
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SUBSCRIBE: to stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for appUuid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 172
    new-instance v7, Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct {v7}, Lcom/vectorwatch/android/models/StreamPlacementModel;-><init>()V

    .line 173
    .local v7, "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setAppId(Ljava/lang/Integer;)V

    .line 174
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setElementId(Ljava/lang/Integer;)V

    .line 175
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setWatchFaceId(Ljava/lang/Integer;)V

    .line 176
    invoke-virtual {v7, p2}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setAppUuid(Ljava/lang/String;)V

    .line 178
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/managers/StreamsManager$1;

    invoke-direct {v1, p0, v7, p7}, Lcom/vectorwatch/android/managers/StreamsManager$1;-><init>(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;Landroid/content/Context;)V

    invoke-static {p7, v0, v7, p1, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->subscribeStream(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPlacementModel;Lcom/vectorwatch/android/models/StreamChannelSettings;Lretrofit/Callback;)V

    goto/16 :goto_0
.end method

.method public static removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z
    .locals 2
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "elementId"    # I
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 516
    if-nez p0, :cond_0

    .line 517
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Trying to remove from face a null stream"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 518
    const/4 v0, 0x0

    .line 523
    :goto_0
    return v0

    .line 521
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/vectorwatch/android/managers/StreamsManager;->requestUnsubscribeFromChannel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)V

    .line 523
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static requestUnsubscribeFromChannel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)V
    .locals 4
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "appId"    # I
    .param p2, "faceId"    # I
    .param p3, "elementId"    # I
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 560
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 559
    invoke-static {v0, v1, v2, v3, p4}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->markStreamForUnsubscribe(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/content/Context;)V

    .line 562
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->refreshLiveStreamManager(II)V

    .line 564
    invoke-static {p4}, Lcom/vectorwatch/android/managers/StreamsManager;->notifyCloud(Landroid/content/Context;)V

    .line 565
    return-void
.end method

.method public static saveStreamToDatabase(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)Z
    .locals 4
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    if-nez p0, :cond_1

    .line 67
    sget-object v1, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v2, "Trying to save null stream."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    .line 80
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stream save to db - name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 73
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->saveStream(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)Z

    move-result v0

    .line 75
    .local v0, "res":Z
    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V
    .locals 3
    .param p0, "model"    # Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 617
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getLength()B

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 618
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v1, "STREAMS - CHANGE COMPL - Should have value string."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 632
    :goto_0
    return-void

    .line 622
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementType()Lcom/vectorwatch/android/utils/Constants$ElementType;

    move-result-object v0

    if-nez v0, :cond_2

    .line 623
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Element type should never be NULL!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 627
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STREAMS - CHANGE COMPL - appID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getAppId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getWatchfaceId()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " eID"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 628
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementId()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " eType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementType()Lcom/vectorwatch/android/utils/Constants$ElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$ElementType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 629
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getLength()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 627
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 631
    invoke-static {p1, p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSyncComplication(Landroid/content/Context;Lcom/vectorwatch/android/models/ChangeComplicationModel;)Ljava/util/UUID;

    goto :goto_0
.end method

.method public static setDefaultStreamsOnDefaultApp(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)V
    .locals 18
    .param p0, "fullAppDetails"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 782
    if-eqz p0, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 783
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v15, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    .line 785
    .local v15, "watchfaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vectorwatch/android/models/Watchface;

    .line 786
    .local v14, "watchface":Lcom/vectorwatch/android/models/Watchface;
    if-eqz v14, :cond_0

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 787
    invoke-virtual {v14}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v11

    .line 789
    .local v11, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_1
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/models/Element;

    .line 790
    .local v10, "element":Lcom/vectorwatch/android/models/Element;
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 792
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v1

    .line 793
    .local v1, "stream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v1, :cond_1

    .line 794
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/Watchface;->getId()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 795
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 796
    sget-object v2, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DEFAULT STREAM "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " set on app "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 798
    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    .line 800
    .local v12, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v2

    .line 799
    invoke-virtual {v12, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 801
    .local v8, "defaultStreamSettingsString":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DEFAULT STREAM - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 802
    sget-object v2, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Debug - place default stream (def app)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " app = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " face = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 804
    invoke-virtual {v14}, Lcom/vectorwatch/android/models/Watchface;->getId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " element = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 805
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 802
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 806
    sget-object v2, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    const-string v3, "Setup - default stream on def app "

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 807
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v2

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 808
    invoke-virtual {v14}, Lcom/vectorwatch/android/models/Watchface;->getId()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object/from16 v7, p1

    .line 807
    invoke-static/range {v1 .. v7}, Lcom/vectorwatch/android/managers/StreamsManager;->placeDefaultStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIILandroid/content/Context;)V

    goto/16 :goto_0

    .line 816
    .end local v1    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v8    # "defaultStreamSettingsString":Ljava/lang/String;
    .end local v10    # "element":Lcom/vectorwatch/android/models/Element;
    .end local v11    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    .end local v12    # "gson":Lcom/google/gson/Gson;
    .end local v14    # "watchface":Lcom/vectorwatch/android/models/Watchface;
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->streamData:Lcom/vectorwatch/android/models/StreamValueModel;

    .line 818
    .local v9, "defaultValuesForStream":Lcom/vectorwatch/android/models/StreamValueModel;
    if-eqz v9, :cond_3

    .line 819
    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    .line 820
    .restart local v12    # "gson":Lcom/google/gson/Gson;
    invoke-virtual {v12, v9}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 821
    .local v13, "streamUpdates":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SET UP STREAM - place default value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 822
    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->pushNotification(Ljava/lang/String;Landroid/content/Context;)V

    .line 826
    .end local v9    # "defaultValuesForStream":Lcom/vectorwatch/android/models/StreamValueModel;
    .end local v12    # "gson":Lcom/google/gson/Gson;
    .end local v13    # "streamUpdates":Ljava/lang/String;
    .end local v15    # "watchfaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    :cond_3
    return-void
.end method

.method public static setUpDefaultsOnWatchface(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)V
    .locals 19
    .param p0, "fullApp"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 662
    if-nez p0, :cond_1

    .line 714
    :cond_0
    :goto_0
    return-void

    .line 666
    :cond_1
    sget-object v7, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Set up defaults on watch - app name = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " app id = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 667
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 669
    .local v4, "appId":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->streamData:Lcom/vectorwatch/android/models/StreamValueModel;

    .line 671
    .local v8, "defaultValuesForStream":Lcom/vectorwatch/android/models/StreamValueModel;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->streams:Ljava/util/List;

    if-eqz v7, :cond_4

    .line 672
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->streams:Ljava/util/List;

    .line 674
    .local v12, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v7, :cond_4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v7, v7, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v7, :cond_4

    .line 675
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v7, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    move-object/from16 v16, v0

    .line 677
    .local v16, "watchfaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/models/Watchface;

    .line 678
    .local v15, "watchface":Lcom/vectorwatch/android/models/Watchface;
    invoke-virtual {v15}, Lcom/vectorwatch/android/models/Watchface;->getId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 680
    .local v5, "watchfaceId":I
    invoke-virtual {v15}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v10

    .line 682
    .local v10, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    if-eqz v10, :cond_2

    .line 683
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_3
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vectorwatch/android/models/Element;

    .line 684
    .local v9, "element":Lcom/vectorwatch/android/models/Element;
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 685
    .local v6, "elementId":I
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v3

    .line 686
    .local v3, "elementType":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v14

    .line 688
    .local v14, "streamUuid":Ljava/lang/String;
    if-eqz v14, :cond_3

    .line 690
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v2

    .line 692
    .local v2, "defaultSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    if-eqz v2, :cond_3

    .line 693
    invoke-static {v14, v12}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamWithUuidFromList(Ljava/lang/String;Ljava/util/List;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v1

    .local v1, "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v7, p1

    .line 694
    invoke-static/range {v1 .. v7}, Lcom/vectorwatch/android/managers/StreamsManager;->placeDefaultStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIILandroid/content/Context;)V

    .line 698
    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->saveStreamToDatabase(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)Z

    goto :goto_1

    .line 708
    .end local v1    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v2    # "defaultSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v3    # "elementType":Ljava/lang/String;
    .end local v5    # "watchfaceId":I
    .end local v6    # "elementId":I
    .end local v9    # "element":Lcom/vectorwatch/android/models/Element;
    .end local v10    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    .end local v12    # "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .end local v14    # "streamUuid":Ljava/lang/String;
    .end local v15    # "watchface":Lcom/vectorwatch/android/models/Watchface;
    .end local v16    # "watchfaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    :cond_4
    if-eqz v8, :cond_0

    .line 709
    new-instance v11, Lcom/google/gson/Gson;

    invoke-direct {v11}, Lcom/google/gson/Gson;-><init>()V

    .line 710
    .local v11, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v11, v8}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 711
    .local v13, "streamUpdates":Ljava/lang/String;
    sget-object v7, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "SET UP STREAM - place default value "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 712
    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->pushNotification(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public static setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I
    .locals 1
    .param p0, "channelLabel"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "secondsToLive"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 740
    invoke-static {p0, p1, p2, p3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static triggerRestoreStreams(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 326
    sget-object v1, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    invoke-static {v1, p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamsWithGivenState(Lcom/vectorwatch/android/models/Stream$State;Landroid/content/Context;)Ljava/util/List;

    move-result-object v9

    .line 327
    .local v9, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_0

    .line 328
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream;

    .line 329
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1, p0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppUuid(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 330
    .local v2, "uuid":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    .line 331
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v3

    .line 330
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 331
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 332
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v5

    .line 331
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x1

    move-object v7, p0

    .line 330
    invoke-static/range {v0 .. v7}, Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V

    .line 333
    sget-object v1, Lcom/vectorwatch/android/managers/StreamsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RESTORE STREAMS - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v4

    .line 334
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 333
    invoke-interface {v1, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 327
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 336
    .end local v0    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v2    # "uuid":Ljava/lang/String;
    :cond_0
    return-void
.end method
