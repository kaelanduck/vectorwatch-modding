.class public Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;
.super Ljava/lang/Object;
.source "GeofenceManager.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;,
        Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;
    }
.end annotation


# static fields
.field public static final CONNECTION_FAILURE_RESOLUTION_REQUEST:I = 0x2328

.field public static final GEOFENCE_EXPIRATION_TIME:J = -0x1L

.field private static final GEOFENCE_RADIUS_IN_METERS:F = 2000.0f

.field public static final LOITERING_DELAY_MILLIS:I = 0xdbba0

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private geofencesToMonitor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/location/Geofence;",
            ">;"
        }
    .end annotation
.end field

.field private mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mGeofencePendingIntent:Landroid/app/PendingIntent;

.field private mGeofenceRequestState:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

.field private mMainActivityContext:Landroid/content/Context;

.field private timestamp_dwell:J

.field private timestamp_exit:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v0, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-wide v0, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->timestamp_dwell:J

    .line 62
    iput-wide v0, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->timestamp_exit:J

    .line 81
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;->NO_REQUEST:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

    iput-object v0, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofenceRequestState:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

    .line 102
    return-void
.end method

.method private addGeofences(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/Geofence;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "geofences":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/Geofence;>;"
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: Adding geofences."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 246
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;->ADD_GEOFENCE_REQUEST:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

    iput-object v0, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofenceRequestState:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

    .line 248
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->GeofencingApi:Lcom/google/android/gms/location/GeofencingApi;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 250
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->getGeofencingRequest(Ljava/util/List;)Lcom/google/android/gms/location/GeofencingRequest;

    move-result-object v2

    .line 251
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->getGeofencePendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    .line 248
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/location/GeofencingApi;->addGeofences(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 252
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 253
    return-void
.end method

.method public static checkForLocationPermission(Lcom/vectorwatch/android/models/ChannelSettingsChange;)V
    .locals 14
    .param p0, "changeChannelSettings"    # Lcom/vectorwatch/android/models/ChannelSettingsChange;

    .prologue
    .line 616
    const/4 v5, 0x0

    .line 619
    .local v5, "needsLocation":Z
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->getNewChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v9

    .line 620
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v8

    .line 621
    .local v8, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 622
    .local v3, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 623
    .local v2, "key":Ljava/lang/String;
    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/settings/Setting;

    .line 624
    .local v0, "currSetting":Lcom/vectorwatch/android/models/settings/Setting;
    if-eqz v0, :cond_0

    .line 627
    const/4 v7, 0x0

    .line 628
    .local v7, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPermission;>;"
    iget-object v7, v0, Lcom/vectorwatch/android/models/settings/Setting;->permissions:Ljava/util/List;

    .line 629
    if-eqz v7, :cond_0

    .line 630
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/models/StreamPermission;

    .line 631
    .local v6, "permission":Lcom/vectorwatch/android/models/StreamPermission;
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/StreamPermission;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "LOCATION_GEO_FENCING"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 632
    const/4 v5, 0x1

    goto :goto_0

    .line 642
    .end local v0    # "currSetting":Lcom/vectorwatch/android/models/settings/Setting;
    .end local v2    # "key":Ljava/lang/String;
    .end local v6    # "permission":Lcom/vectorwatch/android/models/StreamPermission;
    .end local v7    # "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPermission;>;"
    :cond_2
    if-eqz v5, :cond_3

    .line 644
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v9

    const-string v10, "pref_geofence_last_known_location"

    const-class v11, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    invoke-virtual {v9, v10, v11}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    .line 646
    .local v1, "currentGeofence":Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    if-eqz v1, :cond_3

    .line 647
    new-instance v4, Lcom/vectorwatch/android/models/LocationCloudRequestModel;

    .line 648
    invoke-virtual {v1}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->getLatitude()D

    move-result-wide v10

    .line 649
    invoke-virtual {v1}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->getLongitude()D

    move-result-wide v12

    invoke-direct {v4, v10, v11, v12, v13}, Lcom/vectorwatch/android/models/LocationCloudRequestModel;-><init>(DD)V

    .line 650
    .local v4, "locationRequestModel":Lcom/vectorwatch/android/models/LocationCloudRequestModel;
    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setLocation(Lcom/vectorwatch/android/models/LocationCloudRequestModel;)V

    .line 654
    .end local v1    # "currentGeofence":Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    .end local v4    # "locationRequestModel":Lcom/vectorwatch/android/models/LocationCloudRequestModel;
    :cond_3
    return-void
.end method

.method private getGeofenceFromPreferences()Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    .locals 3

    .prologue
    .line 352
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: Get from preferences"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 353
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "pref_geofence_last_known_location"

    const-class v2, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    .line 354
    invoke-virtual {v0, v1, v2}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    return-object v0
.end method

.method private getGeofencePendingIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 313
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofencePendingIntent:Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    .line 314
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE_MNGR]: Pending intent reused."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 315
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofencePendingIntent:Landroid/app/PendingIntent;

    .line 325
    :goto_0
    return-object v1

    .line 318
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE_MNGR]: Pending intent created."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 320
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.aol.android.geofence.ACTION_RECEIVE_GEOFENCE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 323
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mMainActivityContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofencePendingIntent:Landroid/app/PendingIntent;

    .line 325
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofencePendingIntent:Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method private getGeofencingRequest(Ljava/util/List;)Lcom/google/android/gms/location/GeofencingRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/Geofence;",
            ">;)",
            "Lcom/google/android/gms/location/GeofencingRequest;"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "geofences":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/Geofence;>;"
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE_MNGR]: Get geofencing req."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 298
    new-instance v0, Lcom/google/android/gms/location/GeofencingRequest$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/location/GeofencingRequest$Builder;-><init>()V

    .line 300
    .local v0, "builder":Lcom/google/android/gms/location/GeofencingRequest$Builder;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/GeofencingRequest$Builder;->setInitialTrigger(I)Lcom/google/android/gms/location/GeofencingRequest$Builder;

    .line 301
    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/GeofencingRequest$Builder;->addGeofences(Ljava/util/List;)Lcom/google/android/gms/location/GeofencingRequest$Builder;

    .line 303
    invoke-virtual {v0}, Lcom/google/android/gms/location/GeofencingRequest$Builder;->build()Lcom/google/android/gms/location/GeofencingRequest;

    move-result-object v1

    return-object v1
.end method

.method private getLastKnownLocation()Landroid/location/Location;
    .locals 6

    .prologue
    .line 364
    sget-object v1, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v2, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2}, Lcom/google/android/gms/location/FusedLocationProviderApi;->getLastLocation(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/location/Location;

    move-result-object v0

    .line 366
    .local v0, "lastKnownLocation":Landroid/location/Location;
    if-eqz v0, :cond_0

    .line 367
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GEOFENCE_MNGR]: Get last known location. It is lat = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "long ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 369
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 367
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 372
    :cond_0
    return-object v0
.end method

.method public static getRegisteredGeofence()Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    .locals 4

    .prologue
    .line 534
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v1

    const-string v2, "pref_geofence_last_known_location"

    const-class v3, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    invoke-virtual {v1, v2, v3}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    .line 541
    .local v0, "lastKnownGeofence":Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    if-nez v0, :cond_0

    .line 542
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE] : The last know geofence is null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 543
    const/4 v0, 0x0

    .line 546
    .end local v0    # "lastKnownGeofence":Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    :cond_0
    return-object v0
.end method

.method private initialSetup()V
    .locals 14

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->getGeofenceFromPreferences()Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    move-result-object v11

    .line 175
    .local v11, "lastKnownGeofence":Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    if-eqz v11, :cond_0

    .line 177
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GEOFENCE_MNGR]: Last saved geofence details lat = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "long = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 179
    invoke-virtual {v11}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 183
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v12

    .line 185
    .local v12, "location":Landroid/location/Location;
    if-nez v12, :cond_2

    .line 186
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE_MNGR]: No last location found."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 190
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mMainActivityContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/location/LocationManager;

    .line 191
    .local v13, "locationManager":Landroid/location/LocationManager;
    const-string v1, "gps"

    invoke-virtual {v13, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE_MNGR]: Location provider (GPS) disabled"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 220
    .end local v13    # "locationManager":Landroid/location/LocationManager;
    :goto_0
    return-void

    .line 194
    .restart local v13    # "locationManager":Landroid/location/LocationManager;
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v2, "[GEOFENCE_MNGR]: Location provider did not get the last location yet"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    .end local v13    # "locationManager":Landroid/location/LocationManager;
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GEOFENCE_MNGR]: Last known location = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " long = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 201
    invoke-virtual {v12}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 200
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 204
    new-instance v0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    .line 205
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 206
    invoke-virtual {v12}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 207
    invoke-virtual {v12}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const/high16 v6, 0x44fa0000    # 2000.0f

    const-wide/16 v7, -0x1

    const/4 v9, 0x6

    invoke-direct/range {v0 .. v9}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;-><init>(Ljava/lang/String;DDFJI)V

    .line 211
    .local v0, "simpleGeofence":Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
    invoke-static {v0}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->saveGeofenceToPreferences(Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;)V

    .line 214
    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->toGeofence()Lcom/google/android/gms/location/Geofence;

    move-result-object v10

    .line 216
    .local v10, "geofence":Lcom/google/android/gms/location/Geofence;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->geofencesToMonitor:Ljava/util/ArrayList;

    .line 217
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->geofencesToMonitor:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->geofencesToMonitor:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->addGeofences(Ljava/util/List;)V

    goto :goto_0
.end method

.method private isGooglePlayServicesAvailable(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 229
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 230
    .local v0, "resultCode":I
    if-nez v0, :cond_0

    .line 231
    const/4 v1, 0x1

    .line 233
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private removeGeofenceFromPreferences()V
    .locals 2

    .prologue
    .line 342
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: Remove from preferences"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 343
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "pref_geofence_last_known_location"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method public static removeGeofenceFromPrefs()V
    .locals 2

    .prologue
    .line 521
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "pref_geofence_last_known_location"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 522
    return-void
.end method

.method private static saveGeofenceToPreferences(Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;)V
    .locals 3
    .param p0, "geofence"    # Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;

    .prologue
    .line 334
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GEOFENCE_MNGR]: Save to preferences id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 335
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "pref_geofence_last_known_location"

    invoke-virtual {v0, v1, p0}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 336
    return-void
.end method

.method private stopGeofenceMonitoring()V
    .locals 3

    .prologue
    .line 276
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: removing all geofences."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 278
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->GeofencingApi:Lcom/google/android/gms/location/GeofencingApi;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 281
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->getGeofencePendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    .line 278
    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/GeofencingApi;->removeGeofences(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/PendingResult;

    .line 282
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public handleGeofenceDwellEventEvent(Lcom/vectorwatch/android/events/GeofenceDwellEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/GeofenceDwellEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 402
    return-void
.end method

.method public handleGeofenceErrorEvent(Lcom/vectorwatch/android/events/GeofenceErrorEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/GeofenceErrorEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 453
    return-void
.end method

.method public handleGeofenceExitEventEvent(Lcom/vectorwatch/android/events/GeofenceExitEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/GeofenceExitEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 428
    return-void
.end method

.method public handleLocationServicesChangedEvent(Lcom/vectorwatch/android/events/LocationServicesChangedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/LocationServicesChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 470
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 107
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: Connected!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 126
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: Failed!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 113
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->log:Lorg/slf4j/Logger;

    const-string v1, "[GEOFENCE_MNGR]: Suspended!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "result"    # Lcom/google/android/gms/common/api/Result;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 155
    return-void
.end method

.method public removeGeofences(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "geofencesIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;->REMOVE_GEOFENCE_REQUEST:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

    iput-object v0, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mGeofenceRequestState:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$RequestState;

    .line 264
    iget-object v0, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->GeofencingApi:Lcom/google/android/gms/location/GeofencingApi;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/location/GeofencingApi;->removeGeofences(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/List;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 268
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 270
    :cond_0
    return-void
.end method
