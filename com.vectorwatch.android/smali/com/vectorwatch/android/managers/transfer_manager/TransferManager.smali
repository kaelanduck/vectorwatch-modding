.class public interface abstract Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;
.super Ljava/lang/Object;
.source "TransferManager.java"


# virtual methods
.method public abstract receivedVftpMessage(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
.end method

.method public abstract requestFile(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)V
.end method

.method public abstract sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V
.end method

.method public abstract sendFile([BILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V
.end method

.method public abstract setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;
.end method

.method public abstract setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;
.end method
