.class public Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;
.super Ljava/lang/Object;
.source "TransferManagerImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mBus:Lcom/vectorwatch/android/MainThreadBus;

.field private mContext:Landroid/content/Context;

.field private mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

.field private mFileQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;",
            ">;"
        }
    .end annotation
.end field

.field private mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

.field private mLastReceivedPackage:I

.field private mTotalPackagesToReceive:I

.field private mVftpDataSendRequestId:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 47
    iput v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    .line 48
    iput v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mLastReceivedPackage:I

    .line 50
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 51
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    .line 60
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    .line 61
    return-void
.end method

.method private getFileFromPath(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$VftpFileType;ISZZ)Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .locals 14
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p3, "fileId"    # I
    .param p4, "uncompressedFileSize"    # S
    .param p5, "isCompressed"    # Z
    .param p6, "isForced"    # Z

    .prologue
    .line 159
    if-nez p1, :cond_0

    .line 160
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - null path received."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x0

    .line 194
    :goto_0
    return-object v0

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    .line 166
    .local v8, "assetManager":Landroid/content/res/AssetManager;
    if-nez v8, :cond_1

    .line 167
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Trying to real localization file but can\'t retrieve assets manager."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 168
    const/4 v0, 0x0

    goto :goto_0

    .line 171
    :cond_1
    const/4 v12, 0x0

    .line 173
    .local v12, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    :try_start_0
    new-instance v9, Ljava/io/BufferedInputStream;

    invoke-virtual {v8, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v9, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 174
    .local v9, "bis":Ljava/io/BufferedInputStream;
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 178
    .local v10, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_1
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->read()I

    move-result v13

    .local v13, "readByte":I
    const/4 v1, -0x1

    if-eq v13, v1, :cond_2

    .line 179
    invoke-virtual {v10, v13}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 189
    .end local v9    # "bis":Ljava/io/BufferedInputStream;
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v13    # "readByte":I
    :catch_0
    move-exception v11

    .line 190
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    .line 191
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Error"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move-object v0, v12

    .end local v12    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    goto :goto_0

    .line 182
    .end local v0    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v9    # "bis":Ljava/io/BufferedInputStream;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .restart local v13    # "readByte":I
    :cond_2
    :try_start_1
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->close()V

    .line 183
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 185
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 187
    .local v3, "result":[B
    new-instance v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    array-length v1, v3

    int-to-short v4, v1

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;-><init>(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I[BSSZZ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .end local v12    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .restart local v0    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    goto :goto_0
.end method

.method private getFileFromPath([BLcom/vectorwatch/android/utils/Constants$VftpFileType;ISZZ)Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .locals 8
    .param p1, "content"    # [B
    .param p2, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p3, "fileId"    # I
    .param p4, "uncompressedFileSize"    # S
    .param p5, "isCompressed"    # Z
    .param p6, "isForced"    # Z

    .prologue
    .line 200
    const/4 v0, 0x0

    .line 201
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    if-eqz p1, :cond_0

    .line 202
    new-instance v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .end local v0    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    array-length v1, p1

    int-to-short v4, v1

    move-object v1, p2

    move v2, p3

    move-object v3, p1

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;-><init>(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I[BSSZZ)V

    .line 208
    .restart local v0    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    :goto_0
    return-object v0

    .line 205
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Error - null file content"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getFileName(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/lang/String;
    .locals 2
    .param p1, "baseFileName"    # Ljava/lang/String;
    .param p2, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p3, "fileId"    # I

    .prologue
    .line 663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUniqueFileId(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/lang/String;
    .locals 2
    .param p1, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p2, "fileId"    # I

    .prologue
    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isFileTypeInQueue(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)Z
    .locals 3
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .prologue
    .line 641
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 643
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 644
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 646
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 647
    const/4 v2, 0x1

    .line 651
    .end local v0    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isTransferInProgress()Z
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-eqz v0, :cond_0

    .line 670
    const/4 v0, 0x1

    .line 673
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populateReceivedFileContent(I[B)Z
    .locals 5
    .param p1, "index"    # I
    .param p2, "data"    # [B

    .prologue
    const/4 v1, 0x0

    .line 554
    if-nez p2, :cond_0

    .line 555
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - trying to add null data to a file content."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 558
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v2}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v2

    if-nez v2, :cond_1

    .line 559
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Trying to add file content, but the member variable for VFTP file has not been allocated."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 576
    :goto_0
    return v1

    .line 564
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v2}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v2

    array-length v2, v2

    mul-int/lit16 v3, p1, 0x80

    array-length v4, p2

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_2

    .line 565
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Trying to add more data than the file_content property has been allocated for."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 569
    :cond_2
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    mul-int/lit16 v3, p1, 0x80

    invoke-virtual {v2, v3, p2}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->addContent(I[B)Z

    move-result v0

    .line 571
    .local v0, "addedSuccessfully":Z
    if-nez v0, :cond_3

    .line 572
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Could not add received data to file content property."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 576
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private populateReceivedFileFields(SZZSLcom/vectorwatch/android/utils/Constants$VftpFileType;I)Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .locals 2
    .param p1, "fileSize"    # S
    .param p2, "isCompressed"    # Z
    .param p3, "isForced"    # Z
    .param p4, "compressedSize"    # S
    .param p5, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p6, "fileId"    # I

    .prologue
    .line 530
    new-instance v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-direct {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;-><init>()V

    .line 531
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setUncompressedSize(Ljava/lang/Short;)V

    .line 532
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setIsCompressed(Ljava/lang/Boolean;)V

    .line 533
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setIsForced(Ljava/lang/Boolean;)V

    .line 534
    invoke-static {p4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setCompressedSize(Ljava/lang/Short;)V

    .line 535
    invoke-virtual {v0, p5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 536
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setFileId(Ljava/lang/Integer;)V

    .line 538
    if-eqz p2, :cond_0

    .line 539
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->allocateDataBuffer(I)V

    .line 544
    :goto_0
    return-object v0

    .line 541
    :cond_0
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->allocateDataBuffer(I)V

    goto :goto_0
.end method

.method private printQueue()V
    .locals 5

    .prologue
    .line 808
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 809
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    if-eqz v0, :cond_0

    .line 810
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VFTP - TEST - queue: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 813
    .end local v0    # "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    :cond_1
    return-void
.end method

.method private receivedData(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 10
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 390
    iget-object v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-nez v7, :cond_1

    .line 391
    sget-object v7, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v8, "VFTP - RECEIVING DATA even though there is no record of a file being sent by the watch."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 398
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v6

    .line 401
    .local v6, "type":B
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    .line 402
    .local v3, "index":S
    iput v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mLastReceivedPackage:I

    .line 403
    sget-object v7, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VFTP - Received index = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " out of "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 406
    sget-object v7, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VFTP - length = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v9

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " DATA SLICE = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 407
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v9

    invoke-static {v9}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 406
    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 410
    iget v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mLastReceivedPackage:I

    add-int/lit8 v7, v7, 0x1

    iget v8, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    if-ge v7, v8, :cond_2

    .line 411
    const/16 v4, 0x80

    .line 424
    .local v4, "sizeOfFileSlice":I
    :goto_1
    sget-object v7, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VFTP - size of slice = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 426
    new-array v1, v4, [B

    .line 427
    .local v1, "data":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 430
    invoke-direct {p0, v3, v1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->populateReceivedFileContent(I[B)Z

    move-result v5

    .line 432
    .local v5, "success":Z
    if-nez v5, :cond_4

    .line 434
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    goto/16 :goto_0

    .line 415
    .end local v1    # "data":[B
    .end local v4    # "sizeOfFileSlice":I
    .end local v5    # "success":Z
    :cond_2
    iget-object v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsCompressed()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 416
    iget-object v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getCompressedSize()Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Short;->shortValue()S

    move-result v7

    rem-int/lit16 v4, v7, 0x80

    .restart local v4    # "sizeOfFileSlice":I
    goto :goto_1

    .line 418
    .end local v4    # "sizeOfFileSlice":I
    :cond_3
    iget-object v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getUncompressedSize()Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Short;->shortValue()S

    move-result v7

    rem-int/lit16 v4, v7, 0x80

    .restart local v4    # "sizeOfFileSlice":I
    goto :goto_1

    .line 440
    .restart local v1    # "data":[B
    .restart local v5    # "success":Z
    :cond_4
    sget-object v7, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VFTP - last package = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mLastReceivedPackage:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " total = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 441
    iget v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mLastReceivedPackage:I

    add-int/lit8 v7, v7, 0x1

    iget v8, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    if-ne v7, v8, :cond_0

    .line 442
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v7

    new-instance v8, Lcom/vectorwatch/android/events/ReceivedVftpFileFromWatchEvent;

    iget-object v9, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-direct {v8, v9}, Lcom/vectorwatch/android/events/ReceivedVftpFileFromWatchEvent;-><init>(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)V

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 445
    iget-object v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v2

    .line 448
    .local v2, "fileType":Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    iget-object v7, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v2, v7}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->storeReceivedData(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)V

    .line 451
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->resetForReceivedData()V

    goto/16 :goto_0
.end method

.method private receivedPut(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 13
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 301
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 306
    .local v7, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->get()B

    .line 309
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    .line 312
    .local v1, "fileSize":S
    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/vectorwatch/android/utils/FileUtils;->isSpaceAvailable(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-static {v0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpStatus(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpStatus;)Ljava/util/UUID;

    .line 382
    :goto_0
    return-void

    .line 318
    :cond_0
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->get()B

    move-result v9

    .line 319
    .local v9, "flags":B
    and-int/lit8 v0, v9, 0x1

    int-to-byte v10, v0

    .line 320
    .local v10, "isCompressed":B
    and-int/lit8 v0, v9, 0x2

    int-to-byte v11, v0

    .line 323
    .local v11, "isForced":B
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    .line 326
    .local v4, "fileCompressedSize":S
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->get()B

    move-result v8

    .line 327
    .local v8, "fileTypeByte":B
    invoke-static {v8}, Lcom/vectorwatch/android/utils/Constants;->getVftpFileTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v5

    .line 330
    .local v5, "fileType":Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 332
    .local v6, "fileId":I
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - Msg received. Type = PUT | file_size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | isComp/isForced = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | compressed_size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | file_type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 334
    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | file_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 332
    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 350
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v5, v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "workout_progress_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 351
    invoke-direct {p0, v5, v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->getUniqueFileId(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 350
    invoke-static {v0}, Lcom/vectorwatch/android/utils/FileUtils;->isFileCreated(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    invoke-direct {p0, v5, v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->getUniqueFileId(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/FileUtils;->getCurrentWorkoutFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/FileManager;->deleteFile(Ljava/io/File;)Z

    .line 357
    :cond_1
    const/4 v0, 0x1

    if-ne v10, v0, :cond_2

    .line 358
    invoke-static {v4}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getPackageCount(I)I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    .line 364
    :goto_1
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 365
    .local v12, "respBuffer":Ljava/nio/ByteBuffer;
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-nez v0, :cond_5

    .line 366
    if-eqz v10, :cond_3

    const/4 v2, 0x1

    :goto_2
    if-eqz v11, :cond_4

    const/4 v3, 0x1

    :goto_3
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->populateReceivedFileFields(SZZSLcom/vectorwatch/android/utils/Constants$VftpFileType;I)Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 374
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-static {v0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpStatus(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpStatus;)Ljava/util/UUID;

    goto/16 :goto_0

    .line 360
    .end local v12    # "respBuffer":Ljava/nio/ByteBuffer;
    :cond_2
    invoke-static {v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getPackageCount(I)I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    goto :goto_1

    .line 366
    .restart local v12    # "respBuffer":Ljava/nio/ByteBuffer;
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 379
    :cond_5
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Received PUT even though current transfer TO watch file is not null."

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_ERROR:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-static {v0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpStatus(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpStatus;)Ljava/util/UUID;

    goto/16 :goto_0
.end method

.method private receivedRequest(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 7
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 255
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v4

    .line 256
    .local v4, "payload":[B
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 261
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    .line 264
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 265
    .local v3, "fileTypeByte":B
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 266
    .local v2, "fileType":Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    packed-switch v3, :pswitch_data_0

    .line 282
    :goto_0
    :pswitch_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 292
    .local v1, "fileId":I
    iget-object v5, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_NOT_FOUND:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-static {v5, v6}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpStatus(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpStatus;)Ljava/util/UUID;

    .line 293
    return-void

    .line 271
    .end local v1    # "fileId":I
    :pswitch_1
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 272
    goto :goto_0

    .line 274
    :pswitch_2
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 275
    goto :goto_0

    .line 277
    :pswitch_3
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private receivedStatus(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 6
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 217
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 220
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    .line 222
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    .line 224
    .local v1, "statusByte":B
    sget-object v3, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "VFTP - status received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 225
    packed-switch v1, :pswitch_data_0

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 227
    :pswitch_0
    iget-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-eqz v3, :cond_0

    .line 228
    iget-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-direct {p0, v3}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->sendFileData(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)Z

    move-result v2

    .line 229
    .local v2, "success":Z
    if-nez v2, :cond_0

    .line 230
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    goto :goto_0

    .line 236
    .end local v2    # "success":Z
    :pswitch_1
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/VftpStatusNoSpaceEvent;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/VftpStatusNoSpaceEvent;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 239
    :pswitch_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/VftpStatusFileNotFoundStatus;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/VftpStatusFileNotFoundStatus;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 242
    :pswitch_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/VftpStatusFileExistsEvent;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/VftpStatusFileExistsEvent;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 225
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private resetForReceivedData()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 461
    iput v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mTotalPackagesToReceive:I

    .line 462
    iput v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mLastReceivedPackage:I

    .line 463
    return-void
.end method

.method private sendDetailsForNextFile()V
    .locals 5

    .prologue
    .line 680
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_2

    .line 681
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Triggered send operation but there is no file in queue."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 726
    :cond_1
    :goto_0
    return-void

    .line 685
    :cond_2
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 687
    .local v0, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    if-nez v0, :cond_3

    .line 688
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - first element in queue is null."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 691
    :cond_3
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VFTP - Trying to transfer a file of type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 695
    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v2, v3, :cond_4

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-direct {p0, v2}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->isFileTypeInQueue(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 697
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - reseting LOCALE - flag"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 698
    const-string v2, "flag_sync_locale"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 703
    :cond_4
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VFTP - SEND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileId()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 705
    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsCompressed()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsForced()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getUncompressedSize()Ljava/lang/Short;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 706
    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getCompressedSize()Ljava/lang/Short;

    move-result-object v2

    if-nez v2, :cond_6

    .line 707
    :cond_5
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Trying to send file with incorrect details."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 708
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    .line 711
    :cond_6
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    if-nez v2, :cond_7

    .line 712
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Trying to send file from null context."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 713
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    .line 718
    :cond_7
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 721
    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsForced()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->sendPutFileInfo(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;Z)Z

    move-result v1

    .line 723
    .local v1, "success":Z
    if-nez v1, :cond_1

    .line 724
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    goto/16 :goto_0
.end method

.method private sendFileData(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)Z
    .locals 1
    .param p1, "file"    # Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .prologue
    .line 613
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpData(Landroid/content/Context;Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    .line 614
    const/4 v0, 0x1

    return v0
.end method

.method private sendPutFileInfo(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;Z)Z
    .locals 7
    .param p1, "file"    # Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    .param p2, "isForced"    # Z

    .prologue
    .line 590
    invoke-virtual {p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getUncompressedSize()Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v1

    .line 591
    .local v1, "size":S
    invoke-virtual {p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsCompressed()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 592
    .local v3, "isCompressed":Z
    invoke-virtual {p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v0

    array-length v0, v0

    int-to-short v2, v0

    .line 593
    .local v2, "compressedSize":S
    invoke-virtual {p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v5

    .line 594
    .local v5, "fileType":Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    invoke-virtual {p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 596
    .local v6, "fileId":I
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    move v4, p2

    invoke-static/range {v0 .. v6}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpPut(Landroid/content/Context;SSZZLcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/util/UUID;

    .line 603
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 599
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v4, "VFTP - Trying to PUT file, but context is null"

    invoke-interface {v0, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 600
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private storeReceivedData(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)V
    .locals 5
    .param p1, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p2, "fileId"    # I

    .prologue
    .line 471
    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->getVal()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 504
    :goto_0
    :pswitch_0
    return-void

    .line 489
    :pswitch_1
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP: Store received workout data."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 490
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->getUniqueFileId(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/FileUtils;->getCurrentWorkoutFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 491
    .local v1, "file":Ljava/io/File;
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v2}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v0

    .line 492
    .local v0, "content":[B
    invoke-static {v1, v0}, Lcom/vectorwatch/android/cloud_communication/FileManager;->performWriteToFile(Ljava/io/File;[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 493
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    .line 494
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/FileManager;->closeFile(Ljava/io/File;)Z

    .line 495
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/FileManager;->deleteFile(Ljava/io/File;)Z

    goto :goto_0

    .line 498
    :cond_0
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/FileManager;->closeFile(Ljava/io/File;)Z

    .line 499
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getUploadDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/FileManager;->generateUploadFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/cloud_communication/FileManager;->renameFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z

    .line 500
    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/FileManager;->uploadAllFilesToCloud(Landroid/content/Context;)V

    goto :goto_0

    .line 471
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private transferFailedResets()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 622
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileFromWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v0, v1, :cond_0

    .line 624
    const-string v0, "flag_sync_locale"

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 629
    :cond_0
    iput-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 630
    iput-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    .line 631
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->resetForReceivedData()V

    .line 632
    return-void
.end method


# virtual methods
.method public handleCommandStatusEvent(Lcom/vectorwatch/android/events/CommandStatusEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/vectorwatch/android/events/CommandStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 731
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    if-nez v1, :cond_1

    .line 769
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 736
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getCommandUuid()Ljava/util/UUID;

    move-result-object v1

    if-nez v1, :cond_2

    .line 737
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP: Received command status event for command with null uuid."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 741
    :cond_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getCommandUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 746
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getStatus()Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    move-result-object v0

    .line 748
    .local v0, "status":Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl$1;->$SwitchMap$com$vectorwatch$android$events$CommandStatusEvent$Status:[I

    invoke-virtual {v0}, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 753
    :pswitch_1
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - Sent chunk = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getPacketIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getTotalPackets()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 754
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v1, v2, :cond_0

    .line 755
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/LanguageFileProgress;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getPacketIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 756
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getTotalPackets()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/events/LanguageFileProgress;-><init>(II)V

    .line 755
    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 760
    :pswitch_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;-><init>(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 761
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v1, v2, :cond_3

    .line 762
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/LanguageFileProgress;

    invoke-direct {v2, v4, v4}, Lcom/vectorwatch/android/events/LanguageFileProgress;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 765
    :cond_3
    iput-object v5, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 766
    iput-object v5, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    goto/16 :goto_0

    .line 748
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 773
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v0, v1, :cond_0

    .line 775
    const-string v0, "flag_sync_locale"

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 779
    :cond_0
    iput-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileToWatch:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 780
    iput-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mVftpDataSendRequestId:Ljava/util/UUID;

    .line 781
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->resetForReceivedData()V

    .line 784
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 785
    return-void
.end method

.method public handleVftpFileTransferredToWatchCompletedEvent(Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 789
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;->getFile()Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v1

    if-nez v1, :cond_1

    .line 790
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "VFTP - Transfer competed yet the event is null or has no file details."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 791
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->sendDetailsForNextFile()V

    .line 805
    :goto_0
    return-void

    .line 795
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;->getFile()Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v1, v2, :cond_2

    .line 796
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;->getFile()Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 798
    .local v0, "fileId":I
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - File with id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " successfully sent. Sending the sync_localization "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "command."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 801
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendLocalization(Landroid/content/Context;I)Ljava/util/UUID;

    .line 804
    .end local v0    # "fileId":I
    :cond_2
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->sendDetailsForNextFile()V

    goto :goto_0
.end method

.method public receivedVftpMessage(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 4
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 126
    if-nez p1, :cond_1

    .line 127
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - received null message from watch."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 128
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->transferFailedResets()V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 133
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    .line 135
    .local v1, "type":B
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 137
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Received STATUS data from watch."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 138
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->receivedStatus(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto :goto_0

    .line 139
    :cond_2
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 141
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Received REQ data from watch."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 142
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->receivedRequest(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto :goto_0

    .line 143
    :cond_3
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 145
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Received PUT data from watch."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->receivedPut(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto :goto_0

    .line 147
    :cond_4
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 149
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - Received DATA message from watch."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->receivedData(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto :goto_0
.end method

.method public requestFile(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)V
    .locals 1
    .param p1, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p2, "fileId"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendVftpRequest(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/util/UUID;

    .line 122
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V
    .locals 8
    .param p1, "fileCompletePath"    # Ljava/lang/String;
    .param p2, "fileId"    # I
    .param p3, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p4, "uncompressedFileSize"    # S
    .param p5, "isCompressed"    # Z
    .param p6, "isForced"    # Z

    .prologue
    .line 80
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move v3, p2

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->getFileFromPath(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$VftpFileType;ISZZ)Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v7

    .line 81
    .local v7, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    if-nez v7, :cond_0

    .line 96
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v0, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->printQueue()V

    .line 90
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->isTransferInProgress()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "VFTP - No transfer in progress. Triggering transfer for next file."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->sendDetailsForNextFile()V

    goto :goto_0

    .line 94
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "VFTP - Transfer in progress. Triggering transfer for next file."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendFile([BILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V
    .locals 8
    .param p1, "content"    # [B
    .param p2, "fileId"    # I
    .param p3, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p4, "uncompressedFileSize"    # S
    .param p5, "isCompressed"    # Z
    .param p6, "isForced"    # Z

    .prologue
    .line 101
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move v3, p2

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->getFileFromPath([BLcom/vectorwatch/android/utils/Constants$VftpFileType;ISZZ)Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v7

    .line 102
    .local v7, "file":Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
    if-nez v7, :cond_0

    .line 117
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mFileQueue:Ljava/util/Queue;

    invoke-interface {v0, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->printQueue()V

    .line 111
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->isTransferInProgress()Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "VFTP - No transfer in progress. Triggering transfer for next file."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 113
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->sendDetailsForNextFile()V

    goto :goto_0

    .line 115
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "VFTP - Transfer in progress. Triggering transfer for next file."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;

    move-result-object v0

    return-object v0
.end method

.method public setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mContext:Landroid/content/Context;

    .line 67
    return-object p0
.end method

.method public bridge synthetic setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;

    move-result-object v0

    return-object v0
.end method

.method public setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;
    .locals 0
    .param p1, "eventBus"    # Lcom/vectorwatch/android/MainThreadBus;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;->mBus:Lcom/vectorwatch/android/MainThreadBus;

    .line 73
    return-object p0
.end method
