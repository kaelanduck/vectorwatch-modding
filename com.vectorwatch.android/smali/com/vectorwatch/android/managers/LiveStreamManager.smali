.class public Lcom/vectorwatch/android/managers/LiveStreamManager;
.super Lcom/vectorwatch/android/managers/StreamsManager;
.source "LiveStreamManager.java"

# interfaces
.implements Lcom/vectorwatch/android/managers/LiveStreamManagerInterface;


# static fields
.field public static final ALTITUDE_KEY:Ljava/lang/String; = "GPS_ALT"

.field private static final GPS_LOCATION_TIMEOUT:F = 10.0f

.field public static final LATITUDE_KEY:Ljava/lang/String; = "GPS_LAT"

.field public static final LONGITUDE_KEY:Ljava/lang/String; = "GPS_LON"

.field private static final METER_PER_SECOND_TO_MILES_PER_HOUR:F = 2.23694f

.field private static final MIN_ACC:F = 20.0f

.field public static final NUMBER_OF_FEET_PER_METER:D = 3.28084

.field public static final PACE_KEY:Ljava/lang/String; = "GPS_PACE"

.field private static final PACE_MULT:I = 0x3c

.field public static final SPEED_KEY:Ljava/lang/String; = "GPS_SPD"

.field private static final SPEED_MIN_LIMIT:F = 0.278f

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private currentApplicationId:Ljava/lang/Integer;

.field private currentWatchfaceId:Ljava/lang/Integer;

.field private formatter:Ljava/text/DecimalFormat;

.field private mContext:Landroid/content/Context;

.field private mLiveStreamHandler:Landroid/os/Handler;

.field private mLiveStreamHash:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLiveStreamRunnable:Ljava/lang/Runnable;

.field private mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

.field private mNextUpdate:I

.field private mStreamList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/LiveStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/StreamsManager;-><init>()V

    .line 58
    const v0, 0x7fffffff

    iput v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mNextUpdate:I

    .line 66
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->formatter:Ljava/text/DecimalFormat;

    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 70
    new-instance v0, Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-direct {v0, p1}, Lcom/vectorwatch/android/utils/LocationHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    .line 72
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    .line 74
    new-instance v0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/managers/LiveStreamManager$1;-><init>(Lcom/vectorwatch/android/managers/LiveStreamManager;)V

    iput-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    .line 94
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/managers/LiveStreamManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/LiveStreamManager;->computeTimers()V

    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/managers/LiveStreamManager;Lcom/vectorwatch/android/models/LiveStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/LiveStream;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/managers/LiveStreamManager;->getDataForWatch(Lcom/vectorwatch/android/models/LiveStream;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/managers/LiveStreamManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/LiveStreamManager;->pushDataToWatch()V

    return-void
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/managers/LiveStreamManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 34
    iget v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mNextUpdate:I

    return v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/managers/LiveStreamManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private computeTimers()V
    .locals 4

    .prologue
    .line 316
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 317
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/LiveStream;

    .line 318
    .local v1, "stream":Lcom/vectorwatch/android/models/LiveStream;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/LiveStream;->getNextUpdate()I

    move-result v2

    iget v3, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mNextUpdate:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/LiveStream;->setNextUpdate(I)V

    .line 316
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 320
    .end local v1    # "stream":Lcom/vectorwatch/android/models/LiveStream;
    :cond_0
    return-void
.end method

.method private getDataForWatch(Lcom/vectorwatch/android/models/LiveStream;)V
    .locals 18
    .param p1, "liveStream"    # Lcom/vectorwatch/android/models/LiveStream;

    .prologue
    .line 156
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_LAT"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 157
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->isGPSAvailable()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 158
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_LAT"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 159
    .local v6, "format":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->getLocation()Landroid/location/Location;

    move-result-object v7

    .line 161
    .local v7, "location":Landroid/location/Location;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/vectorwatch/android/managers/LiveStreamManager;->isLocationValid(Landroid/location/Location;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 162
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v7}, Landroid/location/Location;->getLatitude()D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 163
    .local v11, "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v11, v13, v14}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 299
    .end local v6    # "format":Ljava/lang/String;
    .end local v7    # "location":Landroid/location/Location;
    .end local v11    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 165
    .restart local v6    # "format":Ljava/lang/String;
    .restart local v7    # "location":Landroid/location/Location;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f0901c3

    .line 166
    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 165
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto :goto_0

    .line 169
    .end local v6    # "format":Ljava/lang/String;
    .end local v7    # "location":Landroid/location/Location;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f090136

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto :goto_0

    .line 172
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_LON"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 173
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->isGPSAvailable()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 174
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_LON"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 175
    .restart local v6    # "format":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->getLocation()Landroid/location/Location;

    move-result-object v7

    .line 176
    .restart local v7    # "location":Landroid/location/Location;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/vectorwatch/android/managers/LiveStreamManager;->isLocationValid(Landroid/location/Location;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 177
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v7}, Landroid/location/Location;->getLongitude()D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 178
    .restart local v11    # "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v11, v13, v14}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 180
    .end local v11    # "value":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f0901c3

    .line 181
    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 180
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 184
    .end local v6    # "format":Ljava/lang/String;
    .end local v7    # "location":Landroid/location/Location;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f090136

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 187
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_SPD"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 188
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->isGPSAvailable()Z

    move-result v12

    if-eqz v12, :cond_b

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_SPD"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 191
    .restart local v6    # "format":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->getLocation()Landroid/location/Location;

    move-result-object v7

    .line 193
    .restart local v7    # "location":Landroid/location/Location;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/vectorwatch/android/managers/LiveStreamManager;->isLocationValid(Landroid/location/Location;)Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-virtual {v7}, Landroid/location/Location;->hasSpeed()Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-virtual {v7}, Landroid/location/Location;->getAccuracy()F

    move-result v12

    const/high16 v13, 0x41a00000    # 20.0f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_a

    .line 194
    invoke-virtual {v7}, Landroid/location/Location;->getSpeed()F

    move-result v10

    .line 195
    .local v10, "speed":F
    const v12, 0x3e8e5604    # 0.278f

    cmpl-float v12, v10, v12

    if-lez v12, :cond_8

    .line 196
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 197
    const v12, 0x40666666    # 3.6f

    mul-float/2addr v10, v12

    .line 198
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 199
    .restart local v11    # "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "km/h"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 202
    .end local v11    # "value":Ljava/lang/String;
    :cond_7
    const v12, 0x400f2a06

    mul-float/2addr v10, v12

    .line 203
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 204
    .restart local v11    # "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "mph"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 208
    .end local v11    # "value":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 209
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0901cb

    .line 210
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "km/h"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 209
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 212
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0901cb

    .line 213
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "mph"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 212
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 217
    .end local v10    # "speed":F
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f0901c3

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 221
    .end local v6    # "format":Ljava/lang/String;
    .end local v7    # "location":Landroid/location/Location;
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f090136

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 224
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_PACE"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_14

    .line 225
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->isGPSAvailable()Z

    move-result v12

    if-eqz v12, :cond_13

    .line 226
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->getLocation()Landroid/location/Location;

    move-result-object v7

    .line 228
    .restart local v7    # "location":Landroid/location/Location;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/vectorwatch/android/managers/LiveStreamManager;->isLocationValid(Landroid/location/Location;)Z

    move-result v12

    if-eqz v12, :cond_12

    invoke-virtual {v7}, Landroid/location/Location;->hasSpeed()Z

    move-result v12

    if-eqz v12, :cond_12

    invoke-virtual {v7}, Landroid/location/Location;->getAccuracy()F

    move-result v12

    const/high16 v13, 0x41a00000    # 20.0f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_12

    .line 229
    invoke-virtual {v7}, Landroid/location/Location;->getSpeed()F

    move-result v10

    .line 230
    .restart local v10    # "speed":F
    const v12, 0x3e8e5604    # 0.278f

    cmpl-float v12, v10, v12

    if-lez v12, :cond_10

    .line 231
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 232
    const v12, 0x40666666    # 3.6f

    mul-float/2addr v10, v12

    .line 233
    const/high16 v12, 0x42700000    # 60.0f

    div-float v8, v12, v10

    .line 234
    .local v8, "pace":F
    float-to-double v12, v8

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    sub-double/2addr v12, v14

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    mul-double/2addr v12, v14

    double-to-int v12, v12

    mul-int/lit8 v12, v12, 0x3c

    div-int/lit8 v9, v12, 0x64

    .line 235
    .local v9, "sec":I
    const/high16 v12, 0x426c0000    # 59.0f

    cmpg-float v12, v8, v12

    if-gez v12, :cond_d

    .line 236
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->formatter:Ljava/text/DecimalFormat;

    .line 237
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v15

    int-to-long v0, v15

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->formatter:Ljava/text/DecimalFormat;

    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/km"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 236
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 240
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0901ca

    .line 241
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/km"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 240
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 244
    .end local v8    # "pace":F
    .end local v9    # "sec":I
    :cond_e
    const v12, 0x400f2a06

    mul-float/2addr v10, v12

    .line 245
    const/high16 v12, 0x42700000    # 60.0f

    div-float v8, v12, v10

    .line 246
    .restart local v8    # "pace":F
    float-to-double v12, v8

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    sub-double/2addr v12, v14

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    mul-double/2addr v12, v14

    double-to-int v12, v12

    mul-int/lit8 v12, v12, 0x3c

    div-int/lit8 v9, v12, 0x64

    .line 247
    .restart local v9    # "sec":I
    const/high16 v12, 0x426c0000    # 59.0f

    cmpg-float v12, v8, v12

    if-gez v12, :cond_f

    .line 248
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->formatter:Ljava/text/DecimalFormat;

    .line 249
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v15

    int-to-long v0, v15

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->formatter:Ljava/text/DecimalFormat;

    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/mi"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 248
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 252
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0901ca

    .line 253
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/mi"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 252
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 257
    .end local v8    # "pace":F
    .end local v9    # "sec":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 258
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0901ca

    .line 259
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/km"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 258
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 261
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0901ca

    .line 262
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/mi"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    .line 261
    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 266
    .end local v10    # "speed":F
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f0901c3

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 270
    .end local v7    # "location":Landroid/location/Location;
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f090136

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 273
    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_ALT"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 274
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->isGPSAvailable()Z

    move-result v12

    if-eqz v12, :cond_17

    .line 275
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "GPS_ALT"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 276
    .restart local v6    # "format":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/LocationHelper;->getLocation()Landroid/location/Location;

    move-result-object v7

    .line 278
    .restart local v7    # "location":Landroid/location/Location;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/vectorwatch/android/managers/LiveStreamManager;->isLocationValid(Landroid/location/Location;)Z

    move-result v12

    if-eqz v12, :cond_16

    invoke-virtual {v7}, Landroid/location/Location;->hasAltitude()Z

    move-result v12

    if-eqz v12, :cond_16

    .line 279
    invoke-virtual {v7}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    .line 280
    .local v2, "altitude":D
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_15

    .line 281
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 282
    .restart local v11    # "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "m"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 285
    .end local v11    # "value":Ljava/lang/String;
    :cond_15
    const-wide v12, 0x400a3f290abb44e5L    # 3.28084

    mul-double v4, v2, v12

    .line 286
    .local v4, "altitudeInFoot":D
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 287
    .restart local v11    # "value":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "ft"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 291
    .end local v2    # "altitude":D
    .end local v4    # "altitudeInFoot":D
    .end local v11    # "value":Ljava/lang/String;
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f0901c3

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0

    .line 295
    .end local v6    # "format":Ljava/lang/String;
    .end local v7    # "location":Landroid/location/Location;
    :cond_17
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/LiveStream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    const v14, 0x7f090136

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v13, v14, v15}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    goto/16 :goto_0
.end method

.method private isLocationValid(Landroid/location/Location;)Z
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 142
    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 143
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPermissionGranted(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1
    .param p0, "permission"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 138
    invoke-static {p1, p0}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pushDataToWatch()V
    .locals 5

    .prologue
    .line 305
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->currentApplicationId:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->currentWatchfaceId:Ljava/lang/Integer;

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdatesForLiveStream(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/util/List;

    move-result-object v1

    .line 307
    .local v1, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 308
    .local v0, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    iget-object v3, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_0

    .line 310
    .end local v0    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_0
    return-void
.end method

.method private refreshStreams()V
    .locals 4

    .prologue
    .line 357
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/LiveStreamManager;->setNextUpdateTime()V

    .line 358
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 360
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 365
    :goto_0
    return-void

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 363
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mNextUpdate:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private setNextUpdateTime()V
    .locals 3

    .prologue
    .line 326
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 327
    iget v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mNextUpdate:I

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/LiveStream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/LiveStream;->getRefreshInterval()I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mNextUpdate:I

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 329
    :cond_0
    return-void
.end method


# virtual methods
.method public addLiveStream(Lcom/vectorwatch/android/models/LiveStream;)V
    .locals 2
    .param p1, "stream"    # Lcom/vectorwatch/android/models/LiveStream;

    .prologue
    .line 104
    if-nez p1, :cond_0

    .line 105
    sget-object v0, Lcom/vectorwatch/android/managers/LiveStreamManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Live stream add - stream is null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/LiveStreamManager;->isPermissionGranted(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    new-instance v1, Lcom/vectorwatch/android/managers/LiveStreamManager$2;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/managers/LiveStreamManager$2;-><init>(Lcom/vectorwatch/android/managers/LiveStreamManager;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/LocationHelper;->startLocationUpdates(Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;)Lcom/vectorwatch/android/utils/LocationHelper;

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/LiveStreamManager;->refreshStreams()V

    goto :goto_0
.end method

.method public clearLiveStreamManager()V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 394
    :cond_2
    return-void
.end method

.method public closeEventBus()V
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 436
    return-void
.end method

.method public containsKey(II)Z
    .locals 3
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I

    .prologue
    .line 375
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 440
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 442
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 443
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 444
    iget-object v0, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLocationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 446
    :cond_0
    return-void
.end method

.method public declared-synchronized refreshLiveStreamManager(II)V
    .locals 8
    .param p1, "appId"    # I
    .param p2, "faceId"    # I

    .prologue
    .line 405
    monitor-enter p0

    :try_start_0
    sget-object v5, Lcom/vectorwatch/android/managers/LiveStreamManager;->log:Lorg/slf4j/Logger;

    const-string v6, "Debug - Refresh stream live"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 406
    iget-object v5, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 407
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 408
    .local v4, "uniqueLiveHash":Ljava/lang/String;
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->removeLiveStream(II)V

    .line 410
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->currentApplicationId:Ljava/lang/Integer;

    .line 411
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->currentWatchfaceId:Ljava/lang/Integer;

    .line 413
    iget-object v5, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mContext:Landroid/content/Context;

    invoke-static {v5, p1, p2}, Lcom/vectorwatch/android/managers/StreamsManager;->getActiveLiveStreamsForAppAndWatchface(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v3

    .line 415
    .local v3, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    sget-object v5, Lcom/vectorwatch/android/managers/LiveStreamManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Debug - Refresh stream live size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 416
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 417
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 418
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/settings/Setting;

    .line 419
    .local v2, "setting":Lcom/vectorwatch/android/models/settings/Setting;
    new-instance v1, Lcom/vectorwatch/android/models/LiveStream;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/LiveStream;-><init>()V

    .line 420
    .local v1, "liveStream":Lcom/vectorwatch/android/models/LiveStream;
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/LiveStream;->setStream(Lcom/vectorwatch/android/models/Stream;)V

    .line 421
    invoke-virtual {v1, p1}, Lcom/vectorwatch/android/models/LiveStream;->setAppId(I)V

    .line 422
    invoke-virtual {v1, p2}, Lcom/vectorwatch/android/models/LiveStream;->setWatchfaceId(I)V

    .line 423
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/LiveStream;->setNextUpdate(I)V

    .line 424
    iget v5, v2, Lcom/vectorwatch/android/models/settings/Setting;->refreshInterval:I

    sget v7, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->MIN_REFRESH_TIME:I

    if-le v5, v7, :cond_0

    iget v5, v2, Lcom/vectorwatch/android/models/settings/Setting;->refreshInterval:I

    :goto_2
    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/LiveStream;->setRefreshInterval(I)V

    .line 425
    iget-object v5, v2, Lcom/vectorwatch/android/models/settings/Setting;->value:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/LiveStream;->setValue(Ljava/lang/String;)V

    .line 427
    iget-object v5, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 428
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/vectorwatch/android/managers/LiveStreamManager;->addLiveStream(Lcom/vectorwatch/android/models/LiveStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 405
    .end local v0    # "i":I
    .end local v1    # "liveStream":Lcom/vectorwatch/android/models/LiveStream;
    .end local v2    # "setting":Lcom/vectorwatch/android/models/settings/Setting;
    .end local v3    # "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .end local v4    # "uniqueLiveHash":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 424
    .restart local v0    # "i":I
    .restart local v1    # "liveStream":Lcom/vectorwatch/android/models/LiveStream;
    .restart local v2    # "setting":Lcom/vectorwatch/android/models/settings/Setting;
    .restart local v3    # "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .restart local v4    # "uniqueLiveHash":Ljava/lang/String;
    :cond_0
    :try_start_1
    sget v5, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->MIN_REFRESH_TIME:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 416
    .end local v1    # "liveStream":Lcom/vectorwatch/android/models/LiveStream;
    .end local v2    # "setting":Lcom/vectorwatch/android/models/settings/Setting;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 432
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public removeLiveStream(II)V
    .locals 5
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I

    .prologue
    .line 339
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mLiveStreamHash:Ljava/util/Set;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 341
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 342
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v1, "removeStreamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LiveStream;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 344
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/LiveStream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/LiveStream;->getAppId()I

    move-result v2

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/LiveStream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/LiveStream;->getWatchfaceId()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 345
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 348
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager;->mStreamList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 349
    invoke-direct {p0}, Lcom/vectorwatch/android/managers/LiveStreamManager;->refreshStreams()V

    .line 351
    .end local v0    # "i":I
    .end local v1    # "removeStreamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LiveStream;>;"
    :cond_2
    return-void
.end method
