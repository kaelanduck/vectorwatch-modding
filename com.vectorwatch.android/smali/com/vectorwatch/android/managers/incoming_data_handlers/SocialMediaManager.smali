.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;
.super Ljava/lang/Object;
.source "SocialMediaManager.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private isRegistered:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->mContext:Landroid/content/Context;

    .line 50
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private shareActivityToFacebook(Landroid/content/Context;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsSteps(Landroid/content/Context;)I

    move-result v5

    .line 54
    .local v5, "crtSteps":I
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsCalories(Landroid/content/Context;)I

    move-result v2

    .line 55
    .local v2, "crtCalories":I
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsSleep(Landroid/content/Context;)I

    move-result v4

    .line 56
    .local v4, "crtSleep":I
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTotalsDistance(Landroid/content/Context;)I

    move-result v3

    .line 58
    .local v3, "crtDistance":I
    sget-object v11, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v11

    float-to-int v9, v11

    .line 59
    .local v9, "goalSteps":I
    sget-object v11, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v11

    float-to-int v6, v11

    .line 60
    .local v6, "goalCalories":I
    sget-object v11, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v11

    float-to-int v8, v11

    .line 61
    .local v8, "goalSleep":I
    sget-object v11, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    .line 62
    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v11

    .line 61
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/vectorwatch/android/utils/Helpers;->getDistanceWithCrtUnitSystem(Landroid/content/Context;F)F

    move-result v11

    float-to-int v7, v11

    .line 64
    .local v7, "goalDistance":I
    new-instance v10, Lcom/vectorwatch/android/models/ShareValuesItem;

    new-instance v11, Lcom/vectorwatch/android/models/CloudGoalValueModel;

    invoke-direct {v11, v9, v5}, Lcom/vectorwatch/android/models/CloudGoalValueModel;-><init>(II)V

    new-instance v12, Lcom/vectorwatch/android/models/CloudGoalValueModel;

    invoke-direct {v12, v6, v2}, Lcom/vectorwatch/android/models/CloudGoalValueModel;-><init>(II)V

    new-instance v13, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    int-to-float v14, v7

    int-to-float v15, v3

    invoke-direct {v13, v14, v15}, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;-><init>(FF)V

    new-instance v14, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    int-to-float v15, v8

    int-to-float v0, v4

    move/from16 v16, v0

    invoke-direct/range {v14 .. v16}, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;-><init>(FF)V

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/vectorwatch/android/models/ShareValuesItem;-><init>(Lcom/vectorwatch/android/models/CloudGoalValueModel;Lcom/vectorwatch/android/models/CloudGoalValueModel;Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;)V

    .line 72
    .local v10, "shareValuesItem":Lcom/vectorwatch/android/models/ShareValuesItem;
    invoke-static {}, Lcom/facebook/AccessToken;->getCurrentAccessToken()Lcom/facebook/AccessToken;

    move-result-object v1

    .line 74
    .local v1, "accessToken":Lcom/facebook/AccessToken;
    if-eqz v1, :cond_0

    .line 75
    const-string v11, "goalAchievement"

    new-instance v12, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;-><init>(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)V

    move-object/from16 v0, p1

    invoke-static {v0, v11, v10, v12}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getShareLink(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lretrofit/Callback;)V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->startLoginProcess(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private startLoginProcess(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    .line 141
    new-instance v0, Lcom/vectorwatch/android/models/AlertMessage;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f090124

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0901e9

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0901d8

    .line 142
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xbb8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/models/AlertWatchFlags;

    const/4 v7, 0x0

    invoke-direct {v6, v10, v7}, Lcom/vectorwatch/android/models/AlertWatchFlags;-><init>(ZZ)V

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/models/AlertMessage;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/AlertWatchFlags;Ljava/util/List;)V

    .line 144
    .local v0, "message":Lcom/vectorwatch/android/models/AlertMessage;
    invoke-static {v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v1

    invoke-static {p1, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 146
    :try_start_0
    new-instance v9, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileActivity;

    invoke-direct {v9, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .local v9, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v9, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 148
    const/high16 v1, 0x20000

    invoke-virtual {v9, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 149
    const-string v1, "share"

    const/4 v2, 0x1

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 150
    invoke-virtual {p1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-void

    .line 151
    .end local v9    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v8

    .line 152
    .local v8, "e":Ljava/lang/Exception;
    new-instance v9, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileActivity;

    invoke-direct {v9, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    .restart local v9    # "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v9, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 154
    const-string v1, "share"

    invoke-virtual {v9, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 155
    invoke-virtual {p1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public handleActivityTotalsUpdateEvent(Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->unregisterToEventBus()V

    .line 184
    iget-object v0, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->shareActivityToFacebook(Landroid/content/Context;)V

    .line 185
    return-void
.end method

.method public isRegistered()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->isRegistered:Z

    return v0
.end method

.method public registerToEventBus()V
    .locals 1

    .prologue
    .line 170
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->isRegistered:Z

    .line 172
    return-void
.end method

.method public shareActivity()V
    .locals 5

    .prologue
    .line 160
    iget-boolean v2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->isRegistered:Z

    if-nez v2, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->registerToEventBus()V

    .line 162
    iget-object v2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getActivitySyncLastTimestamp(Landroid/content/Context;)I

    move-result v1

    .line 163
    .local v1, "startTime":I
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentTime()I

    move-result v0

    .line 164
    .local v0, "endTime":I
    sget-object v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SYNC_GET_ACTIVITY: (before request) start = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " end = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 165
    iget-object v2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActivityRequest(Landroid/content/Context;II)Ljava/util/UUID;

    .line 167
    .end local v0    # "endTime":I
    .end local v1    # "startTime":I
    :cond_0
    return-void
.end method

.method public unregisterToEventBus()V
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->isRegistered:Z

    if-eqz v0, :cond_0

    .line 176
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 178
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->isRegistered:Z

    .line 179
    return-void
.end method
