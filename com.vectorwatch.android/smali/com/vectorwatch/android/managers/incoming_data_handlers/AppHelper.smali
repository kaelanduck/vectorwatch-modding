.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;
.super Ljava/lang/Object;
.source "AppHelper.java"


# static fields
.field private static final ACTION_KEY_L_PRESS:I = 0x2

.field private static final ACTION_KEY_PRESS:I = 0x0

.field private static final ACTION_KEY__DB_PRESS:I = 0x1

.field private static final APPLICATION_ACTION_DESTINATION_ENDPOINT_NEXT_TRACK:Ljava/lang/String; = "UID_FOR_NEXT_TRACK"

.field private static final APPLICATION_ACTION_DESTINATION_ENDPOINT_PLAY_PAUSE:Ljava/lang/String; = "UID_FOR_PLAY/PAUSE"

.field private static final APPLICATION_ACTION_DESTINATION_ENDPOINT_PREV_TRACK:Ljava/lang/String; = "UID_FOR_PREV_TRACK"

.field private static final APPLICATION_ACTION_DESTINATION_ENDPOINT_UPDATE:Ljava/lang/String; = "UID_UPDATE_MUSIC_DATA"

.field private static final APPLICATION_ACTION_DESTINATION_ENDPOINT_VOL_DOWN:Ljava/lang/String; = "UID_FOR_VOL_DOWN"

.field private static final APPLICATION_ACTION_DESTINATION_ENDPOINT_VOL_UP:Ljava/lang/String; = "UID_FOR_VOL_UP"

.field private static final APPLICATION_ACTION_DESTINATION_OPEN_CAMERA:Ljava/lang/String; = "openCamera"

.field private static final APPLICATION_ACTION_DESTINATION_TAKE_PHOTO:Ljava/lang/String; = "takePhoto"

.field private static final APPLICATION_ACTION_DESTINATION_TYPE_CLOUD:Ljava/lang/String; = "CLOUD"

.field private static final APPLICATION_ACTION_DESTINATION_TYPE_MOBILE:Ljava/lang/String; = "MOBILE"

.field private static final APPLICATION_ACTION_KEY_DB_PRESS:Ljava/lang/String; = "DB_PRESS"

.field private static final APPLICATION_ACTION_KEY_L_PRESS:Ljava/lang/String; = "L_PRESS"

.field private static final APPLICATION_ACTION_KEY_PRESS:Ljava/lang/String; = "PRESS"

.field private static final APPLICATION_WATCH_BUTTON_ACTION_DOUBLE_PRESS:Ljava/lang/String; = "DOUBLE_PRESS"

.field private static final APPLICATION_WATCH_BUTTON_ACTION_LONG_PRESS:Ljava/lang/String; = "LONG_PRESS"

.field private static final APPLICATION_WATCH_BUTTON_ACTION_PRESS:Ljava/lang/String; = "PRESS"

.field private static final APPLICATION_WATCH_BUTTON_DOWN:Ljava/lang/String; = "DOWN"

.field private static final APPLICATION_WATCH_BUTTON_MIDDLE:Ljava/lang/String; = "MIDDLE"

.field private static final APPLICATION_WATCH_BUTTON_UP:Ljava/lang/String; = "UP"

.field public static final BATTERY_STREAM_CHANNEL_LABEL:Ljava/lang/String; = "21F1FC164BE69DA33A39BDE17402A120"

.field public static final BATTERY_STREAM_UUID:Ljava/lang/String; = "560e9eb0e48057d1360d3700b9c77777"

.field private static final BUTTON_1:I = 0x0

.field private static final BUTTON_2:I = 0x1

.field private static final BUTTON_3:I = 0x2

.field public static final CMDNAME:Ljava/lang/String; = "command"

.field public static final CMDNEXT:Ljava/lang/String; = "next"

.field public static final CMDPAUSE:Ljava/lang/String; = "pause"

.field public static final CMDPREVIOUS:Ljava/lang/String; = "previous"

.field public static final CMDSTOP:Ljava/lang/String; = "stop"

.field public static final CMDTOGGLEPAUSE:Ljava/lang/String; = "togglepause"

.field public static MIN_REFRESH_TIME:I = 0x0

.field public static final MUSIC_AUTHOR_UUID:Ljava/lang/String; = "3906BFAD46D7E6E1BF757891F26AEFB1"

.field public static final MUSIC_NAME_UUID:Ljava/lang/String; = "6375B098967A1B6F1C37C891B02295F2"

.field public static final PREF_FFC:Ljava/lang/String; = "ffc"

.field public static final PREF_FLASH_MODE:Ljava/lang/String; = "flashMode"

.field public static final PREF_FOCUS_MODE:Ljava/lang/String; = "focusMode"

.field public static final PREF_MIRROR_PREVIEW:Ljava/lang/String; = "mirrorPreview"

.field public static final PREF_ZOOM_STYLE:Ljava/lang/String; = "zoomStyle"

.field public static final SERVICECMD:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field public static final TTL_INTERVAL_SECONDS:I = 0x32

.field private static final VECTOR_PATH_DIRECTORY:Ljava/lang/String; = "/VectorWatch/"

.field public static final WATCHFACE_DATA_MESSAGE_TYPE_CANCEL:I = 0x1

.field public static final WATCHFACE_DATA_MESSAGE_TYPE_REQUEST:I

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    .line 115
    const/4 v0, 0x4

    sput v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->MIN_REFRESH_TIME:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static callCloudForButtonPress(Landroid/content/Context;IILcom/vectorwatch/android/models/EventDestination$ButtonWatch;IILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "button"    # Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;
    .param p4, "identifier"    # I
    .param p5, "value"    # I
    .param p6, "buttonIndex"    # Ljava/lang/String;
    .param p7, "buttonEvent"    # Ljava/lang/String;

    .prologue
    .line 367
    new-instance v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;-><init>(Landroid/content/Context;)V

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->setData(IILcom/vectorwatch/android/models/EventDestination$ButtonWatch;IILjava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->enqueueRequest()V

    .line 369
    return-void
.end method

.method public static getAlertFromRetrofitError(Lretrofit/RetrofitError;)Lcom/vectorwatch/android/models/AlertMessage;
    .locals 7
    .param p0, "retrofitError"    # Lretrofit/RetrofitError;

    .prologue
    const/4 v5, 0x0

    .line 534
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v4

    if-nez v4, :cond_0

    .line 535
    sget-object v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    const-string v6, "getAlertFromRetrofitError: getResponse is null!"

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    move-object v4, v5

    .line 549
    :goto_0
    return-object v4

    .line 537
    :cond_0
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v4

    invoke-virtual {v4}, Lretrofit/client/Response;->getBody()Lretrofit/mime/TypedInput;

    move-result-object v4

    if-nez v4, :cond_1

    .line 538
    sget-object v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    const-string v6, "getAlertFromRetrofitError: getResponse\'s body is null!"

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    move-object v4, v5

    .line 539
    goto :goto_0

    .line 542
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v4

    invoke-virtual {v4}, Lretrofit/client/Response;->getBody()Lretrofit/mime/TypedInput;

    move-result-object v4

    check-cast v4, Lretrofit/mime/TypedByteArray;

    invoke-virtual {v4}, Lretrofit/mime/TypedByteArray;->getBytes()[B

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 543
    .local v2, "json":Ljava/lang/String;
    new-instance v4, Lcom/google/gson/GsonBuilder;

    invoke-direct {v4}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/google/gson/GsonBuilder;->serializeNulls()Lcom/google/gson/GsonBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    .line 546
    .local v1, "gson":Lcom/google/gson/Gson;
    :try_start_0
    const-class v4, Lcom/vectorwatch/android/models/AlertMessageResponse;

    invoke-virtual {v1, v2, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/AlertMessageResponse;

    .line 547
    .local v3, "response":Lcom/vectorwatch/android/models/AlertMessageResponse;
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/AlertMessageResponse;->getData()Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;->getAlert()Lcom/vectorwatch/android/models/AlertMessage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 548
    .end local v3    # "response":Lcom/vectorwatch/android/models/AlertMessageResponse;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-object v4, v5

    .line 549
    goto :goto_0
.end method

.method public static getSizeForDataAlerts(Lcom/vectorwatch/android/models/AlertMessage;)I
    .locals 4
    .param p0, "alertMessage"    # Lcom/vectorwatch/android/models/AlertMessage;

    .prologue
    .line 554
    const/4 v1, 0x0

    .line 557
    .local v1, "size":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 558
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 564
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getContent()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 565
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 571
    :goto_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getLabel()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 572
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 578
    :goto_2
    add-int/lit8 v1, v1, 0x1

    .line 582
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 583
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 584
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/AlertCallbacks;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/AlertCallbacks;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v2

    .line 585
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 586
    add-int/lit8 v3, v1, 0x4

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/AlertCallbacks;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int v1, v3, v2

    .line 583
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 560
    .end local v0    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_0

    .line 567
    :cond_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 574
    :cond_2
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 588
    .restart local v0    # "i":I
    :cond_3
    add-int/lit8 v1, v1, 0x4

    goto :goto_4

    .line 593
    .end local v0    # "i":I
    :cond_4
    return v1
.end method

.method public static handleButtonPressed(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dataMsg"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 124
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 129
    .local v11, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 130
    .local v2, "appId":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 131
    .local v3, "watchfaceId":B
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v10

    .line 132
    .local v10, "buttonId":B
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->get()B

    move-result v12

    .line 133
    .local v12, "eventButton":B
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 134
    .local v5, "identifier":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 135
    .local v6, "value":I
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, " App Button press"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, " "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, " "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, " "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 139
    packed-switch v12, :pswitch_data_0

    .line 153
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Invalid event id: "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, " App for button: "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 141
    :pswitch_0
    const-string v8, "PRESS"

    .line 142
    .local v8, "buttonAction":Ljava/lang/String;
    const-string v15, "PRESS"

    .line 157
    .local v15, "generatedEvent":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v9

    .line 158
    .local v9, "appDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-eqz v9, :cond_1

    iget-object v1, v9, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-nez v1, :cond_2

    .line 159
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No app content for app: "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 145
    .end local v8    # "buttonAction":Ljava/lang/String;
    .end local v9    # "appDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .end local v15    # "generatedEvent":Ljava/lang/String;
    :pswitch_1
    const-string v8, "DOUBLE_PRESS"

    .line 146
    .restart local v8    # "buttonAction":Ljava/lang/String;
    const-string v15, "DB_PRESS"

    .line 147
    .restart local v15    # "generatedEvent":Ljava/lang/String;
    goto :goto_1

    .line 149
    .end local v8    # "buttonAction":Ljava/lang/String;
    .end local v15    # "generatedEvent":Ljava/lang/String;
    :pswitch_2
    const-string v8, "LONG_PRESS"

    .line 150
    .restart local v8    # "buttonAction":Ljava/lang/String;
    const-string v15, "L_PRESS"

    .line 151
    .restart local v15    # "generatedEvent":Ljava/lang/String;
    goto :goto_1

    .line 163
    .restart local v9    # "appDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    :cond_2
    iget-object v1, v9, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v1, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    move-object/from16 v17, v0

    .line 164
    .local v17, "watchfaces":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    if-eqz v17, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v3, :cond_4

    .line 165
    :cond_3
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "App was no watchfaces or invalid id: "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_4
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Watchface;->getEventsDestination()Lcom/vectorwatch/android/models/EventDestination;

    move-result-object v14

    .line 171
    .local v14, "events":Lcom/vectorwatch/android/models/EventDestination;
    if-eqz v14, :cond_0

    .line 174
    packed-switch v10, :pswitch_data_1

    .line 188
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Invalid button id: "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, " App for button: "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 176
    :pswitch_3
    const-string v7, "UP"

    .line 177
    .local v7, "button":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/vectorwatch/android/models/EventDestination;->getBTN1()Ljava/util/List;

    move-result-object v13

    .line 192
    .local v13, "eventButtonList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    :goto_2
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_3
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_0

    .line 193
    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->getEventType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 194
    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->getDestinationType()Ljava/lang/String;

    move-result-object v1

    const-string v4, "MOBILE"

    .line 195
    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 196
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v1, v4, :cond_5

    .line 197
    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->getDestinationEndpoint()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->takeActionForButtonPress(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 180
    .end local v7    # "button":Ljava/lang/String;
    .end local v13    # "eventButtonList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    .end local v16    # "i":I
    :pswitch_4
    const-string v7, "MIDDLE"

    .line 181
    .restart local v7    # "button":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/vectorwatch/android/models/EventDestination;->getBTN2()Ljava/util/List;

    move-result-object v13

    .line 182
    .restart local v13    # "eventButtonList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    goto :goto_2

    .line 184
    .end local v7    # "button":Ljava/lang/String;
    .end local v13    # "eventButtonList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    :pswitch_5
    const-string v7, "DOWN"

    .line 185
    .restart local v7    # "button":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/vectorwatch/android/models/EventDestination;->getBTN3()Ljava/util/List;

    move-result-object v13

    .line 186
    .restart local v13    # "eventButtonList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    goto :goto_2

    .line 199
    .restart local v16    # "i":I
    :cond_5
    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->getDestinationEndpoint()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->takeActionForButtonPressFor43(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :cond_6
    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->getDestinationType()Ljava/lang/String;

    move-result-object v1

    const-string v4, "CLOUD"

    .line 202
    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v8}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->callCloudForButtonPress(Landroid/content/Context;IILcom/vectorwatch/android/models/EventDestination$ButtonWatch;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 192
    :cond_7
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 174
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static handleDataRequest(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dataMsg"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 375
    new-instance v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;-><init>(Landroid/content/Context;)V

    .line 376
    .local v3, "helper":Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->setBinaryData([B)Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .line 378
    iget v5, v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-static {v5, p0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    .line 380
    .local v0, "appDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-nez v0, :cond_1

    .line 381
    sget-object v5, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Request for details, yet app with id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is null in DB."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget v5, v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v6, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v6, v6, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 386
    iget-object v5, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    iget v6, v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Watchface;

    .line 387
    .local v4, "watchface":Lcom/vectorwatch/android/models/Watchface;
    iget v5, v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 388
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v5

    iget v6, v3, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Element;

    .line 390
    .local v2, "element":Lcom/vectorwatch/android/models/Element;
    :try_start_0
    invoke-static {p0, v3, v2}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->handleRemoteMethod(Landroid/content/Context;Lcom/vectorwatch/android/utils/AppDataRequesterHelper;Lcom/vectorwatch/android/models/Element;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v1

    .line 392
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static handleRemoteMethod(Landroid/content/Context;Lcom/vectorwatch/android/utils/AppDataRequesterHelper;Lcom/vectorwatch/android/models/Element;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "helper"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    .param p2, "element"    # Lcom/vectorwatch/android/models/Element;

    .prologue
    .line 403
    if-eqz p2, :cond_2

    .line 404
    :try_start_0
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Element;->getDatasource()Lcom/vectorwatch/android/models/Element$DataSource;

    move-result-object v0

    .line 405
    .local v0, "dataSource":Lcom/vectorwatch/android/models/Element$DataSource;
    iget-object v6, v0, Lcom/vectorwatch/android/models/Element$DataSource;->destinationType:Ljava/lang/String;

    const-string v7, "MOBILE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 406
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v5

    .line 407
    .local v5, "uniqueLabel":Ljava/lang/String;
    iget-object v6, v0, Lcom/vectorwatch/android/models/Element$DataSource;->remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

    iget-object v6, v6, Lcom/vectorwatch/android/models/Element$RemoteMethod;->method:Ljava/lang/String;

    const-string v7, "UID_UPDATE_MUSIC_DATA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 408
    const/4 v1, -0x1

    .line 409
    .local v1, "duration":I
    const-string v6, "3906BFAD46D7E6E1BF757891F26AEFB1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 410
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v6, v6

    add-int/lit8 v6, v6, 0x32

    invoke-static {v6}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v1

    .line 411
    const-string v6, "3906BFAD46D7E6E1BF757891F26AEFB1"

    sget-object v7, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    invoke-static {v6, v7, v1, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 414
    :cond_0
    const-string v6, "6375B098967A1B6F1C37C891B02295F2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 415
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v6, v6

    add-int/lit8 v6, v6, 0x32

    invoke-static {v6}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v1

    .line 416
    const-string v6, "6375B098967A1B6F1C37C891B02295F2"

    sget-object v7, Lcom/vectorwatch/android/VectorApplication;->sLastTrack:Ljava/lang/String;

    invoke-static {v6, v7, v1, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 420
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 421
    .local v4, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 422
    .local v3, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-static {p0, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 429
    .end local v0    # "dataSource":Lcom/vectorwatch/android/models/Element$DataSource;
    .end local v1    # "duration":I
    .end local v3    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    .end local v4    # "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    .end local v5    # "uniqueLabel":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 430
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 432
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    return-void

    .line 425
    .restart local v0    # "dataSource":Lcom/vectorwatch/android/models/Element$DataSource;
    :cond_3
    :try_start_1
    iget-object v6, v0, Lcom/vectorwatch/android/models/Element$DataSource;->destinationType:Ljava/lang/String;

    const-string v7, "CLOUD"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 426
    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->enqueueRequest()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static handleWatchfaceRequest(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dataMsg"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 682
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 683
    .local v1, "dataBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 684
    .local v3, "watchfaceMesssage":B
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 685
    .local v0, "appId":I
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    .line 687
    .local v2, "watchfaceId":B
    packed-switch v3, :pswitch_data_0

    .line 705
    :cond_0
    :goto_0
    return-void

    .line 690
    :pswitch_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->containsKey(II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 694
    sget-object v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DATA_MESSAGE_TYPE_REQUEST Request for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " watchfaceId "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 696
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->refreshLiveStreamManager(II)V

    goto :goto_0

    .line 700
    :pswitch_1
    sget-object v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DATA_MESSAGE_TYPE_REQUEST Cancel for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " watchfaceId "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 702
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->removeLiveStream(II)V

    goto :goto_0

    .line 687
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static openCamera(Landroid/content/Context;ZLandroid/content/SharedPreferences;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "hasFailed"    # Z
    .param p2, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v7, 0x0

    .line 604
    new-instance v0, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;-><init>(Landroid/content/Context;)V

    .line 606
    .local v0, "b":Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->skipConfirm()Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    .line 608
    const-string v6, "ffc"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 609
    sget-object v6, Lcom/commonsware/cwac/cam2/Facing;->FRONT:Lcom/commonsware/cwac/cam2/Facing;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 614
    :goto_0
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->updateMediaStore()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 616
    const-string v6, "mirrorPreview"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 617
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->mirrorPreview()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 620
    :cond_0
    sget-object v6, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->HIGH:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->quality(Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 622
    const-string v6, "focusMode"

    const/4 v7, -0x1

    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v3

    .line 624
    .local v3, "rawFocusMode":I
    packed-switch v3, :pswitch_data_0

    .line 636
    :goto_1
    const-string v6, "flashMode"

    const-string v7, "-1"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 638
    .local v2, "rawFlashMode":I
    packed-switch v2, :pswitch_data_1

    .line 653
    :goto_2
    const-string/jumbo v6, "zoomStyle"

    const-string v7, "0"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 655
    .local v4, "rawZoomStyle":I
    packed-switch v4, :pswitch_data_2

    .line 666
    :goto_3
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/VectorWatch/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 667
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v0, v1}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->to(Ljava/io/File;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 671
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    .line 673
    .local v5, "result":Landroid/content/Intent;
    if-nez p1, :cond_2

    .line 674
    invoke-virtual {p0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 679
    :goto_4
    return-void

    .line 611
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "rawFlashMode":I
    .end local v3    # "rawFocusMode":I
    .end local v4    # "rawZoomStyle":I
    .end local v5    # "result":Landroid/content/Intent;
    :cond_1
    sget-object v6, Lcom/commonsware/cwac/cam2/Facing;->BACK:Lcom/commonsware/cwac/cam2/Facing;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_0

    .line 626
    .restart local v3    # "rawFocusMode":I
    :pswitch_0
    sget-object v6, Lcom/commonsware/cwac/cam2/FocusMode;->CONTINUOUS:Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_1

    .line 629
    :pswitch_1
    sget-object v6, Lcom/commonsware/cwac/cam2/FocusMode;->OFF:Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_1

    .line 632
    :pswitch_2
    sget-object v6, Lcom/commonsware/cwac/cam2/FocusMode;->EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_1

    .line 640
    .restart local v2    # "rawFlashMode":I
    :pswitch_3
    sget-object v6, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_2

    .line 643
    :pswitch_4
    sget-object v6, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_2

    .line 646
    :pswitch_5
    sget-object v6, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_2

    .line 649
    :pswitch_6
    sget-object v6, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_2

    .line 658
    .restart local v4    # "rawZoomStyle":I
    :pswitch_7
    sget-object v6, Lcom/commonsware/cwac/cam2/ZoomStyle;->PINCH:Lcom/commonsware/cwac/cam2/ZoomStyle;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->zoomStyle(Lcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    goto :goto_3

    .line 662
    :pswitch_8
    sget-object v6, Lcom/commonsware/cwac/cam2/ZoomStyle;->SEEKBAR:Lcom/commonsware/cwac/cam2/ZoomStyle;

    invoke-virtual {v0, v6}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->zoomStyle(Lcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    goto :goto_3

    .line 676
    .restart local v1    # "f":Ljava/io/File;
    .restart local v5    # "result":Landroid/content/Intent;
    :cond_2
    const/high16 v6, 0x10000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 677
    invoke-virtual {p0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    .line 624
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 638
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 655
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 9
    .param p0, "appId"    # I
    .param p1, "elementId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "content"    # Ljava/lang/String;
    .param p5, "label"    # Ljava/lang/String;

    .prologue
    .line 447
    const-string v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    mul-int/lit16 v4, p1, 0x100

    add-int/2addr v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 449
    .local v8, "eval":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 450
    .local v7, "callbackList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlertCallbacks;>;"
    new-instance v1, Lcom/vectorwatch/android/models/AlertCallbacks;

    const-string v2, "0"

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->ordinal()I

    move-result v3

    sget-object v4, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 451
    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->ordinal()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/vectorwatch/android/models/AlertCallbacks;-><init>(Ljava/lang/String;II)V

    .line 450
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    new-instance v1, Lcom/vectorwatch/android/models/AlertCallbacks;

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_REFRESH_ELEMENT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->ordinal()I

    move-result v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 453
    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->ordinal()I

    move-result v3

    invoke-direct {v1, v8, v2, v3}, Lcom/vectorwatch/android/models/AlertCallbacks;-><init>(Ljava/lang/String;II)V

    .line 452
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    new-instance v1, Lcom/vectorwatch/android/models/AlertCallbacks;

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_REFRESH_ELEMENT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->ordinal()I

    move-result v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 455
    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->ordinal()I

    move-result v3

    invoke-direct {v1, v8, v2, v3}, Lcom/vectorwatch/android/models/AlertCallbacks;-><init>(Ljava/lang/String;II)V

    .line 454
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    new-instance v0, Lcom/vectorwatch/android/models/AlertMessage;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/models/AlertWatchFlags;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v6, v2, v3}, Lcom/vectorwatch/android/models/AlertWatchFlags;-><init>(ZZ)V

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/models/AlertMessage;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/AlertWatchFlags;Ljava/util/List;)V

    .line 461
    .local v0, "alertMessage":Lcom/vectorwatch/android/models/AlertMessage;
    invoke-static {v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v1

    return-object v1
.end method

.method public static packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B
    .locals 10
    .param p0, "alertMessage"    # Lcom/vectorwatch/android/models/AlertMessage;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 468
    invoke-static {p0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->getSizeForDataAlerts(Lcom/vectorwatch/android/models/AlertMessage;)I

    move-result v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 470
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getTitle()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 471
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    array-length v8, v8

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 472
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 473
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 479
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getContent()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getContent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1

    .line 480
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getContent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    array-length v8, v8

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 481
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getContent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 482
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 488
    :goto_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getLabel()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 489
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    array-length v8, v8

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 490
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 491
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 497
    :goto_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 498
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 499
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_5

    .line 500
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getCallbacks()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/AlertCallbacks;

    .line 501
    .local v1, "callback":Lcom/vectorwatch/android/models/AlertCallbacks;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AlertCallbacks;->getKey()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 502
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AlertCallbacks;->getIndex()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 504
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_3

    .line 505
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    array-length v8, v8

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 506
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AlertCallbacks;->getEval()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 507
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 499
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 475
    .end local v1    # "callback":Lcom/vectorwatch/android/models/AlertCallbacks;
    .end local v4    # "i":I
    :cond_0
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 476
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 484
    :cond_1
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 485
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_1

    .line 493
    :cond_2
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 494
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 509
    .restart local v1    # "callback":Lcom/vectorwatch/android/models/AlertCallbacks;
    .restart local v4    # "i":I
    :cond_3
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 510
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_4

    .line 514
    .end local v1    # "callback":Lcom/vectorwatch/android/models/AlertCallbacks;
    .end local v4    # "i":I
    :cond_4
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 517
    :cond_5
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v8

    add-int/lit8 v8, v8, 0x7

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 518
    .local v2, "dataBytes":Ljava/nio/ByteBuffer;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getAppId()I

    move-result v8

    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 519
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getDisplayTime()I

    move-result v8

    int-to-short v8, v8

    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 520
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getFlags()Lcom/vectorwatch/android/models/AlertWatchFlags;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/AlertWatchFlags;->getSilent()Z

    move-result v8

    if-eqz v8, :cond_6

    move v5, v6

    .line 521
    .local v5, "silent":I
    :goto_5
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/AlertMessage;->getFlags()Lcom/vectorwatch/android/models/AlertWatchFlags;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/AlertWatchFlags;->getForced()Z

    move-result v8

    if-eqz v8, :cond_7

    move v3, v6

    .line 522
    .local v3, "force":I
    :goto_6
    shl-int/lit8 v6, v5, 0x1

    int-to-byte v6, v6

    shl-int/lit8 v7, v3, 0x0

    int-to-byte v7, v7

    or-int/2addr v6, v7

    int-to-byte v6, v6

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 523
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 525
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    return-object v6

    .end local v3    # "force":I
    .end local v5    # "silent":I
    :cond_6
    move v5, v7

    .line 520
    goto :goto_5

    .restart local v5    # "silent":I
    :cond_7
    move v3, v7

    .line 521
    goto :goto_6
.end method

.method private static takeActionForButtonPress(Landroid/content/Context;Ljava/lang/String;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destination"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/16 v9, 0x57

    const/16 v8, 0x55

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 294
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 296
    .local v0, "am":Landroid/media/AudioManager;
    const-string v4, "UID_FOR_PLAY/PAUSE"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 297
    new-instance v1, Landroid/view/KeyEvent;

    invoke-direct {v1, v6, v8}, Landroid/view/KeyEvent;-><init>(II)V

    .line 298
    .local v1, "downEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 300
    new-instance v3, Landroid/view/KeyEvent;

    invoke-direct {v3, v5, v8}, Landroid/view/KeyEvent;-><init>(II)V

    .line 301
    .local v3, "upEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 302
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    .line 303
    .local v2, "manager":Landroid/media/AudioManager;
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;

    invoke-direct {v5, v2, p0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;-><init>(Landroid/media/AudioManager;Landroid/content/Context;)V

    const-wide/16 v6, 0x5dc

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 345
    .end local v1    # "downEvent":Landroid/view/KeyEvent;
    .end local v2    # "manager":Landroid/media/AudioManager;
    .end local v3    # "upEvent":Landroid/view/KeyEvent;
    :goto_0
    return-void

    .line 326
    :cond_0
    const-string v4, "UID_FOR_VOL_DOWN"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 327
    const/4 v4, -0x1

    invoke-virtual {v0, v7, v4, v5}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_0

    .line 328
    :cond_1
    const-string v4, "UID_FOR_VOL_UP"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 329
    invoke-virtual {v0, v7, v5, v5}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_0

    .line 330
    :cond_2
    const-string v4, "UID_FOR_PREV_TRACK"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 331
    new-instance v1, Landroid/view/KeyEvent;

    const/16 v4, 0x58

    invoke-direct {v1, v6, v4}, Landroid/view/KeyEvent;-><init>(II)V

    .line 332
    .restart local v1    # "downEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 334
    new-instance v3, Landroid/view/KeyEvent;

    const/16 v4, 0x58

    invoke-direct {v3, v5, v4}, Landroid/view/KeyEvent;-><init>(II)V

    .line 335
    .restart local v3    # "upEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 336
    .end local v1    # "downEvent":Landroid/view/KeyEvent;
    .end local v3    # "upEvent":Landroid/view/KeyEvent;
    :cond_3
    const-string v4, "UID_FOR_NEXT_TRACK"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 337
    new-instance v1, Landroid/view/KeyEvent;

    invoke-direct {v1, v6, v9}, Landroid/view/KeyEvent;-><init>(II)V

    .line 338
    .restart local v1    # "downEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 340
    new-instance v3, Landroid/view/KeyEvent;

    invoke-direct {v3, v5, v9}, Landroid/view/KeyEvent;-><init>(II)V

    .line 341
    .restart local v3    # "upEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 343
    .end local v1    # "downEvent":Landroid/view/KeyEvent;
    .end local v3    # "upEvent":Landroid/view/KeyEvent;
    :cond_4
    invoke-static {p0, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->testForCameraButtonAction(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static takeActionForButtonPressFor43(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destination"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 218
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 220
    .local v3, "prefs":Landroid/content/SharedPreferences;
    const-string v4, "UID_FOR_PLAY/PAUSE"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 221
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    .line 222
    .local v2, "manager":Landroid/media/AudioManager;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    .local v1, "i":Landroid/content/Intent;
    const-string v4, "command"

    const-string v5, "togglepause"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 226
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$1;

    invoke-direct {v5, v2, p0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$1;-><init>(Landroid/media/AudioManager;Landroid/content/Context;)V

    const-wide/16 v6, 0x5dc

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 266
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "manager":Landroid/media/AudioManager;
    :goto_0
    return-void

    .line 249
    :cond_0
    const-string v4, "UID_FOR_VOL_DOWN"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 250
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 251
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v4, -0x1

    invoke-virtual {v0, v6, v4, v5}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_0

    .line 252
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_1
    const-string v4, "UID_FOR_VOL_UP"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 253
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 254
    .restart local v0    # "am":Landroid/media/AudioManager;
    invoke-virtual {v0, v6, v5, v5}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_0

    .line 255
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_2
    const-string v4, "UID_FOR_PREV_TRACK"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 256
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v4, "command"

    const-string v5, "previous"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 259
    .end local v1    # "i":Landroid/content/Intent;
    :cond_3
    const-string v4, "UID_FOR_NEXT_TRACK"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 260
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 261
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v4, "command"

    const-string v5, "next"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 264
    .end local v1    # "i":Landroid/content/Intent;
    :cond_4
    invoke-static {p0, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->testForCameraButtonAction(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static testForCameraButtonAction(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destination"    # Ljava/lang/String;

    .prologue
    .line 275
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 277
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "openCamera"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 278
    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 279
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 280
    :cond_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->tryOpenCamera(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 282
    :cond_2
    const-string v1, "takePhoto"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 283
    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 284
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 285
    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->takePicture()V

    goto :goto_0

    .line 287
    :cond_3
    invoke-static {p0, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->tryOpenCamera(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method

.method public static tryOpenCamera(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 354
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->openCamera(Landroid/content/Context;ZLandroid/content/SharedPreferences;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :goto_0
    return-void

    .line 355
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    invoke-static {p0, v1, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->openCamera(Landroid/content/Context;ZLandroid/content/SharedPreferences;)V

    goto :goto_0
.end method
