.class public Lcom/vectorwatch/android/managers/EventManager;
.super Ljava/lang/Object;
.source "EventManager.java"


# static fields
.field private static final COLS:[Ljava/lang/String;

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const-class v0, Lcom/vectorwatch/android/managers/EventManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    .line 31
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/managers/EventManager;->COLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkCalendarEventsForSync(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 157
    sget-object v4, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    if-nez v4, :cond_2

    .line 159
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    .line 161
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getNext12HoursEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 164
    .local v3, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 165
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CalendarEventModel;

    .line 166
    .local v1, "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getUniqueLabel()Ljava/lang/String;

    move-result-object v2

    .line 167
    .local v2, "eventLabel":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v1    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    .end local v2    # "eventLabel":Ljava/lang/String;
    :cond_0
    move v5, v6

    .line 207
    :cond_1
    :goto_1
    return v5

    .line 175
    .end local v3    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    :cond_2
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getNext12HoursEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 176
    .restart local v3    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    sget-object v7, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CALENDAR: events = "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-nez v3, :cond_3

    move v4, v5

    :goto_2
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 179
    if-eqz v3, :cond_1

    .line 180
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sget-object v7, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-eq v4, v7, :cond_4

    .line 182
    invoke-static {v3}, Lcom/vectorwatch/android/managers/EventManager;->recomputeEventList(Ljava/util/List;)V

    .line 183
    sget-object v4, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CAL: Do sync lists are different "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    .line 184
    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 183
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move v5, v6

    .line 185
    goto :goto_1

    .line 176
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_2

    .line 190
    :cond_4
    const/4 v0, 0x0

    .line 191
    .local v0, "doSync":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CalendarEventModel;

    .line 192
    .restart local v1    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    sget-object v7, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getUniqueLabel()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 193
    const/4 v0, 0x1

    .line 198
    .end local v1    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    :cond_6
    if-ne v0, v6, :cond_1

    .line 199
    invoke-static {v3}, Lcom/vectorwatch/android/managers/EventManager;->recomputeEventList(Ljava/util/List;)V

    .line 200
    sget-object v4, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    const-string v5, "CAL: Do sync, an existing event or a new one has entered scope"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move v5, v6

    .line 201
    goto/16 :goto_1
.end method

.method public static getCalendarEvents(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getNext12HoursEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 146
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    return-object v0
.end method

.method public static getNext12HoursEvents(Landroid/content/Context;)Ljava/util/List;
    .locals 29
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v14, Landroid/text/format/Time;

    invoke-direct {v14}, Landroid/text/format/Time;-><init>()V

    .line 38
    .local v14, "dayStart":Landroid/text/format/Time;
    invoke-virtual {v14}, Landroid/text/format/Time;->setToNow()V

    .line 40
    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12}, Landroid/text/format/Time;-><init>()V

    .line 41
    .local v12, "dayEnd":Landroid/text/format/Time;
    invoke-virtual {v12, v14}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 42
    iget v4, v14, Landroid/text/format/Time;->hour:I

    add-int/lit8 v4, v4, 0xc

    iput v4, v12, Landroid/text/format/Time;->hour:I

    .line 43
    iget v4, v14, Landroid/text/format/Time;->minute:I

    add-int/lit8 v4, v4, 0x3b

    iput v4, v12, Landroid/text/format/Time;->minute:I

    .line 44
    iget v4, v14, Landroid/text/format/Time;->second:I

    add-int/lit8 v4, v4, 0x3b

    iput v4, v12, Landroid/text/format/Time;->second:I

    .line 46
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 47
    .local v15, "dayStartInMillis":Ljava/lang/Long;
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, 0x3e7

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 49
    .local v13, "dayEndInMillis":Ljava/lang/Long;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v20, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    .line 52
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v21

    .line 53
    .local v21, "eventsUriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v21

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 54
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v21

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 56
    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 57
    .local v5, "eventsUri":Landroid/net/Uri;
    const/4 v11, 0x0

    .line 58
    .local v11, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lcom/vectorwatch/android/managers/EventManager;->COLS:[Ljava/lang/String;

    const-string v7, "allDay == 0"

    const/4 v8, 0x0

    const-string v9, "dtstart ASC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 61
    if-nez v11, :cond_1

    .line 135
    :cond_0
    return-object v20

    .line 65
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_b

    .line 66
    const-string v4, "title"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 67
    .local v28, "title":Ljava/lang/String;
    const-string v4, "eventLocation"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 68
    .local v23, "location":Ljava/lang/String;
    const-string v4, "dtstart"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    .line 69
    .local v25, "startTime":Ljava/lang/Long;
    const-string v4, "dtend"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 70
    .local v17, "endTime":Ljava/lang/Long;
    const-string v4, "duration"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 71
    .local v16, "duration":Ljava/lang/String;
    const-string v4, "selfAttendeeStatus"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 72
    .local v26, "status":I
    sget-object v4, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CAL: Event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 74
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-eqz v4, :cond_2

    if-eqz v16, :cond_3

    .line 78
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v27

    .line 79
    .local v27, "tCalendar":Ljava/util/Calendar;
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 81
    new-instance v18, Landroid/text/format/Time;

    invoke-direct/range {v18 .. v18}, Landroid/text/format/Time;-><init>()V

    .line 82
    .local v18, "evDate":Landroid/text/format/Time;
    invoke-virtual/range {v18 .. v18}, Landroid/text/format/Time;->setToNow()V

    .line 84
    const/16 v4, 0xb

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    move-object/from16 v0, v18

    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 85
    const/16 v4, 0xc

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    move-object/from16 v0, v18

    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 86
    const/16 v4, 0xd

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    move-object/from16 v0, v18

    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 88
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    .line 89
    sget-object v4, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CAL: Event  new "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 91
    new-instance v10, Lcom/vectorwatch/android/managers/DurationCalendar;

    invoke-direct {v10}, Lcom/vectorwatch/android/managers/DurationCalendar;-><init>()V

    .line 92
    .local v10, "cal":Lcom/vectorwatch/android/managers/DurationCalendar;
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/vectorwatch/android/managers/DurationCalendar;->parse(Ljava/lang/String;)V

    .line 93
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v10}, Lcom/vectorwatch/android/managers/DurationCalendar;->getMillis()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 94
    sget-object v4, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CAL: Event  new "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 98
    .end local v10    # "cal":Lcom/vectorwatch/android/managers/DurationCalendar;
    .end local v18    # "evDate":Landroid/text/format/Time;
    .end local v27    # "tCalendar":Ljava/util/Calendar;
    :cond_3
    sget-object v4, Lcom/vectorwatch/android/managers/EventManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CAL: Event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 99
    const/4 v4, 0x2

    move/from16 v0, v26

    if-eq v0, v4, :cond_a

    .line 100
    const-string v4, "settings_contextual_tentative_meetings"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-static {v4, v6, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v24

    .line 102
    .local v24, "showTentative":Z
    if-nez v26, :cond_4

    if-nez v24, :cond_7

    :cond_4
    const/4 v4, 0x3

    move/from16 v0, v26

    if-ne v0, v4, :cond_5

    if-nez v24, :cond_7

    :cond_5
    const/4 v4, 0x4

    move/from16 v0, v26

    if-ne v0, v4, :cond_6

    if-nez v24, :cond_7

    :cond_6
    const/4 v4, 0x1

    move/from16 v0, v26

    if-ne v0, v4, :cond_a

    .line 105
    :cond_7
    if-eqz v28, :cond_8

    const-string v4, ""

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "No title"

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 106
    :cond_8
    const-string v28, "My Event"

    .line 108
    :cond_9
    new-instance v10, Lcom/vectorwatch/android/models/CalendarEventModel;

    new-instance v4, Ljava/util/Date;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    new-instance v6, Ljava/util/Date;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-direct {v10, v0, v1, v4, v6}, Lcom/vectorwatch/android/models/CalendarEventModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    .line 110
    .local v10, "cal":Lcom/vectorwatch/android/models/CalendarEventModel;
    move-object/from16 v0, v20

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    .end local v10    # "cal":Lcom/vectorwatch/android/models/CalendarEventModel;
    .end local v24    # "showTentative":Z
    :cond_a
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    .line 115
    .end local v16    # "duration":Ljava/lang/String;
    .end local v17    # "endTime":Ljava/lang/Long;
    .end local v23    # "location":Ljava/lang/String;
    .end local v25    # "startTime":Ljava/lang/Long;
    .end local v26    # "status":I
    .end local v28    # "title":Ljava/lang/String;
    :cond_b
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 117
    new-instance v4, Lcom/vectorwatch/android/managers/EventManager$1;

    invoke-direct {v4}, Lcom/vectorwatch/android/managers/EventManager$1;-><init>()V

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 129
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v22

    if-ge v0, v4, :cond_0

    .line 130
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/CalendarEventModel;

    .line 131
    .local v19, "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    move/from16 v0, v22

    int-to-byte v4, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/CalendarEventModel;->setIndex(B)V

    .line 132
    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 129
    add-int/lit8 v22, v22, 0x1

    goto :goto_1
.end method

.method private static recomputeEventList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    sget-object v2, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    if-nez v2, :cond_0

    .line 218
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    .line 220
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 221
    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 222
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/CalendarEventModel;

    .line 223
    .local v0, "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CalendarEventModel;->getUniqueLabel()Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "eventLabel":Ljava/lang/String;
    sget-object v3, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 227
    .end local v0    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    .end local v1    # "eventLabel":Ljava/lang/String;
    :cond_1
    return-void
.end method
