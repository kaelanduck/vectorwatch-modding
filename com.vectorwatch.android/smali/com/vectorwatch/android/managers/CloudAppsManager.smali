.class public Lcom/vectorwatch/android/managers/CloudAppsManager;
.super Ljava/lang/Object;
.source "CloudAppsManager.java"


# static fields
.field public static final INSTALL_APP_ID_NOT_SET:I = -0x1

.field public static final REMOVE_APP_ID_NOT_SET:I = -0x1

.field private static final log:Lorg/slf4j/Logger;

.field public static final sendVisibleAppOption:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/vectorwatch/android/managers/CloudAppsManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    .line 42
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->POSITION:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    sput-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendVisibleAppOption:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static appContainsDefaultStreams(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z
    .locals 8
    .param p0, "fullAppDetails"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 371
    if-eqz p0, :cond_2

    iget-object v4, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v4, v4, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 372
    iget-object v4, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v3, v4, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    .line 374
    .local v3, "watchfaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Watchface;

    .line 375
    .local v2, "watchface":Lcom/vectorwatch/android/models/Watchface;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 376
    sget-object v5, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Default streams - App "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " watchface "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getId()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 378
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v1

    .line 380
    .local v1, "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    .line 381
    .local v0, "element":Lcom/vectorwatch/android/models/Element;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 382
    sget-object v4, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Default streams - App "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " watchface "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getId()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " element "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 383
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 382
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 386
    const/4 v4, 0x1

    .line 393
    .end local v0    # "element":Lcom/vectorwatch/android/models/Element;
    .end local v1    # "elementList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    .end local v2    # "watchface":Lcom/vectorwatch/android/models/Watchface;
    .end local v3    # "watchfaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static deleteAppFromLocker(ILandroid/content/Context;)V
    .locals 2
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->softDeleteApp(ILandroid/content/Context;)V

    .line 81
    sget-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    const-string v1, "App deleted. Notify service"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 82
    invoke-static {p1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->notifyCloud(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method public static getAppsWithGivenState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 2
    .param p0, "state"    # Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 96
    .local v0, "cloudElementSummaryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    return-object v0
.end method

.method public static getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;
    .locals 12
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 327
    .local v5, "elements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getContentForApp(ILandroid/content/Context;)[B

    move-result-object v1

    .line 329
    .local v1, "content":[B
    if-nez v1, :cond_1

    .line 355
    :cond_0
    return-object v5

    .line 333
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    .line 335
    .local v3, "contentString":Ljava/lang/String;
    new-instance v6, Lcom/google/gson/Gson;

    invoke-direct {v6}, Lcom/google/gson/Gson;-><init>()V

    .line 336
    .local v6, "gson":Lcom/google/gson/Gson;
    const-class v8, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-virtual {v6, v3, v8}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 337
    .local v0, "appDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    .line 339
    .local v2, "contentPojo":Lcom/vectorwatch/android/models/AppContent;
    if-eqz v2, :cond_0

    .line 340
    iget-object v8, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v8, :cond_0

    .line 341
    iget-object v8, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/models/Watchface;

    .line 342
    .local v7, "watchface":Lcom/vectorwatch/android/models/Watchface;
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 343
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Element;

    .line 345
    .local v4, "element":Lcom/vectorwatch/android/models/Element;
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 346
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/vectorwatch/android/models/Element$Type;->TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 347
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/vectorwatch/android/models/Element$Type;->LONG_TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 348
    :cond_4
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .locals 1
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 359
    invoke-static {p0, p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    return-object v0
.end method

.method public static installAppOnWatchFinished(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V
    .locals 5
    .param p0, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSTALL APP - app name = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    .line 112
    .local v0, "appId":I
    invoke-static {p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 114
    .local v1, "appIdsListAsInstalled":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-gt p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 115
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Installed app finished with wrong position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size of currently installed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 138
    :goto_0
    return-void

    .line 120
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_2

    .line 121
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 124
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 125
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_3
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SETUP APPS: added app = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in DB."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 130
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {v0, v2, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updateAppState(ILcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)V

    .line 131
    invoke-static {v1, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updatePositionsForApps(Ljava/util/List;Landroid/content/Context;)V

    .line 133
    invoke-static {p2}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendAppsOrderToCloud(Landroid/content/Context;)V

    .line 137
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DEFAULT INSTALL - Send event for installed app finished for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z
    .locals 4
    .param p0, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 287
    invoke-static {p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAllInstalledApps(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 289
    .local v1, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 290
    .local v0, "uuid":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 291
    const/4 v2, 0x1

    .line 295
    .end local v0    # "uuid":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .param p0, "appUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 306
    invoke-static {p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAllInstalledApps(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 308
    .local v1, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 309
    .local v0, "uuid":Ljava/lang/String;
    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 310
    const/4 v2, 0x1

    .line 314
    .end local v0    # "uuid":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static notifyCloud(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 232
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 234
    return-void
.end method

.method public static saveAppToDatabase(Lcom/vectorwatch/android/models/DownloadedAppDetails;ILandroid/content/Context;)V
    .locals 5
    .param p0, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p1, "positionInList"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Save app - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    invoke-static {p0, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->saveApp(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z

    move-result v1

    .line 55
    .local v1, "ok":Z
    if-eqz v1, :cond_0

    .line 56
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "APP INSTALL MANAGER - SAVED APP = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    .line 58
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    .line 60
    .local v0, "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    invoke-static {p2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInSetupMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    iget-object v2, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithId(ILandroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/vectorwatch/android/managers/CloudAppsManager;->installAppOnWatchFinished(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    .line 64
    invoke-static {p0, p2}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpDefaultsOnWatchface(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)V

    .line 71
    .end local v0    # "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    :cond_0
    :goto_0
    return-void

    .line 68
    .restart local v0    # "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;

    invoke-direct {v3, p0, p1}, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;-><init>(Lcom/vectorwatch/android/models/DownloadedAppDetails;I)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static sendActiveWatchFace(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V
    .locals 3
    .param p0, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 268
    sget-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending command - VISIBLE_APP - id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 269
    sget-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager$2;->$SwitchMap$com$vectorwatch$android$utils$Constants$CurrentAppOption:[I

    sget-object v1, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendVisibleAppOption:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 277
    :goto_0
    return-void

    .line 271
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendVisibleAppOption:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActiveApp(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;I)Ljava/util/UUID;

    goto :goto_0

    .line 274
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendVisibleAppOption:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    invoke-static {p2, v0, p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActiveApp(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;I)Ljava/util/UUID;

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static sendAppsOrder(Ljava/util/List;Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 260
    .local p0, "appsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-static {p1, p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAppsOrder(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    goto :goto_0
.end method

.method public static sendAppsOrderToCloud(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    const-string v0, "dirty_order_to_cloud"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 144
    new-instance v0, Lcom/vectorwatch/android/managers/CloudAppsManager$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager$1;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncOrder(Landroid/content/Context;Lretrofit/Callback;)V

    .line 158
    return-void
.end method

.method public static triggerUninstallApp(I)V
    .locals 1
    .param p0, "appId"    # I

    .prologue
    .line 237
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerUninstallApp(I)V

    .line 238
    return-void
.end method

.method public static uninstallAppFromWatch(IILandroid/content/Context;)V
    .locals 3
    .param p0, "appId"    # I
    .param p1, "oldPositionInList"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 190
    invoke-static {p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 192
    .local v0, "appIdsListAsInstalled":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 193
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p0, :cond_1

    .line 194
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 199
    :cond_0
    invoke-static {v0, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updatePositionsForApps(Ljava/util/List;Landroid/content/Context;)V

    .line 201
    invoke-static {p2}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendAppsOrderToCloud(Landroid/content/Context;)V

    .line 203
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {p0, p1, v2, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->markAppStateChanging(IILcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)V

    .line 206
    invoke-static {p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->triggerUninstallApp(I)V

    .line 207
    return-void

    .line 192
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static uninstallAppFromWatch(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V
    .locals 5
    .param p0, "elementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "oldPositionInList"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uninstalling app "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from watch."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 171
    .local v0, "appId":Ljava/lang/Integer;
    invoke-static {p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 172
    .local v1, "appIdsListAsInstalled":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 173
    invoke-static {v1, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updatePositionsForApps(Ljava/util/List;Landroid/content/Context;)V

    .line 175
    invoke-static {p2}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendAppsOrderToCloud(Landroid/content/Context;)V

    .line 177
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {v2, p1, v3, p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->markAppStateChanging(IILcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)V

    .line 180
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/managers/CloudAppsManager;->triggerUninstallApp(I)V

    .line 181
    return-void
.end method

.method public static uninstallAppFromWatchFinished(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)V
    .locals 3
    .param p0, "elementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    .line 221
    .local v0, "appId":I
    invoke-static {v0, p1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->deleteAppFromLocker(ILandroid/content/Context;)V

    .line 223
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 224
    return-void
.end method

.method public static updateAppPosition(Ljava/util/List;Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 241
    .local p0, "cloudElementSummaryList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .local v0, "appsIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 244
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 247
    :cond_0
    invoke-static {v0, p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updatePositionsForApps(Ljava/util/List;Landroid/content/Context;)V

    .line 249
    invoke-static {p1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendAppsOrderToCloud(Landroid/content/Context;)V

    .line 251
    invoke-static {v0, p1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendAppsOrder(Ljava/util/List;Landroid/content/Context;)V

    .line 252
    return-void
.end method

.method public static updateAppsDatabase(Ljava/util/List;Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 403
    .local p0, "updatedDefaultApps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    if-nez p0, :cond_1

    .line 404
    sget-object v1, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    const-string v2, "ERROR - trying to update default apps based on a null app list."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 415
    :cond_0
    return-void

    .line 408
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 409
    .local v0, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    sget-object v2, Lcom/vectorwatch/android/managers/CloudAppsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DEFAULTS - Received updated apps - Updating app "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 410
    invoke-static {v0, p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updateApp(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z

    goto :goto_0
.end method
