.class public Lcom/vectorwatch/android/MainActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/WatchModesFragment$OnWatchModesFragmentInteractionListener;
.implements Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;
    }
.end annotation


# static fields
.field public static final EXTRA_TAB_INFO:Ljava/lang/String; = "extra_tab_info"

.field private static final REQUEST_PERMISSIONS_REQUEST_CODE:I = 0x22

.field public static final STATE_NOT_CONNECTED:I = 0x1

.field public static final STATE_NOT_PAIRED:I = 0x0

.field public static final TAB_INDEX_ACTIVITY:I = 0x2

.field public static final TAB_INDEX_SETTINGS:I = 0x3

.field public static final TAB_INDEX_STORE:I = 0x1

.field public static final TAB_INDEX_WATCHMAKER:I = 0x0

.field public static final TAG:Ljava/lang/String; = "MainActivity"

.field private static final log:Lorg/slf4j/Logger;

.field public static sSettingsView:Landroid/view/View;


# instance fields
.field final TAG_ACTIVITY:Ljava/lang/String;

.field final TAG_SETTINGS:Ljava/lang/String;

.field final TAG_STORE:Ljava/lang/String;

.field final TAG_WATCHMAKER:Ljava/lang/String;

.field private activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

.field private alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

.field private currentPage:I

.field mActiveWatchFaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityTab:Landroid/support/design/widget/TabLayout$Tab;

.field private mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

.field private mGoToConnect:Z

.field private mIgnoreBoard:Z

.field private mIsInForegroundMode:Z

.field private mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

.field public mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

.field private mSettingsTab:Landroid/support/design/widget/TabLayout$Tab;

.field private mStoreTab:Landroid/support/design/widget/TabLayout$Tab;

.field private mTabLayout:Landroid/support/design/widget/TabLayout;

.field private mWatchTab:Landroid/support/design/widget/TabLayout$Tab;

.field saveToHistory:Z

.field private settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

.field watchMakerFragment:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 107
    const-string v0, "WATCHMAKER"

    iput-object v0, p0, Lcom/vectorwatch/android/MainActivity;->TAG_WATCHMAKER:Ljava/lang/String;

    .line 108
    const-string v0, "STORE"

    iput-object v0, p0, Lcom/vectorwatch/android/MainActivity;->TAG_STORE:Ljava/lang/String;

    .line 109
    const-string v0, "MESSAGE_TYPE_ACTIVITY"

    iput-object v0, p0, Lcom/vectorwatch/android/MainActivity;->TAG_ACTIVITY:Ljava/lang/String;

    .line 110
    const-string v0, "SETTINGS"

    iput-object v0, p0, Lcom/vectorwatch/android/MainActivity;->TAG_SETTINGS:Ljava/lang/String;

    .line 924
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/MainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/MainActivity;->currentPage:I

    return v0
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/MainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/vectorwatch/android/MainActivity;->currentPage:I

    return p1
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/helper/OrientationManager;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/view/BadgeView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    return-object v0
.end method

.method static synthetic access$300()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    return-object v0
.end method

.method static synthetic access$402(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;
    .param p1, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    return-object p1
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;
    .param p1, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity;->alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    return-object p1
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    return-object v0
.end method

.method static synthetic access$602(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/MainActivity;
    .param p1, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity;->settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    return-object p1
.end method

.method private changeTabsFont(Landroid/support/design/widget/TabLayout;)V
    .locals 10
    .param p1, "tabLayout"    # Landroid/support/design/widget/TabLayout;

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    const-string v9, "fonts/Reader-Regular.otf"

    invoke-static {v8, v9}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    .line 486
    .local v5, "typeface":Landroid/graphics/Typeface;
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/support/design/widget/TabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 487
    .local v6, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    .line 488
    .local v4, "tabsCount":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 489
    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 490
    .local v7, "vgTab":Landroid/view/ViewGroup;
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 491
    .local v2, "tabChildsCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 492
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 493
    .local v3, "tabViewChild":Landroid/view/View;
    instance-of v8, v3, Landroid/widget/TextView;

    if-eqz v8, :cond_0

    move-object v8, v3

    .line 494
    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 495
    check-cast v3, Landroid/widget/TextView;

    .end local v3    # "tabViewChild":Landroid/view/View;
    const/4 v8, 0x2

    const/high16 v9, 0x41400000    # 12.0f

    invoke-virtual {v3, v8, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 491
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 488
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 499
    .end local v0    # "i":I
    .end local v2    # "tabChildsCount":I
    .end local v7    # "vgTab":Landroid/view/ViewGroup;
    :cond_2
    return-void
.end method

.method private checkPermissions()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 653
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 655
    .local v0, "permissionState":I
    sget-object v4, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FITNESS_API: permision state = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 656
    if-nez v0, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v3

    .line 655
    goto :goto_0

    :cond_1
    move v2, v3

    .line 656
    goto :goto_1
.end method

.method private checkPlayServices(Landroid/app/Activity;)Z
    .locals 3
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 977
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    .line 978
    .local v0, "apiAvailability":Lcom/google/android/gms/common/GoogleApiAvailability;
    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    .line 980
    .local v1, "resultCode":I
    if-eqz v1, :cond_1

    .line 981
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isUserResolvableError(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 982
    const/16 v2, 0x2328

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->getErrorDialog(Landroid/app/Activity;II)Landroid/app/Dialog;

    move-result-object v2

    .line 983
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 986
    :cond_0
    const/4 v2, 0x0

    .line 989
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkSyncFlags()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 445
    const-string v2, "flag_sync_system_apps"

    invoke-static {v2, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 447
    .local v0, "syncDefFromCloud":Z
    const-string v2, "flag_sync_defaults_to_cloud"

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 450
    .local v1, "syncDefToCloud":Z
    sget-object v2, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SETUP: sync_to_cloud = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sync_from_cloud = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 451
    const-string v2, "flag_sync_system_apps"

    invoke-static {v2, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "flag_sync_defaults_to_cloud"

    .line 452
    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 453
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "SYNC DEFAULTS - Trigger updates with cloud due to sync defaults."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 455
    invoke-direct {p0}, Lcom/vectorwatch/android/MainActivity;->triggerUpdatesWithCloud()V

    .line 456
    invoke-direct {p0}, Lcom/vectorwatch/android/MainActivity;->goToSetupWatch()V

    .line 457
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->setIsInSetupMode(Z)Lcom/vectorwatch/android/utils/AppInstallManager;

    move v2, v4

    .line 461
    :goto_0
    return v2

    .line 460
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->setIsInSetupMode(Z)Lcom/vectorwatch/android/utils/AppInstallManager;

    move v2, v3

    .line 461
    goto :goto_0
.end method

.method private forcePair(Ljava/lang/String;)V
    .locals 6
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 586
    const-string v4, "bluetooth"

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    .line 588
    .local v1, "bluetoothManager":Landroid/bluetooth/BluetoothManager;
    if-nez v1, :cond_0

    .line 589
    sget-object v4, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "Unable to initialize BluetoothManager."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 592
    :cond_0
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 593
    .local v0, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v0, :cond_1

    .line 594
    sget-object v4, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "Unable to obtain a BluetoothAdapter."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 597
    :cond_1
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    .line 599
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    :try_start_0
    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->createBond(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :goto_0
    return-void

    .line 600
    :catch_0
    move-exception v3

    .line 601
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private goToConnectWatch(I)V
    .locals 2
    .param p1, "option"    # I

    .prologue
    .line 540
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 541
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_IS_PAIRED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 542
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 543
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 544
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 545
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    .line 546
    return-void
.end method

.method private goToLogin()V
    .locals 2

    .prologue
    .line 505
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 506
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 507
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 508
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    .line 509
    return-void
.end method

.method private goToSetupWatch()V
    .locals 2

    .prologue
    .line 515
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/SetupWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 516
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 517
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 518
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    .line 519
    return-void
.end method

.method private goToUpdateOrCompatibilityScreenIfNeeded(Z)Z
    .locals 11
    .param p1, "mainActivityOnResume"    # Z

    .prologue
    const/high16 v10, 0x4000000

    const v9, 0x8000

    const/4 v8, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 610
    sget-object v5, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SETUP: unpaired_from_phone = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "unpaired_from_phone_settings"

    .line 611
    invoke-static {v7, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 610
    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 613
    const-string v5, "unpaired_from_phone_settings"

    .line 614
    invoke-static {v5, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 615
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v1

    .line 616
    .local v1, "ongoingOtaState":I
    if-eq v1, v8, :cond_0

    const/4 v5, 0x6

    if-ne v1, v5, :cond_1

    :cond_0
    const-string v5, "check_compatibility"

    .line 618
    invoke-static {v5, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 619
    sget-object v4, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "TEST - main activity - INACTIVE & CHECK"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 620
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 621
    .local v2, "updateIntent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 622
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "key_info_type"

    sget-object v5, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->CHECK_COMPATIBILITY:Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    .line 623
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->getVal()I

    move-result v5

    .line 622
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 624
    invoke-virtual {v2, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 625
    invoke-virtual {v2, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 626
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 627
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 628
    const-string v4, "force_ota"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 630
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 631
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    .line 646
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "ongoingOtaState":I
    .end local v2    # "updateIntent":Landroid/content/Intent;
    :goto_0
    return v3

    .line 633
    .restart local v1    # "ongoingOtaState":I
    :cond_1
    if-eq v1, v8, :cond_2

    .line 634
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isForceOta(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 635
    sget-object v4, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "TEST - main activity - !INACTIVE & FORCE"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 636
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 637
    .restart local v2    # "updateIntent":Landroid/content/Intent;
    invoke-virtual {v2, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 638
    invoke-virtual {v2, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 639
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 640
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 641
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    goto :goto_0

    .end local v1    # "ongoingOtaState":I
    .end local v2    # "updateIntent":Landroid/content/Intent;
    :cond_2
    move v3, v4

    .line 646
    goto :goto_0
.end method

.method private needsToPairToDevice()Z
    .locals 2

    .prologue
    .line 527
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 528
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    :cond_0
    const/4 v0, 0x1

    .line 531
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requestPermissions()V
    .locals 4

    .prologue
    .line 662
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    .line 663
    invoke-static {p0, v1}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    .line 668
    .local v0, "shouldProvideRationale":Z
    if-eqz v0, :cond_0

    .line 669
    sget-object v1, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "FITNESS_API: Displaying permission rationale to provide additional context."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 670
    const v1, 0x7f100106

    .line 671
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0901e3

    const/4 v3, -0x2

    .line 670
    invoke-static {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    const v2, 0x7f0901d8

    new-instance v3, Lcom/vectorwatch/android/MainActivity$3;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/MainActivity$3;-><init>(Lcom/vectorwatch/android/MainActivity;)V

    .line 674
    invoke-virtual {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    .line 683
    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar;->show()V

    .line 693
    :goto_0
    return-void

    .line 685
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "FITNESS_API: Requesting permission"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 689
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    invoke-static {p0, v1, v2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private triggerConnectToWatchIfNeeded()V
    .locals 5

    .prologue
    .line 433
    const-string v3, "flag_action_forget_requested"

    const/4 v4, 0x0

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    .line 435
    .local v2, "requestedForget":Z
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 436
    .local v1, "pairedDeviceAddress":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    .line 437
    .local v0, "isConnectedToWatch":Z
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 438
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->connect(Landroid/content/Context;)V

    .line 440
    :cond_0
    return-void
.end method

.method private triggerSettingsSyncsIfNeeded()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 466
    const-string v5, "flag_changed_activity_info"

    invoke-static {v5, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 468
    .local v1, "activityInfoChanged":Z
    const-string v5, "flag_changed_activity_profile"

    invoke-static {v5, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 470
    .local v0, "accountProfileChanged":Z
    const-string v5, "flag_changed_contextual"

    invoke-static {v5, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    .line 472
    .local v3, "contextualChanged":Z
    const-string v5, "flag_changed_goals"

    invoke-static {v5, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    .line 474
    .local v4, "goalsChanged":Z
    const-string v5, "flag_changed_alarms"

    invoke-static {v5, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    .line 477
    .local v2, "alarmsChanged":Z
    if-nez v3, :cond_0

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    if-eqz v4, :cond_1

    .line 478
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 480
    :cond_1
    return-void
.end method

.method private triggerUpdatesWithCloud()V
    .locals 3

    .prologue
    .line 552
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 553
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 554
    return-void
.end method


# virtual methods
.method public buildFitnessClient()V
    .locals 3

    .prologue
    .line 755
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FITNESS_API: BUILD CLIENT METHOD Client = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 756
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-nez v0, :cond_0

    .line 757
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Google Fit Client == null -> build client."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 759
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/fitness/Fitness;->HISTORY_API:Lcom/google/android/gms/common/api/Api;

    .line 760
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/api/Scope;

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    .line 761
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addScope(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/api/Scope;

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    .line 762
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addScope(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/api/Scope;

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    .line 763
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addScope(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/MainActivity$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/MainActivity$6;-><init>(Lcom/vectorwatch/android/MainActivity;)V

    .line 764
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lcom/vectorwatch/android/MainActivity$5;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/MainActivity$5;-><init>(Lcom/vectorwatch/android/MainActivity;)V

    .line 790
    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->enableAutoManage(Landroid/support/v4/app/FragmentActivity;ILcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 807
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 809
    :cond_0
    return-void
.end method

.method public handleAppUninstallCommandSentEvent(Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 900
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;->getRemovedAppId()I

    move-result v2

    .line 902
    .local v2, "removedAppId":I
    invoke-static {v2, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v1

    .line 905
    .local v1, "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-nez v1, :cond_0

    .line 906
    sget-object v3, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v4, "App should still be in database for confirming uninstall from watch finished."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 922
    :goto_0
    return-void

    .line 910
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 911
    .local v0, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 912
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->editImg:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setEditImg(Ljava/lang/String;)V

    .line 913
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 914
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 915
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 916
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 917
    iget v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->rating:F

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 918
    iget-object v3, v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 919
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireUserAuth(Z)V

    .line 921
    invoke-static {v0, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->uninstallAppFromWatchFinished(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public handleBleTrulyConnectedReceivedEvent(Lcom/vectorwatch/android/events/BleTrulyConnectedReceivedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BleTrulyConnectedReceivedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 853
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "TEST - TRULY CONNECTED received"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 854
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/MainActivity;->goToUpdateOrCompatibilityScreenIfNeeded(Z)Z

    .line 855
    return-void
.end method

.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)Z
    .locals 8
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 557
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v2

    .line 558
    .local v2, "state":I
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    .line 559
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getDeviceAddress()Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "address":Ljava/lang/String;
    sget-object v4, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received bond state changed event for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " (address = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") - new state is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " (10 = BOND_NONE, 11 = BOND_BONDING, 12 = BOND_BONDED"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 563
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_0

    .line 565
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/MainActivity;->forcePair(Ljava/lang/String;)V

    .line 567
    invoke-static {v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 568
    invoke-static {v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceName(Ljava/lang/String;Landroid/content/Context;)V

    .line 570
    const-string v4, "unpaired_from_phone_settings"

    invoke-static {v4, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 574
    const-string v4, "previous_watch_shape"

    .line 575
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 574
    invoke-static {v4, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 577
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->forgetWatch(Landroid/content/Context;)V

    .line 578
    invoke-direct {p0, v3}, Lcom/vectorwatch/android/MainActivity;->goToConnectWatch(I)V

    .line 579
    const/4 v3, 0x1

    .line 582
    :cond_0
    return v3
.end method

.method public handleGoogleFitClientIsNull(Lcom/vectorwatch/android/events/GoogleFitClientIsNullEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/GoogleFitClientIsNullEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 860
    sget-object v1, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "FITNESS_API: handleGoogleFitClientIsNull event."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 861
    const-string v1, "flag_sync_to_google_fit"

    const/4 v2, 0x0

    .line 862
    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 863
    .local v0, "isSyncToGoogleFitEnabled":Z
    if-eqz v0, :cond_0

    .line 864
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->buildFitnessClient()V

    .line 866
    :cond_0
    return-void
.end method

.method public handleInternetConnectionEnabledEvent(Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 883
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 896
    :goto_0
    return-void

    .line 889
    :cond_0
    const-string v0, "dirty_order_to_cloud"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 891
    invoke-static {p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendAppsOrderToCloud(Landroid/content/Context;)V

    .line 894
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "FILE MANAGER: Connected to internet -> try to upload files."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 895
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/FileManager;->uploadAllFilesToCloud(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public handleSettingsSyncedToWatchEvent(Lcom/vectorwatch/android/events/SettingsSyncedToWatchEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/vectorwatch/android/events/SettingsSyncedToWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 878
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->clearSettingsPreferencesDirtyFields(Landroid/content/Context;)V

    .line 879
    return-void
.end method

.method public handleStoreBadgeCountEvent(Lcom/vectorwatch/android/events/StoreCountBadgeEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/StoreCountBadgeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 814
    sget v0, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    if-lez v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/BadgeView;->show()V

    .line 816
    sget v0, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    invoke-static {p0, v0}, Lme/leolin/shortcutbadger/ShortcutBadger;->applyCount(Landroid/content/Context;I)Z

    .line 818
    :cond_0
    return-void
.end method

.method public handleTabSelectedEvent(Lcom/vectorwatch/android/events/TabSelectedEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/vectorwatch/android/events/TabSelectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 828
    iget-object v3, p0, Lcom/vectorwatch/android/MainActivity;->watchMakerFragment:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    if-eqz v3, :cond_0

    .line 829
    sput-boolean v5, Lcom/vectorwatch/android/VectorApplication;->sShowStreamDropped:Z

    .line 832
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/TabSelectedEvent;->getPrevTabIndex()I

    move-result v3

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/TabSelectedEvent;->getTabIndex()I

    move-result v3

    if-eq v3, v4, :cond_1

    .line 834
    const-string v3, "flag_changed_goals"

    invoke-static {v3, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 837
    .local v1, "goalsChanged":Z
    if-eqz v1, :cond_1

    .line 838
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 842
    .end local v1    # "goalsChanged":Z
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/TabSelectedEvent;->getTabIndex()I

    move-result v3

    if-ne v3, v4, :cond_2

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/TabSelectedEvent;->getPrevTabIndex()I

    move-result v3

    if-eq v3, v4, :cond_2

    .line 844
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getActivitySyncLastTimestamp(Landroid/content/Context;)I

    move-result v2

    .line 845
    .local v2, "startTime":I
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentTime()I

    move-result v0

    .line 846
    .local v0, "endTime":I
    sget-object v3, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SYNC_GET_ACTIVITY: (before request) start = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " end = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 847
    invoke-static {p0, v2, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActivityRequest(Landroid/content/Context;II)Ljava/util/UUID;

    .line 849
    .end local v0    # "endTime":I
    .end local v2    # "startTime":I
    :cond_2
    return-void
.end method

.method public handleTriggerBuildFitnessClientEvent(Lcom/vectorwatch/android/events/TriggerBuildFitnessClientEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/TriggerBuildFitnessClientEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 871
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Triggered build fitness client event."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 872
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->buildFitnessClient()V

    .line 873
    return-void
.end method

.method public handleWatchUpdateCompleted(Lcom/vectorwatch/android/events/WatchUpdateCompletedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchUpdateCompletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 823
    const-string v0, "flag_get_cpu_id"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 824
    return-void
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 606
    iget-boolean v0, p0, Lcom/vectorwatch/android/MainActivity;->mIsInForegroundMode:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 391
    iget v0, p0, Lcom/vectorwatch/android/MainActivity;->currentPage:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/helper/OrientationManager;->disable()V

    .line 396
    :cond_0
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    .line 397
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 24
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 159
    invoke-super/range {p0 .. p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 160
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 164
    const-string v20, "pref_last_triggered_alarm_sync"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-object/from16 v3, p0

    invoke-static {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J

    move-result-wide v10

    .line 166
    .local v10, "lastTriggeredSyncActivity":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 167
    .local v6, "crtTime":J
    sub-long v20, v6, v10

    const-wide/32 v22, 0xa4cb80

    cmp-long v20, v20, v22

    if-lez v20, :cond_0

    .line 168
    sget-object v20, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v21, "SYNC ACTIVITY ALARM: Reset. Probably killed."

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 169
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->setAlarm(Landroid/content/Context;)V

    .line 179
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/MainActivity;->checkPlayServices(Landroid/app/Activity;)Z

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v20

    const-string v21, "ignore_board"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vectorwatch/android/MainActivity;->mIgnoreBoard:Z

    .line 182
    new-instance v20, Lcom/vectorwatch/android/ui/helper/OrientationManager;

    const/16 v21, 0x3

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move/from16 v2, v21

    move-object/from16 v3, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/helper/OrientationManager;-><init>(Landroid/content/Context;ILcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    .line 184
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->triggerConnectToWatchIfNeeded()V

    .line 186
    const-string v20, "flag_show_onboarding"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v14

    .line 189
    .local v14, "showedOnboardingBefore":Z
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v8, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 190
    .local v8, "current":Ljava/util/Locale;
    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v20

    const-string v21, "en"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 191
    if-nez v14, :cond_2

    .line 192
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vectorwatch/android/MainActivity;->mIgnoreBoard:Z

    move/from16 v20, v0

    if-nez v20, :cond_2

    .line 193
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/vectorwatch/android/MainActivity;->overridePendingTransition(II)V

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    .line 195
    new-instance v20, Landroid/content/Intent;

    const-class v21, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 387
    :cond_1
    :goto_0
    return-void

    .line 201
    :cond_2
    const-string v20, "unpaired_from_phone_settings"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 203
    new-instance v20, Lcom/vectorwatch/android/events/BondStateChangedEvent;

    const/16 v21, 0xa

    .line 204
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    .line 205
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v20 .. v23}, Lcom/vectorwatch/android/events/BondStateChangedEvent;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 203
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 215
    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->needsLogin(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 216
    new-instance v20, Lcom/vectorwatch/android/MainActivity$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/MainActivity$1;-><init>(Lcom/vectorwatch/android/MainActivity;)V

    invoke-static/range {v20 .. v20}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->checkCloudStatus(Lretrofit/Callback;)V

    .line 228
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->goToLogin()V

    goto :goto_0

    .line 208
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->needsToPairToDevice()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 209
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/MainActivity;->goToConnectWatch(I)V

    .line 210
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V

    .line 211
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    goto :goto_0

    .line 232
    :cond_5
    sget-object v20, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v21, "SETUP: onCreate - main activity"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 233
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/MainActivity;->goToUpdateOrCompatibilityScreenIfNeeded(Z)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 234
    sget-object v20, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v21, "SETUP: needs check compatibility"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->checkSyncFlags()Z

    move-result v20

    if-nez v20, :cond_1

    .line 242
    const v20, 0x7f030027

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->setContentView(I)V

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246
    const-string v20, "flag_sync_locale"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v18

    .line 248
    .local v18, "syncNeeded":Z
    if-eqz v18, :cond_7

    const-string v20, "flag_sync_system_apps"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v20

    if-nez v20, :cond_7

    const-string v20, "flag_sync_defaults_to_cloud"

    const/16 v21, 0x0

    .line 249
    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v20

    if-nez v20, :cond_7

    .line 250
    sget-object v20, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v21, "LOCALE - sync locale flag is set and there is no need for setup."

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 251
    const-string v20, "pref_locale_watch"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 253
    .local v9, "currentLanguage":Ljava/lang/String;
    if-eqz v9, :cond_9

    .line 254
    move-object/from16 v0, p0

    invoke-static {v9, v0}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    .line 261
    .end local v9    # "currentLanguage":Ljava/lang/String;
    :cond_7
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->triggerSettingsSyncsIfNeeded()V

    .line 263
    const v20, 0x7f100107

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/support/design/widget/TabLayout;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    .line 264
    const v20, 0x7f100109

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mWatchTab:Landroid/support/design/widget/TabLayout$Tab;

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mActivityTab:Landroid/support/design/widget/TabLayout$Tab;

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mStoreTab:Landroid/support/design/widget/TabLayout$Tab;

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mSettingsTab:Landroid/support/design/widget/TabLayout$Tab;

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v20

    const v21, 0x7f0300b3

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    .line 272
    .local v19, "watchTabView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mWatchTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 273
    const v20, 0x7f1002d0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    const v21, 0x7f020160

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 274
    const v20, 0x7f1002d2

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    const v21, 0x7f090250

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v20

    const v21, 0x7f0300b3

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 277
    .local v17, "storeTabView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mStoreTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 278
    const v20, 0x7f1002d0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 279
    .local v15, "storeImage":Landroid/widget/ImageView;
    const v20, 0x7f02015f

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 280
    const v20, 0x7f1002d1

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 281
    .local v16, "storeTabHolder":Landroid/widget/ImageView;
    const v20, 0x7f1002d2

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    const v21, 0x7f090239

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    new-instance v20, Lcom/vectorwatch/android/ui/view/BadgeView;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/view/BadgeView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    move-object/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0f00b2

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/ui/view/BadgeView;->setTextColor(I)V

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/ui/view/BadgeView;->setBadgePosition(I)V

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    move-object/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const/high16 v22, 0x40400000    # 3.0f

    invoke-static/range {v21 .. v22}, Lcom/github/lzyzsd/circleprogress/Utils;->sp2px(Landroid/content/res/Resources;F)F

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/ui/view/BadgeView;->setTextSize(F)V

    .line 288
    sget v20, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    if-lez v20, :cond_8

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/view/BadgeView;->show()V

    .line 290
    sget v20, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, Lme/leolin/shortcutbadger/ShortcutBadger;->applyCount(Landroid/content/Context;I)Z

    .line 293
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v20

    const v21, 0x7f0300b3

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 294
    .local v4, "activityTabView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mActivityTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 295
    const v20, 0x7f1002d0

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    const v21, 0x7f02015c

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 296
    const v20, 0x7f1002d2

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    const v21, 0x7f09024a

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v20

    const v21, 0x7f0300b3

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    .line 299
    .local v13, "settingsTabView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mSettingsTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 300
    const v20, 0x7f1002d0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    const v21, 0x7f02015e

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 301
    const v20, 0x7f1002d2

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    const v21, 0x7f09024c

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mWatchTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mStoreTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mActivityTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mSettingsTab:Landroid/support/design/widget/TabLayout$Tab;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/MainActivity;->changeTabsFont(Landroid/support/design/widget/TabLayout;)V

    .line 310
    new-instance v12, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v12, v0, v1}, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;-><init>(Lcom/vectorwatch/android/MainActivity;Landroid/app/FragmentManager;)V

    .line 311
    .local v12, "pagerAdapter":Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setOffscreenPageLimit(I)V

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v20, v0

    new-instance v21, Landroid/support/design/widget/TabLayout$TabLayoutOnPageChangeListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v22, v0

    invoke-direct/range {v21 .. v22}, Landroid/support/design/widget/TabLayout$TabLayoutOnPageChangeListener;-><init>(Landroid/support/design/widget/TabLayout;)V

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/MainActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    move-object/from16 v20, v0

    new-instance v21, Lcom/vectorwatch/android/MainActivity$2;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/MainActivity$2;-><init>(Lcom/vectorwatch/android/MainActivity;)V

    invoke-virtual/range {v20 .. v21}, Landroid/support/design/widget/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 381
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vectorwatch/android/MainActivity;->currentPage:I

    .line 382
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vectorwatch/android/MainActivity;->saveToHistory:Z

    .line 384
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 386
    .local v5, "context":Landroid/content/Context;
    sget-object v20, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    move-object/from16 v0, v20

    invoke-static {v0, v5}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getAppsWithGivenState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/MainActivity;->mActiveWatchFaces:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 256
    .end local v4    # "activityTabView":Landroid/view/View;
    .end local v5    # "context":Landroid/content/Context;
    .end local v12    # "pagerAdapter":Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;
    .end local v13    # "settingsTabView":Landroid/view/View;
    .end local v15    # "storeImage":Landroid/widget/ImageView;
    .end local v16    # "storeTabHolder":Landroid/widget/ImageView;
    .end local v17    # "storeTabView":Landroid/view/View;
    .end local v19    # "watchTabView":Landroid/view/View;
    .restart local v9    # "currentLanguage":Ljava/lang/String;
    :cond_9
    sget-object v20, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v21, "LOCALE - flag to set locale to watch is set, but the locale is null."

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 406
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 407
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 408
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/helper/OrientationManager;->disable()V

    .line 411
    :cond_0
    return-void
.end method

.method public onFragmentInteraction(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 402
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 156
    return-void
.end method

.method public onOrientationChange(Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;)V
    .locals 3
    .param p1, "screenOrientation"    # Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->isInForeground()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    sget-object v1, Lcom/vectorwatch/android/MainActivity$7;->$SwitchMap$com$vectorwatch$android$ui$helper$OrientationManager$ScreenOrientation:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 419
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 420
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 428
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onPause()V

    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/MainActivity;->mIsInForegroundMode:Z

    .line 430
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "grantResults"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 701
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: onRequestPermissionResult"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 702
    const/16 v0, 0x22

    if-ne p1, v0, :cond_0

    .line 703
    array-length v0, p3

    if-gtz v0, :cond_1

    .line 706
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: User interaction was cancelled."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    const/4 v0, 0x0

    aget v0, p3, v0

    if-eqz v0, :cond_0

    .line 723
    const v0, 0x7f100106

    .line 724
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0901e2

    const/4 v2, -0x2

    .line 723
    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const v1, 0x7f090047

    new-instance v2, Lcom/vectorwatch/android/MainActivity$4;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/MainActivity$4;-><init>(Lcom/vectorwatch/android/MainActivity;)V

    .line 727
    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 741
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 129
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 130
    iput-boolean v4, p0, Lcom/vectorwatch/android/MainActivity;->mIsInForegroundMode:Z

    .line 131
    const-string v0, "unpaired_from_phone_settings"

    invoke-static {v0, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    new-instance v0, Lcom/vectorwatch/android/events/BondStateChangedEvent;

    const/16 v1, 0xa

    .line 134
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 135
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/events/BondStateChangedEvent;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/MainActivity;->handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    :goto_0
    return-void

    .line 138
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/MainActivity;->needsToPairToDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "Does not have watch shape or cpu id."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 140
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/MainActivity;->goToConnectWatch(I)V

    .line 141
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V

    .line 142
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    goto :goto_0

    .line 144
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->needsLogin(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 145
    invoke-direct {p0}, Lcom/vectorwatch/android/MainActivity;->goToLogin()V

    .line 146
    invoke-virtual {p0}, Lcom/vectorwatch/android/MainActivity;->finish()V

    .line 149
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "TEST - onResume - main activity"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/MainActivity;->goToUpdateOrCompatibilityScreenIfNeeded(Z)Z

    goto :goto_0
.end method
