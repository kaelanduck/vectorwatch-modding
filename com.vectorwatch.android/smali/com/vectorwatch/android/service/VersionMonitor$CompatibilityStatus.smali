.class public final enum Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;
.super Ljava/lang/Enum;
.source "VersionMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/VersionMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CompatibilityStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

.field public static final enum COMPATIBLE:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

.field public static final enum COMPATIBLE_UPDATE_FOR_APP:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

.field public static final enum COMPATIBLE_UPDATE_FOR_WACTH:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

.field public static final enum INCOMPATIBLE_OLD_APP_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

.field public static final enum INCOMPATIBLE_OLD_WATCH_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const-string v1, "COMPATIBLE"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    new-instance v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const-string v1, "INCOMPATIBLE_OLD_WATCH_VERSION"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_WATCH_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    new-instance v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const-string v1, "INCOMPATIBLE_OLD_APP_VERSION"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_APP_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    new-instance v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const-string v1, "COMPATIBLE_UPDATE_FOR_WACTH"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE_UPDATE_FOR_WACTH:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    .line 39
    new-instance v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const-string v1, "COMPATIBLE_UPDATE_FOR_APP"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE_UPDATE_FOR_APP:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_WATCH_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_APP_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE_UPDATE_FOR_WACTH:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE_UPDATE_FOR_APP:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->$VALUES:[Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->$VALUES:[Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    return-object v0
.end method
