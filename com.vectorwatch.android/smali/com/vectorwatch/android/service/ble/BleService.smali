.class public Lcom/vectorwatch/android/service/ble/BleService;
.super Landroid/app/Service;
.source "BleService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/BleService$ServiceBinder;
    }
.end annotation


# static fields
.field public static final ACTION_GATT_RSSI:Ljava/lang/String; = "ACTION_GATT_RSSI"

.field public static final ACTION_GATT_SERVICES_DISCOVERED:Ljava/lang/String; = "ACTION_GATT_SERVICES_DISCOVERED"

.field public static final EXTRA_DATA:Ljava/lang/String; = "EXTRA_DATA"

.field public static final NOTIFICATION:Ljava/lang/String; = "service_notification"

.field public static final NOTIFICATION_PAYLOAD:Ljava/lang/String; = "notification_payload"

.field public static final PUSH_UPDATE:Ljava/lang/String; = "PUSH_UPDATE"

.field public static final SERIALIZED_DATA_COMMAND:Ljava/lang/String; = "serialized_command"

.field public static final SERIALIZED_DATA_COMMAND_BUNDLE:Ljava/lang/String; = "serialized_command_bundle"

.field public static final SERVICE_SEND:Ljava/lang/String; = "service_send"

.field public static final SYSTEM_COMMAND:Ljava/lang/String; = "service_command"

.field public static final SYSTEM_COMMAND_CLOSE:I = 0x2

.field public static final SYSTEM_COMMAND_CONNECT:I = 0x0

.field public static final SYSTEM_COMMAND_DISCONNECT:I = 0x1

.field public static final SYSTEM_COMMAND_FORGET_WATCH:I = 0x4

.field public static final SYSTEM_COMMAND_RECONNECT:I = 0x3

.field public static final SYSTEM_COMMAND_STOP:I = 0x5

.field public static final UUID_BLE_SHIELD_DATA_RX:Ljava/util/UUID;

.field public static final UUID_BLE_SHIELD_DATA_SERVICE:Ljava/util/UUID;

.field public static final UUID_BLE_SHIELD_DATA_TX:Ljava/util/UUID;

.field public static final UUID_BLE_SHIELD_RX:Ljava/util/UUID;

.field public static final UUID_BLE_SHIELD_SERVICE:Ljava/util/UUID;

.field public static final UUID_BLE_SHIELD_TX:Ljava/util/UUID;

.field public static final UUID_BLE_SHIELD_TX_NOT_INFO:Ljava/util/UUID;

.field public static calendarEventsForToday:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;"
        }
    .end annotation
.end field

.field protected static isConnectedToWatch:Z

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private final fServiceBinder:Landroid/os/IBinder;

.field private mWorkerThread:Landroid/os/HandlerThread;

.field private mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

.field private startSticky:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_TX_NOT_INFO:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_TX_NOT_INFO:Ljava/util/UUID;

    .line 38
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_TX:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_TX:Ljava/util/UUID;

    .line 39
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_RX:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_RX:Ljava/util/UUID;

    .line 40
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_RX:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_RX:Ljava/util/UUID;

    .line 41
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_TX:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_TX:Ljava/util/UUID;

    .line 42
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_SERVICE:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_SERVICE:Ljava/util/UUID;

    .line 43
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_SERVICE:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_SERVICE:Ljava/util/UUID;

    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->calendarEventsForToday:Ljava/util/Map;

    .line 67
    const-class v0, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    .line 71
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/service/ble/BleService;->isConnectedToWatch:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 75
    new-instance v0, Lcom/vectorwatch/android/service/ble/BleService$ServiceBinder;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/service/ble/BleService$ServiceBinder;-><init>(Lcom/vectorwatch/android/service/ble/BleService;)V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleService;->fServiceBinder:Landroid/os/IBinder;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThread:Landroid/os/HandlerThread;

    .line 234
    return-void
.end method

.method public static getIsConnectedToWatch()Z
    .locals 1

    .prologue
    .line 85
    sget-boolean v0, Lcom/vectorwatch/android/service/ble/BleService;->isConnectedToWatch:Z

    return v0
.end method

.method private processServiceCommand(I)V
    .locals 4
    .param p1, "commandType"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 178
    packed-switch p1, :pswitch_data_0

    .line 199
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 200
    invoke-static {v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 199
    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 201
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 202
    return-void

    .line 180
    .end local v0    # "msg":Landroid/os/Message;
    :pswitch_0
    iput-boolean v2, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    goto :goto_0

    .line 183
    :pswitch_1
    iput-boolean v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    goto :goto_0

    .line 186
    :pswitch_2
    iput-boolean v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    goto :goto_0

    .line 189
    :pswitch_3
    iput-boolean v2, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    goto :goto_0

    .line 192
    :pswitch_4
    iput-boolean v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleService;->fServiceBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 90
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "BluetoothWorkerThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThread:Landroid/os/HandlerThread;

    .line 91
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 92
    new-instance v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    .line 94
    const-string v1, "counter_service_killed"

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 97
    .local v0, "countKilled":I
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Vector Service has been created. Previous kills = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 98
    const-string v1, "counter_service_killed"

    add-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 100
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 211
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 212
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    const-string v2, "onDestroy for Service has been called."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isHandlerThreadTerminating()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    .line 215
    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_HANDLER_THREAD_STOPPING:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 217
    invoke-static {v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v2

    .line 216
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 219
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 221
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 222
    invoke-static {v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v2

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 221
    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 224
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    const-string v2, "Stop self"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 229
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x2

    .line 112
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    .line 113
    iput-boolean v10, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    .line 167
    :cond_0
    :goto_0
    return v6

    .line 118
    :cond_1
    if-nez p1, :cond_2

    .line 120
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    sget-object v9, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v9

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v8, v9, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 121
    .local v4, "msg":Landroid/os/Message;
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v8, v4}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 123
    iget-boolean v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    if-eqz v8, :cond_0

    move v6, v7

    .line 125
    goto :goto_0

    .line 131
    .end local v4    # "msg":Landroid/os/Message;
    :cond_2
    const-string v8, "serialized_command_bundle"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 132
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "serialized_command_bundle"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 133
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v8, "serialized_command"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 135
    .local v1, "command":Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    sget-object v9, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_QUEUE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 136
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v9

    .line 135
    invoke-static {v8, v9, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 137
    .restart local v4    # "msg":Landroid/os/Message;
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v8, v4}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 163
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "command":Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    .end local v4    # "msg":Landroid/os/Message;
    :cond_3
    :goto_1
    iget-boolean v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->startSticky:Z

    if-eqz v8, :cond_0

    move v6, v7

    .line 165
    goto :goto_0

    .line 138
    :cond_4
    const-string v8, "service_command"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 140
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "service_command"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 141
    .local v2, "commandType":I
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/ble/BleService;->processServiceCommand(I)V

    goto :goto_1

    .line 142
    .end local v2    # "commandType":I
    :cond_5
    const-string v8, "notification_payload"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 143
    sget-object v8, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    const-string v9, "onStartCommand - we have notification"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "notification_payload"

    .line 145
    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .line 146
    .local v5, "notificationInfo":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    if-eqz v5, :cond_6

    .line 147
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/BleService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->setContext(Landroid/content/Context;)V

    .line 148
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    sget-object v9, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_NOTIFICATION:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 149
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v9

    .line 148
    invoke-static {v8, v9, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 151
    .restart local v4    # "msg":Landroid/os/Message;
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/BleService;->mWorkerThreadHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v8, v4}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 152
    sget-object v8, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    const-string v9, "BLEService: Notification sent to Bluetooth"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 154
    .end local v4    # "msg":Landroid/os/Message;
    :cond_6
    sget-object v8, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    const-string v9, "BLEService: Notification info is null"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 156
    .end local v5    # "notificationInfo":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :cond_7
    const-string v8, "PUSH_UPDATE"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 157
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "PUSH_UPDATE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 158
    .local v3, "data":Ljava/lang/String;
    sget-object v8, Lcom/vectorwatch/android/service/ble/BleService;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onStartCommand - we have push notification: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/BleService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->pushNotification(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1
.end method
