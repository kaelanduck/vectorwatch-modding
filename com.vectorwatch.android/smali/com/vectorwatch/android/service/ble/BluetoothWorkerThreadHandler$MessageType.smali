.class public final enum Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
.super Ljava/lang/Enum;
.source "BluetoothWorkerThreadHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_BLE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_BLE_CONNECTION_STATE_CHANGED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_BLE_PACKET_FOR_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_FORGET_WATCH:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_HANDLER_THREAD_STOPPING:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_NOTIFICATION:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_NOTIFICATION_DETAILS_REQUEST:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_PACKET_SENT_TO_BLE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_QUEUE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_READ_RSSI:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_RECEIVE_MESSAGE_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_RECEIVE_RAW_DATA_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_RESPONSE_APP_INSTALL_RECEIVED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum MESSAGE_TYPE_TIMEOUT:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

.field public static final enum UNDEFINED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 662
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_DATA"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 663
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_NOTIFICATION"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_NOTIFICATION:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 664
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_NOTIFICATION_DETAILS_REQUEST"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_NOTIFICATION_DETAILS_REQUEST:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 665
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_RECEIVE_MESSAGE_FROM_DEVICE"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RECEIVE_MESSAGE_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 666
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_RECEIVE_RAW_DATA_FROM_DEVICE"

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RECEIVE_RAW_DATA_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 667
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_READ_RSSI"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_READ_RSSI:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 668
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_BLE_CONNECTION_STATE_CHANGED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_CONNECTION_STATE_CHANGED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 669
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_BLE_PACKET_FOR_DEVICE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_PACKET_FOR_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 670
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_BLE_COMMAND"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 671
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_FORGET_WATCH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_FORGET_WATCH:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 672
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_HANDLER_THREAD_STOPPING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_HANDLER_THREAD_STOPPING:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 673
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_QUEUE_COMMAND"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_QUEUE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 674
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_PACKET_SENT_TO_BLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_PACKET_SENT_TO_BLE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 675
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_TIMEOUT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_TIMEOUT:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 676
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "MESSAGE_TYPE_RESPONSE_APP_INSTALL_RECEIVED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RESPONSE_APP_INSTALL_RECEIVED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 677
    new-instance v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    const-string v1, "UNDEFINED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->UNDEFINED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 661
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_NOTIFICATION:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_NOTIFICATION_DETAILS_REQUEST:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RECEIVE_MESSAGE_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RECEIVE_RAW_DATA_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_READ_RSSI:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_CONNECTION_STATE_CHANGED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_PACKET_FOR_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_FORGET_WATCH:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_HANDLER_THREAD_STOPPING:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_QUEUE_COMMAND:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_PACKET_SENT_TO_BLE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_TIMEOUT:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RESPONSE_APP_INSTALL_RECEIVED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->UNDEFINED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->$VALUES:[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 661
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I
    .locals 4
    .param p0, "type"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .prologue
    .line 680
    invoke-static {}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->values()[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 681
    .local v0, "mt":Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    if-ne p0, v0, :cond_0

    .line 682
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->ordinal()I

    move-result v1

    .line 686
    .end local v0    # "mt":Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    :goto_1
    return v1

    .line 680
    .restart local v0    # "mt":Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 686
    .end local v0    # "mt":Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static getType(I)Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 690
    invoke-static {}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->values()[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    move-result-object v0

    array-length v0, v0

    if-ge p0, v0, :cond_0

    if-gez p0, :cond_1

    .line 691
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->UNDEFINED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    .line 694
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->values()[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 661
    const-class v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .locals 1

    .prologue
    .line 661
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->$VALUES:[Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    return-object v0
.end method
