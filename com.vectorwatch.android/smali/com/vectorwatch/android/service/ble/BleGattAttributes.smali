.class public Lcom/vectorwatch/android/service/ble/BleGattAttributes;
.super Ljava/lang/Object;
.source "BleGattAttributes.java"


# static fields
.field public static BLE_SHIELD_DATA_RX:Ljava/lang/String;

.field public static BLE_SHIELD_DATA_SERVICE:Ljava/lang/String;

.field public static BLE_SHIELD_DATA_TX:Ljava/lang/String;

.field public static BLE_SHIELD_RX:Ljava/lang/String;

.field public static BLE_SHIELD_SERVICE:Ljava/lang/String;

.field public static BLE_SHIELD_TX:Ljava/lang/String;

.field public static BLE_SHIELD_TX_NOT_INFO:Ljava/lang/String;

.field public static CLIENT_CHARACTERISTIC_CONFIG:Ljava/lang/String;

.field private static attributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    .line 12
    const-string v0, "81A50000-9EBD-0436-3358-BB370C7DA4C5"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_SERVICE:Ljava/lang/String;

    .line 13
    const-string v0, "00002902-0000-1000-8000-00805f9b34fb"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->CLIENT_CHARACTERISTIC_CONFIG:Ljava/lang/String;

    .line 14
    const-string v0, "81A50002-9EBD-0436-3358-BB370C7DA4C5"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_TX:Ljava/lang/String;

    .line 15
    const-string v0, "81A50003-9EBD-0436-3358-BB370C7DA4C5"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_TX_NOT_INFO:Ljava/lang/String;

    .line 16
    const-string v0, "81A50001-9EBD-0436-3358-BB370C7DA4C5"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_RX:Ljava/lang/String;

    .line 17
    const-string v0, "9e3b0000-d7ab-adf3-f683-baa2a0e81612"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_SERVICE:Ljava/lang/String;

    .line 18
    const-string v0, "9e3b0001-d7ab-adf3-f683-baa2a0e81612"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_TX:Ljava/lang/String;

    .line 19
    const-string v0, "9e3b0002-d7ab-adf3-f683-baa2a0e81612"

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_RX:Ljava/lang/String;

    .line 23
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_SERVICE:Ljava/lang/String;

    const-string v2, "BLE Shield Service"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_SERVICE:Ljava/lang/String;

    const-string v2, "BLE Shield Data Service"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_TX:Ljava/lang/String;

    const-string v2, "BLE Shield TX"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_RX:Ljava/lang/String;

    const-string v2, "BLE Shield RX"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_RX:Ljava/lang/String;

    const-string v2, "BLE Shield RX Data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_DATA_TX:Ljava/lang/String;

    const-string v2, "BLE Shield TX Data "

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->BLE_SHIELD_TX_NOT_INFO:Ljava/lang/String;

    const-string v2, "BLE Shield TX for notifications"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lookup(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 36
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->attributes:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 37
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    .end local p1    # "defaultName":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "defaultName":Ljava/lang/String;
    :cond_0
    move-object p1, v0

    goto :goto_0
.end method
