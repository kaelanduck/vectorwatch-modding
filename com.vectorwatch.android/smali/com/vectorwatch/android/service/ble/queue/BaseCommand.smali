.class public abstract Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.super Ljava/lang/Object;
.source "BaseCommand.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private cmdBase:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field private priority:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

.field private timestamp:J

.field private uuid:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->priority:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 36
    return-void
.end method

.method protected constructor <init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V
    .locals 1
    .param p1, "base"    # Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    .param p2, "priority"    # Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;
    .param p3, "timestamp"    # J

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->priority:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->cmdBase:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 40
    iput-object p2, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->priority:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 41
    iput-wide p3, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->timestamp:J

    .line 42
    return-void
.end method


# virtual methods
.method protected getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->cmdBase:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    return-object v0
.end method

.method public getPriority()Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->priority:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->timestamp:J

    return-wide v0
.end method

.method public abstract getTransferMessages()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation
.end method

.method public getUuid()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->uuid:Ljava/util/UUID;

    return-object v0
.end method

.method protected setCommandBase(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;)V
    .locals 0
    .param p1, "base"    # Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->cmdBase:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 74
    return-void
.end method

.method public setPriority(Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;)V
    .locals 0
    .param p1, "priority"    # Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->priority:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 70
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->timestamp:J

    .line 54
    return-void
.end method

.method protected setUuid(Ljava/util/UUID;)V
    .locals 0
    .param p1, "uuid"    # Ljava/util/UUID;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->uuid:Ljava/util/UUID;

    .line 62
    return-void
.end method
