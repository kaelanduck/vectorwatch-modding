.class public abstract Lcom/vectorwatch/android/service/ble/messages/BtMessage;
.super Ljava/lang/Object;
.source "BtMessage.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private callback:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->callback:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public getCallback()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->callback:Ljava/lang/Runnable;

    return-object v0
.end method

.method public abstract getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
.end method

.method public abstract serialize()[B
.end method

.method public setCallback(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->callback:Ljava/lang/Runnable;

    .line 26
    return-void
.end method
