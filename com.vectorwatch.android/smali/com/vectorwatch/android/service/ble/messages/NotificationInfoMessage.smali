.class public Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
.super Lcom/vectorwatch/android/service/ble/messages/BtMessage;
.source "NotificationInfoMessage.java"


# static fields
.field public static final MISSED_CALL:S = 0x6s

.field public static final PHONE_CALL:S = 0x0s

.field public static final REMOVE_MISSED_CALL:S = 0x7s

.field public static final REMOVE_NOTIFICATION:S = 0x9s

.field public static final REMOVE_PHONE_CALL:S = 0x1s

.field public static final REMOVE_SMS:S = 0x3s

.field public static final REMOVE_SOCIAL:S = 0x5s

.field public static final SMS:S = 0x2s

.field public static final SOCIAL:S = 0x4s

.field public static callId:[B = null

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private additionalInfo:Ljava/lang/String;

.field private btId:[B

.field private containerId:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private packageName:Ljava/lang/String;

.field private statusBarNotificationId:I

.field private stbnKey:Ljava/lang/String;

.field private stbnTag:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private type:S

.field private uuid:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->callId:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "containerId"    # Ljava/lang/String;
    .param p2, "type"    # S
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "additionalInfo"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;
    .param p6, "statusBarNotificationId"    # I
    .param p7, "stbnTag"    # Ljava/lang/String;
    .param p8, "stbnKey"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->containerId:Ljava/lang/String;

    .line 71
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->uuid:Ljava/util/UUID;

    .line 72
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    .line 75
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->containerId:Ljava/lang/String;

    .line 76
    iput-short p2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->type:S

    .line 77
    iput-object p3, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->title:Ljava/lang/String;

    .line 78
    iput-object p4, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->additionalInfo:Ljava/lang/String;

    .line 79
    iput-object p5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->packageName:Ljava/lang/String;

    .line 80
    iput p6, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->statusBarNotificationId:I

    .line 82
    iput-object p8, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->stbnKey:Ljava/lang/String;

    .line 83
    iput-object p7, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->stbnTag:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public constructor <init>(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "additionalInfo"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "statusBarNotificationId"    # I
    .param p6, "stbnTag"    # Ljava/lang/String;
    .param p7, "stbnKey"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->containerId:Ljava/lang/String;

    .line 49
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->uuid:Ljava/util/UUID;

    .line 51
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->type:S

    .line 52
    iput-object p2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->title:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->additionalInfo:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->packageName:Ljava/lang/String;

    .line 55
    iput p5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->statusBarNotificationId:I

    .line 56
    iput-object p7, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->stbnKey:Ljava/lang/String;

    .line 57
    iput-object p6, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->stbnTag:Ljava/lang/String;

    .line 58
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    .line 59
    return-void
.end method

.method public static isCall([B)Z
    .locals 5
    .param p0, "id"    # [B

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 94
    if-eqz p0, :cond_0

    array-length v2, p0

    if-ne v2, v4, :cond_0

    .line 95
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_2

    .line 96
    aget-byte v2, p0, v0

    sget-object v3, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->callId:[B

    aget-byte v3, v3, v0

    if-eq v2, v3, :cond_1

    .line 102
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 95
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getAdditionalInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->additionalInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getBtByteId()[B
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    return-object v0
.end method

.method public getBtIntId()I
    .locals 3

    .prologue
    .line 111
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 112
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    return v1
.end method

.method public getContainerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->containerId:Ljava/lang/String;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDataForTransfer()[B
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 136
    const/16 v1, 0x8

    new-array v0, v1, [B

    .line 138
    .local v0, "protocol":[B
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->type:S

    packed-switch v1, :pswitch_data_0

    .line 209
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    aget-byte v1, v1, v2

    aput-byte v1, v0, v6

    .line 210
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    aget-byte v2, v2, v4

    aput-byte v2, v0, v1

    .line 211
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    aget-byte v2, v2, v3

    aput-byte v2, v0, v1

    .line 212
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    aget-byte v2, v2, v5

    aput-byte v2, v0, v1

    .line 214
    return-object v0

    .line 140
    :pswitch_1
    aput-byte v2, v0, v2

    .line 141
    aput-byte v2, v0, v4

    .line 142
    aput-byte v4, v0, v3

    .line 143
    aput-byte v4, v0, v5

    goto :goto_0

    .line 148
    :pswitch_2
    aput-byte v3, v0, v2

    .line 149
    aput-byte v2, v0, v4

    .line 150
    aput-byte v4, v0, v3

    .line 151
    aput-byte v2, v0, v5

    goto :goto_0

    .line 155
    :pswitch_3
    aput-byte v2, v0, v2

    .line 156
    aput-byte v2, v0, v4

    .line 157
    aput-byte v6, v0, v3

    .line 158
    aput-byte v4, v0, v5

    goto :goto_0

    .line 163
    :pswitch_4
    aput-byte v3, v0, v2

    .line 164
    aput-byte v2, v0, v4

    .line 165
    aput-byte v6, v0, v3

    .line 166
    aput-byte v2, v0, v5

    goto :goto_0

    .line 170
    :pswitch_5
    aput-byte v2, v0, v2

    .line 171
    aput-byte v2, v0, v4

    .line 172
    aput-byte v6, v0, v3

    .line 173
    aput-byte v4, v0, v5

    goto :goto_0

    .line 178
    :pswitch_6
    aput-byte v3, v0, v2

    .line 179
    aput-byte v2, v0, v4

    .line 180
    aput-byte v6, v0, v3

    .line 181
    aput-byte v2, v0, v5

    goto :goto_0

    .line 185
    :pswitch_7
    aput-byte v2, v0, v2

    .line 186
    aput-byte v2, v0, v4

    .line 187
    aput-byte v3, v0, v3

    .line 188
    aput-byte v4, v0, v5

    goto :goto_0

    .line 193
    :pswitch_8
    aput-byte v3, v0, v2

    .line 194
    aput-byte v2, v0, v4

    .line 195
    aput-byte v3, v0, v3

    .line 196
    aput-byte v2, v0, v5

    goto :goto_0

    .line 201
    :pswitch_9
    aput-byte v3, v0, v2

    .line 202
    aput-byte v2, v0, v4

    .line 203
    aput-byte v6, v0, v3

    .line 204
    aput-byte v2, v0, v5

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusBarNotificationId()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->statusBarNotificationId:I

    return v0
.end method

.method public getStbnKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->stbnKey:Ljava/lang/String;

    return-object v0
.end method

.method public getStbnTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->stbnTag:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()S
    .locals 1

    .prologue
    .line 116
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->type:S

    return v0
.end method

.method public getUuid()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->uuid:Ljava/util/UUID;

    return-object v0
.end method

.method public serialize()[B
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getDataForTransfer()[B

    move-result-object v0

    return-object v0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->context:Landroid/content/Context;

    .line 235
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->title:Ljava/lang/String;

    .line 223
    return-void
.end method

.method public setType(S)V
    .locals 0
    .param p1, "type"    # S

    .prologue
    .line 120
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->type:S

    .line 121
    return-void
.end method

.method public setUniqueId(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 132
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->btId:[B

    .line 133
    return-void
.end method
