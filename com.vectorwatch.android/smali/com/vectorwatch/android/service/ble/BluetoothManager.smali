.class public Lcom/vectorwatch/android/service/ble/BluetoothManager;
.super Ljava/lang/Object;
.source "BluetoothManager.java"


# static fields
.field public static final UNKNOWN_PACKET_INDEX:I = -0x1

.field public static final UNKNOWN_TOTAL_PACKETS_COUNT:I = -0x1

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/vectorwatch/android/service/ble/BluetoothManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static close(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: close"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 141
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 142
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "service_command"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 144
    return-void
.end method

.method public static connect(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: connect"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 102
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 103
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "service_command"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 104
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 105
    return-void
.end method

.method public static deleteStreamsCoommand(Landroid/content/Context;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mAppid"    # I

    .prologue
    .line 694
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: app order"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 696
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/DeleteStreamsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/DeleteStreamsCommand;-><init>()V

    .line 699
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/DeleteStreamsCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/DeleteStreamsCommand;->setAppId(I)V

    .line 703
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 714
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/DeleteStreamsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 704
    :catch_0
    move-exception v1

    .line 705
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 706
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send apps order - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 707
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 708
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 709
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send apps order - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 710
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 711
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send apps order - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static disconnect(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: disconnect"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 115
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "service_command"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 118
    return-void
.end method

.method public static forgetWatch(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 165
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: forget watch"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 167
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "service_command"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 168
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 169
    return-void
.end method

.method public static getAppOrder(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 538
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: battery req"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 540
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetAppsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetAppsCommand;-><init>()V

    .line 546
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/GetAppsCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 557
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetAppsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 547
    :catch_0
    move-exception v1

    .line 548
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 549
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send battery req - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 550
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 551
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 552
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send battery req - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 553
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 554
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send battery req - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static reconnect(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: reconnect"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 128
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "service_command"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 130
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 131
    return-void
.end method

.method public static sendActiveApp(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "option"    # Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;
    .param p2, "optionDetails"    # I

    .prologue
    .line 863
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: active app"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 865
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActiveAppCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActiveAppCommand;-><init>()V

    .line 868
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncActiveAppCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActiveAppCommand;->setOption(Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;)V

    .line 869
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActiveAppCommand;->setOptionDetails(I)V

    .line 873
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 884
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActiveAppCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 874
    :catch_0
    move-exception v1

    .line 875
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 876
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send active app - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 877
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 878
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 879
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send active app - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 880
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 881
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send active app - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendActivityOptions(Landroid/content/Context;ZZZ)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "goalAchievement"    # Z
    .param p2, "goalAlmost"    # Z
    .param p3, "activityReminder"    # Z

    .prologue
    .line 1359
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BLE CALLS: activityOptions: goalAchievement - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " goalAlmost: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " activityReminder: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1361
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;-><init>()V

    .line 1364
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->setGoalAchievementAlert(Z)V

    .line 1365
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->setGoalAlmostAlert(Z)V

    .line 1366
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->setActivityReminder(Z)V

    .line 1370
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1381
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1371
    :catch_0
    move-exception v1

    .line 1372
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1373
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send activity options - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1374
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1375
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1376
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send activity options - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1377
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1378
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send activity options - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendActivityRequest(Landroid/content/Context;II)Ljava/util/UUID;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startTime"    # I
    .param p2, "endTime"    # I

    .prologue
    .line 819
    sget-object v5, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v6, "BLE CALLS: activity request"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 821
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v5

    const-string v6, "last_known_system_info"

    const-class v7, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 823
    .local v3, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-nez v3, :cond_0

    .line 824
    sget-object v5, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v6, "Send activity request: - System info missing"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 825
    const/4 v5, 0x0

    .line 850
    :goto_0
    return-object v5

    .line 828
    :cond_0
    invoke-virtual {v3}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v5

    mul-int/lit8 v5, v5, 0x64

    invoke-virtual {v3}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    add-int/2addr v5, v6

    .line 829
    invoke-virtual {v3}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v6

    add-int v0, v5, v6

    .line 830
    .local v0, "build":I
    const/16 v5, 0x4d

    if-le v0, v5, :cond_1

    const/4 v4, 0x1

    .line 831
    .local v4, "isVosBuildOver077":Z
    :goto_1
    new-instance v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;

    invoke-direct {v1, v4}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;-><init>(Z)V

    .line 834
    .local v1, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;
    invoke-virtual {v1, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->setStartTime(I)V

    .line 835
    invoke-virtual {v1, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->setEndTime(I)V

    .line 839
    :try_start_0
    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 850
    :goto_2
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->getUuid()Ljava/util/UUID;

    move-result-object v5

    goto :goto_0

    .line 830
    .end local v1    # "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;
    .end local v4    # "isVosBuildOver077":Z
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 840
    .restart local v1    # "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;
    .restart local v4    # "isVosBuildOver077":Z
    :catch_0
    move-exception v2

    .line 841
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 842
    sget-object v5, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Send activity req - IOException - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2

    .line 843
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 844
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 845
    sget-object v5, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Send activity req - IllegalAccessException -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2

    .line 846
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 847
    .local v2, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Send activity req - random error "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static sendAlarms(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;)",
            "Ljava/util/UUID;"
        }
    .end annotation

    .prologue
    .line 787
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: alarms"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 789
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlarmsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlarmsCommand;-><init>()V

    .line 792
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlarmsCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlarmsCommand;->setAlarmList(Ljava/util/List;)V

    .line 796
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 807
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlarmsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 797
    :catch_0
    move-exception v1

    .line 798
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 799
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send alarms - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 800
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 801
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 802
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send alarms - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 803
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 804
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send alarms - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packedAlert"    # [B

    .prologue
    .line 756
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: alert"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 759
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlertCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlertCommand;-><init>()V

    .line 762
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlertCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlertCommand;->setPackedAlert([B)V

    .line 766
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 777
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAlertCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 767
    :catch_0
    move-exception v1

    .line 768
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 769
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send alert - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 770
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 771
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 772
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send alert - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 773
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 774
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send alert - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendAppInstall(Landroid/content/Context;Lcom/vectorwatch/android/models/InstallMessageModel;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "model"    # Lcom/vectorwatch/android/models/InstallMessageModel;

    .prologue
    .line 725
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: app install"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 727
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;-><init>()V

    .line 730
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->setInstallMessageModel(Lcom/vectorwatch/android/models/InstallMessageModel;)V

    .line 734
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 745
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 735
    :catch_0
    move-exception v1

    .line 736
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 737
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send app install - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 738
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 739
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 740
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send app install - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 741
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 742
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send app install - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendAppUninstall(Landroid/content/Context;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I

    .prologue
    .line 631
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: app install"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 633
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;-><init>()V

    .line 636
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->setRemovedAppId(I)V

    .line 637
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_REMOVE:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->setInstallType(Lcom/vectorwatch/android/utils/Constants$MessageInstallType;)V

    .line 641
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 652
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 642
    :catch_0
    move-exception v1

    .line 643
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 644
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send app uninstall - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 645
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 646
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 647
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send app uninstall - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 648
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 649
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send app uninstall - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendAppsOrder(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/UUID;"
        }
    .end annotation

    .prologue
    .line 663
    .local p1, "appIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: app order"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 665
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppOrderCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppOrderCommand;-><init>()V

    .line 668
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppOrderCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppOrderCommand;->setAppIdsInOrder(Ljava/util/List;)V

    .line 672
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 683
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppOrderCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 673
    :catch_0
    move-exception v1

    .line 674
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 675
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send apps order - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 676
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 677
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 678
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send apps order - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 679
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 680
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send apps order - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendBacklightIntensity(Landroid/content/Context;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intensity"    # I

    .prologue
    .line 568
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: backlight intensity"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 570
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightIntensityCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightIntensityCommand;-><init>()V

    .line 573
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightIntensityCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightIntensityCommand;->setIntensity(I)V

    .line 577
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 588
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightIntensityCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 578
    :catch_0
    move-exception v1

    .line 579
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 580
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send backlight intensity - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 581
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 582
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 583
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send backlight intensity - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 584
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 585
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send backlight intensity - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendBacklightTimeout(Landroid/content/Context;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeoutSeconds"    # I

    .prologue
    .line 599
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: backlight timeout"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 601
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;-><init>()V

    .line 604
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->setTimeout(I)V

    .line 605
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_TIMEOUT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->setSettingsType(Lcom/vectorwatch/android/utils/Constants$SettingsType;)V

    .line 609
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 620
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 610
    :catch_0
    move-exception v1

    .line 611
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 612
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send backlight timeout - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 613
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 614
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 615
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send backlight timeout - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 616
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 617
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send backlight timeout - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendBatteryRequest(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 509
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: battery req"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 511
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isSystemInfoWith4Decimals()Z

    move-result v2

    invoke-direct {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;-><init>(Z)V

    .line 517
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 528
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 518
    :catch_0
    move-exception v1

    .line 519
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 520
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send battery req - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 521
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 522
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 523
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send battery req - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 524
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 525
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send battery req - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendBootloaderUpdate(Landroid/content/Context;Ljava/lang/String;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pathToFile"    # Ljava/lang/String;

    .prologue
    .line 955
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: boot update"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 957
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;-><init>()V

    .line 960
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->setPathToFile(Ljava/lang/String;)V

    .line 964
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 975
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 965
    :catch_0
    move-exception v1

    .line 966
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 967
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send bootloader update - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 968
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 969
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 970
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send bootloader update - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 971
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 972
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send bootloader update - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendBootloaderUpdateStart(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 924
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: boot update start"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 926
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/StartBootloaderUpdateCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/StartBootloaderUpdateCommand;-><init>()V

    .line 933
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/StartBootloaderUpdateCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 944
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/StartBootloaderUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 934
    :catch_0
    move-exception v1

    .line 935
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 936
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send bootloader update start - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 937
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 938
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 939
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send bootloader update start - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 940
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 941
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send bootloader update start - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;)",
            "Ljava/util/UUID;"
        }
    .end annotation

    .prologue
    .line 1287
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: send calendar"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1289
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;-><init>()V

    .line 1292
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->setEvents(Ljava/util/List;)V

    .line 1296
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1307
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1297
    :catch_0
    move-exception v1

    .line 1298
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1299
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send calendar event - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1300
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1301
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1302
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send calendar event - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1303
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1304
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send calendar event - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendChangeActiveAppRequest(Landroid/content/Context;IIIZZ)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "animationType"    # I
    .param p4, "forced"    # Z
    .param p5, "vibration"    # Z

    .prologue
    .line 1207
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: change active app"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1209
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;-><init>()V

    .line 1212
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setAnimationType(I)V

    .line 1213
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setAppId(I)V

    .line 1214
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setForced(Z)V

    .line 1215
    invoke-virtual {v0, p5}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setVibration(Z)V

    .line 1216
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setWatchFaceId(I)V

    .line 1220
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1231
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1221
    :catch_0
    move-exception v1

    .line 1222
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1223
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send change active face - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1224
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1225
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1226
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send change active face - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1227
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1228
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send change active face - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cmd"    # Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->setTimestamp(J)V

    .line 86
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "serialized_command"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 89
    const-string v2, "serialized_command_bundle"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 92
    return-void
.end method

.method public static sendDisplaySecondHand(Landroid/content/Context;Z)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isEnabled"    # Z

    .prologue
    .line 477
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: display second hand"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 479
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;-><init>()V

    .line 482
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_SHOW_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->setSettingsType(Lcom/vectorwatch/android/utils/Constants$SettingsType;)V

    .line 483
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->setSettingsCount(B)V

    .line 484
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->setIsEnabled(Z)V

    .line 488
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 499
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 489
    :catch_0
    move-exception v1

    .line 490
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 491
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send display second hand - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 492
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 493
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 494
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send display second hand - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 495
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 496
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send display second hand - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendEraseWatchCrashLogs(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1171
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: erase crash logs"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1173
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;-><init>()V

    .line 1180
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1191
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1181
    :catch_0
    move-exception v1

    .line 1182
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1183
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send erase watch logs - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1184
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1185
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1186
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send erase watch logs - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1187
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1188
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send erase watch logs - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendGoals(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/GoalModel;",
            ">;)",
            "Ljava/util/UUID;"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "currentGoals":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/GoalModel;>;"
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: sync goals"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 448
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;-><init>()V

    .line 451
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->setGoals(Ljava/util/List;)V

    .line 455
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 466
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 456
    :catch_0
    move-exception v1

    .line 457
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 458
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send goals - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 459
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 460
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 461
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send goals - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 462
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 463
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send goals - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendInstallFinish(Landroid/content/Context;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I

    .prologue
    .line 1017
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: install finish"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1019
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/InstallAppFinishedCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/InstallAppFinishedCommand;-><init>()V

    .line 1022
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/InstallAppFinishedCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/InstallAppFinishedCommand;->setAppId(I)V

    .line 1023
    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/service/ble/queue/commands/InstallAppFinishedCommand;->setContext(Landroid/content/Context;)V

    .line 1027
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1038
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/InstallAppFinishedCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1028
    :catch_0
    move-exception v1

    .line 1029
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1030
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send install finish - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1031
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1032
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1033
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send install finish - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1034
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1035
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send install finish - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendKernelUpdate(Landroid/content/Context;Ljava/lang/String;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pathToFile"    # Ljava/lang/String;

    .prologue
    .line 986
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: kernel update"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 988
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;-><init>()V

    .line 991
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->setPathToFile(Ljava/lang/String;)V

    .line 995
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1006
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 996
    :catch_0
    move-exception v1

    .line 997
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 998
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send kernel update - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 999
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1000
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1001
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send kernel update - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1002
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1003
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send kernel update - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendKernelUpdateStart(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 894
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: kernel update start"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 896
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/StartKernelUpdateCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/StartKernelUpdateCommand;-><init>()V

    .line 903
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/StartKernelUpdateCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 914
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/StartKernelUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 904
    :catch_0
    move-exception v1

    .line 905
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 906
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send kernel update start - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 907
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 908
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 909
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send kernel update start - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 910
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 911
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send kernel update start - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendLocalization(Landroid/content/Context;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileId"    # I

    .prologue
    .line 414
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: localization"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 416
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncLocalizationCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncLocalizationCommand;-><init>()V

    .line 419
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncLocalizationCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncLocalizationCommand;->setFileId(I)V

    .line 420
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_LOCALIZATION:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncLocalizationCommand;->setSettingsType(Lcom/vectorwatch/android/utils/Constants$SettingsType;)V

    .line 424
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 435
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncLocalizationCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 425
    :catch_0
    move-exception v1

    .line 426
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 427
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send localization - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 428
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 429
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 430
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send localization - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 431
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 432
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send localization - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendNotificationMode(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$NotificationMode;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mode"    # Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .prologue
    .line 1528
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: sync time"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1530
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;-><init>()V

    .line 1533
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->setNotificationMode(Lcom/vectorwatch/android/utils/Constants$NotificationMode;)V

    .line 1537
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1548
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1538
    :catch_0
    move-exception v1

    .line 1539
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1540
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send Time - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1541
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1542
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1543
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send Time - IllegalAccessException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1544
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1545
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send time - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendPushData(Landroid/content/Context;[BIIILcom/vectorwatch/android/utils/Constants$AppElementType;III)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "commandBytes"    # [B
    .param p2, "appId"    # I
    .param p3, "watchfaceId"    # I
    .param p4, "elementId"    # I
    .param p5, "appElementType"    # Lcom/vectorwatch/android/utils/Constants$AppElementType;
    .param p6, "ttl"    # I
    .param p7, "dataId"    # I
    .param p8, "ttlType"    # I

    .prologue
    .line 1249
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: push data"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1251
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;-><init>()V

    .line 1254
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setAppId(I)V

    .line 1255
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setWatchfaceId(I)V

    .line 1256
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setElementId(I)V

    .line 1257
    invoke-virtual {v0, p5}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setAppElementType(Lcom/vectorwatch/android/utils/Constants$AppElementType;)V

    .line 1258
    invoke-virtual {v0, p6}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setTtl(I)V

    .line 1259
    invoke-virtual {v0, p7}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setDataId(I)V

    .line 1260
    invoke-virtual {v0, p8}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setTtlType(I)V

    .line 1261
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setCommandBytes([B)V

    .line 1265
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1276
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1266
    :catch_0
    move-exception v1

    .line 1267
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1268
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send push data - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1269
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1270
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1271
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send push data - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1272
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1273
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send push data - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendSerialNumberRequest(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1141
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: serial no req"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1143
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetSerialNumberCommand;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isSystemInfoWith4Decimals()Z

    move-result v2

    invoke-direct {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/GetSerialNumberCommand;-><init>(Z)V

    .line 1150
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/GetSerialNumberCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1161
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetSerialNumberCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1151
    :catch_0
    move-exception v1

    .line 1152
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1153
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send serial no req - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1154
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1155
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1156
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send serial no req - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1157
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1158
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send serial no req - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendSettings(Landroid/content/Context;)Ljava/util/UUID;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1391
    sget-object v20, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v21, "BLE CALLS: send settings"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1393
    new-instance v7, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;

    invoke-direct {v7}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;-><init>()V

    .line 1398
    .local v7, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;
    const-string v20, "settings_greeting_name_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1400
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v17

    .line 1401
    .local v17, "name":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setName(Ljava/lang/String;)V

    .line 1407
    .end local v17    # "name":Ljava/lang/String;
    :goto_0
    const-string v20, "activity_info_age_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 1410
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v20

    .line 1409
    invoke-static/range {v20 .. v21}, Lcom/vectorwatch/android/utils/Helpers;->calculateAgeBasedOnBirthdayInMillis(J)I

    move-result v4

    .line 1411
    .local v4, "age":I
    int-to-short v0, v4

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setAge(S)V

    .line 1417
    .end local v4    # "age":I
    :goto_1
    const-string v20, "activity_info_gender_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1419
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v10

    .line 1420
    .local v10, "gender":I
    int-to-short v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setGender(S)V

    .line 1426
    .end local v10    # "gender":I
    :goto_2
    const-string v20, "activity_info_weight_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 1428
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v19

    .line 1429
    .local v19, "weight":I
    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setWeight(S)V

    .line 1435
    .end local v19    # "weight":I
    :goto_3
    const-string v20, "activity_info_height_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 1437
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v11

    .line 1438
    .local v11, "height":I
    int-to-short v0, v11

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setHeight(S)V

    .line 1444
    .end local v11    # "height":I
    :goto_4
    const-string v20, "settings_unit_system_format_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 1446
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getUnitSystemFormatPreference(Landroid/content/Context;)I

    move-result v18

    .line 1447
    .local v18, "unitSystem":I
    move/from16 v0, v18

    int-to-short v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setUnitSystem(S)V

    .line 1453
    .end local v18    # "unitSystem":I
    :goto_5
    const-string v20, "settings_time_format_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 1455
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTimeFormatPreference(Landroid/content/Context;)I

    move-result v12

    .line 1456
    .local v12, "hourMode":I
    int-to-short v0, v12

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setHourMode(S)V

    .line 1462
    .end local v12    # "hourMode":I
    :goto_6
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualMorningFaceSettings(Landroid/content/Context;)Z

    move-result v16

    .line 1463
    .local v16, "morningGreet":Z
    move/from16 v0, v16

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setMorningGreet(Z)V

    .line 1464
    const-string v20, "settings_contextual_morning_face_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v13

    .line 1466
    .local v13, "isDirty":Z
    invoke-virtual {v7, v13}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDirtyMorningGreet(Z)V

    .line 1469
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoDiscreetSettings(Landroid/content/Context;)Z

    move-result v5

    .line 1470
    .local v5, "autoDiscreet":Z
    invoke-virtual {v7, v5}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setAutoDiscreet(Z)V

    .line 1471
    const-string v20, "settings_contextual_auto_discreet_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v13

    .line 1473
    invoke-virtual {v7, v13}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDirtyAutoDiscreet(Z)V

    .line 1476
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoSleepSettings(Landroid/content/Context;)Z

    move-result v6

    .line 1477
    .local v6, "autoSleep":Z
    invoke-virtual {v7, v6}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setAutoSleep(Z)V

    .line 1478
    const-string v20, "settings_contextual_auto_sleep_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v13

    .line 1480
    invoke-virtual {v7, v13}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDirtyAutoSleep(Z)V

    .line 1483
    const-string v20, "drop_notification_option"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v8

    .line 1485
    .local v8, "dropMode":Z
    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDropMode(Z)V

    .line 1486
    const-string v20, "drop_notification_option_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v13

    .line 1488
    invoke-virtual {v7, v13}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDirtyDropMode(Z)V

    .line 1491
    const-string v20, "dnd_button"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v14

    .line 1493
    .local v14, "isDndOn":Z
    invoke-virtual {v7, v14}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDndOn(Z)V

    .line 1494
    const-string v20, "dnd_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v13

    .line 1495
    invoke-virtual {v7, v13}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDirtyDnd(Z)V

    .line 1498
    const-string v20, "glance_mode"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v15

    .line 1500
    .local v15, "isGlanceOn":Z
    invoke-virtual {v7, v15}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setGlanceOn(Z)V

    .line 1501
    const-string v20, "glance_dirty"

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v13

    .line 1502
    invoke-virtual {v7, v13}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setDirtyGlance(Z)V

    .line 1506
    :try_start_0
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1517
    :goto_7
    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v20

    return-object v20

    .line 1403
    .end local v5    # "autoDiscreet":Z
    .end local v6    # "autoSleep":Z
    .end local v8    # "dropMode":Z
    .end local v13    # "isDirty":Z
    .end local v14    # "isDndOn":Z
    .end local v15    # "isGlanceOn":Z
    .end local v16    # "morningGreet":Z
    :cond_0
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1413
    :cond_1
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setAge(S)V

    goto/16 :goto_1

    .line 1422
    :cond_2
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setGender(S)V

    goto/16 :goto_2

    .line 1431
    :cond_3
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setWeight(S)V

    goto/16 :goto_3

    .line 1440
    :cond_4
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setHeight(S)V

    goto/16 :goto_4

    .line 1449
    :cond_5
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setUnitSystem(S)V

    goto/16 :goto_5

    .line 1458
    :cond_6
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setHourMode(S)V

    goto/16 :goto_6

    .line 1507
    .restart local v5    # "autoDiscreet":Z
    .restart local v6    # "autoSleep":Z
    .restart local v8    # "dropMode":Z
    .restart local v13    # "isDirty":Z
    .restart local v14    # "isDndOn":Z
    .restart local v15    # "isGlanceOn":Z
    .restart local v16    # "morningGreet":Z
    :catch_0
    move-exception v9

    .line 1508
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 1509
    sget-object v20, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Send settings - IOException - "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_7

    .line 1510
    .end local v9    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 1511
    .local v9, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1512
    sget-object v20, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Send settings - IllegalAccessException -"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1513
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v9

    .line 1514
    .local v9, "e":Ljava/lang/Exception;
    sget-object v20, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Send settings - random error "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_7
.end method

.method public static sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "route"    # Lcom/vectorwatch/android/models/PushUpdateRoute;

    .prologue
    .line 352
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: stream value update"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 354
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;-><init>()V

    .line 357
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->setRoute(Lcom/vectorwatch/android/models/PushUpdateRoute;)V

    .line 361
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 372
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 362
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 364
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpStatus - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 365
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 366
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 367
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpStatus - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 368
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 369
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpStatus - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendSyncComplication(Landroid/content/Context;Lcom/vectorwatch/android/models/ChangeComplicationModel;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "model"    # Lcom/vectorwatch/android/models/ChangeComplicationModel;

    .prologue
    .line 383
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: sync complication"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 385
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;-><init>()V

    .line 388
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->setComplication(Lcom/vectorwatch/android/models/ChangeComplicationModel;)V

    .line 392
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 403
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 393
    :catch_0
    move-exception v1

    .line 394
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 395
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send sync complication - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 396
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 397
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 398
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send sync complication - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 399
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 400
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send sync complication - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1111
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BLE CALLS: sys info req. isSystemWith4 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isSystemInfoWith4Decimals()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1113
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetSystemInfoCommand;

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isSystemInfoWith4Decimals()Z

    move-result v2

    invoke-direct {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/GetSystemInfoCommand;-><init>(Z)V

    .line 1120
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/GetSystemInfoCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1131
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetSystemInfoCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1121
    :catch_0
    move-exception v1

    .line 1122
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1123
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send sys info req - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1124
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1125
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1126
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send sys info req - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1127
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1128
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send sys info req - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendTime(Landroid/content/Context;)Ljava/util/UUID;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 180
    sget-object v4, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v5, "BLE CALLS: sync time"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 182
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;-><init>()V

    .line 185
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getCurrentTimeInSeconds()J

    move-result-wide v2

    .line 186
    .local v2, "crtTime":J
    invoke-virtual {v0, v2, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->setCrtTime(J)V

    .line 187
    invoke-static {v2, v3}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getDstInfo(J)Lcom/vectorwatch/android/models/DstInfoModel;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->setDstInfo(Lcom/vectorwatch/android/models/DstInfoModel;)V

    .line 191
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 202
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->getUuid()Ljava/util/UUID;

    move-result-object v4

    return-object v4

    .line 192
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 194
    sget-object v4, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send Time - IOException - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 195
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 196
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 197
    sget-object v4, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send Time - IllegalAccessException - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 199
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send time - random error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendUuidRequest(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1078
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: uuid req"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1080
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetUuidCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetUuidCommand;-><init>()V

    .line 1090
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/GetUuidCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1101
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetUuidCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1091
    :catch_0
    move-exception v1

    .line 1092
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1093
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send uuid req - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1094
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1095
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1096
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send uuid req - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1097
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1098
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send uuid req - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendVftpData(Landroid/content/Context;Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .prologue
    .line 320
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: vftp data"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 322
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;-><init>()V

    .line 325
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V

    .line 326
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->setFile(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)V

    .line 330
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 341
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 331
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 333
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpData - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 334
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 335
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 336
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpData - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 337
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 338
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpData - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendVftpPut(Landroid/content/Context;SSZZLcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "realSize"    # S
    .param p2, "compressedSize"    # S
    .param p3, "isCompressed"    # Z
    .param p4, "isForced"    # Z
    .param p5, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p6, "fileId"    # I

    .prologue
    .line 283
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: vftp put"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 285
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;-><init>()V

    .line 288
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V

    .line 289
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setRealSize(S)V

    .line 290
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setCompressedSize(S)V

    .line 291
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setIsCompressed(Z)V

    .line 292
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setIsForced(Z)V

    .line 293
    invoke-virtual {v0, p5}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setFileType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 294
    invoke-virtual {v0, p6}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setFileId(I)V

    .line 298
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 309
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 299
    :catch_0
    move-exception v1

    .line 300
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 301
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpPut - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 302
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 303
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 304
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpPut - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 306
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpPut - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendVftpRequest(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpFileType;I)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p2, "fileId"    # I

    .prologue
    .line 212
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: vftp request"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 214
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;-><init>()V

    .line 217
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;->setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V

    .line 218
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;->setFileType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 219
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;->setFileId(I)V

    .line 223
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 234
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpRequestCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 224
    :catch_0
    move-exception v1

    .line 225
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 226
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpReq - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 227
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 228
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 229
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpReq - IllegalAccessException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 230
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 231
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpReq - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendVftpStatus(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$VftpStatus;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "status"    # Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .prologue
    .line 244
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: vftp status"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 246
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;-><init>()V

    .line 249
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V

    .line 250
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->setStatus(Lcom/vectorwatch/android/utils/Constants$VftpStatus;)V

    .line 254
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 265
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 255
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 257
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpStatus - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 258
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 259
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 260
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpStatus - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 261
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 262
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send VftpStatus - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendWarningOption(Landroid/content/Context;ZZZZ)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "linkLostIcon"    # Z
    .param p2, "lowBatteryIcon"    # Z
    .param p3, "linkLostNotification"    # Z
    .param p4, "lowBatteryNotification"    # Z

    .prologue
    .line 1322
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: warning"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1324
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;-><init>()V

    .line 1327
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->setDefaultLinkLostIcon(Z)V

    .line 1328
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->setDefaultLinkLostNotification(Z)V

    .line 1329
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->setDefaultLowBatteryIcon(Z)V

    .line 1330
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->setDefaultLowBatteryNotifications(Z)V

    .line 1334
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1345
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1335
    :catch_0
    move-exception v1

    .line 1336
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1337
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send warning options - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1338
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1339
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1340
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send warning options - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1341
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1342
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send warning options - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendWatchLogsRequest(Landroid/content/Context;)Ljava/util/UUID;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1048
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE CALLS: watch log req"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1050
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetWatchLogsCommand;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetWatchLogsCommand;-><init>()V

    .line 1057
    .local v0, "command":Lcom/vectorwatch/android/service/ble/queue/commands/GetWatchLogsCommand;
    :try_start_0
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCommand(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1068
    :goto_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetWatchLogsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    return-object v2

    .line 1058
    :catch_0
    move-exception v1

    .line 1059
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1060
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send watch logs req - IOException - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1061
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1062
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 1063
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send watch logs req - IllegalAccessException -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1064
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1065
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send watch logs req - random error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static stop(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothManager;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: stop"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 154
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "service_command"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 157
    return-void
.end method
