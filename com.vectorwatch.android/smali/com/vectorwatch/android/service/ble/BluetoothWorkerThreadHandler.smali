.class public Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;
.super Landroid/os/Handler;
.source "BluetoothWorkerThreadHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private isHandlerThreadTerminating:Z

.field private mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

.field private mConnectionState:I

.field private mContext:Landroid/content/Context;

.field private mNotificationsManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

.field private mQueue:Ljava/util/PriorityQueue;

.field private mResponseRouter:Lcom/vectorwatch/android/service/ble/ResponseRouter;

.field private mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

.field private mTransformer:Lcom/vectorwatch/android/service/ble/Transformer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    .line 95
    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-direct {v0, p0, p1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;-><init>(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    .line 97
    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;-><init>(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;)V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransformer:Lcom/vectorwatch/android/service/ble/Transformer;

    .line 99
    new-instance v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;

    invoke-direct {v0, p0, p1}, Lcom/vectorwatch/android/service/ble/ResponseRouter;-><init>(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mResponseRouter:Lcom/vectorwatch/android/service/ble/ResponseRouter;

    .line 101
    invoke-static {}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->getInstance()Lcom/vectorwatch/android/service/ble/NotificationsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mNotificationsManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    .line 103
    iput v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    .line 104
    invoke-direct {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->setIsHandlerThreadTerminating(Z)V

    .line 106
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xb

    new-instance v2, Lcom/vectorwatch/android/service/ble/queue/PriorityComparator;

    invoke-direct {v2}, Lcom/vectorwatch/android/service/ble/queue/PriorityComparator;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 109
    return-void
.end method

.method private addDetailsToBlePackets(Ljava/util/List;Ljava/util/UUID;)Ljava/util/List;
    .locals 4
    .param p2, "cmdUuid"    # Ljava/util/UUID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;",
            "Ljava/util/UUID;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    .local p1, "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    if-nez p1, :cond_1

    .line 710
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE: null packet list for extra details."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 711
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 726
    :cond_0
    :goto_0
    return-object p1

    .line 714
    .restart local p1    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 716
    .local v1, "totalPackets":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .line 717
    .local v0, "pckt":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    if-nez v0, :cond_2

    .line 718
    sget-object v2, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE: null packet in the list for extra details."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 719
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 721
    .restart local p1    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    :cond_2
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->setPacketUuid(Ljava/util/UUID;)V

    .line 722
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->setIndex(I)V

    .line 723
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->setTotalPackets(I)V

    goto :goto_1
.end method

.method private addToQueue(Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V
    .locals 5
    .param p1, "command"    # Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .prologue
    const/4 v4, -0x1

    .line 112
    if-nez p1, :cond_0

    .line 113
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v1, "QUEUE: Received NULL command!!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 129
    :goto_0
    return-void

    .line 118
    :cond_0
    iget v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 119
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/CommandStatusEvent;

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-direct {v1, v2, v3, v4, v4}, Lcom/vectorwatch/android/events/CommandStatusEvent;-><init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 126
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BLE CALLS: ADDED command! priority = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getPriority()Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " cmd type ="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " command in progress status = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " connection "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "status = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private handleBleCloseGatt()V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v0}, Lcom/vectorwatch/android/service/ble/Transceiver;->closeConnection()V

    .line 475
    return-void
.end method

.method private handleBleConnect(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 388
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to connect to watch. Current connection status is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 391
    iget v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    if-nez v1, :cond_0

    .line 393
    const v1, 0x7f0900aa

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "VectorWatch"

    const v3, 0x7f0200ea

    const-class v4, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v2, v3, p1, v4}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->buildNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)Landroid/app/Notification;

    move-result-object v0

    .line 395
    .local v0, "notification":Landroid/app/Notification;
    check-cast p1, Lcom/vectorwatch/android/service/ble/BleService;

    .end local p1    # "context":Landroid/content/Context;
    const v1, 0x1e240

    invoke-virtual {p1, v1, v0}, Lcom/vectorwatch/android/service/ble/BleService;->startForeground(ILandroid/app/Notification;)V

    .line 397
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v1}, Lcom/vectorwatch/android/service/ble/Transceiver;->connect()Z

    .line 400
    const-string v1, "flag_action_forget_requested"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 403
    .end local v0    # "notification":Landroid/app/Notification;
    :cond_0
    return-void
.end method

.method private handleBleDisconnect()V
    .locals 2

    .prologue
    .line 463
    iget v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 465
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v0}, Lcom/vectorwatch/android/service/ble/Transceiver;->disconnect()V

    .line 467
    :cond_0
    return-void
.end method

.method private handleForgetWatch()V
    .locals 4

    .prologue
    .line 410
    const-string v1, "flag_action_forget_requested"

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 413
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->clear()V

    .line 414
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 415
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleBleCloseGatt()V

    .line 416
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->unpairAndForgetWatch(Ljava/lang/String;)V

    .line 419
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    const-class v2, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 420
    .local v0, "pairWatchIntent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 421
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 422
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 423
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 426
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->stop(Landroid/content/Context;)V

    .line 427
    return-void
.end method

.method private handlePacketSentToBle(Lcom/vectorwatch/android/service/ble/BleGattPacket;)V
    .locals 3
    .param p1, "bleGattPacket"    # Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .prologue
    .line 574
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isCommandInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BLE CALLS: [progress]:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <?> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 576
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getTotalPackets()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getStatus()Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " || "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 577
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getPacketUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 575
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 581
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getPacketUuid()Ljava/util/UUID;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 582
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getStatus()Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->SUCCESS:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    if-ne v0, v1, :cond_1

    .line 584
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getTotalPackets()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->packetSendWithSuccess(II)V

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getTotalPackets()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->packetSendFailed(II)V

    goto :goto_0

    .line 590
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE CALLS: CALLBACK: [progress] Not the same uuid."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleReconnect()V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v0}, Lcom/vectorwatch/android/service/ble/Transceiver;->reconnect()V

    .line 484
    return-void
.end method

.method private handleStop()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v0}, Lcom/vectorwatch/android/service/ble/Transceiver;->destroy()V

    .line 479
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mResponseRouter:Lcom/vectorwatch/android/service/ble/ResponseRouter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/ResponseRouter;->shutDownExecutorService()V

    .line 480
    return-void
.end method

.method private isCommandInProgress()Z
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private packCommandAndSendToBle(Ljava/util/List;Landroid/bluetooth/BluetoothGattCharacteristic;Ljava/util/UUID;)Ljava/util/List;
    .locals 7
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "cmdUuid"    # Ljava/util/UUID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            "Ljava/util/UUID;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    .local p1, "allMessages":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    sget-object v5, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BLE CALLS: pack and send to BLE. connectivity = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " command in send = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 323
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 325
    .local v0, "allGattPackets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 326
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/service/ble/messages/BtMessage;

    .line 328
    .local v3, "message":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    if-eqz v3, :cond_1

    .line 329
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransformer:Lcom/vectorwatch/android/service/ble/Transformer;

    invoke-interface {v4, v3, p2}, Lcom/vectorwatch/android/service/ble/Transformer;->packMessageForDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/util/List;

    move-result-object v1

    .line 330
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 325
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 320
    .end local v0    # "allGattPackets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    .end local v2    # "i":I
    .end local v3    # "message":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 332
    .restart local v0    # "allGattPackets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    .restart local v2    # "i":I
    .restart local v3    # "message":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    :cond_1
    sget-object v4, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "BLE CALLS: Null command partial message in command."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 333
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 340
    .end local v3    # "message":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    :goto_2
    return-object v4

    .line 338
    :cond_2
    invoke-direct {p0, v0, p3}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->addDetailsToBlePackets(Ljava/util/List;Ljava/util/UUID;)Ljava/util/List;

    move-result-object v0

    move-object v4, v0

    .line 340
    goto :goto_2
.end method

.method private packNotificationsAndSendToBle(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;Ljava/util/UUID;)V
    .locals 2
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "cmdUuid"    # Ljava/util/UUID;

    .prologue
    .line 305
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransformer:Lcom/vectorwatch/android/service/ble/Transformer;

    invoke-interface {v1, p1, p2}, Lcom/vectorwatch/android/service/ble/Transformer;->packMessageForDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/util/List;

    move-result-object v0

    .line 308
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    invoke-direct {p0, v0, p3}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->addDetailsToBlePackets(Ljava/util/List;Ljava/util/UUID;)Ljava/util/List;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v1, v0}, Lcom/vectorwatch/android/service/ble/Transceiver;->addPacketsToBuffer(Ljava/util/List;)V

    .line 311
    return-void
.end method

.method private packetSendFailed(II)V
    .locals 4
    .param p1, "packetIndex"    # I
    .param p2, "totalPackets"    # I

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isCommandInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE CALLS: CALLBACK: Received command failed but there is NO command IN PROGRESS."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 295
    :goto_0
    return-void

    .line 288
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BLE CALLS: Command send failed. Call callback for command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/CommandStatusEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/vectorwatch/android/events/CommandStatusEvent;-><init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 294
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->requestSendCommandToDevice()V

    goto :goto_0
.end method

.method private packetSendWithSuccess(II)V
    .locals 4
    .param p1, "packetIndex"    # I
    .param p2, "totalPackets"    # I

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isCommandInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE CALLS: Received command executed successfully but there is NO command IN PROGRESS."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 274
    :goto_0
    return-void

    .line 261
    :cond_0
    add-int/lit8 v0, p1, 0x1

    if-ne v0, p2, :cond_1

    .line 263
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BLE CALLS: CALLBACK: Command successfully sent. Call callback for command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 264
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 265
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/CommandStatusEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->SUCCESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/vectorwatch/android/events/CommandStatusEvent;-><init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 268
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->requestSendCommandToDevice()V

    goto :goto_0

    .line 271
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/CommandStatusEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->PROGRESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/vectorwatch/android/events/CommandStatusEvent;-><init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private receiveRawDataFromDevice(Lcom/vectorwatch/android/models/BtResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/vectorwatch/android/models/BtResponse;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransformer:Lcom/vectorwatch/android/service/ble/Transformer;

    iget-object v1, p1, Lcom/vectorwatch/android/models/BtResponse;->data:[B

    iget-object v2, p1, Lcom/vectorwatch/android/models/BtResponse;->uuid:Ljava/util/UUID;

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/Transformer;->unpackMessageFromDevice([BLjava/util/UUID;)V

    .line 203
    return-void
.end method

.method private receiveReadRssi()V
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v0}, Lcom/vectorwatch/android/service/ble/Transceiver;->readRssi()Z

    .line 492
    return-void
.end method

.method private receivedBleCommand(I)V
    .locals 1
    .param p1, "bleCmdType"    # I

    .prologue
    .line 358
    packed-switch p1, :pswitch_data_0

    .line 380
    :goto_0
    return-void

    .line 360
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleBleConnect(Landroid/content/Context;)V

    goto :goto_0

    .line 363
    :pswitch_1
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleBleDisconnect()V

    goto :goto_0

    .line 366
    :pswitch_2
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleForgetWatch()V

    goto :goto_0

    .line 369
    :pswitch_3
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleReconnect()V

    goto :goto_0

    .line 372
    :pswitch_4
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleBleCloseGatt()V

    goto :goto_0

    .line 375
    :pswitch_5
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    .line 376
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 377
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handleStop()V

    goto :goto_0

    .line 358
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method private receivedConnectionStateChanged(I)V
    .locals 9
    .param p1, "newState"    # I

    .prologue
    const/4 v8, 0x0

    const v4, 0x7f090093

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 496
    packed-switch p1, :pswitch_data_0

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 498
    :pswitch_0
    sput-boolean v6, Lcom/vectorwatch/android/service/ble/BleService;->isConnectedToWatch:Z

    .line 499
    iput v6, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    .line 500
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    const v2, 0x7f0900aa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "VectorWatch"

    const v3, 0x7f0200ea

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    const-class v5, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    .line 504
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->clear()V

    .line 505
    iput-object v8, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 509
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/LinkStateChangedEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/LinkStateChangedEvent;-><init>()V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 510
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/InstallAppEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    .line 511
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090063

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v7, v3}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 510
    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 515
    :pswitch_1
    iput v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    .line 516
    sput-boolean v6, Lcom/vectorwatch/android/service/ble/BleService;->isConnectedToWatch:Z

    .line 517
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "VectorWatch"

    const v3, 0x7f0200f9

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    const-class v5, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 522
    :pswitch_2
    const/4 v1, 0x3

    iput v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    .line 523
    sput-boolean v6, Lcom/vectorwatch/android/service/ble/BleService;->isConnectedToWatch:Z

    goto :goto_0

    .line 527
    :pswitch_3
    iput-object v8, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 529
    const-string v1, "flag_check_for_connect_disconnect_need"

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    .line 530
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 531
    .local v0, "needsDisconnectConnect":Z
    if-eqz v0, :cond_1

    if-ne p1, v7, :cond_1

    .line 532
    const-string v1, "flag_check_for_connect_disconnect_need"

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1, v6, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 534
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v2, "Transceiver - Needs disconnect - connect after first pair."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 535
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->reconnect(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 539
    :cond_1
    sput-boolean v3, Lcom/vectorwatch/android/service/ble/BleService;->isConnectedToWatch:Z

    .line 540
    iput v7, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    .line 541
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "VectorWatch"

    const v3, 0x7f0200e6

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    const-class v5, Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V

    .line 546
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->mandatoryRequestsAtConnect(Landroid/content/Context;)V

    .line 549
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/BleTrulyConnectedReceivedEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/BleTrulyConnectedReceivedEvent;-><init>()V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 553
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/LinkStateChangedEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/LinkStateChangedEvent;-><init>()V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 556
    const-string v1, "check_compatibility"

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1, v6, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 558
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC_APPS: truly connected - no need to check compatibility."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 559
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/service/VersionMonitor;->startConnectionSyncs(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 496
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private requestSendCommandToDevice()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 210
    iget v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isCommandInProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 212
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 214
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getTransferMessages()Ljava/util/List;

    move-result-object v0

    .line 216
    .local v0, "allMessagesInCommand":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 217
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    sget-object v4, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_TX:Ljava/util/UUID;

    invoke-interface {v3, v4}, Lcom/vectorwatch/android/service/ble/Transceiver;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    .line 220
    .local v1, "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 221
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v3

    .line 220
    invoke-direct {p0, v0, v1, v3}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->packCommandAndSendToBle(Ljava/util/List;Landroid/bluetooth/BluetoothGattCharacteristic;Ljava/util/UUID;)Ljava/util/List;

    move-result-object v2

    .line 222
    .local v2, "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 223
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendToBle(Ljava/util/List;)V

    .line 247
    .end local v0    # "allMessagesInCommand":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    .end local v1    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v2    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    :cond_0
    :goto_0
    return-void

    .line 225
    .restart local v0    # "allMessagesInCommand":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    .restart local v1    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    .restart local v2    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE CALLS: After packing ... packets size is 0."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 228
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/CommandStatusEvent;

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-direct {v4, v5, v6, v7, v7}, Lcom/vectorwatch/android/events/CommandStatusEvent;-><init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 232
    iput-object v8, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 233
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->requestSendCommandToDevice()V

    goto :goto_0

    .line 236
    .end local v1    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v2    # "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE CALLS: Command is NOT OK and will be dropped. Request send next."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 239
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/CommandStatusEvent;

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    invoke-virtual {v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getUuid()Ljava/util/UUID;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-direct {v4, v5, v6, v7, v7}, Lcom/vectorwatch/android/events/CommandStatusEvent;-><init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 243
    iput-object v8, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mCommandInProgress:Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 244
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->requestSendCommandToDevice()V

    goto :goto_0
.end method

.method private sendNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 5
    .param p1, "notification"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue
    .line 138
    iget v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mConnectionState:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    .line 139
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE CALLS: Received notification but the watch is disconnected."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 151
    :cond_0
    return-void

    .line 142
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mNotificationsManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->processNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;

    move-result-object v2

    .line 144
    .local v2, "notificationMessagesArray":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    sget-object v4, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_TX:Ljava/util/UUID;

    invoke-interface {v3, v4}, Lcom/vectorwatch/android/service/ble/Transceiver;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    .line 146
    .local v0, "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    if-eqz v2, :cond_0

    .line 147
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/service/ble/messages/BtMessage;

    .line 148
    .local v1, "message":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-direct {p0, v1, v0, v4}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->packNotificationsAndSendToBle(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;Ljava/util/UUID;)V

    goto :goto_0
.end method

.method private sendToBle(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 349
    .local p1, "gattPackets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/service/ble/Transceiver;->addPacketsToBuffer(Ljava/util/List;)V

    .line 350
    return-void
.end method

.method private setIsHandlerThreadTerminating(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 566
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isHandlerThreadTerminating:Z

    .line 567
    return-void
.end method

.method private unpairAndForgetWatch(Ljava/lang/String;)V
    .locals 7
    .param p1, "pairedDeviceAddress"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 435
    if-nez p1, :cond_0

    .line 436
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "There should be a paired device at this point. Trying to forget a device though none was saved as paired."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 439
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    .line 440
    .local v1, "mBluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 442
    .local v2, "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 443
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 444
    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->unpairDevice(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 449
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    const-string v3, "previous_watch_shape"

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    .line 450
    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    .line 449
    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 452
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 453
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceName(Ljava/lang/String;Landroid/content/Context;)V

    .line 454
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchOldCpuId(Ljava/lang/String;Landroid/content/Context;)V

    .line 455
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchNewCpuId(Ljava/lang/String;Landroid/content/Context;)V

    .line 456
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mContext:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchShape(Ljava/lang/String;Landroid/content/Context;)V

    .line 457
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 606
    sget-object v7, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$1;->$SwitchMap$com$vectorwatch$android$service$ble$BluetoothWorkerThreadHandler$MessageType:[I

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-static {v8}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getType(I)Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 653
    sget-object v7, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v8, "Should not get here. Problem with message type in BluetoothWorkerThreadHandler."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 655
    :goto_0
    return-void

    .line 609
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 610
    .local v1, "command":Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    invoke-direct {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->addToQueue(Lcom/vectorwatch/android/service/ble/queue/BaseCommand;)V

    .line 611
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->requestSendCommandToDevice()V

    goto :goto_0

    .line 614
    .end local v1    # "command":Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    :pswitch_1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .line 615
    .local v4, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    invoke-direct {p0, v4}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    goto :goto_0

    .line 618
    .end local v4    # "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :pswitch_2
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    .line 619
    .local v5, "notificationRequest":Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;
    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->receiveNotificationDetailsRequest(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;)V

    goto :goto_0

    .line 623
    .end local v5    # "notificationRequest":Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;
    :pswitch_3
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/vectorwatch/android/models/BtResponse;

    .line 624
    .local v6, "response":Lcom/vectorwatch/android/models/BtResponse;
    invoke-direct {p0, v6}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->receiveRawDataFromDevice(Lcom/vectorwatch/android/models/BtResponse;)V

    goto :goto_0

    .line 627
    .end local v6    # "response":Lcom/vectorwatch/android/models/BtResponse;
    :pswitch_4
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lcom/vectorwatch/android/service/ble/messages/BtMessage;

    invoke-virtual {p0, v7}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->receiveMessageFromDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;)V

    goto :goto_0

    .line 631
    :pswitch_5
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    .line 632
    .local v3, "newState":Ljava/lang/Integer;
    sget-object v7, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Received connection state changed in Handler: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 633
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->receivedConnectionStateChanged(I)V

    goto :goto_0

    .line 637
    .end local v3    # "newState":Ljava/lang/Integer;
    :pswitch_6
    sget-object v7, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v8, "QUEUE: MESSAGES: Worker thread received MSG_TYPE_BLE_COMMAND:"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 638
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 639
    .local v2, "commandType":I
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->receivedBleCommand(I)V

    goto :goto_0

    .line 642
    .end local v2    # "commandType":I
    :pswitch_7
    sget-object v7, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v8, "Handler going into stopping mode (not receiving any more requests from now)"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 643
    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->setIsHandlerThreadTerminating(Z)V

    goto :goto_0

    .line 646
    :pswitch_8
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->receiveReadRssi()V

    goto :goto_0

    .line 649
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .line 650
    .local v0, "bleGattPacket":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->handlePacketSentToBle(Lcom/vectorwatch/android/service/ble/BleGattPacket;)V

    goto :goto_0

    .line 606
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public isHandlerThreadTerminating()Z
    .locals 1

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isHandlerThreadTerminating:Z

    return v0
.end method

.method public receiveMessageFromDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;)V
    .locals 1
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/BtMessage;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mResponseRouter:Lcom/vectorwatch/android/service/ble/ResponseRouter;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/ble/ResponseRouter;->processDataFromRemoteDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;)V

    .line 192
    return-void
.end method

.method public receiveNotificationDetailsRequest(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;)V
    .locals 5
    .param p1, "notificationRequest"    # Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    .prologue
    .line 159
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$1;->$SwitchMap$com$vectorwatch$android$service$ble$messages$NotificationRequestMessage$NotificationRequestType:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getRequestType()Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 180
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Should not get here. Notification request should be positive action, negative action or details request."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 183
    :goto_0
    return-void

    .line 161
    :pswitch_0
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mNotificationsManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    .line 162
    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->processNotificationDetailsRequest(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;)Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;

    move-result-object v2

    .line 163
    .local v2, "notificationDetails":Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 164
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mTransceiver:Lcom/vectorwatch/android/service/ble/Transceiver;

    sget-object v4, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_TX_NOT_INFO:Ljava/util/UUID;

    invoke-interface {v3, v4}, Lcom/vectorwatch/android/service/ble/Transceiver;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    .line 169
    .local v0, "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-direct {p0, v2, v0, v3}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->packNotificationsAndSendToBle(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;Ljava/util/UUID;)V

    goto :goto_0

    .line 172
    .end local v0    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    .end local v2    # "notificationDetails":Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
    :pswitch_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE CALLS: Got positive action request."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 173
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mResponseRouter:Lcom/vectorwatch/android/service/ble/ResponseRouter;

    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/service/ble/ResponseRouter;->processDataFromRemoteDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;)V

    goto :goto_0

    .line 176
    :pswitch_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE CALLS: Got negative action request."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 177
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->mResponseRouter:Lcom/vectorwatch/android/service/ble/ResponseRouter;

    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/service/ble/ResponseRouter;->processDataFromRemoteDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;)V

    goto :goto_0

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
