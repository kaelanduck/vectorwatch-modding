.class public Lcom/vectorwatch/android/service/AppPushService;
.super Landroid/app/IntentService;
.source "AppPushService.java"


# static fields
.field public static final APP_UUID:Ljava/lang/String; = "appUuid"

.field public static final KEY:Ljava/lang/String; = "key"

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/vectorwatch/android/service/AppPushService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/AppPushService;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "AppPushService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x1

    .line 48
    const-string v9, "key"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 49
    .local v7, "hashKey":Ljava/lang/String;
    const-string v9, "appUuid"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "appUuid":Ljava/lang/String;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    if-nez v4, :cond_2

    .line 52
    :cond_0
    sget-object v9, Lcom/vectorwatch/android/service/AppPushService;->log:Lorg/slf4j/Logger;

    const-string v10, "Receiving Push without a hash key or mAppId. Doing nothing..."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 90
    :cond_1
    :goto_0
    return-void

    .line 58
    :cond_2
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 59
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v9, "com.vectorwatch.android"

    invoke-virtual {v1, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 61
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v9, v2

    if-ge v9, v10, :cond_3

    .line 62
    sget-object v9, Lcom/vectorwatch/android/service/AppPushService;->log:Lorg/slf4j/Logger;

    const-string v10, "There should be at least one account in Accounts & sync"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_3
    const/4 v9, 0x0

    aget-object v0, v2, v9

    .line 68
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v4, p0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppId(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    .line 69
    .local v3, "appId":I
    if-lez v3, :cond_4

    invoke-static {v4, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 71
    :try_start_0
    const-string v9, "Full access"

    const/4 v10, 0x1

    invoke-virtual {v1, v0, v9, v10}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 73
    .local v5, "authToken":Ljava/lang/String;
    invoke-static {v5, v7}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getAppPushData(Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;

    move-result-object v8

    .line 74
    .local v8, "pushResponse":Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;
    if-eqz v8, :cond_1

    .line 75
    iget-object v9, v8, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$AppPushData;

    iget-object v9, v9, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$AppPushData;->appPushData:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;

    invoke-static {v9}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->fromPushResponse(Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    move-result-object v9

    invoke-static {p0, v3, v9}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receiveResponseFromOtherChannels(Landroid/content/Context;ILcom/vectorwatch/android/models/AppCallbackProxyResponse;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 77
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v8    # "pushResponse":Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;
    :catch_0
    move-exception v6

    .line 78
    .local v6, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v6}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 79
    .end local v6    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v6

    .line 80
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 81
    .end local v6    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 82
    .local v6, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v6}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v6    # "e":Landroid/accounts/AuthenticatorException;
    :catch_3
    move-exception v6

    .line 84
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 88
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_4
    new-instance v9, Landroid/content/Intent;

    const-class v10, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v9, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v9}, Lcom/vectorwatch/android/service/AppPushService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
