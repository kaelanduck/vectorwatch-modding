.class public Lcom/vectorwatch/android/service/FitnessClientService;
.super Landroid/app/IntentService;
.source "FitnessClientService.java"


# static fields
.field private static final MAX_DATA_POINTS_PER_DATA_SET:I = 0x3e8

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vectorwatch/android/service/FitnessClientService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "FitnessClientService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method private clearDirtyStatusForActivityStartTimestamps(Ljava/util/List;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V
    .locals 2
    .param p2, "type"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/vectorwatch/android/utils/Constants$GoogleFitData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "syncedTimestamps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    sget-object v0, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Update dirty field in DB."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 336
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/vectorwatch/android/database/DatabaseManager;->updateDirtyFieldForActivityInDb(Ljava/util/List;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 337
    return-void
.end method

.method private connectClient()V
    .locals 2

    .prologue
    .line 401
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    sget-object v0, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Connecting to client"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 403
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 407
    :goto_0
    return-void

    .line 405
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Client null or already connected!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createCorrespondingDataSource(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)Lcom/google/android/gms/fitness/data/DataSource;
    .locals 3
    .param p1, "dataType"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    .prologue
    const/4 v2, 0x0

    .line 299
    sget-object v0, Lcom/vectorwatch/android/service/FitnessClientService$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 322
    sget-object v0, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Create corresponding data source with unknown type."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 323
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 301
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/fitness/data/DataSource$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/DataSource$Builder;-><init>()V

    .line 302
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setAppPackageName(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->TYPE_STEP_COUNT_DELTA:Lcom/google/android/gms/fitness/data/DataType;

    .line 303
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setDataType(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    const-string v1, "VECTOR - step count"

    .line 304
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setStreamName(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    .line 305
    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setType(I)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->build()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    goto :goto_0

    .line 308
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/fitness/data/DataSource$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/DataSource$Builder;-><init>()V

    .line 309
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setAppPackageName(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->TYPE_DISTANCE_DELTA:Lcom/google/android/gms/fitness/data/DataType;

    .line 310
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setDataType(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    const-string v1, "VECTOR - distance"

    .line 311
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setStreamName(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    .line 312
    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setType(I)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->build()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    goto :goto_0

    .line 315
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/fitness/data/DataSource$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/DataSource$Builder;-><init>()V

    .line 316
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setAppPackageName(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->TYPE_CALORIES_EXPENDED:Lcom/google/android/gms/fitness/data/DataType;

    .line 317
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setDataType(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    const-string v1, "VECTOR - calories"

    .line 318
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setStreamName(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    .line 319
    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setType(I)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->build()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private disconnectClient()V
    .locals 3

    .prologue
    .line 388
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    sget-object v0, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v1, "FTNESS_API: Disconnecting from client"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 390
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 395
    :goto_0
    return-void

    .line 392
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FITNESS_API: Client null or already disconnected! "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-nez v0, :cond_1

    const-string v0, "null"

    .line 393
    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392
    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 393
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method

.method private getActivityValuesFromDataSet(Lcom/google/android/gms/fitness/data/DataSet;)Ljava/util/List;
    .locals 6
    .param p1, "dataSet"    # Lcom/google/android/gms/fitness/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/fitness/data/DataSet;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->getDataPoints()Ljava/util/List;

    move-result-object v1

    .line 377
    .local v1, "dataPoints":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/fitness/data/DataPoint;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 378
    .local v0, "dataPoint":Lcom/google/android/gms/fitness/data/DataPoint;
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->getStartTime(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 381
    .end local v0    # "dataPoint":Lcom/google/android/gms/fitness/data/DataPoint;
    :cond_0
    return-object v2
.end method

.method private insertDataSetInGoogleFit(Lcom/google/android/gms/fitness/data/DataSet;)Z
    .locals 6
    .param p1, "dataSet"    # Lcom/google/android/gms/fitness/data/DataSet;

    .prologue
    const/4 v1, 0x0

    .line 347
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 348
    sget-object v2, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v3, "FTNESS_API: No points in dataset."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 349
    invoke-direct {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->disconnectClient()V

    .line 362
    :goto_0
    return v1

    .line 353
    :cond_0
    sget-object v2, Lcom/google/android/gms/fitness/Fitness;->HistoryApi:Lcom/google/android/gms/fitness/HistoryApi;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 354
    invoke-interface {v2, v3, p1}, Lcom/google/android/gms/fitness/HistoryApi;->insertData(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    const-wide/16 v4, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 357
    .local v0, "insertStatus":Lcom/google/android/gms/common/api/Status;
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_1

    .line 358
    sget-object v2, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FITNESS_API: There was a problem inserting the dataset. Status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_0

    .line 361
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FITNESS_API: Dataset inserted successfully. Status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 362
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private sendActivityData(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V
    .locals 17
    .param p1, "dataType"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    .prologue
    .line 196
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Triggered sendActivityData."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v13

    .line 198
    .local v13, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v13, v0}, Lcom/vectorwatch/android/database/DatabaseManager;->getEntriesForGoogleFit(Lio/realm/Realm;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)Ljava/util/List;

    move-result-object v2

    .line 200
    .local v2, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ActivityValueModel;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 201
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FITNESS_API: Null/No activities for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 202
    invoke-virtual {v13}, Lio/realm/Realm;->close()V

    .line 203
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/service/FitnessClientService;->disconnectClient()V

    .line 296
    :goto_0
    return-void

    .line 214
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/vectorwatch/android/service/FitnessClientService;->createCorrespondingDataSource(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v11

    .line 215
    .local v11, "dataSource":Lcom/google/android/gms/fitness/data/DataSource;
    if-nez v11, :cond_2

    .line 216
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Null data source (first)."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v13}, Lio/realm/Realm;->close()V

    goto :goto_0

    .line 221
    :cond_2
    invoke-static {v11}, Lcom/google/android/gms/fitness/data/DataSet;->create(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v10

    .line 223
    .local v10, "dataSet":Lcom/google/android/gms/fitness/data/DataSet;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v15, "syncedActivityStartTimestamps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v12, v3, :cond_7

    .line 228
    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/ActivityValueModel;

    .line 230
    .local v16, "value":Lcom/vectorwatch/android/models/ActivityValueModel;
    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/DataSet;->getDataPoints()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x3e8

    if-lt v3, v4, :cond_5

    .line 231
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Over 1000 data points."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 233
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/vectorwatch/android/service/FitnessClientService;->insertDataSetInGoogleFit(Lcom/google/android/gms/fitness/data/DataSet;)Z

    move-result v14

    .line 234
    .local v14, "success":Z
    if-eqz v14, :cond_3

    .line 235
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Lcom/vectorwatch/android/service/FitnessClientService;->clearDirtyStatusForActivityStartTimestamps(Ljava/util/List;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 239
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/vectorwatch/android/service/FitnessClientService;->createCorrespondingDataSource(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v11

    .line 240
    if-nez v11, :cond_4

    .line 241
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Null data source (after first)."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 242
    invoke-virtual {v13}, Lio/realm/Realm;->close()V

    goto :goto_0

    .line 247
    :cond_4
    invoke-static {v11}, Lcom/google/android/gms/fitness/data/DataSet;->create(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v10

    .line 250
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 258
    .end local v14    # "success":Z
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getStartTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getValue()I

    move-result v3

    if-gtz v3, :cond_6

    .line 227
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 267
    :cond_6
    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/DataSet;->createDataPoint()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v3

    .line 268
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getStartTime()J

    move-result-wide v4

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getEndTime()J

    move-result-wide v6

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/fitness/data/DataPoint;->setTimeInterval(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v9

    .line 270
    .local v9, "dataPoint":Lcom/google/android/gms/fitness/data/DataPoint;
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 283
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Send activity data with unknown type."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 286
    :goto_3
    invoke-virtual {v10, v9}, Lcom/google/android/gms/fitness/data/DataSet;->add(Lcom/google/android/gms/fitness/data/DataPoint;)V

    goto :goto_2

    .line 272
    :pswitch_0
    sget-object v3, Lcom/google/android/gms/fitness/data/Field;->FIELD_STEPS:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v9, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->getValue(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/Value;->setInt(I)V

    goto :goto_3

    .line 276
    :pswitch_1
    sget-object v3, Lcom/google/android/gms/fitness/data/Field;->FIELD_DISTANCE:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v9, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->getValue(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getValue()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/Value;->setFloat(F)V

    goto :goto_3

    .line 280
    :pswitch_2
    sget-object v3, Lcom/google/android/gms/fitness/data/Field;->FIELD_CALORIES:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v9, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->getValue(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/ActivityValueModel;->getValue()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x47c35000    # 100000.0f

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/Value;->setFloat(F)V

    goto :goto_3

    .line 290
    .end local v9    # "dataPoint":Lcom/google/android/gms/fitness/data/DataPoint;
    .end local v16    # "value":Lcom/vectorwatch/android/models/ActivityValueModel;
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/vectorwatch/android/service/FitnessClientService;->insertDataSetInGoogleFit(Lcom/google/android/gms/fitness/data/DataSet;)Z

    move-result v14

    .line 291
    .restart local v14    # "success":Z
    if-eqz v14, :cond_8

    .line 292
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Lcom/vectorwatch/android/service/FitnessClientService;->clearDirtyStatusForActivityStartTimestamps(Ljava/util/List;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 294
    :cond_8
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 295
    invoke-virtual {v13}, Lio/realm/Realm;->close()V

    goto/16 :goto_0

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendUserInfo(JJ)V
    .locals 13
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J

    .prologue
    .line 97
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v6, v0, v1

    .local v6, "height":F
    move-object v1, p0

    move-wide v2, p1

    move-wide/from16 v4, p3

    .line 99
    invoke-direct/range {v1 .. v6}, Lcom/vectorwatch/android/service/FitnessClientService;->syncHeight(JJF)V

    .line 101
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v0

    int-to-float v12, v0

    .local v12, "weight":F
    move-object v7, p0

    move-wide v8, p1

    move-wide/from16 v10, p3

    .line 103
    invoke-direct/range {v7 .. v12}, Lcom/vectorwatch/android/service/FitnessClientService;->syncWeight(JJF)V

    .line 104
    return-void
.end method

.method private syncHeight(JJF)V
    .locals 15
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "height"    # F

    .prologue
    .line 115
    const/4 v3, 0x0

    cmpg-float v3, p5, v3

    if-gtz v3, :cond_0

    .line 116
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "syncHeight: Sync height in Google fit failed."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 147
    :goto_0
    return-void

    .line 120
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Syncing height."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 121
    new-instance v3, Lcom/google/android/gms/fitness/data/DataSource$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/fitness/data/DataSource$Builder;-><init>()V

    .line 122
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setAppPackageName(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->TYPE_HEIGHT:Lcom/google/android/gms/fitness/data/DataType;

    .line 123
    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setDataType(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    const-string v4, "VECTOR - height"

    .line 124
    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setStreamName(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    const/4 v4, 0x0

    .line 125
    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setType(I)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    .line 126
    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->build()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v10

    .line 128
    .local v10, "dataSource":Lcom/google/android/gms/fitness/data/DataSource;
    if-nez v10, :cond_1

    .line 129
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Null data source (first)."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_1
    invoke-static {v10}, Lcom/google/android/gms/fitness/data/DataSet;->create(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v9

    .line 135
    .local v9, "dataSet":Lcom/google/android/gms/fitness/data/DataSet;
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->getTimezoneOffset()I

    move-result v3

    int-to-long v12, v3

    .line 137
    .local v12, "offset":J
    invoke-virtual {v9}, Lcom/google/android/gms/fitness/data/DataSet;->createDataPoint()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v3

    add-long v4, p1, v12

    add-long v6, p3, v12

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/fitness/data/DataPoint;->setTimeInterval(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v2

    .line 139
    .local v2, "dataPoint":Lcom/google/android/gms/fitness/data/DataPoint;
    if-nez v2, :cond_2

    .line 140
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Null data point (height)."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :cond_2
    sget-object v3, Lcom/google/android/gms/fitness/data/Field;->FIELD_HEIGHT:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->getValue(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/data/Value;->setFloat(F)V

    .line 144
    invoke-virtual {v9, v2}, Lcom/google/android/gms/fitness/data/DataSet;->add(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 146
    invoke-direct {p0, v9}, Lcom/vectorwatch/android/service/FitnessClientService;->insertDataSetInGoogleFit(Lcom/google/android/gms/fitness/data/DataSet;)Z

    goto :goto_0
.end method

.method private syncWeight(JJF)V
    .locals 15
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "weight"    # F

    .prologue
    .line 158
    const/4 v3, 0x0

    cmpg-float v3, p5, v3

    if-gtz v3, :cond_0

    .line 159
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "syncHeight: Sync weight in Google fit failed."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 190
    :goto_0
    return-void

    .line 163
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Syncing weight."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 164
    new-instance v3, Lcom/google/android/gms/fitness/data/DataSource$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/fitness/data/DataSource$Builder;-><init>()V

    .line 165
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setAppPackageName(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->TYPE_WEIGHT:Lcom/google/android/gms/fitness/data/DataType;

    .line 166
    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setDataType(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    const-string v4, "VECTOR - weight"

    .line 167
    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setStreamName(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    const/4 v4, 0x0

    .line 168
    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->setType(I)Lcom/google/android/gms/fitness/data/DataSource$Builder;

    move-result-object v3

    .line 169
    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/DataSource$Builder;->build()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v10

    .line 171
    .local v10, "dataSource":Lcom/google/android/gms/fitness/data/DataSource;
    if-nez v10, :cond_1

    .line 172
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Null data source (weight)."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_1
    invoke-static {v10}, Lcom/google/android/gms/fitness/data/DataSet;->create(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v9

    .line 178
    .local v9, "dataSet":Lcom/google/android/gms/fitness/data/DataSet;
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->getTimezoneOffset()I

    move-result v3

    int-to-long v12, v3

    .line 180
    .local v12, "offset":J
    invoke-virtual {v9}, Lcom/google/android/gms/fitness/data/DataSet;->createDataPoint()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v3

    add-long v4, p1, v12

    add-long v6, p3, v12

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/fitness/data/DataPoint;->setTimeInterval(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v2

    .line 182
    .local v2, "dataPoint":Lcom/google/android/gms/fitness/data/DataPoint;
    if-nez v2, :cond_2

    .line 183
    sget-object v3, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v4, "FITNESS_API: Null data point (weight)."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 186
    :cond_2
    sget-object v3, Lcom/google/android/gms/fitness/data/Field;->FIELD_WEIGHT:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->getValue(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/data/Value;->setFloat(F)V

    .line 187
    invoke-virtual {v9, v2}, Lcom/google/android/gms/fitness/data/DataSet;->add(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 189
    invoke-direct {p0, v9}, Lcom/vectorwatch/android/service/FitnessClientService;->insertDataSetInGoogleFit(Lcom/google/android/gms/fitness/data/DataSet;)Z

    goto :goto_0
.end method

.method private triggerInteractionWithGoogleFit()V
    .locals 8

    .prologue
    .line 63
    sget-object v4, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-nez v4, :cond_0

    .line 64
    sget-object v4, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    const-string v5, "FITNESS_API: Need to sync activity to Fit, but the client is null."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/GoogleFitClientIsNullEvent;

    invoke-direct {v5}, Lcom/vectorwatch/android/events/GoogleFitClientIsNullEvent;-><init>()V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 89
    :goto_0
    return-void

    .line 73
    :cond_0
    sget-object v4, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v4}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 74
    invoke-direct {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->connectClient()V

    goto :goto_0

    .line 78
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/database/DatabaseManager;->getStartTimeForGoogleFitChanges(Landroid/content/Context;)J

    move-result-wide v2

    .line 79
    .local v2, "startTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v0, v4, v6

    .line 81
    .local v0, "endTime":J
    sget-object v4, Lcom/vectorwatch/android/service/FitnessClientService;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FITNESS_API: user info for start = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " end = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 82
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/vectorwatch/android/service/FitnessClientService;->sendUserInfo(JJ)V

    .line 83
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-direct {p0, v4}, Lcom/vectorwatch/android/service/FitnessClientService;->sendActivityData(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 84
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-direct {p0, v4}, Lcom/vectorwatch/android/service/FitnessClientService;->sendActivityData(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 85
    sget-object v4, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-direct {p0, v4}, Lcom/vectorwatch/android/service/FitnessClientService;->sendActivityData(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 87
    invoke-direct {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->disconnectClient()V

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vectorwatch/android/service/FitnessClientService;->triggerInteractionWithGoogleFit()V

    .line 55
    return-void
.end method
