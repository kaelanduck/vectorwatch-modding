.class public Lcom/vectorwatch/android/service/nls/NLService;
.super Landroid/service/notification/NotificationListenerService;
.source "NLService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    }
.end annotation


# static fields
.field public static final OPTION_PHONE_CALL:Ljava/lang/String; = "phone_call"

.field private static final log:Lorg/slf4j/Logger;

.field public static reference:Lcom/vectorwatch/android/service/nls/NLService;

.field private static sKeyguardManager:Landroid/app/KeyguardManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/vectorwatch/android/service/nls/NLService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/service/notification/NotificationListenerService;-><init>()V

    .line 752
    return-void
.end method

.method private getAppNotificationType(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 663
    .line 664
    invoke-static {p1}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v0

    .line 666
    .local v0, "supportedApp":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    return-object v0
.end method

.method private getCallerName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "tickerText"    # Ljava/lang/String;

    .prologue
    .line 670
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/nls/NLService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 671
    .local v0, "missedCallText":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private handleAppNotifications(Landroid/service/notification/StatusBarNotification;Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;Ljava/lang/String;)V
    .locals 20
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;
    .param p2, "appType"    # Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 418
    invoke-direct/range {p0 .. p1}, Lcom/vectorwatch/android/service/nls/NLService;->isNotificationValid(Landroid/service/notification/StatusBarNotification;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 419
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v3, "NOTIFICATIONS: invalid notification in handle app notification"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 426
    .local v5, "appTitle":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/nls/NLService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    .line 427
    .local v9, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/nls/NLService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 437
    .end local v9    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v13

    .line 438
    .local v13, "supportedApp":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    const/4 v4, 0x0

    .line 440
    .local v4, "containerId":Ljava/lang/String;
    :try_start_1
    invoke-virtual/range {p0 .. p1}, Lcom/vectorwatch/android/service/nls/NLService;->getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v4

    .line 446
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOTIFICATIONS: Missed social notification id:  "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 447
    const/4 v14, 0x0

    .line 449
    .local v14, "text":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$1;->$SwitchMap$com$vectorwatch$android$service$nls$NLService$SupportedAppsForNotifications:[I

    invoke-virtual {v13}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 588
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getOtherNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 592
    :cond_2
    :goto_2
    const/4 v12, 0x0

    .line 594
    .local v12, "isProgress":Z
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_3

    .line 595
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iget-object v11, v2, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 596
    .local v11, "extras":Landroid/os/Bundle;
    if-eqz v11, :cond_3

    .line 597
    const-string v2, "android.progressMax"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_3

    .line 598
    const/4 v12, 0x1

    .line 603
    .end local v11    # "extras":Landroid/os/Bundle;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iget-wide v2, v2, Landroid/app/Notification;->when:J

    const-wide/16 v16, 0x0

    cmp-long v2, v2, v16

    if-lez v2, :cond_15

    .line 604
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iget-wide v2, v2, Landroid/app/Notification;->when:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x1388

    sub-long v16, v16, v18

    cmp-long v2, v2, v16

    if-gez v2, :cond_15

    .line 605
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v3, "handleAppNotification: Notification is too old."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 428
    .end local v4    # "containerId":Ljava/lang/String;
    .end local v12    # "isProgress":Z
    .end local v13    # "supportedApp":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .end local v14    # "text":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 429
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOTIFICATIONS: Exception name not found "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 430
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 431
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v10

    .line 432
    .local v10, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOTIFICATIONS: General exception "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 433
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 441
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v4    # "containerId":Ljava/lang/String;
    .restart local v13    # "supportedApp":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    :catch_2
    move-exception v10

    .line 442
    .local v10, "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v3, "NOTIFICATIONS: Null status bar notification in app handling"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 451
    .end local v10    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    .restart local v14    # "text":Ljava/lang/String;
    :pswitch_0
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getOtherNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 452
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastSLACKMessage:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 453
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastSLACKMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerSlack:Landroid/os/Handler;

    if-eqz v2, :cond_4

    .line 454
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerSlack:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableSlack:Ljava/lang/Runnable;

    const-wide/16 v16, 0x3e8

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 457
    :cond_4
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastSLACKMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 460
    :cond_5
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastSLACKMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 464
    :pswitch_1
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getSkypeNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 465
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastGMAILMessage:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 466
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastGMAILMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 469
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastGMAILMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 472
    :cond_6
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastGMAILMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 476
    :pswitch_2
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getSkypeNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 477
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastKKOMessage:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 478
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastKKOMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerKKO:Landroid/os/Handler;

    if-eqz v2, :cond_7

    .line 479
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerKKO:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableKKO:Ljava/lang/Runnable;

    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 482
    :cond_7
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastKKOMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 485
    :cond_8
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastKKOMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 489
    :pswitch_3
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getSkypeNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 490
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastGKMessage:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 491
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastGKMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerGK:Landroid/os/Handler;

    if-eqz v2, :cond_9

    .line 492
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerGK:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableGK:Ljava/lang/Runnable;

    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 495
    :cond_9
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastGKMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 498
    :cond_a
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastGKMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 502
    :pswitch_4
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getSkypeNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 503
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastSkypeMessage:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 504
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastSkypeMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerSkype:Landroid/os/Handler;

    if-eqz v2, :cond_b

    .line 505
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerSkype:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableSkype:Ljava/lang/Runnable;

    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 508
    :cond_b
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastSkypeMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 511
    :cond_c
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastSkypeMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 515
    :pswitch_5
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getOtherNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 516
    goto/16 :goto_2

    .line 518
    :pswitch_6
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getHangoutsNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 519
    goto/16 :goto_2

    .line 521
    :pswitch_7
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getFacebookMessengerNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 522
    goto/16 :goto_2

    .line 524
    :pswitch_8
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getOtherNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 525
    goto/16 :goto_2

    .line 527
    :pswitch_9
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getWhatsappNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 528
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mLastWhatsAppMessage:Ljava/lang/String;

    if-eqz v2, :cond_e

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerWA:Landroid/os/Handler;

    if-eqz v2, :cond_e

    .line 529
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mLastWhatsAppMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 530
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerWA:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableWA:Ljava/lang/Runnable;

    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 533
    :cond_d
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->mLastWhatsAppMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 536
    :cond_e
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->mLastWhatsAppMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 540
    :pswitch_a
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getMailHtcNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 541
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    if-eqz v2, :cond_10

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerMail:Landroid/os/Handler;

    if-eqz v2, :cond_10

    .line 542
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 543
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerMail:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableMail:Ljava/lang/Runnable;

    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 546
    :cond_f
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 549
    :cond_10
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 553
    :pswitch_b
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getMailNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 554
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    if-eqz v2, :cond_12

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerMail:Landroid/os/Handler;

    if-eqz v2, :cond_12

    .line 555
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 556
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerMail:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableMail:Ljava/lang/Runnable;

    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 559
    :cond_11
    if-eqz v14, :cond_2

    .line 560
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 564
    :cond_12
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastMAILMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 568
    :pswitch_c
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getMailNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 569
    goto/16 :goto_2

    .line 571
    :pswitch_d
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getViberNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 572
    goto/16 :goto_2

    .line 574
    :pswitch_e
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getOtherNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v14

    .line 575
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastCalendarMessage:Ljava/lang/String;

    if-eqz v2, :cond_14

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerCalendar:Landroid/os/Handler;

    if-eqz v2, :cond_14

    .line 576
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sLastCalendarMessage:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerCalendar:Landroid/os/Handler;

    if-eqz v2, :cond_13

    .line 578
    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->mHandlerCalendar:Landroid/os/Handler;

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mRunnableCalendar:Ljava/lang/Runnable;

    const-wide/16 v16, 0x3e8

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 581
    :cond_13
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastCalendarMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 584
    :cond_14
    sput-object v14, Lcom/vectorwatch/android/VectorApplication;->sLastCalendarMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 609
    .restart local v12    # "isProgress":Z
    :cond_15
    if-eqz v14, :cond_17

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "null"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    if-nez v12, :cond_17

    .line 610
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iget-object v2, v2, Landroid/app/Notification;->sound:Landroid/net/Uri;

    if-nez v2, :cond_16

    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iget-object v2, v2, Landroid/app/Notification;->vibrate:[J

    if-nez v2, :cond_16

    .line 611
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/vectorwatch/android/service/nls/NLService;->hasSilentNotifications(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_16

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UNDEFINED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-eq v13, v2, :cond_17

    .line 613
    :cond_16
    invoke-static {v14}, Lcom/vdurmont/emoji/EmojiParser;->parseToAliases(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 615
    .local v6, "textWithEmoji":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$1;->$SwitchMap$com$vectorwatch$android$service$nls$NLService$SupportedAppsForNotifications:[I

    invoke-virtual {v13}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 622
    const/4 v3, 0x4

    .line 623
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v2, p0

    move-object/from16 v8, p1

    .line 622
    invoke-direct/range {v2 .. v8}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto/16 :goto_0

    .line 617
    :pswitch_f
    const-string v5, "Reminder"

    .line 618
    const/4 v3, 0x4

    const-string v7, "com.apple.reminders"

    move-object/from16 v2, p0

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v8}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto/16 :goto_0

    .line 627
    .end local v6    # "textWithEmoji":Ljava/lang/String;
    :cond_17
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOTIFICATIONS: The notification was not passed to VBL Service. "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 628
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v7

    iget-object v7, v7, Landroid/app/Notification;->sound:Landroid/net/Uri;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 629
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/vectorwatch/android/service/nls/NLService;->hasSilentNotifications(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 627
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 449
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 615
    :pswitch_data_1
    .packed-switch 0xf
        :pswitch_f
    .end packed-switch
.end method

.method private handleNotificationPosted(Landroid/service/notification/StatusBarNotification;)V
    .locals 8
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v5, 0x0

    .line 95
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: On notification posted."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 96
    const-string v3, "prefs_settings_contextual_notifications"

    invoke-static {v3, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 98
    .local v1, "isOptimizeNotifications":Z
    if-eqz v1, :cond_1

    .line 99
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->sKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->sKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v3

    if-nez v3, :cond_1

    .line 100
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Contextual isOptimizeNotifications = true"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->isNotificationValid(Landroid/service/notification/StatusBarNotification;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 106
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: invalid notification in onPost"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_2
    const-string v3, "pref_notification_display"

    const/4 v4, 0x1

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v3

    if-eqz v3, :cond_d

    .line 113
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 114
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-direct {p0, v3}, Lcom/vectorwatch/android/service/nls/NLService;->getAppNotificationType(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v0

    .line 120
    .local v0, "appNotificationType":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.wearable.app"

    .line 121
    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 122
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: is com.google.android.wearable.app"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-ne v0, v3, :cond_5

    .line 128
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS: Send Phone "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 130
    const-string v3, "phone_call"

    invoke-direct {p0, v3}, Lcom/vectorwatch/android/service/nls/NLService;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 131
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->handlePhoneNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 133
    :cond_4
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Application type phone_call is not enabled!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_5
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SMS_MMS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-ne v0, v3, :cond_7

    .line 137
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/nls/NLService;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 138
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->handleSmsMmsNotification(Landroid/service/notification/StatusBarNotification;)V

    .line 142
    :goto_1
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS: Send SMS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 140
    :cond_6
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Application type sms is not enabled!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 143
    :cond_7
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-ne v0, v3, :cond_9

    .line 144
    sget-boolean v3, Lcom/vectorwatch/android/VectorApplication;->mHandleGmailNotif:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mHandlerGMAIL:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 145
    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->mHandlerGMAIL:Landroid/os/Handler;

    sget-object v4, Lcom/vectorwatch/android/VectorApplication;->mRunnableGMAIL:Ljava/lang/Runnable;

    const-wide/16 v6, 0x2bc

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 146
    sput-boolean v5, Lcom/vectorwatch/android/VectorApplication;->mHandleGmailNotif:Z

    .line 148
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/nls/NLService;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 149
    invoke-direct {p0, p1, v0, v2}, Lcom/vectorwatch/android/service/nls/NLService;->handleAppNotifications(Landroid/service/notification/StatusBarNotification;Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 151
    :cond_8
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Application type gmail is not enabled!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    :cond_9
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NULL_PACKAGE_NAME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-eq v0, v3, :cond_b

    .line 155
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS: Send App "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/nls/NLService;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 159
    invoke-direct {p0, p1, v0, v2}, Lcom/vectorwatch/android/service/nls/NLService;->handleAppNotifications(Landroid/service/notification/StatusBarNotification;Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 161
    :cond_a
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Application type general app is not enabled!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 164
    :cond_b
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Null package name encountered and skipped handle notification."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 167
    .end local v0    # "appNotificationType":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_c
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v4, "NOTIFICATIONS: Watch is not connected!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 170
    :cond_d
    sget-object v3, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS: Warning! Notification is not processed. isNotifEnabled = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 171
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSystemNotification(Landroid/content/Context;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 170
    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handleNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
    .locals 9
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->isNotificationValid(Landroid/service/notification/StatusBarNotification;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: invalid notification in onRemoved"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSystemNotification(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 188
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/service/nls/NLService;->getAppNotificationType(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v7

    .line 190
    .local v7, "appNotificationType":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTIFICATIONS: Remove notif request for type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 192
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-ne v7, v0, :cond_2

    .line 194
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTIFICATIONS: Rem Phone remove "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 196
    const/4 v2, 0x0

    .line 199
    .local v2, "containerId":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 205
    const/4 v1, 0x7

    const-string v3, " not important"

    const-string v4, ""

    .line 206
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v6, p1

    .line 205
    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 200
    :catch_0
    move-exception v8

    .line 201
    .local v8, "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: null container id for phone. Remove."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 207
    .end local v2    # "containerId":Ljava/lang/String;
    .end local v8    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SMS_MMS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    if-ne v7, v0, :cond_3

    .line 209
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTIFICATIONS: Send SMS remove "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 211
    const/4 v2, 0x0

    .line 214
    .restart local v2    # "containerId":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 220
    const/4 v1, 0x3

    const-string v3, " not important"

    const-string v4, ""

    .line 221
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v6, p1

    .line 220
    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto/16 :goto_0

    .line 215
    :catch_1
    move-exception v8

    .line 216
    .restart local v8    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: null container id for sms. Remove."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 223
    .end local v2    # "containerId":Ljava/lang/String;
    .end local v8    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTIFICATIONS: Send App remove "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 226
    const/4 v2, 0x0

    .line 229
    .restart local v2    # "containerId":Ljava/lang/String;
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v2

    .line 235
    const/4 v1, 0x5

    const-string v3, " not important"

    const-string v4, ""

    .line 236
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v6, p1

    .line 235
    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto/16 :goto_0

    .line 230
    :catch_2
    move-exception v8

    .line 231
    .restart local v8    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: null container id for supported app. Remove."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handlePhoneNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 18
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 300
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/vectorwatch/android/service/nls/NLService;->getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 306
    .local v3, "containerId":Ljava/lang/String;
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOTIFICATIONS: Missed call sId:  "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 308
    invoke-direct/range {p0 .. p1}, Lcom/vectorwatch/android/service/nls/NLService;->isNotificationValid(Landroid/service/notification/StatusBarNotification;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 309
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: invalid notification in handle phone notification"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 350
    .end local v3    # "containerId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v14

    .line 302
    .local v14, "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null status bar notification exception at handle phone notification."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 313
    .end local v14    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    .restart local v3    # "containerId":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    iget-object v0, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    .line 314
    .local v17, "tickerText":Ljava/lang/CharSequence;
    if-eqz v17, :cond_2

    .line 315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 317
    .local v5, "tickerTextString":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vectorwatch/android/service/nls/NLService;->getCallerName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 318
    .local v4, "callerName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/vectorwatch/android/service/nls/NLService;->treatUnknownCaller(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 322
    const/4 v2, 0x6

    .line 323
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p0

    move-object/from16 v7, p1

    .line 322
    invoke-direct/range {v1 .. v7}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 327
    .end local v4    # "callerName":Ljava/lang/String;
    .end local v5    # "tickerTextString":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOTIFICATIONS: Missed call null package name = "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 330
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.server.telecom"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 331
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.dialer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getGroupKey()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getGroupKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MissedCallNotifier"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 335
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    iget-object v15, v1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 336
    .local v15, "extras":Landroid/os/Bundle;
    if-eqz v15, :cond_0

    .line 337
    const-string v1, "android.text"

    invoke-virtual {v15, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v13

    .line 338
    .local v13, "charSequence":Ljava/lang/CharSequence;
    if-eqz v13, :cond_0

    .line 339
    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 340
    .local v9, "text":Ljava/lang/String;
    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 341
    .local v16, "textLines":[Ljava/lang/String;
    if-eqz v16, :cond_0

    move-object/from16 v0, v16

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v1, v16, v1

    if-eqz v1, :cond_0

    .line 342
    const/4 v7, 0x6

    .line 343
    invoke-virtual/range {p1 .. p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v6, p0

    move-object v8, v3

    move-object v10, v9

    move-object/from16 v12, p1

    .line 342
    invoke-direct/range {v6 .. v12}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto/16 :goto_0
.end method

.method private handleSmsMmsNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 8
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 389
    const/4 v2, 0x0

    .line 391
    .local v2, "containerId":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 397
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->isNotificationValid(Landroid/service/notification/StatusBarNotification;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: invalid notification in handle sms"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 414
    :goto_0
    return-void

    .line 392
    :catch_0
    move-exception v7

    .line 393
    .local v7, "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: Null status bar notification in handle SMS "

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 402
    .end local v7    # "e":Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS: Missed sms sId:  "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 403
    const-string v3, "SMS"

    .line 404
    .local v3, "appTitle":Ljava/lang/String;
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS: App title "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 406
    invoke-static {p1}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getSmsNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v4

    .line 408
    .local v4, "text":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 409
    const/4 v1, 0x2

    .line 410
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v6, p1

    .line 409
    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/service/nls/NLService;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 412
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: text is null!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private hasSilentNotifications(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 641
    .line 642
    invoke-static {p1}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v0

    .line 643
    .local v0, "supportedApp":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$1;->$SwitchMap$com$vectorwatch$android$service$nls$NLService$SupportedAppsForNotifications:[I

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 652
    const/4 v1, 0x0

    :sswitch_0
    return v1

    .line 643
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x10 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method private isEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "appPackageName"    # Ljava/lang/String;

    .prologue
    .line 271
    invoke-static {p1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSupportedAppNotification(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    const/4 v0, 0x1

    .line 275
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNotificationValid(Landroid/service/notification/StatusBarNotification;)Z
    .locals 3
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v0, 0x0

    .line 1360
    if-nez p1, :cond_0

    .line 1361
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Received notification with null status bar notification."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1375
    :goto_0
    return v0

    .line 1365
    :cond_0
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1366
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Received notification with null package name."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1370
    :cond_1
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1371
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Received notification with null null notification."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1375
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isUnsavedNumber(Ljava/lang/String;)Z
    .locals 4
    .param p1, "caller"    # Ljava/lang/String;

    .prologue
    .line 376
    const-string v2, "[a-zA-Z]"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 377
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 379
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 380
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v3, "NOTIFICATIONS: Caller name contains letters."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 381
    const/4 v2, 0x0

    .line 385
    :goto_0
    return v2

    .line 384
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v3, "NOTIFICATIONS: Caller name does not contain letters."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 385
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;)V
    .locals 11
    .param p1, "type"    # S
    .param p2, "containerId"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;
    .param p6, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 676
    if-nez p6, :cond_0

    .line 677
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null statusBarNotification at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 747
    :goto_0
    return-void

    .line 681
    :cond_0
    if-nez p2, :cond_1

    .line 682
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null containerId at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 686
    :cond_1
    if-nez p3, :cond_2

    .line 687
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null title at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 691
    :cond_2
    if-nez p4, :cond_3

    .line 692
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null message at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 696
    :cond_3
    if-nez p5, :cond_4

    .line 697
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null packagename at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 702
    :cond_4
    const/4 v1, 0x7

    if-eq p1, v1, :cond_6

    const/16 v1, 0x9

    if-eq p1, v1, :cond_6

    const/4 v1, 0x1

    if-eq p1, v1, :cond_6

    const/4 v1, 0x3

    if-eq p1, v1, :cond_6

    const/4 v1, 0x5

    if-eq p1, v1, :cond_6

    const-string v1, ""

    .line 707
    invoke-virtual {p4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "null"

    invoke-virtual {p4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 708
    :cond_5
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendToVBLService: Error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 712
    :cond_6
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/nls/NLService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 713
    .local v9, "context":Landroid/content/Context;
    new-instance v10, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v10, v9, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 717
    .local v10, "intentService":Landroid/content/Intent;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_8

    .line 718
    invoke-virtual/range {p6 .. p6}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v7

    .line 719
    .local v7, "tag":Ljava/lang/String;
    if-nez v7, :cond_7

    .line 720
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null tag in statusBarNotification at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 721
    const-string v7, ""

    .line 724
    :cond_7
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .line 725
    invoke-virtual/range {p6 .. p6}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v6

    const-string v8, ""

    move-object v1, p2

    move v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 743
    .local v0, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :goto_1
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Send notification to BLE!"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 745
    const-string v1, "notification_payload"

    invoke-virtual {v10, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 746
    invoke-virtual {v9, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 727
    .end local v0    # "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .end local v7    # "tag":Ljava/lang/String;
    :cond_8
    invoke-virtual/range {p6 .. p6}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v7

    .line 728
    .restart local v7    # "tag":Ljava/lang/String;
    if-nez v7, :cond_9

    .line 729
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null tag in statusBarNotification at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 730
    const-string v7, ""

    .line 733
    :cond_9
    invoke-virtual/range {p6 .. p6}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v8

    .line 734
    .local v8, "key":Ljava/lang/String;
    if-nez v8, :cond_a

    .line 735
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Null key in statusBarNotification at send to BLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 736
    const-string v8, ""

    .line 739
    :cond_a
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .line 740
    invoke-virtual/range {p6 .. p6}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v6

    move-object v1, p2

    move v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    goto :goto_1
.end method

.method private treatUnknownCaller(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "caller"    # Ljava/lang/String;

    .prologue
    .line 361
    const-string v0, ""

    .line 362
    .local v0, "callerFinal":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->isUnsavedNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 363
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 364
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 369
    .end local v1    # "i":I
    :cond_1
    move-object v0, p1

    .line 372
    :cond_2
    return-object v0
.end method

# Prevent notification from being removed from the phone when dismissed from the watch if the setting isn't set

# virtual methods
.method public cancelNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 4
    .param p1, "notification"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue

    const-string v0, "flag_dismiss_from_phone"
    const/4 v1, 0x1

    # SharedPreferencesHelper.getBooleanPreference("flag_dismiss_from_phone", true, (Context)this);
    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z
    move-result v0

    # if (v0 == false) { return; }

    if-nez v0, :yes_dismiss_notification
      return-void

    :yes_dismiss_notification

    .line 243
    if-nez p1, :cond_0

    .line 244
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Cancel notification: Notification is null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 268
    :goto_0
    return-void

    .line 248
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_3

    .line 249
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnTag()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 250
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Cancel notification: Notification has null information"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    invoke-virtual {v1, p1}, Lcom/vectorwatch/android/service/nls/NLService;->cancelNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    goto :goto_0

    .line 256
    :cond_3
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnKey()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 257
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Cancel notification: Notification has stbnKey null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_4
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTIFICATIONS: Notifications Container : dismiss with key"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 263
    :try_start_0
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/service/nls/NLService;->cancelNotification(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NOTIFICATIONS: cancel notif fail on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getContainerId(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 4
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;
        }
    .end annotation

    .prologue
    .line 279
    if-nez p1, :cond_0

    .line 280
    new-instance v1, Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;

    invoke-direct {v1}, Lcom/vectorwatch/android/exceptions/NullStatusBarNotificationException;-><init>()V

    throw v1

    .line 283
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 286
    .local v0, "containerId":Ljava/lang/String;
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifications GROUP_KEY:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getGroupKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "|KEY:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "|TAG:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 287
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 286
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 292
    :goto_0
    return-object v0

    .line 289
    .end local v0    # "containerId":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "containerId":Ljava/lang/String;
    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Landroid/service/notification/NotificationListenerService;->onCreate()V

    .line 46
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v1, "NOTIFICATIONS: NL Service has started"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 47
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/nls/NLService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService;->sKeyguardManager:Landroid/app/KeyguardManager;

    .line 48
    sput-object p0, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    .line 49
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0}, Landroid/service/notification/NotificationListenerService;->onDestroy()V

    .line 54
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.vectorwatch.android.NOTIFICATION_RESTART"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/nls/NLService;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: NL Service destroyed"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public onNotificationPosted(Landroid/service/notification/StatusBarNotification;)V
    .locals 3
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 69
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->handleNotificationPosted(Landroid/service/notification/StatusBarNotification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 72
    const-string v1, "NOTIFICATIONS: Something went wrong with the new posted notification."

    invoke-static {v1}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    .line 73
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Something went wrong with the new posted notification."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
    .locals 3
    .param p1, "statusBarNotification"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 80
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->handleNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 83
    const-string v1, "NOTIFICATIONS: Something went wrong with the removed notification."

    invoke-static {v1}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    .line 84
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: Something went wrong with the removed notification."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3}, Landroid/service/notification/NotificationListenerService;->onStartCommand(Landroid/content/Intent;II)I

    .line 63
    const/4 v0, 0x1

    return v0
.end method
