.class public Lcom/vectorwatch/android/service/nls/ToolbarNotifications;
.super Ljava/lang/Object;
.source "ToolbarNotifications.java"


# static fields
.field public static final VBL_SERVICE_NOTIFICATION_ID:I = 0x1e240


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)Landroid/app/Notification;
    .locals 6
    .param p0, "updateString"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "iconId"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "activityIntent"    # Ljava/lang/Class;

    .prologue
    .line 27
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v4, p3}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {v4, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    .line 29
    invoke-virtual {v4, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

	# set priority to -2 (PRIORITY_MIN)
	const/4 v0, -0x2
	invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;
	move-result-object v4

    .line 30
    invoke-virtual {v4, p0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 34
    .local v0, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p3, p4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v1, "notificationIntent":Landroid/content/Intent;
    invoke-static {p3}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v3

    .line 37
    .local v3, "stackBuilder":Landroid/app/TaskStackBuilder;
    invoke-virtual {v3, p4}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    .line 39
    invoke-virtual {v3, v1}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    .line 40
    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-virtual {v3, v4, v5}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v2

    .line 41
    .local v2, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 42
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    return-object v4
.end method

.method public static updateVectorNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)V
    .locals 3
    .param p0, "updateContent"    # Ljava/lang/String;
    .param p1, "updateTitle"    # Ljava/lang/String;
    .param p2, "iconId"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "activityIntent"    # Ljava/lang/Class;

    .prologue
    .line 55
    invoke-static {p0, p1, p2, p3, p4}, Lcom/vectorwatch/android/service/nls/ToolbarNotifications;->buildNotification(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;Ljava/lang/Class;)Landroid/app/Notification;

    move-result-object v1

    .line 57
    .local v1, "notification":Landroid/app/Notification;
    const-string v2, "notification"

    invoke-virtual {p3, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 61
    .local v0, "mNotificationManager":Landroid/app/NotificationManager;
    const v2, 0x1e240

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 64
    return-void
.end method
