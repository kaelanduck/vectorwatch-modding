.class public Lcom/vectorwatch/android/service/nls/NotificationHandler;
.super Ljava/lang/Object;
.source "NotificationHandler.java"


# static fields
.field private static final LINE_SEPARATOR:Ljava/lang/String; = "line.separator"

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/service/nls/NotificationHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/nls/NotificationHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFacebookMessengerNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 11
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v8, 0x0

    .line 26
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    if-nez v9, :cond_1

    :cond_0
    move-object v4, v8

    .line 76
    :goto_0
    return-object v4

    .line 30
    :cond_1
    const-string v7, ""

    .line 31
    .local v7, "title":Ljava/lang/String;
    const/4 v5, 0x0

    .line 32
    .local v5, "text":Ljava/lang/String;
    const/4 v4, 0x0

    .line 34
    .local v4, "fullText":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v6, v9, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 36
    .local v6, "tickerText":Ljava/lang/CharSequence;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x13

    if-lt v9, v10, :cond_8

    .line 39
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v3, v9, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 41
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_4

    .line 42
    const-string v9, "android.title"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 43
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v9, "android.text"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 44
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v9, "android.textLines"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 46
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_2

    .line 47
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 50
    :cond_2
    const-string v9, "heads active"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 51
    sget-object v9, Lcom/vectorwatch/android/service/nls/NotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v10, "Contains new chat text"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    move-object v4, v8

    .line 52
    goto :goto_0

    .line 55
    :cond_3
    if-eqz v0, :cond_6

    .line 57
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 64
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_4
    :goto_1
    if-eqz v5, :cond_5

    const-string v8, "null"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 65
    :cond_5
    move-object v4, v7

    goto :goto_0

    .line 58
    .restart local v0    # "extraText":Ljava/lang/CharSequence;
    .restart local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .restart local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_6
    if-eqz v1, :cond_4

    array-length v8, v1

    if-lez v8, :cond_4

    .line 60
    array-length v8, v1

    add-int/lit8 v8, v8, -0x1

    aget-object v8, v1, v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 67
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_7
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 72
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_8
    if-eqz v6, :cond_9

    .line 73
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_9
    move-object v4, v5

    .line 76
    goto/16 :goto_0
.end method

.method public static getHangoutsNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 12
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 81
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    if-nez v10, :cond_2

    .line 82
    :cond_0
    const/4 v6, 0x0

    .line 135
    :cond_1
    :goto_0
    return-object v6

    .line 85
    :cond_2
    const-string v9, ""

    .line 86
    .local v9, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 89
    .local v6, "text":Ljava/lang/String;
    const/4 v5, 0x0

    .line 91
    .local v5, "moreConversationsActive":Z
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-object v8, v10, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 93
    .local v8, "tickerText":Ljava/lang/CharSequence;
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x13

    if-lt v10, v11, :cond_7

    .line 96
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-object v3, v10, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 98
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_6

    .line 99
    const-string v10, "android.title"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 100
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v10, "android.text"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 101
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v10, "android.textLines"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 103
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3

    .line 104
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 107
    :cond_3
    const-string v10, "new message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 108
    const/4 v5, 0x1

    .line 112
    :cond_4
    if-nez v5, :cond_5

    if-eqz v0, :cond_5

    .line 113
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 114
    const-string v10, "line.separator"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 115
    .local v7, "textLines":[Ljava/lang/String;
    array-length v10, v7

    if-lez v10, :cond_5

    .line 116
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, v7

    add-int/lit8 v11, v11, -0x1

    aget-object v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .local v4, "fullText":Ljava/lang/String;
    move-object v6, v4

    .line 117
    goto :goto_0

    .line 122
    .end local v4    # "fullText":Ljava/lang/String;
    .end local v7    # "textLines":[Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_6

    if-eqz v1, :cond_6

    array-length v10, v1

    if-lez v10, :cond_6

    .line 123
    const/4 v10, 0x0

    aget-object v10, v1, v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 124
    goto/16 :goto_0

    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_6
    move-object v6, v9

    .line 129
    goto/16 :goto_0

    .line 131
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_7
    if-eqz v8, :cond_1

    .line 132
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0
.end method

.method public static getMailHtcNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 12
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v11, 0x0

    .line 334
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    if-nez v9, :cond_2

    .line 335
    :cond_0
    const/4 v5, 0x0

    .line 387
    :cond_1
    :goto_0
    return-object v5

    .line 338
    :cond_2
    const-string v8, ""

    .line 339
    .local v8, "title":Ljava/lang/String;
    const/4 v5, 0x0

    .line 341
    .local v5, "text":Ljava/lang/String;
    const/4 v4, 0x0

    .line 343
    .local v4, "moreConversationsActive":Z
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v7, v9, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 345
    .local v7, "tickerText":Ljava/lang/CharSequence;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x13

    if-lt v9, v10, :cond_7

    .line 348
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v3, v9, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 350
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_6

    .line 351
    const-string v9, "android.title"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 352
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v9, "android.bigText"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 353
    .local v0, "extraBigText":Ljava/lang/CharSequence;
    const-string v9, "android.textLines"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 355
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3

    .line 356
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 359
    :cond_3
    const-string v9, "1 new"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 360
    const/4 v4, 0x1

    .line 364
    :cond_4
    if-nez v4, :cond_5

    if-eqz v0, :cond_5

    .line 365
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 366
    const-string v9, "line.separator"

    invoke-static {v9}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 368
    .local v6, "textLines":[Ljava/lang/String;
    array-length v9, v6

    if-lez v9, :cond_5

    .line 369
    aget-object v5, v6, v11

    .line 370
    goto :goto_0

    .line 375
    .end local v6    # "textLines":[Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v9, v9, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    if-eqz v9, :cond_6

    if-eqz v1, :cond_6

    array-length v9, v1

    if-lez v9, :cond_6

    .line 376
    aget-object v9, v1, v11

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 377
    goto :goto_0

    .end local v0    # "extraBigText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_6
    move-object v5, v8

    .line 381
    goto :goto_0

    .line 383
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_7
    if-eqz v7, :cond_1

    .line 384
    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public static getMailNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 13
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v12, 0x0

    .line 392
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    if-nez v10, :cond_2

    .line 393
    :cond_0
    const/4 v4, 0x0

    .line 450
    :cond_1
    :goto_0
    return-object v4

    .line 396
    :cond_2
    const-string v9, ""

    .line 397
    .local v9, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 398
    .local v6, "text":Ljava/lang/String;
    const/4 v4, 0x0

    .line 399
    .local v4, "fullText":Ljava/lang/String;
    const/4 v5, 0x0

    .line 401
    .local v5, "moreConversationsActive":Z
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-object v8, v10, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 403
    .local v8, "tickerText":Ljava/lang/CharSequence;
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x13

    if-lt v10, v11, :cond_9

    .line 406
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-object v3, v10, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 408
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_6

    .line 409
    const-string v10, "android.title"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 410
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v10, "android.text"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 411
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v10, "android.textLines"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 413
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3

    .line 414
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 417
    :cond_3
    const-string v10, "new message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "new email"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 418
    :cond_4
    const/4 v5, 0x1

    .line 421
    :cond_5
    if-nez v5, :cond_7

    if-eqz v0, :cond_7

    .line 423
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 424
    const-string v10, "line.separator"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 425
    .local v7, "textLines":[Ljava/lang/String;
    array-length v10, v7

    if-lez v10, :cond_6

    .line 426
    aget-object v6, v7, v12

    .line 434
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    .end local v7    # "textLines":[Ljava/lang/String;
    :cond_6
    :goto_1
    if-nez v5, :cond_8

    .line 437
    if-eqz v9, :cond_1

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    if-eqz v6, :cond_1

    const-string v10, ""

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 438
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 428
    .restart local v0    # "extraText":Ljava/lang/CharSequence;
    .restart local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .restart local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_7
    if-eqz v5, :cond_6

    if-eqz v1, :cond_6

    array-length v10, v1

    if-lez v10, :cond_6

    .line 430
    aget-object v10, v1, v12

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 441
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_8
    move-object v4, v6

    goto/16 :goto_0

    .line 446
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_9
    if-eqz v8, :cond_a

    .line 447
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_a
    move-object v4, v6

    .line 450
    goto/16 :goto_0
.end method

.method public static getOtherNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 10
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 456
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    if-nez v8, :cond_2

    .line 457
    :cond_0
    const/4 v4, 0x0

    .line 505
    :cond_1
    :goto_0
    return-object v4

    .line 460
    :cond_2
    const-string v7, ""

    .line 461
    .local v7, "title":Ljava/lang/String;
    const/4 v5, 0x0

    .line 462
    .local v5, "text":Ljava/lang/String;
    const/4 v4, 0x0

    .line 464
    .local v4, "fullText":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iget-object v6, v8, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 466
    .local v6, "tickerText":Ljava/lang/CharSequence;
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x13

    if-lt v8, v9, :cond_9

    .line 469
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iget-object v3, v8, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 471
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_4

    .line 472
    const-string v8, "android.title"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 473
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v8, "android.text"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 474
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v8, "android.textLines"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 476
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3

    .line 477
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 480
    :cond_3
    if-eqz v0, :cond_5

    .line 481
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 496
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_4
    :goto_1
    if-eqz v5, :cond_1

    const-string v8, ""

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 497
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 482
    .restart local v0    # "extraText":Ljava/lang/CharSequence;
    .restart local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .restart local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_5
    if-eqz v1, :cond_6

    array-length v8, v1

    if-lez v8, :cond_6

    .line 483
    array-length v8, v1

    add-int/lit8 v8, v8, -0x1

    aget-object v8, v1, v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 485
    :cond_6
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iget-object v8, v8, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    if-eqz v8, :cond_7

    .line 486
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v9, v9, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 490
    :cond_7
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 491
    :cond_8
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    invoke-static {v8}, Lcom/vectorwatch/android/service/nls/NotificationHandler;->getTextFromNotification(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 501
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_9
    if-eqz v6, :cond_a

    .line 502
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_a
    move-object v4, v5

    .line 505
    goto/16 :goto_0
.end method

.method public static getSkypeNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 11
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 140
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    if-nez v9, :cond_2

    .line 141
    :cond_0
    const/4 v6, 0x0

    .line 191
    :cond_1
    :goto_0
    return-object v6

    .line 144
    :cond_2
    const-string v8, ""

    .line 145
    .local v8, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 147
    .local v6, "text":Ljava/lang/String;
    const/4 v5, 0x0

    .line 149
    .local v5, "moreConversationsActive":Z
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v7, v9, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 151
    .local v7, "tickerText":Ljava/lang/CharSequence;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x13

    if-lt v9, v10, :cond_6

    .line 154
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v3, v9, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 156
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_1

    .line 157
    const-string v9, "android.title"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 158
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v9, "android.text"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 159
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v9, "android.textLines"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 161
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3

    .line 162
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 165
    :cond_3
    const-string v9, "new message"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 166
    const/4 v5, 0x1

    .line 170
    :cond_4
    if-nez v5, :cond_5

    if-eqz v0, :cond_5

    .line 171
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 172
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .local v4, "fullText":Ljava/lang/String;
    move-object v6, v4

    .line 173
    goto :goto_0

    .line 177
    .end local v4    # "fullText":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 187
    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_6
    if-eqz v7, :cond_1

    .line 188
    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public static getSmsNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 3
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 512
    const/4 v0, 0x0

    .line 514
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iget-object v1, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 516
    .local v1, "tickerText":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 517
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 527
    :cond_0
    return-object v0
.end method

.method private static getTextFromNotification(Landroid/app/Notification;)Ljava/lang/String;
    .locals 13
    .param p0, "notification"    # Landroid/app/Notification;

    .prologue
    const/4 v8, 0x0

    .line 531
    if-nez p0, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-object v8

    .line 536
    :cond_1
    iget-object v9, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 538
    .local v9, "views":Landroid/widget/RemoteViews;
    if-nez v9, :cond_2

    .line 539
    iget-object v9, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 542
    :cond_2
    if-eqz v9, :cond_0

    .line 549
    const-string v8, ""

    .line 551
    .local v8, "text":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v10

    const-string v11, "mActions"

    invoke-virtual {v10, v11}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 552
    .local v2, "field":Ljava/lang/reflect/Field;
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 555
    invoke-virtual {v2, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 558
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    .line 559
    .local v4, "p":Landroid/os/Parcelable;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    .line 560
    .local v5, "parcel":Landroid/os/Parcel;
    const/4 v10, 0x0

    invoke-interface {v4, v5, v10}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    .line 561
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 565
    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 566
    .local v7, "tag":I
    const/4 v10, 0x2

    if-ne v7, v10, :cond_3

    .line 569
    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    .line 570
    invoke-virtual {v5}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 571
    .local v3, "methodName":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 575
    const-string v10, "setText"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 577
    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    .line 580
    sget-object v10, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, v5}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 581
    sget-object v10, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, v5}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 582
    .local v6, "t":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 585
    .end local v6    # "t":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 590
    .end local v0    # "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v3    # "methodName":Ljava/lang/String;
    .end local v4    # "p":Landroid/os/Parcelable;
    .end local v5    # "parcel":Landroid/os/Parcel;
    .end local v7    # "tag":I
    :catch_0
    move-exception v1

    .line 591
    .local v1, "e":Ljava/lang/Exception;
    sget-object v10, Lcom/vectorwatch/android/service/nls/NotificationHandler;->log:Lorg/slf4j/Logger;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static getViberNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 11
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 196
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    if-nez v9, :cond_1

    .line 197
    :cond_0
    const/4 v4, 0x0

    .line 252
    :goto_0
    return-object v4

    .line 200
    :cond_1
    const-string v8, ""

    .line 201
    .local v8, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 203
    .local v6, "text":Ljava/lang/String;
    const/4 v5, 0x0

    .line 205
    .local v5, "moreConversationsActive":Z
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v7, v9, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 207
    .local v7, "tickerText":Ljava/lang/CharSequence;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x13

    if-lt v9, v10, :cond_7

    .line 210
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    iget-object v3, v9, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 212
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_6

    .line 213
    const-string v9, "android.title"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 214
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v9, "android.text"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 215
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v9, "android.textLines"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 217
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_2

    .line 218
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 221
    :cond_2
    invoke-static {v8}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->appNameInList(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 222
    const/4 v5, 0x1

    .line 226
    :cond_3
    if-nez v5, :cond_4

    if-eqz v0, :cond_4

    .line 227
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 228
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "fullText":Ljava/lang/String;
    goto :goto_0

    .line 233
    .end local v4    # "fullText":Ljava/lang/String;
    :cond_4
    if-nez v5, :cond_5

    if-eqz v1, :cond_5

    array-length v9, v1

    if-lez v9, :cond_5

    .line 234
    const/4 v9, 0x0

    aget-object v9, v1, v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 235
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 236
    .restart local v4    # "fullText":Ljava/lang/String;
    goto/16 :goto_0

    .line 240
    .end local v4    # "fullText":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_6

    if-eqz v7, :cond_6

    .line 241
    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v4, v6

    .line 242
    goto/16 :goto_0

    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_6
    move-object v4, v8

    .line 246
    goto/16 :goto_0

    .line 248
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_7
    if-eqz v7, :cond_8

    .line 249
    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_8
    move-object v4, v6

    .line 252
    goto/16 :goto_0
.end method

.method public static getWhatsappNotificationText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 12
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v9, 0x0

    .line 257
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    if-nez v10, :cond_2

    :cond_0
    move-object v4, v9

    .line 328
    :cond_1
    :goto_0
    return-object v4

    .line 261
    :cond_2
    const-string v8, ""

    .line 262
    .local v8, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 264
    .local v6, "text":Ljava/lang/String;
    const/4 v5, 0x0

    .line 266
    .local v5, "moreConversationsActive":Z
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-object v7, v10, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 268
    .local v7, "tickerText":Ljava/lang/CharSequence;
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x13

    if-lt v10, v11, :cond_c

    .line 271
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-object v3, v10, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 273
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_b

    .line 274
    const-string v10, "android.title"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 275
    .local v2, "extraTitle":Ljava/lang/CharSequence;
    const-string v10, "android.text"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 276
    .local v0, "extraText":Ljava/lang/CharSequence;
    const-string v10, "android.textLines"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 278
    .local v1, "extraTextLines":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3

    .line 279
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 282
    :cond_3
    const-string v10, "message"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 283
    const/4 v5, 0x1

    .line 287
    :cond_4
    if-nez v5, :cond_6

    if-eqz v0, :cond_6

    .line 288
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 289
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 290
    .local v4, "fullText":Ljava/lang/String;
    const-string v10, "new messages"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    const-string v10, "message from"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    const-string v10, "messages from"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_5
    move-object v4, v9

    .line 291
    goto :goto_0

    .line 297
    .end local v4    # "fullText":Ljava/lang/String;
    :cond_6
    if-nez v5, :cond_8

    if-eqz v1, :cond_8

    array-length v10, v1

    if-lez v10, :cond_8

    .line 298
    array-length v10, v1

    add-int/lit8 v10, v10, -0x1

    aget-object v10, v1, v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 299
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 300
    .restart local v4    # "fullText":Ljava/lang/String;
    const-string v10, "new messages"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "message from"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "messages from"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_7
    move-object v4, v9

    .line 301
    goto/16 :goto_0

    .line 307
    .end local v4    # "fullText":Ljava/lang/String;
    :cond_8
    if-eqz v5, :cond_b

    if-eqz v1, :cond_b

    array-length v10, v1

    if-lez v10, :cond_b

    .line 308
    array-length v10, v1

    add-int/lit8 v10, v10, -0x1

    aget-object v10, v1, v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 309
    const-string v10, "new messages"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    const-string v10, "message from"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    const-string v10, "messages from"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    :cond_9
    move-object v4, v9

    .line 310
    goto/16 :goto_0

    :cond_a
    move-object v4, v6

    .line 312
    goto/16 :goto_0

    .end local v0    # "extraText":Ljava/lang/CharSequence;
    .end local v1    # "extraTextLines":[Ljava/lang/CharSequence;
    .end local v2    # "extraTitle":Ljava/lang/CharSequence;
    :cond_b
    move-object v4, v8

    .line 317
    goto/16 :goto_0

    .line 319
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_c
    if-eqz v7, :cond_d

    .line 320
    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 323
    :cond_d
    if-eqz v6, :cond_f

    const-string v10, "new messages"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "message from"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "messages from"

    .line 324
    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_f

    :cond_e
    move-object v4, v9

    .line 325
    goto/16 :goto_0

    :cond_f
    move-object v4, v6

    .line 328
    goto/16 :goto_0
.end method
