.class public final enum Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
.super Ljava/lang/Enum;
.source "NLService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/nls/NLService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SupportedAppsForNotifications"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ACCUWEATHER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum AIRBNB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ALDIKO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum AMAZONKINDLE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum AMAZONSHOPPING:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ANYDO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ASANA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum BLOGGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum BLOODBROTHERS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum BMWCONNECTED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum BOOKING:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum BOOMBEACH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CALORIESCOUNTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CANDYCRUSH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CANDYCRUSHSODA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CASTLECASH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CITYMAPPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CLASHOFCLANS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CLASHOFKINGS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum CLUE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum DEERHUNTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum DEEZER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum DIGG:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum DOMINATIONS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum DROPBOX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum DUOLINGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EBAY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EBOOKREADER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EIGHTPOOL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EMAIL_HTC_CLIENT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EMPIRE4K:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ESPN:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ESPNFANTASYFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ESPNFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum EVERNOTE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FACEBOOK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FACEBOOK_MESSENGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FANDANGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FANTASYFOOTBALLMANAGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FARMHEROESSAGA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FEEDLY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FIFA16:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FLICKR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FLIGHTSTATS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FLIPBOARD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FLIXSTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FMH2015:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum FMH2016:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GALAXYLEGEND:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GAMEOFWARFIREAGE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GATEGURU:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOAL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOALLIVESCORES:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLEDOCS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLEDRIVE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLEFIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLEKEEP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLENEWS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLEPLAYBOOKS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLESHEETS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLESLIDES:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GOOGLE_PLUS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum GPSNAVMAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum HANGOUTS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum HAYDAY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum HIPMUNK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum HOTWIRE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum IMDB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum IMO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum INSTAGRAM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum INSTAPAPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum JNJ7MINWORK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum KAKAOTALK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum KAYAK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum KOBOBOOKS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum LINE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum LINKEDIN:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum LIVESCORE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MAGICRUSH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MAPFITNESS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MEETME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MINECRAFT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MINT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MKX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MLB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MOMONDO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MYCALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MYDAYS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum MYDAYSX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum NASAAPP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum NFL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum NFLFANTASYFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum NULL_PACKAGE_NAME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum OFFLINEMAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum OOVOO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum P7MINWORK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PATH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PATHTALK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PAYPAL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PEDOMETER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PERIODTRACKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PERIODTRACKERPRO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PETRESCUE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PINTEREST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PIRATEKINGS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PIXELPHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PIXWORDS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum POCKET:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum POU:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PVZ:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum PVZ2:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum QUORA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum REDDIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum RUNKEEPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum RUNTASTIC:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum RUNTASTICPRO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SKYPE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SKYSCANNER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SLACK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SMS_MMS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SNAPCHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SPOTIFY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum STRAVA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum STUBHUB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SUMMONERSWAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SUNRISECALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum SWARM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TALKINGANGELA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TALKINGTOM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TANGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TASKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TEAMSTREAM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TESLA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum THEWEATHERCHANNEL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TINDER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TODOIST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TOP11:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TRIPADVISOR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TRIPCASE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TRIPIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TRIVAGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TUMBLR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum TWITTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum UBER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum UBREADER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum UNDEFINED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum UNTAPPD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum VIBER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum VINE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum VK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum VKCHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WATTPAD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WAZE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WEATHERCLOCK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WEBMD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WEBMDALLERGY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WECHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WHATSAPP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WICKR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum WOTBLITZ:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum YAHOOWEATHER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum YOUTUBE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ZOMBIEHARVEST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

.field public static final enum ZYNGAPOKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;


# instance fields
.field private final fElementNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 754
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CALENDAR"

    new-instance v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$1;

    invoke-direct {v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$1;-><init>()V

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 759
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PHONE"

    new-instance v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$2;

    invoke-direct {v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$2;-><init>()V

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 765
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SMS_MMS"

    new-instance v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$3;

    invoke-direct {v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$3;-><init>()V

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SMS_MMS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 772
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EMAIL_HTC_CLIENT"

    new-instance v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$4;

    invoke-direct {v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$4;-><init>()V

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EMAIL_HTC_CLIENT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 775
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EMAIL"

    new-instance v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$5;

    invoke-direct {v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$5;-><init>()V

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 781
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SKYPE"

    const/4 v2, 0x5

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$6;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$6;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SKYPE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 784
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLE_PLUS"

    const/4 v2, 0x6

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$7;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$7;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLE_PLUS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 787
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FACEBOOK"

    const/4 v2, 0x7

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$8;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$8;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FACEBOOK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 790
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "HANGOUTS"

    const/16 v2, 0x8

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$9;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$9;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HANGOUTS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 793
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FACEBOOK_MESSENGER"

    const/16 v2, 0x9

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$10;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$10;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FACEBOOK_MESSENGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 796
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "LINKEDIN"

    const/16 v2, 0xa

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$11;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$11;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->LINKEDIN:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 799
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "INSTAGRAM"

    const/16 v2, 0xb

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$12;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$12;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->INSTAGRAM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 802
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WHATSAPP"

    const/16 v2, 0xc

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$13;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$13;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WHATSAPP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 805
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GMAIL"

    const/16 v2, 0xd

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$14;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$14;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 808
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "VIBER"

    const/16 v2, 0xe

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$15;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$15;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VIBER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 811
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TUMBLR"

    const/16 v2, 0xf

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$16;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$16;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TUMBLR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 814
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TWITTER"

    const/16 v2, 0x10

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$17;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$17;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TWITTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 817
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PINTEREST"

    const/16 v2, 0x11

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$18;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$18;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PINTEREST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 820
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "BLOGGER"

    const/16 v2, 0x12

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$19;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$19;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BLOGGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 823
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "YOUTUBE"

    const/16 v2, 0x13

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$20;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$20;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->YOUTUBE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 826
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WECHAT"

    const/16 v2, 0x14

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$21;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$21;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WECHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 829
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FLIXSTER"

    const/16 v2, 0x15

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$22;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$22;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLIXSTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 832
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "IMDB"

    const/16 v2, 0x16

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$23;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$23;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->IMDB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 835
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TODOIST"

    const/16 v2, 0x17

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$24;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$24;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TODOIST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 838
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ANYDO"

    const/16 v2, 0x18

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$25;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$25;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ANYDO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 841
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLEKEEP"

    const/16 v2, 0x19

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$26;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$26;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEKEEP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 844
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EVERNOTE"

    const/16 v2, 0x1a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$27;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$27;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EVERNOTE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 847
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLEDOCS"

    const/16 v2, 0x1b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$28;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$28;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEDOCS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 850
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLESHEETS"

    const/16 v2, 0x1c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$29;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$29;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLESHEETS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 853
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLESLIDES"

    const/16 v2, 0x1d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$30;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$30;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLESLIDES:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 856
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLEDRIVE"

    const/16 v2, 0x1e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$31;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$31;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEDRIVE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 859
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "DROPBOX"

    const/16 v2, 0x1f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$32;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$32;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DROPBOX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 862
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TASKER"

    const/16 v2, 0x20

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$33;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$33;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TASKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 865
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MYCALENDAR"

    const/16 v2, 0x21

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$34;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$34;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MYCALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 868
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PERIODTRACKER"

    const/16 v2, 0x22

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$35;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$35;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PERIODTRACKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 871
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PERIODTRACKERPRO"

    const/16 v2, 0x23

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$36;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$36;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PERIODTRACKERPRO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 874
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CLUE"

    const/16 v2, 0x24

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$37;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$37;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CLUE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 877
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MYDAYS"

    const/16 v2, 0x25

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$38;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$38;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MYDAYS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 880
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MYDAYSX"

    const/16 v2, 0x26

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$39;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$39;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MYDAYSX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 883
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FLICKR"

    const/16 v2, 0x27

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$40;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$40;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLICKR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 886
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "VINE"

    const/16 v2, 0x28

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$41;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$41;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VINE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 889
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "LINE"

    const/16 v2, 0x29

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$42;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$42;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->LINE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 892
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WICKR"

    const/16 v2, 0x2a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$43;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$43;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WICKR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 895
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SPOTIFY"

    const/16 v2, 0x2b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$44;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$44;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SPOTIFY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 898
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "DUOLINGO"

    const/16 v2, 0x2c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$45;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$45;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DUOLINGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 901
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ESPN"

    const/16 v2, 0x2d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$46;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$46;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ESPN:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 904
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ESPNFANTASYFOOTBALL"

    const/16 v2, 0x2e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$47;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$47;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ESPNFANTASYFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 907
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "NFLFANTASYFOOTBALL"

    const/16 v2, 0x2f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$48;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$48;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NFLFANTASYFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 910
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FANTASYFOOTBALLMANAGER"

    const/16 v2, 0x30

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$49;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$49;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FANTASYFOOTBALLMANAGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 913
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FMH2015"

    const/16 v2, 0x31

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$50;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$50;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FMH2015:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 916
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FMH2016"

    const/16 v2, 0x32

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$51;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$51;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FMH2016:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 919
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SLACK"

    const/16 v2, 0x33

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$52;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$52;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SLACK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 922
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TINDER"

    const/16 v2, 0x34

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$53;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$53;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TINDER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 925
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SNAPCHAT"

    const/16 v2, 0x35

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$54;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$54;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SNAPCHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 928
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TEAMSTREAM"

    const/16 v2, 0x36

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$55;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$55;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TEAMSTREAM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 931
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "NASAAPP"

    const/16 v2, 0x37

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$56;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$56;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NASAAPP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 934
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "UNTAPPD"

    const/16 v2, 0x38

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$57;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$57;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UNTAPPD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 937
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MINT"

    const/16 v2, 0x39

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$58;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$58;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MINT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 940
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PAYPAL"

    const/16 v2, 0x3a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$59;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$59;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PAYPAL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 943
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EBAY"

    const/16 v2, 0x3b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$60;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$60;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EBAY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 946
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "AMAZONSHOPPING"

    const/16 v2, 0x3c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$61;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$61;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->AMAZONSHOPPING:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 949
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "JNJ7MINWORK"

    const/16 v2, 0x3d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$62;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$62;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->JNJ7MINWORK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 952
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "P7MINWORK"

    const/16 v2, 0x3e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$63;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$63;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->P7MINWORK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 955
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CALORIESCOUNTER"

    const/16 v2, 0x3f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$64;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$64;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CALORIESCOUNTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 958
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MAPFITNESS"

    const/16 v2, 0x40

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$65;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$65;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MAPFITNESS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 961
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLEFIT"

    const/16 v2, 0x41

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$66;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$66;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEFIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 964
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PEDOMETER"

    const/16 v2, 0x42

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$67;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$67;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PEDOMETER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 967
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "RUNKEEPER"

    const/16 v2, 0x43

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$68;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$68;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->RUNKEEPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 970
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "RUNTASTIC"

    const/16 v2, 0x44

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$69;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$69;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->RUNTASTIC:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 973
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "RUNTASTICPRO"

    const/16 v2, 0x45

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$70;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$70;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->RUNTASTICPRO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 976
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WEBMD"

    const/16 v2, 0x46

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$71;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$71;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WEBMD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 979
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WEBMDALLERGY"

    const/16 v2, 0x47

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$72;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$72;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WEBMDALLERGY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 982
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "STRAVA"

    const/16 v2, 0x48

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$73;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$73;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->STRAVA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 985
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "DIGG"

    const/16 v2, 0x49

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$74;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$74;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DIGG:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 988
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "REDDIT"

    const/16 v2, 0x4a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$75;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$75;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->REDDIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 991
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FEEDLY"

    const/16 v2, 0x4b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$76;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$76;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FEEDLY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 994
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "INSTAPAPER"

    const/16 v2, 0x4c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$77;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$77;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->INSTAPAPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 997
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "POCKET"

    const/16 v2, 0x4d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$78;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$78;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->POCKET:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1000
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FLIPBOARD"

    const/16 v2, 0x4e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$79;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$79;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLIPBOARD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1003
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLENEWS"

    const/16 v2, 0x4f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$80;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$80;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLENEWS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1006
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ACCUWEATHER"

    const/16 v2, 0x50

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$81;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$81;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ACCUWEATHER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1009
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "YAHOOWEATHER"

    const/16 v2, 0x51

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$82;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$82;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->YAHOOWEATHER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1012
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WEATHERCLOCK"

    const/16 v2, 0x52

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$83;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$83;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WEATHERCLOCK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1015
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "THEWEATHERCHANNEL"

    const/16 v2, 0x53

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$84;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$84;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->THEWEATHERCHANNEL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1018
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "AMAZONKINDLE"

    const/16 v2, 0x54

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$85;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$85;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->AMAZONKINDLE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1021
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WATTPAD"

    const/16 v2, 0x55

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$86;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$86;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WATTPAD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1024
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EBOOKREADER"

    const/16 v2, 0x56

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$87;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$87;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EBOOKREADER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1027
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "KOBOBOOKS"

    const/16 v2, 0x57

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$88;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$88;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->KOBOBOOKS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1030
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOOGLEPLAYBOOKS"

    const/16 v2, 0x58

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$89;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$89;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEPLAYBOOKS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1033
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ALDIKO"

    const/16 v2, 0x59

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$90;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$90;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ALDIKO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1036
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "UBREADER"

    const/16 v2, 0x5a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$91;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$91;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UBREADER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1039
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GATEGURU"

    const/16 v2, 0x5b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$92;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$92;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GATEGURU:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1042
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FLIGHTSTATS"

    const/16 v2, 0x5c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$93;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$93;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLIGHTSTATS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1045
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "UBER"

    const/16 v2, 0x5d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$94;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$94;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UBER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1048
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TRIPADVISOR"

    const/16 v2, 0x5e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$95;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$95;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIPADVISOR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1051
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "BOOKING"

    const/16 v2, 0x5f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$96;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$96;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BOOKING:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1054
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TRIVAGO"

    const/16 v2, 0x60

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$97;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$97;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIVAGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1057
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WAZE"

    const/16 v2, 0x61

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$98;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$98;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WAZE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1060
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MAPS"

    const/16 v2, 0x62

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$99;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$99;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1063
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "OFFLINEMAPS"

    const/16 v2, 0x63

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$100;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$100;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->OFFLINEMAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1066
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GPSNAVMAPS"

    const/16 v2, 0x64

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$101;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$101;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GPSNAVMAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1069
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CITYMAPPER"

    const/16 v2, 0x65

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$102;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$102;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CITYMAPPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1072
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "HIPMUNK"

    const/16 v2, 0x66

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$103;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$103;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HIPMUNK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1075
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SKYSCANNER"

    const/16 v2, 0x67

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$104;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$104;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SKYSCANNER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1078
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MOMONDO"

    const/16 v2, 0x68

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$105;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$105;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MOMONDO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1081
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "KAYAK"

    const/16 v2, 0x69

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$106;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$106;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->KAYAK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1084
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "HOTWIRE"

    const/16 v2, 0x6a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$107;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$107;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HOTWIRE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1087
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "STUBHUB"

    const/16 v2, 0x6b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$108;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$108;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->STUBHUB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1090
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TRIPCASE"

    const/16 v2, 0x6c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$109;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$109;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIPCASE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1093
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TRIPIT"

    const/16 v2, 0x6d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$110;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$110;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIPIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1096
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "AIRBNB"

    const/16 v2, 0x6e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$111;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$111;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->AIRBNB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1099
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MLB"

    const/16 v2, 0x6f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$112;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$112;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MLB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1102
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "NFL"

    const/16 v2, 0x70

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$113;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$113;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NFL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1105
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ESPNFOOTBALL"

    const/16 v2, 0x71

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$114;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$114;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ESPNFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1108
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "LIVESCORE"

    const/16 v2, 0x72

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$115;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$115;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->LIVESCORE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1111
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOALLIVESCORES"

    const/16 v2, 0x73

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$116;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$116;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOALLIVESCORES:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1114
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GOAL"

    const/16 v2, 0x74

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$117;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$117;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOAL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1117
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FANDANGO"

    const/16 v2, 0x75

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$118;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$118;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FANDANGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1120
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TESLA"

    const/16 v2, 0x76

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$119;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$119;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TESLA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1123
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "BMWCONNECTED"

    const/16 v2, 0x77

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$120;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$120;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BMWCONNECTED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1126
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SWARM"

    const/16 v2, 0x78

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$121;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$121;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SWARM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1129
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "DEEZER"

    const/16 v2, 0x79

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$122;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$122;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DEEZER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1132
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "KAKAOTALK"

    const/16 v2, 0x7a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$123;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$123;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->KAKAOTALK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1135
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TANGO"

    const/16 v2, 0x7b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$124;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$124;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TANGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1138
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "OOVOO"

    const/16 v2, 0x7c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$125;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$125;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->OOVOO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1141
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "IMO"

    const/16 v2, 0x7d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$126;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$126;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->IMO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1144
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MEETME"

    const/16 v2, 0x7e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$127;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$127;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MEETME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1147
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ASANA"

    const/16 v2, 0x7f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$128;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$128;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ASANA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1150
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "QUORA"

    const/16 v2, 0x80

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$129;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$129;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->QUORA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1153
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TED"

    const/16 v2, 0x81

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$130;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$130;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1156
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SUNRISECALENDAR"

    const/16 v2, 0x82

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$131;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$131;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SUNRISECALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1159
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "VK"

    const/16 v2, 0x83

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$132;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$132;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1162
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "VKCHAT"

    const/16 v2, 0x84

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$133;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$133;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VKCHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1165
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PATH"

    const/16 v2, 0x85

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$134;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$134;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PATH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1168
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PATHTALK"

    const/16 v2, 0x86

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$135;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$135;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PATHTALK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1173
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CLASHOFCLANS"

    const/16 v2, 0x87

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$136;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$136;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CLASHOFCLANS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1176
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CLASHOFKINGS"

    const/16 v2, 0x88

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$137;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$137;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CLASHOFKINGS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1179
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CANDYCRUSH"

    const/16 v2, 0x89

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$138;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$138;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CANDYCRUSH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1182
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CANDYCRUSHSODA"

    const/16 v2, 0x8a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$139;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$139;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CANDYCRUSHSODA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1185
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EMPIRE4K"

    const/16 v2, 0x8b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$140;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$140;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EMPIRE4K:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1188
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TOP11"

    const/16 v2, 0x8c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$141;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$141;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TOP11:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1191
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "CASTLECASH"

    const/16 v2, 0x8d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$142;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$142;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CASTLECASH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1194
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MAGICRUSH"

    const/16 v2, 0x8e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$143;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$143;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MAGICRUSH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1197
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "HAYDAY"

    const/16 v2, 0x8f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$144;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$144;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HAYDAY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1200
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "BOOMBEACH"

    const/16 v2, 0x90

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$145;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$145;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BOOMBEACH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1203
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PVZ"

    const/16 v2, 0x91

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$146;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$146;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PVZ:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1206
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PIRATEKINGS"

    const/16 v2, 0x92

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$147;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$147;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PIRATEKINGS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1209
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GALAXYLEGEND"

    const/16 v2, 0x93

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$148;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$148;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GALAXYLEGEND:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1212
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FARMHEROESSAGA"

    const/16 v2, 0x94

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$149;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$149;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FARMHEROESSAGA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1215
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "GAMEOFWARFIREAGE"

    const/16 v2, 0x95

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$150;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$150;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GAMEOFWARFIREAGE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1218
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "DOMINATIONS"

    const/16 v2, 0x96

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$151;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$151;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DOMINATIONS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1221
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "SUMMONERSWAR"

    const/16 v2, 0x97

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$152;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$152;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SUMMONERSWAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1224
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "BLOODBROTHERS"

    const/16 v2, 0x98

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$153;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$153;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BLOODBROTHERS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1227
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PIXWORDS"

    const/16 v2, 0x99

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$154;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$154;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PIXWORDS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1230
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MINECRAFT"

    const/16 v2, 0x9a

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$155;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$155;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MINECRAFT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1233
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "EIGHTPOOL"

    const/16 v2, 0x9b

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$156;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$156;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EIGHTPOOL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1236
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "MKX"

    const/16 v2, 0x9c

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$157;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$157;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MKX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1239
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "WOTBLITZ"

    const/16 v2, 0x9d

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$158;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$158;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WOTBLITZ:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1242
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PETRESCUE"

    const/16 v2, 0x9e

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$159;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$159;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PETRESCUE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1245
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ZYNGAPOKER"

    const/16 v2, 0x9f

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$160;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$160;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ZYNGAPOKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1248
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TALKINGTOM"

    const/16 v2, 0xa0

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$161;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$161;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TALKINGTOM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1251
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "TALKINGANGELA"

    const/16 v2, 0xa1

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$162;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$162;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TALKINGANGELA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1254
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "POU"

    const/16 v2, 0xa2

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$163;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$163;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->POU:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1257
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "FIFA16"

    const/16 v2, 0xa3

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$164;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$164;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FIFA16:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1260
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "DEERHUNTER"

    const/16 v2, 0xa4

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$165;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$165;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DEERHUNTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1263
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PVZ2"

    const/16 v2, 0xa5

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$166;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$166;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PVZ2:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1266
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "ZOMBIEHARVEST"

    const/16 v2, 0xa6

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$167;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$167;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ZOMBIEHARVEST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1269
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "PIXELPHONE"

    const/16 v2, 0xa7

    new-instance v3, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$168;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications$168;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PIXELPHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1273
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "UNDEFINED"

    const/16 v2, 0xa8

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UNDEFINED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1274
    new-instance v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    const-string v1, "NULL_PACKAGE_NAME"

    const/16 v2, 0xa9

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NULL_PACKAGE_NAME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 752
    const/16 v0, 0xaa

    new-array v0, v0, [Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SMS_MMS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EMAIL_HTC_CLIENT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SKYPE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLE_PLUS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FACEBOOK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HANGOUTS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FACEBOOK_MESSENGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->LINKEDIN:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->INSTAGRAM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WHATSAPP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GMAIL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VIBER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TUMBLR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TWITTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PINTEREST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BLOGGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->YOUTUBE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WECHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLIXSTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->IMDB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TODOIST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ANYDO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEKEEP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EVERNOTE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEDOCS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLESHEETS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLESLIDES:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEDRIVE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DROPBOX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TASKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MYCALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PERIODTRACKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PERIODTRACKERPRO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CLUE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MYDAYS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MYDAYSX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLICKR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VINE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->LINE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WICKR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SPOTIFY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DUOLINGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ESPN:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ESPNFANTASYFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NFLFANTASYFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FANTASYFOOTBALLMANAGER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FMH2015:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FMH2016:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SLACK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TINDER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SNAPCHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TEAMSTREAM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NASAAPP:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UNTAPPD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MINT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PAYPAL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EBAY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->AMAZONSHOPPING:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->JNJ7MINWORK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->P7MINWORK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CALORIESCOUNTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MAPFITNESS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEFIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PEDOMETER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->RUNKEEPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->RUNTASTIC:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->RUNTASTICPRO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WEBMD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WEBMDALLERGY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->STRAVA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DIGG:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->REDDIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FEEDLY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->INSTAPAPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->POCKET:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLIPBOARD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLENEWS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ACCUWEATHER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->YAHOOWEATHER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WEATHERCLOCK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->THEWEATHERCHANNEL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->AMAZONKINDLE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WATTPAD:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EBOOKREADER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->KOBOBOOKS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOOGLEPLAYBOOKS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ALDIKO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UBREADER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GATEGURU:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FLIGHTSTATS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UBER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIPADVISOR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BOOKING:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIVAGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WAZE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->OFFLINEMAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GPSNAVMAPS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CITYMAPPER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HIPMUNK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SKYSCANNER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MOMONDO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->KAYAK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HOTWIRE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->STUBHUB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIPCASE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TRIPIT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->AIRBNB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MLB:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NFL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ESPNFOOTBALL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->LIVESCORE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOALLIVESCORES:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GOAL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FANDANGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TESLA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BMWCONNECTED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SWARM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DEEZER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->KAKAOTALK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TANGO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->OOVOO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->IMO:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MEETME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ASANA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->QUORA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SUNRISECALENDAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->VKCHAT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PATH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PATHTALK:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CLASHOFCLANS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CLASHOFKINGS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CANDYCRUSH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CANDYCRUSHSODA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EMPIRE4K:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TOP11:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->CASTLECASH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MAGICRUSH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->HAYDAY:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BOOMBEACH:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PVZ:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PIRATEKINGS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GALAXYLEGEND:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FARMHEROESSAGA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->GAMEOFWARFIREAGE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DOMINATIONS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->SUMMONERSWAR:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->BLOODBROTHERS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PIXWORDS:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MINECRAFT:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->EIGHTPOOL:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->MKX:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->WOTBLITZ:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PETRESCUE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ZYNGAPOKER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TALKINGTOM:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->TALKINGANGELA:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->POU:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->FIFA16:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->DEERHUNTER:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PVZ2:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->ZOMBIEHARVEST:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->PIXELPHONE:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UNDEFINED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NULL_PACKAGE_NAME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->$VALUES:[Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1278
    .local p3, "stringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1279
    iput-object p3, p0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->fElementNameList:Ljava/util/List;

    .line 1280
    return-void
.end method

.method public static appNameInList(Ljava/lang/String;)Z
    .locals 6
    .param p0, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1309
    if-nez p0, :cond_1

    .line 1319
    :cond_0
    :goto_0
    return v1

    .line 1313
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->values()[Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 1314
    .local v0, "app":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->name()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1315
    const/4 v1, 0x1

    goto :goto_0

    .line 1313
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .locals 5
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 1289
    if-nez p0, :cond_1

    .line 1290
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->NULL_PACKAGE_NAME:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    .line 1299
    :cond_0
    :goto_0
    return-object v0

    .line 1293
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->values()[Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 1294
    .local v0, "app":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->containedIn(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1293
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1299
    .end local v0    # "app":Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->UNDEFINED:Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 752
    const-class v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;
    .locals 1

    .prologue
    .line 752
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->$VALUES:[Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;

    return-object v0
.end method


# virtual methods
.method public containedIn(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1330
    if-nez p1, :cond_1

    .line 1341
    :cond_0
    :goto_0
    return v2

    .line 1334
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->fElementNameList:Ljava/util/List;

    .line 1336
    .local v0, "appNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1337
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1338
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1345
    const-string v1, ""

    .line 1346
    .local v1, "namesCombination":Ljava/lang/String;
    iget-object v2, p0, Lcom/vectorwatch/android/service/nls/NLService$SupportedAppsForNotifications;->fElementNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1347
    .local v0, "elementName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1348
    goto :goto_0

    .line 1349
    .end local v0    # "elementName":Ljava/lang/String;
    :cond_0
    return-object v1
.end method
