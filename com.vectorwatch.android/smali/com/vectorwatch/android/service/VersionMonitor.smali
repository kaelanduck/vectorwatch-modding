.class public Lcom/vectorwatch/android/service/VersionMonitor;
.super Ljava/lang/Object;
.source "VersionMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vectorwatch/android/service/VersionMonitor;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method public static handleCompatibleVersions(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 121
    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v2, "COMPATIBILITY: FLAG COMPAT false at compatible versions"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 124
    const-string v1, "check_compatibility"

    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    const-string v1, "check_compatibility"

    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 127
    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v2, "MAIN_ACTIVITY"

    const-string v3, "Version Monitor - handleCompatibleVersions"

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 128
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    .local v0, "updateIntent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 130
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 131
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 132
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 135
    .end local v0    # "updateIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static handleIncompatibleOlderAppVersion(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 150
    const-string v2, "mandatory_app_update"

    invoke-static {v2, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 152
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    .local v1, "updateIntent":Landroid/content/Intent;
    sget-object v2, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v3, "TEST - incompatible app v"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 154
    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 155
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 156
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 157
    const-string v2, "force_ota"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 160
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "force_app_update"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 164
    return-void
.end method

.method public static handleIncompatibleOlderWatchVersion(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 138
    const-string v1, "mandatory_kernel_update"

    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 139
    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v2, "TEST - incompatible watch v"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 140
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 141
    .local v0, "updateIntent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 142
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 143
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 144
    const-string v1, "force_ota"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 147
    return-void
.end method

.method public static handleResponseFromServer(Lcom/vectorwatch/android/events/CloudMessageModel;)V
    .locals 4
    .param p0, "cloudMessage"    # Lcom/vectorwatch/android/events/CloudMessageModel;

    .prologue
    .line 95
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "message":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 98
    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v2, "COMPATIBILITY RESPONSE - ALREADY UP TO DATE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 99
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;

    sget-object v3, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    invoke-direct {v2, v3, v0}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;-><init>(Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 117
    :cond_0
    :goto_1
    return-void

    .line 95
    .end local v0    # "message":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    .restart local v0    # "message":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->APP_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 105
    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v2, "COMPATIBILITY RESPONSE - APP UPDATE AVAILABLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;

    sget-object v3, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_APP_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    invoke-direct {v2, v3, v0}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;-><init>(Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_1

    .line 111
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 112
    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v2, "COMPATIBILITY RESPONSE - WATCH UPDATE AVAILABLE"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;

    sget-object v3, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_WATCH_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    invoke-direct {v2, v3, v0}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;-><init>(Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static startCompatibilityChecks(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-object v2, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v3, "Compatibility test"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "last_known_system_info"

    const-class v4, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 50
    invoke-virtual {v2, v3, v4}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 53
    .local v0, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-nez v0, :cond_0

    .line 54
    sget-object v2, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v3, "REQUEST - get system info at start compat checks"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 55
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 92
    :goto_0
    return-void

    .line 59
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Compatibility - last know vos details = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "kernel"

    invoke-static {v0, v4}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 62
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getModelForVosUpdateRequest(Landroid/content/Context;)Lcom/vectorwatch/android/models/WatchOsDetailsModel;

    move-result-object v1

    .line 64
    .local v1, "watchOsDetailsModel":Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    if-nez v1, :cond_1

    .line 65
    sget-object v2, Lcom/vectorwatch/android/service/VersionMonitor;->log:Lorg/slf4j/Logger;

    const-string v3, "CHECK COMPATIBILITY - null OS details object for cloud request is null"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 66
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    goto :goto_0

    .line 70
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 71
    invoke-static {p0, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->checkCompatibilityOffline(Landroid/content/Context;Lcom/vectorwatch/com/android/vos/update/SystemInfo;)Z

    goto :goto_0

    .line 73
    :cond_2
    const-string v2, "summary"

    new-instance v3, Lcom/vectorwatch/android/service/VersionMonitor$1;

    invoke-direct {v3}, Lcom/vectorwatch/android/service/VersionMonitor$1;-><init>()V

    invoke-static {p0, v1, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveUpdateDetails(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method public static startConnectionSyncs(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-static {p0}, Lcom/vectorwatch/android/service/VersionMonitor;->handleCompatibleVersions(Landroid/content/Context;)V

    .line 44
    return-void
.end method
