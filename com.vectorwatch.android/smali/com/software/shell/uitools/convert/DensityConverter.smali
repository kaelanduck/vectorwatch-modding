.class public final Lcom/software/shell/uitools/convert/DensityConverter;
.super Ljava/lang/Object;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/uitools/convert/DensityConverter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/uitools/convert/DensityConverter;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calculateDensityScaleFactor(Landroid/content/Context;)F
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sget-object v1, Lcom/software/shell/uitools/convert/DensityConverter;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Density scale factor is: {}"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method public static dpToPx(Landroid/content/Context;F)F
    .locals 5

    invoke-static {p0}, Lcom/software/shell/uitools/convert/DensityConverter;->calculateDensityScaleFactor(Landroid/content/Context;)F

    move-result v0

    mul-float/2addr v0, p1

    sget-object v1, Lcom/software/shell/uitools/convert/DensityConverter;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Density-independent value: {} converted to real pixel value: {}"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return v0
.end method

.method public static pxToDp(Landroid/content/Context;F)F
    .locals 5

    invoke-static {p0}, Lcom/software/shell/uitools/convert/DensityConverter;->calculateDensityScaleFactor(Landroid/content/Context;)F

    move-result v0

    div-float v0, p1, v0

    sget-object v1, Lcom/software/shell/uitools/convert/DensityConverter;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Real pixel value: {} converted to density-independent value: {}"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return v0
.end method
