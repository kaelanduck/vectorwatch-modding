.class public final Lcom/software/shell/uitools/resutils/id/IdGenerator;
.super Ljava/lang/Object;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;

.field private static final NEXT_ID:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/software/shell/uitools/resutils/id/IdGenerator;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/uitools/resutils/id/IdGenerator;->LOGGER:Lorg/slf4j/Logger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/software/shell/uitools/resutils/id/IdGenerator;->NEXT_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static next()I
    .locals 4

    :cond_0
    sget-object v0, Lcom/software/shell/uitools/resutils/id/IdGenerator;->NEXT_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    const v2, 0xffffff

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    sget-object v2, Lcom/software/shell/uitools/resutils/id/IdGenerator;->NEXT_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/software/shell/uitools/resutils/id/IdGenerator;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Next generated ID is: {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v1
.end method
