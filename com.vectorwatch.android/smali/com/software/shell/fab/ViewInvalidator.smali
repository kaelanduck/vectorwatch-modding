.class Lcom/software/shell/fab/ViewInvalidator;
.super Ljava/lang/Object;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# instance fields
.field private invalidationDelay:J

.field private invalidationDelayedRequired:Z

.field private invalidationRequired:Z

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/fab/ViewInvalidator;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/fab/ViewInvalidator;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/software/shell/fab/ViewInvalidator;->view:Landroid/view/View;

    return-void
.end method

.method private reset()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationRequired:Z

    iput-boolean v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationDelayedRequired:Z

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/software/shell/fab/ViewInvalidator;->setInvalidationDelay(J)V

    sget-object v0, Lcom/software/shell/fab/ViewInvalidator;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Reset the view invalidator configuration"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method getInvalidationDelay()J
    .locals 2

    iget-wide v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationDelay:J

    return-wide v0
.end method

.method invalidate()V
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ViewInvalidator;->isInvalidationRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/software/shell/fab/ViewInvalidator;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    sget-object v0, Lcom/software/shell/fab/ViewInvalidator;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Called view invalidation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ViewInvalidator;->isInvalidationDelayedRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/software/shell/fab/ViewInvalidator;->view:Landroid/view/View;

    invoke-virtual {p0}, Lcom/software/shell/fab/ViewInvalidator;->getInvalidationDelay()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->postInvalidateDelayed(J)V

    sget-object v0, Lcom/software/shell/fab/ViewInvalidator;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Called view delayed invalidation. Delay time is: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ViewInvalidator;->getInvalidationDelay()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    invoke-direct {p0}, Lcom/software/shell/fab/ViewInvalidator;->reset()V

    return-void
.end method

.method isInvalidationDelayedRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationDelayedRequired:Z

    return v0
.end method

.method isInvalidationRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationRequired:Z

    return v0
.end method

.method requireDelayedInvalidation()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationDelayedRequired:Z

    sget-object v0, Lcom/software/shell/fab/ViewInvalidator;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set delayed invalidation required"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method requireInvalidation()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationRequired:Z

    sget-object v0, Lcom/software/shell/fab/ViewInvalidator;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set invalidation required"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method setInvalidationDelay(J)V
    .locals 1

    iput-wide p1, p0, Lcom/software/shell/fab/ViewInvalidator;->invalidationDelay:J

    return-void
.end method
