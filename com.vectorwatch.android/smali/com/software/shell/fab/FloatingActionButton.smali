.class public Lcom/software/shell/fab/FloatingActionButton;
.super Lcom/software/shell/fab/ActionButton;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/fab/FloatingActionButton;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/fab/FloatingActionButton;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0, p1}, Lcom/software/shell/fab/ActionButton;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/software/shell/fab/ActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/software/shell/fab/FloatingActionButton;->initActionButton(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/software/shell/fab/ActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/software/shell/fab/FloatingActionButton;->initActionButton(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/software/shell/fab/ActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/software/shell/fab/FloatingActionButton;->initActionButton(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private initActionButton(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/software/shell/fab/R$styleable;->ActionButton:[I

    invoke-virtual {v0, p2, v1, p3, p4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/software/shell/fab/FloatingActionButton;->initType(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/FloatingActionButton;->initShowAnimation(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/FloatingActionButton;->initHideAnimation(Landroid/content/res/TypedArray;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    sget-object v0, Lcom/software/shell/fab/FloatingActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Floating Action Button"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v2, Lcom/software/shell/fab/FloatingActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "Failed to read the attribute"

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private initHideAnimation(Landroid/content/res/TypedArray;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_animation_onHide:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_animation_onHide:I

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->NONE:Lcom/software/shell/fab/ActionButton$Animations;

    iget v1, v1, Lcom/software/shell/fab/ActionButton$Animations;->animResId:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/FloatingActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/software/shell/fab/ActionButton$Animations;->load(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/FloatingActionButton;->setHideAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private initShowAnimation(Landroid/content/res/TypedArray;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_animation_onShow:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_animation_onShow:I

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->NONE:Lcom/software/shell/fab/ActionButton$Animations;

    iget v1, v1, Lcom/software/shell/fab/ActionButton$Animations;->animResId:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/FloatingActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/software/shell/fab/ActionButton$Animations;->load(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/FloatingActionButton;->setShowAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private initType(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_type:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_type:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-static {v0}, Lcom/software/shell/fab/ActionButton$Type;->forId(I)Lcom/software/shell/fab/ActionButton$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/FloatingActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    sget-object v0, Lcom/software/shell/fab/FloatingActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized type: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/FloatingActionButton;->getType()Lcom/software/shell/fab/ActionButton$Type;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getAnimationOnHide()Landroid/view/animation/Animation;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lcom/software/shell/fab/FloatingActionButton;->getHideAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public getAnimationOnShow()Landroid/view/animation/Animation;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lcom/software/shell/fab/FloatingActionButton;->getShowAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public setAnimationOnHide(Landroid/view/animation/Animation;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/FloatingActionButton;->setHideAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public setAnimationOnHide(Lcom/software/shell/fab/ActionButton$Animations;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/FloatingActionButton;->setHideAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    return-void
.end method

.method public setAnimationOnShow(Landroid/view/animation/Animation;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/FloatingActionButton;->setShowAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public setAnimationOnShow(Lcom/software/shell/fab/ActionButton$Animations;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/FloatingActionButton;->setShowAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    return-void
.end method
