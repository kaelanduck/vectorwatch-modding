.class Lcom/software/shell/fab/ShadowResponsiveDrawer;
.super Lcom/software/shell/fab/EffectDrawer;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;

.field private static final SHADOW_DRAWING_STEP:F = 0.5f

.field private static final SHADOW_RESPONSE_FACTOR:F = 1.75f


# instance fields
.field private currentShadowRadius:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Lcom/software/shell/fab/ActionButton;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/software/shell/fab/EffectDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    invoke-direct {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->init()V

    return-void
.end method

.method private init()V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    return-void
.end method


# virtual methods
.method draw(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->updateRadius()V

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget v1, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/software/shell/fab/ActionButton;->getShadowXOffset()F

    move-result v2

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v3

    invoke-virtual {v3}, Lcom/software/shell/fab/ActionButton;->getShadowYOffset()F

    move-result v3

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v4

    invoke-virtual {v4}, Lcom/software/shell/fab/ActionButton;->getShadowColor()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    sget-object v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawn the next Shadow Responsive Effect step"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method getMaxShadowRadius()F
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getMinShadowRadius()F

    move-result v0

    const/high16 v1, 0x3fe00000    # 1.75f

    mul-float/2addr v0, v1

    return v0
.end method

.method getMinShadowRadius()F
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v0

    return v0
.end method

.method setCurrentShadowRadius(F)V
    .locals 0

    iput p1, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    return-void
.end method

.method updateRadius()V
    .locals 3

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getMaxShadowRadius()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getInvalidator()Lcom/software/shell/fab/ViewInvalidator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ViewInvalidator;->requireInvalidation()V

    :cond_0
    :goto_0
    sget-object v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Updated Shadow Responsive Effect current radius to: {}"

    iget v2, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->isPressed()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getMinShadowRadius()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getInvalidator()Lcom/software/shell/fab/ViewInvalidator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ViewInvalidator;->requireInvalidation()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ShadowResponsiveDrawer;->currentShadowRadius:F

    goto :goto_0
.end method
