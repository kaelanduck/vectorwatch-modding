.class public final Lcom/software/shell/fab/TouchPoint;
.super Ljava/lang/Object;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# instance fields
.field private lastX:F

.field private lastY:F

.field private x:F

.field private y:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/fab/TouchPoint;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(FF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/TouchPoint;->setX(F)V

    invoke-virtual {p0, p2}, Lcom/software/shell/fab/TouchPoint;->setY(F)V

    return-void
.end method


# virtual methods
.method public getLastX()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/TouchPoint;->lastX:F

    return v0
.end method

.method public getLastY()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/TouchPoint;->lastY:F

    return v0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/TouchPoint;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/TouchPoint;->y:F

    return v0
.end method

.method isInsideCircle(FFF)Z
    .locals 8

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-virtual {p0}, Lcom/software/shell/fab/TouchPoint;->getX()F

    move-result v0

    sub-float/2addr v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/software/shell/fab/TouchPoint;->getY()F

    move-result v2

    sub-float/2addr v2, p2

    float-to-double v2, v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    float-to-double v4, p3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v0, v2

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "Detected touch point {} inside the main circle"

    if-eqz v0, :cond_1

    const-string v1, "IS"

    :goto_1
    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "IS NOT"

    goto :goto_1
.end method

.method reset()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/TouchPoint;->setX(F)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/TouchPoint;->setY(F)V

    sget-object v0, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Reset touch point"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method final setLastX(F)V
    .locals 3

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lcom/software/shell/fab/TouchPoint;->lastX:F

    sget-object v0, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set touch point last X-axis coordinate to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/TouchPoint;->getLastX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method final setLastY(F)V
    .locals 3

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lcom/software/shell/fab/TouchPoint;->lastY:F

    sget-object v0, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set touch point last Y-axis coordinate to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/TouchPoint;->getLastY()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method final setX(F)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/TouchPoint;->x:F

    sget-object v0, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set touch point X-axis coordinate to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/TouchPoint;->getX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/TouchPoint;->setLastX(F)V

    return-void
.end method

.method final setY(F)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/TouchPoint;->y:F

    sget-object v0, Lcom/software/shell/fab/TouchPoint;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set touch point Y-axis coordinate to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/TouchPoint;->getY()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/TouchPoint;->setLastY(F)V

    return-void
.end method
