.class Lcom/software/shell/fab/RippleEffectDrawer;
.super Lcom/software/shell/fab/EffectDrawer;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;

.field private static final POST_INVALIDATION_DELAY_MS:J = 0x64L

.field private static final RADIUS_INCREMENT:I = 0x5


# instance fields
.field private currentRadius:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/fab/RippleEffectDrawer;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/fab/RippleEffectDrawer;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Lcom/software/shell/fab/ActionButton;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/software/shell/fab/EffectDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    return-void
.end method

.method private drawRipple(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-direct {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getCircleClipPath()Landroid/graphics/Path;

    move-result-object v0

    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getTouchPoint()Lcom/software/shell/fab/TouchPoint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/TouchPoint;->getLastX()F

    move-result v1

    invoke-virtual {v0}, Lcom/software/shell/fab/TouchPoint;->getLastY()F

    move-result v0

    iget v2, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    int-to-float v2, v2

    invoke-direct {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getPreparedPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private getCircleClipPath()Landroid/graphics/Path;
    .locals 5

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v1

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v2

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v3

    invoke-virtual {v3}, Lcom/software/shell/fab/ActionButton;->calculateCircleRadius()F

    move-result v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    return-object v0
.end method

.method private getEndRippleRadius()I
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->calculateCircleRadius()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private getPreparedPaint()Landroid/graphics/Paint;
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->resetPaint()V

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/software/shell/fab/ActionButton;->getButtonColorRipple()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-object v0
.end method

.method private updateRadius()V
    .locals 3

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    invoke-direct {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getEndRippleRadius()I

    move-result v1

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    :cond_0
    :goto_0
    sget-object v0, Lcom/software/shell/fab/RippleEffectDrawer;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Updated Ripple Effect radius to: {}"

    iget v2, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isDrawingInProgress()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getEndRippleRadius()I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isDrawingFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    goto :goto_0
.end method


# virtual methods
.method draw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-direct {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->updateRadius()V

    invoke-direct {p0, p1}, Lcom/software/shell/fab/RippleEffectDrawer;->drawRipple(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getActionButton()Lcom/software/shell/fab/ActionButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton;->getInvalidator()Lcom/software/shell/fab/ViewInvalidator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isDrawingInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/software/shell/fab/ViewInvalidator;->requireInvalidation()V

    sget-object v0, Lcom/software/shell/fab/RippleEffectDrawer;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawing Ripple Effect in progress, invalidating the Action Button"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isDrawingFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isPressed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/software/shell/fab/ViewInvalidator;->requireDelayedInvalidation()V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/software/shell/fab/ViewInvalidator;->setInvalidationDelay(J)V

    sget-object v0, Lcom/software/shell/fab/RippleEffectDrawer;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Completed Ripple Effect drawing, posting the last invalidate"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    goto :goto_0
.end method

.method isDrawingFinished()Z
    .locals 2

    iget v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    invoke-direct {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->getEndRippleRadius()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isDrawingInProgress()Z
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/RippleEffectDrawer;->currentRadius:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/RippleEffectDrawer;->isDrawingFinished()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
