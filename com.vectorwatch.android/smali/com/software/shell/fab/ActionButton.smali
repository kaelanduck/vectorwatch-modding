.class public Lcom/software/shell/fab/ActionButton;
.super Landroid/view/View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/software/shell/fab/ActionButton$Animations;,
        Lcom/software/shell/fab/ActionButton$State;,
        Lcom/software/shell/fab/ActionButton$Type;
    }
.end annotation


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# instance fields
.field private buttonColor:I

.field private buttonColorPressed:I

.field private buttonColorRipple:I

.field private hideAnimation:Landroid/view/animation/Animation;

.field private image:Landroid/graphics/drawable/Drawable;

.field private imageSize:F

.field private final invalidator:Lcom/software/shell/fab/ViewInvalidator;

.field protected final mover:Lcom/software/shell/viewmover/movers/ViewMover;

.field private final paint:Landroid/graphics/Paint;

.field protected final rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

.field private rippleEffectEnabled:Z

.field private shadowColor:I

.field private shadowRadius:F

.field protected final shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

.field private shadowResponsiveEffectEnabled:Z

.field private shadowXOffset:F

.field private shadowYOffset:F

.field private showAnimation:Landroid/view/animation/Animation;

.field private size:F

.field private state:Lcom/software/shell/fab/ActionButton$State;

.field private strokeColor:I

.field private strokeWidth:F

.field private touchPoint:Lcom/software/shell/fab/TouchPoint;

.field private type:Lcom/software/shell/fab/ActionButton$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/fab/ActionButton;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x1

    const/high16 v2, 0x41000000    # 8.0f

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton$Type;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    sget-object v0, Lcom/software/shell/fab/ActionButton$State;->NORMAL:Lcom/software/shell/fab/ActionButton$State;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->state:Lcom/software/shell/fab/ActionButton$State;

    const-string v0, "#FF9B9B9B"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    const-string v0, "#FF696969"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->darkenButtonColorPressed()I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    const-string v0, "#42000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    iput-boolean v3, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    iput v1, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    const/high16 v0, 0x41c00000    # 24.0f

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    new-instance v0, Lcom/software/shell/fab/TouchPoint;

    invoke-direct {v0, v1, v1}, Lcom/software/shell/fab/TouchPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->touchPoint:Lcom/software/shell/fab/TouchPoint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->paint:Landroid/graphics/Paint;

    new-instance v0, Lcom/software/shell/fab/ViewInvalidator;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ViewInvalidator;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->invalidator:Lcom/software/shell/fab/ViewInvalidator;

    new-instance v0, Lcom/software/shell/fab/RippleEffectDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/RippleEffectDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

    new-instance v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    invoke-static {p0}, Lcom/software/shell/viewmover/movers/ViewMoverFactory;->createInstance(Landroid/view/View;)Lcom/software/shell/viewmover/movers/ViewMover;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->mover:Lcom/software/shell/viewmover/movers/ViewMover;

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->initActionButton()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x41000000    # 8.0f

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton$Type;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    sget-object v0, Lcom/software/shell/fab/ActionButton$State;->NORMAL:Lcom/software/shell/fab/ActionButton$State;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->state:Lcom/software/shell/fab/ActionButton$State;

    const-string v0, "#FF9B9B9B"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    const-string v0, "#FF696969"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->darkenButtonColorPressed()I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    const-string v0, "#42000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    iput-boolean v4, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    iput v1, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    const/high16 v0, 0x41c00000    # 24.0f

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    new-instance v0, Lcom/software/shell/fab/TouchPoint;

    invoke-direct {v0, v1, v1}, Lcom/software/shell/fab/TouchPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->touchPoint:Lcom/software/shell/fab/TouchPoint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->paint:Landroid/graphics/Paint;

    new-instance v0, Lcom/software/shell/fab/ViewInvalidator;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ViewInvalidator;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->invalidator:Lcom/software/shell/fab/ViewInvalidator;

    new-instance v0, Lcom/software/shell/fab/RippleEffectDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/RippleEffectDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

    new-instance v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    invoke-static {p0}, Lcom/software/shell/viewmover/movers/ViewMoverFactory;->createInstance(Landroid/view/View;)Lcom/software/shell/viewmover/movers/ViewMover;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->mover:Lcom/software/shell/viewmover/movers/ViewMover;

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->initActionButton()V

    invoke-direct {p0, p1, p2, v3, v3}, Lcom/software/shell/fab/ActionButton;->initActionButtonAttrs(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x1

    const/high16 v2, 0x41000000    # 8.0f

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton$Type;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    sget-object v0, Lcom/software/shell/fab/ActionButton$State;->NORMAL:Lcom/software/shell/fab/ActionButton$State;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->state:Lcom/software/shell/fab/ActionButton$State;

    const-string v0, "#FF9B9B9B"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    const-string v0, "#FF696969"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->darkenButtonColorPressed()I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    const-string v0, "#42000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    iput-boolean v3, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    iput v1, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    const/high16 v0, 0x41c00000    # 24.0f

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    new-instance v0, Lcom/software/shell/fab/TouchPoint;

    invoke-direct {v0, v1, v1}, Lcom/software/shell/fab/TouchPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->touchPoint:Lcom/software/shell/fab/TouchPoint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->paint:Landroid/graphics/Paint;

    new-instance v0, Lcom/software/shell/fab/ViewInvalidator;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ViewInvalidator;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->invalidator:Lcom/software/shell/fab/ViewInvalidator;

    new-instance v0, Lcom/software/shell/fab/RippleEffectDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/RippleEffectDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

    new-instance v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    invoke-static {p0}, Lcom/software/shell/viewmover/movers/ViewMoverFactory;->createInstance(Landroid/view/View;)Lcom/software/shell/viewmover/movers/ViewMover;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->mover:Lcom/software/shell/viewmover/movers/ViewMover;

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->initActionButton()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/software/shell/fab/ActionButton;->initActionButtonAttrs(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    const/4 v3, 0x1

    const/high16 v2, 0x41000000    # 8.0f

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    sget-object v0, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton$Type;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    sget-object v0, Lcom/software/shell/fab/ActionButton$State;->NORMAL:Lcom/software/shell/fab/ActionButton$State;

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->state:Lcom/software/shell/fab/ActionButton$State;

    const-string v0, "#FF9B9B9B"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    const-string v0, "#FF696969"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->darkenButtonColorPressed()I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    const-string v0, "#42000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    iput-boolean v3, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    iput v1, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    const/high16 v0, 0x41c00000    # 24.0f

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    new-instance v0, Lcom/software/shell/fab/TouchPoint;

    invoke-direct {v0, v1, v1}, Lcom/software/shell/fab/TouchPoint;-><init>(FF)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->touchPoint:Lcom/software/shell/fab/TouchPoint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->paint:Landroid/graphics/Paint;

    new-instance v0, Lcom/software/shell/fab/ViewInvalidator;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ViewInvalidator;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->invalidator:Lcom/software/shell/fab/ViewInvalidator;

    new-instance v0, Lcom/software/shell/fab/RippleEffectDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/RippleEffectDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

    new-instance v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-direct {v0, p0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;-><init>(Lcom/software/shell/fab/ActionButton;)V

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    invoke-static {p0}, Lcom/software/shell/viewmover/movers/ViewMoverFactory;->createInstance(Landroid/view/View;)Lcom/software/shell/viewmover/movers/ViewMover;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->mover:Lcom/software/shell/viewmover/movers/ViewMover;

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->initActionButton()V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/software/shell/fab/ActionButton;->initActionButtonAttrs(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private calculateMeasuredHeight()I
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v0

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->calculateShadowHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->calculateStrokeWeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button measured height: {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method private calculateMeasuredWidth()I
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v0

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->calculateShadowWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->calculateStrokeWeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button measured width: {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method private calculateShadowHeight()I
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isShadowResponsiveEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    check-cast v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-virtual {v0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getMaxShadowRadius()F

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasShadow()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowYOffset()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button shadow height: {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private calculateShadowWidth()I
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isShadowResponsiveEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    check-cast v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-virtual {v0}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->getMaxShadowRadius()F

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasShadow()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowXOffset()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button shadow width: {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private calculateStrokeWeight()I
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeWidth()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button stroke width: {}"

    iget v3, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method private darkenButtonColorPressed()I
    .locals 2

    const v0, 0x3f4ccccd    # 0.8f

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColorPressed()I

    move-result v1

    invoke-static {v1, v0}, Lcom/software/shell/uitools/resutils/color/ColorModifier;->modifyExposure(IF)I

    move-result v0

    return v0
.end method

.method private hasElevation()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getElevation()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initActionButton()V
    .locals 2

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->initLayerType()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized the Action Button"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method private initActionButtonAttrs(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/software/shell/fab/R$styleable;->ActionButton:[I

    invoke-virtual {v0, p2, v1, p3, p4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initType(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initSize(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initButtonColor(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initButtonColorPressed(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initRippleEffectEnabled(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initButtonColorRipple(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initShadowRadius(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initShadowXOffset(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initShadowYOffset(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initShadowColor(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initShadowResponsiveEffectEnabled(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initStrokeWidth(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initStrokeColor(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initImage(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initImageSize(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initShowAnimation(Landroid/content/res/TypedArray;)V

    invoke-direct {p0, v1}, Lcom/software/shell/fab/ActionButton;->initHideAnimation(Landroid/content/res/TypedArray;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Successfully initialized the Action Button attributes"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v2, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "Failed to read attribute"

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private initButtonColor(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_button_color:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button color: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initButtonColorPressed(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_button_colorPressed:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->darkenButtonColorPressed()I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button color pressed: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColorPressed()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initButtonColorRipple(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_button_colorRipple:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button Ripple Effect color: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColorRipple()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initHideAnimation(Landroid/content/res/TypedArray;)V
    .locals 2

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_hide_animation:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->NONE:Lcom/software/shell/fab/ActionButton$Animations;

    iget v1, v1, Lcom/software/shell/fab/ActionButton$Animations;->animResId:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/software/shell/fab/ActionButton$Animations;->load(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->hideAnimation:Landroid/view/animation/Animation;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button hide animation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private initImage(Landroid/content/res/TypedArray;)V
    .locals 2

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_image:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->image:Landroid/graphics/drawable/Drawable;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button image"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private initImageSize(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_image_size:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button image size: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImageSize()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initLayerType()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/software/shell/fab/ActionButton;->setLayerType(ILandroid/graphics/Paint;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized the layer type"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private initRippleEffectEnabled(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_rippleEffect_enabled:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectEnabled:Z

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button Ripple Effect enabled: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isRippleEffectEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initShadowColor(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_shadow_color:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button shadow color: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initShadowRadius(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_shadow_radius:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button shadow radius: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initShadowResponsiveEffectEnabled(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_shadowResponsiveEffect_enabled:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button Shadow Responsive Effect enabled: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isShadowResponsiveEffectEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initShadowXOffset(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_shadow_xOffset:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button X-axis offset: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowXOffset()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initShadowYOffset(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_shadow_yOffset:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button shadow Y-axis offset: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowYOffset()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initShowAnimation(Landroid/content/res/TypedArray;)V
    .locals 2

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_show_animation:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/software/shell/fab/ActionButton$Animations;->NONE:Lcom/software/shell/fab/ActionButton$Animations;

    iget v1, v1, Lcom/software/shell/fab/ActionButton$Animations;->animResId:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/software/shell/fab/ActionButton$Animations;->load(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->showAnimation:Landroid/view/animation/Animation;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button show animation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private initSize(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_size:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->size:F

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    :goto_0
    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button size: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton$Type;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    goto :goto_0
.end method

.method private initStrokeColor(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_stroke_color:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button stroke color: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initStrokeWidth(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_stroke_width:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button stroke width: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeWidth()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private initType(Landroid/content/res/TypedArray;)V
    .locals 3

    sget v0, Lcom/software/shell/fab/R$styleable;->ActionButton_type:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v1}, Lcom/software/shell/fab/ActionButton$Type;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-static {v0}, Lcom/software/shell/fab/ActionButton$Type;->forId(I)Lcom/software/shell/fab/ActionButton$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Initialized Action Button type: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getType()Lcom/software/shell/fab/ActionButton$Type;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected calculateCenterX()F
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button center X: {}"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method protected calculateCenterY()F
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button center Y: {}"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method protected final calculateCircleRadius()F
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Calculated Action Button circle radius: {}"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0
.end method

.method public dismiss()V
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->playHideAnimation()V

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Dismissed the Action Button"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected dpToPx(F)F
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/software/shell/uitools/convert/DensityConverter;->dpToPx(Landroid/content/Context;F)F

    move-result v0

    return v0
.end method

.method protected drawCircle(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->resetPaint()V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasShadow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isShadowResponsiveEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    invoke-virtual {v0, p1}, Lcom/software/shell/fab/EffectDrawer;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isRippleEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

    check-cast v0, Lcom/software/shell/fab/RippleEffectDrawer;

    invoke-virtual {v0}, Lcom/software/shell/fab/RippleEffectDrawer;->isDrawingInProgress()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getState()Lcom/software/shell/fab/ActionButton$State;

    move-result-object v2

    sget-object v3, Lcom/software/shell/fab/ActionButton$State;->PRESSED:Lcom/software/shell/fab/ActionButton$State;

    if-eq v2, v3, :cond_1

    if-eqz v0, :cond_4

    :cond_1
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColorPressed()I

    move-result v0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCircleRadius()F

    move-result v2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawn the Action Button circle"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->drawShadow()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColor()I

    move-result v0

    goto :goto_2
.end method

.method protected drawElevation()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v1

    sub-float/2addr v1, v0

    float-to-int v2, v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v1

    sub-float/2addr v1, v0

    float-to-int v3, v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v1

    add-float/2addr v1, v0

    float-to-int v4, v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v5, v0

    new-instance v0, Lcom/software/shell/fab/ActionButton$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/software/shell/fab/ActionButton$1;-><init>(Lcom/software/shell/fab/ActionButton;IIII)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawn the Action Button elevation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method protected drawImage(Landroid/graphics/Canvas;)V
    .locals 8

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImageSize()F

    move-result v1

    div-float/2addr v1, v3

    sub-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImageSize()F

    move-result v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImageSize()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v3, v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImageSize()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImage()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImage()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sget-object v4, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v5, "Drawn the Action Button image on canvas with coordinates: X start point = {}, Y start point = {}, X end point = {}, Y end point = {}"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected drawRipple(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectDrawer:Lcom/software/shell/fab/EffectDrawer;

    invoke-virtual {v0, p1}, Lcom/software/shell/fab/EffectDrawer;->draw(Landroid/graphics/Canvas;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawn the Action Button Ripple Effect"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method protected drawShadow()V
    .locals 5

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowXOffset()F

    move-result v2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowYOffset()F

    move-result v3

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowColor()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawn the Action Button shadow"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method protected drawStroke(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->resetPaint()V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCircleRadius()F

    move-result v2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Drawn the Action Button stroke"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public getButtonColor()I
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    return v0
.end method

.method public getButtonColorPressed()I
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    return v0
.end method

.method public getButtonColorRipple()I
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    return v0
.end method

.method public getButtonSize()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getHideAnimation()Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->hideAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public getImage()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->image:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageSize()F
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getInvalidator()Lcom/software/shell/fab/ViewInvalidator;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->invalidator:Lcom/software/shell/fab/ViewInvalidator;

    return-object v0
.end method

.method protected getPaint()Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->paint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getShadowColor()I
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    return v0
.end method

.method public getShadowRadius()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    return v0
.end method

.method public getShadowXOffset()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    return v0
.end method

.method public getShadowYOffset()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    return v0
.end method

.method public getShowAnimation()Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->showAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public getSize()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    return v0
.end method

.method public getState()Lcom/software/shell/fab/ActionButton$State;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->state:Lcom/software/shell/fab/ActionButton$State;

    return-object v0
.end method

.method public getStrokeColor()I
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    iget v0, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    return v0
.end method

.method public getTouchPoint()Lcom/software/shell/fab/TouchPoint;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->touchPoint:Lcom/software/shell/fab/TouchPoint;

    return-object v0
.end method

.method public getType()Lcom/software/shell/fab/ActionButton$Type;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    return-object v0
.end method

.method public hasImage()Z
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShadow()Z
    .locals 2

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->hasElevation()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStroke()Z
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hide()V
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->playHideAnimation()V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Hidden the Action Button"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public isDismissed()Z
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHidden()Z
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRippleEffectEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectEnabled:Z

    return v0
.end method

.method public isShadowResponsiveEffectEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    return v0
.end method

.method public move(Lcom/software/shell/viewmover/configuration/MovingParams;)V
    .locals 4

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "About to move the Action Button: X-axis delta = {}, Y-axis delta = {}"

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->mover:Lcom/software/shell/viewmover/movers/ViewMover;

    invoke-virtual {v0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;->move(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    return-void
.end method

.method public moveDown(F)V
    .locals 3

    new-instance v0, Lcom/software/shell/viewmover/configuration/MovingParams;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, Lcom/software/shell/viewmover/configuration/MovingParams;-><init>(Landroid/content/Context;FF)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->move(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    return-void
.end method

.method public moveLeft(F)V
    .locals 4

    new-instance v0, Lcom/software/shell/viewmover/configuration/MovingParams;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    neg-float v2, p1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/software/shell/viewmover/configuration/MovingParams;-><init>(Landroid/content/Context;FF)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->move(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    return-void
.end method

.method public moveRight(F)V
    .locals 3

    new-instance v0, Lcom/software/shell/viewmover/configuration/MovingParams;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/software/shell/viewmover/configuration/MovingParams;-><init>(Landroid/content/Context;FF)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->move(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    return-void
.end method

.method public moveUp(F)V
    .locals 4

    new-instance v0, Lcom/software/shell/viewmover/configuration/MovingParams;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    neg-float v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/software/shell/viewmover/configuration/MovingParams;-><init>(Landroid/content/Context;FF)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->move(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Called Action Button onDraw"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->drawCircle(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isRippleEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->drawRipple(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->hasElevation()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->drawElevation()V

    :cond_1
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasStroke()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->drawStroke(Landroid/graphics/Canvas;)V

    :cond_2
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->drawImage(Landroid/graphics/Canvas;)V

    :cond_3
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getInvalidator()Lcom/software/shell/fab/ViewInvalidator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ViewInvalidator;->invalidate()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Called Action Button onMeasure"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->calculateMeasuredWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->calculateMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/software/shell/fab/ActionButton;->setMeasuredDimension(II)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Measured the Action Button size: height = {}, width = {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    new-instance v1, Lcom/software/shell/fab/TouchPoint;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/software/shell/fab/TouchPoint;-><init>(FF)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterX()F

    move-result v2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCenterY()F

    move-result v3

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->calculateCircleRadius()F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/software/shell/fab/TouchPoint;->isInsideCircle(FFF)Z

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Detected unrecognized motion event"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    if-eqz v2, :cond_0

    sget-object v2, Lcom/software/shell/fab/ActionButton$State;->PRESSED:Lcom/software/shell/fab/ActionButton$State;

    invoke-virtual {p0, v2}, Lcom/software/shell/fab/ActionButton;->setState(Lcom/software/shell/fab/ActionButton$State;)V

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->setTouchPoint(Lcom/software/shell/fab/TouchPoint;)V

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Detected the ACTION_DOWN motion event"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    if-eqz v2, :cond_0

    sget-object v1, Lcom/software/shell/fab/ActionButton$State;->NORMAL:Lcom/software/shell/fab/ActionButton$State;

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->setState(Lcom/software/shell/fab/ActionButton$State;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getTouchPoint()Lcom/software/shell/fab/TouchPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/software/shell/fab/TouchPoint;->reset()V

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Detected the ACTION_UP motion event"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getState()Lcom/software/shell/fab/ActionButton$State;

    move-result-object v1

    sget-object v2, Lcom/software/shell/fab/ActionButton$State;->PRESSED:Lcom/software/shell/fab/ActionButton$State;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/software/shell/fab/ActionButton$State;->NORMAL:Lcom/software/shell/fab/ActionButton$State;

    invoke-virtual {p0, v1}, Lcom/software/shell/fab/ActionButton;->setState(Lcom/software/shell/fab/ActionButton$State;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getTouchPoint()Lcom/software/shell/fab/TouchPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/software/shell/fab/TouchPoint;->reset()V

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Detected the ACTION_MOVE motion event"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public playHideAnimation()V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getHideAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public playShowAnimation()V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShowAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public removeHideAnimation()V
    .locals 2

    sget-object v0, Lcom/software/shell/fab/ActionButton$Animations;->NONE:Lcom/software/shell/fab/ActionButton$Animations;

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setHideAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Removed the Action Button hide animation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public removeImage()V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public removeShadow()V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasShadow()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setShadowRadius(F)V

    :cond_0
    return-void
.end method

.method public removeShowAnimation()V
    .locals 2

    sget-object v0, Lcom/software/shell/fab/ActionButton$Animations;->NONE:Lcom/software/shell/fab/ActionButton$Animations;

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setShowAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Removed the Action Button show animation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public removeStroke()V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->hasStroke()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    :cond_0
    return-void
.end method

.method protected final resetPaint()V
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Reset the Action Button paint"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public setButtonColor(I)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/ActionButton;->buttonColor:I

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->invalidate()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button color to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setButtonColorPressed(I)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/ActionButton;->buttonColorPressed:I

    invoke-direct {p0}, Lcom/software/shell/fab/ActionButton;->darkenButtonColorPressed()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setButtonColorRipple(I)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button color pressed to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColorPressed()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setButtonColorRipple(I)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/ActionButton;->buttonColorRipple:I

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Action Button Ripple Effect color changed to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getButtonColorRipple()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setHideAnimation(Landroid/view/animation/Animation;)V
    .locals 2

    iput-object p1, p0, Lcom/software/shell/fab/ActionButton;->hideAnimation:Landroid/view/animation/Animation;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set the Action Button hide animation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public setHideAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/software/shell/fab/ActionButton$Animations;->animResId:I

    invoke-static {v0, v1}, Lcom/software/shell/fab/ActionButton$Animations;->load(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setHideAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iput-object p1, p0, Lcom/software/shell/fab/ActionButton;->image:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->invalidate()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set the Action Button image drawable"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setImageSize(F)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->imageSize:F

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button image size to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getImageSize()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setRippleEffectEnabled(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/software/shell/fab/ActionButton;->rippleEffectEnabled:Z

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string/jumbo v2, "{} the Action Button Ripple Effect"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isRippleEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Enabled"

    :goto_0
    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "Disabled"

    goto :goto_0
.end method

.method public setShadowColor(I)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/ActionButton;->shadowColor:I

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->invalidate()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button shadow color to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setShadowRadius(F)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowRadius:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isShadowResponsiveEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveDrawer:Lcom/software/shell/fab/EffectDrawer;

    check-cast v0, Lcom/software/shell/fab/ShadowResponsiveDrawer;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ShadowResponsiveDrawer;->setCurrentShadowRadius(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->requestLayout()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Action Button shadow radius changed to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowRadius()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setShadowResponsiveEffectEnabled(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/software/shell/fab/ActionButton;->shadowResponsiveEffectEnabled:Z

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->requestLayout()V

    sget-object v1, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string/jumbo v2, "{} the Shadow Responsive Effect"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isShadowResponsiveEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Enabled"

    :goto_0
    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "Disabled"

    goto :goto_0
.end method

.method public setShadowXOffset(F)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowXOffset:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->requestLayout()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button shadow X offset to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowXOffset()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setShadowYOffset(F)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->shadowYOffset:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->requestLayout()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button shadow Y offset to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getShadowYOffset()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setShowAnimation(Landroid/view/animation/Animation;)V
    .locals 2

    iput-object p1, p0, Lcom/software/shell/fab/ActionButton;->showAnimation:Landroid/view/animation/Animation;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set the Action Button show animation"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public setShowAnimation(Lcom/software/shell/fab/ActionButton$Animations;)V
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/software/shell/fab/ActionButton$Animations;->animResId:I

    invoke-static {v0, v1}, Lcom/software/shell/fab/ActionButton$Animations;->load(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setShowAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public setSize(F)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->size:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->requestLayout()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Set the Action Button size to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getSize()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setState(Lcom/software/shell/fab/ActionButton$State;)V
    .locals 3

    iput-object p1, p0, Lcom/software/shell/fab/ActionButton;->state:Lcom/software/shell/fab/ActionButton$State;

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->invalidate()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button state to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getState()Lcom/software/shell/fab/ActionButton$State;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setStrokeColor(I)V
    .locals 3

    iput p1, p0, Lcom/software/shell/fab/ActionButton;->strokeColor:I

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->invalidate()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the stroke color to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/software/shell/fab/ActionButton;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/fab/ActionButton;->strokeWidth:F

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->requestLayout()V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the stroke width to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getStrokeWidth()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method protected setTouchPoint(Lcom/software/shell/fab/TouchPoint;)V
    .locals 0

    iput-object p1, p0, Lcom/software/shell/fab/ActionButton;->touchPoint:Lcom/software/shell/fab/TouchPoint;

    return-void
.end method

.method public setType(Lcom/software/shell/fab/ActionButton$Type;)V
    .locals 3

    iput-object p1, p0, Lcom/software/shell/fab/ActionButton;->type:Lcom/software/shell/fab/ActionButton$Type;

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Changed the Action Button type to: {}"

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getType()Lcom/software/shell/fab/ActionButton$Type;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getType()Lcom/software/shell/fab/ActionButton$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/software/shell/fab/ActionButton$Type;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setSize(F)V

    return-void
.end method

.method public show()V
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->isHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->playShowAnimation()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/software/shell/fab/ActionButton;->setVisibility(I)V

    sget-object v0, Lcom/software/shell/fab/ActionButton;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Shown the Action Button"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public startAnimation(Landroid/view/animation/Animation;)V
    .locals 1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/software/shell/fab/ActionButton;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method
