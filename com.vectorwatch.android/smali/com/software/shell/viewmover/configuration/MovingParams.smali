.class public Lcom/software/shell/viewmover/configuration/MovingParams;
.super Ljava/lang/Object;


# static fields
.field private static final DEFAULT_ANIMATION_DURATION:J = 0x1f4L

.field private static final LOGGER:Lorg/slf4j/Logger;


# instance fields
.field private animationDuration:J

.field private animationInterpolator:Landroid/view/animation/Interpolator;

.field private final context:Landroid/content/Context;

.field private xAxisDelta:F

.field private yAxisDelta:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/viewmover/configuration/MovingParams;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    iput-object p1, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->context:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->xAxisDelta:F

    invoke-direct {p0, p3}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->yAxisDelta:F

    sget-object v0, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Moving params initialized with values: xAxisDelta = {}, yAxisDelta = {}"

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FFJ)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    iput-object p1, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->context:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->xAxisDelta:F

    invoke-direct {p0, p3}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->yAxisDelta:F

    iput-wide p4, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    sget-object v0, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Moving params initialized with values: xAxisDelta = {}, yAxisDelta = {}, animationDuration = {}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FFJLandroid/view/animation/Interpolator;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    iput-object p1, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->context:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->xAxisDelta:F

    invoke-direct {p0, p3}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->yAxisDelta:F

    iput-wide p4, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    iput-object p6, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationInterpolator:Landroid/view/animation/Interpolator;

    sget-object v0, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Moving params initialized with values: xAxisDelta = {}, yAxisDelta = {}, animationDuration = {}animationInterpolator is an instance of {} class"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Lcom/software/shell/viewmover/configuration/MovingParams;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->context:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->xAxisDelta:F

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->yAxisDelta:F

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationInterpolator:Landroid/view/animation/Interpolator;

    sget-object v1, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Cloned moving params initialized with values: xAxisDelta = {}, yAxisDelta = {}, animationDuration = {}, animation interpolator is an instance of {} class"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private dpToPx(F)F
    .locals 1

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/software/shell/uitools/convert/DensityConverter;->dpToPx(Landroid/content/Context;F)F

    move-result v0

    return v0
.end method


# virtual methods
.method public getAnimationDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationDuration:J

    return-wide v0
.end method

.method public getAnimationInterpolator()Landroid/view/animation/Interpolator;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->animationInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getXAxisDelta()F
    .locals 1

    iget v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->xAxisDelta:F

    return v0
.end method

.method public getYAxisDelta()F
    .locals 1

    iget v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->yAxisDelta:F

    return v0
.end method

.method public setXAxisDelta(F)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->xAxisDelta:F

    sget-object v0, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Moving params xAxisDelta set to: {}"

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setYAxisDelta(F)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->dpToPx(F)F

    move-result v0

    iput v0, p0, Lcom/software/shell/viewmover/configuration/MovingParams;->yAxisDelta:F

    sget-object v0, Lcom/software/shell/viewmover/configuration/MovingParams;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Moving params yAxisDelta set to: {}"

    invoke-virtual {p0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
