.class Lcom/software/shell/viewmover/movers/PositionViewMover;
.super Lcom/software/shell/viewmover/movers/ViewMover;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/viewmover/movers/PositionViewMover;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/viewmover/movers/PositionViewMover;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method calculateEndBottomBound(F)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/PositionViewMover;->calculateEndTopBound(F)I

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method calculateEndLeftBound(F)I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    add-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method calculateEndRightBound(F)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/PositionViewMover;->calculateEndLeftBound(F)I

    move-result v0

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method calculateEndTopBound(F)I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    add-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method changeViewPosition(FF)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/PositionViewMover;->calculateEndLeftBound(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, p2}, Lcom/software/shell/viewmover/movers/PositionViewMover;->calculateEndTopBound(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setX(F)V

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setY(F)V

    sget-object v2, Lcom/software/shell/viewmover/movers/PositionViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "Updated view position: leftX = {}, topY = {}"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v2, v3, v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
