.class Lcom/software/shell/viewmover/movers/MarginViewMover;
.super Lcom/software/shell/viewmover/movers/ViewMover;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/viewmover/movers/MarginViewMover;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/viewmover/movers/MarginViewMover;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;-><init>(Landroid/view/View;)V

    return-void
.end method

.method private isViewLeftAligned(Landroid/view/ViewGroup$MarginLayoutParams;)Z
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/software/shell/viewmover/movers/MarginViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "View is {} aligned"

    if-eqz v0, :cond_2

    const-string v1, "LEFT"

    :goto_1
    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "RIGHT"

    goto :goto_1
.end method

.method private isViewTopAligned(Landroid/view/ViewGroup$MarginLayoutParams;)Z
    .locals 4

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/software/shell/viewmover/movers/MarginViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "View is {} aligned"

    if-eqz v0, :cond_2

    const-string v1, "TOP"

    :goto_1
    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "BOTTOM"

    goto :goto_1
.end method


# virtual methods
.method calculateEndBottomBound(F)I
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    float-to-int v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method calculateEndLeftBound(F)I
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    float-to-int v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method calculateEndRightBound(F)I
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    float-to-int v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method calculateEndTopBound(F)I
    .locals 2

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    float-to-int v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method changeViewPosition(FF)V
    .locals 6

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->isViewLeftAligned(Landroid/view/ViewGroup$MarginLayoutParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :goto_0
    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->isViewTopAligned(Landroid/view/ViewGroup$MarginLayoutParams;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v1, v1

    add-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :goto_1
    sget-object v1, Lcom/software/shell/viewmover/movers/MarginViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Updated view margins: left = {}, top = {}, right = {}, bottom = {}"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    int-to-float v1, v1

    sub-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_0

    :cond_1
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    int-to-float v1, v1

    sub-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_1
.end method
