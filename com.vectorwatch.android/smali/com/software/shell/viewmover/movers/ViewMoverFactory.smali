.class public abstract Lcom/software/shell/viewmover/movers/ViewMoverFactory;
.super Ljava/lang/Object;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/viewmover/movers/ViewMoverFactory;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/viewmover/movers/ViewMoverFactory;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createInstance(Landroid/view/View;)Lcom/software/shell/viewmover/movers/ViewMover;
    .locals 5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/software/shell/viewmover/movers/PositionViewMover;

    invoke-direct {v0, p0}, Lcom/software/shell/viewmover/movers/PositionViewMover;-><init>(Landroid/view/View;)V

    :goto_0
    sget-object v1, Lcom/software/shell/viewmover/movers/ViewMoverFactory;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Build version code is: {}. {} will be returned"

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/software/shell/viewmover/movers/MarginViewMover;

    invoke-direct {v0, p0}, Lcom/software/shell/viewmover/movers/MarginViewMover;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method
