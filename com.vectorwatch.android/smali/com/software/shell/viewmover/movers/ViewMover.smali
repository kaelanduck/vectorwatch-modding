.class public abstract Lcom/software/shell/viewmover/movers/ViewMover;
.super Ljava/lang/Object;


# static fields
.field private static final LOGGER:Lorg/slf4j/Logger;


# instance fields
.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/software/shell/viewmover/movers/ViewMover;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/software/shell/viewmover/movers/ViewMover;->view:Landroid/view/View;

    return-void
.end method

.method private createAnimation(Lcom/software/shell/viewmover/configuration/MovingParams;)Landroid/view/animation/Animation;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v1

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v2

    invoke-direct {v0, v3, v1, v3, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getAnimationInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    :cond_0
    new-instance v1, Lcom/software/shell/viewmover/movers/ViewMover$MoveAnimationListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/software/shell/viewmover/movers/ViewMover$MoveAnimationListener;-><init>(Lcom/software/shell/viewmover/movers/ViewMover;Lcom/software/shell/viewmover/configuration/MovingParams;Lcom/software/shell/viewmover/movers/ViewMover$1;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v0
.end method

.method private getVerifiedMovingParams(Lcom/software/shell/viewmover/configuration/MovingParams;)Lcom/software/shell/viewmover/configuration/MovingParams;
    .locals 6

    new-instance v0, Lcom/software/shell/viewmover/configuration/MovingParams;

    invoke-direct {v0, p1}, Lcom/software/shell/viewmover/configuration/MovingParams;-><init>(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/ViewMover;->updateXAxisDelta(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/ViewMover;->updateYAxisDelta(Lcom/software/shell/viewmover/configuration/MovingParams;)V

    sget-object v1, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Updated moving details values: X-axis from {} to {}, Y-axis from {} to {}"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {v0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private hasHorizontalSpaceToMove(F)Z
    .locals 7

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/ViewMover;->getParentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sget-object v1, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Parent view width is: {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;->calculateEndLeftBound(F)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;->calculateEndRightBound(F)I

    move-result v2

    sget-object v3, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v4, "Calculated end bounds: left = {}, right = {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    if-ltz v1, :cond_0

    if-gt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasVerticalSpaceToMove(F)Z
    .locals 7

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/ViewMover;->getParentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sget-object v1, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Parent view height is: {}"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;->calculateEndTopBound(F)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;->calculateEndBottomBound(F)I

    move-result v2

    sget-object v3, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v4, "Calculated end bounds: top = {}, bottom = {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    if-ltz v1, :cond_0

    if-gt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateXAxisDelta(Lcom/software/shell/viewmover/configuration/MovingParams;)V
    .locals 2

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/ViewMover;->hasHorizontalSpaceToMove(F)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Unable to move the view horizontally. No horizontal space left to move"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/software/shell/viewmover/configuration/MovingParams;->setXAxisDelta(F)V

    :cond_0
    return-void
.end method

.method private updateYAxisDelta(Lcom/software/shell/viewmover/configuration/MovingParams;)V
    .locals 2

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/ViewMover;->hasVerticalSpaceToMove(F)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v1, "Unable to move the view vertically. No vertical space left to move"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/software/shell/viewmover/configuration/MovingParams;->setYAxisDelta(F)V

    :cond_0
    return-void
.end method


# virtual methods
.method abstract calculateEndBottomBound(F)I
.end method

.method abstract calculateEndLeftBound(F)I
.end method

.method abstract calculateEndRightBound(F)I
.end method

.method abstract calculateEndTopBound(F)I
.end method

.method abstract changeViewPosition(FF)V
.end method

.method getParentView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/viewmover/movers/ViewMover;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/software/shell/viewmover/movers/ViewMover;->view:Landroid/view/View;

    return-object v0
.end method

.method isMoveNonZero(Lcom/software/shell/viewmover/configuration/MovingParams;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Zero movement detected. No movement will be performed"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isPreviousAnimationCompleted()Z
    .locals 3

    iget-object v0, p0, Lcom/software/shell/viewmover/movers/ViewMover;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v2, "Unable to move the view. View is being currently moving"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public move(Lcom/software/shell/viewmover/configuration/MovingParams;)V
    .locals 5

    invoke-virtual {p0}, Lcom/software/shell/viewmover/movers/ViewMover;->isPreviousAnimationCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/software/shell/viewmover/movers/ViewMover;->getVerifiedMovingParams(Lcom/software/shell/viewmover/configuration/MovingParams;)Lcom/software/shell/viewmover/configuration/MovingParams;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/software/shell/viewmover/movers/ViewMover;->isMoveNonZero(Lcom/software/shell/viewmover/configuration/MovingParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/software/shell/viewmover/movers/ViewMover;->createAnimation(Lcom/software/shell/viewmover/configuration/MovingParams;)Landroid/view/animation/Animation;

    move-result-object v1

    sget-object v2, Lcom/software/shell/viewmover/movers/ViewMover;->LOGGER:Lorg/slf4j/Logger;

    const-string v3, "View is about to be moved at: delta X-axis = {}, delta Y-axis = {}"

    invoke-virtual {v0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getXAxisDelta()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0}, Lcom/software/shell/viewmover/configuration/MovingParams;->getYAxisDelta()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v2, v3, v4, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/software/shell/viewmover/movers/ViewMover;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method
