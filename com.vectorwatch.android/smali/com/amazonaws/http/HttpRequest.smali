.class public Lcom/amazonaws/http/HttpRequest;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# instance fields
.field private final content:Ljava/io/InputStream;

.field private final headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isStreaming:Z

.field private final method:Ljava/lang/String;

.field private uri:Ljava/net/URI;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/net/URI;)V
    .locals 1
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/net/URI;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/amazonaws/http/HttpRequest;-><init>(Ljava/lang/String;Ljava/net/URI;Ljava/util/Map;Ljava/io/InputStream;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/net/URI;Ljava/util/Map;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/net/URI;
    .param p4, "content"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/InputStream;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1}, Lcom/amazonaws/util/StringUtils;->upperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazonaws/http/HttpRequest;->method:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/amazonaws/http/HttpRequest;->uri:Ljava/net/URI;

    .line 61
    if-nez p3, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    .line 62
    :goto_0
    iput-object v0, p0, Lcom/amazonaws/http/HttpRequest;->headers:Ljava/util/Map;

    .line 63
    iput-object p4, p0, Lcom/amazonaws/http/HttpRequest;->content:Ljava/io/InputStream;

    .line 64
    return-void

    .line 62
    :cond_0
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/amazonaws/http/HttpRequest;->content:Ljava/io/InputStream;

    return-object v0
.end method

.method public getContentLength()J
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 119
    iget-object v1, p0, Lcom/amazonaws/http/HttpRequest;->headers:Ljava/util/Map;

    if-nez v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-wide v2

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/amazonaws/http/HttpRequest;->headers:Ljava/util/Map;

    const-string v4, "Content-Length"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123
    .local v0, "len":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/amazonaws/http/HttpRequest;->headers:Ljava/util/Map;

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/amazonaws/http/HttpRequest;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Ljava/net/URI;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/amazonaws/http/HttpRequest;->uri:Ljava/net/URI;

    return-object v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/amazonaws/http/HttpRequest;->isStreaming:Z

    return v0
.end method

.method public setStreaming(Z)V
    .locals 0
    .param p1, "isStreaming"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/amazonaws/http/HttpRequest;->isStreaming:Z

    .line 135
    return-void
.end method

.method setUri(Ljava/net/URI;)V
    .locals 0
    .param p1, "uri"    # Ljava/net/URI;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/amazonaws/http/HttpRequest;->uri:Ljava/net/URI;

    .line 91
    return-void
.end method
