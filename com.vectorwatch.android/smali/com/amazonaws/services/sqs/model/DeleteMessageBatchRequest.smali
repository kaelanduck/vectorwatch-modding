.class public Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "DeleteMessageBatchRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            ">;"
        }
    .end annotation
.end field

.field private queueUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    .line 88
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    .line 108
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 110
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 281
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 298
    :cond_0
    :goto_0
    return v3

    .line 283
    :cond_1
    if-eqz p1, :cond_0

    .line 286
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 288
    check-cast v0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;

    .line 290
    .local v0, "other":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 292
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 296
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    .line 298
    goto :goto_0

    :cond_4
    move v1, v3

    .line 290
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v1, v3

    .line 294
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4
.end method

.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    return-object v0
.end method

.method public getQueueUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->queueUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 271
    const/16 v1, 0x1f

    .line 272
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 274
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 275
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_1
    add-int v0, v2, v3

    .line 276
    return v0

    .line 274
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 275
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setEntries(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "entries":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    if-nez p1, :cond_0

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    .line 204
    :goto_0
    return-void

    .line 203
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    goto :goto_0
.end method

.method public setQueueUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "queueUrl"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->queueUrl:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueueUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 264
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entries: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withEntries(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "entries":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 247
    return-object p0
.end method

.method public varargs withEntries([Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    .locals 4
    .param p1, "entries"    # [Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 222
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    .line 224
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 225
    .local v0, "value":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->entries:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "value":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;
    :cond_1
    return-object p0
.end method

.method public withQueueUrl(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    .locals 0
    .param p1, "queueUrl"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->queueUrl:Ljava/lang/String;

    .line 172
    return-object p0
.end method
