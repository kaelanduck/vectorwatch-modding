.class public Lcom/amazonaws/services/sqs/model/Message;
.super Ljava/lang/Object;
.source "Message.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private body:Ljava/lang/String;

.field private mD5OfBody:Ljava/lang/String;

.field private mD5OfMessageAttributes:Ljava/lang/String;

.field private messageAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field private messageId:Ljava/lang/String;

.field private receiptHandle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addAttributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 404
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicated keys ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") are provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    return-object p0
.end method

.method public addMessageAttributesEntry(Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/amazonaws/services/sqs/model/MessageAttributeValue;

    .prologue
    .line 580
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 581
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicated keys ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") are provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :cond_1
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    return-object p0
.end method

.method public clearAttributesEntries()Lcom/amazonaws/services/sqs/model/Message;
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    .line 422
    return-object p0
.end method

.method public clearMessageAttributesEntries()Lcom/amazonaws/services/sqs/model/Message;
    .locals 1

    .prologue
    .line 597
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    .line 598
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 652
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 695
    :cond_0
    :goto_0
    return v3

    .line 654
    :cond_1
    if-eqz p1, :cond_0

    .line 657
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/Message;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 659
    check-cast v0, Lcom/amazonaws/services/sqs/model/Message;

    .line 661
    .local v0, "other":Lcom/amazonaws/services/sqs/model/Message;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 663
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 664
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 666
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 668
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 669
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 673
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 674
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_f

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 678
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    :cond_5
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_11

    move v1, v2

    :goto_9
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_12

    move v4, v2

    :goto_a
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 682
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 683
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 685
    :cond_6
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_13

    move v1, v2

    :goto_b
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_14

    move v4, v2

    :goto_c
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 687
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 688
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 690
    :cond_7
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_15

    move v1, v2

    :goto_d
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_16

    move v4, v2

    :goto_e
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 692
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 693
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_8
    move v3, v2

    .line 695
    goto/16 :goto_0

    :cond_9
    move v1, v3

    .line 661
    goto/16 :goto_1

    :cond_a
    move v4, v3

    goto/16 :goto_2

    :cond_b
    move v1, v3

    .line 666
    goto/16 :goto_3

    :cond_c
    move v4, v3

    goto/16 :goto_4

    :cond_d
    move v1, v3

    .line 671
    goto/16 :goto_5

    :cond_e
    move v4, v3

    goto/16 :goto_6

    :cond_f
    move v1, v3

    .line 676
    goto/16 :goto_7

    :cond_10
    move v4, v3

    goto/16 :goto_8

    :cond_11
    move v1, v3

    .line 680
    goto/16 :goto_9

    :cond_12
    move v4, v3

    goto :goto_a

    :cond_13
    move v1, v3

    .line 685
    goto :goto_b

    :cond_14
    move v4, v3

    goto :goto_c

    :cond_15
    move v1, v3

    .line 690
    goto :goto_d

    :cond_16
    move v4, v3

    goto :goto_e
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getMD5OfBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->mD5OfBody:Ljava/lang/String;

    return-object v0
.end method

.method public getMD5OfMessageAttributes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->mD5OfMessageAttributes:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 511
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getMessageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->messageId:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiptHandle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/Message;->receiptHandle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 632
    const/16 v1, 0x1f

    .line 633
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 635
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 636
    mul-int/lit8 v4, v0, 0x1f

    .line 637
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 638
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 639
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v0, v4, v2

    .line 640
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v0, v4, v2

    .line 641
    mul-int/lit8 v4, v0, 0x1f

    .line 643
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    move v2, v3

    .line 644
    :goto_5
    add-int v0, v4, v2

    .line 645
    mul-int/lit8 v2, v0, 0x1f

    .line 646
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_6

    :goto_6
    add-int v0, v2, v3

    .line 647
    return v0

    .line 635
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 637
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 638
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 639
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 640
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->hashCode()I

    move-result v2

    goto :goto_4

    .line 643
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v2

    .line 644
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    .line 646
    :cond_6
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->hashCode()I

    move-result v3

    goto :goto_6
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    .line 348
    return-void
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->body:Ljava/lang/String;

    .line 277
    return-void
.end method

.method public setMD5OfBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "mD5OfBody"    # Ljava/lang/String;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->mD5OfBody:Ljava/lang/String;

    .line 232
    return-void
.end method

.method public setMD5OfMessageAttributes(Ljava/lang/String;)V
    .locals 0
    .param p1, "mD5OfMessageAttributes"    # Ljava/lang/String;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->mD5OfMessageAttributes:Ljava/lang/String;

    .line 465
    return-void
.end method

.method public setMessageAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 531
    .local p1, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    .line 532
    return-void
.end method

.method public setMessageId(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageId"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->messageId:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public setReceiptHandle(Ljava/lang/String;)V
    .locals 0
    .param p1, "receiptHandle"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->receiptHandle:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 611
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 613
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 615
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReceiptHandle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getReceiptHandle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 617
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MD5OfBody: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 619
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Body: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 621
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 623
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MD5OfMessageAttributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 625
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageAttributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    :cond_6
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/Message;"
        }
    .end annotation

    .prologue
    .line 378
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->attributes:Ljava/util/Map;

    .line 379
    return-object p0
.end method

.method public withBody(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->body:Ljava/lang/String;

    .line 295
    return-object p0
.end method

.method public withMD5OfBody(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .param p1, "mD5OfBody"    # Ljava/lang/String;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->mD5OfBody:Ljava/lang/String;

    .line 250
    return-object p0
.end method

.method public withMD5OfMessageAttributes(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .param p1, "mD5OfMessageAttributes"    # Ljava/lang/String;

    .prologue
    .line 490
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->mD5OfMessageAttributes:Ljava/lang/String;

    .line 491
    return-object p0
.end method

.method public withMessageAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/Message;"
        }
    .end annotation

    .prologue
    .line 557
    .local p1, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->messageAttributes:Ljava/util/Map;

    .line 558
    return-object p0
.end method

.method public withMessageId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .param p1, "messageId"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->messageId:Ljava/lang/String;

    .line 142
    return-object p0
.end method

.method public withReceiptHandle(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 0
    .param p1, "receiptHandle"    # Ljava/lang/String;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/Message;->receiptHandle:Ljava/lang/String;

    .line 205
    return-object p0
.end method
