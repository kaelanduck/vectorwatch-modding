.class public Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
.super Ljava/lang/Object;
.source "ListDeadLetterSourceQueuesResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private queueUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->queueUrls:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 144
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 158
    :cond_0
    :goto_0
    return v3

    .line 146
    :cond_1
    if-eqz p1, :cond_0

    .line 149
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 151
    check-cast v0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;

    .line 153
    .local v0, "other":Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_4

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 155
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 156
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    move v3, v2

    .line 158
    goto :goto_0

    :cond_3
    move v1, v3

    .line 153
    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2
.end method

.method public getQueueUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->queueUrls:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 135
    const/16 v1, 0x1f

    .line 136
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 138
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 139
    return v0

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public setQueueUrls(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "queueUrls":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->queueUrls:Ljava/util/List;

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->queueUrls:Ljava/util/List;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queueUrls: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_0
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withQueueUrls(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "queueUrls":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->setQueueUrls(Ljava/util/Collection;)V

    .line 113
    return-object p0
.end method

.method public varargs withQueueUrls([Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    .locals 4
    .param p1, "queueUrls"    # [Ljava/lang/String;

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->queueUrls:Ljava/util/List;

    .line 89
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 90
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->queueUrls:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method
