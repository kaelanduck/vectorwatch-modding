.class public Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "GetQueueUrlRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private queueName:Ljava/lang/String;

.field private queueOwnerAWSAccountId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "queueName"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 79
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->setQueueName(Ljava/lang/String;)V

    .line 80
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 233
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 252
    :cond_0
    :goto_0
    return v3

    .line 235
    :cond_1
    if-eqz p1, :cond_0

    .line 238
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 240
    check-cast v0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;

    .line 242
    .local v0, "other":Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 244
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 245
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 249
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 250
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    .line 252
    goto :goto_0

    :cond_4
    move v1, v3

    .line 242
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v1, v3

    .line 247
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4
.end method

.method public getQueueName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->queueName:Ljava/lang/String;

    return-object v0
.end method

.method public getQueueOwnerAWSAccountId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->queueOwnerAWSAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 220
    const/16 v1, 0x1f

    .line 221
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 223
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 224
    mul-int/lit8 v2, v0, 0x1f

    .line 226
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 227
    :goto_1
    add-int v0, v2, v3

    .line 228
    return v0

    .line 223
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 226
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v3

    .line 227
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setQueueName(Ljava/lang/String;)V
    .locals 0
    .param p1, "queueName"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->queueName:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setQueueOwnerAWSAccountId(Ljava/lang/String;)V
    .locals 0
    .param p1, "queueOwnerAWSAccountId"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->queueOwnerAWSAccountId:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueueName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueueOwnerAWSAccountId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withQueueName(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
    .locals 0
    .param p1, "queueName"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->queueName:Ljava/lang/String;

    .line 151
    return-object p0
.end method

.method public withQueueOwnerAWSAccountId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
    .locals 0
    .param p1, "queueOwnerAWSAccountId"    # Ljava/lang/String;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->queueOwnerAWSAccountId:Ljava/lang/String;

    .line 196
    return-object p0
.end method
