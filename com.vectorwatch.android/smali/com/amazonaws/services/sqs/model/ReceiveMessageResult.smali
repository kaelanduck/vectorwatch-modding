.class public Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
.super Ljava/lang/Object;
.source "ReceiveMessageResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private messages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->messages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 135
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 148
    :cond_0
    :goto_0
    return v3

    .line 137
    :cond_1
    if-eqz p1, :cond_0

    .line 140
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 142
    check-cast v0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    .line 144
    .local v0, "other":Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_4

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 146
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    move v3, v2

    .line 148
    goto :goto_0

    :cond_3
    move v1, v3

    .line 144
    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2
.end method

.method public getMessages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->messages:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 126
    const/16 v1, 0x1f

    .line 127
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 129
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 130
    return v0

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public setMessages(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "messages":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/Message;>;"
    if-nez p1, :cond_0

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->messages:Ljava/util/List;

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->messages:Ljava/util/List;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Messages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withMessages(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/Message;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "messages":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/Message;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->setMessages(Ljava/util/Collection;)V

    .line 104
    return-object p0
.end method

.method public varargs withMessages([Lcom/amazonaws/services/sqs/model/Message;)Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    .locals 4
    .param p1, "messages"    # [Lcom/amazonaws/services/sqs/model/Message;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->messages:Ljava/util/List;

    .line 82
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 83
    .local v0, "value":Lcom/amazonaws/services/sqs/model/Message;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->messages:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    .end local v0    # "value":Lcom/amazonaws/services/sqs/model/Message;
    :cond_1
    return-object p0
.end method
