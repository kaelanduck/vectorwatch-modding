.class public Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "ReceiveMessageRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private attributeNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private maxNumberOfMessages:Ljava/lang/Integer;

.field private messageAttributeNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private queueUrl:Ljava/lang/String;

.field private receiveRequestAttemptId:Ljava/lang/String;

.field private visibilityTimeout:Ljava/lang/Integer;

.field private waitTimeSeconds:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 421
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    .line 422
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    .line 437
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 438
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2803
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 2846
    :cond_0
    :goto_0
    return v3

    .line 2805
    :cond_1
    if-eqz p1, :cond_0

    .line 2808
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 2810
    check-cast v0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;

    .line 2812
    .local v0, "other":Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2814
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2816
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_c

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2818
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2819
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2821
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_d

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_e

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2823
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2824
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2826
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_f

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_10

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2828
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2829
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2831
    :cond_5
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_11

    move v1, v2

    :goto_9
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_12

    move v4, v2

    :goto_a
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2833
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2834
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2836
    :cond_6
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_13

    move v1, v2

    :goto_b
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_14

    move v4, v2

    :goto_c
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2838
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2839
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2841
    :cond_7
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_15

    move v1, v2

    :goto_d
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_16

    move v4, v2

    :goto_e
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 2843
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 2844
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_8
    move v3, v2

    .line 2846
    goto/16 :goto_0

    :cond_9
    move v1, v3

    .line 2812
    goto/16 :goto_1

    :cond_a
    move v4, v3

    goto/16 :goto_2

    :cond_b
    move v1, v3

    .line 2816
    goto/16 :goto_3

    :cond_c
    move v4, v3

    goto/16 :goto_4

    :cond_d
    move v1, v3

    .line 2821
    goto/16 :goto_5

    :cond_e
    move v4, v3

    goto/16 :goto_6

    :cond_f
    move v1, v3

    .line 2826
    goto/16 :goto_7

    :cond_10
    move v4, v3

    goto/16 :goto_8

    :cond_11
    move v1, v3

    .line 2831
    goto/16 :goto_9

    :cond_12
    move v4, v3

    goto :goto_a

    :cond_13
    move v1, v3

    .line 2836
    goto :goto_b

    :cond_14
    move v4, v3

    goto :goto_c

    :cond_15
    move v1, v3

    .line 2841
    goto :goto_d

    :cond_16
    move v4, v3

    goto :goto_e
.end method

.method public getAttributeNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 805
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    return-object v0
.end method

.method public getMaxNumberOfMessages()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1959
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->maxNumberOfMessages:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMessageAttributeNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    return-object v0
.end method

.method public getQueueUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->queueUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiveRequestAttemptId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2329
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->receiveRequestAttemptId:Ljava/lang/String;

    return-object v0
.end method

.method public getVisibilityTimeout()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2028
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->visibilityTimeout:Ljava/lang/Integer;

    return-object v0
.end method

.method public getWaitTimeSeconds()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2085
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->waitTimeSeconds:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2779
    const/16 v1, 0x1f

    .line 2780
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 2782
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 2783
    mul-int/lit8 v4, v0, 0x1f

    .line 2784
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 2785
    mul-int/lit8 v4, v0, 0x1f

    .line 2787
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 2788
    mul-int/lit8 v4, v0, 0x1f

    .line 2789
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v0, v4, v2

    .line 2790
    mul-int/lit8 v4, v0, 0x1f

    .line 2791
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v0, v4, v2

    .line 2792
    mul-int/lit8 v4, v0, 0x1f

    .line 2793
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_5

    move v2, v3

    :goto_5
    add-int v0, v4, v2

    .line 2794
    mul-int/lit8 v2, v0, 0x1f

    .line 2796
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    .line 2797
    :goto_6
    add-int v0, v2, v3

    .line 2798
    return v0

    .line 2782
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 2784
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_1

    .line 2787
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_2

    .line 2789
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    goto :goto_3

    .line 2791
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    goto :goto_4

    .line 2793
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    goto :goto_5

    .line 2796
    :cond_6
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v3

    .line 2797
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_6
.end method

.method public setAttributeNames(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1111
    .local p1, "attributeNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    .line 1117
    :goto_0
    return-void

    .line 1116
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    goto :goto_0
.end method

.method public setMaxNumberOfMessages(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "maxNumberOfMessages"    # Ljava/lang/Integer;

    .prologue
    .line 1982
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->maxNumberOfMessages:Ljava/lang/Integer;

    .line 1983
    return-void
.end method

.method public setMessageAttributeNames(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1828
    .local p1, "messageAttributeNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1829
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    .line 1834
    :goto_0
    return-void

    .line 1833
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    goto :goto_0
.end method

.method public setQueueUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "queueUrl"    # Ljava/lang/String;

    .prologue
    .line 475
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->queueUrl:Ljava/lang/String;

    .line 476
    return-void
.end method

.method public setReceiveRequestAttemptId(Ljava/lang/String;)V
    .locals 0
    .param p1, "receiveRequestAttemptId"    # Ljava/lang/String;

    .prologue
    .line 2534
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->receiveRequestAttemptId:Ljava/lang/String;

    .line 2535
    return-void
.end method

.method public setVisibilityTimeout(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "visibilityTimeout"    # Ljava/lang/Integer;

    .prologue
    .line 2045
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->visibilityTimeout:Ljava/lang/Integer;

    .line 2046
    return-void
.end method

.method public setWaitTimeSeconds(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "waitTimeSeconds"    # Ljava/lang/Integer;

    .prologue
    .line 2103
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->waitTimeSeconds:Ljava/lang/Integer;

    .line 2104
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2757
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2758
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2759
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2760
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueueUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2761
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2762
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AttributeNames: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2763
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2764
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageAttributeNames: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2765
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2766
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MaxNumberOfMessages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMaxNumberOfMessages()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2767
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2768
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VisibilityTimeout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2769
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2770
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WaitTimeSeconds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getWaitTimeSeconds()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2771
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2772
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReceiveRequestAttemptId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getReceiveRequestAttemptId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2773
    :cond_6
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2774
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAttributeNames(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;"
        }
    .end annotation

    .prologue
    .line 1744
    .local p1, "attributeNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->setAttributeNames(Ljava/util/Collection;)V

    .line 1745
    return-object p0
.end method

.method public varargs withAttributeNames([Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 4
    .param p1, "attributeNames"    # [Ljava/lang/String;

    .prologue
    .line 1427
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1428
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    .line 1430
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 1431
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->attributeNames:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1430
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1433
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method public withMaxNumberOfMessages(Ljava/lang/Integer;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .param p1, "maxNumberOfMessages"    # Ljava/lang/Integer;

    .prologue
    .line 2010
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->maxNumberOfMessages:Ljava/lang/Integer;

    .line 2011
    return-object p0
.end method

.method public withMessageAttributeNames(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;"
        }
    .end annotation

    .prologue
    .line 1935
    .local p1, "messageAttributeNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->setMessageAttributeNames(Ljava/util/Collection;)V

    .line 1936
    return-object p0
.end method

.method public varargs withMessageAttributeNames([Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 4
    .param p1, "messageAttributeNames"    # [Ljava/lang/String;

    .prologue
    .line 1880
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1881
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    .line 1884
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 1885
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->messageAttributeNames:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1884
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1887
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method public withQueueUrl(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .param p1, "queueUrl"    # Ljava/lang/String;

    .prologue
    .line 499
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->queueUrl:Ljava/lang/String;

    .line 500
    return-object p0
.end method

.method public withReceiveRequestAttemptId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .param p1, "receiveRequestAttemptId"    # Ljava/lang/String;

    .prologue
    .line 2744
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->receiveRequestAttemptId:Ljava/lang/String;

    .line 2745
    return-object p0
.end method

.method public withVisibilityTimeout(Ljava/lang/Integer;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .param p1, "visibilityTimeout"    # Ljava/lang/Integer;

    .prologue
    .line 2067
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->visibilityTimeout:Ljava/lang/Integer;

    .line 2068
    return-object p0
.end method

.method public withWaitTimeSeconds(Ljava/lang/Integer;)Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .locals 0
    .param p1, "waitTimeSeconds"    # Ljava/lang/Integer;

    .prologue
    .line 2126
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->waitTimeSeconds:Ljava/lang/Integer;

    .line 2127
    return-object p0
.end method
