.class public Lcom/amazonaws/services/sqs/model/ListQueuesResult;
.super Ljava/lang/Object;
.source "ListQueuesResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private queueUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->queueUrls:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 135
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 149
    :cond_0
    :goto_0
    return v3

    .line 137
    :cond_1
    if-eqz p1, :cond_0

    .line 140
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/ListQueuesResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 142
    check-cast v0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;

    .line 144
    .local v0, "other":Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_4

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 146
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 147
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    move v3, v2

    .line 149
    goto :goto_0

    :cond_3
    move v1, v3

    .line 144
    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2
.end method

.method public getQueueUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->queueUrls:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 126
    const/16 v1, 0x1f

    .line 127
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 129
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 130
    return v0

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public setQueueUrls(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "queueUrls":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->queueUrls:Ljava/util/List;

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->queueUrls:Ljava/util/List;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QueueUrls: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withQueueUrls(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/ListQueuesResult;"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "queueUrls":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->setQueueUrls(Ljava/util/Collection;)V

    .line 104
    return-object p0
.end method

.method public varargs withQueueUrls([Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    .locals 4
    .param p1, "queueUrls"    # [Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->getQueueUrls()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->queueUrls:Ljava/util/List;

    .line 82
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 83
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/ListQueuesResult;->queueUrls:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method
