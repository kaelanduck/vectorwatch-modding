.class public Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
.super Ljava/lang/Object;
.source "MessageAttributeValue.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private binaryListValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private binaryValue:Ljava/nio/ByteBuffer;

.field private dataType:Ljava/lang/String;

.field private stringListValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private stringValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringListValues:Ljava/util/List;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryListValues:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 481
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 514
    :cond_0
    :goto_0
    return v3

    .line 483
    :cond_1
    if-eqz p1, :cond_0

    .line 486
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 488
    check-cast v0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;

    .line 490
    .local v0, "other":Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 492
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 493
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 495
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 497
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 498
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_c

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 502
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 503
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 505
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_d

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_e

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 507
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 508
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 510
    :cond_5
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_f

    move v1, v2

    :goto_9
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    move v4, v2

    :goto_a
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 512
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_6
    move v3, v2

    .line 514
    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 490
    goto/16 :goto_1

    :cond_8
    move v4, v3

    goto/16 :goto_2

    :cond_9
    move v1, v3

    .line 495
    goto/16 :goto_3

    :cond_a
    move v4, v3

    goto/16 :goto_4

    :cond_b
    move v1, v3

    .line 500
    goto :goto_5

    :cond_c
    move v4, v3

    goto :goto_6

    :cond_d
    move v1, v3

    .line 505
    goto :goto_7

    :cond_e
    move v4, v3

    goto :goto_8

    :cond_f
    move v1, v3

    .line 510
    goto :goto_9

    :cond_10
    move v4, v3

    goto :goto_a
.end method

.method public getBinaryListValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryListValues:Ljava/util/List;

    return-object v0
.end method

.method public getBinaryValue()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryValue:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->dataType:Ljava/lang/String;

    return-object v0
.end method

.method public getStringListValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringListValues:Ljava/util/List;

    return-object v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringValue:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 464
    const/16 v1, 0x1f

    .line 465
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 468
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 469
    mul-int/lit8 v4, v0, 0x1f

    .line 470
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 471
    mul-int/lit8 v4, v0, 0x1f

    .line 472
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 473
    mul-int/lit8 v4, v0, 0x1f

    .line 474
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v0, v4, v2

    .line 475
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    :goto_4
    add-int v0, v2, v3

    .line 476
    return v0

    .line 468
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 470
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hashCode()I

    move-result v2

    goto :goto_1

    .line 472
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_2

    .line 474
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_3

    .line 475
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_4
.end method

.method public setBinaryListValues(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "binaryListValues":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/nio/ByteBuffer;>;"
    if-nez p1, :cond_0

    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryListValues:Ljava/util/List;

    .line 297
    :goto_0
    return-void

    .line 296
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryListValues:Ljava/util/List;

    goto :goto_0
.end method

.method public setBinaryValue(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "binaryValue"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryValue:Ljava/nio/ByteBuffer;

    .line 171
    return-void
.end method

.method public setDataType(Ljava/lang/String;)V
    .locals 0
    .param p1, "dataType"    # Ljava/lang/String;

    .prologue
    .line 399
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->dataType:Ljava/lang/String;

    .line 400
    return-void
.end method

.method public setStringListValues(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "stringListValues":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringListValues:Ljava/util/List;

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringListValues:Ljava/util/List;

    goto :goto_0
.end method

.method public setStringValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "stringValue"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringValue:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 446
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 447
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 449
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StringValue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 451
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BinaryValue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 453
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StringListValues: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 454
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 455
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BinaryListValues: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 457
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    :cond_4
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withBinaryListValues(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;"
        }
    .end annotation

    .prologue
    .line 340
    .local p1, "binaryListValues":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/nio/ByteBuffer;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->setBinaryListValues(Ljava/util/Collection;)V

    .line 341
    return-object p0
.end method

.method public varargs withBinaryListValues([Ljava/nio/ByteBuffer;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 4
    .param p1, "binaryListValues"    # [Ljava/nio/ByteBuffer;

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 315
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryListValues:Ljava/util/List;

    .line 318
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 319
    .local v0, "value":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryListValues:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 321
    .end local v0    # "value":Ljava/nio/ByteBuffer;
    :cond_1
    return-object p0
.end method

.method public withBinaryValue(Ljava/nio/ByteBuffer;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 0
    .param p1, "binaryValue"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->binaryValue:Ljava/nio/ByteBuffer;

    .line 191
    return-object p0
.end method

.method public withDataType(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 0
    .param p1, "dataType"    # Ljava/lang/String;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->dataType:Ljava/lang/String;

    .line 434
    return-object p0
.end method

.method public withStringListValues(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;"
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "stringListValues":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->setStringListValues(Ljava/util/Collection;)V

    .line 265
    return-object p0
.end method

.method public varargs withStringListValues([Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 4
    .param p1, "stringListValues"    # [Ljava/lang/String;

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringListValues:Ljava/util/List;

    .line 243
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 244
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringListValues:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 246
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method public withStringValue(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .locals 0
    .param p1, "stringValue"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->stringValue:Ljava/lang/String;

    .line 140
    return-object p0
.end method
