.class public Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "ListDeadLetterSourceQueuesResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;

    .line 70
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    .locals 8
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 37
    new-instance v0, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;-><init>()V

    .line 39
    .local v0, "listDeadLetterSourceQueuesResult":Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 40
    .local v1, "originalDepth":I
    add-int/lit8 v2, v1, 0x1

    .line 42
    .local v2, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 43
    add-int/lit8 v2, v2, 0x2

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v3

    .line 47
    .local v3, "xmlEvent":I
    if-ne v3, v7, :cond_1

    .line 62
    :goto_1
    return-object v0

    .line 50
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 51
    const-string v4, "QueueUrl"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 52
    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 53
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 52
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;->withQueueUrls([Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;

    goto :goto_0

    .line 56
    :cond_2
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v4

    if-ge v4, v1, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;

    move-result-object v0

    return-object v0
.end method
