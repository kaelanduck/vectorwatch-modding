.class public Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlRequestMarshaller;
.super Ljava/lang/Object;
.source "GetQueueUrlRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;",
        ">;",
        "Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;)Lcom/amazonaws/Request;
    .locals 6
    .param p1, "getQueueUrlRequest"    # Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v4, Lcom/amazonaws/AmazonClientException;

    const-string v5, "Invalid argument passed to marshall(GetQueueUrlRequest)"

    invoke-direct {v4, v5}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 37
    :cond_0
    new-instance v3, Lcom/amazonaws/DefaultRequest;

    const-string v4, "AmazonSQS"

    invoke-direct {v3, p1, v4}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 39
    .local v3, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;>;"
    const-string v4, "Action"

    const-string v5, "GetQueueUrl"

    invoke-interface {v3, v4, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v4, "Version"

    const-string v5, "2012-11-05"

    invoke-interface {v3, v4, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 44
    const-string v0, "QueueName"

    .line 45
    .local v0, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueName()Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "queueName":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .end local v0    # "prefix":Ljava/lang/String;
    .end local v1    # "queueName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 49
    const-string v0, "QueueOwnerAWSAccountId"

    .line 50
    .restart local v0    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->getQueueOwnerAWSAccountId()Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "queueOwnerAWSAccountId":Ljava/lang/String;
    invoke-static {v2}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .end local v0    # "prefix":Ljava/lang/String;
    .end local v2    # "queueOwnerAWSAccountId":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
