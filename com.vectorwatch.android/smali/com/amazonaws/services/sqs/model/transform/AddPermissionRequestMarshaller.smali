.class public Lcom/amazonaws/services/sqs/model/transform/AddPermissionRequestMarshaller;
.super Ljava/lang/Object;
.source "AddPermissionRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sqs/model/AddPermissionRequest;",
        ">;",
        "Lcom/amazonaws/services/sqs/model/AddPermissionRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/AddPermissionRequest;)Lcom/amazonaws/Request;
    .locals 16
    .param p1, "addPermissionRequest"    # Lcom/amazonaws/services/sqs/model/AddPermissionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/AddPermissionRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sqs/model/AddPermissionRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v13, Lcom/amazonaws/AmazonClientException;

    const-string v14, "Invalid argument passed to marshall(AddPermissionRequest)"

    invoke-direct {v13, v14}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 37
    :cond_0
    new-instance v12, Lcom/amazonaws/DefaultRequest;

    const-string v13, "AmazonSQS"

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v13}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 39
    .local v12, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/AddPermissionRequest;>;"
    const-string v13, "Action"

    const-string v14, "AddPermission"

    invoke-interface {v12, v13, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v13, "Version"

    const-string v14, "2012-11-05"

    invoke-interface {v12, v13, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 44
    const-string v10, "QueueUrl"

    .line 45
    .local v10, "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v11

    .line 46
    .local v11, "queueUrl":Ljava/lang/String;
    invoke-static {v11}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v10, v13}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .end local v10    # "prefix":Ljava/lang/String;
    .end local v11    # "queueUrl":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 49
    const-string v10, "Label"

    .line 50
    .restart local v10    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v9

    .line 51
    .local v9, "label":Ljava/lang/String;
    invoke-static {v9}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v10, v13}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .end local v9    # "label":Ljava/lang/String;
    .end local v10    # "prefix":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 54
    const-string v10, "AWSAccountId"

    .line 55
    .restart local v10    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v1

    .line 56
    .local v1, "aWSAccountIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 57
    .local v2, "aWSAccountIdsIndex":I
    move-object v4, v10

    .line 58
    .local v4, "aWSAccountIdsPrefix":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 59
    .local v3, "aWSAccountIdsItem":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 60
    if-eqz v3, :cond_3

    .line 61
    invoke-static {v3}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v12, v10, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 64
    goto :goto_0

    .line 67
    .end local v1    # "aWSAccountIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "aWSAccountIdsIndex":I
    .end local v3    # "aWSAccountIdsItem":Ljava/lang/String;
    .end local v4    # "aWSAccountIdsPrefix":Ljava/lang/String;
    .end local v10    # "prefix":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getActions()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 68
    const-string v10, "ActionName"

    .line 69
    .restart local v10    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->getActions()Ljava/util/List;

    move-result-object v5

    .line 70
    .local v5, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x1

    .line 71
    .local v6, "actionsIndex":I
    move-object v8, v10

    .line 72
    .local v8, "actionsPrefix":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 73
    .local v7, "actionsItem":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 74
    if-eqz v7, :cond_5

    .line 75
    invoke-static {v7}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v12, v10, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_5
    add-int/lit8 v6, v6, 0x1

    .line 78
    goto :goto_1

    .line 82
    .end local v5    # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "actionsIndex":I
    .end local v7    # "actionsItem":Ljava/lang/String;
    .end local v8    # "actionsPrefix":Ljava/lang/String;
    .end local v10    # "prefix":Ljava/lang/String;
    :cond_6
    return-object v12
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/transform/AddPermissionRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/AddPermissionRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
