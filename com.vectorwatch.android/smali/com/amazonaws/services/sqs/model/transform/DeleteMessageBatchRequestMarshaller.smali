.class public Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestMarshaller;
.super Ljava/lang/Object;
.source "DeleteMessageBatchRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;",
        ">;",
        "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;)Lcom/amazonaws/Request;
    .locals 11
    .param p1, "deleteMessageBatchRequest"    # Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v7, Lcom/amazonaws/AmazonClientException;

    const-string v8, "Invalid argument passed to marshall(DeleteMessageBatchRequest)"

    invoke-direct {v7, v8}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 38
    :cond_0
    new-instance v6, Lcom/amazonaws/DefaultRequest;

    const-string v7, "AmazonSQS"

    invoke-direct {v6, p1, v7}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 40
    .local v6, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;>;"
    const-string v7, "Action"

    const-string v8, "DeleteMessageBatch"

    invoke-interface {v6, v7, v8}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v7, "Version"

    const-string v8, "2012-11-05"

    invoke-interface {v6, v7, v8}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 45
    const-string v4, "QueueUrl"

    .line 46
    .local v4, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getQueueUrl()Ljava/lang/String;

    move-result-object v5

    .line 47
    .local v5, "queueUrl":Ljava/lang/String;
    invoke-static {v5}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .end local v4    # "prefix":Ljava/lang/String;
    .end local v5    # "queueUrl":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 50
    const-string v4, "DeleteMessageBatchRequestEntry"

    .line 52
    .restart local v4    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v0

    .line 53
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    const/4 v1, 0x1

    .line 54
    .local v1, "entriesIndex":I
    move-object v3, v4

    .line 55
    .local v3, "entriesPrefix":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;

    .line 56
    .local v2, "entriesItem":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 57
    if-eqz v2, :cond_2

    .line 58
    invoke-static {}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;->getInstance()Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v2, v6, v9}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;Lcom/amazonaws/Request;Ljava/lang/String;)V

    .line 61
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 62
    goto :goto_0

    .line 66
    .end local v0    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    .end local v1    # "entriesIndex":I
    .end local v2    # "entriesItem":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;
    .end local v3    # "entriesPrefix":Ljava/lang/String;
    .end local v4    # "prefix":Ljava/lang/String;
    :cond_3
    return-object v6
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
