.class Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;
.super Ljava/lang/Object;
.source "MessageAttributeValueStaxMarshaller.java"


# static fields
.field private static instance:Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;

    .line 81
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/MessageAttributeValueStaxMarshaller;

    return-object v0
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/MessageAttributeValue;Lcom/amazonaws/Request;Ljava/lang/String;)V
    .locals 16
    .param p1, "_messageAttributeValue"    # Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .param p3, "_prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            "Lcom/amazonaws/Request",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<*>;"
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 31
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "StringValue"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 32
    .local v7, "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v12

    .line 33
    .local v12, "stringValue":Ljava/lang/String;
    invoke-static {v12}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-interface {v0, v7, v13}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .end local v7    # "prefix":Ljava/lang/String;
    .end local v12    # "stringValue":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 36
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "BinaryValue"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 37
    .restart local v7    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 38
    .local v5, "binaryValue":Ljava/nio/ByteBuffer;
    invoke-static {v5}, Lcom/amazonaws/util/StringUtils;->fromByteBuffer(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-interface {v0, v7, v13}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .end local v5    # "binaryValue":Ljava/nio/ByteBuffer;
    .end local v7    # "prefix":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 41
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "StringListValue"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 42
    .restart local v7    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v8

    .line 43
    .local v8, "stringListValues":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x1

    .line 44
    .local v9, "stringListValuesIndex":I
    move-object v11, v7

    .line 45
    .local v11, "stringListValuesPrefix":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 46
    .local v10, "stringListValuesItem":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 47
    if-eqz v10, :cond_2

    .line 48
    invoke-static {v10}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-interface {v0, v7, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_2
    add-int/lit8 v9, v9, 0x1

    .line 51
    goto :goto_0

    .line 54
    .end local v7    # "prefix":Ljava/lang/String;
    .end local v8    # "stringListValues":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "stringListValuesIndex":I
    .end local v10    # "stringListValuesItem":Ljava/lang/String;
    .end local v11    # "stringListValuesPrefix":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 55
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "BinaryListValue"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 57
    .restart local v7    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v1

    .line 58
    .local v1, "binaryListValues":Ljava/util/List;, "Ljava/util/List<Ljava/nio/ByteBuffer;>;"
    const/4 v2, 0x1

    .line 59
    .local v2, "binaryListValuesIndex":I
    move-object v4, v7

    .line 60
    .local v4, "binaryListValuesPrefix":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/nio/ByteBuffer;

    .line 61
    .local v3, "binaryListValuesItem":Ljava/nio/ByteBuffer;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 62
    if-eqz v3, :cond_4

    .line 63
    invoke-static {v3}, Lcom/amazonaws/util/StringUtils;->fromByteBuffer(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-interface {v0, v7, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 66
    goto :goto_1

    .line 69
    .end local v1    # "binaryListValues":Ljava/util/List;, "Ljava/util/List<Ljava/nio/ByteBuffer;>;"
    .end local v2    # "binaryListValuesIndex":I
    .end local v3    # "binaryListValuesItem":Ljava/nio/ByteBuffer;
    .end local v4    # "binaryListValuesPrefix":Ljava/lang/String;
    .end local v7    # "prefix":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 70
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "DataType"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 71
    .restart local v7    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v6

    .line 72
    .local v6, "dataType":Ljava/lang/String;
    invoke-static {v6}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-interface {v0, v7, v13}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .end local v6    # "dataType":Ljava/lang/String;
    .end local v7    # "prefix":Ljava/lang/String;
    :cond_6
    return-void
.end method
