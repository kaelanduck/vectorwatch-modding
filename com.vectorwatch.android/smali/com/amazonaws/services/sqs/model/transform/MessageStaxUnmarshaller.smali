.class Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;
.super Ljava/lang/Object;
.source "MessageStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$MessageAttributesMapEntryUnmarshaller;,
        Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$AttributesMapEntryUnmarshaller;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sqs/model/Message;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;

    .line 179
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sqs/model/Message;
    .locals 8
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    new-instance v2, Lcom/amazonaws/services/sqs/model/Message;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/Message;-><init>()V

    .line 119
    .local v2, "message":Lcom/amazonaws/services/sqs/model/Message;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v3

    .line 120
    .local v3, "originalDepth":I
    add-int/lit8 v4, v3, 0x1

    .line 122
    .local v4, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 123
    add-int/lit8 v4, v4, 0x2

    .line 126
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v5

    .line 127
    .local v5, "xmlEvent":I
    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 171
    :goto_1
    return-object v2

    .line 130
    :cond_1
    const/4 v6, 0x2

    if-ne v5, v6, :cond_8

    .line 131
    const-string v6, "MessageId"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 132
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/amazonaws/services/sqs/model/Message;->setMessageId(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_2
    const-string v6, "ReceiptHandle"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 136
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/amazonaws/services/sqs/model/Message;->setReceiptHandle(Ljava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_3
    const-string v6, "MD5OfBody"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 141
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/amazonaws/services/sqs/model/Message;->setMD5OfBody(Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_4
    const-string v6, "Body"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 145
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/amazonaws/services/sqs/model/Message;->setBody(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_5
    const-string v6, "Attribute"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 149
    invoke-static {}, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$AttributesMapEntryUnmarshaller;->getInstance()Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$AttributesMapEntryUnmarshaller;

    move-result-object v6

    .line 150
    invoke-virtual {v6, p1}, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$AttributesMapEntryUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 151
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v2, v6, v7}, Lcom/amazonaws/services/sqs/model/Message;->addAttributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/Message;

    goto :goto_0

    .line 154
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_6
    const-string v6, "MD5OfMessageAttributes"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 155
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v6

    .line 156
    invoke-virtual {v6, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v6

    .line 155
    invoke-virtual {v2, v6}, Lcom/amazonaws/services/sqs/model/Message;->setMD5OfMessageAttributes(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 159
    :cond_7
    const-string v6, "MessageAttribute"

    invoke-virtual {p1, v6, v4}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 161
    invoke-static {}, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$MessageAttributesMapEntryUnmarshaller;->getInstance()Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$MessageAttributesMapEntryUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller$MessageAttributesMapEntryUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 162
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;

    invoke-virtual {v2, v6, v7}, Lcom/amazonaws/services/sqs/model/Message;->addMessageAttributesEntry(Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;)Lcom/amazonaws/services/sqs/model/Message;

    goto/16 :goto_0

    .line 165
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    :cond_8
    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 166
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v6

    if-ge v6, v3, :cond_0

    goto/16 :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/transform/MessageStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sqs/model/Message;

    move-result-object v0

    return-object v0
.end method
