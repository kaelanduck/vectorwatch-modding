.class public Lcom/amazonaws/services/sqs/model/transform/ListQueuesRequestMarshaller;
.super Ljava/lang/Object;
.source "ListQueuesRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sqs/model/ListQueuesRequest;",
        ">;",
        "Lcom/amazonaws/services/sqs/model/ListQueuesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/ListQueuesRequest;)Lcom/amazonaws/Request;
    .locals 5
    .param p1, "listQueuesRequest"    # Lcom/amazonaws/services/sqs/model/ListQueuesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/ListQueuesRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sqs/model/ListQueuesRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v3, Lcom/amazonaws/AmazonClientException;

    const-string v4, "Invalid argument passed to marshall(ListQueuesRequest)"

    invoke-direct {v3, v4}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 37
    :cond_0
    new-instance v2, Lcom/amazonaws/DefaultRequest;

    const-string v3, "AmazonSQS"

    invoke-direct {v2, p1, v3}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 39
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/ListQueuesRequest;>;"
    const-string v3, "Action"

    const-string v4, "ListQueues"

    invoke-interface {v2, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v3, "Version"

    const-string v4, "2012-11-05"

    invoke-interface {v2, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;->getQueueNamePrefix()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 44
    const-string v0, "QueueNamePrefix"

    .line 45
    .local v0, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;->getQueueNamePrefix()Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "queueNamePrefix":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .end local v0    # "prefix":Ljava/lang/String;
    .end local v1    # "queueNamePrefix":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/transform/ListQueuesRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/ListQueuesRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
