.class Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;
.super Ljava/lang/Object;
.source "ChangeMessageVisibilityBatchRequestEntryStaxMarshaller.java"


# static fields
.field private static instance:Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;

    .line 54
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestEntryStaxMarshaller;

    return-object v0
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;Lcom/amazonaws/Request;Ljava/lang/String;)V
    .locals 6
    .param p1, "_changeMessageVisibilityBatchRequestEntry"    # Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;
    .param p3, "_prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;",
            "Lcom/amazonaws/Request",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<*>;"
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 32
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "id":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v1, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "prefix":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;->getReceiptHandle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 37
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ReceiptHandle"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    .restart local v1    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;->getReceiptHandle()Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "receiptHandle":Ljava/lang/String;
    invoke-static {v2}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v1, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .end local v1    # "prefix":Ljava/lang/String;
    .end local v2    # "receiptHandle":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 42
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "VisibilityTimeout"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    .restart local v1    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v3

    .line 45
    .local v3, "visibilityTimeout":Ljava/lang/Integer;
    invoke-static {v3}, Lcom/amazonaws/util/StringUtils;->fromInteger(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v1, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .end local v1    # "prefix":Ljava/lang/String;
    .end local v3    # "visibilityTimeout":Ljava/lang/Integer;
    :cond_2
    return-void
.end method
