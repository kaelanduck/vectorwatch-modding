.class Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;
.super Ljava/lang/Object;
.source "DeleteMessageBatchRequestEntryStaxMarshaller.java"


# static fields
.field private static instance:Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;

    .line 47
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;->instance:Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestEntryStaxMarshaller;

    return-object v0
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;Lcom/amazonaws/Request;Ljava/lang/String;)V
    .locals 5
    .param p1, "_deleteMessageBatchRequestEntry"    # Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;
    .param p3, "_prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            "Lcom/amazonaws/Request",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<*>;"
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 31
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "id":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v1, v3}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "prefix":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;->getReceiptHandle()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 36
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ReceiptHandle"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 37
    .restart local v1    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;->getReceiptHandle()Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "receiptHandle":Ljava/lang/String;
    invoke-static {v2}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v1, v3}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .end local v1    # "prefix":Ljava/lang/String;
    .end local v2    # "receiptHandle":Ljava/lang/String;
    :cond_1
    return-void
.end method
