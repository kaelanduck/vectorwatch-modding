.class public Lcom/amazonaws/services/sqs/model/SendMessageResult;
.super Ljava/lang/Object;
.source "SendMessageResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mD5OfMessageAttributes:Ljava/lang/String;

.field private mD5OfMessageBody:Ljava/lang/String;

.field private messageId:Ljava/lang/String;

.field private sequenceNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 422
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 451
    :cond_0
    :goto_0
    return v3

    .line 424
    :cond_1
    if-eqz p1, :cond_0

    .line 427
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/SendMessageResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 429
    check-cast v0, Lcom/amazonaws/services/sqs/model/SendMessageResult;

    .line 431
    .local v0, "other":Lcom/amazonaws/services/sqs/model/SendMessageResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 433
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 434
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 436
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_9

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 438
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 439
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 441
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 443
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 444
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_c

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_d

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 448
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 449
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    move v3, v2

    .line 451
    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 431
    goto/16 :goto_1

    :cond_7
    move v4, v3

    goto/16 :goto_2

    :cond_8
    move v1, v3

    .line 436
    goto :goto_3

    :cond_9
    move v4, v3

    goto :goto_4

    :cond_a
    move v1, v3

    .line 441
    goto :goto_5

    :cond_b
    move v4, v3

    goto :goto_6

    :cond_c
    move v1, v3

    .line 446
    goto :goto_7

    :cond_d
    move v4, v3

    goto :goto_8
.end method

.method public getMD5OfMessageAttributes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->mD5OfMessageAttributes:Ljava/lang/String;

    return-object v0
.end method

.method public getMD5OfMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->mD5OfMessageBody:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->messageId:Ljava/lang/String;

    return-object v0
.end method

.method public getSequenceNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->sequenceNumber:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 405
    const/16 v1, 0x1f

    .line 406
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 409
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 410
    mul-int/lit8 v4, v0, 0x1f

    .line 412
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    .line 413
    :goto_1
    add-int v0, v4, v2

    .line 414
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 415
    mul-int/lit8 v2, v0, 0x1f

    .line 416
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    :goto_3
    add-int v0, v2, v3

    .line 417
    return v0

    .line 409
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 412
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v2

    .line 413
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 414
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 416
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public setMD5OfMessageAttributes(Ljava/lang/String;)V
    .locals 0
    .param p1, "mD5OfMessageAttributes"    # Ljava/lang/String;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->mD5OfMessageAttributes:Ljava/lang/String;

    .line 183
    return-void
.end method

.method public setMD5OfMessageBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "mD5OfMessageBody"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->mD5OfMessageBody:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setMessageId(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageId"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->messageId:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public setSequenceNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "sequenceNumber"    # Ljava/lang/String;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->sequenceNumber:Ljava/lang/String;

    .line 341
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 390
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MD5OfMessageBody: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MD5OfMessageAttributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMessageId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SequenceNumber: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getSequenceNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_3
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withMD5OfMessageAttributes(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 0
    .param p1, "mD5OfMessageAttributes"    # Ljava/lang/String;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->mD5OfMessageAttributes:Ljava/lang/String;

    .line 209
    return-object p0
.end method

.method public withMD5OfMessageBody(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 0
    .param p1, "mD5OfMessageBody"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->mD5OfMessageBody:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public withMessageId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 0
    .param p1, "messageId"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->messageId:Ljava/lang/String;

    .line 278
    return-object p0
.end method

.method public withSequenceNumber(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 0
    .param p1, "sequenceNumber"    # Ljava/lang/String;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageResult;->sequenceNumber:Ljava/lang/String;

    .line 377
    return-object p0
.end method
