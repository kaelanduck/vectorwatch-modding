.class public Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
.super Ljava/lang/Object;
.source "SendMessageBatchRequestEntry.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private delaySeconds:Ljava/lang/Integer;

.field private id:Ljava/lang/String;

.field private messageAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field private messageBody:Ljava/lang/String;

.field private messageDeduplicationId:Ljava/lang/String;

.field private messageGroupId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    .line 219
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "messageBody"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    .line 236
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->setId(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->setMessageBody(Ljava/lang/String;)V

    .line 238
    return-void
.end method


# virtual methods
.method public addMessageAttributesEntry(Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/amazonaws/services/sqs/model/MessageAttributeValue;

    .prologue
    .line 537
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 538
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicated keys ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") are provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    return-object p0
.end method

.method public clearMessageAttributesEntries()Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    .line 555
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1515
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 1553
    :cond_0
    :goto_0
    return v3

    .line 1517
    :cond_1
    if-eqz p1, :cond_0

    .line 1520
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1522
    check-cast v0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    .line 1524
    .local v0, "other":Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_9

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1526
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1528
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1530
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1531
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1533
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_c

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_d

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1535
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1536
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1538
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_e

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_f

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1540
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1541
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1543
    :cond_5
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_10

    move v1, v2

    :goto_9
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_11

    move v4, v2

    :goto_a
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1545
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1546
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1548
    :cond_6
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_12

    move v1, v2

    :goto_b
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_13

    move v4, v2

    :goto_c
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1550
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1551
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_7
    move v3, v2

    .line 1553
    goto/16 :goto_0

    :cond_8
    move v1, v3

    .line 1524
    goto/16 :goto_1

    :cond_9
    move v4, v3

    goto/16 :goto_2

    :cond_a
    move v1, v3

    .line 1528
    goto/16 :goto_3

    :cond_b
    move v4, v3

    goto/16 :goto_4

    :cond_c
    move v1, v3

    .line 1533
    goto/16 :goto_5

    :cond_d
    move v4, v3

    goto/16 :goto_6

    :cond_e
    move v1, v3

    .line 1538
    goto :goto_7

    :cond_f
    move v4, v3

    goto :goto_8

    :cond_10
    move v1, v3

    .line 1543
    goto :goto_9

    :cond_11
    move v4, v3

    goto :goto_a

    :cond_12
    move v1, v3

    .line 1548
    goto :goto_b

    :cond_13
    move v4, v3

    goto :goto_c
.end method

.method public getDelaySeconds()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->delaySeconds:Ljava/lang/Integer;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageBody:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageDeduplicationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageDeduplicationId:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageGroupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageGroupId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1494
    const/16 v1, 0x1f

    .line 1495
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 1497
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 1498
    mul-int/lit8 v4, v0, 0x1f

    .line 1499
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 1500
    mul-int/lit8 v4, v0, 0x1f

    .line 1501
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 1502
    mul-int/lit8 v4, v0, 0x1f

    .line 1503
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v0, v4, v2

    .line 1504
    mul-int/lit8 v4, v0, 0x1f

    .line 1506
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    .line 1507
    :goto_4
    add-int v0, v4, v2

    .line 1508
    mul-int/lit8 v2, v0, 0x1f

    .line 1509
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    :goto_5
    add-int v0, v2, v3

    .line 1510
    return v0

    .line 1497
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 1499
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 1501
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    goto :goto_2

    .line 1503
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->hashCode()I

    move-result v2

    goto :goto_3

    .line 1506
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v2

    .line 1507
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 1509
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_5
.end method

.method public setDelaySeconds(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "delaySeconds"    # Ljava/lang/Integer;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->delaySeconds:Ljava/lang/Integer;

    .line 408
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->id:Ljava/lang/String;

    .line 272
    return-void
.end method

.method public setMessageAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 487
    .local p1, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    .line 488
    return-void
.end method

.method public setMessageBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageBody"    # Ljava/lang/String;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageBody:Ljava/lang/String;

    .line 321
    return-void
.end method

.method public setMessageDeduplicationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageDeduplicationId"    # Ljava/lang/String;

    .prologue
    .line 967
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageDeduplicationId:Ljava/lang/String;

    .line 968
    return-void
.end method

.method public setMessageGroupId(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageGroupId"    # Ljava/lang/String;

    .prologue
    .line 1364
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageGroupId:Ljava/lang/String;

    .line 1365
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1475
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1476
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1478
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1479
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageBody: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1480
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DelaySeconds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1482
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1483
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageAttributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1484
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1485
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageDeduplicationId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageDeduplicationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1486
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1487
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageGroupId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageGroupId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1488
    :cond_5
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1489
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withDelaySeconds(Ljava/lang/Integer;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 0
    .param p1, "delaySeconds"    # Ljava/lang/Integer;

    .prologue
    .line 446
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->delaySeconds:Ljava/lang/Integer;

    .line 447
    return-object p0
.end method

.method public withId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->id:Ljava/lang/String;

    .line 294
    return-object p0
.end method

.method public withMessageAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageAttributes:Ljava/util/Map;

    .line 514
    return-object p0
.end method

.method public withMessageBody(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 0
    .param p1, "messageBody"    # Ljava/lang/String;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageBody:Ljava/lang/String;

    .line 339
    return-object p0
.end method

.method public withMessageDeduplicationId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 0
    .param p1, "messageDeduplicationId"    # Ljava/lang/String;

    .prologue
    .line 1180
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageDeduplicationId:Ljava/lang/String;

    .line 1181
    return-object p0
.end method

.method public withMessageGroupId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    .locals 0
    .param p1, "messageGroupId"    # Ljava/lang/String;

    .prologue
    .line 1461
    iput-object p1, p0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->messageGroupId:Ljava/lang/String;

    .line 1462
    return-object p0
.end method
