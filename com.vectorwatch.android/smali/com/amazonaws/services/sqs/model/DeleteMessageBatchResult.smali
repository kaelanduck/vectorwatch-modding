.class public Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
.super Ljava/lang/Object;
.source "DeleteMessageBatchResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private failed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;",
            ">;"
        }
    .end annotation
.end field

.field private successful:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->successful:Ljava/util/List;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->failed:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 223
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 241
    :cond_0
    :goto_0
    return v3

    .line 225
    :cond_1
    if-eqz p1, :cond_0

    .line 228
    instance-of v1, p1, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 230
    check-cast v0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;

    .line 232
    .local v0, "other":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 234
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 235
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 239
    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    .line 241
    goto :goto_0

    :cond_4
    move v1, v3

    .line 232
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v1, v3

    .line 237
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4
.end method

.method public getFailed()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->failed:Ljava/util/List;

    return-object v0
.end method

.method public getSuccessful()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->successful:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 213
    const/16 v1, 0x1f

    .line 214
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 216
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 217
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_1
    add-int v0, v2, v3

    .line 218
    return v0

    .line 216
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0

    .line 217
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setFailed(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "failed":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;>;"
    if-nez p1, :cond_0

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->failed:Ljava/util/List;

    .line 147
    :goto_0
    return-void

    .line 146
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->failed:Ljava/util/List;

    goto :goto_0
.end method

.method public setSuccessful(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "successful":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;>;"
    if-nez p1, :cond_0

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->successful:Ljava/util/List;

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->successful:Ljava/util/List;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Successful: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withFailed(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "failed":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->setFailed(Ljava/util/Collection;)V

    .line 189
    return-object p0
.end method

.method public varargs withFailed([Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    .locals 4
    .param p1, "failed"    # [Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 165
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->failed:Ljava/util/List;

    .line 167
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 168
    .local v0, "value":Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->failed:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 170
    .end local v0    # "value":Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;
    :cond_1
    return-object p0
.end method

.method public withSuccessful(Ljava/util/Collection;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "successful":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->setSuccessful(Ljava/util/Collection;)V

    .line 115
    return-object p0
.end method

.method public varargs withSuccessful([Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    .locals 4
    .param p1, "successful"    # [Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->successful:Ljava/util/List;

    .line 92
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 93
    .local v0, "value":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;
    iget-object v3, p0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->successful:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "value":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;
    :cond_1
    return-object p0
.end method
