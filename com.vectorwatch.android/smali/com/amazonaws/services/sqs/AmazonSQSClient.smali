.class public Lcom/amazonaws/services/sqs/AmazonSQSClient;
.super Lcom/amazonaws/AmazonWebServiceClient;
.source "AmazonSQSClient.java"

# interfaces
.implements Lcom/amazonaws/services/sqs/AmazonSQS;


# instance fields
.field private awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

.field protected final exceptionUnmarshallers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/transform/Unmarshaller",
            "<",
            "Lcom/amazonaws/AmazonServiceException;",
            "Lorg/w3c/dom/Node;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 162
    new-instance v0, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;

    invoke-direct {v0}, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;-><init>()V

    new-instance v1, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v1}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 163
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/ClientConfiguration;)V
    .locals 1
    .param p1, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 186
    new-instance v0, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;

    invoke-direct {v0}, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 187
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentials;)V
    .locals 1
    .param p1, "awsCredentials"    # Lcom/amazonaws/auth/AWSCredentials;

    .prologue
    .line 211
    new-instance v0, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v0}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;-><init>(Lcom/amazonaws/auth/AWSCredentials;Lcom/amazonaws/ClientConfiguration;)V

    .line 212
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentials;Lcom/amazonaws/ClientConfiguration;)V
    .locals 1
    .param p1, "awsCredentials"    # Lcom/amazonaws/auth/AWSCredentials;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;

    .prologue
    .line 239
    new-instance v0, Lcom/amazonaws/internal/StaticCredentialsProvider;

    invoke-direct {v0, p1}, Lcom/amazonaws/internal/StaticCredentialsProvider;-><init>(Lcom/amazonaws/auth/AWSCredentials;)V

    invoke-direct {p0, v0, p2}, Lcom/amazonaws/services/sqs/AmazonSQSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 240
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;

    .prologue
    .line 265
    new-instance v0, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v0}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 266
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;

    .prologue
    .line 296
    new-instance v0, Lcom/amazonaws/http/UrlHttpClient;

    invoke-direct {v0, p2}, Lcom/amazonaws/http/UrlHttpClient;-><init>(Lcom/amazonaws/ClientConfiguration;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/http/HttpClient;)V

    .line 297
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/http/HttpClient;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;
    .param p3, "httpClient"    # Lcom/amazonaws/http/HttpClient;

    .prologue
    .line 344
    invoke-static {p2}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->adjustClientConfiguration(Lcom/amazonaws/ClientConfiguration;)Lcom/amazonaws/ClientConfiguration;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/amazonaws/AmazonWebServiceClient;-><init>(Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/http/HttpClient;)V

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    .line 346
    iput-object p1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

    .line 348
    invoke-direct {p0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->init()V

    .line 349
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/metrics/RequestMetricCollector;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;
    .param p3, "requestMetricCollector"    # Lcom/amazonaws/metrics/RequestMetricCollector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 319
    invoke-static {p2}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->adjustClientConfiguration(Lcom/amazonaws/ClientConfiguration;)Lcom/amazonaws/ClientConfiguration;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/amazonaws/AmazonWebServiceClient;-><init>(Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/metrics/RequestMetricCollector;)V

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    .line 321
    iput-object p1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

    .line 323
    invoke-direct {p0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->init()V

    .line 324
    return-void
.end method

.method private static adjustClientConfiguration(Lcom/amazonaws/ClientConfiguration;)Lcom/amazonaws/ClientConfiguration;
    .locals 1
    .param p0, "orig"    # Lcom/amazonaws/ClientConfiguration;

    .prologue
    .line 381
    move-object v0, p0

    .line 383
    .local v0, "config":Lcom/amazonaws/ClientConfiguration;
    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 352
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/BatchEntryIdsNotDistinctExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/BatchEntryIdsNotDistinctExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/BatchRequestTooLongExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/BatchRequestTooLongExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 354
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/EmptyBatchRequestExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/EmptyBatchRequestExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/InvalidAttributeNameExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/InvalidAttributeNameExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/InvalidBatchEntryIdExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/InvalidBatchEntryIdExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/InvalidIdFormatExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/InvalidIdFormatExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/InvalidMessageContentsExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/InvalidMessageContentsExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/MessageNotInflightExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/MessageNotInflightExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/OverLimitExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/OverLimitExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/PurgeQueueInProgressExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/PurgeQueueInProgressExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/QueueDeletedRecentlyExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/QueueDeletedRecentlyExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/QueueDoesNotExistExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/QueueDoesNotExistExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/QueueNameExistsExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/QueueNameExistsExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/ReceiptHandleIsInvalidExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/ReceiptHandleIsInvalidExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/TooManyEntriesInBatchRequestExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/TooManyEntriesInBatchRequestExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sqs/model/transform/UnsupportedOperationExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sqs/model/transform/UnsupportedOperationExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/transform/StandardErrorUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/transform/StandardErrorUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    const-string v1, "sqs.us-east-1.amazonaws.com"

    invoke-virtual {p0, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->setEndpoint(Ljava/lang/String;)V

    .line 373
    new-instance v0, Lcom/amazonaws/handlers/HandlerChainFactory;

    invoke-direct {v0}, Lcom/amazonaws/handlers/HandlerChainFactory;-><init>()V

    .line 374
    .local v0, "chainFactory":Lcom/amazonaws/handlers/HandlerChainFactory;
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->requestHandler2s:Ljava/util/List;

    const-string v2, "/com/amazonaws/services/sqs/request.handlers"

    invoke-virtual {v0, v2}, Lcom/amazonaws/handlers/HandlerChainFactory;->newRequestHandlerChain(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 376
    iget-object v1, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->requestHandler2s:Ljava/util/List;

    const-string v2, "/com/amazonaws/services/sqs/request.handler2s"

    invoke-virtual {v0, v2}, Lcom/amazonaws/handlers/HandlerChainFactory;->newRequestHandler2Chain(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 378
    return-void
.end method

.method private invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    .locals 5
    .param p3, "executionContext"    # Lcom/amazonaws/http/ExecutionContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            "Y:",
            "Lcom/amazonaws/AmazonWebServiceRequest;",
            ">(",
            "Lcom/amazonaws/Request",
            "<TY;>;",
            "Lcom/amazonaws/transform/Unmarshaller",
            "<TX;",
            "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
            ">;",
            "Lcom/amazonaws/http/ExecutionContext;",
            ")",
            "Lcom/amazonaws/Response",
            "<TX;>;"
        }
    .end annotation

    .prologue
    .line 2764
    .local p1, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<TY;>;"
    .local p2, "unmarshaller":Lcom/amazonaws/transform/Unmarshaller;, "Lcom/amazonaws/transform/Unmarshaller<TX;Lcom/amazonaws/transform/StaxUnmarshallerContext;>;"
    iget-object v4, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endpoint:Ljava/net/URI;

    invoke-interface {p1, v4}, Lcom/amazonaws/Request;->setEndpoint(Ljava/net/URI;)V

    .line 2765
    iget v4, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->timeOffset:I

    invoke-interface {p1, v4}, Lcom/amazonaws/Request;->setTimeOffset(I)V

    .line 2766
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v2

    .line 2768
    .local v2, "originalRequest":Lcom/amazonaws/AmazonWebServiceRequest;
    iget-object v4, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

    invoke-interface {v4}, Lcom/amazonaws/auth/AWSCredentialsProvider;->getCredentials()Lcom/amazonaws/auth/AWSCredentials;

    move-result-object v0

    .line 2769
    .local v0, "credentials":Lcom/amazonaws/auth/AWSCredentials;
    invoke-virtual {v2}, Lcom/amazonaws/AmazonWebServiceRequest;->getRequestCredentials()Lcom/amazonaws/auth/AWSCredentials;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2770
    invoke-virtual {v2}, Lcom/amazonaws/AmazonWebServiceRequest;->getRequestCredentials()Lcom/amazonaws/auth/AWSCredentials;

    move-result-object v0

    .line 2773
    :cond_0
    invoke-virtual {p3, v0}, Lcom/amazonaws/http/ExecutionContext;->setCredentials(Lcom/amazonaws/auth/AWSCredentials;)V

    .line 2775
    new-instance v3, Lcom/amazonaws/http/StaxResponseHandler;

    invoke-direct {v3, p2}, Lcom/amazonaws/http/StaxResponseHandler;-><init>(Lcom/amazonaws/transform/Unmarshaller;)V

    .line 2776
    .local v3, "responseHandler":Lcom/amazonaws/http/StaxResponseHandler;, "Lcom/amazonaws/http/StaxResponseHandler<TX;>;"
    new-instance v1, Lcom/amazonaws/http/DefaultErrorResponseHandler;

    iget-object v4, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->exceptionUnmarshallers:Ljava/util/List;

    invoke-direct {v1, v4}, Lcom/amazonaws/http/DefaultErrorResponseHandler;-><init>(Ljava/util/List;)V

    .line 2778
    .local v1, "errorResponseHandler":Lcom/amazonaws/http/DefaultErrorResponseHandler;
    iget-object v4, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->client:Lcom/amazonaws/http/AmazonHttpClient;

    invoke-virtual {v4, p1, v3, v1, p3}, Lcom/amazonaws/http/AmazonHttpClient;->execute(Lcom/amazonaws/Request;Lcom/amazonaws/http/HttpResponseHandler;Lcom/amazonaws/http/HttpResponseHandler;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public addPermission(Lcom/amazonaws/services/sqs/model/AddPermissionRequest;)V
    .locals 5
    .param p1, "addPermissionRequest"    # Lcom/amazonaws/services/sqs/model/AddPermissionRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 437
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 438
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 439
    const/4 v2, 0x0

    .line 440
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/AddPermissionRequest;>;"
    const/4 v3, 0x0

    .line 442
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/AddPermissionRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/AddPermissionRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/AddPermissionRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/AddPermissionRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 444
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 445
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 447
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 449
    return-void

    .line 447
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public addPermission(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2376
    .local p3, "aWSAccountIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;-><init>()V

    .line 2377
    .local v0, "addPermissionRequest":Lcom/amazonaws/services/sqs/model/AddPermissionRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2378
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->setLabel(Ljava/lang/String;)V

    .line 2379
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->setAWSAccountIds(Ljava/util/Collection;)V

    .line 2380
    invoke-virtual {v0, p4}, Lcom/amazonaws/services/sqs/model/AddPermissionRequest;->setActions(Ljava/util/Collection;)V

    .line 2381
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->addPermission(Lcom/amazonaws/services/sqs/model/AddPermissionRequest;)V

    .line 2382
    return-void
.end method

.method public changeMessageVisibility(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;)V
    .locals 5
    .param p1, "changeMessageVisibilityRequest"    # Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 514
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 515
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 516
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 517
    const/4 v2, 0x0

    .line 518
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;>;"
    const/4 v3, 0x0

    .line 520
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityRequestMarshaller;-><init>()V

    .line 521
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 523
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 524
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 528
    return-void

    .line 526
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public changeMessageVisibility(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .param p2, "receiptHandle"    # Ljava/lang/String;
    .param p3, "visibilityTimeout"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2461
    new-instance v0, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;-><init>()V

    .line 2462
    .local v0, "changeMessageVisibilityRequest":Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2463
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;->setReceiptHandle(Ljava/lang/String;)V

    .line 2464
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;->setVisibilityTimeout(Ljava/lang/Integer;)V

    .line 2465
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->changeMessageVisibility(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;)V

    .line 2466
    return-void
.end method

.method public changeMessageVisibilityBatch(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;)Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchResult;
    .locals 5
    .param p1, "changeMessageVisibilityBatchRequest"    # Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 578
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 579
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 580
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 581
    const/4 v2, 0x0

    .line 582
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;>;"
    const/4 v3, 0x0

    .line 584
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestMarshaller;-><init>()V

    .line 585
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 587
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 588
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ChangeMessageVisibilityBatchResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 590
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 590
    return-object v4

    .line 592
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public changeMessageVisibilityBatch(Ljava/lang/String;Ljava/util/List;)Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchResult;
    .locals 2
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2525
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequestEntry;>;"
    new-instance v0, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;-><init>()V

    .line 2526
    .local v0, "changeMessageVisibilityBatchRequest":Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2527
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 2528
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->changeMessageVisibilityBatch(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchRequest;)Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityBatchResult;

    move-result-object v1

    return-object v1
.end method

.method public createQueue(Lcom/amazonaws/services/sqs/model/CreateQueueRequest;)Lcom/amazonaws/services/sqs/model/CreateQueueResult;
    .locals 5
    .param p1, "createQueueRequest"    # Lcom/amazonaws/services/sqs/model/CreateQueueRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 685
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 686
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 687
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 688
    const/4 v2, 0x0

    .line 689
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/CreateQueueRequest;>;"
    const/4 v3, 0x0

    .line 691
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/CreateQueueResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/CreateQueueRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/CreateQueueRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/CreateQueueRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/CreateQueueRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 693
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 694
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/CreateQueueResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/CreateQueueResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 695
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/CreateQueueResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 695
    return-object v4

    .line 697
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public createQueue(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/CreateQueueResult;
    .locals 2
    .param p1, "queueName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1735
    new-instance v0, Lcom/amazonaws/services/sqs/model/CreateQueueRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/CreateQueueRequest;-><init>()V

    .line 1736
    .local v0, "createQueueRequest":Lcom/amazonaws/services/sqs/model/CreateQueueRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/CreateQueueRequest;->setQueueName(Ljava/lang/String;)V

    .line 1737
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createQueue(Lcom/amazonaws/services/sqs/model/CreateQueueRequest;)Lcom/amazonaws/services/sqs/model/CreateQueueResult;

    move-result-object v1

    return-object v1
.end method

.method public deleteMessage(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;)V
    .locals 5
    .param p1, "deleteMessageRequest"    # Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 745
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 746
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 747
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 748
    const/4 v2, 0x0

    .line 749
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;>;"
    const/4 v3, 0x0

    .line 751
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 753
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 754
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 756
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 758
    return-void

    .line 756
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public deleteMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .param p2, "receiptHandle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2159
    new-instance v0, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;-><init>()V

    .line 2160
    .local v0, "deleteMessageRequest":Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2161
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;->setReceiptHandle(Ljava/lang/String;)V

    .line 2162
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->deleteMessage(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;)V

    .line 2163
    return-void
.end method

.method public deleteMessageBatch(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    .locals 5
    .param p1, "deleteMessageBatchRequest"    # Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 805
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 806
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 807
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 808
    const/4 v2, 0x0

    .line 809
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;>;"
    const/4 v3, 0x0

    .line 811
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 813
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 814
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/DeleteMessageBatchResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 816
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 818
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 816
    return-object v4

    .line 818
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public deleteMessageBatch(Ljava/lang/String;Ljava/util/List;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    .locals 2
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2044
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    new-instance v0, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;-><init>()V

    .line 2045
    .local v0, "deleteMessageBatchRequest":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2046
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 2047
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->deleteMessageBatch(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;

    move-result-object v1

    return-object v1
.end method

.method public deleteQueue(Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;)V
    .locals 5
    .param p1, "deleteQueueRequest"    # Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 860
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 861
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 862
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 863
    const/4 v2, 0x0

    .line 864
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;>;"
    const/4 v3, 0x0

    .line 866
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/DeleteQueueRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/DeleteQueueRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/DeleteQueueRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 868
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 869
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 871
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 873
    return-void

    .line 871
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public deleteQueue(Ljava/lang/String;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1783
    new-instance v0, Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;-><init>()V

    .line 1784
    .local v0, "deleteQueueRequest":Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 1785
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->deleteQueue(Lcom/amazonaws/services/sqs/model/DeleteQueueRequest;)V

    .line 1786
    return-void
.end method

.method public getCachedResponseMetadata(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/ResponseMetadata;
    .locals 1
    .param p1, "request"    # Lcom/amazonaws/AmazonWebServiceRequest;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2758
    iget-object v0, p0, Lcom/amazonaws/services/sqs/AmazonSQSClient;->client:Lcom/amazonaws/http/AmazonHttpClient;

    invoke-virtual {v0, p1}, Lcom/amazonaws/http/AmazonHttpClient;->getResponseMetadataForRequest(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/ResponseMetadata;

    move-result-object v0

    return-object v0
.end method

.method public getQueueAttributes(Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;)Lcom/amazonaws/services/sqs/model/GetQueueAttributesResult;
    .locals 5
    .param p1, "getQueueAttributesRequest"    # Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 909
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 910
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 911
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 912
    const/4 v2, 0x0

    .line 913
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;>;"
    const/4 v3, 0x0

    .line 915
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/GetQueueAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/GetQueueAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/GetQueueAttributesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/GetQueueAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 917
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 918
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/GetQueueAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/GetQueueAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 920
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/GetQueueAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 922
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 920
    return-object v4

    .line 922
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getQueueAttributes(Ljava/lang/String;Ljava/util/List;)Lcom/amazonaws/services/sqs/model/GetQueueAttributesResult;
    .locals 2
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/GetQueueAttributesResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1616
    .local p2, "attributeNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;-><init>()V

    .line 1617
    .local v0, "getQueueAttributesRequest":Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 1618
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;->setAttributeNames(Ljava/util/Collection;)V

    .line 1619
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->getQueueAttributes(Lcom/amazonaws/services/sqs/model/GetQueueAttributesRequest;)Lcom/amazonaws/services/sqs/model/GetQueueAttributesResult;

    move-result-object v1

    return-object v1
.end method

.method public getQueueUrl(Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;)Lcom/amazonaws/services/sqs/model/GetQueueUrlResult;
    .locals 5
    .param p1, "getQueueUrlRequest"    # Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 955
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 956
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 957
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 958
    const/4 v2, 0x0

    .line 959
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;>;"
    const/4 v3, 0x0

    .line 961
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/GetQueueUrlResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 963
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 964
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/GetQueueUrlResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 965
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/GetQueueUrlResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 967
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 965
    return-object v4

    .line 967
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getQueueUrl(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/GetQueueUrlResult;
    .locals 2
    .param p1, "queueName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1824
    new-instance v0, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;-><init>()V

    .line 1825
    .local v0, "getQueueUrlRequest":Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;->setQueueName(Ljava/lang/String;)V

    .line 1826
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->getQueueUrl(Lcom/amazonaws/services/sqs/model/GetQueueUrlRequest;)Lcom/amazonaws/services/sqs/model/GetQueueUrlResult;

    move-result-object v1

    return-object v1
.end method

.method public listDeadLetterSourceQueues(Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesRequest;)Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    .locals 5
    .param p1, "listDeadLetterSourceQueuesRequest"    # Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 999
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1000
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1001
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1002
    const/4 v2, 0x0

    .line 1003
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesRequest;>;"
    const/4 v3, 0x0

    .line 1005
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesRequestMarshaller;-><init>()V

    .line 1006
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1008
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1009
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ListDeadLetterSourceQueuesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1011
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/ListDeadLetterSourceQueuesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1013
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1011
    return-object v4

    .line 1013
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listQueues()Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2070
    new-instance v0, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;-><init>()V

    .line 2071
    .local v0, "listQueuesRequest":Lcom/amazonaws/services/sqs/model/ListQueuesRequest;
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->listQueues(Lcom/amazonaws/services/sqs/model/ListQueuesRequest;)Lcom/amazonaws/services/sqs/model/ListQueuesResult;

    move-result-object v1

    return-object v1
.end method

.method public listQueues(Lcom/amazonaws/services/sqs/model/ListQueuesRequest;)Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    .locals 5
    .param p1, "listQueuesRequest"    # Lcom/amazonaws/services/sqs/model/ListQueuesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1038
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1039
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1040
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1041
    const/4 v2, 0x0

    .line 1042
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/ListQueuesRequest;>;"
    const/4 v3, 0x0

    .line 1044
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/ListQueuesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ListQueuesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ListQueuesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/ListQueuesRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/ListQueuesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1046
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1047
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ListQueuesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ListQueuesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1048
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1050
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1048
    return-object v4

    .line 1050
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listQueues(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ListQueuesResult;
    .locals 2
    .param p1, "queueNamePrefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2102
    new-instance v0, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;-><init>()V

    .line 2103
    .local v0, "listQueuesRequest":Lcom/amazonaws/services/sqs/model/ListQueuesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/ListQueuesRequest;->setQueueNamePrefix(Ljava/lang/String;)V

    .line 2104
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->listQueues(Lcom/amazonaws/services/sqs/model/ListQueuesRequest;)Lcom/amazonaws/services/sqs/model/ListQueuesResult;

    move-result-object v1

    return-object v1
.end method

.method public purgeQueue(Lcom/amazonaws/services/sqs/model/PurgeQueueRequest;)V
    .locals 5
    .param p1, "purgeQueueRequest"    # Lcom/amazonaws/services/sqs/model/PurgeQueueRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1086
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1087
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1088
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1089
    const/4 v2, 0x0

    .line 1090
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/PurgeQueueRequest;>;"
    const/4 v3, 0x0

    .line 1092
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/PurgeQueueRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/PurgeQueueRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/PurgeQueueRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/PurgeQueueRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1094
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1095
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1097
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1099
    return-void

    .line 1097
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public receiveMessage(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;)Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    .locals 5
    .param p1, "receiveMessageRequest"    # Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1201
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1202
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1203
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1204
    const/4 v2, 0x0

    .line 1205
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;>;"
    const/4 v3, 0x0

    .line 1207
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ReceiveMessageRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ReceiveMessageRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/ReceiveMessageRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1209
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1210
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/ReceiveMessageResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/ReceiveMessageResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1211
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1213
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1211
    return-object v4

    .line 1213
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public receiveMessage(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    .locals 2
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1934
    new-instance v0, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;-><init>()V

    .line 1935
    .local v0, "receiveMessageRequest":Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 1936
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->receiveMessage(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;)Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    move-result-object v1

    return-object v1
.end method

.method public removePermission(Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;)V
    .locals 5
    .param p1, "removePermissionRequest"    # Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1235
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1236
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1237
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1238
    const/4 v2, 0x0

    .line 1239
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;>;"
    const/4 v3, 0x0

    .line 1241
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/RemovePermissionRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/RemovePermissionRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/RemovePermissionRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1243
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1244
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1246
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1248
    return-void

    .line 1246
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public removePermission(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2279
    new-instance v0, Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;-><init>()V

    .line 2280
    .local v0, "removePermissionRequest":Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2281
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;->setLabel(Ljava/lang/String;)V

    .line 2282
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->removePermission(Lcom/amazonaws/services/sqs/model/RemovePermissionRequest;)V

    .line 2283
    return-void
.end method

.method public sendMessage(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 5
    .param p1, "sendMessageRequest"    # Lcom/amazonaws/services/sqs/model/SendMessageRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1287
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1288
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1289
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1290
    const/4 v2, 0x0

    .line 1291
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/SendMessageRequest;>;"
    const/4 v3, 0x0

    .line 1293
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/SendMessageRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/SendMessageRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/SendMessageRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1295
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1296
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/SendMessageResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/SendMessageResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1297
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/SendMessageResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1299
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1297
    return-object v4

    .line 1299
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 2
    .param p1, "queueUrl"    # Ljava/lang/String;
    .param p2, "messageBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1985
    new-instance v0, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;-><init>()V

    .line 1986
    .local v0, "sendMessageRequest":Lcom/amazonaws/services/sqs/model/SendMessageRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 1987
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->setMessageBody(Ljava/lang/String;)V

    .line 1988
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->sendMessage(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Lcom/amazonaws/services/sqs/model/SendMessageResult;

    move-result-object v1

    return-object v1
.end method

.method public sendMessageBatch(Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;)Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;
    .locals 5
    .param p1, "sendMessageBatchRequest"    # Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1375
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1376
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1377
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1378
    const/4 v2, 0x0

    .line 1379
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;>;"
    const/4 v3, 0x0

    .line 1381
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/SendMessageBatchRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/SendMessageBatchRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/SendMessageBatchRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1383
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1384
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/SendMessageBatchResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/SendMessageBatchResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1386
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1388
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1386
    return-object v4

    .line 1388
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public sendMessageBatch(Ljava/lang/String;Ljava/util/List;)Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;
    .locals 2
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;",
            ">;)",
            "Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2246
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;>;"
    new-instance v0, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;-><init>()V

    .line 2247
    .local v0, "sendMessageBatchRequest":Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2248
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 2249
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->sendMessageBatch(Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;)Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;

    move-result-object v1

    return-object v1
.end method

.method public setQueueAttributes(Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;)V
    .locals 5
    .param p1, "setQueueAttributesRequest"    # Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1419
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1420
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1421
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1422
    const/4 v2, 0x0

    .line 1423
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;>;"
    const/4 v3, 0x0

    .line 1425
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sqs/model/transform/SetQueueAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sqs/model/transform/SetQueueAttributesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sqs/model/transform/SetQueueAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1427
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1428
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1430
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1432
    return-void

    .line 1430
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public setQueueAttributes(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "queueUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2731
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;-><init>()V

    .line 2732
    .local v0, "setQueueAttributesRequest":Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;->setQueueUrl(Ljava/lang/String;)V

    .line 2733
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;->setAttributes(Ljava/util/Map;)V

    .line 2734
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sqs/AmazonSQSClient;->setQueueAttributes(Lcom/amazonaws/services/sqs/model/SetQueueAttributesRequest;)V

    .line 2735
    return-void
.end method
