.class Lcom/amazonaws/services/sqs/buffered/QueueBuffer;
.super Ljava/lang/Object;
.source "QueueBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazonaws/services/sqs/buffered/QueueBuffer$DaemonThreadFactory;
    }
.end annotation


# static fields
.field static executor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

.field private final realSqs:Lcom/amazonaws/services/sqs/AmazonSQSAsync;

.field private final receiveBuffer:Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;

.field private final sendBuffer:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer$DaemonThreadFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer$DaemonThreadFactory;-><init>(Lcom/amazonaws/services/sqs/buffered/QueueBuffer$1;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->executor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method constructor <init>(Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;Ljava/lang/String;Lcom/amazonaws/services/sqs/AmazonSQSAsync;)V
    .locals 2
    .param p1, "paramConfig"    # Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "sqs"    # Lcom/amazonaws/services/sqs/AmazonSQSAsync;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p3, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->realSqs:Lcom/amazonaws/services/sqs/AmazonSQSAsync;

    .line 70
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    .line 71
    new-instance v0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    sget-object v1, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p3, v1, p1, p2}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;-><init>(Lcom/amazonaws/services/sqs/AmazonSQS;Ljava/util/concurrent/Executor;Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->sendBuffer:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    .line 72
    new-instance v0, Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;

    sget-object v1, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p3, v1, p1, p2}, Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;-><init>(Lcom/amazonaws/services/sqs/AmazonSQS;Ljava/util/concurrent/Executor;Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->receiveBuffer:Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;

    .line 73
    return-void
.end method

.method private waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ResultType:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TResultType;>;)TResultType;"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TResultType;>;"
    const/4 v4, 0x0

    .line 226
    .local v4, "toReturn":Ljava/lang/Object;, "TResultType;"
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 250
    return-object v4

    .line 227
    :catch_0
    move-exception v3

    .line 228
    .local v3, "ie":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 229
    new-instance v1, Lcom/amazonaws/AmazonClientException;

    const-string v5, "Thread interrupted while waiting for execution result"

    invoke-direct {v1, v5}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    .line 231
    .local v1, "ce":Lcom/amazonaws/AmazonClientException;
    invoke-virtual {v1, v3}, Lcom/amazonaws/AmazonClientException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 232
    throw v1

    .line 233
    .end local v1    # "ce":Lcom/amazonaws/AmazonClientException;
    .end local v3    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v2

    .line 238
    .local v2, "ee":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 240
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v5, v0, Lcom/amazonaws/AmazonClientException;

    if-eqz v5, :cond_0

    .line 241
    check-cast v0, Lcom/amazonaws/AmazonClientException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 244
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_0
    new-instance v1, Lcom/amazonaws/AmazonClientException;

    const-string v5, "Caught an exception while waiting for request to complete..."

    invoke-direct {v1, v5}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    .line 246
    .restart local v1    # "ce":Lcom/amazonaws/AmazonClientException;
    invoke-virtual {v1, v2}, Lcom/amazonaws/AmazonClientException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 247
    throw v1
.end method


# virtual methods
.method public changeMessageVisibility(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;
    .locals 3
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;",
            "Lcom/amazonaws/handlers/AsyncHandler",
            "<",
            "Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;",
            "Ljava/lang/Void;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    .local p2, "handler":Lcom/amazonaws/handlers/AsyncHandler;, "Lcom/amazonaws/handlers/AsyncHandler<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Ljava/lang/Void;>;"
    const/4 v0, 0x0

    .line 145
    .local v0, "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Ljava/lang/Void;>;"
    if-eqz p2, :cond_0

    .line 146
    new-instance v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    .end local v0    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Ljava/lang/Void;>;"
    invoke-direct {v0, p2, p1}, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;-><init>(Lcom/amazonaws/handlers/AsyncHandler;Lcom/amazonaws/AmazonWebServiceRequest;)V

    .line 150
    .restart local v0    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Ljava/lang/Void;>;"
    :cond_0
    iget-object v2, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->sendBuffer:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    .line 151
    invoke-virtual {v2, p1, v0}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->changeMessageVisibility(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    move-result-object v1

    .line 152
    .local v1, "future":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Ljava/lang/Void;>;"
    invoke-virtual {v1, p0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setBuffer(Lcom/amazonaws/services/sqs/buffered/QueueBuffer;)V

    .line 153
    return-object v1
.end method

.method public changeMessageVisibilitySync(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;

    .prologue
    .line 161
    iget-object v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->sendBuffer:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->changeMessageVisibility(Lcom/amazonaws/services/sqs/model/ChangeMessageVisibilityRequest;Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    move-result-object v0

    .line 162
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    invoke-direct {p0, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public deleteMessage(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;
    .locals 3
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;",
            "Lcom/amazonaws/handlers/AsyncHandler",
            "<",
            "Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;",
            "Ljava/lang/Void;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    .local p2, "handler":Lcom/amazonaws/handlers/AsyncHandler;, "Lcom/amazonaws/handlers/AsyncHandler<Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Ljava/lang/Void;>;"
    const/4 v0, 0x0

    .line 114
    .local v0, "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Ljava/lang/Void;>;"
    if-eqz p2, :cond_0

    .line 115
    new-instance v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    .end local v0    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Ljava/lang/Void;>;"
    invoke-direct {v0, p2, p1}, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;-><init>(Lcom/amazonaws/handlers/AsyncHandler;Lcom/amazonaws/AmazonWebServiceRequest;)V

    .line 118
    .restart local v0    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Ljava/lang/Void;>;"
    :cond_0
    iget-object v2, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->sendBuffer:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    invoke-virtual {v2, p1, v0}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->deleteMessage(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    move-result-object v1

    .line 120
    .local v1, "future":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Ljava/lang/Void;>;"
    invoke-virtual {v1, p0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setBuffer(Lcom/amazonaws/services/sqs/buffered/QueueBuffer;)V

    .line 121
    return-object v1
.end method

.method public deleteMessageSync(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;

    .prologue
    .line 131
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->deleteMessage(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 132
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    invoke-direct {p0, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 133
    return-void
.end method

.method public receiveMessage(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;
    .locals 8
    .param p1, "rq"    # Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;",
            "Lcom/amazonaws/handlers/AsyncHandler",
            "<",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "handler":Lcom/amazonaws/handlers/AsyncHandler;, "Lcom/amazonaws/handlers/AsyncHandler<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 176
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getAttributeNames()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_0
    move v3, v6

    .line 177
    .local v3, "noAttributes":Z
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 178
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getMessageAttributeNames()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_1
    move v4, v6

    .line 179
    .local v4, "noMessageAttributesToRetrieve":Z
    :goto_1
    iget-object v7, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    invoke-virtual {v7}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->getMaxInflightReceiveBatches()I

    move-result v7

    if-lez v7, :cond_5

    iget-object v7, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    .line 180
    invoke-virtual {v7}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->getMaxDoneReceiveBatches()I

    move-result v7

    if-lez v7, :cond_5

    move v0, v6

    .line 181
    .local v0, "bufferngEnabled":Z
    :goto_2
    if-eqz v4, :cond_6

    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    .line 182
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;->getVisibilityTimeout()Ljava/lang/Integer;

    move-result-object v5

    if-nez v5, :cond_6

    .line 183
    const/4 v1, 0x0

    .line 184
    .local v1, "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    if-eqz p2, :cond_2

    .line 185
    new-instance v1, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    .end local v1    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    invoke-direct {v1, p2, p1}, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;-><init>(Lcom/amazonaws/handlers/AsyncHandler;Lcom/amazonaws/AmazonWebServiceRequest;)V

    .line 189
    .restart local v1    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    :cond_2
    iget-object v5, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->receiveBuffer:Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;

    .line 190
    invoke-virtual {v5, p1, v1}, Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;->receiveMessageAsync(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    move-result-object v2

    .line 191
    .local v2, "future":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    invoke-virtual {v2, p0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setBuffer(Lcom/amazonaws/services/sqs/buffered/QueueBuffer;)V

    .line 194
    .end local v1    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    .end local v2    # "future":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    :goto_3
    return-object v2

    .end local v0    # "bufferngEnabled":Z
    .end local v3    # "noAttributes":Z
    .end local v4    # "noMessageAttributesToRetrieve":Z
    :cond_3
    move v3, v5

    .line 176
    goto :goto_0

    .restart local v3    # "noAttributes":Z
    :cond_4
    move v4, v5

    .line 178
    goto :goto_1

    .restart local v4    # "noMessageAttributesToRetrieve":Z
    :cond_5
    move v0, v5

    .line 180
    goto :goto_2

    .line 194
    .restart local v0    # "bufferngEnabled":Z
    :cond_6
    iget-object v5, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->realSqs:Lcom/amazonaws/services/sqs/AmazonSQSAsync;

    invoke-interface {v5, p1}, Lcom/amazonaws/services/sqs/AmazonSQSAsync;->receiveMessageAsync(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;)Ljava/util/concurrent/Future;

    move-result-object v2

    goto :goto_3
.end method

.method public receiveMessageSync(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;)Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    .locals 2
    .param p1, "rq"    # Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;

    .prologue
    .line 204
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->receiveMessage(Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 205
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;>;"
    invoke-direct {p0, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    return-object v1
.end method

.method public sendMessage(Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;
    .locals 3
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/SendMessageRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/model/SendMessageRequest;",
            "Lcom/amazonaws/handlers/AsyncHandler",
            "<",
            "Lcom/amazonaws/services/sqs/model/SendMessageRequest;",
            "Lcom/amazonaws/services/sqs/model/SendMessageResult;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/amazonaws/services/sqs/model/SendMessageResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    .local p2, "handler":Lcom/amazonaws/handlers/AsyncHandler;, "Lcom/amazonaws/handlers/AsyncHandler<Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    const/4 v0, 0x0

    .line 84
    .local v0, "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    if-eqz p2, :cond_0

    .line 85
    new-instance v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    .end local v0    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    invoke-direct {v0, p2, p1}, Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;-><init>(Lcom/amazonaws/handlers/AsyncHandler;Lcom/amazonaws/AmazonWebServiceRequest;)V

    .line 88
    .restart local v0    # "callback":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    :cond_0
    iget-object v2, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->sendBuffer:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    invoke-virtual {v2, p1, v0}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->sendMessage(Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    move-result-object v1

    .line 90
    .local v1, "future":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    invoke-virtual {v1, p0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setBuffer(Lcom/amazonaws/services/sqs/buffered/QueueBuffer;)V

    .line 91
    return-object v1
.end method

.method public sendMessageSync(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Lcom/amazonaws/services/sqs/model/SendMessageResult;
    .locals 2
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    .prologue
    .line 100
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->sendMessage(Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/handlers/AsyncHandler;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 101
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/amazonaws/services/sqs/model/SendMessageResult;>;"
    invoke-direct {p0, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amazonaws/services/sqs/model/SendMessageResult;

    return-object v1
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->receiveBuffer:Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;

    invoke-virtual {v0}, Lcom/amazonaws/services/sqs/buffered/ReceiveQueueBuffer;->shutdown()V

    .line 216
    return-void
.end method
