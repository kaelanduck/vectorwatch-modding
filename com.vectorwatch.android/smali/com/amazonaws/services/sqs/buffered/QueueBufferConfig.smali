.class public Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
.super Ljava/lang/Object;
.source "QueueBufferConfig.java"


# static fields
.field public static final LONGPOLL_WAIT_TIMEOUT_SECONDS_DEFAULT:I = 0x14

.field private static final LONG_POLL_DEFAULT:Z = true

.field public static final MAX_BATCH_OPEN_MS_DEFAULT:J = 0xc8L

.field public static final MAX_BATCH_SIZE_BYTES_DEFAULT:J = 0x3ffffL

.field public static final MAX_BATCH_SIZE_DEFAULT:I = 0xa

.field public static final MAX_DONE_RECEIVE_BATCHES_DEFAULT:I = 0xa

.field public static final MAX_INFLIGHT_OUTBOUND_BATCHES_DEFAULT:I = 0x5

.field public static final MAX_INFLIGHT_RECEIVE_BATCHES_DEFAULT:I = 0xa

.field public static final SERVICE_MAX_BATCH_SIZE_BYTES:J = 0x3ffffL

.field public static final VISIBILITY_TIMEOUT_SECONDS_DEFAULT:I = -0x1


# instance fields
.field private longPoll:Z

.field private longPollWaitTimeoutSeconds:I

.field private maxBatchOpenMs:J

.field private maxBatchSize:I

.field private maxBatchSizeBytes:J

.field private maxDoneReceiveBatches:I

.field private maxInflightOutboundBatches:I

.field private maxInflightReceiveBatches:I

.field private visibilityTimeoutSeconds:I


# direct methods
.method public constructor <init>()V
    .locals 13

    .prologue
    const/16 v5, 0xa

    .line 142
    const-wide/16 v2, 0xc8

    const/4 v4, 0x5

    const/4 v7, 0x1

    const-wide/32 v8, 0x3ffff

    const/4 v10, -0x1

    const/16 v11, 0x14

    move-object v1, p0

    move v6, v5

    move v12, v5

    invoke-direct/range {v1 .. v12}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;-><init>(JIIIZJIII)V

    .line 151
    return-void
.end method

.method public constructor <init>(JIIIZJIII)V
    .locals 1
    .param p1, "maxBatchOpenMs"    # J
    .param p3, "maxInflightOutboundBatches"    # I
    .param p4, "maxInflightReceiveBatches"    # I
    .param p5, "maxDoneReceiveBatches"    # I
    .param p6, "paramLongPoll"    # Z
    .param p7, "maxBatchSizeBytes"    # J
    .param p9, "visibilityTimeout"    # I
    .param p10, "longPollTimeout"    # I
    .param p11, "maxBatch"    # I

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-wide p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    .line 131
    iput p3, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    .line 132
    iput p4, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    .line 133
    iput p5, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    .line 134
    iput-boolean p6, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    .line 135
    iput-wide p7, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    .line 136
    iput p9, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    .line 137
    iput p10, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    .line 138
    iput p11, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    .line 139
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;)V
    .locals 2
    .param p1, "other"    # Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iget-boolean v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    iput-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    .line 156
    iget v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    .line 157
    iget-wide v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    iput-wide v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    .line 158
    iget v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    .line 159
    iget-wide v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    iput-wide v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    .line 160
    iget v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    .line 161
    iget v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    .line 162
    iget v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    .line 163
    iget v0, p1, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    .line 164
    return-void
.end method


# virtual methods
.method public getLongPollWaitTimeoutSeconds()I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    return v0
.end method

.method public getMaxBatchOpenMs()J
    .locals 2

    .prologue
    .line 188
    iget-wide v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    return-wide v0
.end method

.method public getMaxBatchSize()I
    .locals 1

    .prologue
    .line 410
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    return v0
.end method

.method public getMaxBatchSizeBytes()J
    .locals 2

    .prologue
    .line 327
    iget-wide v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    return-wide v0
.end method

.method public getMaxDoneReceiveBatches()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    return v0
.end method

.method public getMaxInflightOutboundBatches()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    return v0
.end method

.method public getMaxInflightReceiveBatches()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    return v0
.end method

.method public getVisibilityTimeoutSeconds()I
    .locals 1

    .prologue
    .line 361
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    return v0
.end method

.method public isLongPoll()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    return v0
.end method

.method public setLongPoll(Z)V
    .locals 0
    .param p1, "longPoll"    # Z

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    .line 223
    return-void
.end method

.method public setLongPollWaitTimeoutSeconds(I)V
    .locals 0
    .param p1, "longPollWaitTimeoutSeconds"    # I

    .prologue
    .line 387
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    .line 388
    return-void
.end method

.method public setMaxBatchOpenMs(J)V
    .locals 1
    .param p1, "maxBatchOpenMs"    # J

    .prologue
    .line 200
    iput-wide p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    .line 201
    return-void
.end method

.method public setMaxBatchSize(I)V
    .locals 0
    .param p1, "maxBatchSize"    # I

    .prologue
    .line 418
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    .line 419
    return-void
.end method

.method public setMaxBatchSizeBytes(J)V
    .locals 3
    .param p1, "maxBatchSizeBytes"    # J

    .prologue
    .line 340
    const-wide/32 v0, 0x3ffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 341
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Maximum Size of the message cannot be greater than the allowed limit of 262143 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_0
    iput-wide p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    .line 346
    return-void
.end method

.method public setMaxDoneReceiveBatches(I)V
    .locals 0
    .param p1, "maxDoneReceiveBatches"    # I

    .prologue
    .line 312
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    .line 313
    return-void
.end method

.method public setMaxInflightOutboundBatches(I)V
    .locals 0
    .param p1, "maxInflightOutboundBatches"    # I

    .prologue
    .line 247
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    .line 248
    return-void
.end method

.method public setMaxInflightReceiveBatches(I)V
    .locals 0
    .param p1, "maxInflightReceiveBatches"    # I

    .prologue
    .line 271
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    .line 272
    return-void
.end method

.method public setVisibilityTimeoutSeconds(I)V
    .locals 0
    .param p1, "visibilityTimeoutSeconds"    # I

    .prologue
    .line 372
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    .line 373
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QueueBufferConfig [maxBatchSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxBatchOpenMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", longPoll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxInflightOutboundBatches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxInflightReceiveBatches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxDoneReceiveBatches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxBatchSizeBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visibilityTimeoutSeconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", longPollWaitTimeoutSeconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method validate()V
    .locals 2

    .prologue
    .line 438
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    if-nez v0, :cond_0

    .line 439
    new-instance v0, Lcom/amazonaws/AmazonClientException;

    const-string v1, "Visibility timeout value may not be equal to zero "

    invoke-direct {v0, v1}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 441
    :cond_0
    return-void
.end method

.method public withLongPoll(Z)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "longPoll"    # Z

    .prologue
    .line 226
    iput-boolean p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPoll:Z

    .line 227
    return-object p0
.end method

.method public withLongPollWaitTimeoutSeconds(I)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "longPollWaitTimeoutSeconds"    # I

    .prologue
    .line 401
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->longPollWaitTimeoutSeconds:I

    .line 402
    return-object p0
.end method

.method public withMaxBatchOpenMs(J)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 1
    .param p1, "maxBatchOpenMs"    # J

    .prologue
    .line 204
    iput-wide p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchOpenMs:J

    .line 205
    return-object p0
.end method

.method public withMaxBatchSize(I)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "maxBatchSize"    # I

    .prologue
    .line 426
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSize:I

    .line 427
    return-object p0
.end method

.method public withMaxBatchSizeBytes(J)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 1
    .param p1, "maxBatchSizeBytes"    # J

    .prologue
    .line 349
    iput-wide p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxBatchSizeBytes:J

    .line 350
    return-object p0
.end method

.method public withMaxDoneReceiveBatches(I)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "maxDoneReceiveBatches"    # I

    .prologue
    .line 316
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxDoneReceiveBatches:I

    .line 317
    return-object p0
.end method

.method public withMaxInflightOutboundBatches(I)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "maxInflightOutboundBatches"    # I

    .prologue
    .line 251
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightOutboundBatches:I

    .line 252
    return-object p0
.end method

.method public withMaxInflightReceiveBatches(I)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "maxInflightReceiveBatches"    # I

    .prologue
    .line 275
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->maxInflightReceiveBatches:I

    .line 276
    return-object p0
.end method

.method public withVisibilityTimeoutSeconds(I)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    .locals 0
    .param p1, "visibilityTimeoutSeconds"    # I

    .prologue
    .line 376
    iput p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->visibilityTimeoutSeconds:I

    .line 377
    return-object p0
.end method
