.class Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;
.super Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$OutboundBatchTask;
.source "SendQueueBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendMessageBatchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$OutboundBatchTask",
        "<",
        "Lcom/amazonaws/services/sqs/model/SendMessageRequest;",
        "Lcom/amazonaws/services/sqs/model/SendMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field batchSizeBytes:I

.field final synthetic this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;


# direct methods
.method private constructor <init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)V
    .locals 1

    .prologue
    .line 457
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    invoke-direct {p0, p1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$OutboundBatchTask;-><init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)V

    .line 459
    const/4 v0, 0x0

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->batchSizeBytes:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;
    .param p2, "x1"    # Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$1;

    .prologue
    .line 457
    invoke-direct {p0, p1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;-><init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)V

    return-void
.end method


# virtual methods
.method declared-synchronized isFull()Z
    .locals 4

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    invoke-static {v1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$300(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->getMaxBatchSize()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->batchSizeBytes:I

    int-to-long v0, v0

    iget-object v2, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    .line 477
    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    invoke-static {v2}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$300(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->getMaxBatchSizeBytes()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 476
    :goto_0
    monitor-exit p0

    return v0

    .line 477
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected bridge synthetic isOkToAdd(Lcom/amazonaws/AmazonWebServiceRequest;)Z
    .locals 1

    .prologue
    .line 457
    check-cast p1, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->isOkToAdd(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Z

    move-result v0

    return v0
.end method

.method protected declared-synchronized isOkToAdd(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    invoke-static {v1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$300(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->getMaxBatchSize()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 465
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getMessageBody()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    iget v1, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->batchSizeBytes:I

    add-int/2addr v0, v1

    int-to-long v0, v0

    iget-object v2, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->config:Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;
    invoke-static {v2}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$300(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;

    move-result-object v2

    .line 466
    invoke-virtual {v2}, Lcom/amazonaws/services/sqs/buffered/QueueBufferConfig;->getMaxBatchSizeBytes()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 463
    :goto_0
    monitor-exit p0

    return v0

    .line 466
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected bridge synthetic onRequestAdded(Lcom/amazonaws/AmazonWebServiceRequest;)V
    .locals 0

    .prologue
    .line 457
    check-cast p1, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->onRequestAdded(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)V

    return-void
.end method

.method protected onRequestAdded(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    .prologue
    .line 471
    iget v0, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->batchSizeBytes:I

    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->batchSizeBytes:I

    .line 472
    return-void
.end method

.method process()V
    .locals 13

    .prologue
    .line 482
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 523
    :cond_0
    return-void

    .line 485
    :cond_1
    new-instance v9, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;

    invoke-direct {v9}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;-><init>()V

    iget-object v10, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    .line 486
    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->qUrl:Ljava/lang/String;
    invoke-static {v10}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$400(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;->withQueueUrl(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;

    move-result-object v1

    .line 487
    .local v1, "batchRequest":Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;
    sget-object v9, Lcom/amazonaws/services/sqs/buffered/AmazonSQSBufferedAsyncClient;->USER_AGENT:Ljava/lang/String;

    invoke-static {v1, v9}, Lcom/amazonaws/services/sqs/buffered/ResultConverter;->appendUserAgent(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)Lcom/amazonaws/AmazonWebServiceRequest;

    .line 489
    new-instance v3, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    .line 490
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 491
    .local v3, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v8

    .local v8, "n":I
    :goto_0
    if-ge v6, v8, :cond_2

    .line 492
    new-instance v9, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    invoke-direct {v9}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;-><init>()V

    .line 493
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->withId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    move-result-object v10

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    .line 494
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-virtual {v9}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getMessageBody()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->withMessageBody(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    move-result-object v10

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    .line 495
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-virtual {v9}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getDelaySeconds()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v9}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->withDelaySeconds(Ljava/lang/Integer;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    move-result-object v10

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    .line 496
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-virtual {v9}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v10, v9}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->withMessageAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    move-result-object v9

    .line 492
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 491
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 497
    :cond_2
    invoke-virtual {v1, v3}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 499
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->sqsClient:Lcom/amazonaws/services/sqs/AmazonSQS;
    invoke-static {v9}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$500(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/AmazonSQS;

    move-result-object v9

    .line 500
    invoke-interface {v9, v1}, Lcom/amazonaws/services/sqs/AmazonSQS;->sendMessageBatch(Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;)Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;

    move-result-object v2

    .line 503
    .local v2, "batchResult":Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;
    invoke-virtual {v2}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v9

    .line 502
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;

    .line 504
    .local v4, "entry":Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 505
    .local v7, "index":I
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    invoke-static {v4}, Lcom/amazonaws/services/sqs/buffered/ResultConverter;->convert(Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;)Lcom/amazonaws/services/sqs/model/SendMessageResult;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 508
    .end local v4    # "entry":Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;
    .end local v7    # "index":I
    :cond_3
    invoke-virtual {v2}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;

    .line 509
    .local v5, "errorEntry":Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;
    invoke-virtual {v5}, Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 510
    .restart local v7    # "index":I
    invoke-virtual {v5}, Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;->isSenderFault()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 511
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    invoke-static {v5}, Lcom/amazonaws/services/sqs/buffered/ResultConverter;->convert(Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;)Ljava/lang/Exception;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setFailure(Ljava/lang/Exception;)V

    goto :goto_2

    .line 516
    :cond_4
    :try_start_0
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    iget-object v10, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->sqsClient:Lcom/amazonaws/services/sqs/AmazonSQS;
    invoke-static {v10}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$500(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/AmazonSQS;

    move-result-object v12

    iget-object v10, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    invoke-interface {v12, v10}, Lcom/amazonaws/services/sqs/AmazonSQS;->sendMessage(Lcom/amazonaws/services/sqs/model/SendMessageRequest;)Lcom/amazonaws/services/sqs/model/SendMessageResult;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/amazonaws/AmazonClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "ace":Lcom/amazonaws/AmazonClientException;
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$SendMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    invoke-virtual {v9, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setFailure(Ljava/lang/Exception;)V

    goto :goto_2
.end method
