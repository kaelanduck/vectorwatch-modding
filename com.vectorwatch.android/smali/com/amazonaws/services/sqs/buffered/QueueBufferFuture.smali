.class Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;
.super Ljava/lang/Object;
.source "QueueBufferFuture.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Req:",
        "Lcom/amazonaws/AmazonWebServiceRequest;",
        "Res:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<TRes;>;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback",
            "<TReq;TRes;>;"
        }
    .end annotation
.end field

.field private done:Z

.field private e:Ljava/lang/Exception;

.field private issuingBuffer:Lcom/amazonaws/services/sqs/buffered/QueueBuffer;

.field private result:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRes;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;-><init>(Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback",
            "<TReq;TRes;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    .local p1, "cb":Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback<TReq;TRes;>;"
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->result:Ljava/lang/Object;

    .line 32
    iput-object v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->e:Ljava/lang/Exception;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z

    .line 47
    iput-object v1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->issuingBuffer:Lcom/amazonaws/services/sqs/buffered/QueueBuffer;

    .line 54
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->callback:Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->result:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;)Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;
    .locals 1
    .param p0, "x0"    # Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->callback:Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;)Ljava/lang/Exception;
    .locals 1
    .param p0, "x0"    # Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->e:Ljava/lang/Exception;

    return-object v0
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1, "arg0"    # Z

    .prologue
    .line 111
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRes;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    :goto_0
    const-wide v0, 0x7fffffffffffffffL

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1, v2}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 123
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 17
    .param p1, "timeout"    # J
    .param p3, "tu"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TRes;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    monitor-enter p0

    :try_start_0
    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    sget-object v13, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v12, v14, v15, v13}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    .line 137
    .local v10, "waitStartMs":J
    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, p1

    move-object/from16 v2, p3

    invoke-virtual {v12, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    .line 138
    .local v8, "timeoutMs":J
    move-wide v6, v8

    .line 140
    .local v6, "timeToWaitMs":J
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z

    if-nez v12, :cond_1

    .line 144
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-gtz v12, :cond_0

    .line 145
    new-instance v12, Ljava/util/concurrent/TimeoutException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Timed out waiting for results after "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-wide/from16 v0, p1

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    .end local v6    # "timeToWaitMs":J
    .end local v8    # "timeoutMs":J
    .end local v10    # "waitStartMs":J
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 149
    .restart local v6    # "timeToWaitMs":J
    .restart local v8    # "timeoutMs":J
    .restart local v10    # "waitStartMs":J
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 152
    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    sget-object v13, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v12, v14, v15, v13}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 153
    .local v4, "nowMs":J
    sub-long v12, v4, v10

    sub-long v6, v8, v12

    .line 155
    goto :goto_0

    .line 159
    .end local v4    # "nowMs":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->e:Ljava/lang/Exception;

    if-eqz v12, :cond_2

    .line 160
    new-instance v12, Ljava/util/concurrent/ExecutionException;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->e:Ljava/lang/Exception;

    invoke-direct {v12, v13}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v12

    .line 164
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->result:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v12
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 170
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized isDone()Z
    .locals 1

    .prologue
    .line 175
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBuffer(Lcom/amazonaws/services/sqs/buffered/QueueBuffer;)V
    .locals 0
    .param p1, "paramBuffer"    # Lcom/amazonaws/services/sqs/buffered/QueueBuffer;

    .prologue
    .line 115
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->issuingBuffer:Lcom/amazonaws/services/sqs/buffered/QueueBuffer;

    .line 116
    return-void
.end method

.method public declared-synchronized setFailure(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "paramE"    # Ljava/lang/Exception;

    .prologue
    .line 86
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 88
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->e:Ljava/lang/Exception;

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z

    .line 90
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 95
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->callback:Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->issuingBuffer:Lcom/amazonaws/services/sqs/buffered/QueueBuffer;

    if-eqz v0, :cond_0

    .line 96
    sget-object v0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture$2;

    invoke-direct {v1, p0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture$2;-><init>(Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSuccess(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRes;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;, "Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture<TReq;TRes;>;"
    .local p1, "paramResult":Ljava/lang/Object;, "TRes;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 80
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 63
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->result:Ljava/lang/Object;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->done:Z

    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 70
    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->callback:Lcom/amazonaws/services/sqs/buffered/QueueBufferCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->issuingBuffer:Lcom/amazonaws/services/sqs/buffered/QueueBuffer;

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/amazonaws/services/sqs/buffered/QueueBuffer;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture$1;

    invoke-direct {v1, p0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture$1;-><init>(Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
