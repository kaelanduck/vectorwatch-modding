.class Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;
.super Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$OutboundBatchTask;
.source "SendQueueBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteMessageBatchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$OutboundBatchTask",
        "<",
        "Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;


# direct methods
.method private constructor <init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    invoke-direct {p0, p1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$OutboundBatchTask;-><init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;
    .param p2, "x1"    # Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$1;

    .prologue
    .line 527
    invoke-direct {p0, p1}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;-><init>(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)V

    return-void
.end method


# virtual methods
.method process()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 532
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 570
    :cond_0
    return-void

    .line 535
    :cond_1
    new-instance v9, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;

    invoke-direct {v9}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;-><init>()V

    iget-object v10, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    .line 536
    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->qUrl:Ljava/lang/String;
    invoke-static {v10}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$400(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->withQueueUrl(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;

    move-result-object v1

    .line 537
    .local v1, "batchRequest":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;
    sget-object v9, Lcom/amazonaws/services/sqs/buffered/AmazonSQSBufferedAsyncClient;->USER_AGENT:Ljava/lang/String;

    invoke-static {v1, v9}, Lcom/amazonaws/services/sqs/buffered/ResultConverter;->appendUserAgent(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)Lcom/amazonaws/AmazonWebServiceRequest;

    .line 539
    new-instance v3, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->requests:Ljava/util/List;

    .line 540
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 541
    .local v3, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v8

    .local v8, "n":I
    :goto_0
    if-ge v6, v8, :cond_2

    .line 542
    new-instance v9, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;

    invoke-direct {v9}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;-><init>()V

    .line 543
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 542
    invoke-virtual {v9, v10}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;->withId(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;

    move-result-object v10

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->requests:Ljava/util/List;

    .line 544
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;

    invoke-virtual {v9}, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;->getReceiptHandle()Ljava/lang/String;

    move-result-object v9

    .line 543
    invoke-virtual {v10, v9}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;->withReceiptHandle(Ljava/lang/String;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequestEntry;

    move-result-object v9

    .line 542
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 545
    :cond_2
    invoke-virtual {v1, v3}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;->setEntries(Ljava/util/Collection;)V

    .line 547
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->sqsClient:Lcom/amazonaws/services/sqs/AmazonSQS;
    invoke-static {v9}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$500(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/AmazonSQS;

    move-result-object v9

    .line 548
    invoke-interface {v9, v1}, Lcom/amazonaws/services/sqs/AmazonSQS;->deleteMessageBatch(Lcom/amazonaws/services/sqs/model/DeleteMessageBatchRequest;)Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;

    move-result-object v2

    .line 551
    .local v2, "batchResult":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;
    invoke-virtual {v2}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v9

    .line 550
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;

    .line 552
    .local v4, "entry":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 553
    .local v7, "index":I
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    invoke-virtual {v9, v11}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 556
    .end local v4    # "entry":Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResultEntry;
    .end local v7    # "index":I
    :cond_3
    invoke-virtual {v2}, Lcom/amazonaws/services/sqs/model/DeleteMessageBatchResult;->getFailed()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;

    .line 557
    .local v5, "errorEntry":Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;
    invoke-virtual {v5}, Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 558
    .restart local v7    # "index":I
    invoke-virtual {v5}, Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;->isSenderFault()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 559
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    invoke-static {v5}, Lcom/amazonaws/services/sqs/buffered/ResultConverter;->convert(Lcom/amazonaws/services/sqs/model/BatchResultErrorEntry;)Ljava/lang/Exception;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setFailure(Ljava/lang/Exception;)V

    goto :goto_2

    .line 563
    :cond_4
    :try_start_0
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->this$0:Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;

    # getter for: Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->sqsClient:Lcom/amazonaws/services/sqs/AmazonSQS;
    invoke-static {v9}, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;->access$500(Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer;)Lcom/amazonaws/services/sqs/AmazonSQS;

    move-result-object v11

    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->requests:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;

    invoke-interface {v11, v9}, Lcom/amazonaws/services/sqs/AmazonSQS;->deleteMessage(Lcom/amazonaws/services/sqs/model/DeleteMessageRequest;)V

    .line 564
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/amazonaws/AmazonClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 565
    :catch_0
    move-exception v0

    .line 566
    .local v0, "ace":Lcom/amazonaws/AmazonClientException;
    iget-object v9, p0, Lcom/amazonaws/services/sqs/buffered/SendQueueBuffer$DeleteMessageBatchTask;->futures:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;

    invoke-virtual {v9, v0}, Lcom/amazonaws/services/sqs/buffered/QueueBufferFuture;->setFailure(Ljava/lang/Exception;)V

    goto :goto_2
.end method
