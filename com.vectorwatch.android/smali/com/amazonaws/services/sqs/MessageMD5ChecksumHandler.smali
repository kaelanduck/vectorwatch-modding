.class public Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;
.super Lcom/amazonaws/handlers/AbstractRequestHandler;
.source "MessageMD5ChecksumHandler.java"


# static fields
.field private static final BINARY_LIST_TYPE_FIELD_INDEX:B = 0x4t

.field private static final BINARY_TYPE_FIELD_INDEX:B = 0x2t

.field private static final INTEGER_SIZE_IN_BYTES:I = 0x4

.field private static final MD5_MISMATCH_ERROR_MESSAGE:Ljava/lang/String; = "MD5 returned by SQS does not match the calculation on the original request. (MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

.field private static final MD5_MISMATCH_ERROR_MESSAGE_WITH_ID:Ljava/lang/String; = "MD5 returned by SQS does not match the calculation on the original request. (Message ID: %s, MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

.field private static final MESSAGE_ATTRIBUTES:Ljava/lang/String; = "message attributes"

.field private static final MESSAGE_BODY:Ljava/lang/String; = "message body"

.field private static final STRING_LIST_TYPE_FIELD_INDEX:B = 0x3t

.field private static final STRING_TYPE_FIELD_INDEX:B = 0x1t

.field private static final log:Lorg/apache/commons/logging/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/amazonaws/handlers/AbstractRequestHandler;-><init>()V

    return-void
.end method

.method private static calculateMessageAttributesMd5(Ljava/util/Map;)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sqs/model/MessageAttributeValue;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    sget-object v8, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v8}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 237
    sget-object v8, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Message attribtues: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    .line 239
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 240
    .local v6, "sortedAttributeNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 242
    const/4 v5, 0x0

    .line 244
    .local v5, "md5Digest":Ljava/security/MessageDigest;
    :try_start_0
    const-string v8, "MD5"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 246
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 247
    .local v0, "attrName":Ljava/lang/String;
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;

    .line 250
    .local v1, "attrValue":Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    invoke-static {v5, v0}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 252
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 255
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 256
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Ljava/security/MessageDigest;->update(B)V

    .line 257
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 273
    .end local v0    # "attrName":Ljava/lang/String;
    .end local v1    # "attrValue":Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    :catch_0
    move-exception v3

    .line 274
    .local v3, "e":Ljava/lang/Exception;
    new-instance v8, Lcom/amazonaws/AmazonClientException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to calculate the MD5 hash of the message attributes. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 276
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v3}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 258
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "attrName":Ljava/lang/String;
    .restart local v1    # "attrValue":Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 259
    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Ljava/security/MessageDigest;->update(B)V

    .line 260
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 261
    :cond_3
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 262
    const/4 v9, 0x3

    invoke-virtual {v5, v9}, Ljava/security/MessageDigest;->update(B)V

    .line 263
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getStringListValues()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 264
    .local v7, "strListMember":Ljava/lang/String;
    invoke-static {v5, v7}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/lang/String;)V

    goto :goto_1

    .line 266
    .end local v7    # "strListMember":Ljava/lang/String;
    :cond_4
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 267
    const/4 v9, 0x4

    invoke-virtual {v5, v9}, Ljava/security/MessageDigest;->update(B)V

    .line 268
    invoke-virtual {v1}, Lcom/amazonaws/services/sqs/model/MessageAttributeValue;->getBinaryListValues()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/ByteBuffer;

    .line 269
    .local v2, "byteListMember":Ljava/nio/ByteBuffer;
    invoke-static {v5, v2}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/nio/ByteBuffer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 279
    .end local v0    # "attrName":Ljava/lang/String;
    .end local v1    # "attrValue":Lcom/amazonaws/services/sqs/model/MessageAttributeValue;
    .end local v2    # "byteListMember":Ljava/nio/ByteBuffer;
    :cond_5
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v8

    invoke-static {v8}, Lcom/amazonaws/util/BinaryUtils;->toHex([B)Ljava/lang/String;

    move-result-object v4

    .line 280
    .local v4, "expectedMd5Hex":Ljava/lang/String;
    sget-object v8, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v8}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 281
    sget-object v8, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Expected  MD5 of message attributes: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    .line 283
    :cond_6
    return-object v4
.end method

.method private static calculateMessageBodyMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "messageBody"    # Ljava/lang/String;

    .prologue
    .line 214
    sget-object v3, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v3}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    sget-object v3, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Message body: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    .line 219
    :cond_0
    :try_start_0
    sget-object v3, Lcom/amazonaws/util/StringUtils;->UTF8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/amazonaws/util/Md5Utils;->computeMD5Hash([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 224
    .local v1, "expectedMd5":[B
    invoke-static {v1}, Lcom/amazonaws/util/BinaryUtils;->toHex([B)Ljava/lang/String;

    move-result-object v2

    .line 225
    .local v2, "expectedMd5Hex":Ljava/lang/String;
    sget-object v3, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v3}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 226
    sget-object v3, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Expected  MD5 of message body: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    .line 228
    :cond_1
    return-object v2

    .line 220
    .end local v1    # "expectedMd5":[B
    .end local v2    # "expectedMd5Hex":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/amazonaws/AmazonClientException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to calculate the MD5 hash of the message body. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 222
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method private static receiveMessageResultMd5Check(Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;)V
    .locals 14
    .param p0, "receiveMessageResult"    # Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    .prologue
    const/4 v9, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 144
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;->getMessages()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/amazonaws/services/sqs/model/Message;

    .line 145
    .local v6, "messageReceived":Lcom/amazonaws/services/sqs/model/Message;
    invoke-virtual {v6}, Lcom/amazonaws/services/sqs/model/Message;->getBody()Ljava/lang/String;

    move-result-object v5

    .line 146
    .local v5, "messageBody":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfBody()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "bodyMd5Returned":Ljava/lang/String;
    invoke-static {v5}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->calculateMessageBodyMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 148
    .local v3, "clientSideBodyMd5":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 149
    new-instance v7, Lcom/amazonaws/AmazonClientException;

    const-string v8, "MD5 returned by SQS does not match the calculation on the original request. (MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

    new-array v9, v9, [Ljava/lang/Object;

    const-string v10, "message body"

    aput-object v10, v9, v11

    aput-object v3, v9, v12

    aput-object v1, v9, v13

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 155
    :cond_1
    invoke-virtual {v6}, Lcom/amazonaws/services/sqs/model/Message;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    .line 156
    .local v4, "messageAttr":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 157
    invoke-virtual {v6}, Lcom/amazonaws/services/sqs/model/Message;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "attrMd5Returned":Ljava/lang/String;
    invoke-static {v4}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->calculateMessageAttributesMd5(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 159
    .local v2, "clientSideAttrMd5":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 160
    new-instance v7, Lcom/amazonaws/AmazonClientException;

    const-string v8, "MD5 returned by SQS does not match the calculation on the original request. (MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

    new-array v9, v9, [Ljava/lang/Object;

    const-string v10, "message attributes"

    aput-object v10, v9, v11

    aput-object v2, v9, v12

    aput-object v0, v9, v13

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 167
    .end local v0    # "attrMd5Returned":Ljava/lang/String;
    .end local v1    # "bodyMd5Returned":Ljava/lang/String;
    .end local v2    # "clientSideAttrMd5":Ljava/lang/String;
    .end local v3    # "clientSideBodyMd5":Ljava/lang/String;
    .end local v4    # "messageAttr":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    .end local v5    # "messageBody":Ljava/lang/String;
    .end local v6    # "messageReceived":Lcom/amazonaws/services/sqs/model/Message;
    :cond_2
    return-void
.end method

.method private static sendMessageBatchOperationMd5Check(Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;)V
    .locals 13
    .param p0, "sendMessageBatchRequest"    # Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;
    .param p1, "sendMessageBatchResult"    # Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;

    .prologue
    .line 177
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 178
    .local v5, "idToRequestEntryMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;>;"
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;->getEntries()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    .line 180
    .local v4, "entry":Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 184
    .end local v4    # "entry":Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;
    :cond_0
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 185
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;->getSuccessful()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;

    .line 186
    .local v4, "entry":Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    invoke-virtual {v8}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageBody()Ljava/lang/String;

    move-result-object v7

    .line 187
    .local v7, "messageBody":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "bodyMd5Returned":Ljava/lang/String;
    invoke-static {v7}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->calculateMessageBodyMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 189
    .local v3, "clientSideBodyMd5":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 190
    new-instance v8, Lcom/amazonaws/AmazonClientException;

    const-string v9, "MD5 returned by SQS does not match the calculation on the original request. (Message ID: %s, MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "message body"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 192
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getId()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    aput-object v3, v10, v11

    const/4 v11, 0x3

    aput-object v1, v10, v11

    .line 190
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 196
    :cond_2
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getId()Ljava/lang/String;

    move-result-object v8

    .line 195
    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;

    .line 196
    invoke-virtual {v8}, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequestEntry;->getMessageAttributes()Ljava/util/Map;

    move-result-object v6

    .line 197
    .local v6, "messageAttr":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 198
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "attrMd5Returned":Ljava/lang/String;
    invoke-static {v6}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->calculateMessageAttributesMd5(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "clientSideAttrMd5":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 201
    new-instance v8, Lcom/amazonaws/AmazonClientException;

    const-string v9, "MD5 returned by SQS does not match the calculation on the original request. (Message ID: %s, MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "message attributes"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 203
    invoke-virtual {v4}, Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;->getId()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    aput-object v2, v10, v11

    const/4 v11, 0x3

    aput-object v0, v10, v11

    .line 201
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 208
    .end local v0    # "attrMd5Returned":Ljava/lang/String;
    .end local v1    # "bodyMd5Returned":Ljava/lang/String;
    .end local v2    # "clientSideAttrMd5":Ljava/lang/String;
    .end local v3    # "clientSideBodyMd5":Ljava/lang/String;
    .end local v4    # "entry":Lcom/amazonaws/services/sqs/model/SendMessageBatchResultEntry;
    .end local v6    # "messageAttr":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    .end local v7    # "messageBody":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private static sendMessageOperationMd5Check(Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;)V
    .locals 13
    .param p0, "sendMessageRequest"    # Lcom/amazonaws/services/sqs/model/SendMessageRequest;
    .param p1, "sendMessageResult"    # Lcom/amazonaws/services/sqs/model/SendMessageResult;

    .prologue
    const/4 v8, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getMessageBody()Ljava/lang/String;

    move-result-object v5

    .line 116
    .local v5, "messageBodySent":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageBody()Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "bodyMd5Returned":Ljava/lang/String;
    invoke-static {v5}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->calculateMessageBodyMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 118
    .local v3, "clientSideBodyMd5":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 119
    new-instance v6, Lcom/amazonaws/AmazonClientException;

    const-string v7, "MD5 returned by SQS does not match the calculation on the original request. (MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "message body"

    aput-object v9, v8, v10

    aput-object v3, v8, v11

    aput-object v1, v8, v12

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sqs/model/SendMessageRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    .line 126
    .local v4, "messageAttrSent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sqs/model/MessageAttributeValue;>;"
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 127
    invoke-static {v4}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->calculateMessageAttributesMd5(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, "clientSideAttrMd5":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sqs/model/SendMessageResult;->getMD5OfMessageAttributes()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "attrMd5Returned":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 130
    new-instance v6, Lcom/amazonaws/AmazonClientException;

    const-string v7, "MD5 returned by SQS does not match the calculation on the original request. (MD5 calculated by the %s: \"%s\", MD5 checksum returned: \"%s\")"

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "message attributes"

    aput-object v9, v8, v10

    aput-object v2, v8, v11

    aput-object v0, v8, v12

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 135
    .end local v0    # "attrMd5Returned":Ljava/lang/String;
    .end local v2    # "clientSideAttrMd5":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private static updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/lang/String;)V
    .locals 4
    .param p0, "digest"    # Ljava/security/MessageDigest;
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 292
    sget-object v2, Lcom/amazonaws/util/StringUtils;->UTF8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    .line 293
    .local v1, "utf8Encoded":[B
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    array-length v3, v1

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 295
    .local v0, "lengthBytes":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 296
    invoke-virtual {p0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 297
    return-void
.end method

.method private static updateLengthAndBytes(Ljava/security/MessageDigest;Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p0, "digest"    # Ljava/security/MessageDigest;
    .param p1, "binaryValue"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 307
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 308
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    .line 309
    .local v1, "size":I
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 310
    .local v0, "lengthBytes":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 311
    invoke-virtual {p0, p1}, Ljava/security/MessageDigest;->update(Ljava/nio/ByteBuffer;)V

    .line 312
    return-void
.end method


# virtual methods
.method public afterResponse(Lcom/amazonaws/Request;Ljava/lang/Object;Lcom/amazonaws/util/TimingInfo;)V
    .locals 6
    .param p2, "response"    # Ljava/lang/Object;
    .param p3, "timingInfo"    # Lcom/amazonaws/util/TimingInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/Request",
            "<*>;",
            "Ljava/lang/Object;",
            "Lcom/amazonaws/util/TimingInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<*>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 82
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v5

    instance-of v5, v5, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    if-eqz v5, :cond_1

    instance-of v5, p2, Lcom/amazonaws/services/sqs/model/SendMessageResult;

    if-eqz v5, :cond_1

    .line 85
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v3

    check-cast v3, Lcom/amazonaws/services/sqs/model/SendMessageRequest;

    .local v3, "sendMessageRequest":Lcom/amazonaws/services/sqs/model/SendMessageRequest;
    move-object v4, p2

    .line 86
    check-cast v4, Lcom/amazonaws/services/sqs/model/SendMessageResult;

    .line 87
    .local v4, "sendMessageResult":Lcom/amazonaws/services/sqs/model/SendMessageResult;
    invoke-static {v3, v4}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->sendMessageOperationMd5Check(Lcom/amazonaws/services/sqs/model/SendMessageRequest;Lcom/amazonaws/services/sqs/model/SendMessageResult;)V

    .line 106
    .end local v3    # "sendMessageRequest":Lcom/amazonaws/services/sqs/model/SendMessageRequest;
    .end local v4    # "sendMessageResult":Lcom/amazonaws/services/sqs/model/SendMessageResult;
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v5

    instance-of v5, v5, Lcom/amazonaws/services/sqs/model/ReceiveMessageRequest;

    if-eqz v5, :cond_2

    instance-of v5, p2, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    if-eqz v5, :cond_2

    move-object v0, p2

    .line 93
    check-cast v0, Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;

    .line 94
    .local v0, "receiveMessageResult":Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    invoke-static {v0}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->receiveMessageResultMd5Check(Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;)V

    goto :goto_0

    .line 98
    .end local v0    # "receiveMessageResult":Lcom/amazonaws/services/sqs/model/ReceiveMessageResult;
    :cond_2
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v5

    instance-of v5, v5, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;

    if-eqz v5, :cond_0

    instance-of v5, p2, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;

    if-eqz v5, :cond_0

    .line 101
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v1

    check-cast v1, Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;

    .local v1, "sendMessageBatchRequest":Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;
    move-object v2, p2

    .line 102
    check-cast v2, Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;

    .line 103
    .local v2, "sendMessageBatchResult":Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;
    invoke-static {v1, v2}, Lcom/amazonaws/services/sqs/MessageMD5ChecksumHandler;->sendMessageBatchOperationMd5Check(Lcom/amazonaws/services/sqs/model/SendMessageBatchRequest;Lcom/amazonaws/services/sqs/model/SendMessageBatchResult;)V

    goto :goto_0
.end method
