.class public Lcom/amazonaws/services/sns/AmazonSNSClient;
.super Lcom/amazonaws/AmazonWebServiceClient;
.source "AmazonSNSClient.java"

# interfaces
.implements Lcom/amazonaws/services/sns/AmazonSNS;


# instance fields
.field private awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

.field protected final exceptionUnmarshallers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/transform/Unmarshaller",
            "<",
            "Lcom/amazonaws/AmazonServiceException;",
            "Lorg/w3c/dom/Node;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 89
    new-instance v0, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;

    invoke-direct {v0}, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;-><init>()V

    new-instance v1, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v1}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/ClientConfiguration;)V
    .locals 1
    .param p1, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 113
    new-instance v0, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;

    invoke-direct {v0}, Lcom/amazonaws/auth/DefaultAWSCredentialsProviderChain;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 114
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentials;)V
    .locals 1
    .param p1, "awsCredentials"    # Lcom/amazonaws/auth/AWSCredentials;

    .prologue
    .line 138
    new-instance v0, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v0}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentials;Lcom/amazonaws/ClientConfiguration;)V

    .line 139
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentials;Lcom/amazonaws/ClientConfiguration;)V
    .locals 1
    .param p1, "awsCredentials"    # Lcom/amazonaws/auth/AWSCredentials;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;

    .prologue
    .line 166
    new-instance v0, Lcom/amazonaws/internal/StaticCredentialsProvider;

    invoke-direct {v0, p1}, Lcom/amazonaws/internal/StaticCredentialsProvider;-><init>(Lcom/amazonaws/auth/AWSCredentials;)V

    invoke-direct {p0, v0, p2}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 167
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;

    .prologue
    .line 192
    new-instance v0, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v0}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V

    .line 193
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;

    .prologue
    .line 223
    new-instance v0, Lcom/amazonaws/http/UrlHttpClient;

    invoke-direct {v0, p2}, Lcom/amazonaws/http/UrlHttpClient;-><init>(Lcom/amazonaws/ClientConfiguration;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;-><init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/http/HttpClient;)V

    .line 224
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/http/HttpClient;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;
    .param p3, "httpClient"    # Lcom/amazonaws/http/HttpClient;

    .prologue
    .line 271
    invoke-static {p2}, Lcom/amazonaws/services/sns/AmazonSNSClient;->adjustClientConfiguration(Lcom/amazonaws/ClientConfiguration;)Lcom/amazonaws/ClientConfiguration;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/amazonaws/AmazonWebServiceClient;-><init>(Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/http/HttpClient;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    .line 273
    iput-object p1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

    .line 275
    invoke-direct {p0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->init()V

    .line 276
    return-void
.end method

.method public constructor <init>(Lcom/amazonaws/auth/AWSCredentialsProvider;Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/metrics/RequestMetricCollector;)V
    .locals 1
    .param p1, "awsCredentialsProvider"    # Lcom/amazonaws/auth/AWSCredentialsProvider;
    .param p2, "clientConfiguration"    # Lcom/amazonaws/ClientConfiguration;
    .param p3, "requestMetricCollector"    # Lcom/amazonaws/metrics/RequestMetricCollector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 246
    invoke-static {p2}, Lcom/amazonaws/services/sns/AmazonSNSClient;->adjustClientConfiguration(Lcom/amazonaws/ClientConfiguration;)Lcom/amazonaws/ClientConfiguration;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/amazonaws/AmazonWebServiceClient;-><init>(Lcom/amazonaws/ClientConfiguration;Lcom/amazonaws/metrics/RequestMetricCollector;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    .line 248
    iput-object p1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

    .line 250
    invoke-direct {p0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->init()V

    .line 251
    return-void
.end method

.method private static adjustClientConfiguration(Lcom/amazonaws/ClientConfiguration;)Lcom/amazonaws/ClientConfiguration;
    .locals 1
    .param p0, "orig"    # Lcom/amazonaws/ClientConfiguration;

    .prologue
    .line 302
    move-object v0, p0

    .line 304
    .local v0, "config":Lcom/amazonaws/ClientConfiguration;
    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 279
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/AuthorizationErrorExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/AuthorizationErrorExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/EndpointDisabledExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/EndpointDisabledExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/InternalErrorExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/InternalErrorExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/InvalidParameterExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/InvalidParameterExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/InvalidParameterValueExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/InvalidParameterValueExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/NotFoundExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/NotFoundExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationDisabledExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationDisabledExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/SubscriptionLimitExceededExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/SubscriptionLimitExceededExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/ThrottledExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/ThrottledExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/services/sns/model/transform/TopicLimitExceededExceptionUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/transform/TopicLimitExceededExceptionUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    new-instance v2, Lcom/amazonaws/transform/StandardErrorUnmarshaller;

    invoke-direct {v2}, Lcom/amazonaws/transform/StandardErrorUnmarshaller;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    const-string v1, "sns.us-east-1.amazonaws.com"

    invoke-virtual {p0, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->setEndpoint(Ljava/lang/String;)V

    .line 294
    new-instance v0, Lcom/amazonaws/handlers/HandlerChainFactory;

    invoke-direct {v0}, Lcom/amazonaws/handlers/HandlerChainFactory;-><init>()V

    .line 295
    .local v0, "chainFactory":Lcom/amazonaws/handlers/HandlerChainFactory;
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->requestHandler2s:Ljava/util/List;

    const-string v2, "/com/amazonaws/services/sns/request.handlers"

    invoke-virtual {v0, v2}, Lcom/amazonaws/handlers/HandlerChainFactory;->newRequestHandlerChain(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 297
    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->requestHandler2s:Ljava/util/List;

    const-string v2, "/com/amazonaws/services/sns/request.handler2s"

    invoke-virtual {v0, v2}, Lcom/amazonaws/handlers/HandlerChainFactory;->newRequestHandler2Chain(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 299
    return-void
.end method

.method private invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    .locals 5
    .param p3, "executionContext"    # Lcom/amazonaws/http/ExecutionContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            "Y:",
            "Lcom/amazonaws/AmazonWebServiceRequest;",
            ">(",
            "Lcom/amazonaws/Request",
            "<TY;>;",
            "Lcom/amazonaws/transform/Unmarshaller",
            "<TX;",
            "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
            ">;",
            "Lcom/amazonaws/http/ExecutionContext;",
            ")",
            "Lcom/amazonaws/Response",
            "<TX;>;"
        }
    .end annotation

    .prologue
    .line 2814
    .local p1, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<TY;>;"
    .local p2, "unmarshaller":Lcom/amazonaws/transform/Unmarshaller;, "Lcom/amazonaws/transform/Unmarshaller<TX;Lcom/amazonaws/transform/StaxUnmarshallerContext;>;"
    iget-object v4, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->endpoint:Ljava/net/URI;

    invoke-interface {p1, v4}, Lcom/amazonaws/Request;->setEndpoint(Ljava/net/URI;)V

    .line 2815
    iget v4, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->timeOffset:I

    invoke-interface {p1, v4}, Lcom/amazonaws/Request;->setTimeOffset(I)V

    .line 2816
    invoke-interface {p1}, Lcom/amazonaws/Request;->getOriginalRequest()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v2

    .line 2818
    .local v2, "originalRequest":Lcom/amazonaws/AmazonWebServiceRequest;
    iget-object v4, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->awsCredentialsProvider:Lcom/amazonaws/auth/AWSCredentialsProvider;

    invoke-interface {v4}, Lcom/amazonaws/auth/AWSCredentialsProvider;->getCredentials()Lcom/amazonaws/auth/AWSCredentials;

    move-result-object v0

    .line 2819
    .local v0, "credentials":Lcom/amazonaws/auth/AWSCredentials;
    invoke-virtual {v2}, Lcom/amazonaws/AmazonWebServiceRequest;->getRequestCredentials()Lcom/amazonaws/auth/AWSCredentials;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2820
    invoke-virtual {v2}, Lcom/amazonaws/AmazonWebServiceRequest;->getRequestCredentials()Lcom/amazonaws/auth/AWSCredentials;

    move-result-object v0

    .line 2823
    :cond_0
    invoke-virtual {p3, v0}, Lcom/amazonaws/http/ExecutionContext;->setCredentials(Lcom/amazonaws/auth/AWSCredentials;)V

    .line 2825
    new-instance v3, Lcom/amazonaws/http/StaxResponseHandler;

    invoke-direct {v3, p2}, Lcom/amazonaws/http/StaxResponseHandler;-><init>(Lcom/amazonaws/transform/Unmarshaller;)V

    .line 2826
    .local v3, "responseHandler":Lcom/amazonaws/http/StaxResponseHandler;, "Lcom/amazonaws/http/StaxResponseHandler<TX;>;"
    new-instance v1, Lcom/amazonaws/http/DefaultErrorResponseHandler;

    iget-object v4, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->exceptionUnmarshallers:Ljava/util/List;

    invoke-direct {v1, v4}, Lcom/amazonaws/http/DefaultErrorResponseHandler;-><init>(Ljava/util/List;)V

    .line 2828
    .local v1, "errorResponseHandler":Lcom/amazonaws/http/DefaultErrorResponseHandler;
    iget-object v4, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->client:Lcom/amazonaws/http/AmazonHttpClient;

    invoke-virtual {v4, p1, v3, v1, p3}, Lcom/amazonaws/http/AmazonHttpClient;->execute(Lcom/amazonaws/Request;Lcom/amazonaws/http/HttpResponseHandler;Lcom/amazonaws/http/HttpResponseHandler;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public addPermission(Lcom/amazonaws/services/sns/model/AddPermissionRequest;)V
    .locals 5
    .param p1, "addPermissionRequest"    # Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 328
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 329
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 330
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 331
    const/4 v2, 0x0

    .line 332
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/AddPermissionRequest;>;"
    const/4 v3, 0x0

    .line 334
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/AddPermissionRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/AddPermissionRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/AddPermissionRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/AddPermissionRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 336
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 337
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 341
    return-void

    .line 339
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public addPermission(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1973
    .local p3, "aWSAccountIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "actionNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;-><init>()V

    .line 1974
    .local v0, "addPermissionRequest":Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 1975
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setLabel(Ljava/lang/String;)V

    .line 1976
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setAWSAccountIds(Ljava/util/Collection;)V

    .line 1977
    invoke-virtual {v0, p4}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setActionNames(Ljava/util/Collection;)V

    .line 1978
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->addPermission(Lcom/amazonaws/services/sns/model/AddPermissionRequest;)V

    .line 1979
    return-void
.end method

.method public checkIfPhoneNumberIsOptedOut(Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutRequest;)Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
    .locals 5
    .param p1, "checkIfPhoneNumberIsOptedOutRequest"    # Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 376
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 377
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 378
    const/4 v2, 0x0

    .line 379
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutRequest;>;"
    const/4 v3, 0x0

    .line 381
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutRequestMarshaller;-><init>()V

    .line 382
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 384
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 385
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 387
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 387
    return-object v4

    .line 389
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public confirmSubscription(Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;
    .locals 5
    .param p1, "confirmSubscriptionRequest"    # Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 424
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 425
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 426
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 427
    const/4 v2, 0x0

    .line 428
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;>;"
    const/4 v3, 0x0

    .line 430
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ConfirmSubscriptionRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ConfirmSubscriptionRequestMarshaller;-><init>()V

    .line 431
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ConfirmSubscriptionRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 433
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 434
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ConfirmSubscriptionResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ConfirmSubscriptionResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 436
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 436
    return-object v4

    .line 438
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public confirmSubscription(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2188
    new-instance v0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;-><init>()V

    .line 2189
    .local v0, "confirmSubscriptionRequest":Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2190
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setToken(Ljava/lang/String;)V

    .line 2191
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->confirmSubscription(Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;

    move-result-object v1

    return-object v1
.end method

.method public confirmSubscription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .param p3, "authenticateOnUnsubscribe"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2146
    new-instance v0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;-><init>()V

    .line 2147
    .local v0, "confirmSubscriptionRequest":Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2148
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setToken(Ljava/lang/String;)V

    .line 2149
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setAuthenticateOnUnsubscribe(Ljava/lang/String;)V

    .line 2150
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->confirmSubscription(Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionResult;

    move-result-object v1

    return-object v1
.end method

.method public createPlatformApplication(Lcom/amazonaws/services/sns/model/CreatePlatformApplicationRequest;)Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;
    .locals 5
    .param p1, "createPlatformApplicationRequest"    # Lcom/amazonaws/services/sns/model/CreatePlatformApplicationRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 503
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 504
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 505
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 506
    const/4 v2, 0x0

    .line 507
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/CreatePlatformApplicationRequest;>;"
    const/4 v3, 0x0

    .line 509
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationRequestMarshaller;-><init>()V

    .line 510
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/CreatePlatformApplicationRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 512
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 513
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 515
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 515
    return-object v4

    .line 517
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public createPlatformEndpoint(Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;
    .locals 5
    .param p1, "createPlatformEndpointRequest"    # Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 567
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 568
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 569
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 570
    const/4 v2, 0x0

    .line 571
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;>;"
    const/4 v3, 0x0

    .line 573
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CreatePlatformEndpointRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformEndpointRequestMarshaller;-><init>()V

    .line 574
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformEndpointRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 576
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 577
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CreatePlatformEndpointResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformEndpointResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 579
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 579
    return-object v4

    .line 581
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public createTopic(Lcom/amazonaws/services/sns/model/CreateTopicRequest;)Lcom/amazonaws/services/sns/model/CreateTopicResult;
    .locals 5
    .param p1, "createTopicRequest"    # Lcom/amazonaws/services/sns/model/CreateTopicRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 614
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 615
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 616
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 617
    const/4 v2, 0x0

    .line 618
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/CreateTopicRequest;>;"
    const/4 v3, 0x0

    .line 620
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/CreateTopicResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CreateTopicRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CreateTopicRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/CreateTopicRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/CreateTopicRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 622
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 623
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/CreateTopicResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/CreateTopicResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 624
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/CreateTopicResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 626
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 624
    return-object v4

    .line 626
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public createTopic(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/CreateTopicResult;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2268
    new-instance v0, Lcom/amazonaws/services/sns/model/CreateTopicRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/CreateTopicRequest;-><init>()V

    .line 2269
    .local v0, "createTopicRequest":Lcom/amazonaws/services/sns/model/CreateTopicRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/CreateTopicRequest;->setName(Ljava/lang/String;)V

    .line 2270
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createTopic(Lcom/amazonaws/services/sns/model/CreateTopicRequest;)Lcom/amazonaws/services/sns/model/CreateTopicResult;

    move-result-object v1

    return-object v1
.end method

.method public deleteEndpoint(Lcom/amazonaws/services/sns/model/DeleteEndpointRequest;)V
    .locals 5
    .param p1, "deleteEndpointRequest"    # Lcom/amazonaws/services/sns/model/DeleteEndpointRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 658
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 659
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 660
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 661
    const/4 v2, 0x0

    .line 662
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/DeleteEndpointRequest;>;"
    const/4 v3, 0x0

    .line 664
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/DeleteEndpointRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/DeleteEndpointRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/DeleteEndpointRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/DeleteEndpointRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 666
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 667
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 671
    return-void

    .line 669
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public deletePlatformApplication(Lcom/amazonaws/services/sns/model/DeletePlatformApplicationRequest;)V
    .locals 5
    .param p1, "deletePlatformApplicationRequest"    # Lcom/amazonaws/services/sns/model/DeletePlatformApplicationRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 698
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 699
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 700
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 701
    const/4 v2, 0x0

    .line 702
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/DeletePlatformApplicationRequest;>;"
    const/4 v3, 0x0

    .line 704
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/DeletePlatformApplicationRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/DeletePlatformApplicationRequestMarshaller;-><init>()V

    .line 705
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/DeletePlatformApplicationRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/DeletePlatformApplicationRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 707
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 708
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 712
    return-void

    .line 710
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public deleteTopic(Lcom/amazonaws/services/sns/model/DeleteTopicRequest;)V
    .locals 5
    .param p1, "deleteTopicRequest"    # Lcom/amazonaws/services/sns/model/DeleteTopicRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 737
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 738
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 739
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 740
    const/4 v2, 0x0

    .line 741
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/DeleteTopicRequest;>;"
    const/4 v3, 0x0

    .line 743
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/DeleteTopicRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/DeleteTopicRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/DeleteTopicRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/DeleteTopicRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 745
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 746
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 748
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 750
    return-void

    .line 748
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public deleteTopic(Ljava/lang/String;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2298
    new-instance v0, Lcom/amazonaws/services/sns/model/DeleteTopicRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/DeleteTopicRequest;-><init>()V

    .line 2299
    .local v0, "deleteTopicRequest":Lcom/amazonaws/services/sns/model/DeleteTopicRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/DeleteTopicRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2300
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->deleteTopic(Lcom/amazonaws/services/sns/model/DeleteTopicRequest;)V

    .line 2301
    return-void
.end method

.method public getCachedResponseMetadata(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/ResponseMetadata;
    .locals 1
    .param p1, "request"    # Lcom/amazonaws/AmazonWebServiceRequest;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2808
    iget-object v0, p0, Lcom/amazonaws/services/sns/AmazonSNSClient;->client:Lcom/amazonaws/http/AmazonHttpClient;

    invoke-virtual {v0, p1}, Lcom/amazonaws/http/AmazonHttpClient;->getResponseMetadataForRequest(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/ResponseMetadata;

    move-result-object v0

    return-object v0
.end method

.method public getEndpointAttributes(Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;)Lcom/amazonaws/services/sns/model/GetEndpointAttributesResult;
    .locals 5
    .param p1, "getEndpointAttributesRequest"    # Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 782
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 783
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 784
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 785
    const/4 v2, 0x0

    .line 786
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;>;"
    const/4 v3, 0x0

    .line 788
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/GetEndpointAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetEndpointAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetEndpointAttributesRequestMarshaller;-><init>()V

    .line 789
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/GetEndpointAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/GetEndpointAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 791
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 792
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetEndpointAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetEndpointAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 794
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/GetEndpointAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 794
    return-object v4

    .line 796
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getPlatformApplicationAttributes(Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesRequest;)Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesResult;
    .locals 5
    .param p1, "getPlatformApplicationAttributesRequest"    # Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 830
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 831
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 832
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 833
    const/4 v2, 0x0

    .line 834
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesRequest;>;"
    const/4 v3, 0x0

    .line 836
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetPlatformApplicationAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetPlatformApplicationAttributesRequestMarshaller;-><init>()V

    .line 837
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/GetPlatformApplicationAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 839
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 840
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetPlatformApplicationAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetPlatformApplicationAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 842
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/GetPlatformApplicationAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 844
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 842
    return-object v4

    .line 844
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getSMSAttributes(Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;)Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;
    .locals 5
    .param p1, "getSMSAttributesRequest"    # Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 875
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 876
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 877
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 878
    const/4 v2, 0x0

    .line 879
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;>;"
    const/4 v3, 0x0

    .line 881
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 883
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 884
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 886
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 888
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 886
    return-object v4

    .line 888
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getSubscriptionAttributes(Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;)Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesResult;
    .locals 5
    .param p1, "getSubscriptionAttributesRequest"    # Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 918
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 919
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 920
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 921
    const/4 v2, 0x0

    .line 922
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;>;"
    const/4 v3, 0x0

    .line 924
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesRequestMarshaller;-><init>()V

    .line 925
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 927
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 928
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 930
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 932
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 930
    return-object v4

    .line 932
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getSubscriptionAttributes(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesResult;
    .locals 2
    .param p1, "subscriptionArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1789
    new-instance v0, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;-><init>()V

    .line 1790
    .local v0, "getSubscriptionAttributesRequest":Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;->setSubscriptionArn(Ljava/lang/String;)V

    .line 1791
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->getSubscriptionAttributes(Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;)Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesResult;

    move-result-object v1

    return-object v1
.end method

.method public getTopicAttributes(Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;)Lcom/amazonaws/services/sns/model/GetTopicAttributesResult;
    .locals 5
    .param p1, "getTopicAttributesRequest"    # Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 963
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 964
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 965
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 966
    const/4 v2, 0x0

    .line 967
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;>;"
    const/4 v3, 0x0

    .line 969
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/GetTopicAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetTopicAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetTopicAttributesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/GetTopicAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 971
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 972
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/GetTopicAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/GetTopicAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 974
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/GetTopicAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 976
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 974
    return-object v4

    .line 976
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public getTopicAttributes(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/GetTopicAttributesResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2007
    new-instance v0, Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;-><init>()V

    .line 2008
    .local v0, "getTopicAttributesRequest":Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2009
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->getTopicAttributes(Lcom/amazonaws/services/sns/model/GetTopicAttributesRequest;)Lcom/amazonaws/services/sns/model/GetTopicAttributesResult;

    move-result-object v1

    return-object v1
.end method

.method public listEndpointsByPlatformApplication(Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;)Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
    .locals 5
    .param p1, "listEndpointsByPlatformApplicationRequest"    # Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1016
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1017
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1018
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1019
    const/4 v2, 0x0

    .line 1020
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;>;"
    const/4 v3, 0x0

    .line 1022
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationRequestMarshaller;-><init>()V

    .line 1023
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1025
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1026
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1029
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1031
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1029
    return-object v4

    .line 1031
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listPhoneNumbersOptedOut(Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;)Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutResult;
    .locals 5
    .param p1, "listPhoneNumbersOptedOutRequest"    # Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1071
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1072
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1073
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1074
    const/4 v2, 0x0

    .line 1075
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;>;"
    const/4 v3, 0x0

    .line 1077
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutRequestMarshaller;-><init>()V

    .line 1078
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1080
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1081
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1083
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1085
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1083
    return-object v4

    .line 1085
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listPlatformApplications()Lcom/amazonaws/services/sns/model/ListPlatformApplicationsResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2783
    new-instance v0, Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;-><init>()V

    .line 2784
    .local v0, "listPlatformApplicationsRequest":Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listPlatformApplications(Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;)Lcom/amazonaws/services/sns/model/ListPlatformApplicationsResult;

    move-result-object v1

    return-object v1
.end method

.method public listPlatformApplications(Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;)Lcom/amazonaws/services/sns/model/ListPlatformApplicationsResult;
    .locals 5
    .param p1, "listPlatformApplicationsRequest"    # Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1124
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1125
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1126
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1127
    const/4 v2, 0x0

    .line 1128
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;>;"
    const/4 v3, 0x0

    .line 1130
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ListPlatformApplicationsResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListPlatformApplicationsRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListPlatformApplicationsRequestMarshaller;-><init>()V

    .line 1131
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ListPlatformApplicationsRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListPlatformApplicationsRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1133
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1134
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListPlatformApplicationsResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListPlatformApplicationsResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1136
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ListPlatformApplicationsResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1138
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1136
    return-object v4

    .line 1138
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listSubscriptions()Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2641
    new-instance v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;-><init>()V

    .line 2642
    .local v0, "listSubscriptionsRequest":Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listSubscriptions(Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;

    move-result-object v1

    return-object v1
.end method

.method public listSubscriptions(Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;
    .locals 5
    .param p1, "listSubscriptionsRequest"    # Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1171
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1172
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1173
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1174
    const/4 v2, 0x0

    .line 1175
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;>;"
    const/4 v3, 0x0

    .line 1177
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1179
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1180
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1182
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1184
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1182
    return-object v4

    .line 1184
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listSubscriptions(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;
    .locals 2
    .param p1, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2674
    new-instance v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;-><init>()V

    .line 2675
    .local v0, "listSubscriptionsRequest":Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;->setNextToken(Ljava/lang/String;)V

    .line 2676
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listSubscriptions(Lcom/amazonaws/services/sns/model/ListSubscriptionsRequest;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;

    move-result-object v1

    return-object v1
.end method

.method public listSubscriptionsByTopic(Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 5
    .param p1, "listSubscriptionsByTopicRequest"    # Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1218
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1219
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1220
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1221
    const/4 v2, 0x0

    .line 1222
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;>;"
    const/4 v3, 0x0

    .line 1224
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicRequestMarshaller;-><init>()V

    .line 1225
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1227
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1228
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1230
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1232
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1230
    return-object v4

    .line 1232
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listSubscriptionsByTopic(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2708
    new-instance v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;-><init>()V

    .line 2709
    .local v0, "listSubscriptionsByTopicRequest":Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2710
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listSubscriptionsByTopic(Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    move-result-object v1

    return-object v1
.end method

.method public listSubscriptionsByTopic(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2746
    new-instance v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;-><init>()V

    .line 2747
    .local v0, "listSubscriptionsByTopicRequest":Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2748
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->setNextToken(Ljava/lang/String;)V

    .line 2749
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listSubscriptionsByTopic(Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    move-result-object v1

    return-object v1
.end method

.method public listTopics()Lcom/amazonaws/services/sns/model/ListTopicsResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2067
    new-instance v0, Lcom/amazonaws/services/sns/model/ListTopicsRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListTopicsRequest;-><init>()V

    .line 2068
    .local v0, "listTopicsRequest":Lcom/amazonaws/services/sns/model/ListTopicsRequest;
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listTopics(Lcom/amazonaws/services/sns/model/ListTopicsRequest;)Lcom/amazonaws/services/sns/model/ListTopicsResult;

    move-result-object v1

    return-object v1
.end method

.method public listTopics(Lcom/amazonaws/services/sns/model/ListTopicsRequest;)Lcom/amazonaws/services/sns/model/ListTopicsResult;
    .locals 5
    .param p1, "listTopicsRequest"    # Lcom/amazonaws/services/sns/model/ListTopicsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1260
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1261
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1262
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1263
    const/4 v2, 0x0

    .line 1264
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListTopicsRequest;>;"
    const/4 v3, 0x0

    .line 1266
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/ListTopicsResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListTopicsRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListTopicsRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/ListTopicsRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListTopicsRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1268
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1269
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/ListTopicsResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/ListTopicsResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1270
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/ListTopicsResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1272
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1270
    return-object v4

    .line 1272
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public listTopics(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ListTopicsResult;
    .locals 2
    .param p1, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2098
    new-instance v0, Lcom/amazonaws/services/sns/model/ListTopicsRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListTopicsRequest;-><init>()V

    .line 2099
    .local v0, "listTopicsRequest":Lcom/amazonaws/services/sns/model/ListTopicsRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/ListTopicsRequest;->setNextToken(Ljava/lang/String;)V

    .line 2100
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->listTopics(Lcom/amazonaws/services/sns/model/ListTopicsRequest;)Lcom/amazonaws/services/sns/model/ListTopicsResult;

    move-result-object v1

    return-object v1
.end method

.method public optInPhoneNumber(Lcom/amazonaws/services/sns/model/OptInPhoneNumberRequest;)Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;
    .locals 5
    .param p1, "optInPhoneNumberRequest"    # Lcom/amazonaws/services/sns/model/OptInPhoneNumberRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1304
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1305
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1306
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1307
    const/4 v2, 0x0

    .line 1308
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/OptInPhoneNumberRequest;>;"
    const/4 v3, 0x0

    .line 1310
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/OptInPhoneNumberRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1312
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1313
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1315
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1317
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1315
    return-object v4

    .line 1317
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public publish(Lcom/amazonaws/services/sns/model/PublishRequest;)Lcom/amazonaws/services/sns/model/PublishResult;
    .locals 5
    .param p1, "publishRequest"    # Lcom/amazonaws/services/sns/model/PublishRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1366
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1367
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1368
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1369
    const/4 v2, 0x0

    .line 1370
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/PublishRequest;>;"
    const/4 v3, 0x0

    .line 1372
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/PublishResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/PublishRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/PublishRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/PublishRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/PublishRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1374
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1375
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/PublishResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/PublishResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1376
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/PublishResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1378
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1376
    return-object v4

    .line 1378
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public publish(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2464
    new-instance v0, Lcom/amazonaws/services/sns/model/PublishRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;-><init>()V

    .line 2465
    .local v0, "publishRequest":Lcom/amazonaws/services/sns/model/PublishRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2466
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/PublishRequest;->setMessage(Ljava/lang/String;)V

    .line 2467
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->publish(Lcom/amazonaws/services/sns/model/PublishRequest;)Lcom/amazonaws/services/sns/model/PublishResult;

    move-result-object v1

    return-object v1
.end method

.method public publish(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2609
    new-instance v0, Lcom/amazonaws/services/sns/model/PublishRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;-><init>()V

    .line 2610
    .local v0, "publishRequest":Lcom/amazonaws/services/sns/model/PublishRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2611
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/PublishRequest;->setMessage(Ljava/lang/String;)V

    .line 2612
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sns/model/PublishRequest;->setSubject(Ljava/lang/String;)V

    .line 2613
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->publish(Lcom/amazonaws/services/sns/model/PublishRequest;)Lcom/amazonaws/services/sns/model/PublishResult;

    move-result-object v1

    return-object v1
.end method

.method public removePermission(Lcom/amazonaws/services/sns/model/RemovePermissionRequest;)V
    .locals 5
    .param p1, "removePermissionRequest"    # Lcom/amazonaws/services/sns/model/RemovePermissionRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1404
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1405
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1406
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1407
    const/4 v2, 0x0

    .line 1408
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/RemovePermissionRequest;>;"
    const/4 v3, 0x0

    .line 1410
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/RemovePermissionRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/RemovePermissionRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/RemovePermissionRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/RemovePermissionRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1412
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1413
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1415
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1417
    return-void

    .line 1415
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public removePermission(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2038
    new-instance v0, Lcom/amazonaws/services/sns/model/RemovePermissionRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/RemovePermissionRequest;-><init>()V

    .line 2039
    .local v0, "removePermissionRequest":Lcom/amazonaws/services/sns/model/RemovePermissionRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/RemovePermissionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2040
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/RemovePermissionRequest;->setLabel(Ljava/lang/String;)V

    .line 2041
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->removePermission(Lcom/amazonaws/services/sns/model/RemovePermissionRequest;)V

    .line 2042
    return-void
.end method

.method public setEndpointAttributes(Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;)V
    .locals 5
    .param p1, "setEndpointAttributesRequest"    # Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1445
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1446
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1447
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1448
    const/4 v2, 0x0

    .line 1449
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;>;"
    const/4 v3, 0x0

    .line 1451
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SetEndpointAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SetEndpointAttributesRequestMarshaller;-><init>()V

    .line 1452
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/SetEndpointAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SetEndpointAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1454
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1455
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1457
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1459
    return-void

    .line 1457
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public setPlatformApplicationAttributes(Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;)V
    .locals 5
    .param p1, "setPlatformApplicationAttributesRequest"    # Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1491
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1492
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1493
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1494
    const/4 v2, 0x0

    .line 1495
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;>;"
    const/4 v3, 0x0

    .line 1497
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SetPlatformApplicationAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SetPlatformApplicationAttributesRequestMarshaller;-><init>()V

    .line 1498
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/SetPlatformApplicationAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1500
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1501
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1503
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1505
    return-void

    .line 1503
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public setSMSAttributes(Lcom/amazonaws/services/sns/model/SetSMSAttributesRequest;)Lcom/amazonaws/services/sns/model/SetSMSAttributesResult;
    .locals 5
    .param p1, "setSMSAttributesRequest"    # Lcom/amazonaws/services/sns/model/SetSMSAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1540
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1541
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1542
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1543
    const/4 v2, 0x0

    .line 1544
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SetSMSAttributesRequest;>;"
    const/4 v3, 0x0

    .line 1546
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/SetSMSAttributesResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SetSMSAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SetSMSAttributesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/SetSMSAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SetSMSAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1548
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1549
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SetSMSAttributesResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SetSMSAttributesResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1551
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/SetSMSAttributesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1553
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1551
    return-object v4

    .line 1553
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public setSubscriptionAttributes(Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;)V
    .locals 5
    .param p1, "setSubscriptionAttributesRequest"    # Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1581
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1582
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1583
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1584
    const/4 v2, 0x0

    .line 1585
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;>;"
    const/4 v3, 0x0

    .line 1587
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SetSubscriptionAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SetSubscriptionAttributesRequestMarshaller;-><init>()V

    .line 1588
    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/SetSubscriptionAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1590
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1591
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1593
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1595
    return-void

    .line 1593
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public setSubscriptionAttributes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "subscriptionArn"    # Ljava/lang/String;
    .param p2, "attributeName"    # Ljava/lang/String;
    .param p3, "attributeValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1757
    new-instance v0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;-><init>()V

    .line 1758
    .local v0, "setSubscriptionAttributesRequest":Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->setSubscriptionArn(Ljava/lang/String;)V

    .line 1759
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->setAttributeName(Ljava/lang/String;)V

    .line 1760
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->setAttributeValue(Ljava/lang/String;)V

    .line 1761
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->setSubscriptionAttributes(Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;)V

    .line 1762
    return-void
.end method

.method public setTopicAttributes(Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;)V
    .locals 5
    .param p1, "setTopicAttributesRequest"    # Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1619
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1620
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1621
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1622
    const/4 v2, 0x0

    .line 1623
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;>;"
    const/4 v3, 0x0

    .line 1625
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SetTopicAttributesRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SetTopicAttributesRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/SetTopicAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1627
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1628
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1630
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1632
    return-void

    .line 1630
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public setTopicAttributes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "attributeName"    # Ljava/lang/String;
    .param p3, "attributeValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2227
    new-instance v0, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;-><init>()V

    .line 2228
    .local v0, "setTopicAttributesRequest":Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->setTopicArn(Ljava/lang/String;)V

    .line 2229
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->setAttributeName(Ljava/lang/String;)V

    .line 2230
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->setAttributeValue(Ljava/lang/String;)V

    .line 2231
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->setTopicAttributes(Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;)V

    .line 2232
    return-void
.end method

.method public subscribe(Lcom/amazonaws/services/sns/model/SubscribeRequest;)Lcom/amazonaws/services/sns/model/SubscribeResult;
    .locals 5
    .param p1, "subscribeRequest"    # Lcom/amazonaws/services/sns/model/SubscribeRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1662
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1663
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1664
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1665
    const/4 v2, 0x0

    .line 1666
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SubscribeRequest;>;"
    const/4 v3, 0x0

    .line 1668
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Lcom/amazonaws/services/sns/model/SubscribeResult;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SubscribeRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SubscribeRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/SubscribeRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SubscribeRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1670
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1671
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/SubscribeResultStaxUnmarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/SubscribeResultStaxUnmarshaller;-><init>()V

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;

    move-result-object v3

    .line 1672
    invoke-virtual {v3}, Lcom/amazonaws/Response;->getAwsResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/amazonaws/services/sns/model/SubscribeResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1674
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1672
    return-object v4

    .line 1674
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public subscribe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SubscribeResult;
    .locals 2
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "endpoint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1927
    new-instance v0, Lcom/amazonaws/services/sns/model/SubscribeRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;-><init>()V

    .line 1928
    .local v0, "subscribeRequest":Lcom/amazonaws/services/sns/model/SubscribeRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->setTopicArn(Ljava/lang/String;)V

    .line 1929
    invoke-virtual {v0, p2}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->setProtocol(Ljava/lang/String;)V

    .line 1930
    invoke-virtual {v0, p3}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->setEndpoint(Ljava/lang/String;)V

    .line 1931
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->subscribe(Lcom/amazonaws/services/sns/model/SubscribeRequest;)Lcom/amazonaws/services/sns/model/SubscribeResult;

    move-result-object v1

    return-object v1
.end method

.method public unsubscribe(Lcom/amazonaws/services/sns/model/UnsubscribeRequest;)V
    .locals 5
    .param p1, "unsubscribeRequest"    # Lcom/amazonaws/services/sns/model/UnsubscribeRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 1707
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->createExecutionContext(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/http/ExecutionContext;

    move-result-object v1

    .line 1708
    .local v1, "executionContext":Lcom/amazonaws/http/ExecutionContext;
    invoke-virtual {v1}, Lcom/amazonaws/http/ExecutionContext;->getAwsRequestMetrics()Lcom/amazonaws/util/AWSRequestMetrics;

    move-result-object v0

    .line 1709
    .local v0, "awsRequestMetrics":Lcom/amazonaws/util/AWSRequestMetrics;
    sget-object v4, Lcom/amazonaws/util/AWSRequestMetrics$Field;->ClientExecuteTime:Lcom/amazonaws/util/AWSRequestMetrics$Field;

    invoke-virtual {v0, v4}, Lcom/amazonaws/util/AWSRequestMetrics;->startEvent(Lcom/amazonaws/metrics/MetricType;)V

    .line 1710
    const/4 v2, 0x0

    .line 1711
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/UnsubscribeRequest;>;"
    const/4 v3, 0x0

    .line 1713
    .local v3, "response":Lcom/amazonaws/Response;, "Lcom/amazonaws/Response<Ljava/lang/Void;>;"
    :try_start_0
    new-instance v4, Lcom/amazonaws/services/sns/model/transform/UnsubscribeRequestMarshaller;

    invoke-direct {v4}, Lcom/amazonaws/services/sns/model/transform/UnsubscribeRequestMarshaller;-><init>()V

    invoke-virtual {v4, p1}, Lcom/amazonaws/services/sns/model/transform/UnsubscribeRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/UnsubscribeRequest;)Lcom/amazonaws/Request;

    move-result-object v2

    .line 1715
    invoke-interface {v2, v0}, Lcom/amazonaws/Request;->setAWSRequestMetrics(Lcom/amazonaws/util/AWSRequestMetrics;)V

    .line 1716
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v1}, Lcom/amazonaws/services/sns/AmazonSNSClient;->invoke(Lcom/amazonaws/Request;Lcom/amazonaws/transform/Unmarshaller;Lcom/amazonaws/http/ExecutionContext;)Lcom/amazonaws/Response;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1718
    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    .line 1720
    return-void

    .line 1718
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v0, v2, v3}, Lcom/amazonaws/services/sns/AmazonSNSClient;->endClientExecution(Lcom/amazonaws/util/AWSRequestMetrics;Lcom/amazonaws/Request;Lcom/amazonaws/Response;)V

    throw v4
.end method

.method public unsubscribe(Ljava/lang/String;)V
    .locals 1
    .param p1, "subscriptionArn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amazonaws/AmazonServiceException;,
            Lcom/amazonaws/AmazonClientException;
        }
    .end annotation

    .prologue
    .line 2332
    new-instance v0, Lcom/amazonaws/services/sns/model/UnsubscribeRequest;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/UnsubscribeRequest;-><init>()V

    .line 2333
    .local v0, "unsubscribeRequest":Lcom/amazonaws/services/sns/model/UnsubscribeRequest;
    invoke-virtual {v0, p1}, Lcom/amazonaws/services/sns/model/UnsubscribeRequest;->setSubscriptionArn(Ljava/lang/String;)V

    .line 2334
    invoke-virtual {p0, v0}, Lcom/amazonaws/services/sns/AmazonSNSClient;->unsubscribe(Lcom/amazonaws/services/sns/model/UnsubscribeRequest;)V

    .line 2335
    return-void
.end method
