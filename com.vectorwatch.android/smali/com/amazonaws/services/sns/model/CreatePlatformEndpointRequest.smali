.class public Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "CreatePlatformEndpointRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private customUserData:Ljava/lang/String;

.field private platformApplicationArn:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addAttributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 340
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicated keys ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") are provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    return-object p0
.end method

.method public clearAttributesEntries()Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    .line 357
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 401
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 429
    :cond_0
    :goto_0
    return v3

    .line 403
    :cond_1
    if-eqz p1, :cond_0

    .line 406
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 408
    check-cast v0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;

    .line 410
    .local v0, "other":Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 412
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 413
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_9

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 417
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 421
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 422
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 424
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_c

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_d

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 426
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 427
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    move v3, v2

    .line 429
    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 410
    goto/16 :goto_1

    :cond_7
    move v4, v3

    goto/16 :goto_2

    :cond_8
    move v1, v3

    .line 415
    goto :goto_3

    :cond_9
    move v4, v3

    goto :goto_4

    :cond_a
    move v1, v3

    .line 419
    goto :goto_5

    :cond_b
    move v4, v3

    goto :goto_6

    :cond_c
    move v1, v3

    .line 424
    goto :goto_7

    :cond_d
    move v4, v3

    goto :goto_8
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public getCustomUserData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->customUserData:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformApplicationArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->platformApplicationArn:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->token:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 385
    const/16 v1, 0x1f

    .line 386
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 390
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    .line 391
    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 392
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 393
    mul-int/lit8 v4, v0, 0x1f

    .line 394
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 395
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_3

    :goto_3
    add-int v0, v2, v3

    .line 396
    return v0

    .line 390
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v2

    .line 391
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 392
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 394
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 395
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    .line 296
    return-void
.end method

.method public setCustomUserData(Ljava/lang/String;)V
    .locals 0
    .param p1, "customUserData"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->customUserData:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public setPlatformApplicationArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "platformApplicationArn"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->platformApplicationArn:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->token:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 372
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlatformApplicationArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CustomUserData: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getCustomUserData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 378
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    :cond_3
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->attributes:Ljava/util/Map;

    .line 318
    return-object p0
.end method

.method public withCustomUserData(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .locals 0
    .param p1, "customUserData"    # Ljava/lang/String;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->customUserData:Ljava/lang/String;

    .line 261
    return-object p0
.end method

.method public withPlatformApplicationArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .locals 0
    .param p1, "platformApplicationArn"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->platformApplicationArn:Ljava/lang/String;

    .line 132
    return-object p0
.end method

.method public withToken(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CreatePlatformEndpointRequest;->token:Ljava/lang/String;

    .line 207
    return-object p0
.end method
