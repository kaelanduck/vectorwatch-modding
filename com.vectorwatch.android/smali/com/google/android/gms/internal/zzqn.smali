.class public Lcom/google/android/gms/internal/zzqn;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzqn$zza;
    }
.end annotation


# static fields
.field public static final AGGREGATE_INPUT_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final zzaDq:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzamn$zza;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final zzaFA:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFB:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFC:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFD:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFE:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFF:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFG:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFH:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFI:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFJ:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFK:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFL:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFM:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFN:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFO:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFP:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFQ:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFR:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFS:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFT:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFU:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFV:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFW:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFX:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFY:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFZ:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFn:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFo:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFp:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFq:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFr:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFs:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFt:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFu:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFv:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFw:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFx:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFy:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaFz:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGa:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGb:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGc:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGd:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGe:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGf:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGg:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGh:Lcom/google/android/gms/internal/zzamn$zza;

.field public static final zzaGi:[Ljava/lang/String;

.field private static final zzaGj:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/internal/zzamn$zza;",
            "Lcom/google/android/gms/internal/zzqn$zza;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "com.google.step_count.delta"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEm:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFn:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.step_count.cumulative"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEm:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFo:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.step_count.cadence"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEE:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFp:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.activity.segment"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEj:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFq:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.floor_change"

    new-array v1, v8, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEj:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEk:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEL:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEO:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFr:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.calories.consumed"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEG:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFs:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.calories.expended"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEG:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFt:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.calories.bmr"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEG:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFu:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.power.sample"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEH:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFv:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.activity.sample"

    new-array v1, v6, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEj:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEk:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFw:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.accelerometer"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFd:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFe:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFf:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFx:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.sensor.events"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFi:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFg:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFh:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFy:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.internal.goal"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEx:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFz:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.heart_rate.bpm"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEr:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFA:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.location.sample"

    new-array v1, v8, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEs:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEt:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEu:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEv:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFB:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.location.track"

    new-array v1, v8, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEs:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEt:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEu:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEv:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFC:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.distance.delta"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEw:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFD:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.distance.cumulative"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEw:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFE:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.speed"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaED:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFF:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.cycling.wheel_revolution.cumulative"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEF:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFG:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.cycling.wheel_revolution.rpm"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEE:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFH:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.cycling.pedaling.cumulative"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEF:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFI:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.cycling.pedaling.cadence"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEE:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFJ:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.height"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEz:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFK:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.weight"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEA:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFL:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.body.fat.percentage"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEC:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFM:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.body.waist.circumference"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEB:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFN:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.body.hip.circumference"

    new-array v1, v5, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEB:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFO:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.nutrition"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEK:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEI:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEJ:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFP:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.activity.exercise"

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaER:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaES:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEn:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEU:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaET:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFQ:Lcom/google/android/gms/internal/zzamn$zza;

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFq:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v2, v2, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFs:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v2, v2, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFt:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v2, v2, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFD:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v2, v2, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFr:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v2, v2, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFA:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFB:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFP:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFF:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFn:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFL:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->AGGREGATE_INPUT_TYPES:Ljava/util/Set;

    const-string v0, "com.google.activity.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEj:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEn:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEV:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFR:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.floor_change.summary"

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEp:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEq:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEM:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEN:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEP:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/gms/internal/zzqm;->zzaEQ:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFS:Lcom/google/android/gms/internal/zzamn$zza;

    sget-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFn:Lcom/google/android/gms/internal/zzamn$zza;

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFT:Lcom/google/android/gms/internal/zzamn$zza;

    sget-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFD:Lcom/google/android/gms/internal/zzamn$zza;

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFU:Lcom/google/android/gms/internal/zzamn$zza;

    sget-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFs:Lcom/google/android/gms/internal/zzamn$zza;

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFV:Lcom/google/android/gms/internal/zzamn$zza;

    sget-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFt:Lcom/google/android/gms/internal/zzamn$zza;

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFW:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.heart_rate.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFX:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.location.bounding_box"

    new-array v1, v8, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEZ:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFa:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFb:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFc:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFY:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.power.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaFZ:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.speed.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGa:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.weight.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGb:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.calories.bmr.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGc:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.body.fat.percentage.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGd:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.body.hip.circumference.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGe:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.body.waist.circumference.summary"

    new-array v1, v7, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEW:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEX:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEY:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGf:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.nutrition.summary"

    new-array v1, v6, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEK:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEI:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGg:Lcom/google/android/gms/internal/zzamn$zza;

    const-string v0, "com.google.internal.session"

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/gms/internal/zzamn$zzb;

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFj:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaEj:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFk:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFl:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/gms/internal/zzqm;->zzaFm:Lcom/google/android/gms/internal/zzamn$zzb;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGh:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {}, Lcom/google/android/gms/internal/zzqn;->zzxP()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaDq:Ljava/util/Map;

    const/16 v0, 0x2b

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.google.accelerometer"

    aput-object v1, v0, v4

    const-string v1, "com.google.activity.exercise"

    aput-object v1, v0, v5

    const-string v1, "com.google.activity.sample"

    aput-object v1, v0, v6

    const-string v1, "com.google.activity.segment"

    aput-object v1, v0, v7

    const-string v1, "com.google.activity.summary"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "com.google.body.fat.percentage"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.google.body.fat.percentage.summary"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.google.body.hip.circumference"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.google.body.hip.circumference.summary"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.google.body.waist.circumference"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.google.body.waist.circumference.summary"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.google.calories.bmr"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.google.calories.bmr.summary"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.google.calories.consumed"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.google.calories.expended"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.google.cycling.pedaling.cadence"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.google.cycling.pedaling.cumulative"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.google.cycling.wheel_revolution.cumulative"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.google.cycling.wheel_revolution.rpm"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.google.distance.cumulative"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.google.distance.delta"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "com.google.floor_change"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "com.google.floor_change.summary"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "com.google.heart_rate.bpm"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "com.google.heart_rate.summary"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "com.google.height"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "com.google.internal.goal"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "com.google.internal.session"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "com.google.location.bounding_box"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "com.google.location.sample"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "com.google.location.track"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "com.google.nutrition"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "com.google.nutrition.summary"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "com.google.power.sample"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "com.google.power.summary"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "com.google.sensor.events"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "com.google.speed"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "com.google.speed.summary"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "com.google.step_count.cadence"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "com.google.step_count.cumulative"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "com.google.step_count.delta"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "com.google.weight"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "com.google.weight.summary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGi:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFo:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFE:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFI:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFD:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFn:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFt:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFs:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFr:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFM:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFO:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFN:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFP:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFK:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFL:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/gms/internal/zzqn;->zzaFA:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sget-object v4, Lcom/google/android/gms/internal/zzqn$zza;->zzaGk:Lcom/google/android/gms/internal/zzqn$zza;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/util/Map;Ljava/util/Collection;Lcom/google/android/gms/internal/zzqn$zza;)V

    sget-object v0, Lcom/google/android/gms/internal/zzqn$zza;->zzaGl:Lcom/google/android/gms/internal/zzqn$zza;

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/util/Map;Ljava/util/Collection;Lcom/google/android/gms/internal/zzqn$zza;)V

    sget-object v0, Lcom/google/android/gms/internal/zzqn$zza;->zzaGm:Lcom/google/android/gms/internal/zzqn$zza;

    invoke-static {v3, v2, v0}, Lcom/google/android/gms/internal/zzqn;->zza(Ljava/util/Map;Ljava/util/Collection;Lcom/google/android/gms/internal/zzqn$zza;)V

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGj:Ljava/util/Map;

    return-void
.end method

.method public static varargs zza(Ljava/lang/String;[Lcom/google/android/gms/internal/zzamn$zzb;)Lcom/google/android/gms/internal/zzamn$zza;
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/zzamn$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzamn$zza;-><init>()V

    iput-object p0, v0, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    iput-object p1, v0, Lcom/google/android/gms/internal/zzamn$zza;->zzcat:[Lcom/google/android/gms/internal/zzamn$zzb;

    return-object v0
.end method

.method private static zza(Ljava/util/Map;Ljava/util/Collection;Lcom/google/android/gms/internal/zzqn$zza;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/internal/zzamn$zza;",
            "Lcom/google/android/gms/internal/zzqn$zza;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/internal/zzamn$zza;",
            ">;",
            "Lcom/google/android/gms/internal/zzqn$zza;",
            ")V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzamn$zza;

    invoke-interface {p0, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static zzee(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/zzqn;->zzaGi:[Ljava/lang/String;

    invoke-static {v0, p0}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static zzxP()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzamn$zza;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFq:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFR:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFs:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFV:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFt:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFW:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFD:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFU:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFr:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFS:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFB:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFY:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFv:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFZ:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFA:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFX:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFF:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaGa:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFn:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaFT:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/gms/internal/zzqn;->zzaFL:Lcom/google/android/gms/internal/zzamn$zza;

    iget-object v1, v1, Lcom/google/android/gms/internal/zzamn$zza;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/internal/zzqn;->zzaGb:Lcom/google/android/gms/internal/zzamn$zza;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method
