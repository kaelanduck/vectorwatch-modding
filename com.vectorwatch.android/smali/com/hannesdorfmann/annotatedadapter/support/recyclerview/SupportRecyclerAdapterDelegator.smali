.class public interface abstract Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;
.super Ljava/lang/Object;
.source "SupportRecyclerAdapterDelegator.java"


# virtual methods
.method public abstract checkBinderInterfaceImplemented(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;)V
.end method

.method public abstract getViewTypeCount()I
.end method

.method public abstract onBindViewHolder(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
.end method

.method public abstract onCreateViewHolder(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
.end method
