.class public abstract Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SupportAnnotatedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static delgators:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerDelegators;


# instance fields
.field private adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

.field protected inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 23
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 25
    sget-object v2, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerDelegators;

    if-nez v2, :cond_0

    .line 27
    :try_start_0
    const-string v2, "com.hannesdorfmann.annotatedadapter.AutoSupportDelegators"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 28
    .local v0, "autoClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerDelegators;

    sput-object v2, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerDelegators;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .end local v0    # "autoClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    sget-object v2, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerDelegators;

    invoke-interface {v2, p0}, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerDelegators;->getDelegator(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;)Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

    move-result-object v2

    iput-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

    .line 38
    iget-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

    if-nez v2, :cond_1

    .line 39
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Could not load the AdapterDelegator!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 29
    :catch_0
    move-exception v1

    .line 30
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 31
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Could not load com.hannesdorfmann.annotatedadapter.AutoSupportDelegators"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 43
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

    invoke-interface {v2, p0}, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;->checkBinderInterfaceImplemented(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;)V

    .line 44
    return-void
.end method


# virtual methods
.method public getInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->inflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "i"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

    invoke-interface {v0, p0, p1, p2}, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;->onBindViewHolder(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 52
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "i"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;

    invoke-interface {v0, p0, p1, p2}, Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportRecyclerAdapterDelegator;->onCreateViewHolder(Lcom/hannesdorfmann/annotatedadapter/support/recyclerview/SupportAnnotatedAdapter;Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    return-object v0
.end method
