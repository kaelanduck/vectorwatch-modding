.class abstract Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SimpleAbsListAdapter.java"


# instance fields
.field protected context:Landroid/content/Context;

.field protected inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->context:Landroid/content/Context;

    .line 25
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 26
    return-void
.end method


# virtual methods
.method public abstract bindView(IILandroid/view/View;)V
.end method

.method public getInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->inflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->getItemViewType(I)I

    move-result v0

    .line 35
    .local v0, "type":I
    if-nez p2, :cond_0

    .line 36
    invoke-virtual {p0, v0, p3}, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->newView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 38
    :cond_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->bindView(IILandroid/view/View;)V

    .line 39
    return-object p2
.end method

.method public abstract newView(ILandroid/view/ViewGroup;)Landroid/view/View;
.end method
