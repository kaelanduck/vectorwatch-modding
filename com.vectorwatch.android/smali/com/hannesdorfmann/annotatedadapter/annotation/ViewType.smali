.class public interface abstract annotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
.super Ljava/lang/Object;
.source "ViewType.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        checkValue = true
        fields = {}
        initMethod = false
        views = {}
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Documented;
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->CLASS:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->FIELD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract checkValue()Z
.end method

.method public abstract fields()[Lcom/hannesdorfmann/annotatedadapter/annotation/Field;
.end method

.method public abstract initMethod()Z
.end method

.method public abstract layout()I
.end method

.method public abstract views()[Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
.end method
