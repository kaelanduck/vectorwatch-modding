.class public interface abstract Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;
.super Ljava/lang/Object;
.source "AbsListViewAdapterDelegator.java"


# virtual methods
.method public abstract checkBinderInterfaceImplemented(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;)V
.end method

.method public abstract getViewTypeCount()I
.end method

.method public abstract onBindViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/View;II)V
.end method

.method public abstract onCreateViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/ViewGroup;I)Landroid/view/View;
.end method
