.class public abstract Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
.super Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;
.source "AbsListViewAnnotatedAdapter.java"


# instance fields
.field private adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

.field private delgators:Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 18
    invoke-direct {p0, p1}, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;-><init>(Landroid/content/Context;)V

    .line 14
    iput-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    .line 15
    iput-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;

    .line 20
    iget-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;

    if-nez v2, :cond_0

    .line 22
    :try_start_0
    const-string v2, "com.hannesdorfmann.annotatedadapter.AutoAbsListViewDelegators"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 23
    .local v0, "autoClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;

    iput-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    .end local v0    # "autoClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    iget-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->delgators:Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;

    invoke-interface {v2, p0}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;->getDelegator(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;)Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    move-result-object v2

    iput-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    .line 33
    iget-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    if-nez v2, :cond_1

    .line 34
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Could not load the AdapterDelegator!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 24
    :catch_0
    move-exception v1

    .line 25
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 26
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Could not load com.hannesdorfmann.annotatedadapter.AutoAbsListViewDelegators"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 38
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v2, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    invoke-interface {v2, p0}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;->checkBinderInterfaceImplemented(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;)V

    .line 39
    return-void
.end method


# virtual methods
.method public bindView(IILandroid/view/View;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "type"    # I
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    invoke-interface {v0, p0, p3, p1, p2}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;->onBindViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/View;II)V

    .line 43
    return-void
.end method

.method public bridge synthetic getInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Landroid/view/ViewGroup;

    .prologue
    .line 12
    invoke-super {p0, p1, p2, p3}, Lcom/hannesdorfmann/annotatedadapter/SimpleAbsListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    invoke-interface {v0}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public newView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "type"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;->adapterDelegator:Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;

    invoke-interface {v0, p0, p2, p1}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;->onCreateViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
