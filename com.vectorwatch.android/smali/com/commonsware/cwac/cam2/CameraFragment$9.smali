.class Lcom/commonsware/cwac/cam2/CameraFragment$9;
.super Ljava/lang/Object;
.source "CameraFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/CameraFragment;->takePicture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraFragment;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 453
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$9;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 456
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment$9;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "output"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 458
    .local v1, "output":Landroid/net/Uri;
    new-instance v0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;

    invoke-direct {v0}, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;-><init>()V

    .line 460
    .local v0, "b":Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;
    if-eqz v1, :cond_0

    .line 461
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment$9;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment$9;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 462
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "updateMediaStore"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 461
    invoke-virtual {v0, v2, v1, v3}, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->toUri(Landroid/content/Context;Landroid/net/Uri;Z)Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;

    .line 465
    :cond_0
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment$9;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$100(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/commonsware/cwac/cam2/CameraController;

    move-result-object v2

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->build()Lcom/commonsware/cwac/cam2/PictureTransaction;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/commonsware/cwac/cam2/CameraController;->takePicture(Lcom/commonsware/cwac/cam2/PictureTransaction;)V

    .line 466
    return-void
.end method
