.class public final enum Lcom/commonsware/cwac/cam2/FlashMode;
.super Ljava/lang/Enum;
.source "FlashMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/commonsware/cwac/cam2/FlashMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/commonsware/cwac/cam2/FlashMode;

.field public static final enum ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

.field public static final enum AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

.field public static final enum OFF:Lcom/commonsware/cwac/cam2/FlashMode;

.field public static final enum REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;


# instance fields
.field private final cameraTwoMode:I

.field private final classicMode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 21
    new-instance v0, Lcom/commonsware/cwac/cam2/FlashMode;

    const-string v1, "OFF"

    const-string v2, "off"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/commonsware/cwac/cam2/FlashMode;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 23
    new-instance v0, Lcom/commonsware/cwac/cam2/FlashMode;

    const-string v1, "ALWAYS"

    const-string v2, "on"

    invoke-direct {v0, v1, v3, v2, v5}, Lcom/commonsware/cwac/cam2/FlashMode;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 25
    new-instance v0, Lcom/commonsware/cwac/cam2/FlashMode;

    const-string v1, "AUTO"

    const-string v2, "auto"

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/commonsware/cwac/cam2/FlashMode;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 27
    new-instance v0, Lcom/commonsware/cwac/cam2/FlashMode;

    const-string v1, "REDEYE"

    const-string v2, "red-eye"

    invoke-direct {v0, v1, v5, v2, v7}, Lcom/commonsware/cwac/cam2/FlashMode;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 20
    new-array v0, v7, [Lcom/commonsware/cwac/cam2/FlashMode;

    sget-object v1, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->$VALUES:[Lcom/commonsware/cwac/cam2/FlashMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "classicMode"    # Ljava/lang/String;
    .param p4, "cameraTwoMode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput-object p3, p0, Lcom/commonsware/cwac/cam2/FlashMode;->classicMode:Ljava/lang/String;

    .line 63
    iput p4, p0, Lcom/commonsware/cwac/cam2/FlashMode;->cameraTwoMode:I

    .line 64
    return-void
.end method

.method static lookupCameraTwoMode(I)Lcom/commonsware/cwac/cam2/FlashMode;
    .locals 1
    .param p0, "cameraTwoMode"    # I

    .prologue
    .line 48
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v0

    if-ne p0, v0, :cond_0

    .line 49
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 58
    :goto_0
    return-object v0

    .line 50
    :cond_0
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 51
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    goto :goto_0

    .line 52
    :cond_1
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v0

    if-ne p0, v0, :cond_2

    .line 53
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    goto :goto_0

    .line 54
    :cond_2
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v0

    if-ne p0, v0, :cond_3

    .line 55
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    goto :goto_0

    .line 58
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static lookupClassicMode(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/FlashMode;
    .locals 1
    .param p0, "classicMode"    # Ljava/lang/String;

    .prologue
    .line 34
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getClassicMode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 44
    :goto_0
    return-object v0

    .line 36
    :cond_0
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getClassicMode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    goto :goto_0

    .line 38
    :cond_1
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getClassicMode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    goto :goto_0

    .line 40
    :cond_2
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getClassicMode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 41
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    goto :goto_0

    .line 44
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/FlashMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/FlashMode;

    return-object v0
.end method

.method public static values()[Lcom/commonsware/cwac/cam2/FlashMode;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/commonsware/cwac/cam2/FlashMode;->$VALUES:[Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0}, [Lcom/commonsware/cwac/cam2/FlashMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/commonsware/cwac/cam2/FlashMode;

    return-object v0
.end method


# virtual methods
.method public getCameraTwoMode()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/commonsware/cwac/cam2/FlashMode;->cameraTwoMode:I

    return v0
.end method

.method public getClassicMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/FlashMode;->classicMode:Ljava/lang/String;

    return-object v0
.end method
