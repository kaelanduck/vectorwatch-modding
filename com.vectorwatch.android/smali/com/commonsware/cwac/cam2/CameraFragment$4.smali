.class Lcom/commonsware/cwac/cam2/CameraFragment$4;
.super Ljava/lang/Object;
.source "CameraFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/CameraFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraFragment;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$4;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 277
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$4;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$200(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/github/clans/fab/FloatingActionButton;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 280
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$4;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$100(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/commonsware/cwac/cam2/CameraController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraController;->switchCamera()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$4;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$100(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/commonsware/cwac/cam2/CameraController;

    move-result-object v1

    const/16 v2, 0xda7

    invoke-virtual {v1, v2, v0}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 283
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception switching camera"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
