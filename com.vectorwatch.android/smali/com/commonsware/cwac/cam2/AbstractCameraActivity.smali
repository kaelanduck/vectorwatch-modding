.class public abstract Lcom/commonsware/cwac/cam2/AbstractCameraActivity;
.super Landroid/app/Activity;
.source "AbstractCameraActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;,
        Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;
    }
.end annotation


# static fields
.field public static final EXTRA_ALLOW_SWITCH_FLASH_MODE:Ljava/lang/String; = "cwac_cam2_allow_switch_flash_mode"

.field public static final EXTRA_DEBUG_ENABLED:Ljava/lang/String; = "cwac_cam2_debug"

.field public static final EXTRA_FACING:Ljava/lang/String; = "cwac_cam2_facing"

.field public static final EXTRA_FACING_EXACT_MATCH:Ljava/lang/String; = "cwac_cam2_facing_exact_match"

.field public static final EXTRA_FLASH_MODES:Ljava/lang/String; = "cwac_cam2_flash_modes"

.field public static final EXTRA_FOCUS_MODE:Ljava/lang/String; = "cwac_cam2_focus_mode"

.field public static final EXTRA_FORCE_CLASSIC:Ljava/lang/String; = "cwac_cam2_force_classic"

.field public static final EXTRA_MIRROR_PREVIEW:Ljava/lang/String; = "cwac_cam2_mirror_preview"

.field public static final EXTRA_UNHANDLED_ERROR_RECEIVER:Ljava/lang/String; = "cwac_cam2_unhandled_error_receiver"

.field public static final EXTRA_UPDATE_MEDIA_STORE:Ljava/lang/String; = "cwac_cam2_update_media_store"

.field private static final REQUEST_PERMS:I = 0x3459

.field protected static final TAG_CAMERA:Ljava/lang/String;

.field public static cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const-class v0, Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->TAG_CAMERA:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 413
    return-void
.end method

.method private hasPermission(Ljava/lang/String;)Z
    .locals 2
    .param p1, "perm"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 376
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->useRuntimePermissions()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    invoke-virtual {p0, p1}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->checkSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 380
    :cond_0
    :goto_0
    return v0

    .line 377
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private netPermissions([Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "wanted"    # [Ljava/lang/String;

    .prologue
    .line 388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, p1, v2

    .line 391
    .local v0, "perm":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->hasPermission(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 392
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 396
    .end local v0    # "perm":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    return-object v2
.end method

.method private useRuntimePermissions()Z
    .locals 2

    .prologue
    .line 384
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract buildFragment()Lcom/commonsware/cwac/cam2/CameraFragment;
.end method

.method canSwitchSources()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 371
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cwac_cam2_facing_exact_match"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected abstract configEngine(Lcom/commonsware/cwac/cam2/CameraEngine;)V
.end method

.method protected abstract getNeededPermissions()[Ljava/lang/String;
.end method

.method protected getOutputUri()Landroid/net/Uri;
    .locals 8

    .prologue
    .line 292
    const/4 v1, 0x0

    .line 294
    .local v1, "output":Landroid/net/Uri;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 297
    .local v0, "clipData":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 298
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 302
    .end local v0    # "clipData":Landroid/content/ClipData;
    :cond_0
    if-nez v1, :cond_1

    .line 303
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 306
    :cond_1
    return-object v1
.end method

.method protected init()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 310
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    sget-object v10, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->TAG_CAMERA:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v9

    check-cast v9, Lcom/commonsware/cwac/cam2/CameraFragment;

    sput-object v9, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 312
    const/4 v6, 0x0

    .line 314
    .local v6, "fragNeedsToBeAdded":Z
    sget-object v9, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    if-nez v9, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->buildFragment()Lcom/commonsware/cwac/cam2/CameraFragment;

    move-result-object v9

    sput-object v9, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 316
    const/4 v6, 0x1

    .line 320
    :cond_0
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "cwac_cam2_focus_mode"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/commonsware/cwac/cam2/FocusMode;

    .line 322
    .local v4, "focusMode":Lcom/commonsware/cwac/cam2/FocusMode;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "cwac_cam2_allow_switch_flash_mode"

    invoke-virtual {v9, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 324
    .local v0, "allowChangeFlashMode":Z
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "cwac_cam2_unhandled_error_receiver"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/os/ResultReceiver;

    .line 326
    .local v8, "onError":Landroid/os/ResultReceiver;
    new-instance v2, Lcom/commonsware/cwac/cam2/CameraController;

    .line 328
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->isVideo()Z

    move-result v9

    invoke-direct {v2, v4, v8, v0, v9}, Lcom/commonsware/cwac/cam2/CameraController;-><init>(Lcom/commonsware/cwac/cam2/FocusMode;Landroid/os/ResultReceiver;ZZ)V

    .line 330
    .local v2, "ctrl":Lcom/commonsware/cwac/cam2/CameraController;
    sget-object v9, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v9, v2}, Lcom/commonsware/cwac/cam2/CameraFragment;->setController(Lcom/commonsware/cwac/cam2/CameraController;)V

    .line 331
    sget-object v9, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 332
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "cwac_cam2_mirror_preview"

    .line 333
    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 332
    invoke-virtual {v9, v10}, Lcom/commonsware/cwac/cam2/CameraFragment;->setMirrorPreview(Z)V

    .line 336
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "cwac_cam2_facing"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/commonsware/cwac/cam2/Facing;

    .line 338
    .local v3, "facing":Lcom/commonsware/cwac/cam2/Facing;
    if-nez v3, :cond_1

    .line 339
    sget-object v3, Lcom/commonsware/cwac/cam2/Facing;->BACK:Lcom/commonsware/cwac/cam2/Facing;

    .line 342
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "cwac_cam2_facing_exact_match"

    .line 343
    invoke-virtual {v9, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 344
    .local v7, "match":Z
    new-instance v9, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;

    invoke-direct {v9}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;-><init>()V

    .line 346
    invoke-virtual {v9, v3}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;

    move-result-object v9

    .line 347
    invoke-virtual {v9, v7}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->facingExactMatch(Z)Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;

    move-result-object v9

    .line 348
    invoke-virtual {v9}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->build()Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    move-result-object v1

    .line 350
    .local v1, "criteria":Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "cwac_cam2_force_classic"

    invoke-virtual {v9, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 352
    .local v5, "forceClassic":Z
    const-string v9, "samsung"

    sget-object v10, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "ha3gub"

    sget-object v10, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 353
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "k3gxx"

    sget-object v10, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 354
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 355
    :cond_2
    const/4 v5, 0x1

    .line 358
    :cond_3
    invoke-static {p0, v5}, Lcom/commonsware/cwac/cam2/CameraEngine;->buildInstance(Landroid/content/Context;Z)Lcom/commonsware/cwac/cam2/CameraEngine;

    move-result-object v9

    invoke-virtual {v2, v9, v1}, Lcom/commonsware/cwac/cam2/CameraController;->setEngine(Lcom/commonsware/cwac/cam2/CameraEngine;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V

    .line 359
    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/CameraController;->getEngine()Lcom/commonsware/cwac/cam2/CameraEngine;

    move-result-object v9

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "cwac_cam2_debug"

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/commonsware/cwac/cam2/CameraEngine;->setDebug(Z)V

    .line 360
    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/CameraController;->getEngine()Lcom/commonsware/cwac/cam2/CameraEngine;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->configEngine(Lcom/commonsware/cwac/cam2/CameraEngine;)V

    .line 362
    if-eqz v6, :cond_4

    .line 363
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    .line 364
    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v9

    const v10, 0x1020002

    sget-object v11, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    sget-object v12, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->TAG_CAMERA:Ljava/lang/String;

    .line 365
    invoke-virtual {v9, v10, v11, v12}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v9

    .line 366
    invoke-virtual {v9}, Landroid/app/FragmentTransaction;->commit()I

    .line 368
    :cond_4
    return-void
.end method

.method protected abstract isVideo()Z
.end method

.method protected abstract needsActionBar()Z
.end method

.method protected abstract needsOverlay()Z
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 174
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 175
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/high16 v4, 0x400000

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 176
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/high16 v4, 0x80000

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 177
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/high16 v4, 0x200000

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 178
    invoke-static {p0}, Lcom/commonsware/cwac/cam2/util/Utils;->validateEnvironment(Landroid/content/Context;)V

    .line 180
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->needsOverlay()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 181
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 188
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 191
    .local v0, "ab":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setElevation(F)V

    .line 210
    .end local v0    # "ab":Landroid/app/ActionBar;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->useRuntimePermissions()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 211
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getNeededPermissions()[Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->netPermissions([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, "perms":[Ljava/lang/String;
    array-length v3, v1

    if-nez v3, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->init()V

    .line 221
    .end local v1    # "perms":[Ljava/lang/String;
    :goto_1
    return-void

    .line 195
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 197
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 198
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setWillNotDraw(Z)V

    goto :goto_0

    .line 202
    .end local v2    # "v":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->needsActionBar()Z

    move-result v3

    if-nez v3, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 205
    .restart local v0    # "ab":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_0

    .line 216
    .end local v0    # "ab":Landroid/app/ActionBar;
    .restart local v1    # "perms":[Ljava/lang/String;
    :cond_3
    const/16 v3, 0x3459

    invoke-virtual {p0, v1, v3}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_1

    .line 219
    .end local v1    # "perms":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->init()V

    goto :goto_1
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraController$ControllerDestroyedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraController$ControllerDestroyedEvent;

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->finish()V

    .line 284
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->finish()V

    .line 279
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoGenericEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoGenericEvent;

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->finish()V

    .line 289
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 267
    const/16 v0, 0x1b

    if-ne p1, v0, :cond_0

    .line 268
    sget-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->performCameraAction()V

    .line 270
    const/4 v0, 0x1

    .line 273
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->getNeededPermissions()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->netPermissions([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "perms":[Ljava/lang/String;
    array-length v1, v0

    if-nez v1, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->init()V

    .line 235
    :goto_0
    return-void

    .line 232
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->setResult(I)V

    .line 233
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->finish()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 245
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 246
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 254
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 256
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    sget-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->stopVideoRecording()V

    .line 262
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 263
    return-void

    .line 259
    :cond_0
    sget-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->shutdown()V

    goto :goto_0
.end method
