.class public Lcom/commonsware/cwac/cam2/ImageContext;
.super Ljava/lang/Object;
.source "ImageContext.java"


# static fields
.field private static final LOG_2:D


# instance fields
.field private bmp:Landroid/graphics/Bitmap;

.field private ctxt:Landroid/content/Context;

.field private jpeg:[B

.field private thumbnail:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/commonsware/cwac/cam2/ImageContext;->LOG_2:D

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[B)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "jpeg"    # [B

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->ctxt:Landroid/content/Context;

    .line 43
    invoke-virtual {p0, p2}, Lcom/commonsware/cwac/cam2/ImageContext;->setJpeg([B)V

    .line 44
    return-void
.end method

.method private createBitmap(ILandroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "inSampleSize"    # I
    .param p2, "inBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "limit"    # I

    .prologue
    const/4 v1, 0x0

    .line 135
    new-instance v8, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v8}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 137
    .local v8, "opts":Landroid/graphics/BitmapFactory$Options;
    iput p1, v8, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 138
    iput-object p2, v8, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/ImageContext;->jpeg:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ImageContext;->jpeg:[B

    array-length v4, v4

    .line 144
    invoke-static {v2, v3, v4, v8}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 146
    .local v0, "result":Landroid/graphics/Bitmap;
    if-lez p3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v2

    if-le v2, p3, :cond_0

    .line 147
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v1, p2, p3}, Lcom/commonsware/cwac/cam2/ImageContext;->createBitmap(ILandroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 160
    .end local v0    # "result":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .line 150
    :catch_0
    move-exception v7

    .line 151
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v1, p2, p3}, Lcom/commonsware/cwac/cam2/ImageContext;->createBitmap(ILandroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 155
    .end local v7    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v0    # "result":Landroid/graphics/Bitmap;
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 156
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 158
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 160
    goto :goto_0
.end method

.method private createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "inBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "limit"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ImageContext;->jpeg:[B

    array-length v1, v1

    int-to-double v4, v1

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    mul-double/2addr v4, v6

    int-to-double v6, p2

    div-double v2, v4, v6

    .line 124
    .local v2, "ratio":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 125
    const/4 v1, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget-wide v6, Lcom/commonsware/cwac/cam2/ImageContext;->LOG_2:D

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    shl-int v0, v1, v4

    .line 130
    .local v0, "inSampleSize":I
    :goto_0
    invoke-direct {p0, v0, p1, p2}, Lcom/commonsware/cwac/cam2/ImageContext;->createBitmap(ILandroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1

    .line 127
    .end local v0    # "inSampleSize":I
    :cond_0
    const/4 v0, 0x1

    .restart local v0    # "inSampleSize":I
    goto :goto_0
.end method

.method private updateBitmap()V
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ImageContext;->bmp:Landroid/graphics/Bitmap;

    const/4 v2, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/commonsware/cwac/cam2/ImageContext;->createBitmap(ILandroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->bmp:Landroid/graphics/Bitmap;

    .line 165
    return-void
.end method


# virtual methods
.method public buildPreviewThumbnail(Landroid/content/Context;Ljava/lang/Float;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "quality"    # Ljava/lang/Float;

    .prologue
    const/high16 v6, 0x100000

    .line 93
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ImageContext;->thumbnail:Landroid/graphics/Bitmap;

    if-nez v4, :cond_2

    .line 94
    const v2, 0x1e8480

    .line 96
    .local v2, "limit":I
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    .line 97
    const-string v4, "activity"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 98
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v1, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 99
    .local v1, "flags":I
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    .line 101
    .local v3, "memoryClass":I
    and-int v4, v1, v6

    if-eqz v4, :cond_0

    .line 102
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v3

    .line 105
    :cond_0
    mul-int v4, v6, v3

    int-to-float v4, v4

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v2, v4

    .line 108
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "flags":I
    .end local v3    # "memoryClass":I
    :cond_1
    const/4 v4, 0x0

    invoke-direct {p0, v4, v2}, Lcom/commonsware/cwac/cam2/ImageContext;->createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/commonsware/cwac/cam2/ImageContext;->thumbnail:Landroid/graphics/Bitmap;

    .line 111
    .end local v2    # "limit":I
    :cond_2
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ImageContext;->thumbnail:Landroid/graphics/Bitmap;

    return-object v4
.end method

.method public buildResultThumbnail()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 117
    const/4 v0, 0x0

    const v1, 0xb71b0

    invoke-direct {p0, v0, v1}, Lcom/commonsware/cwac/cam2/ImageContext;->createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "force"    # Z

    .prologue
    .line 82
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->bmp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/ImageContext;->updateBitmap()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->bmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->ctxt:Landroid/content/Context;

    return-object v0
.end method

.method public getJpeg()[B
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->jpeg:[B

    return-object v0
.end method

.method public setJpeg([B)V
    .locals 1
    .param p1, "jpeg"    # [B

    .prologue
    const/4 v0, 0x0

    .line 67
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ImageContext;->jpeg:[B

    .line 68
    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->bmp:Landroid/graphics/Bitmap;

    .line 69
    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ImageContext;->thumbnail:Landroid/graphics/Bitmap;

    .line 70
    return-void
.end method
