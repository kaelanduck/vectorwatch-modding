.class Lcom/commonsware/cwac/cam2/CameraFragment$1;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "CameraFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraFragment;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$1;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 78
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    .line 81
    .local v1, "scale":F
    cmpl-float v2, v1, v3

    if-lez v2, :cond_1

    .line 82
    const/16 v0, 0x14

    .line 89
    .local v0, "delta":I
    :goto_0
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment$1;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->inSmoothPinchZoom:Z
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$000(Lcom/commonsware/cwac/cam2/CameraFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment$1;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$100(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/commonsware/cwac/cam2/CameraController;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/commonsware/cwac/cam2/CameraController;->changeZoom(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment$1;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    const/4 v3, 0x1

    # setter for: Lcom/commonsware/cwac/cam2/CameraFragment;->inSmoothPinchZoom:Z
    invoke-static {v2, v3}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$002(Lcom/commonsware/cwac/cam2/CameraFragment;Z)Z

    .line 94
    .end local v0    # "delta":I
    :cond_0
    return-void

    .line 83
    :cond_1
    cmpg-float v2, v1, v3

    if-gez v2, :cond_0

    .line 84
    const/16 v0, -0x14

    .restart local v0    # "delta":I
    goto :goto_0
.end method
