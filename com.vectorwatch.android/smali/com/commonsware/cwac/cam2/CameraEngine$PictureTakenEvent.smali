.class public Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;
.super Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;
.source "CameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PictureTakenEvent"
.end annotation


# instance fields
.field private imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

.field private xact:Lcom/commonsware/cwac/cam2/PictureTransaction;


# direct methods
.method public constructor <init>(Lcom/commonsware/cwac/cam2/PictureTransaction;Lcom/commonsware/cwac/cam2/ImageContext;)V
    .locals 0
    .param p1, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;
    .param p2, "imageContext"    # Lcom/commonsware/cwac/cam2/ImageContext;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>()V

    .line 162
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    .line 163
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    .line 164
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>(Ljava/lang/Exception;)V

    .line 168
    return-void
.end method


# virtual methods
.method public getImageContext()Lcom/commonsware/cwac/cam2/ImageContext;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    return-object v0
.end method

.method public getPictureTransaction()Lcom/commonsware/cwac/cam2/PictureTransaction;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    return-object v0
.end method
