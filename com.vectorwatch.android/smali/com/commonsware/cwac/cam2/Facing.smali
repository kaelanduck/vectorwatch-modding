.class public final enum Lcom/commonsware/cwac/cam2/Facing;
.super Ljava/lang/Enum;
.source "Facing.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/commonsware/cwac/cam2/Facing;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/commonsware/cwac/cam2/Facing;

.field public static final enum BACK:Lcom/commonsware/cwac/cam2/Facing;

.field public static final enum FRONT:Lcom/commonsware/cwac/cam2/Facing;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/commonsware/cwac/cam2/Facing;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v2}, Lcom/commonsware/cwac/cam2/Facing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/Facing;->FRONT:Lcom/commonsware/cwac/cam2/Facing;

    new-instance v0, Lcom/commonsware/cwac/cam2/Facing;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v3}, Lcom/commonsware/cwac/cam2/Facing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/Facing;->BACK:Lcom/commonsware/cwac/cam2/Facing;

    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/commonsware/cwac/cam2/Facing;

    sget-object v1, Lcom/commonsware/cwac/cam2/Facing;->FRONT:Lcom/commonsware/cwac/cam2/Facing;

    aput-object v1, v0, v2

    sget-object v1, Lcom/commonsware/cwac/cam2/Facing;->BACK:Lcom/commonsware/cwac/cam2/Facing;

    aput-object v1, v0, v3

    sput-object v0, Lcom/commonsware/cwac/cam2/Facing;->$VALUES:[Lcom/commonsware/cwac/cam2/Facing;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/Facing;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/commonsware/cwac/cam2/Facing;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/Facing;

    return-object v0
.end method

.method public static values()[Lcom/commonsware/cwac/cam2/Facing;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/commonsware/cwac/cam2/Facing;->$VALUES:[Lcom/commonsware/cwac/cam2/Facing;

    invoke-virtual {v0}, [Lcom/commonsware/cwac/cam2/Facing;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/commonsware/cwac/cam2/Facing;

    return-object v0
.end method


# virtual methods
.method isFront()Z
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/commonsware/cwac/cam2/Facing;->FRONT:Lcom/commonsware/cwac/cam2/Facing;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
