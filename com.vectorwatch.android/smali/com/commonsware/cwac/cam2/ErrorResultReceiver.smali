.class public Lcom/commonsware/cwac/cam2/ErrorResultReceiver;
.super Landroid/os/ResultReceiver;
.source "ErrorResultReceiver.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 17
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    .line 18
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ErrorResultReceiver;->activity:Landroid/app/Activity;

    .line 19
    return-void
.end method


# virtual methods
.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "resultCode"    # I
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1, p2}, Landroid/os/ResultReceiver;->onReceiveResult(ILandroid/os/Bundle;)V

    .line 25
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ErrorResultReceiver;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ErrorResultReceiver;->activity:Landroid/app/Activity;

    const-string v1, "We had an error"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 28
    :cond_0
    return-void
.end method
