.class public Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
.super Ljava/lang/Object;
.source "VideoTransaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/VideoTransaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field result:Lcom/commonsware/cwac/cam2/VideoTransaction;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/commonsware/cwac/cam2/VideoTransaction;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/commonsware/cwac/cam2/VideoTransaction;-><init>(Lcom/commonsware/cwac/cam2/VideoTransaction$1;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/VideoTransaction;

    return-void
.end method


# virtual methods
.method public build()Lcom/commonsware/cwac/cam2/VideoTransaction;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/VideoTransaction;

    return-object v0
.end method

.method public durationLimit(I)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    .locals 1
    .param p1, "durationLimit"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/VideoTransaction;

    iput p1, v0, Lcom/commonsware/cwac/cam2/VideoTransaction;->durationLimit:I

    .line 73
    return-object p0
.end method

.method public quality(I)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    .locals 1
    .param p1, "quality"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/VideoTransaction;

    iput p1, v0, Lcom/commonsware/cwac/cam2/VideoTransaction;->quality:I

    .line 61
    return-object p0
.end method

.method public sizeLimit(I)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    .locals 1
    .param p1, "sizeLimit"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/VideoTransaction;

    iput p1, v0, Lcom/commonsware/cwac/cam2/VideoTransaction;->sizeLimit:I

    .line 67
    return-object p0
.end method

.method public to(Ljava/io/File;)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    .locals 1
    .param p1, "outputPath"    # Ljava/io/File;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/VideoTransaction;

    iput-object p1, v0, Lcom/commonsware/cwac/cam2/VideoTransaction;->outputPath:Ljava/io/File;

    .line 55
    return-object p0
.end method
