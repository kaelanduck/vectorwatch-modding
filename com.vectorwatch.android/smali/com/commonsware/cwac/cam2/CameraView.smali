.class public Lcom/commonsware/cwac/cam2/CameraView;
.super Landroid/view/TextureView;
.source "CameraView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraView$StateCallback;
    }
.end annotation


# instance fields
.field private mirror:Z

.field private previewSize:Lcom/commonsware/cwac/cam2/util/Size;

.field private stateCallback:Lcom/commonsware/cwac/cam2/CameraView$StateCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->mirror:Z

    .line 56
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraView;->initListener()V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2, v0}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->mirror:Z

    .line 67
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraView;->initListener()V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->mirror:Z

    .line 82
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraView;->initListener()V

    .line 83
    return-void
.end method

.method private adjustAspectRatio(III)V
    .locals 15
    .param p1, "previewWidth"    # I
    .param p2, "previewHeight"    # I
    .param p3, "rotation"    # I

    .prologue
    .line 189
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 190
    .local v6, "txform":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraView;->getWidth()I

    move-result v10

    .line 191
    .local v10, "viewWidth":I
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraView;->getHeight()I

    move-result v9

    .line 192
    .local v9, "viewHeight":I
    new-instance v4, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v10

    int-to-float v14, v9

    invoke-direct {v4, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 193
    .local v4, "rectView":Landroid/graphics/RectF;
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    .line 194
    .local v7, "viewCenterX":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    .line 195
    .local v8, "viewCenterY":F
    new-instance v3, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move/from16 v0, p2

    int-to-float v13, v0

    move/from16 v0, p1

    int-to-float v14, v0

    invoke-direct {v3, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 196
    .local v3, "rectPreview":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    .line 197
    .local v1, "previewCenterX":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    .line 199
    .local v2, "previewCenterY":F
    const/4 v11, 0x1

    move/from16 v0, p3

    if-eq v11, v0, :cond_0

    const/4 v11, 0x3

    move/from16 v0, p3

    if-ne v11, v0, :cond_3

    .line 201
    :cond_0
    sub-float v11, v7, v1

    sub-float v12, v8, v2

    invoke-virtual {v3, v11, v12}, Landroid/graphics/RectF;->offset(FF)V

    .line 204
    sget-object v11, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v6, v4, v3, v11}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 207
    int-to-float v11, v9

    move/from16 v0, p2

    int-to-float v12, v0

    div-float/2addr v11, v12

    int-to-float v12, v10

    move/from16 v0, p1

    int-to-float v13, v0

    div-float/2addr v12, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 210
    .local v5, "scale":F
    invoke-virtual {v6, v5, v5, v7, v8}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 211
    add-int/lit8 v11, p3, -0x2

    mul-int/lit8 v11, v11, 0x5a

    int-to-float v11, v11

    invoke-virtual {v6, v11, v7, v8}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 219
    .end local v5    # "scale":F
    :cond_1
    :goto_0
    iget-boolean v11, p0, Lcom/commonsware/cwac/cam2/CameraView;->mirror:Z

    if-eqz v11, :cond_2

    .line 220
    const/high16 v11, -0x40800000    # -1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-virtual {v6, v11, v12, v7, v8}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 223
    :cond_2
    invoke-virtual {p0, v6}, Lcom/commonsware/cwac/cam2/CameraView;->setTransform(Landroid/graphics/Matrix;)V

    .line 224
    return-void

    .line 214
    :cond_3
    const/4 v11, 0x2

    move/from16 v0, p3

    if-ne v11, v0, :cond_1

    .line 215
    const/high16 v11, 0x43340000    # 180.0f

    invoke-virtual {v6, v11, v7, v8}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    goto :goto_0
.end method

.method private enterTheMatrix()V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v1

    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 179
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v2

    .line 180
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 178
    invoke-direct {p0, v1, v2, v0}, Lcom/commonsware/cwac/cam2/CameraView;->adjustAspectRatio(III)V

    .line 182
    :cond_0
    return-void
.end method

.method private initListener()V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0, p0}, Lcom/commonsware/cwac/cam2/CameraView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 143
    return-void
.end method


# virtual methods
.method public getPreviewSize()Lcom/commonsware/cwac/cam2/util/Size;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 104
    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onMeasure(II)V

    .line 106
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 107
    .local v2, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 108
    .local v0, "height":I
    const/4 v1, 0x1

    .line 110
    .local v1, "isFullBleed":Z
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    if-nez v3, :cond_0

    .line 111
    invoke-virtual {p0, v2, v0}, Lcom/commonsware/cwac/cam2/CameraView;->setMeasuredDimension(II)V

    .line 131
    :goto_0
    return-void

    .line 113
    :cond_0
    if-eqz v1, :cond_2

    .line 114
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v3

    mul-int/2addr v3, v0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v4

    div-int/2addr v3, v4

    if-le v2, v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 116
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v3

    mul-int/2addr v3, v2

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v4

    div-int/2addr v3, v4

    .line 115
    invoke-virtual {p0, v2, v3}, Lcom/commonsware/cwac/cam2/CameraView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 118
    :cond_1
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v3

    mul-int/2addr v3, v0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v4

    div-int/2addr v3, v4

    invoke-virtual {p0, v3, v0}, Lcom/commonsware/cwac/cam2/CameraView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 122
    :cond_2
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v3

    mul-int/2addr v3, v0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v4

    div-int/2addr v3, v4

    if-ge v2, v3, :cond_3

    .line 123
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 124
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v3

    mul-int/2addr v3, v2

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v4

    div-int/2addr v3, v4

    .line 123
    invoke-virtual {p0, v2, v3}, Lcom/commonsware/cwac/cam2/CameraView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 126
    :cond_3
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v3

    mul-int/2addr v3, v0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v4

    div-int/2addr v3, v4

    invoke-virtual {p0, v3, v0}, Lcom/commonsware/cwac/cam2/CameraView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .param p2, "i"    # I
    .param p3, "i1"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->stateCallback:Lcom/commonsware/cwac/cam2/CameraView$StateCallback;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraView;->stateCallback:Lcom/commonsware/cwac/cam2/CameraView$StateCallback;

    invoke-interface {v0, p0}, Lcom/commonsware/cwac/cam2/CameraView$StateCallback;->onReady(Lcom/commonsware/cwac/cam2/CameraView;)V

    .line 150
    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 159
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraView;->stateCallback:Lcom/commonsware/cwac/cam2/CameraView$StateCallback;

    if-eqz v1, :cond_0

    .line 161
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraView;->stateCallback:Lcom/commonsware/cwac/cam2/CameraView$StateCallback;

    invoke-interface {v1, p0}, Lcom/commonsware/cwac/cam2/CameraView$StateCallback;->onDestroyed(Lcom/commonsware/cwac/cam2/CameraView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception destroying state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .param p2, "i"    # I
    .param p3, "i1"    # I

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraView;->enterTheMatrix()V

    .line 155
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 174
    return-void
.end method

.method public setMirror(Z)V
    .locals 0
    .param p1, "mirror"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraView;->mirror:Z

    .line 139
    return-void
.end method

.method public setPreviewSize(Lcom/commonsware/cwac/cam2/util/Size;)V
    .locals 0
    .param p1, "previewSize"    # Lcom/commonsware/cwac/cam2/util/Size;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraView;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 98
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraView;->enterTheMatrix()V

    .line 99
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraView;->requestLayout()V

    .line 100
    return-void
.end method

.method public setStateCallback(Lcom/commonsware/cwac/cam2/CameraView$StateCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/commonsware/cwac/cam2/CameraView$StateCallback;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraView;->stateCallback:Lcom/commonsware/cwac/cam2/CameraView$StateCallback;

    .line 135
    return-void
.end method
