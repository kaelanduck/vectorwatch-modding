.class public Lcom/commonsware/cwac/cam2/ConfirmationFragment;
.super Landroid/app/Fragment;
.source "ConfirmationFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;
    }
.end annotation


# instance fields
.field private imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

.field private iv:Landroid/widget/ImageView;

.field private quality:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 136
    return-void
.end method

.method private getContract()Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;

    return-object v0
.end method

.method private loadImage(Ljava/lang/Float;)V
    .locals 3
    .param p1, "quality"    # Ljava/lang/Float;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->iv:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/commonsware/cwac/cam2/ImageContext;->buildPreviewThumbnail(Landroid/content/Context;Ljava/lang/Float;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 134
    return-void
.end method

.method public static newInstance()Lcom/commonsware/cwac/cam2/ConfirmationFragment;
    .locals 2

    .prologue
    .line 36
    new-instance v1, Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;-><init>()V

    .line 37
    .local v1, "result":Lcom/commonsware/cwac/cam2/ConfirmationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 54
    instance-of v0, p1, Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Hosting activity must implement Contract interface"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 59
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 46
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->setRetainInstance(Z)V

    .line 49
    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->setHasOptionsMenu(Z)V

    .line 50
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 101
    sget v0, Lcom/commonsware/cwac/cam2/R$menu;->cwac_cam2_confirm:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 102
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->iv:Landroid/widget/ImageView;

    .line 65
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->quality:Ljava/lang/Float;

    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->loadImage(Ljava/lang/Float;)V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->iv:Landroid/widget/ImageView;

    return-object v0
.end method

.method public onHiddenChanged(Z)V
    .locals 4
    .param p1, "isHidden"    # Z

    .prologue
    const/4 v3, 0x1

    .line 74
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 76
    if-nez p1, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 79
    .local v0, "ab":Landroid/app/ActionBar;
    if-nez v0, :cond_0

    .line 80
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "CameraActivity confirmation requires an action bar!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_action_bar_bg_translucent:I

    .line 84
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 85
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 87
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 88
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 89
    sget v1, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_ic_close_white:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 97
    .end local v0    # "ab":Landroid/app/ActionBar;
    :cond_1
    :goto_0
    return-void

    .line 91
    .restart local v0    # "ab":Landroid/app/ActionBar;
    :cond_2
    sget v1, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_ic_close_white:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 92
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 93
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 106
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 107
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getContract()Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;

    move-result-object v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;->completeRequest(Lcom/commonsware/cwac/cam2/ImageContext;Z)V

    .line 116
    :goto_0
    return v0

    .line 108
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_ok:I

    if-ne v1, v2, :cond_1

    .line 109
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getContract()Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;

    move-result-object v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    invoke-interface {v1, v2, v0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;->completeRequest(Lcom/commonsware/cwac/cam2/ImageContext;Z)V

    goto :goto_0

    .line 110
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_retry:I

    if-ne v1, v2, :cond_2

    .line 111
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->getContract()Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;

    move-result-object v1

    invoke-interface {v1}, Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;->retakePicture()V

    goto :goto_0

    .line 113
    :cond_2
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public setImage(Lcom/commonsware/cwac/cam2/ImageContext;Ljava/lang/Float;)V
    .locals 1
    .param p1, "imageContext"    # Lcom/commonsware/cwac/cam2/ImageContext;
    .param p2, "quality"    # Ljava/lang/Float;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->imageContext:Lcom/commonsware/cwac/cam2/ImageContext;

    .line 121
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->quality:Ljava/lang/Float;

    .line 123
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->iv:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0, p2}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->loadImage(Ljava/lang/Float;)V

    .line 126
    :cond_0
    return-void
.end method
