.class public Lcom/commonsware/cwac/cam2/CameraFragment;
.super Landroid/app/Fragment;
.source "CameraFragment.java"


# static fields
.field private static final ARG_DURATION_LIMIT:Ljava/lang/String; = "durationLimit"

.field private static final ARG_IS_VIDEO:Ljava/lang/String; = "isVideo"

.field private static final ARG_OUTPUT:Ljava/lang/String; = "output"

.field private static final ARG_QUALITY:Ljava/lang/String; = "quality"

.field private static final ARG_SIZE_LIMIT:Ljava/lang/String; = "sizeLimit"

.field private static final ARG_UPDATE_MEDIA_STORE:Ljava/lang/String; = "updateMediaStore"

.field private static final ARG_ZOOM_STYLE:Ljava/lang/String; = "zoomStyle"

.field private static final PINCH_ZOOM_DELTA:I = 0x14


# instance fields
.field private ctlr:Lcom/commonsware/cwac/cam2/CameraController;

.field private fabGallery:Lcom/github/clans/fab/FloatingActionButton;

.field private fabPicture:Lcom/github/clans/fab/FloatingActionButton;

.field private fabSettings:Lcom/github/clans/fab/FloatingActionButton;

.field private fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

.field private inSmoothPinchZoom:Z

.field private isVideoRecording:Z

.field private mirrorPreview:Z

.field private previewStack:Landroid/view/ViewGroup;

.field private scaleDetector:Landroid/view/ScaleGestureDetector;

.field private scaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field private seekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private zoomSlider:Landroid/widget/SeekBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 68
    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    .line 69
    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->mirrorPreview:Z

    .line 71
    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->inSmoothPinchZoom:Z

    .line 74
    new-instance v0, Lcom/commonsware/cwac/cam2/CameraFragment$1;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$1;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->scaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 96
    new-instance v0, Lcom/commonsware/cwac/cam2/CameraFragment$2;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$2;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->seekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/commonsware/cwac/cam2/CameraFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->inSmoothPinchZoom:Z

    return v0
.end method

.method static synthetic access$002(Lcom/commonsware/cwac/cam2/CameraFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->inSmoothPinchZoom:Z

    return p1
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/commonsware/cwac/cam2/CameraController;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/github/clans/fab/FloatingActionButton;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/commonsware/cwac/cam2/CameraFragment;)Landroid/view/ScaleGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->scaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method private canSwitchSources()Z
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->canSwitchSources()Z

    move-result v0

    return v0
.end method

.method private changeMenuIconAnimation(Lcom/github/clans/fab/FloatingActionMenu;)V
    .locals 14
    .param p1, "menu"    # Lcom/github/clans/fab/FloatingActionMenu;

    .prologue
    const-wide/16 v12, 0x96

    const-wide/16 v10, 0x32

    const/4 v8, 0x2

    .line 562
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 563
    .local v4, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionMenu;->getMenuIconView()Landroid/widget/ImageView;

    move-result-object v5

    .line 564
    .local v5, "v":Landroid/widget/ImageView;
    const-string v6, "scaleX"

    new-array v7, v8, [F

    fill-array-data v7, :array_0

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 565
    .local v2, "scaleOutX":Landroid/animation/ObjectAnimator;
    const-string v6, "scaleY"

    new-array v7, v8, [F

    fill-array-data v7, :array_1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 566
    .local v3, "scaleOutY":Landroid/animation/ObjectAnimator;
    const-string v6, "scaleX"

    new-array v7, v8, [F

    fill-array-data v7, :array_2

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 567
    .local v0, "scaleInX":Landroid/animation/ObjectAnimator;
    const-string v6, "scaleY"

    new-array v7, v8, [F

    fill-array-data v7, :array_3

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 569
    .local v1, "scaleInY":Landroid/animation/ObjectAnimator;
    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 570
    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 572
    invoke-virtual {v0, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 573
    invoke-virtual {v1, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 574
    new-instance v6, Lcom/commonsware/cwac/cam2/CameraFragment$10;

    invoke-direct {v6, p0, v5, p1}, Lcom/commonsware/cwac/cam2/CameraFragment$10;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;Landroid/widget/ImageView;Lcom/github/clans/fab/FloatingActionMenu;)V

    invoke-virtual {v0, v6}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 585
    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 586
    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 587
    new-instance v6, Landroid/view/animation/OvershootInterpolator;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-direct {v6, v7}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-virtual {v4, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 588
    invoke-virtual {p1, v4}, Lcom/github/clans/fab/FloatingActionMenu;->setIconToggleAnimatorSet(Landroid/animation/AnimatorSet;)V

    .line 589
    return-void

    .line 564
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
    .end array-data

    .line 565
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
    .end array-data

    .line 566
    :array_2
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3f800000    # 1.0f
    .end array-data

    .line 567
    :array_3
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getZoomStyle()Lcom/commonsware/cwac/cam2/ZoomStyle;
    .locals 3

    .prologue
    .line 592
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "zoomStyle"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/ZoomStyle;

    .line 594
    .local v0, "result":Lcom/commonsware/cwac/cam2/ZoomStyle;
    if-nez v0, :cond_0

    .line 595
    sget-object v0, Lcom/commonsware/cwac/cam2/ZoomStyle;->NONE:Lcom/commonsware/cwac/cam2/ZoomStyle;

    .line 598
    :cond_0
    return-object v0
.end method

.method private isVideo()Z
    .locals 3

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isVideo"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static newPictureInstance(Landroid/net/Uri;ZILcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraFragment;
    .locals 4
    .param p0, "output"    # Landroid/net/Uri;
    .param p1, "updateMediaStore"    # Z
    .param p2, "quality"    # I
    .param p3, "zoomStyle"    # Lcom/commonsware/cwac/cam2/ZoomStyle;

    .prologue
    .line 124
    new-instance v1, Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;-><init>()V

    .line 125
    .local v1, "f":Lcom/commonsware/cwac/cam2/CameraFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "output"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    const-string v2, "updateMediaStore"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    const-string v2, "quality"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    const-string v2, "isVideo"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    const-string/jumbo v2, "zoomStyle"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 132
    invoke-virtual {v1, v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->setArguments(Landroid/os/Bundle;)V

    .line 134
    return-object v1
.end method

.method public static newVideoInstance(Landroid/net/Uri;ZIII)Lcom/commonsware/cwac/cam2/CameraFragment;
    .locals 4
    .param p0, "output"    # Landroid/net/Uri;
    .param p1, "updateMediaStore"    # Z
    .param p2, "quality"    # I
    .param p3, "sizeLimit"    # I
    .param p4, "durationLimit"    # I

    .prologue
    .line 141
    new-instance v1, Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;-><init>()V

    .line 142
    .local v1, "f":Lcom/commonsware/cwac/cam2/CameraFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 144
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "output"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 145
    const-string v2, "updateMediaStore"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 146
    const-string v2, "isVideo"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    const-string v2, "quality"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const-string v2, "sizeLimit"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    const-string v2, "durationLimit"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    invoke-virtual {v1, v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->setArguments(Landroid/os/Bundle;)V

    .line 152
    return-object v1
.end method

.method private prepController()V
    .locals 5

    .prologue
    .line 544
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 545
    .local v0, "cameraViews":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/commonsware/cwac/cam2/CameraView;>;"
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->previewStack:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraView;

    .line 547
    .local v1, "cv":Lcom/commonsware/cwac/cam2/CameraView;
    iget-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->mirrorPreview:Z

    invoke-virtual {v1, v3}, Lcom/commonsware/cwac/cam2/CameraView;->setMirror(Z)V

    .line 548
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 550
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraController;->getNumberOfCameras()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 551
    new-instance v1, Lcom/commonsware/cwac/cam2/CameraView;

    .end local v1    # "cv":Lcom/commonsware/cwac/cam2/CameraView;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/commonsware/cwac/cam2/CameraView;-><init>(Landroid/content/Context;)V

    .line 552
    .restart local v1    # "cv":Lcom/commonsware/cwac/cam2/CameraView;
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lcom/commonsware/cwac/cam2/CameraView;->setVisibility(I)V

    .line 553
    iget-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->mirrorPreview:Z

    invoke-virtual {v1, v3}, Lcom/commonsware/cwac/cam2/CameraView;->setMirror(Z)V

    .line 554
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->previewStack:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 555
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 550
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 558
    :cond_0
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v3, v0}, Lcom/commonsware/cwac/cam2/CameraController;->setCameraViews(Ljava/util/Queue;)V

    .line 559
    return-void
.end method

.method private recordVideo()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 471
    iget-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    if-eqz v3, :cond_0

    .line 472
    invoke-direct {p0, v4}, Lcom/commonsware/cwac/cam2/CameraFragment;->stopVideoRecording(Z)V

    .line 500
    :goto_0
    return-void

    .line 475
    :cond_0
    :try_start_0
    new-instance v0, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;

    invoke-direct {v0}, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;-><init>()V

    .line 477
    .local v0, "b":Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "output"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 479
    .local v2, "output":Landroid/net/Uri;
    new-instance v3, Ljava/io/File;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->to(Ljava/io/File;)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;

    move-result-object v3

    .line 480
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "quality"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->quality(I)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;

    move-result-object v3

    .line 481
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "sizeLimit"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->sizeLimit(I)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;

    move-result-object v3

    .line 483
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "durationLimit"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 482
    invoke-virtual {v3, v4}, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->durationLimit(I)Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;

    .line 485
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;->build()Lcom/commonsware/cwac/cam2/VideoTransaction;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/commonsware/cwac/cam2/CameraController;->recordVideo(Lcom/commonsware/cwac/cam2/VideoTransaction;)V

    .line 486
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    .line 487
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v4, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_ic_stop:I

    invoke-virtual {v3, v4}, Lcom/github/clans/fab/FloatingActionButton;->setImageResource(I)V

    .line 489
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v4, Lcom/commonsware/cwac/cam2/R$color;->cwac_cam2_recording_fab:I

    invoke-virtual {v3, v4}, Lcom/github/clans/fab/FloatingActionButton;->setColorNormalResId(I)V

    .line 491
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v4, Lcom/commonsware/cwac/cam2/R$color;->cwac_cam2_recording_fab_pressed:I

    invoke-virtual {v3, v4}, Lcom/github/clans/fab/FloatingActionButton;->setColorPressedResId(I)V

    .line 493
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 494
    .end local v0    # "b":Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    .end local v2    # "output":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 495
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Exception recording video"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private setVideoFABToNormal()V
    .locals 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v1, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_ic_videocam:I

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setImageResource(I)V

    .line 526
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v1, Lcom/commonsware/cwac/cam2/R$color;->cwac_cam2_picture_fab:I

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setColorNormalResId(I)V

    .line 528
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v1, Lcom/commonsware/cwac/cam2/R$color;->cwac_cam2_picture_fab_pressed:I

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setColorPressedResId(I)V

    .line 530
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->canSwitchSources()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 531
    return-void
.end method

.method private stopVideoRecording(Z)V
    .locals 4
    .param p1, "abandon"    # Z

    .prologue
    const/4 v3, 0x0

    .line 511
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->setVideoFABToNormal()V

    .line 514
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v1, p1}, Lcom/commonsware/cwac/cam2/CameraController;->stopVideoRecording(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    iput-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    .line 521
    :goto_0
    return-void

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    const/16 v2, 0xda8

    invoke-virtual {v1, v2, v0}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 517
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception stopping recording of video"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 519
    iput-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-boolean v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    throw v1
.end method


# virtual methods
.method public getController()Lcom/commonsware/cwac/cam2/CameraController;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 162
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 164
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->setRetainInstance(Z)V

    .line 165
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->scaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->scaleDetector:Landroid/view/ScaleGestureDetector;

    .line 167
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 256
    sget v1, Lcom/commonsware/cwac/cam2/R$layout;->cwac_cam2_fragment:I

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 258
    .local v0, "v":Landroid/view/View;
    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_preview_stack:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->previewStack:Landroid/view/ViewGroup;

    .line 260
    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_picture:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/github/clans/fab/FloatingActionButton;

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    .line 262
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    sget v2, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_ic_videocam:I

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setImageResource(I)V

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraFragment$3;

    invoke-direct {v2, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$3;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_switch_camera:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/github/clans/fab/FloatingActionButton;

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    .line 274
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraFragment$4;

    invoke-direct {v2, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$4;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_settings_camera:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/github/clans/fab/FloatingActionButton;

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSettings:Lcom/github/clans/fab/FloatingActionButton;

    .line 289
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSettings:Lcom/github/clans/fab/FloatingActionButton;

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraFragment$5;

    invoke-direct {v2, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$5;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_gallery:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/github/clans/fab/FloatingActionButton;

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabGallery:Lcom/github/clans/fab/FloatingActionButton;

    .line 299
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabGallery:Lcom/github/clans/fab/FloatingActionButton;

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraFragment$6;

    invoke-direct {v2, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$6;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_settings:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/github/clans/fab/FloatingActionMenu;

    invoke-direct {p0, v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->changeMenuIconAnimation(Lcom/github/clans/fab/FloatingActionMenu;)V

    .line 309
    invoke-virtual {p0, v3}, Lcom/commonsware/cwac/cam2/CameraFragment;->onHiddenChanged(Z)V

    .line 312
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v1, v3}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 313
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v1, v3}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 315
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraController;->getNumberOfCameras()I

    move-result v1

    if-lez v1, :cond_1

    .line 316
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->prepController()V

    .line 319
    :cond_1
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraController;->destroy()V

    .line 242
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 243
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 324
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 325
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {p1, v0}, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;->isEventForController(Lcom/commonsware/cwac/cam2/CameraController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->prepController()V

    .line 376
    :cond_0
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    .prologue
    .line 380
    iget-object v0, p1, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;->exception:Ljava/lang/Exception;

    if-nez v0, :cond_3

    .line 381
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->canSwitchSources()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 382
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 383
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/commonsware/cwac/cam2/R$id;->cwac_cam2_zoom:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->zoomSlider:Landroid/widget/SeekBar;

    .line 385
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraController;->supportsZoom()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 386
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getZoomStyle()Lcom/commonsware/cwac/cam2/ZoomStyle;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/ZoomStyle;->PINCH:Lcom/commonsware/cwac/cam2/ZoomStyle;

    if-ne v0, v1, :cond_1

    .line 387
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->previewStack:Landroid/view/ViewGroup;

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraFragment$7;

    invoke-direct {v1, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$7;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 394
    :cond_1
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getZoomStyle()Lcom/commonsware/cwac/cam2/ZoomStyle;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/ZoomStyle;->SEEKBAR:Lcom/commonsware/cwac/cam2/ZoomStyle;

    if-ne v0, v1, :cond_0

    .line 395
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->zoomSlider:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->zoomSlider:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->seekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    goto :goto_0

    .line 399
    :cond_2
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->previewStack:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 400
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->zoomSlider:Landroid/widget/SeekBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    goto :goto_0

    .line 403
    :cond_3
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    const/16 v1, 0xda3

    iget-object v2, p1, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;->exception:Ljava/lang/Exception;

    invoke-virtual {v0, v1, v2}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 404
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$SmoothZoomCompletedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$SmoothZoomCompletedEvent;

    .prologue
    .line 440
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->inSmoothPinchZoom:Z

    .line 441
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->zoomSlider:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 442
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;

    .prologue
    const/4 v5, 0x0

    .line 410
    iput-boolean v5, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    .line 412
    iget-object v3, p1, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;->exception:Ljava/lang/Exception;

    if-nez v3, :cond_1

    .line 413
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "updateMediaStore"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 414
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 415
    .local v0, "app":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "output"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 416
    .local v1, "output":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 418
    .local v2, "path":Ljava/lang/String;
    new-instance v3, Lcom/commonsware/cwac/cam2/CameraFragment$8;

    invoke-direct {v3, p0, v0, v2}, Lcom/commonsware/cwac/cam2/CameraFragment$8;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;Landroid/content/Context;Ljava/lang/String;)V

    .line 426
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraFragment$8;->start()V

    .line 429
    .end local v0    # "app":Landroid/content/Context;
    .end local v1    # "output":Landroid/net/Uri;
    .end local v2    # "path":Ljava/lang/String;
    :cond_0
    iput-boolean v5, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    .line 430
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->setVideoFABToNormal()V

    .line 437
    :goto_0
    return-void

    .line 431
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 432
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->shutdown()V

    goto :goto_0

    .line 434
    :cond_2
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    const/16 v4, 0xda5

    iget-object v5, p1, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;->exception:Ljava/lang/Exception;

    invoke-virtual {v3, v4, v5}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 435
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public onHiddenChanged(Z)V
    .locals 4
    .param p1, "isHidden"    # Z

    .prologue
    const/4 v3, 0x0

    .line 185
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 187
    if-nez p1, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 190
    .local v0, "ab":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 192
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/commonsware/cwac/cam2/R$drawable;->cwac_cam2_action_bar_bg_transparent:I

    .line 193
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 191
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 196
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 197
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 204
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    if-eqz v1, :cond_1

    .line 205
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabPicture:Lcom/github/clans/fab/FloatingActionButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 206
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->fabSwitch:Lcom/github/clans/fab/FloatingActionButton;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->canSwitchSources()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/github/clans/fab/FloatingActionButton;->setEnabled(Z)V

    .line 209
    .end local v0    # "ab":Landroid/app/ActionBar;
    :cond_1
    return-void

    .line 199
    .restart local v0    # "ab":Landroid/app/ActionBar;
    :cond_2
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 200
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 174
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 176
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 178
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraController;->start()V

    .line 181
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 217
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    if-eqz v1, :cond_0

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraController;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :cond_0
    :goto_0
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 228
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 229
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    const/16 v2, 0xda6

    invoke-virtual {v1, v2, v0}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 222
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception stopping controller"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected performCameraAction()V
    .locals 1

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->recordVideo()V

    .line 450
    :goto_0
    return-void

    .line 448
    :cond_0
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->takePicture()V

    goto :goto_0
.end method

.method public setController(Lcom/commonsware/cwac/cam2/CameraController;)V
    .locals 3
    .param p1, "ctlr"    # Lcom/commonsware/cwac/cam2/CameraController;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    .line 357
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quality"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/commonsware/cwac/cam2/CameraController;->setQuality(I)V

    .line 358
    return-void
.end method

.method public setMirrorPreview(Z)V
    .locals 0
    .param p1, "mirror"    # Z

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->mirrorPreview:Z

    .line 369
    return-void
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 328
    iget-boolean v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->isVideoRecording:Z

    if-eqz v1, :cond_1

    .line 329
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->stopVideoRecording(Z)V

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    if-eqz v1, :cond_0

    .line 333
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraController;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    const/16 v2, 0xda6

    invoke-virtual {v1, v2, v0}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 336
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception stopping controller"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public stopVideoRecording()V
    .locals 1

    .prologue
    .line 507
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->stopVideoRecording(Z)V

    .line 508
    return-void
.end method

.method public takePicture()V
    .locals 2

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraFragment$9;

    invoke-direct {v1, p0}, Lcom/commonsware/cwac/cam2/CameraFragment$9;-><init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 468
    return-void
.end method

.method public updateUri(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "output"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 504
    return-void
.end method
