.class public Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;
.super Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
.source "VideoRecorderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/VideoRecorderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder",
        "<",
        "Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 100
    const-class v0, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;

    invoke-direct {p0, p1, v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;->forceClassic()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 112
    invoke-super {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method buildChooserBaseIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public durationLimit(I)Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;
    .locals 2
    .param p1, "limit"    # I

    .prologue
    .line 145
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "android.intent.extra.durationLimit"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 147
    return-object p0
.end method

.method public sizeLimit(I)Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;
    .locals 2
    .param p1, "limit"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "android.intent.extra.sizeLimit"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 134
    return-object p0
.end method

.method public bridge synthetic to(Landroid/net/Uri;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;->to(Landroid/net/Uri;)Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public to(Landroid/net/Uri;)Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;
    .locals 2
    .param p1, "output"    # Landroid/net/Uri;

    .prologue
    .line 117
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must be a file:/// Uri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    invoke-super {p0, p1}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->to(Landroid/net/Uri;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;

    return-object v0
.end method
