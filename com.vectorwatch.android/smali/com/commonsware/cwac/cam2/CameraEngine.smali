.class public abstract Lcom/commonsware/cwac/cam2/CameraEngine;
.super Ljava/lang/Object;
.source "CameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoPreviewFailureEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoPreviewErrorEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoGenericEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$SmoothZoomCompletedEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;,
        Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;
    }
.end annotation


# static fields
.field private static final CORE_POOL_SIZE:I = 0x1

.field private static final KEEP_ALIVE_SECONDS:I = 0x3c

.field private static final MAX_POOL_SIZE:I

.field private static volatile singletonClassic:Lcom/commonsware/cwac/cam2/CameraEngine;

.field private static volatile singletonTwo:Lcom/commonsware/cwac/cam2/CameraEngine;


# instance fields
.field private bus:Lde/greenrobot/event/EventBus;

.field protected eligibleFlashModes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/FlashMode;",
            ">;"
        }
    .end annotation
.end field

.field private isDebug:Z

.field private pool:Ljava/util/concurrent/ThreadPoolExecutor;

.field protected preferredFlashModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/FlashMode;",
            ">;"
        }
    .end annotation
.end field

.field private queue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private savePreviewFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lcom/commonsware/cwac/cam2/CameraEngine;->MAX_POOL_SIZE:I

    .line 40
    sput-object v1, Lcom/commonsware/cwac/cam2/CameraEngine;->singletonClassic:Lcom/commonsware/cwac/cam2/CameraEngine;

    .line 41
    sput-object v1, Lcom/commonsware/cwac/cam2/CameraEngine;->singletonTwo:Lcom/commonsware/cwac/cam2/CameraEngine;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->bus:Lde/greenrobot/event/EventBus;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->isDebug:Z

    .line 44
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->savePreviewFile:Ljava/io/File;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    .line 235
    return-void
.end method

.method public static declared-synchronized buildInstance(Landroid/content/Context;Z)Lcom/commonsware/cwac/cam2/CameraEngine;
    .locals 3
    .param p0, "ctxt"    # Landroid/content/Context;
    .param p1, "forceClassic"    # Z

    .prologue
    .line 337
    const-class v2, Lcom/commonsware/cwac/cam2/CameraEngine;

    monitor-enter v2

    const/4 v0, 0x0

    .line 338
    .local v0, "result":Lcom/commonsware/cwac/cam2/CameraEngine;
    :try_start_0
    sget-object v1, Lcom/commonsware/cwac/cam2/CameraEngine;->singletonClassic:Lcom/commonsware/cwac/cam2/CameraEngine;

    if-nez v1, :cond_0

    .line 339
    new-instance v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-direct {v1, p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/commonsware/cwac/cam2/CameraEngine;->singletonClassic:Lcom/commonsware/cwac/cam2/CameraEngine;

    .line 342
    :cond_0
    sget-object v0, Lcom/commonsware/cwac/cam2/CameraEngine;->singletonClassic:Lcom/commonsware/cwac/cam2/CameraEngine;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    monitor-exit v2

    return-object v0

    .line 337
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static isProblematicDeviceOnNewCameraApi()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 411
    const-string v1, "Huawei"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "angler"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 412
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    :cond_0
    :goto_0
    return v0

    .line 416
    :cond_1
    const-string v1, "LGE"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "occam"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 417
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 421
    :cond_2
    const-string v1, "Amazon"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "full_ford"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 422
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 426
    :cond_3
    const-string v1, "Amazon"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "full_thebes"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 427
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 431
    :cond_4
    const-string v1, "HTC"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "m7_google"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 432
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 436
    :cond_5
    const-string v1, "HTC"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "hiaeuhl_00709"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 437
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 441
    :cond_6
    const-string v1, "Wileyfox"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "Swift"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 442
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 446
    :cond_7
    const-string v1, "Sony"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "C6603"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 447
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    :cond_8
    const-string v1, "Sony"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "C6802"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 452
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 456
    :cond_9
    const-string v1, "Sony"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "D6603"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 457
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 461
    :cond_a
    const-string v1, "samsung"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string/jumbo v1, "zerofltexx"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 462
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 466
    :cond_b
    const-string v1, "OnePlus"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ONE E100"

    .line 467
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 471
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public abstract buildSession(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;
.end method

.method public abstract close(Lcom/commonsware/cwac/cam2/CameraSession;)V
.end method

.method public getBus()Lde/greenrobot/event/EventBus;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->bus:Lde/greenrobot/event/EventBus;

    return-object v0
.end method

.method public getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 8

    .prologue
    .line 390
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->pool:Ljava/util/concurrent/ThreadPoolExecutor;

    if-nez v0, :cond_0

    .line 391
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x1

    sget v3, Lcom/commonsware/cwac/cam2/CameraEngine;->MAX_POOL_SIZE:I

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->pool:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->pool:Ljava/util/concurrent/ThreadPoolExecutor;

    return-object v0
.end method

.method public abstract handleOrientationChange(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;)V
.end method

.method hasMoreThanOneEligibleFlashMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 407
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDebug()Z
    .locals 1

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->isDebug:Z

    return v0
.end method

.method public abstract loadCameraDescriptors(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
.end method

.method public abstract open(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
.end method

.method public abstract recordVideo(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/VideoTransaction;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public savePreviewFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->savePreviewFile:Ljava/io/File;

    return-object v0
.end method

.method public setBus(Lde/greenrobot/event/EventBus;)V
    .locals 0
    .param p1, "bus"    # Lde/greenrobot/event/EventBus;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->bus:Lde/greenrobot/event/EventBus;

    .line 355
    return-void
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1, "isDebug"    # Z

    .prologue
    .line 371
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->isDebug:Z

    .line 372
    return-void
.end method

.method public setDebugSavePreviewFile(Ljava/io/File;)V
    .locals 0
    .param p1, "savePreviewFile"    # Ljava/io/File;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->savePreviewFile:Ljava/io/File;

    .line 383
    return-void
.end method

.method setPreferredFlashModes(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/FlashMode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403
    .local p1, "flashModes":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/FlashMode;>;"
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->preferredFlashModes:Ljava/util/List;

    .line 404
    return-void
.end method

.method public setThreadPool(Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 0
    .param p1, "pool"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    .line 399
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine;->pool:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 400
    return-void
.end method

.method public abstract stopVideoRecording(Lcom/commonsware/cwac/cam2/CameraSession;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract supportsDynamicFlashModes()Z
.end method

.method public abstract supportsZoom(Lcom/commonsware/cwac/cam2/CameraSession;)Z
.end method

.method public abstract takePicture(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
.end method

.method public abstract zoomTo(Lcom/commonsware/cwac/cam2/CameraSession;I)Z
.end method
