.class public final enum Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;
.super Ljava/lang/Enum;
.source "AbstractCameraActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/AbstractCameraActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Quality"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

.field public static final enum HIGH:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

.field public static final enum LOW:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 400
    new-instance v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2, v2}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->LOW:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    new-instance v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v3, v3}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->HIGH:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    .line 399
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    sget-object v1, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->LOW:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->HIGH:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    aput-object v1, v0, v3

    sput-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->$VALUES:[Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 405
    iput p3, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->value:I

    .line 406
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 399
    const-class v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    return-object v0
.end method

.method public static values()[Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;
    .locals 1

    .prologue
    .line 399
    sget-object v0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->$VALUES:[Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    invoke-virtual {v0}, [Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    return-object v0
.end method


# virtual methods
.method getValue()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->value:I

    return v0
.end method
