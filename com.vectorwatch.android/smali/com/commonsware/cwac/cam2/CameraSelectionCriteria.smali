.class public Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;
.super Ljava/lang/Object;
.source "CameraSelectionCriteria.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;
    }
.end annotation


# instance fields
.field private facing:Lcom/commonsware/cwac/cam2/Facing;

.field private facingExactMatch:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facingExactMatch:Z

    .line 49
    return-void
.end method

.method static synthetic access$002(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/Facing;
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;
    .param p1, "x1"    # Lcom/commonsware/cwac/cam2/Facing;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facing:Lcom/commonsware/cwac/cam2/Facing;

    return-object p1
.end method

.method static synthetic access$102(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facingExactMatch:Z

    return p1
.end method


# virtual methods
.method public getFacing()Lcom/commonsware/cwac/cam2/Facing;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facing:Lcom/commonsware/cwac/cam2/Facing;

    return-object v0
.end method

.method public getFacingExactMatch()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facingExactMatch:Z

    return v0
.end method
