.class public abstract Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
.super Ljava/lang/Object;
.source "AbstractCameraActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/AbstractCameraActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "IntentBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final result:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 423
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    invoke-static {p1}, Lcom/commonsware/cwac/cam2/util/Utils;->validateEnvironment(Landroid/content/Context;)V

    .line 425
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    .line 426
    return-void
.end method


# virtual methods
.method public allowSwitchFlashMode()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 627
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_allow_switch_flash_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 629
    return-object p0
.end method

.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 436
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    return-object v0
.end method

.method public buildChooser(Ljava/lang/CharSequence;)Landroid/content/Intent;
    .locals 6
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 448
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .line 450
    .local v1, "original":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->buildChooserBaseIntent()Landroid/content/Intent;

    move-result-object v2

    .line 452
    .local v2, "toChooseFrom":Landroid/content/Intent;
    const-string v3, "output"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 453
    const-string v3, "output"

    const-string v4, "output"

    .line 454
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 453
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 457
    :cond_0
    invoke-static {v2, p1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 459
    .local v0, "chooser":Landroid/content/Intent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/content/Intent;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 462
    return-object v0
.end method

.method abstract buildChooserBaseIntent()Landroid/content/Intent;
.end method

.method public debug()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 497
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_debug"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    return-object p0
.end method

.method public facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 2
    .param p1, "facing"    # Lcom/commonsware/cwac/cam2/Facing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/commonsware/cwac/cam2/Facing;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 473
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_facing"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 475
    return-object p0
.end method

.method public facingExactMatch()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 485
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_facing_exact_match"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 487
    return-object p0
.end method

.method public flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 2
    .param p1, "mode"    # Lcom/commonsware/cwac/cam2/FlashMode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/commonsware/cwac/cam2/FlashMode;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 588
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/commonsware/cwac/cam2/FlashMode;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->flashModes([Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public flashModes(Ljava/util/List;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/FlashMode;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    .line 616
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    .local p1, "modes":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/FlashMode;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_flash_modes"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 619
    return-object p0
.end method

.method public flashModes([Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 1
    .param p1, "modes"    # [Lcom/commonsware/cwac/cam2/FlashMode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/commonsware/cwac/cam2/FlashMode;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 602
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->flashModes(Ljava/util/List;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 2
    .param p1, "focusMode"    # Lcom/commonsware/cwac/cam2/FocusMode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/commonsware/cwac/cam2/FocusMode;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 574
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_focus_mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 576
    return-object p0
.end method

.method public forceClassic()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 552
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_force_classic"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 554
    return-object p0
.end method

.method public mirrorPreview()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 563
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_mirror_preview"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 565
    return-object p0
.end method

.method public onError(Landroid/os/ResultReceiver;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 2
    .param p1, "rr"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/ResultReceiver;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 655
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_unhandled_error_receiver"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 657
    return-object p0
.end method

.method public quality(Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .param p1, "q"    # Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 642
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "android.intent.extra.videoQuality"

    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 644
    return-object p0
.end method

.method public to(Landroid/net/Uri;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 2
    .param p1, "output"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 525
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "output"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 527
    return-object p0
.end method

.method public to(Ljava/io/File;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 1
    .param p1, "f"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 512
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->to(Landroid/net/Uri;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public updateMediaStore()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 541
    .local p0, "this":Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;, "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder<TT;>;"
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_update_media_store"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 543
    return-object p0
.end method
