.class public final enum Lcom/commonsware/cwac/cam2/FocusMode;
.super Ljava/lang/Enum;
.source "FocusMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/commonsware/cwac/cam2/FocusMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/commonsware/cwac/cam2/FocusMode;

.field public static final enum CONTINUOUS:Lcom/commonsware/cwac/cam2/FocusMode;

.field public static final enum EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

.field public static final enum OFF:Lcom/commonsware/cwac/cam2/FocusMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/commonsware/cwac/cam2/FocusMode;

    const-string v1, "CONTINUOUS"

    invoke-direct {v0, v1, v2}, Lcom/commonsware/cwac/cam2/FocusMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FocusMode;->CONTINUOUS:Lcom/commonsware/cwac/cam2/FocusMode;

    new-instance v0, Lcom/commonsware/cwac/cam2/FocusMode;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lcom/commonsware/cwac/cam2/FocusMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FocusMode;->OFF:Lcom/commonsware/cwac/cam2/FocusMode;

    new-instance v0, Lcom/commonsware/cwac/cam2/FocusMode;

    const-string v1, "EDOF"

    invoke-direct {v0, v1, v4}, Lcom/commonsware/cwac/cam2/FocusMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/commonsware/cwac/cam2/FocusMode;->EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/commonsware/cwac/cam2/FocusMode;

    sget-object v1, Lcom/commonsware/cwac/cam2/FocusMode;->CONTINUOUS:Lcom/commonsware/cwac/cam2/FocusMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/commonsware/cwac/cam2/FocusMode;->OFF:Lcom/commonsware/cwac/cam2/FocusMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/commonsware/cwac/cam2/FocusMode;->EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/commonsware/cwac/cam2/FocusMode;->$VALUES:[Lcom/commonsware/cwac/cam2/FocusMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/FocusMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/FocusMode;

    return-object v0
.end method

.method public static values()[Lcom/commonsware/cwac/cam2/FocusMode;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/commonsware/cwac/cam2/FocusMode;->$VALUES:[Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0}, [Lcom/commonsware/cwac/cam2/FocusMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/commonsware/cwac/cam2/FocusMode;

    return-object v0
.end method
