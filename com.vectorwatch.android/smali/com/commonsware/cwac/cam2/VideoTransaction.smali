.class public Lcom/commonsware/cwac/cam2/VideoTransaction;
.super Ljava/lang/Object;
.source "VideoTransaction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/VideoTransaction$Builder;
    }
.end annotation


# instance fields
.field durationLimit:I

.field outputPath:Ljava/io/File;

.field quality:I

.field sizeLimit:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->quality:I

    .line 22
    iput v1, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->sizeLimit:I

    .line 23
    iput v1, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->durationLimit:I

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/commonsware/cwac/cam2/VideoTransaction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/commonsware/cwac/cam2/VideoTransaction$1;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/VideoTransaction;-><init>()V

    return-void
.end method


# virtual methods
.method public getDurationLimit()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->durationLimit:I

    return v0
.end method

.method public getOutputPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->outputPath:Ljava/io/File;

    return-object v0
.end method

.method public getQuality()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->quality:I

    return v0
.end method

.method public getSizeLimit()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/commonsware/cwac/cam2/VideoTransaction;->sizeLimit:I

    return v0
.end method
