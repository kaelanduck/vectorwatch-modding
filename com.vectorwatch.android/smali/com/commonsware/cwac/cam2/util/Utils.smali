.class public Lcom/commonsware/cwac/cam2/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    return-void
.end method

.method public static chooseOptimalSize(Ljava/util/List;IILcom/commonsware/cwac/cam2/util/Size;)Lcom/commonsware/cwac/cam2/util/Size;
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "aspectRatio"    # Lcom/commonsware/cwac/cam2/util/Size;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;II",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ")",
            "Lcom/commonsware/cwac/cam2/util/Size;"
        }
    .end annotation

    .prologue
    .line 148
    .local p0, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/util/Size;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v0, "bigEnough":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/util/Size;>;"
    invoke-virtual {p3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v3

    .line 150
    .local v3, "w":I
    invoke-virtual {p3}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v1

    .line 151
    .local v1, "h":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/commonsware/cwac/cam2/util/Size;

    .line 152
    .local v2, "option":Lcom/commonsware/cwac/cam2/util/Size;
    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v5

    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v6

    mul-int/2addr v6, v1

    div-int/2addr v6, v3

    if-ne v5, v6, :cond_0

    .line 153
    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    if-lt v5, p1, :cond_0

    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v5

    if-lt v5, p2, :cond_0

    .line 154
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 159
    .end local v2    # "option":Lcom/commonsware/cwac/cam2/util/Size;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 160
    new-instance v4, Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;

    invoke-direct {v4}, Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;-><init>()V

    invoke-static {v0, v4}, Ljava/util/Collections;->min(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/commonsware/cwac/cam2/util/Size;

    .line 163
    :goto_1
    return-object v4

    :cond_2
    new-instance v4, Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;

    invoke-direct {v4}, Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;-><init>()V

    invoke-static {p0, v4}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/commonsware/cwac/cam2/util/Size;

    goto :goto_1
.end method

.method public static getLargestPictureSize(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/util/Size;
    .locals 7
    .param p0, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 96
    const/4 v1, 0x0

    .line 98
    .local v1, "result":Lcom/commonsware/cwac/cam2/util/Size;
    invoke-interface {p0}, Lcom/commonsware/cwac/cam2/CameraDescriptor;->getPictureSizes()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/commonsware/cwac/cam2/util/Size;

    .line 99
    .local v3, "size":Lcom/commonsware/cwac/cam2/util/Size;
    if-nez v1, :cond_1

    .line 100
    move-object v1, v3

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    mul-int v2, v5, v6

    .line 103
    .local v2, "resultArea":I
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    mul-int v0, v5, v6

    .line 105
    .local v0, "newArea":I
    if-le v0, v2, :cond_0

    .line 106
    move-object v1, v3

    goto :goto_0

    .line 111
    .end local v0    # "newArea":I
    .end local v2    # "resultArea":I
    .end local v3    # "size":Lcom/commonsware/cwac/cam2/util/Size;
    :cond_2
    return-object v1
.end method

.method public static getSmallestPictureSize(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/util/Size;
    .locals 7
    .param p0, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 115
    const/4 v1, 0x0

    .line 117
    .local v1, "result":Lcom/commonsware/cwac/cam2/util/Size;
    invoke-interface {p0}, Lcom/commonsware/cwac/cam2/CameraDescriptor;->getPictureSizes()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/commonsware/cwac/cam2/util/Size;

    .line 118
    .local v3, "size":Lcom/commonsware/cwac/cam2/util/Size;
    if-nez v1, :cond_1

    .line 119
    move-object v1, v3

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    mul-int v2, v5, v6

    .line 122
    .local v2, "resultArea":I
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    mul-int v0, v5, v6

    .line 124
    .local v0, "newArea":I
    if-ge v0, v2, :cond_0

    .line 125
    move-object v1, v3

    goto :goto_0

    .line 130
    .end local v0    # "newArea":I
    .end local v2    # "resultArea":I
    .end local v3    # "size":Lcom/commonsware/cwac/cam2/util/Size;
    :cond_2
    return-object v1
.end method

.method public static isSystemBarOnBottom(Landroid/content/Context;)Z
    .locals 8
    .param p0, "ctxt"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 86
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 87
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 88
    .local v1, "cfg":Landroid/content/res/Configuration;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 89
    .local v2, "dm":Landroid/util/DisplayMetrics;
    iget v6, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v7, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eq v6, v7, :cond_2

    iget v6, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v7, 0x258

    if-ge v6, v7, :cond_2

    move v0, v5

    .line 92
    .local v0, "canMove":Z
    :goto_0
    if-eqz v0, :cond_0

    iget v6, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v7, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v6, v7, :cond_1

    :cond_0
    move v4, v5

    :cond_1
    return v4

    .end local v0    # "canMove":Z
    :cond_2
    move v0, v4

    .line 89
    goto :goto_0
.end method

.method public static validateEnvironment(Landroid/content/Context;)V
    .locals 5
    .param p0, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 52
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_0

    .line 53
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "App is running on device older than API Level 14"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 56
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 58
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v3, "android.hardware.camera.any"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.hardware.camera"

    .line 59
    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 60
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "App is running on device that lacks a camera"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 63
    :cond_1
    instance-of v3, p0, Lcom/commonsware/cwac/cam2/CameraActivity;

    if-eqz v3, :cond_2

    .line 65
    :try_start_0
    check-cast p0, Lcom/commonsware/cwac/cam2/CameraActivity;

    .end local p0    # "ctxt":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 67
    .local v1, "info":Landroid/content/pm/ActivityInfo;
    iget-boolean v3, v1, Landroid/content/pm/ActivityInfo;->exported:Z

    if-eqz v3, :cond_2

    .line 68
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "A CameraActivity cannot be exported!"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .end local v1    # "info":Landroid/content/pm/ActivityInfo;
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot find this activity!"

    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 74
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    return-void
.end method
