.class Lcom/commonsware/cwac/cam2/CameraFragment$2;
.super Ljava/lang/Object;
.source "CameraFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraFragment;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraFragment;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraFragment$2;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 102
    if-eqz p3, :cond_0

    .line 103
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraFragment$2;->this$0:Lcom/commonsware/cwac/cam2/CameraFragment;

    # getter for: Lcom/commonsware/cwac/cam2/CameraFragment;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->access$100(Lcom/commonsware/cwac/cam2/CameraFragment;)Lcom/commonsware/cwac/cam2/CameraController;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/commonsware/cwac/cam2/CameraController;->setZoom(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 107
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 112
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 117
    return-void
.end method
