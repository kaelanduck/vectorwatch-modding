.class public Lcom/commonsware/cwac/cam2/CameraController;
.super Ljava/lang/Object;
.source "CameraController.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraView$StateCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraController$ControllerDestroyedEvent;,
        Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;,
        Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;
    }
.end annotation


# instance fields
.field private final allowChangeFlashMode:Z

.field private availablePreviews:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraView;",
            ">;"
        }
    .end annotation
.end field

.field private cameras:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private currentCamera:I

.field private engine:Lcom/commonsware/cwac/cam2/CameraEngine;

.field private flashModePlugin:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

.field private final focusMode:Lcom/commonsware/cwac/cam2/FocusMode;

.field private final isVideo:Z

.field private isVideoRecording:Z

.field private final onError:Landroid/os/ResultReceiver;

.field private final previews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraDescriptor;",
            "Lcom/commonsware/cwac/cam2/CameraView;",
            ">;"
        }
    .end annotation
.end field

.field private quality:I

.field private session:Lcom/commonsware/cwac/cam2/CameraSession;

.field private switchPending:Z

.field private zoomLevel:I


# direct methods
.method public constructor <init>(Lcom/commonsware/cwac/cam2/FocusMode;Landroid/os/ResultReceiver;ZZ)V
    .locals 3
    .param p1, "focusMode"    # Lcom/commonsware/cwac/cam2/FocusMode;
    .param p2, "onError"    # Landroid/os/ResultReceiver;
    .param p3, "allowChangeFlashMode"    # Z
    .param p4, "isVideo"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    .line 49
    iput v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->previews:Ljava/util/HashMap;

    .line 52
    iput-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->availablePreviews:Ljava/util/Queue;

    .line 53
    iput-boolean v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->switchPending:Z

    .line 54
    iput-boolean v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideoRecording:Z

    .line 58
    iput v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    .line 59
    iput v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->quality:I

    .line 66
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraController;->onError:Landroid/os/ResultReceiver;

    .line 67
    if-nez p1, :cond_0

    sget-object p1, Lcom/commonsware/cwac/cam2/FocusMode;->CONTINUOUS:Lcom/commonsware/cwac/cam2/FocusMode;

    .end local p1    # "focusMode":Lcom/commonsware/cwac/cam2/FocusMode;
    :cond_0
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraController;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;

    .line 69
    iput-boolean p4, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideo:Z

    .line 70
    iput-boolean p3, p0, Lcom/commonsware/cwac/cam2/CameraController;->allowChangeFlashMode:Z

    .line 71
    return-void
.end method

.method private getNextCameraIndex()I
    .locals 2

    .prologue
    .line 288
    iget v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    add-int/lit8 v0, v1, 0x1

    .line 290
    .local v0, "next":I
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 291
    const/4 v0, 0x0

    .line 294
    :cond_0
    return v0
.end method

.method private getPreview(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraView;
    .locals 3
    .param p1, "camera"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 274
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->previews:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraView;

    .line 277
    .local v1, "result":Lcom/commonsware/cwac/cam2/CameraView;
    if-nez v1, :cond_0

    .line 278
    :try_start_0
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->availablePreviews:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraView;

    move-object v1, v0

    .line 279
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->previews:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :cond_0
    :goto_0
    return-object v1

    .line 281
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private handleZoom()Z
    .locals 3

    .prologue
    const/16 v1, 0x64

    .line 264
    iget v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    if-gez v0, :cond_1

    .line 265
    const/4 v0, 0x0

    iput v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    .line 270
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    iget v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    invoke-virtual {v0, v1, v2}, Lcom/commonsware/cwac/cam2/CameraEngine;->zoomTo(Lcom/commonsware/cwac/cam2/CameraSession;I)Z

    move-result v0

    return v0

    .line 266
    :cond_1
    iget v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    if-le v0, v1, :cond_0

    .line 267
    iput v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    goto :goto_0
.end method

.method private open()V
    .locals 10

    .prologue
    .line 298
    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    if-nez v5, :cond_2

    .line 299
    const/4 v3, 0x0

    .line 300
    .local v3, "previewSize":Lcom/commonsware/cwac/cam2/util/Size;
    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    iget v6, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .line 301
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraDescriptor;
    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraController;->getPreview(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraView;

    move-result-object v1

    .line 304
    .local v1, "cv":Lcom/commonsware/cwac/cam2/CameraView;
    iget v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->quality:I

    if-lez v5, :cond_3

    .line 305
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/util/Utils;->getLargestPictureSize(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v2

    .line 310
    .local v2, "pictureSize":Lcom/commonsware/cwac/cam2/util/Size;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getWidth()I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getHeight()I

    move-result v5

    if-lez v5, :cond_0

    .line 311
    invoke-interface {v0}, Lcom/commonsware/cwac/cam2/CameraDescriptor;->getPreviewSizes()Ljava/util/ArrayList;

    move-result-object v5

    .line 312
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getWidth()I

    move-result v6

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getHeight()I

    move-result v7

    .line 311
    invoke-static {v5, v6, v7, v2}, Lcom/commonsware/cwac/cam2/util/Utils;->chooseOptimalSize(Ljava/util/List;IILcom/commonsware/cwac/cam2/util/Size;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v3

    .line 315
    :cond_0
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v4

    .line 317
    .local v4, "texture":Landroid/graphics/SurfaceTexture;
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    .line 318
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_1

    .line 319
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    .line 320
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    .line 319
    invoke-virtual {v4, v5, v6}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 323
    :cond_1
    new-instance v5, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    invoke-direct {v5}, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;-><init>()V

    iput-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->flashModePlugin:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    .line 325
    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    .line 326
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Lcom/commonsware/cwac/cam2/CameraEngine;->buildSession(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;

    move-result-object v5

    new-instance v6, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    const/16 v7, 0x100

    invoke-direct {v6, v3, v2, v7}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;-><init>(Lcom/commonsware/cwac/cam2/util/Size;Lcom/commonsware/cwac/cam2/util/Size;I)V

    .line 327
    invoke-virtual {v5, v6}, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->addPlugin(Lcom/commonsware/cwac/cam2/CameraPlugin;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;

    move-result-object v5

    new-instance v6, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    .line 329
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v6}, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->addPlugin(Lcom/commonsware/cwac/cam2/CameraPlugin;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;

    move-result-object v5

    new-instance v6, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    .line 330
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/commonsware/cwac/cam2/CameraController;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;

    iget-boolean v9, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideo:Z

    invoke-direct {v6, v7, v8, v9}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/FocusMode;Z)V

    invoke-virtual {v5, v6}, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->addPlugin(Lcom/commonsware/cwac/cam2/CameraPlugin;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraController;->flashModePlugin:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    .line 331
    invoke-virtual {v5, v6}, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->addPlugin(Lcom/commonsware/cwac/cam2/CameraPlugin;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;

    move-result-object v5

    .line 332
    invoke-virtual {v5}, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->build()Lcom/commonsware/cwac/cam2/CameraSession;

    move-result-object v5

    iput-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 334
    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v5, v3}, Lcom/commonsware/cwac/cam2/CameraSession;->setPreviewSize(Lcom/commonsware/cwac/cam2/util/Size;)V

    .line 335
    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v5, v6, v4}, Lcom/commonsware/cwac/cam2/CameraEngine;->open(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V

    .line 338
    .end local v0    # "camera":Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .end local v1    # "cv":Lcom/commonsware/cwac/cam2/CameraView;
    .end local v2    # "pictureSize":Lcom/commonsware/cwac/cam2/util/Size;
    .end local v3    # "previewSize":Lcom/commonsware/cwac/cam2/util/Size;
    .end local v4    # "texture":Landroid/graphics/SurfaceTexture;
    :cond_2
    return-void

    .line 307
    .restart local v0    # "camera":Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .restart local v1    # "cv":Lcom/commonsware/cwac/cam2/CameraView;
    .restart local v3    # "previewSize":Lcom/commonsware/cwac/cam2/util/Size;
    :cond_3
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/util/Utils;->getSmallestPictureSize(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v2

    .restart local v2    # "pictureSize":Lcom/commonsware/cwac/cam2/util/Size;
    goto/16 :goto_0
.end method


# virtual methods
.method public canToggleFlashMode()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->allowChangeFlashMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    .line 239
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraEngine;->supportsDynamicFlashModes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    .line 240
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraEngine;->hasMoreThanOneEligibleFlashMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method changeZoom(I)Z
    .locals 1
    .param p1, "delta"    # I

    .prologue
    .line 252
    iget v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    .line 254
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->handleZoom()Z

    move-result v0

    return v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 142
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraController$ControllerDestroyedEvent;

    invoke-direct {v1, p0}, Lcom/commonsware/cwac/cam2/CameraController$ControllerDestroyedEvent;-><init>(Lcom/commonsware/cwac/cam2/CameraController;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 143
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 144
    return-void
.end method

.method public getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v0

    return-object v0
.end method

.method public getEngine()Lcom/commonsware/cwac/cam2/CameraEngine;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    return-object v0
.end method

.method public getNumberOfCameras()I
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public onDestroyed(Lcom/commonsware/cwac/cam2/CameraView;)V
    .locals 0
    .param p1, "cv"    # Lcom/commonsware/cwac/cam2/CameraView;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraController;->stop()V

    .line 201
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;

    .prologue
    .line 342
    iget-object v0, p1, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;->exception:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 343
    const/16 v0, 0xda2

    iget-object v1, p1, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;->exception:Ljava/lang/Exception;

    invoke-virtual {p0, v0, v1}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 346
    :cond_0
    iget-object v0, p1, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;->descriptors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 347
    iget-object v0, p1, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;->descriptors:Ljava/util/List;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    .line 348
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;-><init>(Lcom/commonsware/cwac/cam2/CameraController;ILcom/commonsware/cwac/cam2/CameraController$1;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 352
    :goto_0
    return-void

    .line 350
    :cond_1
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;

    .prologue
    const/4 v2, 0x0

    .line 382
    iget-object v0, p1, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;->exception:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 383
    const/16 v0, 0xda4

    iget-object v1, p1, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;->exception:Ljava/lang/Exception;

    invoke-virtual {p0, v0, v1}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 384
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraController$NoSuchCameraEvent;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->switchPending:Z

    if-eqz v0, :cond_0

    .line 387
    iput-boolean v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->switchPending:Z

    .line 388
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->getNextCameraIndex()I

    move-result v0

    iput v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    .line 389
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    iget v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraController;->getPreview(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraView;

    move-result-object v0

    .line 390
    invoke-virtual {v0, v2}, Lcom/commonsware/cwac/cam2/CameraView;->setVisibility(I)V

    .line 391
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->open()V

    goto :goto_0
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;

    .prologue
    .line 405
    const/16 v0, 0xda9

    iget-object v1, p1, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;->exception:Ljava/lang/Exception;

    invoke-virtual {p0, v0, v1}, Lcom/commonsware/cwac/cam2/CameraController;->postError(ILjava/lang/Exception;)V

    .line 406
    return-void
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    .prologue
    const/4 v2, 0x1

    .line 356
    iget-object v4, p1, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;->exception:Ljava/lang/Exception;

    if-eqz v4, :cond_0

    .line 378
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    iget v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .line 360
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraDescriptor;
    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraController;->getPreview(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraView;

    move-result-object v1

    .line 364
    .local v1, "cv":Lcom/commonsware/cwac/cam2/CameraView;
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 365
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 366
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v2, :cond_2

    .line 368
    .local v2, "shouldSwapPreviewDimensions":Z
    :goto_1
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/CameraSession;->getPreviewSize()Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v3

    .line 370
    .local v3, "virtualPreviewSize":Lcom/commonsware/cwac/cam2/util/Size;
    if-eqz v2, :cond_1

    .line 371
    new-instance v3, Lcom/commonsware/cwac/cam2/util/Size;

    .end local v3    # "virtualPreviewSize":Lcom/commonsware/cwac/cam2/util/Size;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 372
    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/CameraSession;->getPreviewSize()Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v4

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 373
    invoke-virtual {v5}, Lcom/commonsware/cwac/cam2/CameraSession;->getPreviewSize()Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v5

    invoke-virtual {v5}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/commonsware/cwac/cam2/util/Size;-><init>(II)V

    .line 376
    .restart local v3    # "virtualPreviewSize":Lcom/commonsware/cwac/cam2/util/Size;
    :cond_1
    invoke-virtual {v1, v3}, Lcom/commonsware/cwac/cam2/CameraView;->setPreviewSize(Lcom/commonsware/cwac/cam2/util/Size;)V

    goto :goto_0

    .line 366
    .end local v2    # "shouldSwapPreviewDimensions":Z
    .end local v3    # "virtualPreviewSize":Lcom/commonsware/cwac/cam2/util/Size;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0, v1, p1}, Lcom/commonsware/cwac/cam2/CameraEngine;->handleOrientationChange(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;)V

    .line 401
    :cond_0
    return-void
.end method

.method public onReady(Lcom/commonsware/cwac/cam2/CameraView;)V
    .locals 1
    .param p1, "cv"    # Lcom/commonsware/cwac/cam2/CameraView;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->open()V

    .line 189
    :cond_0
    return-void
.end method

.method postError(ILjava/lang/Exception;)V
    .locals 4
    .param p1, "resultCode"    # I
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 409
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->onError:Landroid/os/ResultReceiver;

    if-eqz v2, :cond_0

    .line 410
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 411
    .local v0, "resultData":Landroid/os/Bundle;
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 413
    .local v1, "sw":Ljava/io/StringWriter;
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p2, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 414
    const-string v2, "stackTrace"

    .line 416
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    .line 415
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->onError:Landroid/os/ResultReceiver;

    invoke-virtual {v2, p1, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 421
    .end local v0    # "resultData":Landroid/os/Bundle;
    .end local v1    # "sw":Ljava/io/StringWriter;
    :goto_0
    return-void

    .line 419
    :cond_0
    const-string v2, "201604"

    const-string v3, "no onError"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public recordVideo(Lcom/commonsware/cwac/cam2/VideoTransaction;)V
    .locals 2
    .param p1, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0, v1, p1}, Lcom/commonsware/cwac/cam2/CameraEngine;->recordVideo(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/VideoTransaction;)V

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideoRecording:Z

    .line 221
    :cond_0
    return-void
.end method

.method public setCameraViews(Ljava/util/Queue;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "cameraViews":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/commonsware/cwac/cam2/CameraView;>;"
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraController;->availablePreviews:Ljava/util/Queue;

    .line 168
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->previews:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 170
    invoke-interface {p1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraView;

    .line 171
    .local v0, "cv":Lcom/commonsware/cwac/cam2/CameraView;
    invoke-virtual {v0, p0}, Lcom/commonsware/cwac/cam2/CameraView;->setStateCallback(Lcom/commonsware/cwac/cam2/CameraView$StateCallback;)V

    goto :goto_0

    .line 174
    .end local v0    # "cv":Lcom/commonsware/cwac/cam2/CameraView;
    :cond_0
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->open()V

    .line 175
    return-void
.end method

.method public setEngine(Lcom/commonsware/cwac/cam2/CameraEngine;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
    .locals 1
    .param p1, "engine"    # Lcom/commonsware/cwac/cam2/CameraEngine;
    .param p2, "criteria"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    .line 92
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 94
    invoke-virtual {p1, p2}, Lcom/commonsware/cwac/cam2/CameraEngine;->loadCameraDescriptors(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V

    .line 95
    return-void
.end method

.method public setQuality(I)V
    .locals 0
    .param p1, "quality"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/commonsware/cwac/cam2/CameraController;->quality:I

    .line 235
    return-void
.end method

.method setZoom(I)Z
    .locals 1
    .param p1, "zoomLevel"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/commonsware/cwac/cam2/CameraController;->zoomLevel:I

    .line 260
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->handleZoom()Z

    move-result v0

    return v0
.end method

.method public start()V
    .locals 4

    .prologue
    .line 108
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->cameras:Ljava/util/List;

    iget v3, p0, Lcom/commonsware/cwac/cam2/CameraController;->currentCamera:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .line 110
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraDescriptor;
    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraController;->getPreview(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraView;

    move-result-object v1

    .line 112
    .local v1, "cv":Lcom/commonsware/cwac/cam2/CameraView;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraView;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraController;->open()V

    .line 116
    .end local v0    # "camera":Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .end local v1    # "cv":Lcom/commonsware/cwac/cam2/CameraView;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    if-eqz v1, :cond_0

    .line 125
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/commonsware/cwac/cam2/CameraController;->stopVideoRecording(Z)V

    .line 127
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 129
    .local v0, "temp":Lcom/commonsware/cwac/cam2/CameraSession;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 130
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    invoke-virtual {v1, v0}, Lcom/commonsware/cwac/cam2/CameraEngine;->close(Lcom/commonsware/cwac/cam2/CameraSession;)V

    .line 133
    .end local v0    # "temp":Lcom/commonsware/cwac/cam2/CameraSession;
    :cond_0
    return-void
.end method

.method public stopVideoRecording(Z)V
    .locals 3
    .param p1, "abandon"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 224
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideoRecording:Z

    if-eqz v0, :cond_0

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0, v1, p1}, Lcom/commonsware/cwac/cam2/CameraEngine;->stopVideoRecording(Lcom/commonsware/cwac/cam2/CameraSession;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    iput-boolean v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideoRecording:Z

    .line 231
    :cond_0
    return-void

    .line 228
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/commonsware/cwac/cam2/CameraController;->isVideoRecording:Z

    throw v0
.end method

.method supportsZoom()Z
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0, v1}, Lcom/commonsware/cwac/cam2/CameraEngine;->supportsZoom(Lcom/commonsware/cwac/cam2/CameraSession;)Z

    move-result v0

    return v0
.end method

.method public switchCamera()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraController;->getPreview(Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/commonsware/cwac/cam2/CameraView;->setVisibility(I)V

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->switchPending:Z

    .line 156
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraController;->stop()V

    .line 158
    :cond_0
    return-void
.end method

.method public takePicture(Lcom/commonsware/cwac/cam2/PictureTransaction;)V
    .locals 2
    .param p1, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController;->engine:Lcom/commonsware/cwac/cam2/CameraEngine;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraController;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v0, v1, p1}, Lcom/commonsware/cwac/cam2/CameraEngine;->takePicture(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V

    .line 214
    :cond_0
    return-void
.end method
