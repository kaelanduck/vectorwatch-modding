.class public Lcom/commonsware/cwac/cam2/CameraActivity;
.super Lcom/commonsware/cwac/cam2/AbstractCameraActivity;
.source "CameraActivity.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/ConfirmationFragment$Contract;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    }
.end annotation


# static fields
.field public static final EXTRA_CONFIRM:Ljava/lang/String; = "cwac_cam2_confirm"

.field public static final EXTRA_CONFIRMATION_QUALITY:Ljava/lang/String; = "cwac_cam2_confirmation_quality"

.field public static final EXTRA_DEBUG_SAVE_PREVIEW_FRAME:Ljava/lang/String; = "cwac_cam2_save_preview"

.field public static final EXTRA_ZOOM_STYLE:Ljava/lang/String; = "cwac_cam2_zoom_style"

.field private static final PERMS:[Ljava/lang/String;

.field private static final TAG_CONFIRM:Ljava/lang/String;


# instance fields
.field private confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

.field private needsThumbnail:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 72
    const-class v0, Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/commonsware/cwac/cam2/CameraActivity;->TAG_CONFIRM:Ljava/lang/String;

    .line 73
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    sput-object v0, Lcom/commonsware/cwac/cam2/CameraActivity;->PERMS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->needsThumbnail:Z

    .line 203
    return-void
.end method

.method private removeFragments()V
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    .line 193
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 194
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 196
    return-void
.end method


# virtual methods
.method protected buildFragment()Lcom/commonsware/cwac/cam2/CameraFragment;
    .locals 5

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getOutputUri()Landroid/net/Uri;

    move-result-object v1

    .line 185
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "cwac_cam2_update_media_store"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 186
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "android.intent.extra.videoQuality"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 187
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "cwac_cam2_zoom_style"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/ZoomStyle;

    .line 184
    invoke-static {v1, v2, v3, v0}, Lcom/commonsware/cwac/cam2/CameraFragment;->newPictureInstance(Landroid/net/Uri;ZILcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraFragment;

    move-result-object v0

    return-object v0
.end method

.method public completeRequest(Lcom/commonsware/cwac/cam2/ImageContext;Z)V
    .locals 1
    .param p1, "imageContext"    # Lcom/commonsware/cwac/cam2/ImageContext;
    .param p2, "isOK"    # Z

    .prologue
    .line 141
    if-nez p2, :cond_0

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/CameraActivity;->setResult(I)V

    .line 143
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->finish()V

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->retakePicture()V

    goto :goto_0
.end method

.method protected configEngine(Lcom/commonsware/cwac/cam2/CameraEngine;)V
    .locals 4
    .param p1, "engine"    # Lcom/commonsware/cwac/cam2/CameraEngine;

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cwac_cam2_save_preview"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "cam2-preview.jpg"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/commonsware/cwac/cam2/CameraEngine;->setDebugSavePreviewFile(Ljava/io/File;)V

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cwac_cam2_flash_modes"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 173
    .local v0, "flashModes":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/FlashMode;>;"
    if-nez v0, :cond_1

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "flashModes":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/FlashMode;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .restart local v0    # "flashModes":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/FlashMode;>;"
    :cond_1
    if-eqz v0, :cond_2

    .line 178
    invoke-virtual {p1, v0}, Lcom/commonsware/cwac/cam2/CameraEngine;->setPreferredFlashModes(Ljava/util/List;)V

    .line 180
    :cond_2
    return-void
.end method

.method protected getNeededPermissions()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/commonsware/cwac/cam2/CameraActivity;->PERMS:[Ljava/lang/String;

    return-object v0
.end method

.method protected init()V
    .locals 5

    .prologue
    .line 84
    invoke-super {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;->init()V

    .line 86
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/commonsware/cwac/cam2/CameraActivity;->TAG_CONFIRM:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    .line 88
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getOutputUri()Landroid/net/Uri;

    move-result-object v0

    .line 90
    .local v0, "output":Landroid/net/Uri;
    if-nez v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->needsThumbnail:Z

    .line 92
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    if-nez v1, :cond_0

    .line 93
    invoke-static {}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->newInstance()Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    .line 94
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    sget-object v4, Lcom/commonsware/cwac/cam2/CameraActivity;->TAG_CONFIRM:Ljava/lang/String;

    .line 96
    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 100
    :cond_0
    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->isVisible()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->isVisible()Z

    move-result v1

    if-nez v1, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    .line 103
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 104
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 105
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 107
    :cond_1
    return-void

    .line 90
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isVideo()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method protected needsActionBar()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x1

    return v0
.end method

.method protected needsOverlay()Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    .prologue
    const/4 v2, 0x1

    .line 111
    iget-object v0, p1, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->exception:Ljava/lang/Exception;

    if-nez v0, :cond_1

    .line 112
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "cwac_cam2_confirm"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->getImageContext()Lcom/commonsware/cwac/cam2/ImageContext;

    move-result-object v1

    .line 114
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "cwac_cam2_confirmation_quality"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 113
    invoke-virtual {v0, v1, v2}, Lcom/commonsware/cwac/cam2/ConfirmationFragment;->setImage(Lcom/commonsware/cwac/cam2/ImageContext;Ljava/lang/Float;)V

    .line 116
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 118
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    .line 119
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 127
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;->getImageContext()Lcom/commonsware/cwac/cam2/ImageContext;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/commonsware/cwac/cam2/CameraActivity;->completeRequest(Lcom/commonsware/cwac/cam2/ImageContext;Z)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->finish()V

    goto :goto_0
.end method

.method public retakePicture()V
    .locals 2

    .prologue
    .line 131
    sget-object v0, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getOutputUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/commonsware/cwac/cam2/CameraFragment;->updateUri(Landroid/net/Uri;)V

    .line 132
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraActivity;->confirmFrag:Lcom/commonsware/cwac/cam2/ConfirmationFragment;

    .line 134
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/CameraActivity;->cameraFrag:Lcom/commonsware/cwac/cam2/CameraFragment;

    .line 135
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 137
    return-void
.end method
