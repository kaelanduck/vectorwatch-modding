.class public Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
.super Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;
.source "CameraActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder",
        "<",
        "Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 213
    const-class v0, Lcom/commonsware/cwac/cam2/CameraActivity;

    invoke-direct {p0, p1, v0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 214
    return-void
.end method


# virtual methods
.method buildChooserBaseIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public confirmationQuality(F)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    .locals 2
    .param p1, "quality"    # F

    .prologue
    .line 262
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 263
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Quality outside (0.0f, 1.0f] range!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_confirmation_quality"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 268
    return-object p0
.end method

.method public debugSavePreviewFrame()Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_save_preview"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 236
    return-object p0
.end method

.method public skipConfirm()Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_confirm"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 230
    return-object p0
.end method

.method public zoomStyle(Lcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    .locals 2
    .param p1, "zoomStyle"    # Lcom/commonsware/cwac/cam2/ZoomStyle;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->result:Landroid/content/Intent;

    const-string v1, "cwac_cam2_zoom_style"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 248
    return-object p0
.end method
