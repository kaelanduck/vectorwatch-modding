.class Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;
.super Ljava/lang/Object;
.source "CameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CrashableEvent"
.end annotation


# instance fields
.field public final exception:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>(Ljava/lang/Exception;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    if-eqz p1, :cond_0

    .line 64
    const-string v0, "CWAC-Cam2"

    const-string v1, "Exception in camera processing"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 67
    :cond_0
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;->exception:Ljava/lang/Exception;

    .line 68
    return-void
.end method
