.class public Lcom/commonsware/cwac/cam2/VideoRecorderActivity;
.super Lcom/commonsware/cwac/cam2/AbstractCameraActivity;
.source "VideoRecorderActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/VideoRecorderActivity$IntentBuilder;
    }
.end annotation


# static fields
.field private static final PERMS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    sput-object v0, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->PERMS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/AbstractCameraActivity;-><init>()V

    .line 90
    return-void
.end method


# virtual methods
.method protected buildFragment()Lcom/commonsware/cwac/cam2/CameraFragment;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->getOutputUri()Landroid/net/Uri;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cwac_cam2_update_media_store"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 63
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extra.videoQuality"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 64
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extra.sizeLimit"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 65
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.intent.extra.durationLimit"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 61
    invoke-static {v0, v1, v2, v3, v4}, Lcom/commonsware/cwac/cam2/CameraFragment;->newVideoInstance(Landroid/net/Uri;ZIII)Lcom/commonsware/cwac/cam2/CameraFragment;

    move-result-object v0

    return-object v0
.end method

.method protected configEngine(Lcom/commonsware/cwac/cam2/CameraEngine;)V
    .locals 0
    .param p1, "engine"    # Lcom/commonsware/cwac/cam2/CameraEngine;

    .prologue
    .line 57
    return-void
.end method

.method protected getNeededPermissions()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->PERMS:[Ljava/lang/String;

    return-object v0
.end method

.method protected isVideo()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method protected needsActionBar()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method protected needsOverlay()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public onEventMainThread(Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;->getVideoTransaction()Lcom/commonsware/cwac/cam2/VideoTransaction;

    move-result-object v0

    if-nez v0, :cond_0

    .line 71
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->setResult(I)V

    .line 72
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->finish()V

    .line 83
    :goto_0
    return-void

    .line 75
    :cond_0
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$1;

    invoke-direct {v1, p0}, Lcom/commonsware/cwac/cam2/VideoRecorderActivity$1;-><init>(Lcom/commonsware/cwac/cam2/VideoRecorderActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
