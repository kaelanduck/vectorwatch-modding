.class public Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;
.super Ljava/lang/Object;
.source "CameraSelectionCriteria.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    invoke-direct {v0}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    return-void
.end method


# virtual methods
.method public build()Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    return-object v0
.end method

.method public facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;
    .locals 1
    .param p1, "facing"    # Lcom/commonsware/cwac/cam2/Facing;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    # setter for: Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facing:Lcom/commonsware/cwac/cam2/Facing;
    invoke-static {v0, p1}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->access$002(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/Facing;

    .line 63
    return-object p0
.end method

.method public facingExactMatch(Z)Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;
    .locals 1
    .param p1, "match"    # Z

    .prologue
    .line 74
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria$Builder;->criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    # setter for: Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->facingExactMatch:Z
    invoke-static {v0, p1}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->access$102(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;Z)Z

    .line 76
    return-object p0
.end method
