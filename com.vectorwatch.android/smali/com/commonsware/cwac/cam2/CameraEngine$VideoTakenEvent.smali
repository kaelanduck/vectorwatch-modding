.class public Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;
.super Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;
.source "CameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VideoTakenEvent"
.end annotation


# instance fields
.field private xact:Lcom/commonsware/cwac/cam2/VideoTransaction;


# direct methods
.method public constructor <init>(Lcom/commonsware/cwac/cam2/VideoTransaction;)V
    .locals 0
    .param p1, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>()V

    .line 189
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;->xact:Lcom/commonsware/cwac/cam2/VideoTransaction;

    .line 190
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>(Ljava/lang/Exception;)V

    .line 194
    return-void
.end method


# virtual methods
.method public getVideoTransaction()Lcom/commonsware/cwac/cam2/VideoTransaction;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;->xact:Lcom/commonsware/cwac/cam2/VideoTransaction;

    return-object v0
.end method
