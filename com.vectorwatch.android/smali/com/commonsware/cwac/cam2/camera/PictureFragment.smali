.class public Lcom/commonsware/cwac/cam2/camera/PictureFragment;
.super Landroid/preference/PreferenceFragment;
.source "PictureFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/camera/PictureFragment$Contract;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 168
    return-void
.end method

.method private takePicture()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 73
    .local v2, "prefs":Landroid/content/SharedPreferences;
    new-instance v0, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;-><init>(Landroid/content/Context;)V

    .line 75
    .local v0, "b":Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->skipConfirm()Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    .line 77
    const-string v7, "ffc"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 78
    sget-object v7, Lcom/commonsware/cwac/cam2/Facing;->FRONT:Lcom/commonsware/cwac/cam2/Facing;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 83
    :goto_0
    const-string v7, "exact_match"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->facingExactMatch()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 87
    :cond_0
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->updateMediaStore()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 89
    const-string v7, "forceClassic"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 90
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->forceClassic()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 93
    :cond_1
    const-string v7, "mirrorPreview"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 94
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->mirrorPreview()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 97
    :cond_2
    const-string v7, "highQuality"

    const/4 v8, 0x1

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 98
    sget-object v7, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->HIGH:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->quality(Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 103
    :goto_1
    const-string v7, "focusMode"

    const-string v8, "-1"

    .line 104
    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 106
    .local v4, "rawFocusMode":I
    packed-switch v4, :pswitch_data_0

    .line 118
    :goto_2
    const-string v7, "flashMode"

    const-string v8, "-1"

    .line 119
    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 121
    .local v3, "rawFlashMode":I
    packed-switch v3, :pswitch_data_1

    .line 136
    :goto_3
    const-string v7, "allowSwitchFlashMode"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 137
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->allowSwitchFlashMode()Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 140
    :cond_3
    const-string/jumbo v7, "zoomStyle"

    const-string v8, "0"

    .line 141
    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 143
    .local v5, "rawZoomStyle":I
    packed-switch v5, :pswitch_data_2

    .line 153
    :goto_4
    const-string v7, "confirmationQuality"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "confirmationQuality":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v7, "Default"

    .line 156
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 157
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->confirmationQuality(F)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    .line 160
    :cond_4
    new-instance v7, Lcom/commonsware/cwac/cam2/ErrorResultReceiver;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/commonsware/cwac/cam2/ErrorResultReceiver;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->onError(Landroid/os/ResultReceiver;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    .line 164
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->build()Landroid/content/Intent;

    move-result-object v6

    .line 165
    .local v6, "result":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/commonsware/cwac/cam2/camera/PictureFragment$Contract;

    invoke-interface {v7, v6}, Lcom/commonsware/cwac/cam2/camera/PictureFragment$Contract;->takePicture(Landroid/content/Intent;)V

    .line 166
    return-void

    .line 80
    .end local v1    # "confirmationQuality":Ljava/lang/String;
    .end local v3    # "rawFlashMode":I
    .end local v4    # "rawFocusMode":I
    .end local v5    # "rawZoomStyle":I
    .end local v6    # "result":Landroid/content/Intent;
    :cond_5
    sget-object v7, Lcom/commonsware/cwac/cam2/Facing;->BACK:Lcom/commonsware/cwac/cam2/Facing;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->facing(Lcom/commonsware/cwac/cam2/Facing;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto/16 :goto_0

    .line 100
    :cond_6
    sget-object v7, Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;->LOW:Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->quality(Lcom/commonsware/cwac/cam2/AbstractCameraActivity$Quality;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto/16 :goto_1

    .line 108
    .restart local v4    # "rawFocusMode":I
    :pswitch_0
    sget-object v7, Lcom/commonsware/cwac/cam2/FocusMode;->CONTINUOUS:Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_2

    .line 111
    :pswitch_1
    sget-object v7, Lcom/commonsware/cwac/cam2/FocusMode;->OFF:Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_2

    .line 114
    :pswitch_2
    sget-object v7, Lcom/commonsware/cwac/cam2/FocusMode;->EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->focusMode(Lcom/commonsware/cwac/cam2/FocusMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto/16 :goto_2

    .line 123
    .restart local v3    # "rawFlashMode":I
    :pswitch_3
    sget-object v7, Lcom/commonsware/cwac/cam2/FlashMode;->OFF:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_3

    .line 126
    :pswitch_4
    sget-object v7, Lcom/commonsware/cwac/cam2/FlashMode;->ALWAYS:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto :goto_3

    .line 129
    :pswitch_5
    sget-object v7, Lcom/commonsware/cwac/cam2/FlashMode;->AUTO:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto/16 :goto_3

    .line 132
    :pswitch_6
    sget-object v7, Lcom/commonsware/cwac/cam2/FlashMode;->REDEYE:Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->flashMode(Lcom/commonsware/cwac/cam2/FlashMode;)Lcom/commonsware/cwac/cam2/AbstractCameraActivity$IntentBuilder;

    goto/16 :goto_3

    .line 145
    .restart local v5    # "rawZoomStyle":I
    :pswitch_7
    sget-object v7, Lcom/commonsware/cwac/cam2/ZoomStyle;->PINCH:Lcom/commonsware/cwac/cam2/ZoomStyle;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->zoomStyle(Lcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    goto :goto_4

    .line 149
    :pswitch_8
    sget-object v7, Lcom/commonsware/cwac/cam2/ZoomStyle;->SEEKBAR:Lcom/commonsware/cwac/cam2/ZoomStyle;

    invoke-virtual {v0, v7}, Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;->zoomStyle(Lcom/commonsware/cwac/cam2/ZoomStyle;)Lcom/commonsware/cwac/cam2/CameraActivity$IntentBuilder;

    goto :goto_4

    .line 106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 121
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 143
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 49
    instance-of v0, p1, Lcom/commonsware/cwac/cam2/camera/PictureFragment$Contract;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Hosting activity does not implement Contract interface!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    sget v0, Lcom/commonsware/cwac/cam2/R$xml;->prefs_picture:I

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->addPreferencesFromResource(I)V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->setHasOptionsMenu(Z)V

    .line 43
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 56
    sget v0, Lcom/commonsware/cwac/cam2/R$menu;->actions:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 57
    sget v0, Lcom/commonsware/cwac/cam2/R$id;->video_activity:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 58
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 176
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroyView()V

    .line 177
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->takePicture()V

    .line 178
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 62
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/commonsware/cwac/cam2/R$id;->take_picture:I

    if-ne v0, v1, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->takePicture()V

    .line 65
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
