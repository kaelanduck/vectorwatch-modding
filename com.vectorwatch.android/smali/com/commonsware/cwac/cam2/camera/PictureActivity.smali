.class public Lcom/commonsware/cwac/cam2/camera/PictureActivity;
.super Landroid/app/Activity;
.source "PictureActivity.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/camera/PictureFragment$Contract;


# static fields
.field private static final REQUEST_CAMERA:I = 0x539

.field private static final STATE_OUTPUT:Ljava/lang/String; = "com.commonsware.cwac.cam2.playground.PictureActivity.STATE_OUTPUT"

.field private static final TAG_PLAYGROUND:Ljava/lang/String;

.field private static final TAG_RESULT:Ljava/lang/String;


# instance fields
.field private output:Landroid/net/Uri;

.field private playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

.field private result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->TAG_PLAYGROUND:Ljava/lang/String;

    .line 29
    const-class v0, Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->TAG_RESULT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    .line 33
    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    .line 34
    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->output:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 93
    const/16 v1, 0x539

    if-ne p1, v1, :cond_0

    .line 94
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 95
    const-string v1, "data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 97
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 98
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->output:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->setImage(Landroid/net/Uri;)V

    .line 103
    :goto_0
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    .line 105
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    .line 106
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    const/4 v2, 0x0

    .line 107
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 108
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 111
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 100
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    invoke-virtual {v1, v0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x1020002

    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    if-eqz p1, :cond_0

    .line 41
    const-string v0, "com.commonsware.cwac.cam2.playground.PictureActivity.STATE_OUTPUT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->output:Landroid/net/Uri;

    .line 44
    :cond_0
    const-string v0, "mounted"

    .line 45
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    const-string v0, "Cannot access external storage!"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 47
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->finish()V

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->TAG_PLAYGROUND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    .line 51
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->TAG_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    .line 53
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    if-nez v0, :cond_2

    .line 54
    new-instance v0, Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    invoke-direct {v0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    .line 55
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    sget-object v2, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->TAG_PLAYGROUND:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    if-nez v0, :cond_3

    .line 62
    invoke-static {}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->newInstance()Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    .line 63
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    sget-object v2, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->TAG_RESULT:Ljava/lang/String;

    .line 65
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    .line 66
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/camera/PictureFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_4

    .line 71
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->result:Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    .line 73
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->playground:Lcom/commonsware/cwac/cam2/camera/PictureFragment;

    .line 74
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 77
    :cond_4
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 83
    const-string v0, "com.commonsware.cwac.cam2.playground.PictureActivity.STATE_OUTPUT"

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->output:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    return-void
.end method

.method public setOutput(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->output:Landroid/net/Uri;

    .line 116
    return-void
.end method

.method public takePicture(Landroid/content/Intent;)V
    .locals 1
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 88
    const/16 v0, 0x539

    invoke-virtual {p0, p1, v0}, Lcom/commonsware/cwac/cam2/camera/PictureActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 89
    return-void
.end method
