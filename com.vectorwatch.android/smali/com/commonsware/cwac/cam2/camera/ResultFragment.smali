.class public Lcom/commonsware/cwac/cam2/camera/ResultFragment;
.super Landroid/app/Fragment;
.source "ResultFragment.java"


# static fields
.field private static final ARG_BITMAP:Ljava/lang/String; = "bitmap"

.field private static final ARG_URI:Ljava/lang/String; = "uri"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method private getSSSIV()Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;

    return-object v0
.end method

.method static newInstance()Lcom/commonsware/cwac/cam2/camera/ResultFragment;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/commonsware/cwac/cam2/camera/ResultFragment;

    invoke-direct {v0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;-><init>()V

    .line 36
    .local v0, "f":Lcom/commonsware/cwac/cam2/camera/ResultFragment;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 38
    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 51
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getSSSIV()Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;->setOrientation(I)V

    .line 53
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "bitmap"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 55
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "uri"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 58
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {p0, v1}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->setImage(Landroid/net/Uri;)V

    .line 64
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method setImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "bitmap"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 68
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getSSSIV()Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;

    move-result-object v0

    invoke-static {p1}, Lcom/davemorrissey/labs/subscaleview/ImageSource;->bitmap(Landroid/graphics/Bitmap;)Lcom/davemorrissey/labs/subscaleview/ImageSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;->setImage(Lcom/davemorrissey/labs/subscaleview/ImageSource;)V

    .line 73
    :cond_0
    return-void
.end method

.method setImage(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 77
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "bitmap"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/camera/ResultFragment;->getSSSIV()Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;

    move-result-object v0

    invoke-static {p1}, Lcom/davemorrissey/labs/subscaleview/ImageSource;->uri(Landroid/net/Uri;)Lcom/davemorrissey/labs/subscaleview/ImageSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/davemorrissey/labs/subscaleview/SubsamplingScaleImageView;->setImage(Lcom/davemorrissey/labs/subscaleview/ImageSource;)V

    .line 82
    :cond_0
    return-void
.end method
