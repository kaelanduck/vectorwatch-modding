.class public Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;
.super Ljava/lang/Object;
.source "CameraController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ControllerReadyEvent"
.end annotation


# instance fields
.field private final cameraCount:I

.field private final ctlr:Lcom/commonsware/cwac/cam2/CameraController;


# direct methods
.method private constructor <init>(Lcom/commonsware/cwac/cam2/CameraController;I)V
    .locals 0
    .param p1, "ctlr"    # Lcom/commonsware/cwac/cam2/CameraController;
    .param p2, "cameraCount"    # I

    .prologue
    .line 443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    iput p2, p0, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;->cameraCount:I

    .line 445
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    .line 446
    return-void
.end method

.method synthetic constructor <init>(Lcom/commonsware/cwac/cam2/CameraController;ILcom/commonsware/cwac/cam2/CameraController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/commonsware/cwac/cam2/CameraController;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/commonsware/cwac/cam2/CameraController$1;

    .prologue
    .line 439
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;-><init>(Lcom/commonsware/cwac/cam2/CameraController;I)V

    return-void
.end method


# virtual methods
.method public getNumberOfCameras()I
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;->cameraCount:I

    return v0
.end method

.method public isEventForController(Lcom/commonsware/cwac/cam2/CameraController;)Z
    .locals 1
    .param p1, "ctlr"    # Lcom/commonsware/cwac/cam2/CameraController;

    .prologue
    .line 453
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraController$ControllerReadyEvent;->ctlr:Lcom/commonsware/cwac/cam2/CameraController;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
