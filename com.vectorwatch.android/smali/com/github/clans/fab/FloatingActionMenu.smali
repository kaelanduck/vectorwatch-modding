.class public Lcom/github/clans/fab/FloatingActionMenu;
.super Landroid/view/ViewGroup;
.source "FloatingActionMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION:I = 0x12c

.field private static final CLOSED_PLUS_ROTATION:F = 0.0f

.field private static final LABELS_POSITION_LEFT:I = 0x0

.field private static final LABELS_POSITION_RIGHT:I = 0x1

.field private static final OPENED_PLUS_ROTATION_LEFT:F = -135.0f

.field private static final OPENED_PLUS_ROTATION_RIGHT:F = 135.0f

.field private static final OPEN_DOWN:I = 0x1

.field private static final OPEN_UP:I


# instance fields
.field private mAnimationDelayPerItem:I

.field private mBackgroundColor:I

.field private mButtonSpacing:I

.field private mButtonsCount:I

.field private mCloseAnimatorSet:Landroid/animation/AnimatorSet;

.field private mCloseInterpolator:Landroid/view/animation/Interpolator;

.field mGestureDetector:Landroid/view/GestureDetector;

.field private mHideBackgroundAnimator:Landroid/animation/ValueAnimator;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIconAnimated:Z

.field private mIconToggleSet:Landroid/animation/AnimatorSet;

.field private mImageToggle:Landroid/widget/ImageView;

.field private mIsAnimated:Z

.field private mIsMenuButtonAnimationRunning:Z

.field private mIsMenuOpening:Z

.field private mIsSetClosedOnTouchOutside:Z

.field private mLabelsColorNormal:I

.field private mLabelsColorPressed:I

.field private mLabelsColorRipple:I

.field private mLabelsContext:Landroid/content/Context;

.field private mLabelsCornerRadius:I

.field private mLabelsEllipsize:I

.field private mLabelsHideAnimation:I

.field private mLabelsMargin:I

.field private mLabelsMaxLines:I

.field private mLabelsPaddingBottom:I

.field private mLabelsPaddingLeft:I

.field private mLabelsPaddingRight:I

.field private mLabelsPaddingTop:I

.field private mLabelsPosition:I

.field private mLabelsShowAnimation:I

.field private mLabelsShowShadow:Z

.field private mLabelsSingleLine:Z

.field private mLabelsStyle:I

.field private mLabelsTextColor:Landroid/content/res/ColorStateList;

.field private mLabelsTextSize:F

.field private mLabelsVerticalOffset:I

.field private mMaxButtonWidth:I

.field private mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

.field private mMenuButtonHideAnimation:Landroid/view/animation/Animation;

.field private mMenuButtonShowAnimation:Landroid/view/animation/Animation;

.field private mMenuColorNormal:I

.field private mMenuColorPressed:I

.field private mMenuColorRipple:I

.field private mMenuFabSize:I

.field private mMenuLabelText:Ljava/lang/String;

.field private mMenuOpened:Z

.field private mMenuShadowColor:I

.field private mMenuShadowRadius:F

.field private mMenuShadowXOffset:F

.field private mMenuShadowYOffset:F

.field private mMenuShowShadow:Z

.field private mOpenAnimatorSet:Landroid/animation/AnimatorSet;

.field private mOpenDirection:I

.field private mOpenInterpolator:Landroid/view/animation/Interpolator;

.field private mShowBackgroundAnimator:Landroid/animation/ValueAnimator;

.field private mToggleListener:Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;

.field private mUiHandler:Landroid/os/Handler;

.field private mUsingMenuLabel:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/github/clans/fab/FloatingActionMenu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/github/clans/fab/FloatingActionMenu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x41000000    # 8.0f

    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v2, 0x40800000    # 4.0f

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    .line 44
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    .line 47
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    .line 50
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMargin:I

    .line 51
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsVerticalOffset:I

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    .line 58
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingTop:I

    .line 59
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingRight:I

    .line 60
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingBottom:I

    .line 61
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingLeft:I

    .line 64
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsCornerRadius:I

    .line 71
    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowRadius:F

    .line 72
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowXOffset:F

    .line 73
    iput v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowYOffset:F

    .line 81
    iput-boolean v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsAnimated:Z

    .line 87
    iput-boolean v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconAnimated:Z

    .line 586
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/github/clans/fab/FloatingActionMenu$4;

    invoke-direct {v2, p0}, Lcom/github/clans/fab/FloatingActionMenu$4;-><init>(Lcom/github/clans/fab/FloatingActionMenu;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mGestureDetector:Landroid/view/GestureDetector;

    .line 119
    invoke-direct {p0, p1, p2}, Lcom/github/clans/fab/FloatingActionMenu;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/github/clans/fab/FloatingActionMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsAnimated:Z

    return v0
.end method

.method static synthetic access$100(Lcom/github/clans/fab/FloatingActionMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsSetClosedOnTouchOutside:Z

    return v0
.end method

.method static synthetic access$200(Lcom/github/clans/fab/FloatingActionMenu;)Lcom/github/clans/fab/FloatingActionButton;
    .locals 1
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    return-object v0
.end method

.method static synthetic access$302(Lcom/github/clans/fab/FloatingActionMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuOpened:Z

    return p1
.end method

.method static synthetic access$400(Lcom/github/clans/fab/FloatingActionMenu;)Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;
    .locals 1
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mToggleListener:Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/github/clans/fab/FloatingActionMenu;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonHideAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$602(Lcom/github/clans/fab/FloatingActionMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    return p1
.end method

.method static synthetic access$700(Lcom/github/clans/fab/FloatingActionMenu;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/github/clans/fab/FloatingActionMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->hideMenuButtonWithImage(Z)V

    return-void
.end method

.method private addLabel(Lcom/github/clans/fab/FloatingActionButton;)V
    .locals 8
    .param p1, "fab"    # Lcom/github/clans/fab/FloatingActionButton;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 465
    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getLabelText()Ljava/lang/String;

    move-result-object v2

    .line 467
    .local v2, "text":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 516
    :goto_0
    return-void

    .line 469
    :cond_0
    new-instance v0, Lcom/github/clans/fab/Label;

    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/github/clans/fab/Label;-><init>(Landroid/content/Context;)V

    .line 470
    .local v0, "label":Lcom/github/clans/fab/Label;
    invoke-virtual {v0, v6}, Lcom/github/clans/fab/Label;->setClickable(Z)V

    .line 471
    invoke-virtual {v0, p1}, Lcom/github/clans/fab/Label;->setFab(Lcom/github/clans/fab/FloatingActionButton;)V

    .line 472
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsShowAnimation:I

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setShowAnimation(Landroid/view/animation/Animation;)V

    .line 473
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsHideAnimation:I

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setHideAnimation(Landroid/view/animation/Animation;)V

    .line 475
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsStyle:I

    if-lez v4, :cond_2

    .line 476
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsStyle:I

    invoke-virtual {v0, v4, v5}, Lcom/github/clans/fab/Label;->setTextAppearance(Landroid/content/Context;I)V

    .line 477
    invoke-virtual {v0, v7}, Lcom/github/clans/fab/Label;->setShowShadow(Z)V

    .line 478
    invoke-virtual {v0, v6}, Lcom/github/clans/fab/Label;->setUsingStyle(Z)V

    .line 511
    :cond_1
    :goto_1
    invoke-virtual {v0, v2}, Lcom/github/clans/fab/Label;->setText(Ljava/lang/CharSequence;)V

    .line 512
    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->addView(Landroid/view/View;)V

    .line 515
    sget v4, Lcom/github/clans/fab/R$id;->fab_label:I

    invoke-virtual {p1, v4, v0}, Lcom/github/clans/fab/FloatingActionButton;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 480
    :cond_2
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsColorNormal:I

    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsColorPressed:I

    iget v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsColorRipple:I

    invoke-virtual {v0, v4, v5, v6}, Lcom/github/clans/fab/Label;->setColors(III)V

    .line 481
    iget-boolean v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsShowShadow:Z

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setShowShadow(Z)V

    .line 482
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsCornerRadius:I

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setCornerRadius(I)V

    .line 483
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsEllipsize:I

    if-lez v4, :cond_3

    .line 484
    invoke-direct {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->setLabelEllipsize(Lcom/github/clans/fab/Label;)V

    .line 486
    :cond_3
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMaxLines:I

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setMaxLines(I)V

    .line 487
    invoke-virtual {v0}, Lcom/github/clans/fab/Label;->updateBackground()V

    .line 489
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsTextSize:F

    invoke-virtual {v0, v7, v4}, Lcom/github/clans/fab/Label;->setTextSize(IF)V

    .line 490
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 492
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingLeft:I

    .line 493
    .local v1, "left":I
    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingTop:I

    .line 494
    .local v3, "top":I
    iget-boolean v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsShowShadow:Z

    if-eqz v4, :cond_4

    .line 495
    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getShadowRadius()I

    move-result v4

    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getShadowXOffset()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 496
    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getShadowRadius()I

    move-result v4

    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getShadowYOffset()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 499
    :cond_4
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingLeft:I

    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingTop:I

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/github/clans/fab/Label;->setPadding(IIII)V

    .line 506
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMaxLines:I

    if-ltz v4, :cond_5

    iget-boolean v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsSingleLine:Z

    if-eqz v4, :cond_1

    .line 507
    :cond_5
    iget-boolean v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsSingleLine:Z

    invoke-virtual {v0, v4}, Lcom/github/clans/fab/Label;->setSingleLine(Z)V

    goto :goto_1
.end method

.method private adjustForOvershoot(I)I
    .locals 4
    .param p1, "dimension"    # I

    .prologue
    .line 430
    int-to-double v0, p1

    const-wide v2, 0x3f9eb851eb851eb8L    # 0.03

    mul-double/2addr v0, v2

    int-to-double v2, p1

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private createDefaultIconAnimation()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/high16 v5, 0x43070000    # 135.0f

    const/high16 v4, -0x3cf90000    # -135.0f

    .line 263
    iget v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenDirection:I

    if-nez v6, :cond_2

    .line 264
    iget v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    if-nez v6, :cond_0

    move v0, v4

    .line 265
    .local v0, "collapseAngle":F
    :goto_0
    iget v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    if-nez v6, :cond_1

    move v2, v4

    .line 271
    .local v2, "expandAngle":F
    :goto_1
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    const-string v5, "rotation"

    const/4 v6, 0x2

    new-array v6, v6, [F

    aput v0, v6, v8

    aput v7, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 278
    .local v1, "collapseAnimator":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    const-string v5, "rotation"

    const/4 v6, 0x2

    new-array v6, v6, [F

    aput v7, v6, v8

    aput v2, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 285
    .local v3, "expandAnimator":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 286
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 288
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 289
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 291
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 292
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 293
    return-void

    .end local v0    # "collapseAngle":F
    .end local v1    # "collapseAnimator":Landroid/animation/ObjectAnimator;
    .end local v2    # "expandAngle":F
    .end local v3    # "expandAnimator":Landroid/animation/ObjectAnimator;
    :cond_0
    move v0, v5

    .line 264
    goto :goto_0

    .restart local v0    # "collapseAngle":F
    :cond_1
    move v2, v5

    .line 265
    goto :goto_1

    .line 267
    .end local v0    # "collapseAngle":F
    :cond_2
    iget v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    if-nez v6, :cond_3

    move v0, v5

    .line 268
    .restart local v0    # "collapseAngle":F
    :goto_2
    iget v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    if-nez v6, :cond_4

    move v2, v5

    .restart local v2    # "expandAngle":F
    :goto_3
    goto :goto_1

    .end local v0    # "collapseAngle":F
    .end local v2    # "expandAngle":F
    :cond_3
    move v0, v4

    .line 267
    goto :goto_2

    .restart local v0    # "collapseAngle":F
    :cond_4
    move v2, v4

    .line 268
    goto :goto_3
.end method

.method private createLabels()V
    .locals 4

    .prologue
    .line 443
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    if-ge v1, v2, :cond_2

    .line 445
    invoke-virtual {p0, v1}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    if-ne v2, v3, :cond_1

    .line 443
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 447
    :cond_1
    invoke-virtual {p0, v1}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/github/clans/fab/FloatingActionButton;

    .line 449
    .local v0, "fab":Lcom/github/clans/fab/FloatingActionButton;
    sget v2, Lcom/github/clans/fab/R$id;->fab_label:I

    invoke-virtual {v0, v2}, Lcom/github/clans/fab/FloatingActionButton;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 451
    invoke-direct {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->addLabel(Lcom/github/clans/fab/FloatingActionButton;)V

    .line 453
    iget-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    if-ne v0, v2, :cond_0

    .line 454
    iget-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    new-instance v3, Lcom/github/clans/fab/FloatingActionMenu$3;

    invoke-direct {v3, p0}, Lcom/github/clans/fab/FloatingActionMenu$3;-><init>(Lcom/github/clans/fab/FloatingActionMenu;)V

    invoke-virtual {v2, v3}, Lcom/github/clans/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 462
    .end local v0    # "fab":Lcom/github/clans/fab/FloatingActionButton;
    :cond_2
    return-void
.end method

.method private createMenuButton()V
    .locals 4

    .prologue
    .line 237
    new-instance v0, Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    .line 239
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    iget-boolean v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShowShadow:Z

    iput-boolean v1, v0, Lcom/github/clans/fab/FloatingActionButton;->mShowShadow:Z

    .line 240
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShowShadow:Z

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowRadius:F

    invoke-static {v1, v2}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Lcom/github/clans/fab/FloatingActionButton;->mShadowRadius:I

    .line 242
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowXOffset:F

    invoke-static {v1, v2}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Lcom/github/clans/fab/FloatingActionButton;->mShadowXOffset:I

    .line 243
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowYOffset:F

    invoke-static {v1, v2}, Lcom/github/clans/fab/Util;->dpToPx(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Lcom/github/clans/fab/FloatingActionButton;->mShadowYOffset:I

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorNormal:I

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorPressed:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorRipple:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/github/clans/fab/FloatingActionButton;->setColors(III)V

    .line 246
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowColor:I

    iput v1, v0, Lcom/github/clans/fab/FloatingActionButton;->mShadowColor:I

    .line 247
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuFabSize:I

    iput v1, v0, Lcom/github/clans/fab/FloatingActionButton;->mFabSize:I

    .line 248
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0}, Lcom/github/clans/fab/FloatingActionButton;->updateBackground()V

    .line 249
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    iget-object v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuLabelText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->setLabelText(Ljava/lang/String;)V

    .line 251
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    .line 252
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 254
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-super {p0}, Landroid/view/ViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/github/clans/fab/FloatingActionMenu;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->addView(Landroid/view/View;)V

    .line 257
    invoke-direct {p0}, Lcom/github/clans/fab/FloatingActionMenu;->createDefaultIconAnimation()V

    .line 258
    return-void
.end method

.method private hideMenuButtonWithImage(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 557
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuButtonHidden()Z

    move-result v0

    if-nez v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->hide(Z)V

    .line 559
    if-eqz p1, :cond_0

    .line 560
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonHideAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 563
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    .line 565
    :cond_1
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 123
    sget-object v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu:[I

    invoke-virtual {p1, p2, v2, v5, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 124
    .local v0, "attr":Landroid/content/res/TypedArray;
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_buttonSpacing:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    .line 125
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_margin:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMargin:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMargin:I

    .line 126
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_position:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    .line 127
    sget v3, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_showAnimation:I

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    if-nez v2, :cond_4

    sget v2, Lcom/github/clans/fab/R$anim;->fab_slide_in_from_right:I

    :goto_0
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsShowAnimation:I

    .line 129
    sget v3, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_hideAnimation:I

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    if-nez v2, :cond_5

    sget v2, Lcom/github/clans/fab/R$anim;->fab_slide_out_to_right:I

    :goto_1
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsHideAnimation:I

    .line 131
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_paddingTop:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingTop:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingTop:I

    .line 132
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_paddingRight:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingRight:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingRight:I

    .line 133
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_paddingBottom:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingBottom:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingBottom:I

    .line 134
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_paddingLeft:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingLeft:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingLeft:I

    .line 135
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_textColor:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsTextColor:Landroid/content/res/ColorStateList;

    .line 137
    iget-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsTextColor:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_0

    .line 138
    invoke-static {v7}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsTextColor:Landroid/content/res/ColorStateList;

    .line 140
    :cond_0
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_textSize:I

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/github/clans/fab/R$dimen;->labels_text_size:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsTextSize:F

    .line 141
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_cornerRadius:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsCornerRadius:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsCornerRadius:I

    .line 142
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_showShadow:I

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsShowShadow:Z

    .line 143
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_colorNormal:I

    const v3, -0xcccccd

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsColorNormal:I

    .line 144
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_colorPressed:I

    const v3, -0xbbbbbc

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsColorPressed:I

    .line 145
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_colorRipple:I

    const v3, 0x66ffffff

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsColorRipple:I

    .line 146
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_showShadow:I

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShowShadow:Z

    .line 147
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_shadowColor:I

    const/high16 v3, 0x66000000

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowColor:I

    .line 148
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_shadowRadius:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowRadius:F

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowRadius:F

    .line 149
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_shadowXOffset:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowXOffset:F

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowXOffset:F

    .line 150
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_shadowYOffset:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowYOffset:F

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuShadowYOffset:F

    .line 151
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_colorNormal:I

    const v3, -0x25bcca

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorNormal:I

    .line 152
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_colorPressed:I

    const v3, -0x18afbd

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorPressed:I

    .line 153
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_colorRipple:I

    const v3, -0x66000001

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorRipple:I

    .line 154
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_animationDelayPerItem:I

    const/16 v3, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    .line 155
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_icon:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 156
    iget-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIcon:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_1

    .line 157
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/github/clans/fab/R$drawable;->fab_add:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 159
    :cond_1
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_singleLine:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsSingleLine:Z

    .line 160
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_ellipsize:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsEllipsize:I

    .line 161
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_maxLines:I

    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMaxLines:I

    .line 162
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_fab_size:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuFabSize:I

    .line 163
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_style:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsStyle:I

    .line 164
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_openDirection:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenDirection:I

    .line 165
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_backgroundColor:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mBackgroundColor:I

    .line 167
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_fab_label:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    iput-boolean v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUsingMenuLabel:Z

    .line 169
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_fab_label:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuLabelText:Ljava/lang/String;

    .line 172
    :cond_2
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 173
    sget v2, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_labels_padding:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 174
    .local v1, "padding":I
    invoke-direct {p0, v1}, Lcom/github/clans/fab/FloatingActionMenu;->initPadding(I)V

    .line 177
    .end local v1    # "padding":I
    :cond_3
    new-instance v2, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v2}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenInterpolator:Landroid/view/animation/Interpolator;

    .line 178
    new-instance v2, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseInterpolator:Landroid/view/animation/Interpolator;

    .line 179
    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsStyle:I

    invoke-direct {v2, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsContext:Landroid/content/Context;

    .line 181
    invoke-direct {p0}, Lcom/github/clans/fab/FloatingActionMenu;->initBackgroundDimAnimation()V

    .line 182
    invoke-direct {p0}, Lcom/github/clans/fab/FloatingActionMenu;->createMenuButton()V

    .line 183
    invoke-direct {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->initMenuButtonAnimations(Landroid/content/res/TypedArray;)V

    .line 185
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 186
    return-void

    .line 127
    :cond_4
    sget v2, Lcom/github/clans/fab/R$anim;->fab_slide_in_from_left:I

    goto/16 :goto_0

    .line 129
    :cond_5
    sget v2, Lcom/github/clans/fab/R$anim;->fab_slide_out_to_left:I

    goto/16 :goto_1
.end method

.method private initBackgroundDimAnimation()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x12c

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 199
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mBackgroundColor:I

    invoke-static {v4}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    .line 200
    .local v2, "maxAlpha":I
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mBackgroundColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 201
    .local v3, "red":I
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mBackgroundColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 202
    .local v1, "green":I
    iget v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mBackgroundColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 204
    .local v0, "blue":I
    new-array v4, v8, [I

    aput v6, v4, v6

    aput v2, v4, v7

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mShowBackgroundAnimator:Landroid/animation/ValueAnimator;

    .line 205
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mShowBackgroundAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 206
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mShowBackgroundAnimator:Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/github/clans/fab/FloatingActionMenu$1;

    invoke-direct {v5, p0, v3, v1, v0}, Lcom/github/clans/fab/FloatingActionMenu$1;-><init>(Lcom/github/clans/fab/FloatingActionMenu;III)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 214
    new-array v4, v8, [I

    aput v2, v4, v6

    aput v6, v4, v7

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mHideBackgroundAnimator:Landroid/animation/ValueAnimator;

    .line 215
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mHideBackgroundAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 216
    iget-object v4, p0, Lcom/github/clans/fab/FloatingActionMenu;->mHideBackgroundAnimator:Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/github/clans/fab/FloatingActionMenu$2;

    invoke-direct {v5, p0, v3, v1, v0}, Lcom/github/clans/fab/FloatingActionMenu$2;-><init>(Lcom/github/clans/fab/FloatingActionMenu;III)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 223
    return-void
.end method

.method private initMenuButtonAnimations(Landroid/content/res/TypedArray;)V
    .locals 6
    .param p1, "attr"    # Landroid/content/res/TypedArray;

    .prologue
    .line 189
    sget v4, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_fab_show_animation:I

    sget v5, Lcom/github/clans/fab/R$anim;->fab_scale_up:I

    invoke-virtual {p1, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 190
    .local v3, "showResId":I
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 191
    .local v2, "showAnimation":Landroid/view/animation/Animation;
    invoke-virtual {p0, v2}, Lcom/github/clans/fab/FloatingActionMenu;->setMenuButtonShowAnimation(Landroid/view/animation/Animation;)V

    .line 193
    sget v4, Lcom/github/clans/fab/R$styleable;->FloatingActionMenu_menu_fab_hide_animation:I

    sget v5, Lcom/github/clans/fab/R$anim;->fab_scale_down:I

    invoke-virtual {p1, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 194
    .local v1, "hideResId":I
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 195
    .local v0, "hideAnimation":Landroid/view/animation/Animation;
    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->setMenuButtonHideAnimation(Landroid/view/animation/Animation;)V

    .line 196
    return-void
.end method

.method private initPadding(I)V
    .locals 0
    .param p1, "padding"    # I

    .prologue
    .line 230
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingTop:I

    .line 231
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingRight:I

    .line 232
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingBottom:I

    .line 233
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPaddingLeft:I

    .line 234
    return-void
.end method

.method private isBackgroundEnabled()Z
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mBackgroundColor:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLabelEllipsize(Lcom/github/clans/fab/Label;)V
    .locals 1
    .param p1, "label"    # Lcom/github/clans/fab/Label;

    .prologue
    .line 519
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsEllipsize:I

    packed-switch v0, :pswitch_data_0

    .line 533
    :goto_0
    return-void

    .line 521
    :pswitch_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Lcom/github/clans/fab/Label;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 524
    :pswitch_1
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Lcom/github/clans/fab/Label;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 527
    :pswitch_2
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Lcom/github/clans/fab/Label;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 530
    :pswitch_3
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Lcom/github/clans/fab/Label;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 519
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showMenuButtonWithImage(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 568
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuButtonHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 569
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->show(Z)V

    .line 570
    if-eqz p1, :cond_0

    .line 571
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 575
    :cond_1
    return-void
.end method


# virtual methods
.method public addMenuButton(Lcom/github/clans/fab/FloatingActionButton;)V
    .locals 1
    .param p1, "fab"    # Lcom/github/clans/fab/FloatingActionButton;

    .prologue
    .line 952
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p0, p1, v0}, Lcom/github/clans/fab/FloatingActionMenu;->addView(Landroid/view/View;I)V

    .line 953
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    .line 954
    invoke-direct {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->addLabel(Lcom/github/clans/fab/FloatingActionButton;)V

    .line 955
    return-void
.end method

.method public addMenuButton(Lcom/github/clans/fab/FloatingActionButton;I)V
    .locals 2
    .param p1, "fab"    # Lcom/github/clans/fab/FloatingActionButton;
    .param p2, "index"    # I

    .prologue
    .line 964
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    add-int/lit8 v0, v1, -0x2

    .line 965
    .local v0, "size":I
    if-gez p2, :cond_1

    .line 966
    const/4 p2, 0x0

    .line 971
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/github/clans/fab/FloatingActionMenu;->addView(Landroid/view/View;I)V

    .line 972
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    .line 973
    invoke-direct {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->addLabel(Lcom/github/clans/fab/FloatingActionButton;)V

    .line 974
    return-void

    .line 967
    :cond_1
    if-le p2, v0, :cond_0

    .line 968
    move p2, v0

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 553
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    return v0
.end method

.method public close(Z)V
    .locals 10
    .param p1, "animate"    # Z

    .prologue
    .line 672
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isOpened()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 673
    invoke-direct {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isBackgroundEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 674
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mHideBackgroundAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 677
    :cond_0
    iget-boolean v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconAnimated:Z

    if-eqz v5, :cond_1

    .line 678
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconToggleSet:Landroid/animation/AnimatorSet;

    if-eqz v5, :cond_3

    .line 679
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconToggleSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 686
    :cond_1
    :goto_0
    const/4 v2, 0x0

    .line 687
    .local v2, "delay":I
    const/4 v1, 0x0

    .line 688
    .local v1, "counter":I
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuOpening:Z

    .line 689
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getChildCount()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 690
    invoke-virtual {p0, v4}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 691
    .local v0, "child":Landroid/view/View;
    instance-of v5, v0, Lcom/github/clans/fab/FloatingActionButton;

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_2

    .line 692
    add-int/lit8 v1, v1, 0x1

    move-object v3, v0

    .line 694
    check-cast v3, Lcom/github/clans/fab/FloatingActionButton;

    .line 695
    .local v3, "fab":Lcom/github/clans/fab/FloatingActionButton;
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    new-instance v6, Lcom/github/clans/fab/FloatingActionMenu$7;

    invoke-direct {v6, p0, v3, p1}, Lcom/github/clans/fab/FloatingActionMenu$7;-><init>(Lcom/github/clans/fab/FloatingActionMenu;Lcom/github/clans/fab/FloatingActionButton;Z)V

    int-to-long v8, v2

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 710
    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    add-int/2addr v2, v5

    .line 689
    .end local v3    # "fab":Lcom/github/clans/fab/FloatingActionButton;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 681
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "counter":I
    .end local v2    # "delay":I
    .end local v4    # "i":I
    :cond_3
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 682
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    .line 714
    .restart local v1    # "counter":I
    .restart local v2    # "delay":I
    .restart local v4    # "i":I
    :cond_4
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    new-instance v6, Lcom/github/clans/fab/FloatingActionMenu$8;

    invoke-direct {v6, p0}, Lcom/github/clans/fab/FloatingActionMenu$8;-><init>(Lcom/github/clans/fab/FloatingActionMenu;)V

    add-int/lit8 v1, v1, 0x1

    iget v7, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    mul-int/2addr v7, v1

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 725
    .end local v1    # "counter":I
    .end local v2    # "delay":I
    .end local v4    # "i":I
    :cond_5
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->generateDefaultLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 547
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 537
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 542
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getAnimationDelayPerItem()I
    .locals 1

    .prologue
    .line 765
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    return v0
.end method

.method public getIconToggleAnimatorSet()Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconToggleSet:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method public getMenuButtonColorNormal()I
    .locals 1

    .prologue
    .line 920
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorNormal:I

    return v0
.end method

.method public getMenuButtonColorPressed()I
    .locals 1

    .prologue
    .line 934
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorPressed:I

    return v0
.end method

.method public getMenuButtonColorRipple()I
    .locals 1

    .prologue
    .line 948
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorRipple:I

    return v0
.end method

.method public getMenuButtonLabelText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuLabelText:Ljava/lang/String;

    return-object v0
.end method

.method public getMenuIconView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    return-object v0
.end method

.method public hideMenu(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    if-nez v0, :cond_0

    .line 831
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    .line 832
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 833
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->close(Z)V

    .line 834
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/github/clans/fab/FloatingActionMenu$9;

    invoke-direct {v1, p0, p1}, Lcom/github/clans/fab/FloatingActionMenu$9;-><init>(Lcom/github/clans/fab/FloatingActionMenu;Z)V

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 845
    :cond_1
    if-eqz p1, :cond_2

    .line 846
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonHideAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->startAnimation(Landroid/view/animation/Animation;)V

    .line 848
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->setVisibility(I)V

    .line 849
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    goto :goto_0
.end method

.method public hideMenuButton(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    .line 881
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuButtonHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    if-nez v0, :cond_0

    .line 882
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuButtonAnimationRunning:Z

    .line 883
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 884
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->close(Z)V

    .line 885
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/github/clans/fab/FloatingActionMenu$10;

    invoke-direct {v1, p0, p1}, Lcom/github/clans/fab/FloatingActionMenu$10;-><init>(Lcom/github/clans/fab/FloatingActionMenu;Z)V

    iget v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 892
    :cond_1
    invoke-direct {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->hideMenuButtonWithImage(Z)V

    goto :goto_0
.end method

.method public isAnimated()Z
    .locals 1

    .prologue
    .line 757
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsAnimated:Z

    return v0
.end method

.method public isIconAnimated()Z
    .locals 1

    .prologue
    .line 777
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconAnimated:Z

    return v0
.end method

.method public isMenuButtonHidden()Z
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0}, Lcom/github/clans/fab/FloatingActionButton;->isHidden()Z

    move-result v0

    return v0
.end method

.method public isMenuHidden()Z
    .locals 2

    .prologue
    .line 803
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuOpened:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 435
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 436
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->bringChildToFront(Landroid/view/View;)V

    .line 437
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->bringChildToFront(Landroid/view/View;)V

    .line 438
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    .line 439
    invoke-direct {p0}, Lcom/github/clans/fab/FloatingActionMenu;->createLabels()V

    .line 440
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 27
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 350
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    move/from16 v24, v0

    if-nez v24, :cond_1

    sub-int v24, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    move/from16 v25, v0

    div-int/lit8 v25, v25, 0x2

    sub-int v24, v24, v25

    .line 351
    invoke-virtual/range {p0 .. p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingRight()I

    move-result v25

    sub-int v5, v24, v25

    .line 353
    .local v5, "buttonsHorizontalCenter":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenDirection:I

    move/from16 v24, v0

    if-nez v24, :cond_2

    const/16 v23, 0x1

    .line 355
    .local v23, "openUp":Z
    :goto_1
    if-eqz v23, :cond_3

    sub-int v24, p5, p3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v25, v0

    .line 356
    invoke-virtual/range {v25 .. v25}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v25

    sub-int v24, v24, v25

    invoke-virtual/range {p0 .. p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingBottom()I

    move-result v25

    sub-int v21, v24, v25

    .line 358
    .local v21, "menuButtonTop":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredWidth()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    sub-int v20, v5, v24

    .line 360
    .local v20, "menuButtonLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredWidth()I

    move-result v25

    add-int v25, v25, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v26, v0

    .line 361
    invoke-virtual/range {v26 .. v26}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v26

    add-int v26, v26, v21

    .line 360
    move-object/from16 v0, v24

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/clans/fab/FloatingActionButton;->layout(IIII)V

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    sub-int v11, v5, v24

    .line 364
    .local v11, "imageLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    add-int v24, v24, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    sub-int v12, v24, v25

    .line 366
    .local v12, "imageTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v25

    add-int v25, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    move-object/from16 v26, v0

    .line 367
    invoke-virtual/range {v26 .. v26}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v26

    add-int v26, v26, v12

    .line 366
    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v11, v12, v1, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 369
    if-eqz v23, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v24, v0

    .line 370
    invoke-virtual/range {v24 .. v24}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v24

    add-int v24, v24, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    move/from16 v25, v0

    add-int v22, v24, v25

    .line 373
    .local v22, "nextY":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    move/from16 v24, v0

    add-int/lit8 v10, v24, -0x1

    .local v10, "i":I
    :goto_4
    if-ltz v10, :cond_f

    .line 374
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 376
    .local v6, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    if-ne v6, v0, :cond_5

    .line 373
    :cond_0
    :goto_5
    add-int/lit8 v10, v10, -0x1

    goto :goto_4

    .line 351
    .end local v5    # "buttonsHorizontalCenter":I
    .end local v6    # "child":Landroid/view/View;
    .end local v10    # "i":I
    .end local v11    # "imageLeft":I
    .end local v12    # "imageTop":I
    .end local v20    # "menuButtonLeft":I
    .end local v21    # "menuButtonTop":I
    .end local v22    # "nextY":I
    .end local v23    # "openUp":Z
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    .line 352
    invoke-virtual/range {p0 .. p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingLeft()I

    move-result v25

    add-int v5, v24, v25

    goto/16 :goto_0

    .line 353
    .restart local v5    # "buttonsHorizontalCenter":I
    :cond_2
    const/16 v23, 0x0

    goto/16 :goto_1

    .line 357
    .restart local v23    # "openUp":Z
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingTop()I

    move-result v21

    goto/16 :goto_2

    .restart local v11    # "imageLeft":I
    .restart local v12    # "imageTop":I
    .restart local v20    # "menuButtonLeft":I
    .restart local v21    # "menuButtonTop":I
    :cond_4
    move/from16 v22, v21

    .line 370
    goto :goto_3

    .restart local v6    # "child":Landroid/view/View;
    .restart local v10    # "i":I
    .restart local v22    # "nextY":I
    :cond_5
    move-object v9, v6

    .line 378
    check-cast v9, Lcom/github/clans/fab/FloatingActionButton;

    .line 380
    .local v9, "fab":Lcom/github/clans/fab/FloatingActionButton;
    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getVisibility()I

    move-result v24

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_0

    .line 382
    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredWidth()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    sub-int v7, v5, v24

    .line 383
    .local v7, "childX":I
    if-eqz v23, :cond_8

    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v24

    sub-int v24, v22, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    move/from16 v25, v0

    sub-int v8, v24, v25

    .line 385
    .local v8, "childY":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    if-eq v9, v0, :cond_6

    .line 386
    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredWidth()I

    move-result v24

    add-int v24, v24, v7

    .line 387
    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v25

    add-int v25, v25, v8

    .line 386
    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v9, v7, v8, v0, v1}, Lcom/github/clans/fab/FloatingActionButton;->layout(IIII)V

    .line 389
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuOpening:Z

    move/from16 v24, v0

    if-nez v24, :cond_6

    .line 390
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Lcom/github/clans/fab/FloatingActionButton;->hide(Z)V

    .line 394
    :cond_6
    sget v24, Lcom/github/clans/fab/R$id;->fab_label:I

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Lcom/github/clans/fab/FloatingActionButton;->getTag(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/View;

    .line 395
    .local v13, "label":Landroid/view/View;
    if-eqz v13, :cond_7

    .line 396
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mUsingMenuLabel:Z

    move/from16 v24, v0

    if-eqz v24, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMargin:I

    move/from16 v25, v0

    add-int v19, v24, v25

    .line 397
    .local v19, "labelsOffset":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    move/from16 v24, v0

    if-nez v24, :cond_a

    sub-int v18, v5, v19

    .line 401
    .local v18, "labelXNearButton":I
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    move/from16 v24, v0

    if-nez v24, :cond_b

    .line 402
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    sub-int v17, v18, v24

    .line 405
    .local v17, "labelXAwayFromButton":I
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    move/from16 v24, v0

    if-nez v24, :cond_c

    move/from16 v14, v17

    .line 409
    .local v14, "labelLeft":I
    :goto_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsPosition:I

    move/from16 v24, v0

    if-nez v24, :cond_d

    move/from16 v15, v18

    .line 413
    .local v15, "labelRight":I
    :goto_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsVerticalOffset:I

    move/from16 v24, v0

    sub-int v24, v8, v24

    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredHeight()I

    move-result v25

    .line 414
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v26

    sub-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    add-int v16, v24, v25

    .line 416
    .local v16, "labelTop":I
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v24

    add-int v24, v24, v16

    move/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v13, v14, v0, v15, v1}, Landroid/view/View;->layout(IIII)V

    .line 418
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuOpening:Z

    move/from16 v24, v0

    if-nez v24, :cond_7

    .line 419
    const/16 v24, 0x4

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    .line 423
    .end local v14    # "labelLeft":I
    .end local v15    # "labelRight":I
    .end local v16    # "labelTop":I
    .end local v17    # "labelXAwayFromButton":I
    .end local v18    # "labelXNearButton":I
    .end local v19    # "labelsOffset":I
    :cond_7
    if-eqz v23, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    move/from16 v24, v0

    sub-int v22, v8, v24

    .line 425
    :goto_c
    goto/16 :goto_5

    .end local v8    # "childY":I
    .end local v13    # "label":Landroid/view/View;
    :cond_8
    move/from16 v8, v22

    .line 383
    goto/16 :goto_6

    .line 396
    .restart local v8    # "childY":I
    .restart local v13    # "label":Landroid/view/View;
    :cond_9
    invoke-virtual {v9}, Lcom/github/clans/fab/FloatingActionButton;->getMeasuredWidth()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    goto :goto_7

    .line 397
    .restart local v19    # "labelsOffset":I
    :cond_a
    add-int v18, v5, v19

    goto :goto_8

    .line 403
    .restart local v18    # "labelXNearButton":I
    :cond_b
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    add-int v17, v18, v24

    goto :goto_9

    .restart local v17    # "labelXAwayFromButton":I
    :cond_c
    move/from16 v14, v18

    .line 405
    goto :goto_a

    .restart local v14    # "labelLeft":I
    :cond_d
    move/from16 v15, v17

    .line 409
    goto :goto_b

    .line 425
    .end local v14    # "labelLeft":I
    .end local v17    # "labelXAwayFromButton":I
    .end local v18    # "labelXNearButton":I
    .end local v19    # "labelsOffset":I
    :cond_e
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v24

    add-int v24, v24, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    move/from16 v25, v0

    add-int v22, v24, v25

    goto :goto_c

    .line 427
    .end local v6    # "child":Landroid/view/View;
    .end local v7    # "childX":I
    .end local v8    # "childY":I
    .end local v9    # "fab":Lcom/github/clans/fab/FloatingActionButton;
    .end local v13    # "label":Landroid/view/View;
    :cond_f
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 297
    const/4 v14, 0x0

    .line 298
    .local v14, "width":I
    const/4 v9, 0x0

    .line 299
    .local v9, "height":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    .line 300
    const/4 v12, 0x0

    .line 302
    .local v12, "maxLabelWidth":I
    iget-object v2, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/github/clans/fab/FloatingActionMenu;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 304
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    if-ge v10, v1, :cond_2

    .line 305
    invoke-virtual {p0, v10}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 307
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    if-ne v2, v1, :cond_1

    .line 304
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 309
    :cond_1
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/github/clans/fab/FloatingActionMenu;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 310
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    goto :goto_1

    .line 313
    .end local v2    # "child":Landroid/view/View;
    :cond_2
    const/4 v10, 0x0

    :goto_2
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    if-ge v10, v1, :cond_6

    .line 314
    const/4 v13, 0x0

    .line 315
    .local v13, "usedWidth":I
    invoke-virtual {p0, v10}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 317
    .restart local v2    # "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_3

    iget-object v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    if-ne v2, v1, :cond_4

    .line 313
    :cond_3
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 319
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v13, v1

    .line 320
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v9, v1

    .line 322
    sget v1, Lcom/github/clans/fab/R$id;->fab_label:I

    invoke-virtual {v2, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/github/clans/fab/Label;

    .line 323
    .local v4, "label":Lcom/github/clans/fab/Label;
    if-eqz v4, :cond_3

    .line 324
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int v3, v1, v3

    iget-boolean v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUsingMenuLabel:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    div-int v11, v3, v1

    .line 325
    .local v11, "labelOffset":I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v4}, Lcom/github/clans/fab/Label;->calculateShadowWidth()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMargin:I

    add-int/2addr v1, v3

    add-int v6, v1, v11

    .line 326
    .local v6, "labelUsedWidth":I
    const/4 v8, 0x0

    move-object v3, p0

    move/from16 v5, p1

    move/from16 v7, p2

    invoke-virtual/range {v3 .. v8}, Lcom/github/clans/fab/FloatingActionMenu;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 327
    invoke-virtual {v4}, Lcom/github/clans/fab/Label;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v13, v1

    .line 328
    add-int v1, v13, v11

    invoke-static {v12, v1}, Ljava/lang/Math;->max(II)I

    move-result v12

    goto :goto_3

    .line 324
    .end local v6    # "labelUsedWidth":I
    .end local v11    # "labelOffset":I
    :cond_5
    const/4 v1, 0x2

    goto :goto_4

    .line 332
    .end local v2    # "child":Landroid/view/View;
    .end local v4    # "label":Lcom/github/clans/fab/Label;
    .end local v13    # "usedWidth":I
    :cond_6
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMaxButtonWidth:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mLabelsMargin:I

    add-int/2addr v3, v12

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingLeft()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingRight()I

    move-result v3

    add-int v14, v1, v3

    .line 334
    iget v1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonSpacing:I

    iget v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v9, v1

    .line 335
    invoke-direct {p0, v9}, Lcom/github/clans/fab/FloatingActionMenu;->adjustForOvershoot(I)I

    move-result v9

    .line 337
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_7

    .line 338
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getSuggestedMinimumWidth()I

    move-result v1

    move/from16 v0, p1

    invoke-static {v1, v0}, Lcom/github/clans/fab/FloatingActionMenu;->getDefaultSize(II)I

    move-result v14

    .line 341
    :cond_7
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_8

    .line 342
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getSuggestedMinimumHeight()I

    move-result v1

    move/from16 v0, p2

    invoke-static {v1, v0}, Lcom/github/clans/fab/FloatingActionMenu;->getDefaultSize(II)I

    move-result v9

    .line 345
    :cond_8
    invoke-virtual {p0, v14, v9}, Lcom/github/clans/fab/FloatingActionMenu;->setMeasuredDimension(II)V

    .line 346
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsSetClosedOnTouchOutside:Z

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 582
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public open(Z)V
    .locals 10
    .param p1, "animate"    # Z

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isOpened()Z

    move-result v5

    if-nez v5, :cond_5

    .line 617
    invoke-direct {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isBackgroundEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 618
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mShowBackgroundAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 621
    :cond_0
    iget-boolean v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconAnimated:Z

    if-eqz v5, :cond_1

    .line 622
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconToggleSet:Landroid/animation/AnimatorSet;

    if-eqz v5, :cond_3

    .line 623
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconToggleSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 630
    :cond_1
    :goto_0
    const/4 v2, 0x0

    .line 631
    .local v2, "delay":I
    const/4 v1, 0x0

    .line 632
    .local v1, "counter":I
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsMenuOpening:Z

    .line 633
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getChildCount()I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_4

    .line 634
    invoke-virtual {p0, v4}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 635
    .local v0, "child":Landroid/view/View;
    instance-of v5, v0, Lcom/github/clans/fab/FloatingActionButton;

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_2

    .line 636
    add-int/lit8 v1, v1, 0x1

    move-object v3, v0

    .line 638
    check-cast v3, Lcom/github/clans/fab/FloatingActionButton;

    .line 639
    .local v3, "fab":Lcom/github/clans/fab/FloatingActionButton;
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    new-instance v6, Lcom/github/clans/fab/FloatingActionMenu$5;

    invoke-direct {v6, p0, v3, p1}, Lcom/github/clans/fab/FloatingActionMenu$5;-><init>(Lcom/github/clans/fab/FloatingActionMenu;Lcom/github/clans/fab/FloatingActionButton;Z)V

    int-to-long v8, v2

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 654
    iget v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    add-int/2addr v2, v5

    .line 633
    .end local v3    # "fab":Lcom/github/clans/fab/FloatingActionButton;
    :cond_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 625
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "counter":I
    .end local v2    # "delay":I
    .end local v4    # "i":I
    :cond_3
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->cancel()V

    .line 626
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 658
    .restart local v1    # "counter":I
    .restart local v2    # "delay":I
    .restart local v4    # "i":I
    :cond_4
    iget-object v5, p0, Lcom/github/clans/fab/FloatingActionMenu;->mUiHandler:Landroid/os/Handler;

    new-instance v6, Lcom/github/clans/fab/FloatingActionMenu$6;

    invoke-direct {v6, p0}, Lcom/github/clans/fab/FloatingActionMenu$6;-><init>(Lcom/github/clans/fab/FloatingActionMenu;)V

    add-int/lit8 v1, v1, 0x1

    iget v7, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    mul-int/2addr v7, v1

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 669
    .end local v1    # "counter":I
    .end local v2    # "delay":I
    .end local v4    # "i":I
    :cond_5
    return-void
.end method

.method public removeAllMenuButtons()V
    .locals 5

    .prologue
    .line 977
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/github/clans/fab/FloatingActionMenu;->close(Z)V

    .line 979
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 980
    .local v2, "viewsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/github/clans/fab/FloatingActionButton;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 981
    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 982
    .local v1, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    if-eq v1, v3, :cond_0

    iget-object v3, p0, Lcom/github/clans/fab/FloatingActionMenu;->mImageToggle:Landroid/widget/ImageView;

    if-eq v1, v3, :cond_0

    instance-of v3, v1, Lcom/github/clans/fab/FloatingActionButton;

    if-eqz v3, :cond_0

    .line 983
    check-cast v1, Lcom/github/clans/fab/FloatingActionButton;

    .end local v1    # "v":Landroid/view/View;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 980
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 986
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/github/clans/fab/FloatingActionButton;

    .line 987
    .local v1, "v":Lcom/github/clans/fab/FloatingActionButton;
    invoke-virtual {p0, v1}, Lcom/github/clans/fab/FloatingActionMenu;->removeMenuButton(Lcom/github/clans/fab/FloatingActionButton;)V

    goto :goto_1

    .line 989
    .end local v1    # "v":Lcom/github/clans/fab/FloatingActionButton;
    :cond_2
    return-void
.end method

.method public removeMenuButton(Lcom/github/clans/fab/FloatingActionButton;)V
    .locals 1
    .param p1, "fab"    # Lcom/github/clans/fab/FloatingActionButton;

    .prologue
    .line 958
    invoke-virtual {p1}, Lcom/github/clans/fab/FloatingActionButton;->getLabelView()Lcom/github/clans/fab/Label;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->removeView(Landroid/view/View;)V

    .line 959
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->removeView(Landroid/view/View;)V

    .line 960
    iget v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mButtonsCount:I

    .line 961
    return-void
.end method

.method public setAnimated(Z)V
    .locals 7
    .param p1, "animated"    # Z

    .prologue
    const-wide/16 v2, 0x12c

    const-wide/16 v4, 0x0

    .line 751
    iput-boolean p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsAnimated:Z

    .line 752
    iget-object v6, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_0

    move-wide v0, v2

    :goto_0
    invoke-virtual {v6, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 753
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 754
    return-void

    :cond_0
    move-wide v0, v4

    .line 752
    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 753
    goto :goto_1
.end method

.method public setAnimationDelayPerItem(I)V
    .locals 0
    .param p1, "animationDelayPerItem"    # I

    .prologue
    .line 761
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mAnimationDelayPerItem:I

    .line 762
    return-void
.end method

.method public setClosedOnTouchOutside(Z)V
    .locals 0
    .param p1, "close"    # Z

    .prologue
    .line 906
    iput-boolean p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIsSetClosedOnTouchOutside:Z

    .line 907
    return-void
.end method

.method public setIconAnimated(Z)V
    .locals 0
    .param p1, "animated"    # Z

    .prologue
    .line 773
    iput-boolean p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconAnimated:Z

    .line 774
    return-void
.end method

.method public setIconAnimationCloseInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p1, "closeInterpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 743
    return-void
.end method

.method public setIconAnimationInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 733
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 734
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mCloseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 735
    return-void
.end method

.method public setIconAnimationOpenInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p1, "openInterpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 738
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mOpenAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 739
    return-void
.end method

.method public setIconToggleAnimatorSet(Landroid/animation/AnimatorSet;)V
    .locals 0
    .param p1, "toggleAnimatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 785
    iput-object p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mIconToggleSet:Landroid/animation/AnimatorSet;

    .line 786
    return-void
.end method

.method public setMenuButtonColorNormal(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 910
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorNormal:I

    .line 911
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setColorNormal(I)V

    .line 912
    return-void
.end method

.method public setMenuButtonColorNormalResId(I)V
    .locals 1
    .param p1, "colorResId"    # I

    .prologue
    .line 915
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorNormal:I

    .line 916
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setColorNormalResId(I)V

    .line 917
    return-void
.end method

.method public setMenuButtonColorPressed(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 924
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorPressed:I

    .line 925
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setColorPressed(I)V

    .line 926
    return-void
.end method

.method public setMenuButtonColorPressedResId(I)V
    .locals 1
    .param p1, "colorResId"    # I

    .prologue
    .line 929
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorPressed:I

    .line 930
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setColorPressedResId(I)V

    .line 931
    return-void
.end method

.method public setMenuButtonColorRipple(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 938
    iput p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorRipple:I

    .line 939
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setColorRipple(I)V

    .line 940
    return-void
.end method

.method public setMenuButtonColorRippleResId(I)V
    .locals 1
    .param p1, "colorResId"    # I

    .prologue
    .line 943
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuColorRipple:I

    .line 944
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setColorRippleResId(I)V

    .line 945
    return-void
.end method

.method public setMenuButtonHideAnimation(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "hideAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 798
    iput-object p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonHideAnimation:Landroid/view/animation/Animation;

    .line 799
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setHideAnimation(Landroid/view/animation/Animation;)V

    .line 800
    return-void
.end method

.method public setMenuButtonLabelText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 992
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setLabelText(Ljava/lang/String;)V

    .line 993
    return-void
.end method

.method public setMenuButtonShowAnimation(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "showAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 793
    iput-object p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonShowAnimation:Landroid/view/animation/Animation;

    .line 794
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setShowAnimation(Landroid/view/animation/Animation;)V

    .line 795
    return-void
.end method

.method public setOnMenuButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButton:Lcom/github/clans/fab/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/github/clans/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1001
    return-void
.end method

.method public setOnMenuToggleListener(Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;

    .prologue
    .line 769
    iput-object p1, p0, Lcom/github/clans/fab/FloatingActionMenu;->mToggleListener:Lcom/github/clans/fab/FloatingActionMenu$OnMenuToggleListener;

    .line 770
    return-void
.end method

.method public showMenu(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 817
    if-eqz p1, :cond_0

    .line 818
    iget-object v0, p0, Lcom/github/clans/fab/FloatingActionMenu;->mMenuButtonShowAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->startAnimation(Landroid/view/animation/Animation;)V

    .line 820
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/github/clans/fab/FloatingActionMenu;->setVisibility(I)V

    .line 822
    :cond_1
    return-void
.end method

.method public showMenuButton(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 869
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuButtonHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870
    invoke-direct {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->showMenuButtonWithImage(Z)V

    .line 872
    :cond_0
    return-void
.end method

.method public toggle(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->close(Z)V

    .line 613
    :goto_0
    return-void

    .line 611
    :cond_0
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->open(Z)V

    goto :goto_0
.end method

.method public toggleMenu(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 855
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->showMenu(Z)V

    .line 860
    :goto_0
    return-void

    .line 858
    :cond_0
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->hideMenu(Z)V

    goto :goto_0
.end method

.method public toggleMenuButton(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 898
    invoke-virtual {p0}, Lcom/github/clans/fab/FloatingActionMenu;->isMenuButtonHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->showMenuButton(Z)V

    .line 903
    :goto_0
    return-void

    .line 901
    :cond_0
    invoke-virtual {p0, p1}, Lcom/github/clans/fab/FloatingActionMenu;->hideMenuButton(Z)V

    goto :goto_0
.end method
