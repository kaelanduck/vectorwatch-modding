.class Lcom/vdurmont/emoji/EmojiTrie$Node;
.super Ljava/lang/Object;
.source "EmojiTrie.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vdurmont/emoji/EmojiTrie;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Node"
.end annotation


# instance fields
.field private children:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Lcom/vdurmont/emoji/EmojiTrie$Node;",
            ">;"
        }
    .end annotation
.end field

.field private emoji:Lcom/vdurmont/emoji/Emoji;

.field final synthetic this$0:Lcom/vdurmont/emoji/EmojiTrie;


# direct methods
.method private constructor <init>(Lcom/vdurmont/emoji/EmojiTrie;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->this$0:Lcom/vdurmont/emoji/EmojiTrie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->children:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vdurmont/emoji/EmojiTrie;Lcom/vdurmont/emoji/EmojiTrie$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vdurmont/emoji/EmojiTrie;
    .param p2, "x1"    # Lcom/vdurmont/emoji/EmojiTrie$1;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/vdurmont/emoji/EmojiTrie$Node;-><init>(Lcom/vdurmont/emoji/EmojiTrie;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/vdurmont/emoji/EmojiTrie$Node;
    .param p1, "x1"    # C

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->hasChild(C)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/vdurmont/emoji/EmojiTrie$Node;C)V
    .locals 0
    .param p0, "x0"    # Lcom/vdurmont/emoji/EmojiTrie$Node;
    .param p1, "x1"    # C

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->addChild(C)V

    return-void
.end method

.method static synthetic access$300(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Lcom/vdurmont/emoji/EmojiTrie$Node;
    .locals 1
    .param p0, "x0"    # Lcom/vdurmont/emoji/EmojiTrie$Node;
    .param p1, "x1"    # C

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->getChild(C)Lcom/vdurmont/emoji/EmojiTrie$Node;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vdurmont/emoji/EmojiTrie$Node;Lcom/vdurmont/emoji/Emoji;)V
    .locals 0
    .param p0, "x0"    # Lcom/vdurmont/emoji/EmojiTrie$Node;
    .param p1, "x1"    # Lcom/vdurmont/emoji/Emoji;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->setEmoji(Lcom/vdurmont/emoji/Emoji;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vdurmont/emoji/EmojiTrie$Node;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vdurmont/emoji/EmojiTrie$Node;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->isEndOfEmoji()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/vdurmont/emoji/EmojiTrie$Node;)Lcom/vdurmont/emoji/Emoji;
    .locals 1
    .param p0, "x0"    # Lcom/vdurmont/emoji/EmojiTrie$Node;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->getEmoji()Lcom/vdurmont/emoji/Emoji;

    move-result-object v0

    return-object v0
.end method

.method private addChild(C)V
    .locals 4
    .param p1, "child"    # C

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->children:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    new-instance v2, Lcom/vdurmont/emoji/EmojiTrie$Node;

    iget-object v3, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->this$0:Lcom/vdurmont/emoji/EmojiTrie;

    invoke-direct {v2, v3}, Lcom/vdurmont/emoji/EmojiTrie$Node;-><init>(Lcom/vdurmont/emoji/EmojiTrie;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    return-void
.end method

.method private getChild(C)Lcom/vdurmont/emoji/EmojiTrie$Node;
    .locals 2
    .param p1, "child"    # C

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->children:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiTrie$Node;

    return-object v0
.end method

.method private getEmoji()Lcom/vdurmont/emoji/Emoji;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->emoji:Lcom/vdurmont/emoji/Emoji;

    return-object v0
.end method

.method private hasChild(C)Z
    .locals 2
    .param p1, "child"    # C

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->children:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isEndOfEmoji()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->emoji:Lcom/vdurmont/emoji/Emoji;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setEmoji(Lcom/vdurmont/emoji/Emoji;)V
    .locals 0
    .param p1, "emoji"    # Lcom/vdurmont/emoji/Emoji;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/vdurmont/emoji/EmojiTrie$Node;->emoji:Lcom/vdurmont/emoji/Emoji;

    .line 83
    return-void
.end method
