.class public Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
.super Ljava/lang/Object;
.source "EmojiParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vdurmont/emoji/EmojiParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "UnicodeCandidate"
.end annotation


# instance fields
.field public final emoji:Lcom/vdurmont/emoji/Emoji;

.field public final endIndex:I

.field public final fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

.field public final startIndex:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "emoji"    # Ljava/lang/String;
    .param p2, "fitzpatrick"    # Ljava/lang/String;
    .param p3, "startIndex"    # I
    .param p4, "endIndex"    # I

    .prologue
    .line 363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364
    invoke-static {p1}, Lcom/vdurmont/emoji/EmojiManager;->getByUnicode(Ljava/lang/String;)Lcom/vdurmont/emoji/Emoji;

    move-result-object v0

    iput-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    .line 365
    invoke-static {p2}, Lcom/vdurmont/emoji/Fitzpatrick;->fitzpatrickFromUnicode(Ljava/lang/String;)Lcom/vdurmont/emoji/Fitzpatrick;

    move-result-object v0

    iput-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 366
    iput p3, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    .line 367
    iput p4, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    .line 368
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;IILcom/vdurmont/emoji/EmojiParser$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/vdurmont/emoji/EmojiParser$1;

    .prologue
    .line 358
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method


# virtual methods
.method public getEmojiUnicodeWithFitzpatrick()Ljava/lang/String;
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v0}, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    iget-object v1, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    invoke-virtual {v0, v1}, Lcom/vdurmont/emoji/Emoji;->getUnicode(Lcom/vdurmont/emoji/Fitzpatrick;)Ljava/lang/String;

    move-result-object v0

    .line 372
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v0}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEndIndexWithFitzpatrick()I
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    add-int/lit8 v0, v0, 0x2

    .line 377
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    goto :goto_0
.end method
