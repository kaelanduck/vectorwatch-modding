.class final enum Lcom/vdurmont/emoji/EmojiTrie$Matches;
.super Ljava/lang/Enum;
.source "EmojiTrie.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vdurmont/emoji/EmojiTrie;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Matches"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vdurmont/emoji/EmojiTrie$Matches;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vdurmont/emoji/EmojiTrie$Matches;

.field public static final enum EXACTLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

.field public static final enum IMPOSSIBLE:Lcom/vdurmont/emoji/EmojiTrie$Matches;

.field public static final enum POSSIBLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;

    const-string v1, "EXACTLY"

    invoke-direct {v0, v1, v2}, Lcom/vdurmont/emoji/EmojiTrie$Matches;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->EXACTLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    new-instance v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;

    const-string v1, "POSSIBLY"

    invoke-direct {v0, v1, v3}, Lcom/vdurmont/emoji/EmojiTrie$Matches;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->POSSIBLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    new-instance v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;

    const-string v1, "IMPOSSIBLE"

    invoke-direct {v0, v1, v4}, Lcom/vdurmont/emoji/EmojiTrie$Matches;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->IMPOSSIBLE:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vdurmont/emoji/EmojiTrie$Matches;

    sget-object v1, Lcom/vdurmont/emoji/EmojiTrie$Matches;->EXACTLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vdurmont/emoji/EmojiTrie$Matches;->POSSIBLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vdurmont/emoji/EmojiTrie$Matches;->IMPOSSIBLE:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->$VALUES:[Lcom/vdurmont/emoji/EmojiTrie$Matches;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vdurmont/emoji/EmojiTrie$Matches;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;

    return-object v0
.end method

.method public static values()[Lcom/vdurmont/emoji/EmojiTrie$Matches;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->$VALUES:[Lcom/vdurmont/emoji/EmojiTrie$Matches;

    invoke-virtual {v0}, [Lcom/vdurmont/emoji/EmojiTrie$Matches;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vdurmont/emoji/EmojiTrie$Matches;

    return-object v0
.end method


# virtual methods
.method public exactMatch()Z
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->EXACTLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public impossibleMatch()Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/vdurmont/emoji/EmojiTrie$Matches;->IMPOSSIBLE:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
