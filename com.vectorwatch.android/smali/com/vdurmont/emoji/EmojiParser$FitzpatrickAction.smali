.class public final enum Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;
.super Ljava/lang/Enum;
.source "EmojiParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vdurmont/emoji/EmojiParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FitzpatrickAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

.field public static final enum IGNORE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

.field public static final enum PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

.field public static final enum REMOVE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 405
    new-instance v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    const-string v1, "PARSE"

    invoke-direct {v0, v1, v2}, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    .line 410
    new-instance v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v3}, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->REMOVE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    .line 415
    new-instance v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    const-string v1, "IGNORE"

    invoke-direct {v0, v1, v4}, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->IGNORE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    .line 401
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    sget-object v1, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->REMOVE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->IGNORE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->$VALUES:[Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 401
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 401
    const-class v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    return-object v0
.end method

.method public static values()[Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;
    .locals 1

    .prologue
    .line 401
    sget-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->$VALUES:[Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    invoke-virtual {v0}, [Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    return-object v0
.end method
