.class public Lcom/vdurmont/emoji/Emoji;
.super Ljava/lang/Object;
.source "Emoji.java"


# instance fields
.field private final aliases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final description:Ljava/lang/String;

.field private final htmlDec:Ljava/lang/String;

.field private final htmlHex:Ljava/lang/String;

.field private final supportsFitzpatrick:Z

.field private final tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final unicode:Ljava/lang/String;


# direct methods
.method protected varargs constructor <init>(Ljava/lang/String;ZLjava/util/List;Ljava/util/List;[B)V
    .locals 13
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "supportsFitzpatrick"    # Z
    .param p5, "bytes"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p3, "aliases":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/vdurmont/emoji/Emoji;->description:Ljava/lang/String;

    .line 35
    iput-boolean p2, p0, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick:Z

    .line 36
    invoke-static/range {p3 .. p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vdurmont/emoji/Emoji;->aliases:Ljava/util/List;

    .line 37
    invoke-static/range {p4 .. p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vdurmont/emoji/Emoji;->tags:Ljava/util/List;

    .line 39
    const/4 v2, 0x0

    .line 41
    .local v2, "count":I
    :try_start_0
    new-instance v9, Ljava/lang/String;

    const-string v10, "UTF-8"

    move-object/from16 v0, p5

    invoke-direct {v9, v0, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v9, p0, Lcom/vdurmont/emoji/Emoji;->unicode:Ljava/lang/String;

    .line 42
    invoke-virtual {p0}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v8

    .line 43
    .local v8, "stringLength":I
    new-array v6, v8, [Ljava/lang/String;

    .line 44
    .local v6, "pointCodes":[Ljava/lang/String;
    new-array v7, v8, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .local v7, "pointCodesHex":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "offset":I
    move v3, v2

    .end local v2    # "count":I
    .local v3, "count":I
    :goto_0
    if-ge v5, v8, :cond_0

    .line 47
    :try_start_1
    invoke-virtual {p0}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    .line 49
    .local v1, "codePoint":I
    const-string v9, "&#%d;"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v3
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 50
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "count":I
    .restart local v2    # "count":I
    :try_start_2
    const-string v9, "&#x%x;"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v3

    .line 52
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v9

    add-int/2addr v5, v9

    move v3, v2

    .line 53
    .end local v2    # "count":I
    .restart local v3    # "count":I
    goto :goto_0

    .line 54
    .end local v1    # "codePoint":I
    :cond_0
    :try_start_3
    invoke-direct {p0, v6, v3}, Lcom/vdurmont/emoji/Emoji;->stringJoin([Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/vdurmont/emoji/Emoji;->htmlDec:Ljava/lang/String;

    .line 55
    invoke-direct {p0, v7, v3}, Lcom/vdurmont/emoji/Emoji;->stringJoin([Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/vdurmont/emoji/Emoji;->htmlHex:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_1

    .line 59
    return-void

    .line 56
    .end local v3    # "count":I
    .end local v5    # "offset":I
    .end local v6    # "pointCodes":[Ljava/lang/String;
    .end local v7    # "pointCodesHex":[Ljava/lang/String;
    .end local v8    # "stringLength":I
    .restart local v2    # "count":I
    :catch_0
    move-exception v4

    .line 57
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    :goto_1
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 56
    .end local v2    # "count":I
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v3    # "count":I
    .restart local v5    # "offset":I
    .restart local v6    # "pointCodes":[Ljava/lang/String;
    .restart local v7    # "pointCodesHex":[Ljava/lang/String;
    .restart local v8    # "stringLength":I
    :catch_1
    move-exception v4

    move v2, v3

    .end local v3    # "count":I
    .restart local v2    # "count":I
    goto :goto_1
.end method

.method private stringJoin([Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "array"    # [Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    .line 67
    const-string v1, ""

    .line 68
    .local v1, "joined":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 156
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/vdurmont/emoji/Emoji;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/vdurmont/emoji/Emoji;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAliases()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->aliases:Ljava/util/List;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getHtmlDecimal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->htmlDec:Ljava/lang/String;

    return-object v0
.end method

.method public getHtmlHexidecimal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->htmlHex:Ljava/lang/String;

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->tags:Ljava/util/List;

    return-object v0
.end method

.method public getUnicode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->unicode:Ljava/lang/String;

    return-object v0
.end method

.method public getUnicode(Lcom/vdurmont/emoji/Fitzpatrick;)Ljava/lang/String;
    .locals 2
    .param p1, "fitzpatrick"    # Lcom/vdurmont/emoji/Fitzpatrick;

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot get the unicode with a fitzpatrick modifier, the emoji doesn\'t support fitzpatrick."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    if-nez p1, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/vdurmont/emoji/Fitzpatrick;->unicode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vdurmont/emoji/Emoji;->unicode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public supportsFitzpatrick()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Emoji{description=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vdurmont/emoji/Emoji;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", supportsFitzpatrick="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", aliases="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vdurmont/emoji/Emoji;->aliases:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vdurmont/emoji/Emoji;->tags:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", unicode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vdurmont/emoji/Emoji;->unicode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", htmlDec=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vdurmont/emoji/Emoji;->htmlDec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", htmlHex=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vdurmont/emoji/Emoji;->htmlHex:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
