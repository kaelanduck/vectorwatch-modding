.class public Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;
.super Ljava/lang/Object;
.source "EmojiParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vdurmont/emoji/EmojiParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "AliasCandidate"
.end annotation


# instance fields
.field public final alias:Ljava/lang/String;

.field public final fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

.field public final fullString:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fullString"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;
    .param p3, "fitzpatrickString"    # Ljava/lang/String;

    .prologue
    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388
    iput-object p1, p0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fullString:Ljava/lang/String;

    .line 389
    iput-object p2, p0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->alias:Ljava/lang/String;

    .line 390
    if-nez p3, :cond_0

    .line 391
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 395
    :goto_0
    return-void

    .line 393
    :cond_0
    invoke-static {p3}, Lcom/vdurmont/emoji/Fitzpatrick;->fitzpatrickFromType(Ljava/lang/String;)Lcom/vdurmont/emoji/Fitzpatrick;

    move-result-object v0

    iput-object v0, p0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/vdurmont/emoji/EmojiParser$1;

    .prologue
    .line 382
    invoke-direct {p0, p1, p2, p3}, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
