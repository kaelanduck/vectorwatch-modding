.class public Lcom/vdurmont/emoji/EmojiManager;
.super Ljava/lang/Object;
.source "EmojiManager.java"


# static fields
.field private static final ALL_EMOJIS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMOJIS_BY_ALIAS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMOJIS_BY_TAG:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final EMOJI_TRIE:Lcom/vdurmont/emoji/EmojiTrie;

.field private static final PATH:Ljava/lang/String; = "/emojis.json"


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 19
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    sput-object v6, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_ALIAS:Ljava/util/Map;

    .line 20
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    sput-object v6, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_TAG:Ljava/util/Map;

    .line 26
    :try_start_0
    const-class v6, Lcom/vdurmont/emoji/EmojiLoader;

    const-string v7, "/emojis.json"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 27
    .local v4, "stream":Ljava/io/InputStream;
    invoke-static {v4}, Lcom/vdurmont/emoji/EmojiLoader;->loadEmojis(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v3

    .line 28
    .local v3, "emojis":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/Emoji;>;"
    sput-object v3, Lcom/vdurmont/emoji/EmojiManager;->ALL_EMOJIS:Ljava/util/List;

    .line 29
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vdurmont/emoji/Emoji;

    .line 30
    .local v2, "emoji":Lcom/vdurmont/emoji/Emoji;
    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getTags()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 31
    .local v5, "tag":Ljava/lang/String;
    sget-object v6, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_TAG:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 32
    sget-object v6, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_TAG:Ljava/util/Map;

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v6, v5, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    :cond_1
    sget-object v6, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_TAG:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Set;

    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    .end local v2    # "emoji":Lcom/vdurmont/emoji/Emoji;
    .end local v3    # "emojis":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/Emoji;>;"
    .end local v5    # "tag":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 44
    .local v1, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 36
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "emoji":Lcom/vdurmont/emoji/Emoji;
    .restart local v3    # "emojis":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/Emoji;>;"
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getAliases()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 37
    .local v0, "alias":Ljava/lang/String;
    sget-object v8, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_ALIAS:Ljava/util/Map;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 41
    .end local v0    # "alias":Ljava/lang/String;
    .end local v2    # "emoji":Lcom/vdurmont/emoji/Emoji;
    :cond_3
    new-instance v6, Lcom/vdurmont/emoji/EmojiTrie;

    invoke-direct {v6, v3}, Lcom/vdurmont/emoji/EmojiTrie;-><init>(Ljava/util/Collection;)V

    sput-object v6, Lcom/vdurmont/emoji/EmojiManager;->EMOJI_TRIE:Lcom/vdurmont/emoji/EmojiTrie;

    .line 42
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 46
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAll()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->ALL_EMOJIS:Ljava/util/List;

    return-object v0
.end method

.method public static getAllTags()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_TAG:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getByUnicode(Ljava/lang/String;)Lcom/vdurmont/emoji/Emoji;
    .locals 1
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 101
    if-nez p0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->EMOJI_TRIE:Lcom/vdurmont/emoji/EmojiTrie;

    invoke-virtual {v0, p0}, Lcom/vdurmont/emoji/EmojiTrie;->getEmoji(Ljava/lang/String;)Lcom/vdurmont/emoji/Emoji;

    move-result-object v0

    goto :goto_0
.end method

.method public static getForAlias(Ljava/lang/String;)Lcom/vdurmont/emoji/Emoji;
    .locals 2
    .param p0, "alias"    # Ljava/lang/String;

    .prologue
    .line 75
    if-nez p0, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_ALIAS:Ljava/util/Map;

    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiManager;->trimAlias(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/Emoji;

    goto :goto_0
.end method

.method public static getForTag(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    if-nez p0, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->EMOJIS_BY_TAG:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    goto :goto_0
.end method

.method public static isEmoji([C)Lcom/vdurmont/emoji/EmojiTrie$Matches;
    .locals 1
    .param p0, "sequence"    # [C

    .prologue
    .line 137
    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->EMOJI_TRIE:Lcom/vdurmont/emoji/EmojiTrie;

    invoke-virtual {v0, p0}, Lcom/vdurmont/emoji/EmojiTrie;->isEmoji([C)Lcom/vdurmont/emoji/EmojiTrie$Matches;

    move-result-object v0

    return-object v0
.end method

.method public static isEmoji(Ljava/lang/String;)Z
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 124
    if-eqz p0, :cond_0

    sget-object v0, Lcom/vdurmont/emoji/EmojiManager;->EMOJI_TRIE:Lcom/vdurmont/emoji/EmojiTrie;

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vdurmont/emoji/EmojiTrie;->isEmoji([C)Lcom/vdurmont/emoji/EmojiTrie$Matches;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vdurmont/emoji/EmojiTrie$Matches;->exactMatch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static trimAlias(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "alias"    # Ljava/lang/String;

    .prologue
    .line 82
    move-object v0, p0

    .line 83
    .local v0, "result":Ljava/lang/String;
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 86
    :cond_0
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_1
    return-object v0
.end method
