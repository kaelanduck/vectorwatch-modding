.class public Lcom/vdurmont/emoji/EmojiParser;
.super Ljava/lang/Object;
.source "EmojiParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;,
        Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;,
        Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    }
.end annotation


# static fields
.field private static final ALIAS_CANDIDATE_PATTERN:Ljava/util/regex/Pattern;

.field private static final ALL_FITZPATRICK_CODES:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 16
    const-string v2, "(?<=:)\\+?(\\w|\\||\\-)+(?=:)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    sput-object v2, Lcom/vdurmont/emoji/EmojiParser;->ALIAS_CANDIDATE_PATTERN:Ljava/util/regex/Pattern;

    .line 20
    const-string v1, ""

    .line 21
    .local v1, "fitzpatrickRegexString":Ljava/lang/String;
    invoke-static {}, Lcom/vdurmont/emoji/Fitzpatrick;->values()[Lcom/vdurmont/emoji/Fitzpatrick;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 22
    .local v0, "fitzpatrick":Lcom/vdurmont/emoji/Fitzpatrick;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/vdurmont/emoji/Fitzpatrick;->unicode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 21
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 24
    .end local v0    # "fitzpatrick":Lcom/vdurmont/emoji/Fitzpatrick;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(?:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    sput-object v2, Lcom/vdurmont/emoji/EmojiParser;->ALL_FITZPATRICK_CODES:Ljava/util/regex/Pattern;

    .line 25
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static getAliasCandidates(Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p0, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v0, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;>;"
    sget-object v4, Lcom/vdurmont/emoji/EmojiParser;->ALIAS_CANDIDATE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 133
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2, v8}, Ljava/util/regex/Matcher;->useTransparentBounds(Z)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 134
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 135
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "match":Ljava/lang/String;
    const-string/jumbo v4, "|"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 137
    new-instance v4, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;

    invoke-direct {v4, v1, v1, v7, v7}, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$1;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_0
    const-string v4, "\\|"

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 140
    .local v3, "splitted":[Ljava/lang/String;
    array-length v4, v3

    if-eq v4, v9, :cond_1

    array-length v4, v3

    if-le v4, v9, :cond_2

    .line 141
    :cond_1
    new-instance v4, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;

    const/4 v5, 0x0

    aget-object v5, v3, v5

    aget-object v6, v3, v8

    invoke-direct {v4, v1, v5, v6, v7}, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$1;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_2
    new-instance v4, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;

    invoke-direct {v4, v1, v1, v7, v7}, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$1;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    .end local v1    # "match":Ljava/lang/String;
    .end local v3    # "splitted":[Ljava/lang/String;
    :cond_3
    return-object v0
.end method

.method private static getEmojiEndPos([CI)I
    .locals 4
    .param p0, "text"    # [C
    .param p1, "startPos"    # I

    .prologue
    .line 342
    const/4 v0, -0x1

    .line 343
    .local v0, "best":I
    add-int/lit8 v1, p1, 0x1

    .local v1, "j":I
    :goto_0
    array-length v3, p0

    if-gt v1, v3, :cond_2

    .line 344
    invoke-static {p0, p1, v1}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v3

    invoke-static {v3}, Lcom/vdurmont/emoji/EmojiManager;->isEmoji([C)Lcom/vdurmont/emoji/EmojiTrie$Matches;

    move-result-object v2

    .line 346
    .local v2, "status":Lcom/vdurmont/emoji/EmojiTrie$Matches;
    invoke-virtual {v2}, Lcom/vdurmont/emoji/EmojiTrie$Matches;->exactMatch()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 347
    move v0, v1

    .line 343
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    :cond_1
    invoke-virtual {v2}, Lcom/vdurmont/emoji/EmojiTrie$Matches;->impossibleMatch()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 353
    .end local v2    # "status":Lcom/vdurmont/emoji/EmojiTrie$Matches;
    :cond_2
    return v0
.end method

.method private static getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p0, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 320
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 321
    .local v7, "inputCharArray":[C
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 322
    .local v6, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 323
    invoke-static {v7, v3}, Lcom/vdurmont/emoji/EmojiParser;->getEmojiEndPos([CI)I

    move-result v4

    .line 325
    .local v4, "emojiEnd":I
    const/4 v0, -0x1

    if-eq v4, v0, :cond_0

    .line 326
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "emojiString":Ljava/lang/String;
    add-int/lit8 v0, v4, 0x2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-gt v0, v8, :cond_1

    add-int/lit8 v0, v4, 0x2

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 328
    .local v2, "fitzpatrickString":Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    invoke-direct/range {v0 .. v5}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/vdurmont/emoji/EmojiParser$1;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    move v3, v4

    .line 322
    .end local v1    # "emojiString":Ljava/lang/String;
    .end local v2    # "fitzpatrickString":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .restart local v1    # "emojiString":Ljava/lang/String;
    :cond_1
    move-object v2, v5

    .line 327
    goto :goto_1

    .line 332
    .end local v1    # "emojiString":Ljava/lang/String;
    .end local v4    # "emojiEnd":I
    :cond_2
    return-object v6
.end method

.method public static parseToAliases(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    invoke-static {p0, v0}, Lcom/vdurmont/emoji/EmojiParser;->parseToAliases(Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static parseToAliases(Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;)Ljava/lang/String;
    .locals 8
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "fitzpatrickAction"    # Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    .prologue
    const/4 v7, 0x0

    .line 63
    sget-object v4, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->REMOVE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    if-ne p1, v4, :cond_0

    .line 64
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->removeFitzpatrick(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 67
    :cond_0
    const/4 v1, 0x0

    .line 68
    .local v1, "prev":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 70
    .local v2, "replacements":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    .line 71
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    iget v4, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    sget-object v4, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    if-ne p1, v4, :cond_1

    iget-object v4, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v4}, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 74
    iget-object v4, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    if-eqz v4, :cond_1

    .line 75
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v4}, Lcom/vdurmont/emoji/Emoji;->getAliases()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v4, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    invoke-virtual {v4}, Lcom/vdurmont/emoji/Fitzpatrick;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ":"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {v0}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->getEndIndexWithFitzpatrick()I

    move-result v1

    .line 78
    goto :goto_0

    .line 82
    :cond_1
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v4}, Lcom/vdurmont/emoji/Emoji;->getAliases()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ":"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget v1, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    .line 84
    goto :goto_0

    .line 86
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    :cond_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static parseToHtmlDecimal(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 158
    sget-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    invoke-static {p0, v0}, Lcom/vdurmont/emoji/EmojiParser;->parseToHtmlDecimal(Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static parseToHtmlDecimal(Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;)Ljava/lang/String;
    .locals 7
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "fitzpatrickAction"    # Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    .prologue
    .line 177
    sget-object v4, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->IGNORE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    if-eq p1, v4, :cond_0

    .line 178
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->removeFitzpatrick(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 181
    :cond_0
    const/4 v1, 0x0

    .line 182
    .local v1, "prev":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 184
    .local v2, "replacements":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    .line 185
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    iget v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v6}, Lcom/vdurmont/emoji/Emoji;->getHtmlDecimal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    iget v1, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    .line 187
    goto :goto_0

    .line 189
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static parseToHtmlHexadecimal(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 200
    sget-object v0, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->PARSE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    invoke-static {p0, v0}, Lcom/vdurmont/emoji/EmojiParser;->parseToHtmlHexadecimal(Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static parseToHtmlHexadecimal(Ljava/lang/String;Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;)Ljava/lang/String;
    .locals 7
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "fitzpatrickAction"    # Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    .prologue
    .line 219
    sget-object v4, Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;->IGNORE:Lcom/vdurmont/emoji/EmojiParser$FitzpatrickAction;

    if-eq p1, v4, :cond_0

    .line 220
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->removeFitzpatrick(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 223
    :cond_0
    const/4 v1, 0x0

    .line 224
    .local v1, "prev":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 225
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 226
    .local v2, "replacements":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    .line 227
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    iget v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-virtual {v6}, Lcom/vdurmont/emoji/Emoji;->getHtmlHexidecimal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    iget v1, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    .line 229
    goto :goto_0

    .line 231
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static parseToUnicode(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getAliasCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 106
    .local v1, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;>;"
    move-object v4, p0

    .line 107
    .local v4, "result":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;

    .line 108
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;
    iget-object v6, v0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->alias:Ljava/lang/String;

    invoke-static {v6}, Lcom/vdurmont/emoji/EmojiManager;->getForAlias(Ljava/lang/String;)Lcom/vdurmont/emoji/Emoji;

    move-result-object v2

    .line 109
    .local v2, "emoji":Lcom/vdurmont/emoji/Emoji;
    if-eqz v2, :cond_0

    .line 110
    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->supportsFitzpatrick()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, v0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    if-nez v6, :cond_0

    .line 111
    :cond_1
    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v3

    .line 112
    .local v3, "replacement":Ljava/lang/String;
    iget-object v6, v0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    if-eqz v6, :cond_2

    .line 113
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fitzpatrick:Lcom/vdurmont/emoji/Fitzpatrick;

    iget-object v7, v7, Lcom/vdurmont/emoji/Fitzpatrick;->unicode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 115
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;->fullString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 121
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$AliasCandidate;
    .end local v2    # "emoji":Lcom/vdurmont/emoji/Emoji;
    .end local v3    # "replacement":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/vdurmont/emoji/EmojiManager;->getAll()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vdurmont/emoji/Emoji;

    .line 122
    .restart local v2    # "emoji":Lcom/vdurmont/emoji/Emoji;
    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getHtmlHexidecimal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 123
    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getHtmlDecimal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 124
    goto :goto_1

    .line 126
    .end local v2    # "emoji":Lcom/vdurmont/emoji/Emoji;
    :cond_4
    return-object v4
.end method

.method public static removeAllEmojis(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 247
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->removeFitzpatrick(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 250
    const/4 v1, 0x0

    .line 251
    .local v1, "prev":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 253
    .local v2, "replacements":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    .line 254
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    iget v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    iget v1, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->endIndex:I

    .line 256
    goto :goto_0

    .line 258
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static removeAllEmojisExcept(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "emojisToKeep":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vdurmont/emoji/Emoji;>;"
    const/4 v1, 0x0

    .line 296
    .local v1, "prev":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 298
    .local v2, "replacements":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    .line 299
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    iget v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    invoke-virtual {v0}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->getEndIndexWithFitzpatrick()I

    move-result v1

    .line 302
    iget-object v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-interface {p1, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->getEmojiUnicodeWithFitzpatrick()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 307
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static removeEmojis(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 271
    .local p1, "emojisToRemove":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vdurmont/emoji/Emoji;>;"
    const/4 v1, 0x0

    .line 272
    .local v1, "prev":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/vdurmont/emoji/EmojiParser;->getUnicodeCandidates(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 274
    .local v2, "replacements":Ljava/util/List;, "Ljava/util/List<Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;

    .line 275
    .local v0, "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    iget v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->startIndex:I

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    invoke-virtual {v0}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->getEndIndexWithFitzpatrick()I

    move-result v1

    .line 278
    iget-object v5, v0, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->emoji:Lcom/vdurmont/emoji/Emoji;

    invoke-interface {p1, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 279
    invoke-virtual {v0}, Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;->getEmojiUnicodeWithFitzpatrick()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 283
    .end local v0    # "candidate":Lcom/vdurmont/emoji/EmojiParser$UnicodeCandidate;
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static removeFitzpatrick(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 235
    sget-object v0, Lcom/vdurmont/emoji/EmojiParser;->ALL_FITZPATRICK_CODES:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
