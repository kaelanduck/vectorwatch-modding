.class public final enum Lcom/vdurmont/emoji/Fitzpatrick;
.super Ljava/lang/Enum;
.source "Fitzpatrick.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vdurmont/emoji/Fitzpatrick;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vdurmont/emoji/Fitzpatrick;

.field public static final enum TYPE_1_2:Lcom/vdurmont/emoji/Fitzpatrick;

.field public static final enum TYPE_3:Lcom/vdurmont/emoji/Fitzpatrick;

.field public static final enum TYPE_4:Lcom/vdurmont/emoji/Fitzpatrick;

.field public static final enum TYPE_5:Lcom/vdurmont/emoji/Fitzpatrick;

.field public static final enum TYPE_6:Lcom/vdurmont/emoji/Fitzpatrick;


# instance fields
.field public final unicode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/vdurmont/emoji/Fitzpatrick;

    const-string v1, "TYPE_1_2"

    const-string/jumbo v2, "\ud83c\udffb"

    invoke-direct {v0, v1, v3, v2}, Lcom/vdurmont/emoji/Fitzpatrick;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_1_2:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 15
    new-instance v0, Lcom/vdurmont/emoji/Fitzpatrick;

    const-string v1, "TYPE_3"

    const-string/jumbo v2, "\ud83c\udffc"

    invoke-direct {v0, v1, v4, v2}, Lcom/vdurmont/emoji/Fitzpatrick;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_3:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 20
    new-instance v0, Lcom/vdurmont/emoji/Fitzpatrick;

    const-string v1, "TYPE_4"

    const-string/jumbo v2, "\ud83c\udffd"

    invoke-direct {v0, v1, v5, v2}, Lcom/vdurmont/emoji/Fitzpatrick;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_4:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 25
    new-instance v0, Lcom/vdurmont/emoji/Fitzpatrick;

    const-string v1, "TYPE_5"

    const-string/jumbo v2, "\ud83c\udffe"

    invoke-direct {v0, v1, v6, v2}, Lcom/vdurmont/emoji/Fitzpatrick;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_5:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 30
    new-instance v0, Lcom/vdurmont/emoji/Fitzpatrick;

    const-string v1, "TYPE_6"

    const-string/jumbo v2, "\ud83c\udfff"

    invoke-direct {v0, v1, v7, v2}, Lcom/vdurmont/emoji/Fitzpatrick;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_6:Lcom/vdurmont/emoji/Fitzpatrick;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vdurmont/emoji/Fitzpatrick;

    sget-object v1, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_1_2:Lcom/vdurmont/emoji/Fitzpatrick;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_3:Lcom/vdurmont/emoji/Fitzpatrick;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_4:Lcom/vdurmont/emoji/Fitzpatrick;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_5:Lcom/vdurmont/emoji/Fitzpatrick;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vdurmont/emoji/Fitzpatrick;->TYPE_6:Lcom/vdurmont/emoji/Fitzpatrick;

    aput-object v1, v0, v7

    sput-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->$VALUES:[Lcom/vdurmont/emoji/Fitzpatrick;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "unicode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lcom/vdurmont/emoji/Fitzpatrick;->unicode:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static fitzpatrickFromType(Ljava/lang/String;)Lcom/vdurmont/emoji/Fitzpatrick;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 53
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vdurmont/emoji/Fitzpatrick;->valueOf(Ljava/lang/String;)Lcom/vdurmont/emoji/Fitzpatrick;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 55
    :goto_0
    return-object v1

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static fitzpatrickFromUnicode(Ljava/lang/String;)Lcom/vdurmont/emoji/Fitzpatrick;
    .locals 5
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {}, Lcom/vdurmont/emoji/Fitzpatrick;->values()[Lcom/vdurmont/emoji/Fitzpatrick;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 44
    .local v0, "v":Lcom/vdurmont/emoji/Fitzpatrick;
    iget-object v4, v0, Lcom/vdurmont/emoji/Fitzpatrick;->unicode:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 48
    .end local v0    # "v":Lcom/vdurmont/emoji/Fitzpatrick;
    :goto_1
    return-object v0

    .line 43
    .restart local v0    # "v":Lcom/vdurmont/emoji/Fitzpatrick;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    .end local v0    # "v":Lcom/vdurmont/emoji/Fitzpatrick;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vdurmont/emoji/Fitzpatrick;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/vdurmont/emoji/Fitzpatrick;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vdurmont/emoji/Fitzpatrick;

    return-object v0
.end method

.method public static values()[Lcom/vdurmont/emoji/Fitzpatrick;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/vdurmont/emoji/Fitzpatrick;->$VALUES:[Lcom/vdurmont/emoji/Fitzpatrick;

    invoke-virtual {v0}, [Lcom/vdurmont/emoji/Fitzpatrick;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vdurmont/emoji/Fitzpatrick;

    return-object v0
.end method
