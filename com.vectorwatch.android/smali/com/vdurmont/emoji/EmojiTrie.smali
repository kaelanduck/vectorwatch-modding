.class public Lcom/vdurmont/emoji/EmojiTrie;
.super Ljava/lang/Object;
.source "EmojiTrie.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vdurmont/emoji/EmojiTrie$Node;,
        Lcom/vdurmont/emoji/EmojiTrie$Matches;
    }
.end annotation


# instance fields
.field private root:Lcom/vdurmont/emoji/EmojiTrie$Node;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vdurmont/emoji/Emoji;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 10
    .local p1, "emojis":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vdurmont/emoji/Emoji;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v3, Lcom/vdurmont/emoji/EmojiTrie$Node;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/vdurmont/emoji/EmojiTrie$Node;-><init>(Lcom/vdurmont/emoji/EmojiTrie;Lcom/vdurmont/emoji/EmojiTrie$1;)V

    iput-object v3, p0, Lcom/vdurmont/emoji/EmojiTrie;->root:Lcom/vdurmont/emoji/EmojiTrie$Node;

    .line 11
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vdurmont/emoji/Emoji;

    .line 12
    .local v1, "emoji":Lcom/vdurmont/emoji/Emoji;
    iget-object v2, p0, Lcom/vdurmont/emoji/EmojiTrie;->root:Lcom/vdurmont/emoji/EmojiTrie$Node;

    .line 13
    .local v2, "tree":Lcom/vdurmont/emoji/EmojiTrie$Node;
    invoke-virtual {v1}, Lcom/vdurmont/emoji/Emoji;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v6, v5

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v6, :cond_1

    aget-char v0, v5, v3

    .line 14
    .local v0, "c":C
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->hasChild(C)Z
    invoke-static {v2, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$100(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Z

    move-result v7

    if-nez v7, :cond_0

    .line 15
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->addChild(C)V
    invoke-static {v2, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$200(Lcom/vdurmont/emoji/EmojiTrie$Node;C)V

    .line 17
    :cond_0
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->getChild(C)Lcom/vdurmont/emoji/EmojiTrie$Node;
    invoke-static {v2, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$300(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Lcom/vdurmont/emoji/EmojiTrie$Node;

    move-result-object v2

    .line 13
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 19
    .end local v0    # "c":C
    :cond_1
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->setEmoji(Lcom/vdurmont/emoji/Emoji;)V
    invoke-static {v2, v1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$400(Lcom/vdurmont/emoji/EmojiTrie$Node;Lcom/vdurmont/emoji/Emoji;)V

    goto :goto_0

    .line 21
    .end local v1    # "emoji":Lcom/vdurmont/emoji/Emoji;
    .end local v2    # "tree":Lcom/vdurmont/emoji/EmojiTrie$Node;
    :cond_2
    return-void
.end method


# virtual methods
.method public getEmoji(Ljava/lang/String;)Lcom/vdurmont/emoji/Emoji;
    .locals 6
    .param p1, "unicode"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v1, p0, Lcom/vdurmont/emoji/EmojiTrie;->root:Lcom/vdurmont/emoji/EmojiTrie$Node;

    .line 56
    .local v1, "tree":Lcom/vdurmont/emoji/EmojiTrie$Node;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-char v0, v3, v2

    .line 57
    .local v0, "c":C
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->hasChild(C)Z
    invoke-static {v1, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$100(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Z

    move-result v5

    if-nez v5, :cond_0

    .line 58
    const/4 v2, 0x0

    .line 62
    .end local v0    # "c":C
    :goto_1
    return-object v2

    .line 60
    .restart local v0    # "c":C
    :cond_0
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->getChild(C)Lcom/vdurmont/emoji/EmojiTrie$Node;
    invoke-static {v1, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$300(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Lcom/vdurmont/emoji/EmojiTrie$Node;

    move-result-object v1

    .line 56
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 62
    .end local v0    # "c":C
    :cond_1
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->getEmoji()Lcom/vdurmont/emoji/Emoji;
    invoke-static {v1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$600(Lcom/vdurmont/emoji/EmojiTrie$Node;)Lcom/vdurmont/emoji/Emoji;

    move-result-object v2

    goto :goto_1
.end method

.method public isEmoji([C)Lcom/vdurmont/emoji/EmojiTrie$Matches;
    .locals 5
    .param p1, "sequence"    # [C

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    sget-object v2, Lcom/vdurmont/emoji/EmojiTrie$Matches;->POSSIBLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    .line 45
    :goto_0
    return-object v2

    .line 37
    :cond_0
    iget-object v1, p0, Lcom/vdurmont/emoji/EmojiTrie;->root:Lcom/vdurmont/emoji/EmojiTrie$Node;

    .line 38
    .local v1, "tree":Lcom/vdurmont/emoji/EmojiTrie$Node;
    array-length v3, p1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-char v0, p1, v2

    .line 39
    .local v0, "c":C
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->hasChild(C)Z
    invoke-static {v1, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$100(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 40
    sget-object v2, Lcom/vdurmont/emoji/EmojiTrie$Matches;->IMPOSSIBLE:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    goto :goto_0

    .line 42
    :cond_1
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->getChild(C)Lcom/vdurmont/emoji/EmojiTrie$Node;
    invoke-static {v1, v0}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$300(Lcom/vdurmont/emoji/EmojiTrie$Node;C)Lcom/vdurmont/emoji/EmojiTrie$Node;

    move-result-object v1

    .line 38
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 45
    .end local v0    # "c":C
    :cond_2
    # invokes: Lcom/vdurmont/emoji/EmojiTrie$Node;->isEndOfEmoji()Z
    invoke-static {v1}, Lcom/vdurmont/emoji/EmojiTrie$Node;->access$500(Lcom/vdurmont/emoji/EmojiTrie$Node;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/vdurmont/emoji/EmojiTrie$Matches;->EXACTLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/vdurmont/emoji/EmojiTrie$Matches;->POSSIBLY:Lcom/vdurmont/emoji/EmojiTrie$Matches;

    goto :goto_0
.end method
