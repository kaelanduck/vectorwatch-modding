.class public final Lio/realm/RealmConfiguration;
.super Ljava/lang/Object;
.source "RealmConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RealmConfiguration$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULT_MODULE:Ljava/lang/Object;

.field private static final DEFAULT_MODULE_MEDIATOR:Lio/realm/internal/RealmProxyMediator;

.field public static final DEFAULT_REALM_NAME:Ljava/lang/String; = "default.realm"

.field public static final KEY_LENGTH:I = 0x40

.field private static rxJavaAvailable:Ljava/lang/Boolean;


# instance fields
.field private final assetFilePath:Ljava/lang/String;

.field private final canonicalPath:Ljava/lang/String;

.field private final contextWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final deleteRealmIfMigrationNeeded:Z

.field private final durability:Lio/realm/internal/SharedGroup$Durability;

.field private final initialDataTransaction:Lio/realm/Realm$Transaction;

.field private final key:[B

.field private final migration:Lio/realm/RealmMigration;

.field private final realmFileName:Ljava/lang/String;

.field private final realmFolder:Ljava/io/File;

.field private final rxObservableFactory:Lio/realm/rx/RxObservableFactory;

.field private final schemaMediator:Lio/realm/internal/RealmProxyMediator;

.field private final schemaVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    invoke-static {}, Lio/realm/Realm;->getDefaultModule()Ljava/lang/Object;

    move-result-object v1

    sput-object v1, Lio/realm/RealmConfiguration;->DEFAULT_MODULE:Ljava/lang/Object;

    .line 74
    sget-object v1, Lio/realm/RealmConfiguration;->DEFAULT_MODULE:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 75
    sget-object v1, Lio/realm/RealmConfiguration;->DEFAULT_MODULE:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/realm/RealmConfiguration;->getModuleMediator(Ljava/lang/String;)Lio/realm/internal/RealmProxyMediator;

    move-result-object v0

    .line 76
    .local v0, "mediator":Lio/realm/internal/RealmProxyMediator;
    invoke-virtual {v0}, Lio/realm/internal/RealmProxyMediator;->transformerApplied()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    new-instance v1, Ljava/lang/ExceptionInInitializerError;

    const-string v2, "RealmTransformer doesn\'t seem to be applied. Please update the project configuration to use the Realm Gradle plugin. See https://realm.io/news/android-installation-change/"

    invoke-direct {v1, v2}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/String;)V

    throw v1

    .line 81
    :cond_0
    sput-object v0, Lio/realm/RealmConfiguration;->DEFAULT_MODULE_MEDIATOR:Lio/realm/internal/RealmProxyMediator;

    .line 85
    :goto_0
    return-void

    .line 83
    .end local v0    # "mediator":Lio/realm/internal/RealmProxyMediator;
    :cond_1
    const/4 v1, 0x0

    sput-object v1, Lio/realm/RealmConfiguration;->DEFAULT_MODULE_MEDIATOR:Lio/realm/internal/RealmProxyMediator;

    goto :goto_0
.end method

.method private constructor <init>(Lio/realm/RealmConfiguration$Builder;)V
    .locals 3
    .param p1, "builder"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    # getter for: Lio/realm/RealmConfiguration$Builder;->folder:Ljava/io/File;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$000(Lio/realm/RealmConfiguration$Builder;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    .line 103
    # getter for: Lio/realm/RealmConfiguration$Builder;->fileName:Ljava/lang/String;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$100(Lio/realm/RealmConfiguration$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    iget-object v2, p0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lio/realm/Realm;->getCanonicalPath(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->canonicalPath:Ljava/lang/String;

    .line 105
    # getter for: Lio/realm/RealmConfiguration$Builder;->assetFilePath:Ljava/lang/String;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$200(Lio/realm/RealmConfiguration$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->assetFilePath:Ljava/lang/String;

    .line 106
    # getter for: Lio/realm/RealmConfiguration$Builder;->key:[B
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$300(Lio/realm/RealmConfiguration$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->key:[B

    .line 107
    # getter for: Lio/realm/RealmConfiguration$Builder;->schemaVersion:J
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$400(Lio/realm/RealmConfiguration$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/RealmConfiguration;->schemaVersion:J

    .line 108
    # getter for: Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded:Z
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$500(Lio/realm/RealmConfiguration$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lio/realm/RealmConfiguration;->deleteRealmIfMigrationNeeded:Z

    .line 109
    # getter for: Lio/realm/RealmConfiguration$Builder;->migration:Lio/realm/RealmMigration;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$600(Lio/realm/RealmConfiguration$Builder;)Lio/realm/RealmMigration;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    .line 110
    # getter for: Lio/realm/RealmConfiguration$Builder;->durability:Lio/realm/internal/SharedGroup$Durability;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$700(Lio/realm/RealmConfiguration$Builder;)Lio/realm/internal/SharedGroup$Durability;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->durability:Lio/realm/internal/SharedGroup$Durability;

    .line 111
    invoke-direct {p0, p1}, Lio/realm/RealmConfiguration;->createSchemaMediator(Lio/realm/RealmConfiguration$Builder;)Lio/realm/internal/RealmProxyMediator;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    .line 112
    # getter for: Lio/realm/RealmConfiguration$Builder;->rxFactory:Lio/realm/rx/RxObservableFactory;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$800(Lio/realm/RealmConfiguration$Builder;)Lio/realm/rx/RxObservableFactory;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    .line 113
    # getter for: Lio/realm/RealmConfiguration$Builder;->initialDataTransaction:Lio/realm/Realm$Transaction;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$900(Lio/realm/RealmConfiguration$Builder;)Lio/realm/Realm$Transaction;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    .line 114
    # getter for: Lio/realm/RealmConfiguration$Builder;->contextWeakRef:Ljava/lang/ref/WeakReference;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$1000(Lio/realm/RealmConfiguration$Builder;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration;->contextWeakRef:Ljava/lang/ref/WeakReference;

    .line 115
    return-void
.end method

.method synthetic constructor <init>(Lio/realm/RealmConfiguration$Builder;Lio/realm/RealmConfiguration$1;)V
    .locals 0
    .param p1, "x0"    # Lio/realm/RealmConfiguration$Builder;
    .param p2, "x1"    # Lio/realm/RealmConfiguration$1;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lio/realm/RealmConfiguration;-><init>(Lio/realm/RealmConfiguration$Builder;)V

    return-void
.end method

.method static synthetic access$1300()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lio/realm/RealmConfiguration;->DEFAULT_MODULE:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1400()Lio/realm/internal/RealmProxyMediator;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lio/realm/RealmConfiguration;->DEFAULT_MODULE_MEDIATOR:Lio/realm/internal/RealmProxyMediator;

    return-object v0
.end method

.method static synthetic access$1500()Z
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lio/realm/RealmConfiguration;->isRxJavaAvailable()Z

    move-result v0

    return v0
.end method

.method private createSchemaMediator(Lio/realm/RealmConfiguration$Builder;)Lio/realm/internal/RealmProxyMediator;
    .locals 7
    .param p1, "builder"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 257
    # getter for: Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$1100(Lio/realm/RealmConfiguration$Builder;)Ljava/util/HashSet;

    move-result-object v4

    .line 258
    .local v4, "modules":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Object;>;"
    # getter for: Lio/realm/RealmConfiguration$Builder;->debugSchema:Ljava/util/HashSet;
    invoke-static {p1}, Lio/realm/RealmConfiguration$Builder;->access$1200(Lio/realm/RealmConfiguration$Builder;)Ljava/util/HashSet;

    move-result-object v0

    .line 261
    .local v0, "debugSchema":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 262
    new-instance v5, Lio/realm/internal/modules/FilterableMediator;

    sget-object v6, Lio/realm/RealmConfiguration;->DEFAULT_MODULE_MEDIATOR:Lio/realm/internal/RealmProxyMediator;

    invoke-direct {v5, v6, v0}, Lio/realm/internal/modules/FilterableMediator;-><init>(Lio/realm/internal/RealmProxyMediator;Ljava/util/Collection;)V

    .line 277
    :goto_0
    return-object v5

    .line 266
    :cond_0
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 267
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lio/realm/RealmConfiguration;->getModuleMediator(Ljava/lang/String;)Lio/realm/internal/RealmProxyMediator;

    move-result-object v5

    goto :goto_0

    .line 271
    :cond_1
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v2, v5, [Lio/realm/internal/RealmProxyMediator;

    .line 272
    .local v2, "mediators":[Lio/realm/internal/RealmProxyMediator;
    const/4 v1, 0x0

    .line 273
    .local v1, "i":I
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 274
    .local v3, "module":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lio/realm/RealmConfiguration;->getModuleMediator(Ljava/lang/String;)Lio/realm/internal/RealmProxyMediator;

    move-result-object v6

    aput-object v6, v2, v1

    .line 275
    add-int/lit8 v1, v1, 0x1

    .line 276
    goto :goto_1

    .line 277
    .end local v3    # "module":Ljava/lang/Object;
    :cond_2
    new-instance v5, Lio/realm/internal/modules/CompositeMediator;

    invoke-direct {v5, v2}, Lio/realm/internal/modules/CompositeMediator;-><init>([Lio/realm/internal/RealmProxyMediator;)V

    goto :goto_0
.end method

.method private static getModuleMediator(Ljava/lang/String;)Lio/realm/internal/RealmProxyMediator;
    .locals 10
    .param p0, "fullyQualifiedModuleClassName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 282
    const-string v6, "\\."

    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 283
    .local v4, "moduleNameParts":[Ljava/lang/String;
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v4, v6

    .line 284
    .local v5, "moduleSimpleName":Ljava/lang/String;
    const-string v6, "io.realm.%s%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v5, v7, v8

    const-string v8, "Mediator"

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 288
    .local v3, "mediatorName":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 289
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v1, v6, v7

    .line 290
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 291
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/realm/internal/RealmProxyMediator;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v6

    .line 292
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :catch_0
    move-exception v2

    .line 293
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    new-instance v6, Lio/realm/exceptions/RealmException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not find "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 294
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v2

    .line 295
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v6, Lio/realm/exceptions/RealmException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not create an instance of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 296
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v2

    .line 297
    .local v2, "e":Ljava/lang/InstantiationException;
    new-instance v6, Lio/realm/exceptions/RealmException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not create an instance of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 298
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_3
    move-exception v2

    .line 299
    .local v2, "e":Ljava/lang/IllegalAccessException;
    new-instance v6, Lio/realm/exceptions/RealmException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not create an instance of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method

.method private static declared-synchronized isRxJavaAvailable()Z
    .locals 3

    .prologue
    .line 334
    const-class v2, Lio/realm/RealmConfiguration;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lio/realm/RealmConfiguration;->rxJavaAvailable:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 336
    :try_start_1
    const-string v1, "rx.Observable"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 337
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lio/realm/RealmConfiguration;->rxJavaAvailable:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342
    .local v0, "ignore":Ljava/lang/ClassNotFoundException;
    :cond_0
    :goto_0
    :try_start_2
    sget-object v1, Lio/realm/RealmConfiguration;->rxJavaAvailable:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 338
    .end local v0    # "ignore":Ljava/lang/ClassNotFoundException;
    :catch_0
    move-exception v0

    .line 339
    .restart local v0    # "ignore":Ljava/lang/ClassNotFoundException;
    const/4 v1, 0x0

    :try_start_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lio/realm/RealmConfiguration;->rxJavaAvailable:Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 334
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 218
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 234
    :cond_0
    :goto_0
    return v1

    .line 219
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 221
    check-cast v0, Lio/realm/RealmConfiguration;

    .line 223
    .local v0, "that":Lio/realm/RealmConfiguration;
    iget-wide v2, p0, Lio/realm/RealmConfiguration;->schemaVersion:J

    iget-wide v4, v0, Lio/realm/RealmConfiguration;->schemaVersion:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 224
    iget-boolean v2, p0, Lio/realm/RealmConfiguration;->deleteRealmIfMigrationNeeded:Z

    iget-boolean v3, v0, Lio/realm/RealmConfiguration;->deleteRealmIfMigrationNeeded:Z

    if-ne v2, v3, :cond_0

    .line 225
    iget-object v2, p0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 226
    iget-object v2, p0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 227
    iget-object v2, p0, Lio/realm/RealmConfiguration;->canonicalPath:Ljava/lang/String;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->canonicalPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228
    iget-object v2, p0, Lio/realm/RealmConfiguration;->key:[B

    iget-object v3, v0, Lio/realm/RealmConfiguration;->key:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, p0, Lio/realm/RealmConfiguration;->durability:Lio/realm/internal/SharedGroup$Durability;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->durability:Lio/realm/internal/SharedGroup$Durability;

    invoke-virtual {v2, v3}, Lio/realm/internal/SharedGroup$Durability;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    iget-object v2, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    :cond_2
    iget-object v2, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    :cond_3
    iget-object v2, p0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    iget-object v3, v0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    :cond_4
    iget-object v1, p0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    iget-object v2, v0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    invoke-virtual {v1, v2}, Lio/realm/internal/RealmProxyMediator;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_0

    .line 230
    :cond_5
    iget-object v2, v0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 232
    :cond_6
    iget-object v2, v0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 233
    :cond_7
    iget-object v2, v0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    if-eqz v2, :cond_4

    goto/16 :goto_0
.end method

.method getAssetFile()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    iget-object v1, p0, Lio/realm/RealmConfiguration;->contextWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 180
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->assetFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    return-object v1

    .line 183
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Context should not be null. Use Application Context instead of Activity Context."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getDurability()Lio/realm/internal/SharedGroup$Durability;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lio/realm/RealmConfiguration;->durability:Lio/realm/internal/SharedGroup$Durability;

    return-object v0
.end method

.method public getEncryptionKey()[B
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lio/realm/RealmConfiguration;->key:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/RealmConfiguration;->key:[B

    iget-object v1, p0, Lio/realm/RealmConfiguration;->key:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    goto :goto_0
.end method

.method getInitialDataTransaction()Lio/realm/Realm$Transaction;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    return-object v0
.end method

.method public getMigration()Lio/realm/RealmMigration;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lio/realm/RealmConfiguration;->canonicalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getRealmFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getRealmFolder()Ljava/io/File;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    return-object v0
.end method

.method public getRealmObjectClasses()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    invoke-virtual {v0}, Lio/realm/internal/RealmProxyMediator;->getModelClasses()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getRxFactory()Lio/realm/rx/RxObservableFactory;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RxJava seems to be missing from the classpath. Remember to add it as a compile dependency. See https://realm.io/docs/java/latest/#rxjava for more details."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget-object v0, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    return-object v0
.end method

.method getSchemaMediator()Lio/realm/internal/RealmProxyMediator;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    return-object v0
.end method

.method public getSchemaVersion()J
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lio/realm/RealmConfiguration;->schemaVersion:J

    return-wide v0
.end method

.method hasAssetFile()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lio/realm/RealmConfiguration;->assetFilePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 239
    iget-object v1, p0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->hashCode()I

    move-result v0

    .line 240
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int v0, v1, v3

    .line 241
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lio/realm/RealmConfiguration;->canonicalPath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int v0, v1, v3

    .line 242
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lio/realm/RealmConfiguration;->key:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lio/realm/RealmConfiguration;->key:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 243
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lio/realm/RealmConfiguration;->schemaVersion:J

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 244
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    .line 245
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v1, p0, Lio/realm/RealmConfiguration;->deleteRealmIfMigrationNeeded:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    add-int v0, v3, v1

    .line 246
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    invoke-virtual {v3}, Lio/realm/internal/RealmProxyMediator;->hashCode()I

    move-result v3

    add-int v0, v1, v3

    .line 247
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lio/realm/RealmConfiguration;->durability:Lio/realm/internal/SharedGroup$Durability;

    invoke-virtual {v3}, Lio/realm/internal/SharedGroup$Durability;->hashCode()I

    move-result v3

    add-int v0, v1, v3

    .line 248
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lio/realm/RealmConfiguration;->rxObservableFactory:Lio/realm/rx/RxObservableFactory;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_3
    add-int v0, v3, v1

    .line 249
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lio/realm/RealmConfiguration;->initialDataTransaction:Lio/realm/Realm$Transaction;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 251
    return v0

    :cond_1
    move v1, v2

    .line 242
    goto :goto_0

    :cond_2
    move v1, v2

    .line 244
    goto :goto_1

    :cond_3
    move v1, v2

    .line 245
    goto :goto_2

    :cond_4
    move v1, v2

    .line 248
    goto :goto_3
.end method

.method public shouldDeleteRealmIfMigrationNeeded()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lio/realm/RealmConfiguration;->deleteRealmIfMigrationNeeded:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 307
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "realmFolder: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->realmFolder:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const-string v1, "realmFileName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->realmFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string v1, "canonicalPath: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->canonicalPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string v1, "key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lio/realm/RealmConfiguration;->key:[B

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string v1, "schemaVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lio/realm/RealmConfiguration;->schemaVersion:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string v1, "migration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->migration:Lio/realm/RealmMigration;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 318
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string v1, "deleteRealmIfMigrationNeeded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lio/realm/RealmConfiguration;->deleteRealmIfMigrationNeeded:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 320
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const-string v1, "durability: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->durability:Lio/realm/internal/SharedGroup$Durability;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 322
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    const-string v1, "schemaMediator: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmConfiguration;->schemaMediator:Lio/realm/internal/RealmProxyMediator;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 325
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 313
    :cond_0
    const/16 v1, 0x40

    goto :goto_0
.end method
