.class public interface abstract Lio/realm/CloudActivityDayRealmProxyInterface;
.super Ljava/lang/Object;
.source "CloudActivityDayRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$activityType()Ljava/lang/String;
.end method

.method public abstract realmGet$avgAmpl()I
.end method

.method public abstract realmGet$avgPeriod()I
.end method

.method public abstract realmGet$cal()I
.end method

.method public abstract realmGet$dirtyCalories()Z
.end method

.method public abstract realmGet$dirtyCloud()Z
.end method

.method public abstract realmGet$dirtyDistance()Z
.end method

.method public abstract realmGet$dirtySteps()Z
.end method

.method public abstract realmGet$dist()I
.end method

.method public abstract realmGet$effTime()I
.end method

.method public abstract realmGet$offset()I
.end method

.method public abstract realmGet$timestamp()J
.end method

.method public abstract realmGet$timezone()I
.end method

.method public abstract realmGet$val()I
.end method

.method public abstract realmSet$activityType(Ljava/lang/String;)V
.end method

.method public abstract realmSet$avgAmpl(I)V
.end method

.method public abstract realmSet$avgPeriod(I)V
.end method

.method public abstract realmSet$cal(I)V
.end method

.method public abstract realmSet$dirtyCalories(Z)V
.end method

.method public abstract realmSet$dirtyCloud(Z)V
.end method

.method public abstract realmSet$dirtyDistance(Z)V
.end method

.method public abstract realmSet$dirtySteps(Z)V
.end method

.method public abstract realmSet$dist(I)V
.end method

.method public abstract realmSet$effTime(I)V
.end method

.method public abstract realmSet$offset(I)V
.end method

.method public abstract realmSet$timestamp(J)V
.end method

.method public abstract realmSet$timezone(I)V
.end method

.method public abstract realmSet$val(I)V
.end method
