.class public final Lio/realm/RealmConfiguration$Builder;
.super Ljava/lang/Object;
.source "RealmConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/RealmConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private assetFilePath:Ljava/lang/String;

.field private contextWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private debugSchema:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private deleteRealmIfMigrationNeeded:Z

.field private durability:Lio/realm/internal/SharedGroup$Durability;

.field private fileName:Ljava/lang/String;

.field private folder:Ljava/io/File;

.field private initialDataTransaction:Lio/realm/Realm$Transaction;

.field private key:[B

.field private migration:Lio/realm/RealmMigration;

.field private modules:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private rxFactory:Lio/realm/rx/RxObservableFactory;

.field private schemaVersion:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    .line 358
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->debugSchema:Ljava/util/HashSet;

    .line 385
    if-nez p1, :cond_0

    .line 386
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null Context must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388
    :cond_0
    invoke-static {p1}, Lio/realm/internal/RealmCore;->loadLibrary(Landroid/content/Context;)V

    .line 389
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/RealmConfiguration$Builder;->initializeBuilder(Ljava/io/File;)V

    .line 390
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folder"    # Ljava/io/File;

    .prologue
    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    .line 358
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->debugSchema:Ljava/util/HashSet;

    .line 371
    invoke-static {}, Lio/realm/internal/RealmCore;->loadLibrary()V

    .line 372
    invoke-direct {p0, p1}, Lio/realm/RealmConfiguration$Builder;->initializeBuilder(Ljava/io/File;)V

    .line 373
    return-void
.end method

.method static synthetic access$000(Lio/realm/RealmConfiguration$Builder;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->folder:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Lio/realm/RealmConfiguration$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lio/realm/RealmConfiguration$Builder;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->contextWeakRef:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$1100(Lio/realm/RealmConfiguration$Builder;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$1200(Lio/realm/RealmConfiguration$Builder;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->debugSchema:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$200(Lio/realm/RealmConfiguration$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->assetFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lio/realm/RealmConfiguration$Builder;)[B
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->key:[B

    return-object v0
.end method

.method static synthetic access$400(Lio/realm/RealmConfiguration$Builder;)J
    .locals 2
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-wide v0, p0, Lio/realm/RealmConfiguration$Builder;->schemaVersion:J

    return-wide v0
.end method

.method static synthetic access$500(Lio/realm/RealmConfiguration$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-boolean v0, p0, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded:Z

    return v0
.end method

.method static synthetic access$600(Lio/realm/RealmConfiguration$Builder;)Lio/realm/RealmMigration;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->migration:Lio/realm/RealmMigration;

    return-object v0
.end method

.method static synthetic access$700(Lio/realm/RealmConfiguration$Builder;)Lio/realm/internal/SharedGroup$Durability;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->durability:Lio/realm/internal/SharedGroup$Durability;

    return-object v0
.end method

.method static synthetic access$800(Lio/realm/RealmConfiguration$Builder;)Lio/realm/rx/RxObservableFactory;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->rxFactory:Lio/realm/rx/RxObservableFactory;

    return-object v0
.end method

.method static synthetic access$900(Lio/realm/RealmConfiguration$Builder;)Lio/realm/Realm$Transaction;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmConfiguration$Builder;

    .prologue
    .line 348
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->initialDataTransaction:Lio/realm/Realm$Transaction;

    return-object v0
.end method

.method private addModule(Ljava/lang/Object;)V
    .locals 1
    .param p1, "module"    # Ljava/lang/Object;

    .prologue
    .line 579
    if-eqz p1, :cond_0

    .line 580
    invoke-direct {p0, p1}, Lio/realm/RealmConfiguration$Builder;->checkModule(Ljava/lang/Object;)V

    .line 581
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 583
    :cond_0
    return-void
.end method

.method private checkModule(Ljava/lang/Object;)V
    .locals 3
    .param p1, "module"    # Ljava/lang/Object;

    .prologue
    .line 617
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lio/realm/annotations/RealmModule;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a RealmModule. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Add @RealmModule to the class definition."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621
    :cond_0
    return-void
.end method

.method private initializeBuilder(Ljava/io/File;)V
    .locals 3
    .param p1, "folder"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x0

    .line 394
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    .line 395
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "An existing folder must be provided. Yours was "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    .line 396
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const-string v0, "null"

    goto :goto_0

    .line 398
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-nez v0, :cond_3

    .line 399
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Folder is not writable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_3
    iput-object p1, p0, Lio/realm/RealmConfiguration$Builder;->folder:Ljava/io/File;

    .line 403
    const-string v0, "default.realm"

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->fileName:Ljava/lang/String;

    .line 404
    iput-object v2, p0, Lio/realm/RealmConfiguration$Builder;->key:[B

    .line 405
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lio/realm/RealmConfiguration$Builder;->schemaVersion:J

    .line 406
    iput-object v2, p0, Lio/realm/RealmConfiguration$Builder;->migration:Lio/realm/RealmMigration;

    .line 407
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded:Z

    .line 408
    sget-object v0, Lio/realm/internal/SharedGroup$Durability;->FULL:Lio/realm/internal/SharedGroup$Durability;

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->durability:Lio/realm/internal/SharedGroup$Durability;

    .line 409
    # getter for: Lio/realm/RealmConfiguration;->DEFAULT_MODULE:Ljava/lang/Object;
    invoke-static {}, Lio/realm/RealmConfiguration;->access$1300()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 410
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    # getter for: Lio/realm/RealmConfiguration;->DEFAULT_MODULE:Ljava/lang/Object;
    invoke-static {}, Lio/realm/RealmConfiguration;->access$1300()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 412
    :cond_4
    return-void
.end method


# virtual methods
.method public assetFile(Landroid/content/Context;Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "assetFile"    # Ljava/lang/String;

    .prologue
    .line 562
    if-nez p1, :cond_0

    .line 563
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null Context must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 566
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-empty asset file path must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_1
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->durability:Lio/realm/internal/SharedGroup$Durability;

    sget-object v1, Lio/realm/internal/SharedGroup$Durability;->MEM_ONLY:Lio/realm/internal/SharedGroup$Durability;

    if-ne v0, v1, :cond_2

    .line 569
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Realm can not use in-memory configuration if asset file is present."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->contextWeakRef:Ljava/lang/ref/WeakReference;

    .line 573
    iput-object p2, p0, Lio/realm/RealmConfiguration$Builder;->assetFilePath:Ljava/lang/String;

    .line 575
    return-object p0
.end method

.method public build()Lio/realm/RealmConfiguration;
    .locals 2

    .prologue
    .line 610
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->rxFactory:Lio/realm/rx/RxObservableFactory;

    if-nez v0, :cond_0

    # invokes: Lio/realm/RealmConfiguration;->isRxJavaAvailable()Z
    invoke-static {}, Lio/realm/RealmConfiguration;->access$1500()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    new-instance v0, Lio/realm/rx/RealmObservableFactory;

    invoke-direct {v0}, Lio/realm/rx/RealmObservableFactory;-><init>()V

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->rxFactory:Lio/realm/rx/RxObservableFactory;

    .line 613
    :cond_0
    new-instance v0, Lio/realm/RealmConfiguration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/realm/RealmConfiguration;-><init>(Lio/realm/RealmConfiguration$Builder;Lio/realm/RealmConfiguration$1;)V

    return-object v0
.end method

.method public deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded:Z

    .line 480
    return-object p0
.end method

.method public encryptionKey([B)Lio/realm/RealmConfiguration$Builder;
    .locals 5
    .param p1, "key"    # [B

    .prologue
    const/16 v4, 0x40

    .line 430
    if-nez p1, :cond_0

    .line 431
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null key must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 433
    :cond_0
    array-length v0, p1

    if-eq v0, v4, :cond_1

    .line 434
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The provided key must be %s bytes. Yours was: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 435
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 434
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :cond_1
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->key:[B

    .line 438
    return-object p0
.end method

.method public inMemory()Lio/realm/RealmConfiguration$Builder;
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->assetFilePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 493
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Realm can not use in-memory configuration if asset file is present."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 496
    :cond_0
    sget-object v0, Lio/realm/internal/SharedGroup$Durability;->MEM_ONLY:Lio/realm/internal/SharedGroup$Durability;

    iput-object v0, p0, Lio/realm/RealmConfiguration$Builder;->durability:Lio/realm/internal/SharedGroup$Durability;

    .line 498
    return-object p0
.end method

.method public initialData(Lio/realm/Realm$Transaction;)Lio/realm/RealmConfiguration$Builder;
    .locals 0
    .param p1, "transaction"    # Lio/realm/Realm$Transaction;

    .prologue
    .line 546
    iput-object p1, p0, Lio/realm/RealmConfiguration$Builder;->initialDataTransaction:Lio/realm/Realm$Transaction;

    .line 547
    return-object p0
.end method

.method public migration(Lio/realm/RealmMigration;)Lio/realm/RealmConfiguration$Builder;
    .locals 2
    .param p1, "migration"    # Lio/realm/RealmMigration;

    .prologue
    .line 464
    if-nez p1, :cond_0

    .line 465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null migration must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_0
    iput-object p1, p0, Lio/realm/RealmConfiguration$Builder;->migration:Lio/realm/RealmMigration;

    .line 468
    return-object p0
.end method

.method public varargs modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;
    .locals 3
    .param p1, "baseModule"    # Ljava/lang/Object;
    .param p2, "additionalModules"    # [Ljava/lang/Object;

    .prologue
    .line 517
    iget-object v2, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 518
    invoke-direct {p0, p1}, Lio/realm/RealmConfiguration$Builder;->addModule(Ljava/lang/Object;)V

    .line 519
    if-eqz p2, :cond_0

    .line 520
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 521
    aget-object v1, p2, v0

    .line 522
    .local v1, "module":Ljava/lang/Object;
    invoke-direct {p0, v1}, Lio/realm/RealmConfiguration$Builder;->addModule(Ljava/lang/Object;)V

    .line 520
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 525
    .end local v0    # "i":I
    .end local v1    # "module":Ljava/lang/Object;
    :cond_0
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 418
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-empty filename must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_1
    iput-object p1, p0, Lio/realm/RealmConfiguration$Builder;->fileName:Ljava/lang/String;

    .line 423
    return-object p0
.end method

.method public rxFactory(Lio/realm/rx/RxObservableFactory;)Lio/realm/RealmConfiguration$Builder;
    .locals 0
    .param p1, "factory"    # Lio/realm/rx/RxObservableFactory;

    .prologue
    .line 535
    iput-object p1, p0, Lio/realm/RealmConfiguration$Builder;->rxFactory:Lio/realm/rx/RxObservableFactory;

    .line 536
    return-object p0
.end method

.method varargs schema(Ljava/lang/Class;[Ljava/lang/Class;)Lio/realm/RealmConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;[",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Lio/realm/RealmConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 591
    .local p1, "firstClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    .local p2, "additionalClasses":[Ljava/lang/Class;, "[Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    if-nez p1, :cond_0

    .line 592
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null class must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_0
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 595
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->modules:Ljava/util/HashSet;

    # getter for: Lio/realm/RealmConfiguration;->DEFAULT_MODULE_MEDIATOR:Lio/realm/internal/RealmProxyMediator;
    invoke-static {}, Lio/realm/RealmConfiguration;->access$1400()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 596
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->debugSchema:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 597
    if-eqz p2, :cond_1

    .line 598
    iget-object v0, p0, Lio/realm/RealmConfiguration$Builder;->debugSchema:Ljava/util/HashSet;

    invoke-static {v0, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 601
    :cond_1
    return-object p0
.end method

.method public schemaVersion(J)Lio/realm/RealmConfiguration$Builder;
    .locals 3
    .param p1, "schemaVersion"    # J

    .prologue
    .line 451
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 452
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Realm schema version numbers must be 0 (zero) or higher. Yours was: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_0
    iput-wide p1, p0, Lio/realm/RealmConfiguration$Builder;->schemaVersion:J

    .line 455
    return-object p0
.end method
