.class Lio/realm/Realm$1$4;
.super Ljava/lang/Object;
.source "Realm.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/Realm$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lio/realm/Realm$1;

.field final synthetic val$backgroundException:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "this$1"    # Lio/realm/Realm$1;

    .prologue
    .line 1111
    iput-object p1, p0, Lio/realm/Realm$1$4;->this$1:Lio/realm/Realm$1;

    iput-object p2, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1114
    iget-object v0, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 1115
    iget-object v0, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 1116
    :cond_0
    iget-object v0, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 1117
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Async transaction failed"

    iget-object v2, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 1118
    :cond_1
    iget-object v0, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/lang/Error;

    if-eqz v0, :cond_2

    .line 1119
    iget-object v0, p0, Lio/realm/Realm$1$4;->val$backgroundException:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 1121
    :cond_2
    return-void
.end method
