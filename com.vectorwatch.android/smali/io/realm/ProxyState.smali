.class public final Lio/realm/ProxyState;
.super Ljava/lang/Object;
.source "ProxyState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private className:Ljava/lang/String;

.field private clazzName:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;"
        }
    .end annotation
.end field

.field protected currentTableVersion:J

.field private isCompleted:Z

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/RealmChangeListener",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private model:Lio/realm/RealmModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private pendingQuery:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private realm:Lio/realm/BaseRealm;

.field private row:Lio/realm/internal/Row;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/ProxyState;->listeners:Ljava/util/List;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/ProxyState;->isCompleted:Z

    .line 44
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/ProxyState;->currentTableVersion:J

    .line 46
    return-void
.end method

.method public constructor <init>(Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    .local p1, "model":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/ProxyState;->listeners:Ljava/util/List;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/ProxyState;->isCompleted:Z

    .line 44
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/ProxyState;->currentTableVersion:J

    .line 49
    iput-object p1, p0, Lio/realm/ProxyState;->model:Lio/realm/RealmModel;

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    .local p1, "clazzName":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    .local p2, "model":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/ProxyState;->listeners:Ljava/util/List;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/ProxyState;->isCompleted:Z

    .line 44
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/ProxyState;->currentTableVersion:J

    .line 53
    iput-object p1, p0, Lio/realm/ProxyState;->clazzName:Ljava/lang/Class;

    .line 54
    iput-object p2, p0, Lio/realm/ProxyState;->model:Lio/realm/RealmModel;

    .line 55
    return-void
.end method

.method private getTable()Lio/realm/internal/Table;
    .locals 2

    .prologue
    .line 182
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->className:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    iget-object v0, v0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    iget-object v1, p0, Lio/realm/ProxyState;->className:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    iget-object v0, v0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    iget-object v1, p0, Lio/realm/ProxyState;->clazzName:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method private isLoaded()Z
    .locals 1

    .prologue
    .line 189
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 190
    invoke-virtual {p0}, Lio/realm/ProxyState;->getPendingQuery$realm()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lio/realm/ProxyState;->isCompleted$realm()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getListeners$realm()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lio/realm/RealmChangeListener",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 124
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->listeners:Ljava/util/List;

    return-object v0
.end method

.method public getPendingQuery$realm()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->pendingQuery:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getRealm$realm()Lio/realm/BaseRealm;
    .locals 1

    .prologue
    .line 75
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->realm:Lio/realm/BaseRealm;

    return-object v0
.end method

.method public getRow$realm()Lio/realm/internal/Row;
    .locals 1

    .prologue
    .line 83
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    return-object v0
.end method

.method public isCompleted$realm()Z
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-boolean v0, p0, Lio/realm/ProxyState;->isCompleted:Z

    return v0
.end method

.method notifyChangeListeners$realm()V
    .locals 8

    .prologue
    .line 145
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v3, p0, Lio/realm/ProxyState;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 146
    const/4 v1, 0x0

    .line 148
    .local v1, "notify":Z
    iget-object v3, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    invoke-interface {v3}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    .line 149
    .local v2, "table":Lio/realm/internal/Table;
    if-nez v2, :cond_1

    .line 154
    const/4 v1, 0x1

    .line 163
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    .line 164
    iget-object v3, p0, Lio/realm/ProxyState;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmChangeListener;

    .line 165
    .local v0, "listener":Lio/realm/RealmChangeListener;
    iget-object v6, p0, Lio/realm/ProxyState;->model:Lio/realm/RealmModel;

    invoke-interface {v0, v6}, Lio/realm/RealmChangeListener;->onChange(Ljava/lang/Object;)V

    goto :goto_1

    .line 156
    .end local v0    # "listener":Lio/realm/RealmChangeListener;
    :cond_1
    invoke-virtual {v2}, Lio/realm/internal/Table;->getVersion()J

    move-result-wide v4

    .line 157
    .local v4, "version":J
    iget-wide v6, p0, Lio/realm/ProxyState;->currentTableVersion:J

    cmp-long v3, v6, v4

    if-eqz v3, :cond_0

    .line 158
    iput-wide v4, p0, Lio/realm/ProxyState;->currentTableVersion:J

    .line 159
    const/4 v1, 0x1

    goto :goto_0

    .line 169
    .end local v1    # "notify":Z
    .end local v2    # "table":Lio/realm/internal/Table;
    .end local v4    # "version":J
    :cond_2
    return-void
.end method

.method public onCompleted$realm(J)V
    .locals 7
    .param p1, "handoverRowPointer"    # J

    .prologue
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    const/4 v6, 0x1

    .line 128
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-nez v3, :cond_1

    .line 131
    iput-boolean v6, p0, Lio/realm/ProxyState;->isCompleted:Z

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-boolean v3, p0, Lio/realm/ProxyState;->isCompleted:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    sget-object v4, Lio/realm/internal/Row;->EMPTY_ROW:Lio/realm/internal/Row;

    if-ne v3, v4, :cond_0

    .line 134
    :cond_2
    iput-boolean v6, p0, Lio/realm/ProxyState;->isCompleted:Z

    .line 135
    iget-object v3, p0, Lio/realm/ProxyState;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v3}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v4

    invoke-static {p1, p2, v4, v5}, Lio/realm/internal/TableQuery;->nativeImportHandoverRowIntoSharedGroup(JJ)J

    move-result-wide v0

    .line 136
    .local v0, "nativeRowPointer":J
    invoke-direct {p0}, Lio/realm/ProxyState;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    .line 137
    .local v2, "table":Lio/realm/internal/Table;
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->getUncheckedRowByPointer(J)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    iput-object v3, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    goto :goto_0
.end method

.method public onCompleted$realm()Z
    .locals 8

    .prologue
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    const/4 v2, 0x1

    .line 105
    :try_start_0
    iget-object v3, p0, Lio/realm/ProxyState;->pendingQuery:Ljava/util/concurrent/Future;

    invoke-interface {v3}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 106
    .local v1, "handoverResult":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 111
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lio/realm/ProxyState;->onCompleted$realm(J)V

    .line 112
    invoke-virtual {p0}, Lio/realm/ProxyState;->notifyChangeListeners$realm()V

    .line 120
    .end local v1    # "handoverResult":Ljava/lang/Long;
    :goto_0
    return v2

    .line 114
    .restart local v1    # "handoverResult":Ljava/lang/Long;
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lio/realm/ProxyState;->isCompleted:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 116
    .end local v1    # "handoverResult":Ljava/lang/Long;
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 118
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 178
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iput-object p1, p0, Lio/realm/ProxyState;->className:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setPendingQuery$realm(Ljava/util/concurrent/Future;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    .local p1, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    iput-object p1, p0, Lio/realm/ProxyState;->pendingQuery:Ljava/util/concurrent/Future;

    .line 65
    invoke-direct {p0}, Lio/realm/ProxyState;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lio/realm/ProxyState;->onCompleted$realm()Z

    .line 72
    :cond_0
    return-void
.end method

.method public setRealm$realm(Lio/realm/BaseRealm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/BaseRealm;

    .prologue
    .line 79
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iput-object p1, p0, Lio/realm/ProxyState;->realm:Lio/realm/BaseRealm;

    .line 80
    return-void
.end method

.method public setRow$realm(Lio/realm/internal/Row;)V
    .locals 0
    .param p1, "row"    # Lio/realm/internal/Row;

    .prologue
    .line 87
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iput-object p1, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    .line 88
    return-void
.end method

.method public setTableVersion$realm()V
    .locals 2

    .prologue
    .line 172
    .local p0, "this":Lio/realm/ProxyState;, "Lio/realm/ProxyState<TE;>;"
    iget-object v0, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lio/realm/ProxyState;->row:Lio/realm/internal/Row;

    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->getVersion()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/ProxyState;->currentTableVersion:J

    .line 175
    :cond_0
    return-void
.end method
