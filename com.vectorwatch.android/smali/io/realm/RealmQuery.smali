.class public final Lio/realm/RealmQuery;
.super Ljava/lang/Object;
.source "RealmQuery.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final INVALID_NATIVE_POINTER:Ljava/lang/Long;

.field private static final TYPE_MISMATCH:Ljava/lang/String; = "Field \'%s\': type mismatch - %s expected."


# instance fields
.field private argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

.field private className:Ljava/lang/String;

.field private clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field private query:Lio/realm/internal/TableQuery;

.field private realm:Lio/realm/BaseRealm;

.field private schema:Lio/realm/RealmObjectSchema;

.field private table:Lio/realm/internal/TableOrView;

.field private view:Lio/realm/internal/LinkView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lio/realm/RealmQuery;->INVALID_NATIVE_POINTER:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/LinkView;Ljava/lang/Class;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "view"    # Lio/realm/internal/LinkView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/LinkView;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    .line 153
    iput-object p3, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    .line 154
    invoke-virtual {p2}, Lio/realm/internal/LinkView;->where()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    .line 155
    iput-object p2, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    .line 156
    iget-object v0, p1, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p3}, Lio/realm/RealmSchema;->getSchemaForClass(Ljava/lang/Class;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    .line 157
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    iget-object v0, v0, Lio/realm/RealmObjectSchema;->table:Lio/realm/internal/Table;

    iput-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    .line 158
    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/LinkView;Ljava/lang/String;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "view"    # Lio/realm/internal/LinkView;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    .line 176
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    .line 178
    iput-object p3, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    .line 179
    invoke-virtual {p2}, Lio/realm/internal/LinkView;->where()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    .line 180
    iput-object p2, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    .line 181
    iget-object v0, p1, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p3}, Lio/realm/RealmSchema;->getSchemaForClass(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    .line 182
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    iget-object v0, v0, Lio/realm/RealmObjectSchema;->table:Lio/realm/internal/Table;

    iput-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    .line 183
    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Ljava/lang/String;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 160
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    .line 162
    iput-object p2, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    .line 163
    iget-object v0, p1, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p2}, Lio/realm/RealmSchema;->getSchemaForClass(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    .line 164
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    iget-object v0, v0, Lio/realm/RealmObjectSchema;->table:Lio/realm/internal/Table;

    iput-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    .line 165
    iget-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v0}, Lio/realm/internal/TableOrView;->where()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    .line 166
    return-void
.end method

.method private constructor <init>(Lio/realm/Realm;Ljava/lang/Class;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    .line 135
    iput-object p2, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    .line 136
    iget-object v0, p1, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p2}, Lio/realm/RealmSchema;->getSchemaForClass(Ljava/lang/Class;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    .line 137
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    iget-object v0, v0, Lio/realm/RealmObjectSchema;->table:Lio/realm/internal/Table;

    iput-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    .line 139
    iget-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v0}, Lio/realm/internal/TableOrView;->where()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    .line 140
    return-void
.end method

.method private constructor <init>(Lio/realm/RealmResults;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmResults",
            "<TE;>;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    .local p1, "queryResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iget-object v0, p1, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iput-object v0, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    .line 144
    iput-object p2, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    .line 145
    iget-object v0, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p2}, Lio/realm/RealmSchema;->getSchemaForClass(Ljava/lang/Class;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    .line 146
    invoke-virtual {p1}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    .line 148
    invoke-virtual {p1}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/TableOrView;->where()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    .line 149
    return-void
.end method

.method private constructor <init>(Lio/realm/RealmResults;Ljava/lang/String;)V
    .locals 1
    .param p2, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmResults",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    .local p1, "queryResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iget-object v0, p1, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iput-object v0, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    .line 170
    iput-object p2, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p2}, Lio/realm/RealmSchema;->getSchemaForClass(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    .line 172
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    iget-object v0, v0, Lio/realm/RealmObjectSchema;->table:Lio/realm/internal/Table;

    iput-object v0, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    .line 173
    invoke-virtual {p1}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/TableOrView;->where()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    .line 174
    return-void
.end method

.method static synthetic access$000(Lio/realm/RealmQuery;)Lio/realm/internal/TableQuery;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmQuery;

    .prologue
    .line 62
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    return-object v0
.end method

.method static synthetic access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lio/realm/RealmQuery;
    .param p1, "x1"    # Lio/realm/internal/SharedGroup;
    .param p2, "x2"    # Ljava/lang/ref/WeakReference;
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/Object;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lio/realm/RealmQuery;->INVALID_NATIVE_POINTER:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$300(Lio/realm/RealmQuery;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Lio/realm/RealmQuery;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lio/realm/RealmQuery;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Lio/realm/RealmQuery;)Lio/realm/BaseRealm;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmQuery;

    .prologue
    .line 62
    iget-object v0, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    return-object v0
.end method

.method private checkQueryIsNotReused()V
    .locals 2

    .prologue
    .line 1995
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    if-eqz v0, :cond_0

    .line 1996
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This RealmQuery is already used by a find* query, please create a new query"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1998
    :cond_0
    return-void
.end method

.method private checkSortParameters([Ljava/lang/String;[Lio/realm/Sort;)V
    .locals 6
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 1957
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    if-nez p1, :cond_0

    .line 1958
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fieldNames cannot be \'null\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1959
    :cond_0
    if-nez p2, :cond_1

    .line 1960
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sortOrders cannot be \'null\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1961
    :cond_1
    array-length v0, p1

    if-nez v0, :cond_2

    .line 1962
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one field name must be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1963
    :cond_2
    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_3

    .line 1964
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "Number of field names (%d) and sort orders (%d) does not match."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    array-length v5, p1

    .line 1966
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    array-length v5, p2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1964
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1968
    :cond_3
    return-void
.end method

.method private closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    .locals 2
    .param p1, "sharedGroup"    # Lio/realm/internal/SharedGroup;
    .param p3, "what"    # I
    .param p4, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/SharedGroup;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;I",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1981
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    .local p2, "weakHandler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    if-eqz p1, :cond_0

    .line 1982
    invoke-virtual {p1}, Lio/realm/internal/SharedGroup;->close()V

    .line 1984
    :cond_0
    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 1985
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1986
    invoke-virtual {v0, p3, p4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1988
    :cond_1
    return-void
.end method

.method public static createDynamicQuery(Lio/realm/DynamicRealm;Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 1
    .param p0, "realm"    # Lio/realm/DynamicRealm;
    .param p1, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/DynamicRealm;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lio/realm/RealmQuery;

    invoke-direct {v0, p0, p1}, Lio/realm/RealmQuery;-><init>(Lio/realm/BaseRealm;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createQuery(Lio/realm/Realm;Ljava/lang/Class;)Lio/realm/RealmQuery;
    .locals 1
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    new-instance v0, Lio/realm/RealmQuery;

    invoke-direct {v0, p0, p1}, Lio/realm/RealmQuery;-><init>(Lio/realm/Realm;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static createQueryFromList(Lio/realm/RealmList;)Lio/realm/RealmQuery;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/RealmList",
            "<TE;>;)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "list":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lio/realm/RealmQuery;

    iget-object v1, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    iget-object v3, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lio/realm/RealmQuery;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/LinkView;Ljava/lang/Class;)V

    .line 129
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/realm/RealmQuery;

    iget-object v1, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    iget-object v3, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lio/realm/RealmQuery;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/LinkView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static createQueryFromResult(Lio/realm/RealmResults;)Lio/realm/RealmQuery;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/RealmResults",
            "<TE;>;)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "queryResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Lio/realm/RealmQuery;

    iget-object v1, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    invoke-direct {v0, p0, v1}, Lio/realm/RealmQuery;-><init>(Lio/realm/RealmResults;Ljava/lang/Class;)V

    .line 113
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/realm/RealmQuery;

    iget-object v1, p0, Lio/realm/RealmResults;->className:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lio/realm/RealmQuery;-><init>(Lio/realm/RealmResults;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getAndValidateDistinctColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;)J
    .locals 6
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "table"    # Lio/realm/internal/Table;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1215
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1216
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Non-empty field name must be provided."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1218
    :cond_1
    invoke-virtual {p1, p0}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1220
    .local v0, "columnIndex":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 1221
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field name \'%s\' does not exist."

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1224
    :cond_2
    const-string v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1225
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Distinct operation on linked properties is not supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1228
    :cond_3
    invoke-virtual {p1, v0, v1}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1229
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field name \'%s\' must be indexed in order to use it for distinct queries."

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1231
    :cond_4
    return-wide v0
.end method

.method private getColumnIndexForSort(Ljava/lang/String;)J
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 2017
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2018
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Non-empty fieldname required."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2020
    :cond_1
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2021
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sorting using child object fields is not supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2024
    :cond_2
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v1, p1}, Lio/realm/RealmObjectSchema;->getFieldIndex(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 2025
    .local v0, "columnIndex":Ljava/lang/Long;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_4

    .line 2026
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Field name \'%s\' does not exist."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2029
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    return-wide v2
.end method

.method private getSourceRowIndexForFirstObject()J
    .locals 4

    .prologue
    .line 2001
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2}, Lio/realm/internal/TableQuery;->find()J

    move-result-wide v0

    .line 2002
    .local v0, "rowIndex":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 2010
    .end local v0    # "rowIndex":J
    :cond_0
    :goto_0
    return-wide v0

    .line 2005
    .restart local v0    # "rowIndex":J
    :cond_1
    iget-object v2, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    if-eqz v2, :cond_2

    .line 2006
    iget-object v2, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/LinkView;->getTargetRowIndex(J)J

    move-result-wide v0

    goto :goto_0

    .line 2007
    :cond_2
    iget-object v2, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    instance-of v2, v2, Lio/realm/internal/TableView;

    if-eqz v2, :cond_0

    .line 2008
    iget-object v2, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    check-cast v2, Lio/realm/internal/TableView;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableView;->getSourceRowIndex(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method static varargs getValidatedColumIndexes(Lio/realm/internal/Table;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p0, "table"    # Lio/realm/internal/Table;
    .param p1, "firstFieldName"    # Ljava/lang/String;
    .param p2, "remainingFieldNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/Table;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1266
    .local v0, "columnIndexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1, p0}, Lio/realm/RealmQuery;->getAndValidateDistinctColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;)J

    move-result-wide v2

    .line 1267
    .local v2, "firstIndex":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1269
    if-eqz p2, :cond_0

    array-length v6, p2

    if-lez v6, :cond_0

    .line 1270
    array-length v7, p2

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v1, p2, v6

    .line 1271
    .local v1, "field":Ljava/lang/String;
    invoke-static {v1, p0}, Lio/realm/RealmQuery;->getAndValidateDistinctColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;)J

    move-result-wide v4

    .line 1272
    .local v4, "index":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1270
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1275
    .end local v1    # "field":Ljava/lang/String;
    .end local v4    # "index":J
    :cond_0
    return-object v0
.end method

.method private getWeakReferenceHandler()Ljava/lang/ref/WeakReference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1971
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1972
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Your Realm is opened from a thread without a Looper. Async queries need a Handler to send results of your query"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1975
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v1, v1, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private isDynamicQuery()Z
    .locals 1

    .prologue
    .line 1713
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public average(Ljava/lang/String;)D
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 1317
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v2, p1}, Lio/realm/RealmObjectSchema;->getAndCheckFieldIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1318
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmQuery$6;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1326
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1320
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->averageInt(J)D

    move-result-wide v2

    .line 1324
    :goto_0
    return-wide v2

    .line 1322
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->averageDouble(J)D

    move-result-wide v2

    goto :goto_0

    .line 1324
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->averageFloat(J)D

    move-result-wide v2

    goto :goto_0

    .line 1318
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public beginGroup()Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1036
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->group()Lio/realm/internal/TableQuery;

    .line 1037
    return-object p0
.end method

.method public beginsWith(Ljava/lang/String;Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 982
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Case;->SENSITIVE:Lio/realm/Case;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/RealmQuery;->beginsWith(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method

.method public beginsWith(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "casing"    # Lio/realm/Case;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/Case;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 995
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 996
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->beginsWith([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;

    .line 997
    return-object p0
.end method

.method public between(Ljava/lang/String;DD)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "from"    # D
    .param p4, "to"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 908
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v0, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v1

    .line 909
    .local v1, "columnIndices":[J
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/TableQuery;->between([JDD)Lio/realm/internal/TableQuery;

    .line 910
    return-object p0
.end method

.method public between(Ljava/lang/String;FF)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "from"    # F
    .param p3, "to"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "FF)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 923
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 924
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->between([JFF)Lio/realm/internal/TableQuery;

    .line 925
    return-object p0
.end method

.method public between(Ljava/lang/String;II)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "from"    # I
    .param p3, "to"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 878
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v0, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v1

    .line 879
    .local v1, "columnIndices":[J
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/TableQuery;->between([JJJ)Lio/realm/internal/TableQuery;

    .line 880
    return-object p0
.end method

.method public between(Ljava/lang/String;JJ)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "from"    # J
    .param p4, "to"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JJ)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 893
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v0, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v1

    .line 894
    .local v1, "columnIndices":[J
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/TableQuery;->between([JJJ)Lio/realm/internal/TableQuery;

    .line 895
    return-object p0
.end method

.method public between(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "to"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 938
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 939
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->between([JLjava/util/Date;Ljava/util/Date;)Lio/realm/internal/TableQuery;

    .line 940
    return-object p0
.end method

.method public contains(Ljava/lang/String;Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 955
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Case;->SENSITIVE:Lio/realm/Case;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/RealmQuery;->contains(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "casing"    # Lio/realm/Case;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/Case;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 968
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 969
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->contains([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;

    .line 970
    return-object p0
.end method

.method public count()J
    .locals 2

    .prologue
    .line 1417
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->count()J

    move-result-wide v0

    return-wide v0
.end method

.method public distinct(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1110
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1111
    iget-object v4, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v4}, Lio/realm/internal/TableOrView;->getTable()Lio/realm/internal/Table;

    move-result-object v4

    invoke-static {p1, v4}, Lio/realm/RealmQuery;->getAndValidateDistinctColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;)J

    move-result-wide v0

    .line 1112
    .local v0, "columnIndex":J
    iget-object v4, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v4}, Lio/realm/internal/TableQuery;->findAll()Lio/realm/internal/TableView;

    move-result-object v3

    .line 1113
    .local v3, "tableView":Lio/realm/internal/TableView;
    invoke-virtual {v3, v0, v1}, Lio/realm/internal/TableView;->distinct(J)V

    .line 1116
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1118
    iget-object v4, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v5, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lio/realm/RealmResults;->createFromDynamicTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v2

    .line 1122
    .local v2, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    return-object v2

    .line 1120
    .end local v2    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    iget-object v4, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v5, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v4, v3, v5}, Lio/realm/RealmResults;->createFromTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v2

    .restart local v2    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public varargs distinct(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 5
    .param p1, "firstFieldName"    # Ljava/lang/String;
    .param p2, "remainingFieldNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1247
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1248
    iget-object v3, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3}, Lio/realm/internal/TableOrView;->getTable()Lio/realm/internal/Table;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lio/realm/RealmQuery;->getValidatedColumIndexes(Lio/realm/internal/Table;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1249
    .local v0, "columnIndexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v3, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v3}, Lio/realm/internal/TableQuery;->findAll()Lio/realm/internal/TableView;

    move-result-object v2

    .line 1250
    .local v2, "tableView":Lio/realm/internal/TableView;
    invoke-virtual {v2, v0}, Lio/realm/internal/TableView;->distinct(Ljava/util/List;)V

    .line 1253
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1255
    iget-object v3, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v4, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v3, v2, v4}, Lio/realm/RealmResults;->createFromDynamicTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v1

    .line 1259
    .local v1, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    return-object v1

    .line 1257
    .end local v1    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    iget-object v3, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v4, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v3, v2, v4}, Lio/realm/RealmResults;->createFromTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v1

    .restart local v1    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public distinctAsync(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 14
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1138
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1139
    iget-object v1, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v1}, Lio/realm/internal/TableOrView;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    invoke-static {p1, v1}, Lio/realm/RealmQuery;->getAndValidateDistinctColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;)J

    move-result-wide v6

    .line 1140
    .local v6, "columnIndex":J
    invoke-direct {p0}, Lio/realm/RealmQuery;->getWeakReferenceHandler()Ljava/lang/ref/WeakReference;

    move-result-object v9

    .line 1143
    .local v9, "weakHandler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v2, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, v2, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v2}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v12

    invoke-virtual {v1, v12, v13}, Lio/realm/internal/TableQuery;->handoverQuery(J)J

    move-result-wide v4

    .line 1146
    .local v4, "handoverQueryPointer":J
    new-instance v1, Lio/realm/internal/async/ArgumentsHolder;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lio/realm/internal/async/ArgumentsHolder;-><init>(I)V

    iput-object v1, p0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    .line 1147
    iget-object v1, p0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    iput-wide v6, v1, Lio/realm/internal/async/ArgumentsHolder;->columnIndex:J

    .line 1151
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v3

    .line 1156
    .local v3, "realmConfiguration":Lio/realm/RealmConfiguration;
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1158
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v11, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v1, v2, v11}, Lio/realm/RealmResults;->createFromDynamicClass(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v10

    .line 1163
    .local v10, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v1, v1, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v1, v10, p0}, Lio/realm/HandlerController;->addToAsyncRealmResults(Lio/realm/RealmResults;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;

    move-result-object v8

    .line 1165
    .local v8, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    sget-object v11, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    new-instance v1, Lio/realm/RealmQuery$1;

    move-object v2, p0

    invoke-direct/range {v1 .. v9}, Lio/realm/RealmQuery$1;-><init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;JJLjava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v11, v1}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 1208
    .local v0, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    invoke-virtual {v10, v0}, Lio/realm/RealmResults;->setPendingQuery(Ljava/util/concurrent/Future;)V

    .line 1209
    return-object v10

    .line 1160
    .end local v0    # "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    .end local v8    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    .end local v10    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v11, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v1, v2, v11}, Lio/realm/RealmResults;->createFromTableQuery(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v10

    .restart local v10    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public endGroup()Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1047
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->endGroup()Lio/realm/internal/TableQuery;

    .line 1048
    return-object p0
.end method

.method public endsWith(Ljava/lang/String;Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1009
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Case;->SENSITIVE:Lio/realm/Case;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/RealmQuery;->endsWith(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method

.method public endsWith(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "casing"    # Lio/realm/Case;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/Case;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1022
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 1023
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->endsWith([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;

    .line 1024
    return-object p0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 383
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 384
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 385
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 389
    :goto_0
    return-object p0

    .line 387
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lio/realm/internal/TableQuery;->equalTo([JZ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Byte;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Byte;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 276
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 277
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 278
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 282
    :goto_0
    return-object p0

    .line 280
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->equalTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Double;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Double;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 347
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 348
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 349
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 353
    :goto_0
    return-object p0

    .line 351
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->equalTo([JD)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Float;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Float;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 365
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 366
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 367
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 371
    :goto_0
    return-object p0

    .line 369
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lio/realm/internal/TableQuery;->equalTo([JF)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 312
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 313
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 314
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 318
    :goto_0
    return-object p0

    .line 316
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->equalTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 330
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 331
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 332
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 336
    :goto_0
    return-object p0

    .line 334
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->equalTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/Short;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Short;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Short;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 294
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 295
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 296
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 300
    :goto_0
    return-object p0

    .line 298
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Short;->shortValue()S

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->equalTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 249
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Case;->SENSITIVE:Lio/realm/Case;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method

.method public equalTo(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "casing"    # Lio/realm/Case;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/Case;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 262
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 263
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->equalTo([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;

    .line 264
    return-object p0
.end method

.method public equalTo(Ljava/lang/String;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 401
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 402
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->equalTo([JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 403
    return-object p0
.end method

.method public findAll()Lio/realm/RealmResults;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1429
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1431
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1432
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2}, Lio/realm/internal/TableQuery;->findAll()Lio/realm/internal/TableView;

    move-result-object v2

    iget-object v3, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lio/realm/RealmResults;->createFromDynamicTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v0

    .line 1436
    .local v0, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    return-object v0

    .line 1434
    .end local v0    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2}, Lio/realm/internal/TableQuery;->findAll()Lio/realm/internal/TableView;

    move-result-object v2

    iget-object v3, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v1, v2, v3}, Lio/realm/RealmResults;->createFromTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v0

    .restart local v0    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public findAllAsync()Lio/realm/RealmResults;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1448
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1449
    invoke-direct {p0}, Lio/realm/RealmQuery;->getWeakReferenceHandler()Ljava/lang/ref/WeakReference;

    move-result-object v7

    .line 1452
    .local v7, "weakHandler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v2, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, v2, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v2}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, Lio/realm/internal/TableQuery;->handoverQuery(J)J

    move-result-wide v4

    .line 1455
    .local v4, "handoverQueryPointer":J
    new-instance v1, Lio/realm/internal/async/ArgumentsHolder;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lio/realm/internal/async/ArgumentsHolder;-><init>(I)V

    iput-object v1, p0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    .line 1459
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v3

    .line 1464
    .local v3, "realmConfiguration":Lio/realm/RealmConfiguration;
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1466
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v9, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v1, v2, v9}, Lio/realm/RealmResults;->createFromDynamicClass(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v8

    .line 1471
    .local v8, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v1, v1, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v1, v8, p0}, Lio/realm/HandlerController;->addToAsyncRealmResults(Lio/realm/RealmResults;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;

    move-result-object v6

    .line 1473
    .local v6, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    sget-object v9, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    new-instance v1, Lio/realm/RealmQuery$2;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lio/realm/RealmQuery$2;-><init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;JLjava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v9, v1}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 1521
    .local v0, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    invoke-virtual {v8, v0}, Lio/realm/RealmResults;->setPendingQuery(Ljava/util/concurrent/Future;)V

    .line 1522
    return-object v8

    .line 1468
    .end local v0    # "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    .end local v6    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    .end local v8    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v9, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v1, v2, v9}, Lio/realm/RealmResults;->createFromTableQuery(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v8

    .restart local v8    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public findAllSorted(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1656
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Sort;->ASCENDING:Lio/realm/Sort;

    invoke-virtual {p0, p1, v0}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1540
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1541
    iget-object v4, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v4}, Lio/realm/internal/TableQuery;->findAll()Lio/realm/internal/TableView;

    move-result-object v3

    .line 1542
    .local v3, "tableView":Lio/realm/internal/TableView;
    invoke-direct {p0, p1}, Lio/realm/RealmQuery;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 1543
    .local v0, "columnIndex":J
    invoke-virtual {v3, v0, v1, p2}, Lio/realm/internal/TableView;->sort(JLio/realm/Sort;)V

    .line 1546
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1547
    iget-object v4, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v5, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lio/realm/RealmResults;->createFromDynamicTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v2

    .line 1551
    .local v2, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    return-object v2

    .line 1549
    .end local v2    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    iget-object v4, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v5, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v4, v3, v5}, Lio/realm/RealmResults;->createFromTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v2

    .restart local v2    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public findAllSorted(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1834
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/RealmQuery;->findAllSorted([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public findAllSorted([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 10
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1687
    invoke-direct {p0, p1, p2}, Lio/realm/RealmQuery;->checkSortParameters([Ljava/lang/String;[Lio/realm/Sort;)V

    .line 1689
    array-length v7, p1

    if-ne v7, v9, :cond_0

    array-length v7, p2

    if-ne v7, v9, :cond_0

    .line 1690
    aget-object v7, p1, v8

    aget-object v8, p2, v8

    invoke-virtual {p0, v7, v8}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v5

    .line 1708
    :goto_0
    return-object v5

    .line 1692
    :cond_0
    iget-object v7, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v7}, Lio/realm/internal/TableQuery;->findAll()Lio/realm/internal/TableView;

    move-result-object v6

    .line 1693
    .local v6, "tableView":Lio/realm/internal/TableView;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1695
    .local v2, "columnIndices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v7, p1

    if-ge v4, v7, :cond_1

    .line 1696
    aget-object v3, p1, v4

    .line 1697
    .local v3, "fieldName":Ljava/lang/String;
    invoke-direct {p0, v3}, Lio/realm/RealmQuery;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 1698
    .local v0, "columnIndex":J
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1695
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1700
    .end local v0    # "columnIndex":J
    .end local v3    # "fieldName":Ljava/lang/String;
    :cond_1
    invoke-virtual {v6, v2, p2}, Lio/realm/internal/TableView;->sort(Ljava/util/List;[Lio/realm/Sort;)V

    .line 1703
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1704
    iget-object v7, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v8, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v7, v6, v8}, Lio/realm/RealmResults;->createFromDynamicTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v5

    .local v5, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0

    .line 1706
    .end local v5    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_2
    iget-object v7, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v8, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v7, v6, v8}, Lio/realm/RealmResults;->createFromTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v5

    .restart local v5    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public findAllSortedAsync(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1669
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Sort;->ASCENDING:Lio/realm/Sort;

    invoke-virtual {p0, p1, v0}, Lio/realm/RealmQuery;->findAllSortedAsync(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public findAllSortedAsync(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 18
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1564
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct/range {p0 .. p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1565
    invoke-direct/range {p0 .. p1}, Lio/realm/RealmQuery;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v12

    .line 1568
    .local v12, "columnIndex":J
    new-instance v2, Lio/realm/internal/async/ArgumentsHolder;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lio/realm/internal/async/ArgumentsHolder;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    .line 1569
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    move-object/from16 v0, p2

    iput-object v0, v2, Lio/realm/internal/async/ArgumentsHolder;->sortOrder:Lio/realm/Sort;

    .line 1570
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    iput-wide v12, v2, Lio/realm/internal/async/ArgumentsHolder;->columnIndex:J

    .line 1572
    invoke-direct/range {p0 .. p0}, Lio/realm/RealmQuery;->getWeakReferenceHandler()Ljava/lang/ref/WeakReference;

    move-result-object v10

    .line 1575
    .local v10, "weakHandler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v3}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->handoverQuery(J)J

    move-result-wide v6

    .line 1578
    .local v6, "handoverQueryPointer":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v4

    .line 1581
    .local v4, "realmConfiguration":Lio/realm/RealmConfiguration;
    invoke-direct/range {p0 .. p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1583
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v2, v3, v5}, Lio/realm/RealmResults;->createFromDynamicClass(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v14

    .line 1588
    .local v14, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, v2, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    .line 1589
    move-object/from16 v0, p0

    invoke-virtual {v2, v14, v0}, Lio/realm/HandlerController;->addToAsyncRealmResults(Lio/realm/RealmResults;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;

    move-result-object v9

    .line 1591
    .local v9, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    sget-object v15, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    new-instance v2, Lio/realm/RealmQuery$3;

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v10}, Lio/realm/RealmQuery$3;-><init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;Ljava/lang/String;JLio/realm/Sort;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v15, v2}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v11

    .line 1638
    .local v11, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    invoke-virtual {v14, v11}, Lio/realm/RealmResults;->setPendingQuery(Ljava/util/concurrent/Future;)V

    .line 1639
    return-object v14

    .line 1585
    .end local v9    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    .end local v11    # "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    .end local v14    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v2, v3, v5}, Lio/realm/RealmResults;->createFromTableQuery(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v14

    .restart local v14    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public findAllSortedAsync(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1848
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/RealmQuery;->findAllSortedAsync([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public findAllSortedAsync([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 20
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1728
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct/range {p0 .. p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1729
    invoke-direct/range {p0 .. p2}, Lio/realm/RealmQuery;->checkSortParameters([Ljava/lang/String;[Lio/realm/Sort;)V

    .line 1731
    move-object/from16 v0, p1

    array-length v3, v0

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    move-object/from16 v0, p2

    array-length v3, v0

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1732
    const/4 v3, 0x0

    aget-object v3, p1, v3

    const/4 v4, 0x0

    aget-object v4, p2, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lio/realm/RealmQuery;->findAllSortedAsync(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v16

    .line 1813
    :goto_0
    return-object v16

    .line 1735
    :cond_0
    invoke-direct/range {p0 .. p0}, Lio/realm/RealmQuery;->getWeakReferenceHandler()Ljava/lang/ref/WeakReference;

    move-result-object v11

    .line 1738
    .local v11, "weakHandler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v4, v4, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v4}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v3, v0, v1}, Lio/realm/internal/TableQuery;->handoverQuery(J)J

    move-result-wide v6

    .line 1741
    .local v6, "handoverQueryPointer":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v3}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v5

    .line 1743
    .local v5, "realmConfiguration":Lio/realm/RealmConfiguration;
    move-object/from16 v0, p1

    array-length v3, v0

    new-array v8, v3, [J

    .line 1744
    .local v8, "indices":[J
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v14, v3, :cond_1

    .line 1745
    aget-object v2, p1, v14

    .line 1746
    .local v2, "fieldName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lio/realm/RealmQuery;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v12

    .line 1747
    .local v12, "columnIndex":J
    aput-wide v12, v8, v14

    .line 1744
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 1751
    .end local v2    # "fieldName":Ljava/lang/String;
    .end local v12    # "columnIndex":J
    :cond_1
    new-instance v3, Lio/realm/internal/async/ArgumentsHolder;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Lio/realm/internal/async/ArgumentsHolder;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    .line 1752
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    move-object/from16 v0, p2

    iput-object v0, v3, Lio/realm/internal/async/ArgumentsHolder;->sortOrders:[Lio/realm/Sort;

    .line 1753
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    iput-object v8, v3, Lio/realm/internal/async/ArgumentsHolder;->columnIndices:[J

    .line 1757
    invoke-direct/range {p0 .. p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1759
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-object/from16 v0, p0

    iget-object v9, v0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-static {v3, v4, v9}, Lio/realm/RealmResults;->createFromDynamicClass(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v16

    .line 1764
    .local v16, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v3, v0, v1}, Lio/realm/HandlerController;->addToAsyncRealmResults(Lio/realm/RealmResults;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;

    move-result-object v10

    .line 1766
    .local v10, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    sget-object v17, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    new-instance v3, Lio/realm/RealmQuery$4;

    move-object/from16 v4, p0

    move-object/from16 v9, p2

    invoke-direct/range {v3 .. v11}, Lio/realm/RealmQuery$4;-><init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;J[J[Lio/realm/Sort;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v15

    .line 1812
    .local v15, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lio/realm/RealmResults;->setPendingQuery(Ljava/util/concurrent/Future;)V

    goto/16 :goto_0

    .line 1761
    .end local v10    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    .end local v15    # "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    .end local v16    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    move-object/from16 v0, p0

    iget-object v9, v0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-static {v3, v4, v9}, Lio/realm/RealmResults;->createFromTableQuery(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)Lio/realm/RealmResults;

    move-result-object v16

    .restart local v16    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_2
.end method

.method public findFirst()Lio/realm/RealmModel;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1858
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1859
    invoke-direct {p0}, Lio/realm/RealmQuery;->getSourceRowIndexForFirstObject()J

    move-result-wide v2

    .line 1860
    .local v2, "sourceRowIndex":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 1861
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v4, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    iget-object v5, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-virtual {v1, v4, v5, v2, v3}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/RealmModel;

    move-result-object v0

    .line 1864
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findFirstAsync()Lio/realm/RealmModel;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1880
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    invoke-direct {p0}, Lio/realm/RealmQuery;->checkQueryIsNotReused()V

    .line 1881
    invoke-direct {p0}, Lio/realm/RealmQuery;->getWeakReferenceHandler()Ljava/lang/ref/WeakReference;

    move-result-object v7

    .line 1884
    .local v7, "weakHandler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v2, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v2, v2, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v2}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, Lio/realm/internal/TableQuery;->handoverQuery(J)J

    move-result-wide v4

    .line 1887
    .local v4, "handoverQueryPointer":J
    new-instance v1, Lio/realm/internal/async/ArgumentsHolder;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lio/realm/internal/async/ArgumentsHolder;-><init>(I)V

    iput-object v1, p0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    .line 1889
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v3

    .line 1894
    .local v3, "realmConfiguration":Lio/realm/RealmConfiguration;
    invoke-direct {p0}, Lio/realm/RealmQuery;->isDynamicQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1896
    new-instance v9, Lio/realm/DynamicRealmObject;

    iget-object v1, p0, Lio/realm/RealmQuery;->className:Ljava/lang/String;

    invoke-direct {v9, v1}, Lio/realm/DynamicRealmObject;-><init>(Ljava/lang/String;)V

    .local v9, "result":Lio/realm/RealmModel;, "TE;"
    :goto_0
    move-object v8, v9

    .line 1901
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    .line 1902
    .local v8, "proxy":Lio/realm/internal/RealmObjectProxy;
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v1, v1, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v1, v8, p0}, Lio/realm/HandlerController;->addToAsyncRealmObject(Lio/realm/internal/RealmObjectProxy;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;

    move-result-object v6

    .line 1903
    .local v6, "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 1904
    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    sget-object v2, Lio/realm/internal/Row;->EMPTY_ROW:Lio/realm/internal/Row;

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 1906
    sget-object v10, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    new-instance v1, Lio/realm/RealmQuery$5;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lio/realm/RealmQuery$5;-><init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;JLjava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v10, v1}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 1951
    .local v0, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/realm/ProxyState;->setPendingQuery$realm(Ljava/util/concurrent/Future;)V

    .line 1953
    return-object v9

    .line 1898
    .end local v0    # "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    .end local v6    # "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    .end local v8    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local v9    # "result":Lio/realm/RealmModel;, "TE;"
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    iget-object v10, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v10}, Lio/realm/BaseRealm;->getSchema()Lio/realm/RealmSchema;

    move-result-object v10

    iget-object v11, p0, Lio/realm/RealmQuery;->clazz:Ljava/lang/Class;

    invoke-virtual {v10, v11}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v10

    invoke-virtual {v1, v2, v10}, Lio/realm/internal/RealmProxyMediator;->newInstance(Ljava/lang/Class;Lio/realm/internal/ColumnInfo;)Lio/realm/RealmModel;

    move-result-object v9

    .restart local v9    # "result":Lio/realm/RealmModel;, "TE;"
    goto :goto_0
.end method

.method public getArgument()Lio/realm/internal/async/ArgumentsHolder;
    .locals 1

    .prologue
    .line 2033
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->argumentsHolder:Lio/realm/internal/async/ArgumentsHolder;

    return-object v0
.end method

.method public greaterThan(Ljava/lang/String;D)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 621
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 622
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->greaterThan([JD)Lio/realm/internal/TableQuery;

    .line 623
    return-object p0
.end method

.method public greaterThan(Ljava/lang/String;F)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 635
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 636
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->greaterThan([JF)Lio/realm/internal/TableQuery;

    .line 637
    return-object p0
.end method

.method public greaterThan(Ljava/lang/String;I)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 593
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 594
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->greaterThan([JJ)Lio/realm/internal/TableQuery;

    .line 595
    return-object p0
.end method

.method public greaterThan(Ljava/lang/String;J)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 607
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 608
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->greaterThan([JJ)Lio/realm/internal/TableQuery;

    .line 609
    return-object p0
.end method

.method public greaterThan(Ljava/lang/String;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 649
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 650
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->greaterThan([JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 651
    return-object p0
.end method

.method public greaterThanOrEqualTo(Ljava/lang/String;D)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 691
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 692
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->greaterThanOrEqual([JD)Lio/realm/internal/TableQuery;

    .line 693
    return-object p0
.end method

.method public greaterThanOrEqualTo(Ljava/lang/String;F)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 705
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 706
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->greaterThanOrEqual([JF)Lio/realm/internal/TableQuery;

    .line 707
    return-object p0
.end method

.method public greaterThanOrEqualTo(Ljava/lang/String;I)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 663
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 664
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->greaterThanOrEqual([JJ)Lio/realm/internal/TableQuery;

    .line 665
    return-object p0
.end method

.method public greaterThanOrEqualTo(Ljava/lang/String;J)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 677
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 678
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->greaterThanOrEqual([JJ)Lio/realm/internal/TableQuery;

    .line 679
    return-object p0
.end method

.method public greaterThanOrEqualTo(Ljava/lang/String;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 719
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 720
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->greaterThanOrEqual([JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 721
    return-object p0
.end method

.method handoverQueryPointer()J
    .locals 4

    .prologue
    .line 2042
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    iget-object v1, v1, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v1}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/TableQuery;->handoverQuery(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public isEmpty(Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1080
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x3

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lio/realm/RealmFieldType;->BINARY:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 1081
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isEmpty([J)Lio/realm/internal/TableQuery;

    .line 1082
    return-object p0
.end method

.method public isNotEmpty(Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1094
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x3

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lio/realm/RealmFieldType;->BINARY:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 1095
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotEmpty([J)Lio/realm/internal/TableQuery;

    .line 1096
    return-object p0
.end method

.method public isNotNull(Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 231
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x0

    new-array v2, v2, [Lio/realm/RealmFieldType;

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 234
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 235
    return-object p0
.end method

.method public isNull(Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 215
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x0

    new-array v2, v2, [Lio/realm/RealmFieldType;

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 218
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNull([J)Lio/realm/internal/TableQuery;

    .line 219
    return-object p0
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    const/4 v0, 0x0

    .line 192
    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return v0

    .line 196
    :cond_1
    iget-object v1, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    if-eqz v1, :cond_2

    .line 197
    iget-object v0, p0, Lio/realm/RealmQuery;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isAttached()Z

    move-result v0

    goto :goto_0

    .line 199
    :cond_2
    iget-object v1, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v1}, Lio/realm/internal/TableOrView;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/internal/Table;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public lessThan(Ljava/lang/String;D)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 763
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 764
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->lessThan([JD)Lio/realm/internal/TableQuery;

    .line 765
    return-object p0
.end method

.method public lessThan(Ljava/lang/String;F)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 777
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 778
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->lessThan([JF)Lio/realm/internal/TableQuery;

    .line 779
    return-object p0
.end method

.method public lessThan(Ljava/lang/String;I)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 735
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 736
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->lessThan([JJ)Lio/realm/internal/TableQuery;

    .line 737
    return-object p0
.end method

.method public lessThan(Ljava/lang/String;J)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 749
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 750
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->lessThan([JJ)Lio/realm/internal/TableQuery;

    .line 751
    return-object p0
.end method

.method public lessThan(Ljava/lang/String;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 791
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 792
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->lessThan([JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 793
    return-object p0
.end method

.method public lessThanOrEqualTo(Ljava/lang/String;D)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 833
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 834
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->lessThanOrEqual([JD)Lio/realm/internal/TableQuery;

    .line 835
    return-object p0
.end method

.method public lessThanOrEqualTo(Ljava/lang/String;F)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 847
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 848
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->lessThanOrEqual([JF)Lio/realm/internal/TableQuery;

    .line 849
    return-object p0
.end method

.method public lessThanOrEqualTo(Ljava/lang/String;I)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 805
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 806
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->lessThanOrEqual([JJ)Lio/realm/internal/TableQuery;

    .line 807
    return-object p0
.end method

.method public lessThanOrEqualTo(Ljava/lang/String;J)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 819
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 820
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->lessThanOrEqual([JJ)Lio/realm/internal/TableQuery;

    .line 821
    return-object p0
.end method

.method public lessThanOrEqualTo(Ljava/lang/String;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 861
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 862
    .local v0, "columnIndices":[J
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->lessThanOrEqual([JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 863
    return-object p0
.end method

.method public max(Ljava/lang/String;)Ljava/lang/Number;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 1382
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 1383
    iget-object v2, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v2, p1}, Lio/realm/RealmObjectSchema;->getAndCheckFieldIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1384
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmQuery$6;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1392
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1386
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->maximumInt(J)Ljava/lang/Long;

    move-result-object v2

    .line 1390
    :goto_0
    return-object v2

    .line 1388
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->maximumFloat(J)Ljava/lang/Float;

    move-result-object v2

    goto :goto_0

    .line 1390
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->maximumDouble(J)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 1384
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public maximumDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 1406
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v2, p1}, Lio/realm/RealmObjectSchema;->getAndCheckFieldIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1407
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->maximumDate(J)Ljava/util/Date;

    move-result-object v2

    return-object v2
.end method

.method public min(Ljava/lang/String;)Ljava/lang/Number;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 1342
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 1343
    iget-object v2, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v2, p1}, Lio/realm/RealmObjectSchema;->getAndCheckFieldIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1344
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmQuery$6;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1352
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1346
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->minimumInt(J)Ljava/lang/Long;

    move-result-object v2

    .line 1350
    :goto_0
    return-object v2

    .line 1348
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->minimumFloat(J)Ljava/lang/Float;

    move-result-object v2

    goto :goto_0

    .line 1350
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->minimumDouble(J)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 1344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public minimumDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 1366
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v2, p1}, Lio/realm/RealmObjectSchema;->getAndCheckFieldIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1367
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->minimumDate(J)Ljava/util/Date;

    move-result-object v2

    return-object v2
.end method

.method public not()Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1067
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->not()Lio/realm/internal/TableQuery;

    .line 1068
    return-object p0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 555
    iget-object v3, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    new-array v4, v1, [Lio/realm/RealmFieldType;

    sget-object v5, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v5, v4, v2

    invoke-virtual {v3, p1, v4}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 556
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 557
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 561
    :goto_0
    return-object p0

    .line 559
    :cond_0
    iget-object v3, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_1
    invoke-virtual {v3, v0, v1}, Lio/realm/internal/TableQuery;->equalTo([JZ)Lio/realm/internal/TableQuery;

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Byte;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Byte;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 447
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 448
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 449
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 453
    :goto_0
    return-object p0

    .line 451
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->notEqualTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Double;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Double;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 519
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 520
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 521
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 525
    :goto_0
    return-object p0

    .line 523
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->notEqualTo([JD)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Float;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Float;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 537
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 538
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 539
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 543
    :goto_0
    return-object p0

    .line 541
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lio/realm/internal/TableQuery;->notEqualTo([JF)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 483
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 484
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 485
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 489
    :goto_0
    return-object p0

    .line 487
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->notEqualTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 501
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 502
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 503
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 507
    :goto_0
    return-object p0

    .line 505
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->notEqualTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/Short;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Short;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Short;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 465
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 466
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 467
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 471
    :goto_0
    return-object p0

    .line 469
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {p2}, Ljava/lang/Short;->shortValue()S

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lio/realm/internal/TableQuery;->notEqualTo([JJ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/String;)Lio/realm/RealmQuery;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 417
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    sget-object v0, Lio/realm/Case;->SENSITIVE:Lio/realm/Case;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/RealmQuery;->notEqualTo(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/lang/String;Lio/realm/Case;)Lio/realm/RealmQuery;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "casing"    # Lio/realm/Case;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/Case;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    const/4 v5, 0x1

    .line 430
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    new-array v2, v5, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 431
    .local v0, "columnIndices":[J
    array-length v1, v0

    if-le v1, v5, :cond_0

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 432
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Link queries cannot be case insensitive - coming soon."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 434
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->notEqualTo([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;

    .line 435
    return-object p0
.end method

.method public notEqualTo(Ljava/lang/String;Ljava/util/Date;)Lio/realm/RealmQuery;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 573
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v1, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    const/4 v2, 0x1

    new-array v2, v2, [Lio/realm/RealmFieldType;

    const/4 v3, 0x0

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Lio/realm/RealmObjectSchema;->getColumnIndices(Ljava/lang/String;[Lio/realm/RealmFieldType;)[J

    move-result-object v0

    .line 574
    .local v0, "columnIndices":[J
    if-nez p2, :cond_0

    .line 575
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0}, Lio/realm/internal/TableQuery;->isNotNull([J)Lio/realm/internal/TableQuery;

    .line 579
    :goto_0
    return-object p0

    .line 577
    :cond_0
    iget-object v1, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v1, v0, p2}, Lio/realm/internal/TableQuery;->notEqualTo([JLjava/util/Date;)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method public or()Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1057
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v0, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->or()Lio/realm/internal/TableQuery;

    .line 1058
    return-object p0
.end method

.method public sum(Ljava/lang/String;)Ljava/lang/Number;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 1292
    .local p0, "this":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    iget-object v2, p0, Lio/realm/RealmQuery;->schema:Lio/realm/RealmObjectSchema;

    invoke-virtual {v2, p1}, Lio/realm/RealmObjectSchema;->getAndCheckFieldIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 1293
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmQuery$6;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmQuery;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1301
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1295
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->sumInt(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1299
    :goto_0
    return-object v2

    .line 1297
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->sumFloat(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 1299
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->sumDouble(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 1293
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
