.class public Lio/realm/rx/RealmObservableFactory;
.super Ljava/lang/Object;
.source "RealmObservableFactory.java"

# interfaces
.implements Lio/realm/rx/RxObservableFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/rx/RealmObservableFactory$StrongReferenceCounter;
    }
.end annotation


# instance fields
.field objectRefs:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lio/realm/rx/RealmObservableFactory$StrongReferenceCounter",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field

.field resultsRefs:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lio/realm/rx/RealmObservableFactory$StrongReferenceCounter",
            "<",
            "Lio/realm/RealmResults;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lio/realm/rx/RealmObservableFactory$1;

    invoke-direct {v0, p0}, Lio/realm/rx/RealmObservableFactory$1;-><init>(Lio/realm/rx/RealmObservableFactory;)V

    iput-object v0, p0, Lio/realm/rx/RealmObservableFactory;->resultsRefs:Ljava/lang/ThreadLocal;

    .line 57
    new-instance v0, Lio/realm/rx/RealmObservableFactory$2;

    invoke-direct {v0, p0}, Lio/realm/rx/RealmObservableFactory$2;-><init>(Lio/realm/rx/RealmObservableFactory;)V

    iput-object v0, p0, Lio/realm/rx/RealmObservableFactory;->objectRefs:Ljava/lang/ThreadLocal;

    .line 308
    return-void
.end method

.method private getRealmListObservable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">()",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 211
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "RealmList does not support change listeners yet, so cannot create an Observable"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 298
    instance-of v0, p1, Lio/realm/rx/RealmObservableFactory;

    return v0
.end method

.method public from(Lio/realm/DynamicRealm;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/DynamicRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/DynamicRealm;",
            ")",
            "Lrx/Observable",
            "<",
            "Lio/realm/DynamicRealm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p1}, Lio/realm/DynamicRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    .line 97
    .local v0, "realmConfig":Lio/realm/RealmConfiguration;
    new-instance v1, Lio/realm/rx/RealmObservableFactory$4;

    invoke-direct {v1, p0, v0}, Lio/realm/rx/RealmObservableFactory$4;-><init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;)V

    invoke-static {v1}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public from(Lio/realm/DynamicRealm;Lio/realm/DynamicRealmObject;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/DynamicRealm;
    .param p2, "object"    # Lio/realm/DynamicRealmObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/DynamicRealm;",
            "Lio/realm/DynamicRealmObject;",
            ")",
            "Lrx/Observable",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    invoke-virtual {p1}, Lio/realm/DynamicRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    .line 253
    .local v0, "realmConfig":Lio/realm/RealmConfiguration;
    new-instance v1, Lio/realm/rx/RealmObservableFactory$8;

    invoke-direct {v1, p0, v0, p2}, Lio/realm/rx/RealmObservableFactory$8;-><init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;Lio/realm/DynamicRealmObject;)V

    invoke-static {v1}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public from(Lio/realm/DynamicRealm;Lio/realm/RealmList;)Lrx/Observable;
    .locals 1
    .param p1, "realm"    # Lio/realm/DynamicRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/DynamicRealm;",
            "Lio/realm/RealmList",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;)",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmList",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 207
    .local p2, "list":Lio/realm/RealmList;, "Lio/realm/RealmList<Lio/realm/DynamicRealmObject;>;"
    invoke-direct {p0}, Lio/realm/rx/RealmObservableFactory;->getRealmListObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public from(Lio/realm/DynamicRealm;Lio/realm/RealmQuery;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/DynamicRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/DynamicRealm;",
            "Lio/realm/RealmQuery",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;)",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmQuery",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 293
    .local p2, "query":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<Lio/realm/DynamicRealmObject;>;"
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "RealmQuery not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public from(Lio/realm/DynamicRealm;Lio/realm/RealmResults;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/DynamicRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/DynamicRealm;",
            "Lio/realm/RealmResults",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;)",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmResults",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 166
    .local p2, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    invoke-virtual {p1}, Lio/realm/DynamicRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    .line 167
    .local v0, "realmConfig":Lio/realm/RealmConfiguration;
    new-instance v1, Lio/realm/rx/RealmObservableFactory$6;

    invoke-direct {v1, p0, v0, p2}, Lio/realm/rx/RealmObservableFactory$6;-><init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;Lio/realm/RealmResults;)V

    invoke-static {v1}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public from(Lio/realm/Realm;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            ")",
            "Lrx/Observable",
            "<",
            "Lio/realm/Realm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p1}, Lio/realm/Realm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    .line 67
    .local v0, "realmConfig":Lio/realm/RealmConfiguration;
    new-instance v1, Lio/realm/rx/RealmObservableFactory$3;

    invoke-direct {v1, p0, v0}, Lio/realm/rx/RealmObservableFactory$3;-><init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;)V

    invoke-static {v1}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public from(Lio/realm/Realm;Lio/realm/RealmList;)Lrx/Observable;
    .locals 1
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "Lio/realm/RealmList",
            "<TE;>;)",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 202
    .local p2, "list":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-direct {p0}, Lio/realm/rx/RealmObservableFactory;->getRealmListObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public from(Lio/realm/Realm;Lio/realm/RealmModel;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "TE;)",
            "Lrx/Observable",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 216
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    invoke-virtual {p1}, Lio/realm/Realm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    .line 217
    .local v0, "realmConfig":Lio/realm/RealmConfiguration;
    new-instance v1, Lio/realm/rx/RealmObservableFactory$7;

    invoke-direct {v1, p0, v0, p2}, Lio/realm/rx/RealmObservableFactory$7;-><init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;Lio/realm/RealmModel;)V

    invoke-static {v1}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public from(Lio/realm/Realm;Lio/realm/RealmQuery;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "Lio/realm/RealmQuery",
            "<TE;>;)",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmQuery",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 288
    .local p2, "query":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<TE;>;"
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "RealmQuery not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public from(Lio/realm/Realm;Lio/realm/RealmResults;)Lrx/Observable;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "Lio/realm/RealmResults",
            "<TE;>;)",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 129
    .local p2, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p1}, Lio/realm/Realm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    .line 131
    .local v0, "realmConfig":Lio/realm/RealmConfiguration;
    new-instance v1, Lio/realm/rx/RealmObservableFactory$5;

    invoke-direct {v1, p0, v0, p2}, Lio/realm/rx/RealmObservableFactory$5;-><init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;Lio/realm/RealmResults;)V

    invoke-static {v1}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 303
    const/16 v0, 0x25

    return v0
.end method
