.class public Lio/realm/internal/Util;
.super Ljava/lang/Object;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/Util$Testcase;
    }
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 27
    invoke-static {}, Lio/realm/internal/RealmCore;->loadLibrary()V

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method public static getNativeMemUsage()J
    .locals 2

    .prologue
    .line 31
    invoke-static {}, Lio/realm/internal/Util;->nativeGetMemUsage()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getOriginalModelClass(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 97
    .local v0, "superclass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    const-class v1, Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Lio/realm/RealmObject;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 98
    move-object p0, v0

    .line 101
    :cond_0
    return-object p0
.end method

.method public static getTablePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lio/realm/internal/Util;->nativeGetTablePrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static javaPrint(Ljava/lang/String;)V
    .locals 1
    .param p0, "txt"    # Ljava/lang/String;

    .prologue
    .line 43
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method static native nativeGetMemUsage()J
.end method

.method static native nativeGetTablePrefix()Ljava/lang/String;
.end method

.method static native nativeSetDebugLevel(I)V
.end method

.method static native nativeTestcase(IZJ)Ljava/lang/String;
.end method

.method public static setDebugLevel(I)V
    .locals 0
    .param p0, "level"    # I

    .prologue
    .line 37
    invoke-static {p0}, Lio/realm/internal/Util;->nativeSetDebugLevel(I)V

    .line 38
    return-void
.end method
