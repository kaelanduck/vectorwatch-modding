.class public Lio/realm/internal/android/AndroidLogger;
.super Ljava/lang/Object;
.source "AndroidLogger.java"

# interfaces
.implements Lio/realm/internal/log/Logger;


# static fields
.field private static final LOG_ENTRY_MAX_LENGTH:I = 0xfa0


# instance fields
.field private logTag:Ljava/lang/String;

.field private minimumLogLevel:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lio/realm/internal/android/AndroidLogger;->minimumLogLevel:I

    .line 28
    const-string v0, "REALM"

    iput-object v0, p0, Lio/realm/internal/android/AndroidLogger;->logTag:Ljava/lang/String;

    return-void
.end method

.method private log(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "logLevel"    # I
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 50
    iget v0, p0, Lio/realm/internal/android/AndroidLogger;->minimumLogLevel:I

    if-ge p1, v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 54
    :cond_2
    if-eqz p3, :cond_0

    .line 55
    invoke-static {p3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p2

    .line 63
    :cond_3
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xfa0

    if-ge v0, v1, :cond_5

    .line 64
    iget-object v0, p0, Lio/realm/internal/android/AndroidLogger;->logTag:Ljava/lang/String;

    invoke-static {p1, v0, p2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 59
    :cond_4
    if-eqz p3, :cond_3

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 66
    :cond_5
    iget-object v0, p0, Lio/realm/internal/android/AndroidLogger;->logTag:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lio/realm/internal/android/AndroidLogger;->logMessageIgnoringLimit(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private logMessageIgnoringLimit(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "logLevel"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 76
    :goto_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 77
    const/16 v3, 0xa

    invoke-virtual {p3, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 78
    .local v2, "nextNewLineIndex":I
    if-eq v2, v4, :cond_0

    move v0, v2

    .line 79
    .local v0, "chunkLength":I
    :goto_1
    const/16 v3, 0xfa0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 80
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "messageChunk":Ljava/lang/String;
    invoke-static {p1, p2, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 83
    if-eq v2, v4, :cond_1

    if-ne v2, v0, :cond_1

    .line 85
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p3, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 78
    .end local v0    # "chunkLength":I
    .end local v1    # "messageChunk":Ljava/lang/String;
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    .line 87
    .restart local v0    # "chunkLength":I
    .restart local v1    # "messageChunk":Ljava/lang/String;
    :cond_1
    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 90
    .end local v0    # "chunkLength":I
    .end local v1    # "messageChunk":Ljava/lang/String;
    .end local v2    # "nextNewLineIndex":I
    :cond_2
    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 109
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1, p2}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 110
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 134
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 139
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1, p2}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 114
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 119
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1, p2}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 120
    return-void
.end method

.method public setMinimumLogLevel(I)V
    .locals 0
    .param p1, "logLevel"    # I

    .prologue
    .line 45
    iput p1, p0, Lio/realm/internal/android/AndroidLogger;->minimumLogLevel:I

    .line 46
    return-void
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lio/realm/internal/android/AndroidLogger;->logTag:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public v(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 94
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 95
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 99
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 124
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 125
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 129
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1, p2}, Lio/realm/internal/android/AndroidLogger;->log(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 130
    return-void
.end method
