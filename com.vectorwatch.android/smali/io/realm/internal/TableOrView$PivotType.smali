.class public final enum Lio/realm/internal/TableOrView$PivotType;
.super Ljava/lang/Enum;
.source "TableOrView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/TableOrView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PivotType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/TableOrView$PivotType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/realm/internal/TableOrView$PivotType;

.field public static final enum AVG:Lio/realm/internal/TableOrView$PivotType;

.field public static final enum COUNT:Lio/realm/internal/TableOrView$PivotType;

.field public static final enum MAX:Lio/realm/internal/TableOrView$PivotType;

.field public static final enum MIN:Lio/realm/internal/TableOrView$PivotType;

.field public static final enum SUM:Lio/realm/internal/TableOrView$PivotType;


# instance fields
.field final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 367
    new-instance v0, Lio/realm/internal/TableOrView$PivotType;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v2, v2}, Lio/realm/internal/TableOrView$PivotType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/TableOrView$PivotType;->COUNT:Lio/realm/internal/TableOrView$PivotType;

    .line 368
    new-instance v0, Lio/realm/internal/TableOrView$PivotType;

    const-string v1, "SUM"

    invoke-direct {v0, v1, v3, v3}, Lio/realm/internal/TableOrView$PivotType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/TableOrView$PivotType;->SUM:Lio/realm/internal/TableOrView$PivotType;

    .line 369
    new-instance v0, Lio/realm/internal/TableOrView$PivotType;

    const-string v1, "AVG"

    invoke-direct {v0, v1, v4, v4}, Lio/realm/internal/TableOrView$PivotType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/TableOrView$PivotType;->AVG:Lio/realm/internal/TableOrView$PivotType;

    .line 370
    new-instance v0, Lio/realm/internal/TableOrView$PivotType;

    const-string v1, "MIN"

    invoke-direct {v0, v1, v5, v5}, Lio/realm/internal/TableOrView$PivotType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/TableOrView$PivotType;->MIN:Lio/realm/internal/TableOrView$PivotType;

    .line 371
    new-instance v0, Lio/realm/internal/TableOrView$PivotType;

    const-string v1, "MAX"

    invoke-direct {v0, v1, v6, v6}, Lio/realm/internal/TableOrView$PivotType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/TableOrView$PivotType;->MAX:Lio/realm/internal/TableOrView$PivotType;

    .line 366
    const/4 v0, 0x5

    new-array v0, v0, [Lio/realm/internal/TableOrView$PivotType;

    sget-object v1, Lio/realm/internal/TableOrView$PivotType;->COUNT:Lio/realm/internal/TableOrView$PivotType;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/internal/TableOrView$PivotType;->SUM:Lio/realm/internal/TableOrView$PivotType;

    aput-object v1, v0, v3

    sget-object v1, Lio/realm/internal/TableOrView$PivotType;->AVG:Lio/realm/internal/TableOrView$PivotType;

    aput-object v1, v0, v4

    sget-object v1, Lio/realm/internal/TableOrView$PivotType;->MIN:Lio/realm/internal/TableOrView$PivotType;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/internal/TableOrView$PivotType;->MAX:Lio/realm/internal/TableOrView$PivotType;

    aput-object v1, v0, v6

    sput-object v0, Lio/realm/internal/TableOrView$PivotType;->$VALUES:[Lio/realm/internal/TableOrView$PivotType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 375
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 376
    iput p3, p0, Lio/realm/internal/TableOrView$PivotType;->value:I

    .line 377
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/TableOrView$PivotType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 366
    const-class v0, Lio/realm/internal/TableOrView$PivotType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/TableOrView$PivotType;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/TableOrView$PivotType;
    .locals 1

    .prologue
    .line 366
    sget-object v0, Lio/realm/internal/TableOrView$PivotType;->$VALUES:[Lio/realm/internal/TableOrView$PivotType;

    invoke-virtual {v0}, [Lio/realm/internal/TableOrView$PivotType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/TableOrView$PivotType;

    return-object v0
.end method
