.class public final enum Lio/realm/internal/Util$Testcase;
.super Ljava/lang/Enum;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Testcase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/Util$Testcase;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_BadVersion:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_ClassNotFound:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_CrossTableLink:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_EncryptionNotSupported:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_FatalError:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_FileAccessError:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_FileNotFound:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_IOFailed:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_IllegalArgument:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_IndexOutOfBounds:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_NoSuchField:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_NoSuchMethod:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_OutOfMemory:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_RowInvalid:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_RuntimeError:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_TableInvalid:Lio/realm/internal/Util$Testcase;

.field public static final enum Exception_UnsupportedOperation:Lio/realm/internal/Util$Testcase;


# instance fields
.field private final nativeTestcase:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 54
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_ClassNotFound"

    invoke-direct {v0, v1, v4, v4}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_ClassNotFound:Lio/realm/internal/Util$Testcase;

    .line 55
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_NoSuchField"

    invoke-direct {v0, v1, v5, v5}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_NoSuchField:Lio/realm/internal/Util$Testcase;

    .line 56
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_NoSuchMethod"

    invoke-direct {v0, v1, v6, v6}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_NoSuchMethod:Lio/realm/internal/Util$Testcase;

    .line 57
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_IllegalArgument"

    invoke-direct {v0, v1, v7, v7}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_IllegalArgument:Lio/realm/internal/Util$Testcase;

    .line 58
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_IOFailed"

    invoke-direct {v0, v1, v8, v8}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_IOFailed:Lio/realm/internal/Util$Testcase;

    .line 59
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_FileNotFound"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_FileNotFound:Lio/realm/internal/Util$Testcase;

    .line 60
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_FileAccessError"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_FileAccessError:Lio/realm/internal/Util$Testcase;

    .line 61
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_IndexOutOfBounds"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_IndexOutOfBounds:Lio/realm/internal/Util$Testcase;

    .line 62
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_TableInvalid"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_TableInvalid:Lio/realm/internal/Util$Testcase;

    .line 63
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_UnsupportedOperation"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_UnsupportedOperation:Lio/realm/internal/Util$Testcase;

    .line 64
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_OutOfMemory"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_OutOfMemory:Lio/realm/internal/Util$Testcase;

    .line 65
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_FatalError"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_FatalError:Lio/realm/internal/Util$Testcase;

    .line 66
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_RuntimeError"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_RuntimeError:Lio/realm/internal/Util$Testcase;

    .line 67
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_RowInvalid"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_RowInvalid:Lio/realm/internal/Util$Testcase;

    .line 68
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_EncryptionNotSupported"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_EncryptionNotSupported:Lio/realm/internal/Util$Testcase;

    .line 69
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_CrossTableLink"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_CrossTableLink:Lio/realm/internal/Util$Testcase;

    .line 70
    new-instance v0, Lio/realm/internal/Util$Testcase;

    const-string v1, "Exception_BadVersion"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Util$Testcase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Util$Testcase;->Exception_BadVersion:Lio/realm/internal/Util$Testcase;

    .line 53
    const/16 v0, 0x11

    new-array v0, v0, [Lio/realm/internal/Util$Testcase;

    sget-object v1, Lio/realm/internal/Util$Testcase;->Exception_ClassNotFound:Lio/realm/internal/Util$Testcase;

    aput-object v1, v0, v4

    sget-object v1, Lio/realm/internal/Util$Testcase;->Exception_NoSuchField:Lio/realm/internal/Util$Testcase;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/internal/Util$Testcase;->Exception_NoSuchMethod:Lio/realm/internal/Util$Testcase;

    aput-object v1, v0, v6

    sget-object v1, Lio/realm/internal/Util$Testcase;->Exception_IllegalArgument:Lio/realm/internal/Util$Testcase;

    aput-object v1, v0, v7

    sget-object v1, Lio/realm/internal/Util$Testcase;->Exception_IOFailed:Lio/realm/internal/Util$Testcase;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_FileNotFound:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_FileAccessError:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_IndexOutOfBounds:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_TableInvalid:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_UnsupportedOperation:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_OutOfMemory:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_FatalError:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_RuntimeError:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_RowInvalid:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_EncryptionNotSupported:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_CrossTableLink:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lio/realm/internal/Util$Testcase;->Exception_BadVersion:Lio/realm/internal/Util$Testcase;

    aput-object v2, v0, v1

    sput-object v0, Lio/realm/internal/Util$Testcase;->$VALUES:[Lio/realm/internal/Util$Testcase;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nativeValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Lio/realm/internal/Util$Testcase;->nativeTestcase:I

    .line 75
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/Util$Testcase;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 53
    const-class v0, Lio/realm/internal/Util$Testcase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Util$Testcase;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/Util$Testcase;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lio/realm/internal/Util$Testcase;->$VALUES:[Lio/realm/internal/Util$Testcase;

    invoke-virtual {v0}, [Lio/realm/internal/Util$Testcase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/Util$Testcase;

    return-object v0
.end method


# virtual methods
.method public execute(J)Ljava/lang/String;
    .locals 3
    .param p1, "parm1"    # J

    .prologue
    .line 81
    iget v0, p0, Lio/realm/internal/Util$Testcase;->nativeTestcase:I

    const/4 v1, 0x1

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Util;->nativeTestcase(IZJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public expectedResult(J)Ljava/lang/String;
    .locals 3
    .param p1, "parm1"    # J

    .prologue
    .line 78
    iget v0, p0, Lio/realm/internal/Util$Testcase;->nativeTestcase:I

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Util;->nativeTestcase(IZJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
