.class public Lio/realm/internal/TableSpec$ColumnInfo;
.super Ljava/lang/Object;
.source "TableSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/TableSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ColumnInfo"
.end annotation


# instance fields
.field protected final name:Ljava/lang/String;

.field protected final tableSpec:Lio/realm/internal/TableSpec;

.field protected final type:Lio/realm/RealmFieldType;


# direct methods
.method public constructor <init>(Lio/realm/RealmFieldType;Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lio/realm/internal/TableSpec$ColumnInfo;->type:Lio/realm/RealmFieldType;

    .line 36
    sget-object v0, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    if-ne p1, v0, :cond_0

    new-instance v0, Lio/realm/internal/TableSpec;

    invoke-direct {v0}, Lio/realm/internal/TableSpec;-><init>()V

    :goto_0
    iput-object v0, p0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    .line 37
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p0, p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 54
    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 56
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 57
    check-cast v0, Lio/realm/internal/TableSpec$ColumnInfo;

    .line 58
    .local v0, "other":Lio/realm/internal/TableSpec$ColumnInfo;
    iget-object v3, p0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 59
    iget-object v3, v0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 60
    goto :goto_0

    .line 61
    :cond_4
    iget-object v3, p0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    iget-object v4, v0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 62
    goto :goto_0

    .line 63
    :cond_5
    iget-object v3, p0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    if-nez v3, :cond_6

    .line 64
    iget-object v3, v0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    if-eqz v3, :cond_7

    move v1, v2

    .line 65
    goto :goto_0

    .line 66
    :cond_6
    iget-object v3, p0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    iget-object v4, v0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    invoke-virtual {v3, v4}, Lio/realm/internal/TableSpec;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 67
    goto :goto_0

    .line 68
    :cond_7
    iget-object v3, p0, Lio/realm/internal/TableSpec$ColumnInfo;->type:Lio/realm/RealmFieldType;

    iget-object v4, v0, Lio/realm/internal/TableSpec$ColumnInfo;->type:Lio/realm/RealmFieldType;

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 69
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 41
    const/16 v0, 0x1f

    .line 42
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 43
    .local v1, "result":I
    iget-object v2, p0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 44
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 45
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lio/realm/internal/TableSpec$ColumnInfo;->type:Lio/realm/RealmFieldType;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 46
    return v1

    .line 43
    :cond_0
    iget-object v2, p0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 44
    :cond_1
    iget-object v2, p0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    invoke-virtual {v2}, Lio/realm/internal/TableSpec;->hashCode()I

    move-result v2

    goto :goto_1

    .line 45
    :cond_2
    iget-object v3, p0, Lio/realm/internal/TableSpec$ColumnInfo;->type:Lio/realm/RealmFieldType;

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->hashCode()I

    move-result v3

    goto :goto_2
.end method
