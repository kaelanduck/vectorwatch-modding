.class public Lio/realm/internal/Mixed;
.super Ljava/lang/Object;
.source "Mixed.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BINARY_TYPE_BYTE_ARRAY:I = 0x0

.field public static final BINARY_TYPE_BYTE_BUFFER:I = 0x1


# instance fields
.field private value:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lio/realm/internal/Mixed;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lio/realm/internal/Mixed;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(D)V
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 42
    return-void
.end method

.method public constructor <init>(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 38
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 34
    return-void
.end method

.method public constructor <init>(Lio/realm/RealmFieldType;)V
    .locals 1
    .param p1, "columnType"    # Lio/realm/RealmFieldType;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-eqz p1, :cond_0

    sget-object v0, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    if-ne p1, v0, :cond_1

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    sget-boolean v0, Lio/realm/internal/Mixed;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_0
    iput-object p1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "value"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    sget-boolean v0, Lio/realm/internal/Mixed;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 68
    :cond_0
    iput-object p1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/util/Date;)V
    .locals 1
    .param p1, "value"    # Ljava/util/Date;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget-boolean v0, Lio/realm/internal/Mixed;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_0
    iput-object p1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-eqz p1, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 54
    return-void

    .line 53
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "value"    # [B

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-boolean v0, Lio/realm/internal/Mixed;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_0
    iput-object p1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    .line 74
    return-void
.end method

.method public static mixedValue(Ljava/lang/Object;)Lio/realm/internal/Mixed;
    .locals 4
    .param p0, "value"    # Ljava/lang/Object;

    .prologue
    .line 133
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/lang/String;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-direct {v0, p0}, Lio/realm/internal/Mixed;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    .line 152
    :goto_0
    return-object p0

    .line 135
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 136
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/lang/Long;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lio/realm/internal/Mixed;-><init>(J)V

    move-object p0, v0

    goto :goto_0

    .line 137
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 138
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/lang/Integer;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Integer;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lio/realm/internal/Mixed;-><init>(J)V

    move-object p0, v0

    goto :goto_0

    .line 139
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 140
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/lang/Boolean;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v0, v1}, Lio/realm/internal/Mixed;-><init>(Z)V

    move-object p0, v0

    goto :goto_0

    .line 141
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_3
    instance-of v0, p0, Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 142
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/lang/Float;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {v0, v1}, Lio/realm/internal/Mixed;-><init>(F)V

    move-object p0, v0

    goto :goto_0

    .line 143
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_4
    instance-of v0, p0, Ljava/lang/Double;

    if-eqz v0, :cond_5

    .line 144
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/lang/Double;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lio/realm/internal/Mixed;-><init>(D)V

    move-object p0, v0

    goto :goto_0

    .line 145
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_5
    instance-of v0, p0, Ljava/util/Date;

    if-eqz v0, :cond_6

    .line 146
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/util/Date;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-direct {v0, p0}, Lio/realm/internal/Mixed;-><init>(Ljava/util/Date;)V

    move-object p0, v0

    goto :goto_0

    .line 147
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_6
    instance-of v0, p0, Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_7

    .line 148
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, Ljava/nio/ByteBuffer;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-direct {v0, p0}, Lio/realm/internal/Mixed;-><init>(Ljava/nio/ByteBuffer;)V

    move-object p0, v0

    goto :goto_0

    .line 149
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_7
    instance-of v0, p0, [B

    if-eqz v0, :cond_8

    .line 150
    new-instance v0, Lio/realm/internal/Mixed;

    check-cast p0, [B

    .end local p0    # "value":Ljava/lang/Object;
    check-cast p0, [B

    invoke-direct {v0, p0}, Lio/realm/internal/Mixed;-><init>([B)V

    move-object p0, v0

    goto :goto_0

    .line 151
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_8
    instance-of v0, p0, Lio/realm/internal/Mixed;

    if-eqz v0, :cond_9

    .line 152
    check-cast p0, Lio/realm/internal/Mixed;

    .end local p0    # "value":Ljava/lang/Object;
    check-cast p0, Lio/realm/internal/Mixed;

    goto/16 :goto_0

    .line 154
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The value is of unsupported type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 78
    if-ne p0, p1, :cond_1

    move v4, v3

    .line 97
    :cond_0
    :goto_0
    return v4

    .line 81
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    move-object v0, p1

    .line 85
    check-cast v0, Lio/realm/internal/Mixed;

    .line 87
    .local v0, "mixed":Lio/realm/internal/Mixed;
    iget-object v1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, v0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 91
    iget-object v1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_2

    .line 92
    iget-object v1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v1, [B

    check-cast v1, [B

    iget-object v2, v0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    goto :goto_0

    .line 94
    :cond_2
    iget-object v1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v1, v1, Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_4

    .line 95
    iget-object v1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v1, Ljava/nio/ByteBuffer;

    iget-object v2, v0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v2, Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->compareTo(Ljava/nio/ByteBuffer;)I

    move-result v1

    if-nez v1, :cond_3

    move v1, v3

    :goto_1
    move v4, v1

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1

    .line 97
    :cond_4
    iget-object v1, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    iget-object v2, v0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_0
.end method

.method public getBinaryByteArray()[B
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a byte[] from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getBinaryType()I
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 221
    :goto_0
    return v0

    .line 218
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    .line 219
    const/4 v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getBinaryValue()Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a ByteBuffer from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public getBooleanValue()Z
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a boolean from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected getDateTimeValue()J
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getDateValue()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDateValue()Ljava/util/Date;
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Date;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a Date from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getDoubleValue()D
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Double;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a double from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloatValue()F
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Float;

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a float from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getLongValue()J
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a long from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getReadableValue()Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v0

    .line 231
    .local v0, "type":Lio/realm/RealmFieldType;
    :try_start_0
    sget-object v1, Lio/realm/internal/Mixed$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v0}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v1, :pswitch_data_0

    .line 253
    :goto_0
    const-string v1, "ERROR"

    :goto_1
    return-object v1

    .line 233
    :pswitch_0
    :try_start_1
    const-string v1, "Binary"

    goto :goto_1

    .line 235
    :pswitch_1
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getBooleanValue()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 237
    :pswitch_2
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getDateValue()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 239
    :pswitch_3
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getDoubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 241
    :pswitch_4
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getFloatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 243
    :pswitch_5
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getLongValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 245
    :pswitch_6
    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 247
    :pswitch_7
    const-string v1, "Subtable"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 251
    :catch_0
    move-exception v1

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Lio/realm/internal/IllegalMixedTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a String from a Mixed containing a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/Mixed;->getType()Lio/realm/RealmFieldType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/internal/IllegalMixedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lio/realm/RealmFieldType;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 110
    sget-object v0, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    .line 125
    :goto_0
    return-object v0

    .line 112
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 113
    sget-object v0, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 114
    :cond_1
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 115
    sget-object v0, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 116
    :cond_2
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 117
    sget-object v0, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 118
    :cond_3
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 119
    sget-object v0, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 120
    :cond_4
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Date;

    if-eqz v0, :cond_5

    .line 121
    sget-object v0, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 122
    :cond_5
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 123
    sget-object v0, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 124
    :cond_6
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, Ljava/nio/ByteBuffer;

    if-nez v0, :cond_7

    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_8

    .line 125
    :cond_7
    sget-object v0, Lio/realm/RealmFieldType;->BINARY:Lio/realm/RealmFieldType;

    goto :goto_0

    .line 128
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown column type!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lio/realm/internal/Mixed;->value:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
