.class public interface abstract annotation Lio/realm/internal/DefineTable;
.super Ljava/lang/Object;
.source "DefineTable.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lio/realm/internal/DefineTable;
        query = ""
        row = ""
        table = ""
        view = ""
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract query()Ljava/lang/String;
.end method

.method public abstract row()Ljava/lang/String;
.end method

.method public abstract table()Ljava/lang/String;
.end method

.method public abstract view()Ljava/lang/String;
.end method
