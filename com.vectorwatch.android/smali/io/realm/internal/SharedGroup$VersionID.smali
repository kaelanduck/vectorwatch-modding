.class public Lio/realm/internal/SharedGroup$VersionID;
.super Ljava/lang/Object;
.source "SharedGroup.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/SharedGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VersionID"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lio/realm/internal/SharedGroup$VersionID;",
        ">;"
    }
.end annotation


# instance fields
.field final index:J

.field final version:J


# direct methods
.method constructor <init>(JJ)V
    .locals 1
    .param p1, "version"    # J
    .param p3, "index"    # J

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-wide p1, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    .line 255
    iput-wide p3, p0, Lio/realm/internal/SharedGroup$VersionID;->index:J

    .line 256
    return-void
.end method


# virtual methods
.method public compareTo(Lio/realm/internal/SharedGroup$VersionID;)I
    .locals 4
    .param p1, "another"    # Lio/realm/internal/SharedGroup$VersionID;

    .prologue
    .line 260
    iget-wide v0, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    iget-wide v2, p1, Lio/realm/internal/SharedGroup$VersionID;->version:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 261
    const/4 v0, 0x1

    .line 265
    :goto_0
    return v0

    .line 262
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    iget-wide v2, p1, Lio/realm/internal/SharedGroup$VersionID;->version:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 263
    const/4 v0, -0x1

    goto :goto_0

    .line 265
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 249
    check-cast p1, Lio/realm/internal/SharedGroup$VersionID;

    invoke-virtual {p0, p1}, Lio/realm/internal/SharedGroup$VersionID;->compareTo(Lio/realm/internal/SharedGroup$VersionID;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 279
    if-ne p0, p1, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v1

    .line 280
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    .line 281
    :cond_3
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 283
    check-cast v0, Lio/realm/internal/SharedGroup$VersionID;

    .line 284
    .local v0, "versionID":Lio/realm/internal/SharedGroup$VersionID;
    iget-wide v4, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    iget-wide v6, v0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_5

    iget-wide v4, p0, Lio/realm/internal/SharedGroup$VersionID;->index:J

    iget-wide v6, v0, Lio/realm/internal/SharedGroup$VersionID;->index:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 289
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 290
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    iget-wide v4, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 291
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lio/realm/internal/SharedGroup$VersionID;->index:J

    iget-wide v4, p0, Lio/realm/internal/SharedGroup$VersionID;->index:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 292
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VersionID{version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/realm/internal/SharedGroup$VersionID;->version:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/realm/internal/SharedGroup$VersionID;->index:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
