.class public Lio/realm/internal/Group;
.super Ljava/lang/Object;
.source "Group.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field public static final MODE_READONLY:I = 0x0

.field public static final MODE_READWRITE:I = 0x1

.field public static final MODE_READWRITE_NOCREATE:I = 0x2


# instance fields
.field private final context:Lio/realm/internal/Context;

.field protected immutable:Z

.field protected nativePtr:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/Group;->immutable:Z

    .line 49
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    .line 50
    invoke-virtual {p0}, Lio/realm/internal/Group;->createNative()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 51
    invoke-direct {p0}, Lio/realm/internal/Group;->checkNativePtrNotZero()V

    .line 52
    return-void
.end method

.method constructor <init>(Lio/realm/internal/Context;JZ)V
    .locals 0
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "nativePointer"    # J
    .param p4, "immutable"    # Z

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    .line 93
    iput-wide p2, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 94
    iput-boolean p4, p0, Lio/realm/internal/Group;->immutable:Z

    .line 95
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 66
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lio/realm/internal/Group;-><init>(Ljava/lang/String;I)V

    .line 67
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lio/realm/internal/Group;-><init>(Ljava/lang/String;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lio/realm/internal/Group;->immutable:Z

    .line 56
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    .line 57
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Group;->createNative(Ljava/lang/String;I)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 58
    invoke-direct {p0}, Lio/realm/internal/Group;->checkNativePtrNotZero()V

    .line 59
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/Group;->immutable:Z

    .line 82
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    .line 83
    if-eqz p1, :cond_0

    .line 84
    invoke-virtual {p0, p1}, Lio/realm/internal/Group;->createNative(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 85
    invoke-direct {p0}, Lio/realm/internal/Group;->checkNativePtrNotZero()V

    .line 89
    return-void

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/Group;->immutable:Z

    .line 71
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Lio/realm/internal/Group;->createNative([B)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 74
    invoke-direct {p0}, Lio/realm/internal/Group;->checkNativePtrNotZero()V

    .line 78
    return-void

    .line 76
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method private checkNativePtrNotZero()V
    .locals 4

    .prologue
    .line 39
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lio/realm/internal/OutOfMemoryError;

    const-string v1, "Out of native memory."

    invoke-direct {v0, v1}, Lio/realm/internal/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    return-void
.end method

.method protected static native nativeClose(J)V
.end method

.method private verifyGroupIsValid()V
    .locals 4

    .prologue
    .line 127
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Illegal to call methods on a closed Group."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 100
    iget-object v1, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 101
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 102
    iget-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-static {v2, v3}, Lio/realm/internal/Group;->nativeClose(J)V

    .line 103
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 105
    :cond_0
    monitor-exit v1

    .line 106
    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public commit()V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 262
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Group;->nativeCommit(J)V

    .line 263
    return-void
.end method

.method protected native createNative()J
.end method

.method protected native createNative(Ljava/lang/String;I)J
.end method

.method protected native createNative(Ljava/nio/ByteBuffer;)J
.end method

.method protected native createNative([B)J
.end method

.method protected finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 118
    iget-object v1, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 119
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    iget-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Context;->asyncDisposeGroup(J)V

    .line 121
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    .line 123
    :cond_0
    monitor-exit v1

    .line 124
    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTable(Ljava/lang/String;)Lio/realm/internal/Table;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 188
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 189
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v4, "Invalid name. Name must be a non-empty String."

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 192
    :cond_1
    iget-boolean v1, p0, Lio/realm/internal/Group;->immutable:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lio/realm/internal/Group;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 193
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Requested table is not in this Realm. Creating it requires a transaction: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :cond_2
    iget-object v1, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 199
    iget-wide v4, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v4, v5, p1}, Lio/realm/internal/Group;->nativeGetTableNativePtr(JLjava/lang/String;)J

    move-result-wide v2

    .line 202
    .local v2, "nativeTablePointer":J
    :try_start_0
    new-instance v1, Lio/realm/internal/Table;

    iget-object v4, p0, Lio/realm/internal/Group;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v4, p0, v2, v3}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 205
    throw v0
.end method

.method public getTableName(I)Ljava/lang/String;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 153
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 154
    invoke-virtual {p0}, Lio/realm/internal/Group;->size()J

    move-result-wide v0

    .line 155
    .local v0, "cnt":J
    if-ltz p1, :cond_0

    int-to-long v2, p1

    cmp-long v2, v2, v0

    if-ltz v2, :cond_1

    .line 156
    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Table index argument is out of range. possible range is [0, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v4, 0x1

    sub-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 160
    :cond_1
    iget-wide v2, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v2, v3, p1}, Lio/realm/internal/Group;->nativeGetTableName(JI)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public hasTable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 148
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 149
    if-eqz p1, :cond_0

    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1, p1}, Lio/realm/internal/Group;->nativeHasTable(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isClosed()Z
    .locals 4

    .prologue
    .line 114
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    .line 138
    invoke-virtual {p0}, Lio/realm/internal/Group;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isObjectTablesEmpty()Z
    .locals 2

    .prologue
    .line 246
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Group;->nativeIsEmpty(J)Z

    move-result v0

    return v0
.end method

.method protected native nativeCommit(J)V
.end method

.method protected native nativeGetTableName(JI)Ljava/lang/String;
.end method

.method protected native nativeGetTableNativePtr(JLjava/lang/String;)J
.end method

.method protected native nativeHasTable(JLjava/lang/String;)Z
.end method

.method protected native nativeIsEmpty(J)Z
.end method

.method protected native nativeLoadFromMem([B)J
.end method

.method native nativeRemoveTable(JLjava/lang/String;)V
.end method

.method native nativeRenameTable(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method protected native nativeSize(J)J
.end method

.method protected native nativeToJson(J)Ljava/lang/String;
.end method

.method protected native nativeToString(J)Ljava/lang/String;
.end method

.method protected native nativeWriteToFile(JLjava/lang/String;[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected native nativeWriteToMem(J)[B
.end method

.method public removeTable(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 167
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1, p1}, Lio/realm/internal/Group;->nativeRemoveTable(JLjava/lang/String;)V

    .line 168
    return-void
.end method

.method public renameTable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "oldName"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 176
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/Group;->nativeRenameTable(JLjava/lang/String;Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 134
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Group;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toJson()Ljava/lang/String;
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Group;->nativeToJson(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Group;->nativeToString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToFile(Ljava/io/File;[B)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;
    .param p2, "key"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 219
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The destination file must not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :cond_0
    if-eqz p2, :cond_1

    array-length v0, p2

    const/16 v1, 0x40

    if-eq v0, v1, :cond_1

    .line 223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Realm AES keys must be 64 bytes long"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_1
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lio/realm/internal/Group;->nativeWriteToFile(JLjava/lang/String;[B)V

    .line 227
    return-void
.end method

.method public writeToMem()[B
    .locals 2

    .prologue
    .line 235
    invoke-direct {p0}, Lio/realm/internal/Group;->verifyGroupIsValid()V

    .line 236
    iget-wide v0, p0, Lio/realm/internal/Group;->nativePtr:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Group;->nativeWriteToMem(J)[B

    move-result-object v0

    return-object v0
.end method
