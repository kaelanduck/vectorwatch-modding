.class public Lio/realm/internal/RealmCore;
.super Ljava/lang/Object;
.source "RealmCore.java"


# static fields
.field private static final BINARIES_PATH:Ljava/lang/String;

.field private static final FILE_SEP:Ljava/lang/String;

.field private static final JAVA_LIBRARY_PATH:Ljava/lang/String; = "java.library.path"

.field private static final PATH_SEP:Ljava/lang/String;

.field private static volatile libraryIsLoaded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    sput-object v0, Lio/realm/internal/RealmCore;->FILE_SEP:Ljava/lang/String;

    .line 33
    sget-object v0, Ljava/io/File;->pathSeparator:Ljava/lang/String;

    sput-object v0, Lio/realm/internal/RealmCore;->PATH_SEP:Ljava/lang/String;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "lib"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lio/realm/internal/RealmCore;->PATH_SEP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lio/realm/internal/RealmCore;->FILE_SEP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "lib"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/realm/internal/RealmCore;->BINARIES_PATH:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    sput-boolean v0, Lio/realm/internal/RealmCore;->libraryIsLoaded:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addNativeLibraryPath(Ljava/lang/String;)V
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 132
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "java.library.path"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lio/realm/internal/RealmCore;->PATH_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lio/realm/internal/RealmCore;->PATH_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "libraryPath":Ljava/lang/String;
    const-string v2, "java.library.path"

    invoke-static {v2, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    return-void

    .line 134
    .end local v1    # "libraryPath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Cannot set the library path!"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static varargs loadCorrectLibrary([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "libraryCandidateNames"    # [Ljava/lang/String;

    .prologue
    .line 120
    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p0, v1

    .line 122
    .local v0, "libraryCandidateName":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v0    # "libraryCandidateName":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 124
    .restart local v0    # "libraryCandidateName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 127
    .end local v0    # "libraryCandidateName":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized loadLibrary()V
    .locals 4

    .prologue
    .line 51
    const-class v3, Lio/realm/internal/RealmCore;

    monitor-enter v3

    :try_start_0
    sget-boolean v2, Lio/realm/internal/RealmCore;->libraryIsLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 74
    .local v0, "debug":Ljava/lang/String;
    .local v1, "jnilib":Ljava/lang/String;
    :goto_0
    monitor-exit v3

    return-void

    .line 57
    .end local v0    # "debug":Ljava/lang/String;
    .end local v1    # "jnilib":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-static {}, Lio/realm/internal/RealmCore;->osIsWindows()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    invoke-static {}, Lio/realm/internal/RealmCore;->loadLibraryWindows()Ljava/lang/String;

    .line 71
    .restart local v0    # "debug":Ljava/lang/String;
    .restart local v1    # "jnilib":Ljava/lang/String;
    :goto_1
    const/4 v2, 0x1

    sput-boolean v2, Lio/realm/internal/RealmCore;->libraryIsLoaded:Z

    .line 73
    const/4 v2, 0x1

    invoke-static {v2}, Lio/realm/internal/Version;->coreLibVersionCompatible(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 51
    .end local v1    # "jnilib":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 62
    .end local v0    # "debug":Ljava/lang/String;
    :cond_1
    :try_start_2
    const-string v2, "REALM_JAVA_DEBUG"

    invoke-static {v2}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    .restart local v0    # "debug":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 64
    :cond_2
    const-string v1, "realm-jni"

    .line 69
    .restart local v1    # "jnilib":Ljava/lang/String;
    :goto_2
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1

    .line 67
    .end local v1    # "jnilib":Ljava/lang/String;
    :cond_3
    const-string v1, "realm-jni-dbg"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v1    # "jnilib":Ljava/lang/String;
    goto :goto_2
.end method

.method public static declared-synchronized loadLibrary(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    const-class v1, Lio/realm/internal/RealmCore;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lio/realm/internal/RealmCore;->libraryIsLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 91
    :goto_0
    monitor-exit v1

    return-void

    .line 89
    :cond_0
    :try_start_1
    const-string v0, "realm-jni"

    invoke-static {p0, v0}, Lcom/getkeepsafe/relinker/ReLinker;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 90
    const/4 v0, 0x1

    sput-boolean v0, Lio/realm/internal/RealmCore;->libraryIsLoaded:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static loadLibraryWindows()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 96
    :try_start_0
    sget-object v1, Lio/realm/internal/RealmCore;->BINARIES_PATH:Ljava/lang/String;

    invoke-static {v1}, Lio/realm/internal/RealmCore;->addNativeLibraryPath(Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lio/realm/internal/RealmCore;->resetLibraryPath()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "realm_jni32d"

    aput-object v2, v1, v3

    const-string v2, "realm_jni64d"

    aput-object v2, v1, v4

    invoke-static {v1}, Lio/realm/internal/RealmCore;->loadCorrectLibrary([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "jnilib":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 107
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "!!! Realm debug version loaded. !!!\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 116
    :cond_0
    return-object v0

    .line 109
    :cond_1
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "realm_jni32"

    aput-object v2, v1, v3

    const-string v2, "realm_jni64"

    aput-object v2, v1, v4

    invoke-static {v1}, Lio/realm/internal/RealmCore;->loadCorrectLibrary([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    if-nez v0, :cond_0

    .line 111
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Searched java.library.path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "java.library.path"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 112
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t load the Realm JNI library \'realm_jni32.dll or realm_jni64.dll\'. Please include the directory to the library in java.library.path."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    .end local v0    # "jnilib":Ljava/lang/String;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static osIsWindows()Z
    .locals 3

    .prologue
    .line 40
    const-string v1, "os.name"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "os":Ljava/lang/String;
    const-string v1, "win"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method private static resetLibraryPath()V
    .locals 4

    .prologue
    .line 147
    :try_start_0
    const-class v2, Ljava/lang/ClassLoader;

    const-string v3, "sys_paths"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 148
    .local v1, "fieldSysPath":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 149
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Cannot reset the library path!"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
