.class public Lio/realm/internal/ReadTransaction;
.super Lio/realm/internal/Group;
.source "ReadTransaction.java"


# instance fields
.field private final db:Lio/realm/internal/SharedGroup;


# direct methods
.method constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/SharedGroup;J)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "db"    # Lio/realm/internal/SharedGroup;
    .param p3, "nativePointer"    # J

    .prologue
    .line 24
    const/4 v0, 0x1

    invoke-direct {p0, p1, p3, p4, v0}, Lio/realm/internal/Group;-><init>(Lio/realm/internal/Context;JZ)V

    .line 25
    iput-object p2, p0, Lio/realm/internal/ReadTransaction;->db:Lio/realm/internal/SharedGroup;

    .line 26
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lio/realm/internal/ReadTransaction;->db:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->endRead()V

    .line 35
    return-void
.end method

.method public endRead()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lio/realm/internal/ReadTransaction;->db:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->endRead()V

    .line 30
    return-void
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method
