.class public Lio/realm/internal/TableSpec;
.super Ljava/lang/Object;
.source "TableSpec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/TableSpec$ColumnInfo;
    }
.end annotation

.annotation build Lio/realm/internal/Keep;
.end annotation


# instance fields
.field private columnInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/internal/TableSpec$ColumnInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    .line 78
    return-void
.end method


# virtual methods
.method protected addColumn(ILjava/lang/String;)V
    .locals 1
    .param p1, "colTypeIndex"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-static {p1}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lio/realm/internal/TableSpec;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3f

    if-le v0, v1, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column names are currently limited to max 63 characters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iget-object v0, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    new-instance v1, Lio/realm/internal/TableSpec$ColumnInfo;

    invoke-direct {v1, p1, p2}, Lio/realm/internal/TableSpec$ColumnInfo;-><init>(Lio/realm/RealmFieldType;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    return-void
.end method

.method public addSubtableColumn(Ljava/lang/String;)Lio/realm/internal/TableSpec;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x3f

    if-le v1, v2, :cond_0

    .line 93
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Column names are currently limited to max 63 characters."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_0
    new-instance v0, Lio/realm/internal/TableSpec$ColumnInfo;

    sget-object v1, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    invoke-direct {v0, v1, p1}, Lio/realm/internal/TableSpec$ColumnInfo;-><init>(Lio/realm/RealmFieldType;Ljava/lang/String;)V

    .line 96
    .local v0, "columnInfo":Lio/realm/internal/TableSpec$ColumnInfo;
    iget-object v1, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v1, v0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    if-ne p0, p1, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v1

    .line 138
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 139
    goto :goto_0

    .line 140
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 141
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 142
    check-cast v0, Lio/realm/internal/TableSpec;

    .line 143
    .local v0, "other":Lio/realm/internal/TableSpec;
    iget-object v3, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    if-nez v3, :cond_4

    .line 144
    iget-object v3, v0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    if-eqz v3, :cond_0

    move v1, v2

    .line 145
    goto :goto_0

    .line 146
    :cond_4
    iget-object v3, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    iget-object v4, v0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 147
    goto :goto_0
.end method

.method public getColumnCount()J
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getColumnIndex(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 117
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 118
    iget-object v2, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/TableSpec$ColumnInfo;

    .line 119
    .local v0, "columnInfo":Lio/realm/internal/TableSpec$ColumnInfo;
    iget-object v2, v0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    int-to-long v2, v1

    .line 123
    .end local v0    # "columnInfo":Lio/realm/internal/TableSpec$ColumnInfo;
    :goto_1
    return-wide v2

    .line 117
    .restart local v0    # "columnInfo":Lio/realm/internal/TableSpec$ColumnInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    .end local v0    # "columnInfo":Lio/realm/internal/TableSpec$ColumnInfo;
    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_1
.end method

.method public getColumnName(J)Ljava/lang/String;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 113
    iget-object v0, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    long-to-int v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/TableSpec$ColumnInfo;

    iget-object v0, v0, Lio/realm/internal/TableSpec$ColumnInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getColumnType(J)Lio/realm/RealmFieldType;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 109
    iget-object v0, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    long-to-int v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/TableSpec$ColumnInfo;

    iget-object v0, v0, Lio/realm/internal/TableSpec$ColumnInfo;->type:Lio/realm/RealmFieldType;

    return-object v0
.end method

.method public getSubtableSpec(J)Lio/realm/internal/TableSpec;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 101
    iget-object v0, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    long-to-int v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/TableSpec$ColumnInfo;

    iget-object v0, v0, Lio/realm/internal/TableSpec$ColumnInfo;->tableSpec:Lio/realm/internal/TableSpec;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 128
    const/16 v0, 0x1f

    .line 129
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 130
    .local v1, "result":I
    iget-object v2, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 131
    return v1

    .line 130
    :cond_0
    iget-object v2, p0, Lio/realm/internal/TableSpec;->columnInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0
.end method
