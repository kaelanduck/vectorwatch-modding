.class public Lio/realm/internal/TableView;
.super Ljava/lang/Object;
.source "TableView.java"

# interfaces
.implements Lio/realm/internal/TableOrView;
.implements Ljava/io/Closeable;


# static fields
.field private static final DEBUG:Z


# instance fields
.field private final context:Lio/realm/internal/Context;

.field protected nativePtr:J

.field protected final parent:Lio/realm/internal/Table;

.field private final query:Lio/realm/internal/TableQuery;

.field private version:J


# direct methods
.method protected constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "parent"    # Lio/realm/internal/Table;
    .param p3, "nativePtr"    # J

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    .line 51
    iput-object p2, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    .line 52
    iput-wide p3, p0, Lio/realm/internal/TableView;->nativePtr:J

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/internal/TableView;->query:Lio/realm/internal/TableQuery;

    .line 54
    return-void
.end method

.method protected constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;JLio/realm/internal/TableQuery;)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "parent"    # Lio/realm/internal/Table;
    .param p3, "nativePtr"    # J
    .param p5, "query"    # Lio/realm/internal/TableQuery;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    .line 67
    iput-object p2, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    .line 68
    iput-wide p3, p0, Lio/realm/internal/TableView;->nativePtr:J

    .line 69
    iput-object p5, p0, Lio/realm/internal/TableView;->query:Lio/realm/internal/TableQuery;

    .line 70
    return-void
.end method

.method private native createNativeTableView(Lio/realm/internal/Table;J)J
.end method

.method private native nativeAverageDouble(JJ)D
.end method

.method private native nativeAverageFloat(JJ)D
.end method

.method private native nativeAverageInt(JJ)D
.end method

.method private native nativeClear(J)V
.end method

.method private native nativeClearSubtable(JJJ)V
.end method

.method static native nativeClose(J)V
.end method

.method private native nativeDistinct(JJ)J
.end method

.method private native nativeDistinctMulti(J[J)J
.end method

.method private native nativeFindAllBool(JJZ)J
.end method

.method private native nativeFindAllDate(JJJ)J
.end method

.method private native nativeFindAllDouble(JJD)J
.end method

.method private native nativeFindAllFloat(JJF)J
.end method

.method private native nativeFindAllInt(JJJ)J
.end method

.method private native nativeFindAllString(JJLjava/lang/String;)J
.end method

.method private native nativeFindBySourceNdx(JJ)J
.end method

.method private native nativeFindFirstBool(JJZ)J
.end method

.method private native nativeFindFirstDate(JJJ)J
.end method

.method private native nativeFindFirstDouble(JJD)J
.end method

.method private native nativeFindFirstFloat(JJF)J
.end method

.method private native nativeFindFirstInt(JJJ)J
.end method

.method private native nativeFindFirstString(JJLjava/lang/String;)J
.end method

.method private native nativeGetBoolean(JJJ)Z
.end method

.method private native nativeGetByteArray(JJJ)[B
.end method

.method private native nativeGetColumnCount(J)J
.end method

.method private native nativeGetColumnIndex(JLjava/lang/String;)J
.end method

.method private native nativeGetColumnName(JJ)Ljava/lang/String;
.end method

.method private native nativeGetColumnType(JJ)I
.end method

.method private native nativeGetDouble(JJJ)D
.end method

.method private native nativeGetFloat(JJJ)F
.end method

.method private native nativeGetLink(JJJ)J
.end method

.method private native nativeGetLong(JJJ)J
.end method

.method private native nativeGetMixed(JJJ)Lio/realm/internal/Mixed;
.end method

.method private native nativeGetMixedType(JJJ)I
.end method

.method private native nativeGetSourceRowIndex(JJ)J
.end method

.method private native nativeGetString(JJJ)Ljava/lang/String;
.end method

.method private native nativeGetSubtable(JJJ)J
.end method

.method private native nativeGetSubtableSize(JJJ)J
.end method

.method private native nativeGetTimestamp(JJJ)J
.end method

.method private native nativeIsNullLink(JJJ)Z
.end method

.method private native nativeMaximumDouble(JJ)Ljava/lang/Double;
.end method

.method private native nativeMaximumFloat(JJ)Ljava/lang/Float;
.end method

.method private native nativeMaximumInt(JJ)Ljava/lang/Long;
.end method

.method private native nativeMaximumTimestamp(JJ)Ljava/lang/Long;
.end method

.method private native nativeMinimumDouble(JJ)Ljava/lang/Double;
.end method

.method private native nativeMinimumFloat(JJ)Ljava/lang/Float;
.end method

.method private native nativeMinimumInt(JJ)Ljava/lang/Long;
.end method

.method private native nativeMinimumTimestamp(JJ)Ljava/lang/Long;
.end method

.method private native nativeNullifyLink(JJJ)V
.end method

.method private native nativePivot(JJJIJ)V
.end method

.method private native nativeRemoveRow(JJ)V
.end method

.method private native nativeRowToString(JJ)Ljava/lang/String;
.end method

.method private native nativeSetBoolean(JJJZ)V
.end method

.method private native nativeSetByteArray(JJJ[B)V
.end method

.method private native nativeSetDouble(JJJD)V
.end method

.method private native nativeSetFloat(JJJF)V
.end method

.method private native nativeSetLink(JJJJ)V
.end method

.method private native nativeSetLong(JJJJ)V
.end method

.method private native nativeSetMixed(JJJLio/realm/internal/Mixed;)V
.end method

.method private native nativeSetString(JJJLjava/lang/String;)V
.end method

.method private native nativeSetTimestampValue(JJJJ)V
.end method

.method private native nativeSize(J)J
.end method

.method private native nativeSort(JJZ)V
.end method

.method private native nativeSortMulti(J[J[Z)V
.end method

.method private native nativeSumDouble(JJ)D
.end method

.method private native nativeSumFloat(JJ)D
.end method

.method private native nativeSumInt(JJ)J
.end method

.method private native nativeSync(J)J
.end method

.method private native nativeSyncIfNeeded(J)J
.end method

.method private native nativeToJson(J)Ljava/lang/String;
.end method

.method private native nativeToString(JJ)Ljava/lang/String;
.end method

.method private native nativeWhere(J)J
.end method

.method private throwImmutable()V
    .locals 2

    .prologue
    .line 766
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm data can only be changed inside a write transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public averageDouble(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 677
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeAverageDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 654
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeAverageFloat(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageLong(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 632
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeAverageInt(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 442
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableView;->nativeClear(J)V

    .line 443
    return-void
.end method

.method public clearSubtable(JJ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 305
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 306
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeClearSubtable(JJJ)V

    .line 307
    return-void
.end method

.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 79
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 81
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-static {v2, v3}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 86
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    .line 88
    :cond_0
    monitor-exit v1

    .line 89
    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public count(JLjava/lang/String;)J
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 776
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented yet."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public distinct(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 806
    iget-object v0, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 807
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeDistinct(JJ)J

    .line 808
    return-void
.end method

.method public distinct(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 821
    .local p1, "columnIndexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v2}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 822
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [J

    .line 823
    .local v1, "indexes":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 824
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 823
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 826
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v2, v3, v1}, Lio/realm/internal/TableView;->nativeDistinctMulti(J[J)J

    .line 827
    return-void
.end method

.method protected finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 93
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 94
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Context;->asyncDisposeTableView(J)V

    .line 96
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    .line 98
    :cond_0
    monitor-exit v1

    .line 99
    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public findAllBoolean(JZ)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Z

    .prologue
    .line 536
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 537
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeFindAllBool(JJZ)J

    move-result-wide v8

    .line 539
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v2, v3, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 540
    :catch_0
    move-exception v0

    .line 541
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 542
    throw v0
.end method

.method public findAllDouble(JD)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 562
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 563
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeFindAllDouble(JJD)J

    move-result-wide v8

    .line 565
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v2, v3, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 566
    :catch_0
    move-exception v0

    .line 567
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 568
    throw v0
.end method

.method public findAllFloat(JF)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 549
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 550
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeFindAllFloat(JJF)J

    move-result-wide v8

    .line 552
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v2, v3, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 553
    :catch_0
    move-exception v0

    .line 554
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 555
    throw v0
.end method

.method public findAllLong(JJ)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 523
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 524
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeFindAllInt(JJJ)J

    move-result-wide v8

    .line 526
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v2, v3, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 529
    throw v0
.end method

.method public findAllString(JLjava/lang/String;)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 575
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 576
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeFindAllString(JJLjava/lang/String;)J

    move-result-wide v8

    .line 578
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v2, v3, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 581
    throw v0
.end method

.method public findFirstBoolean(JZ)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Z

    .prologue
    .line 481
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeFindFirstBool(JJZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstDate(JLjava/util/Date;)J
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "date"    # Ljava/util/Date;

    .prologue
    .line 497
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public findFirstDouble(JD)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 491
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeFindFirstDouble(JJD)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstFloat(JF)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 486
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeFindFirstFloat(JJF)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 476
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeFindFirstInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstString(JLjava/lang/String;)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 503
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getBinaryByteArray(JJ)[B
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 266
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetByteArray(JJJ)[B

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 197
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetBoolean(JJJ)Z

    move-result v0

    return v0
.end method

.method public getColumnCount()J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableView;->nativeGetColumnCount(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getColumnIndex(Ljava/lang/String;)J
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column name can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableView;->nativeGetColumnIndex(JLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getColumnName(J)Ljava/lang/String;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 149
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumnType(J)Lio/realm/RealmFieldType;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 173
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeGetColumnType(JJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public getDate(JJ)Ljava/util/Date;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 233
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetTimestamp(JJJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getDouble(JJ)D
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 221
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetDouble(JJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(JJ)F
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 209
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetFloat(JJJ)F

    move-result v0

    return v0
.end method

.method public getLink(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 280
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetLink(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 185
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetLong(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMixed(JJ)Lio/realm/internal/Mixed;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 276
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetMixed(JJJ)Lio/realm/internal/Mixed;

    move-result-object v0

    return-object v0
.end method

.method public getMixedType(JJ)Lio/realm/RealmFieldType;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 271
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetMixedType(JJJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public getSourceRowIndex(J)J
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 128
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeGetSourceRowIndex(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getString(JJ)Ljava/lang/String;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 245
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetString(JJJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubtable(JJ)Lio/realm/internal/Table;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 286
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 287
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetSubtable(JJJ)J

    move-result-wide v8

    .line 290
    .local v8, "nativeSubtablePtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/Table;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v2, v3, v8, v9}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 292
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 294
    throw v0
.end method

.method public getSubtableSize(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 300
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeGetSubtableSize(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 781
    iget-wide v0, p0, Lio/realm/internal/TableView;->version:J

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    .line 108
    invoke-virtual {p0}, Lio/realm/internal/TableView;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNullLink(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 431
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeIsNullLink(JJJ)Z

    move-result v0

    return v0
.end method

.method public lowerBoundLong(JJ)J
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 511
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public maximumDate(J)Ljava/util/Date;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 684
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/TableView;->nativeMaximumTimestamp(JJ)Ljava/lang/Long;

    move-result-object v0

    .line 685
    .local v0, "result":Ljava/lang/Long;
    if-nez v0, :cond_0

    .line 686
    const/4 v1, 0x0

    .line 688
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public maximumDouble(J)Ljava/lang/Double;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 666
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeMaximumDouble(JJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(J)Ljava/lang/Float;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 644
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeMaximumFloat(JJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumLong(J)Ljava/lang/Long;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 613
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeMaximumInt(JJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumDate(J)Ljava/util/Date;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 693
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/TableView;->nativeMinimumTimestamp(JJ)Ljava/lang/Long;

    move-result-object v0

    .line 694
    .local v0, "result":Ljava/lang/Long;
    if-nez v0, :cond_0

    .line 695
    const/4 v1, 0x0

    .line 697
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public minimumDouble(J)Ljava/lang/Double;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 672
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeMinimumDouble(JJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(J)Ljava/lang/Float;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 649
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeMinimumFloat(JJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumLong(J)Ljava/lang/Long;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 627
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeMinimumInt(JJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public nullifyLink(JJ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 435
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableView;->nativeNullifyLink(JJJ)V

    .line 436
    return-void
.end method

.method public pivot(JJLio/realm/internal/TableOrView$PivotType;)Lio/realm/internal/Table;
    .locals 13
    .param p1, "stringCol"    # J
    .param p3, "intCol"    # J
    .param p5, "pivotType"    # Lio/realm/internal/TableOrView$PivotType;

    .prologue
    .line 786
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/TableView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {v3, v4}, Lio/realm/RealmFieldType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 787
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Group by column must be of type String"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 788
    :cond_0
    move-wide/from16 v0, p3

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/TableView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {v3, v4}, Lio/realm/RealmFieldType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 789
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Aggregation column must be of type Int"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 790
    :cond_1
    new-instance v2, Lio/realm/internal/Table;

    invoke-direct {v2}, Lio/realm/internal/Table;-><init>()V

    .line 791
    .local v2, "result":Lio/realm/internal/Table;
    iget-wide v4, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object/from16 v0, p5

    iget v10, v0, Lio/realm/internal/TableOrView$PivotType;->value:I

    iget-wide v11, v2, Lio/realm/internal/Table;->nativePtr:J

    move-object v3, p0

    move-wide v6, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v3 .. v12}, Lio/realm/internal/TableView;->nativePivot(JJJIJ)V

    .line 792
    return-object v2
.end method

.method public remove(J)V
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 453
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 454
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeRemoveRow(JJ)V

    .line 455
    return-void
.end method

.method public removeFirst()V
    .locals 4

    .prologue
    .line 459
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 460
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/TableView;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 461
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableView;->nativeRemoveRow(JJ)V

    .line 463
    :cond_1
    return-void
.end method

.method public removeLast()V
    .locals 6

    .prologue
    .line 467
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 468
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/TableView;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 469
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-virtual {p0}, Lio/realm/internal/TableView;->size()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableView;->nativeRemoveRow(JJ)V

    .line 471
    :cond_1
    return-void
.end method

.method public rowToString(J)Ljava/lang/String;
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 737
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeRowToString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setBinaryByteArray(JJ[B)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "data"    # [B

    .prologue
    .line 408
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 409
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/TableView;->nativeSetByteArray(JJJ[B)V

    .line 410
    return-void
.end method

.method public setBoolean(JJZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Z

    .prologue
    .line 333
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 334
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/TableView;->nativeSetBoolean(JJJZ)V

    .line 335
    return-void
.end method

.method public setDate(JJLjava/util/Date;)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Ljava/util/Date;

    .prologue
    .line 372
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 373
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-virtual/range {p5 .. p5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableView;->nativeSetTimestampValue(JJJJ)V

    .line 374
    return-void
.end method

.method public setDouble(JJD)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # D

    .prologue
    .line 359
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 360
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableView;->nativeSetDouble(JJJD)V

    .line 361
    return-void
.end method

.method public setFloat(JJF)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # F

    .prologue
    .line 346
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 347
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/TableView;->nativeSetFloat(JJJF)V

    .line 348
    return-void
.end method

.method public setLink(JJJ)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # J

    .prologue
    .line 426
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 427
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableView;->nativeSetLink(JJJJ)V

    .line 428
    return-void
.end method

.method public setLong(JJJ)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # J

    .prologue
    .line 320
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 321
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableView;->nativeSetLong(JJJJ)V

    .line 322
    return-void
.end method

.method public setMixed(JJLio/realm/internal/Mixed;)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "data"    # Lio/realm/internal/Mixed;

    .prologue
    .line 421
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 422
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/TableView;->nativeSetMixed(JJJLio/realm/internal/Mixed;)V

    .line 423
    return-void
.end method

.method public setString(JJLjava/lang/String;)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Ljava/lang/String;

    .prologue
    .line 385
    iget-object v0, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableView;->throwImmutable()V

    .line 386
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/TableView;->nativeSetString(JJJLjava/lang/String;)V

    .line 387
    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableView;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public sort(J)V
    .locals 7
    .param p1, "columnIndex"    # J

    .prologue
    .line 708
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeSort(JJZ)V

    .line 709
    return-void
.end method

.method public sort(JLio/realm/Sort;)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "sortOrder"    # Lio/realm/Sort;

    .prologue
    .line 703
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Sort;->getValue()Z

    move-result v6

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;->nativeSort(JJZ)V

    .line 704
    return-void
.end method

.method public sort(Ljava/util/List;[Lio/realm/Sort;)V
    .locals 6
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;[",
            "Lio/realm/Sort;",
            ")V"
        }
    .end annotation

    .prologue
    .line 712
    .local p1, "columnIndices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-array v1, v3, [J

    .line 713
    .local v1, "indices":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 714
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v1, v0

    .line 713
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 716
    :cond_0
    invoke-static {p2}, Lio/realm/internal/TableQuery;->getNativeSortOrderValues([Lio/realm/Sort;)[Z

    move-result-object v2

    .line 717
    .local v2, "nativeSortOrder":[Z
    iget-wide v4, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v4, v5, v1, v2}, Lio/realm/internal/TableView;->nativeSortMulti(J[J[Z)V

    .line 718
    return-void
.end method

.method public sourceRowIndex(J)J
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 761
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeFindBySourceNdx(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public sumDouble(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 661
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeSumDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 639
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeSumFloat(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumLong(J)J
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 599
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeSumInt(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public syncIfNeeded()J
    .locals 2

    .prologue
    .line 831
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableView;->nativeSyncIfNeeded(J)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/TableView;->version:J

    .line 832
    iget-wide v0, p0, Lio/realm/internal/TableView;->version:J

    return-wide v0
.end method

.method public toJson()Ljava/lang/String;
    .locals 2

    .prologue
    .line 722
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableView;->nativeToJson(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 727
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    const-wide/16 v2, 0x1f4

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableView;->nativeToString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(J)Ljava/lang/String;
    .locals 3
    .param p1, "maxRows"    # J

    .prologue
    .line 732
    iget-wide v0, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableView;->nativeToString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public upperBoundLong(JJ)J
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 517
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public where()Lio/realm/internal/TableQuery;
    .locals 7

    .prologue
    .line 743
    iget-object v1, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 744
    iget-wide v2, p0, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/TableView;->nativeWhere(J)J

    move-result-wide v4

    .line 746
    .local v4, "nativeQueryPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableQuery;

    iget-object v2, p0, Lio/realm/internal/TableView;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableView;->parent:Lio/realm/internal/Table;

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;JLio/realm/internal/TableOrView;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 747
    :catch_0
    move-exception v0

    .line 748
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v4, v5}, Lio/realm/internal/TableQuery;->nativeClose(J)V

    .line 749
    throw v0
.end method
