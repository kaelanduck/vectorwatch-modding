.class public Lio/realm/internal/TableQuery;
.super Ljava/lang/Object;
.source "TableQuery.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final DATE_NULL_ERROR_MESSAGE:Ljava/lang/String; = "Date value in query criteria must not be null."

.field private static final STRING_NULL_ERROR_MESSAGE:Ljava/lang/String; = "String value in query criteria must not be null."


# instance fields
.field protected DEBUG:Z

.field private final context:Lio/realm/internal/Context;

.field protected nativePtr:J

.field private final origin:Lio/realm/internal/TableOrView;

.field private queryValidated:Z

.field protected final table:Lio/realm/internal/Table;


# direct methods
.method public constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    .locals 3
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "table"    # Lio/realm/internal/Table;
    .param p3, "nativeQueryPtr"    # J

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 44
    iget-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "++++++ new TableQuery, ptr= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 47
    :cond_0
    iput-object p1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    .line 48
    iput-object p2, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    .line 49
    iput-wide p3, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/internal/TableQuery;->origin:Lio/realm/internal/TableOrView;

    .line 51
    return-void
.end method

.method public constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;JLio/realm/internal/TableOrView;)V
    .locals 3
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "table"    # Lio/realm/internal/Table;
    .param p3, "nativeQueryPtr"    # J
    .param p5, "origin"    # Lio/realm/internal/TableOrView;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 54
    iget-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 55
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "++++++ new TableQuery, ptr= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 57
    :cond_0
    iput-object p1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    .line 58
    iput-object p2, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    .line 59
    iput-wide p3, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    .line 60
    iput-object p5, p0, Lio/realm/internal/TableQuery;->origin:Lio/realm/internal/TableOrView;

    .line 61
    return-void
.end method

.method public static getNativeSortOrderValues([Lio/realm/Sort;)[Z
    .locals 3
    .param p0, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 733
    array-length v2, p0

    new-array v1, v2, [Z

    .line 734
    .local v1, "nativeValues":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 735
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lio/realm/Sort;->getValue()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 734
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 737
    :cond_0
    return-object v1
.end method

.method private native nativeAverageDouble(JJJJJ)D
.end method

.method private native nativeAverageFloat(JJJJJ)D
.end method

.method private native nativeAverageInt(JJJJJ)D
.end method

.method public static native nativeBatchUpdateQueries(J[J[[J[[J[[Z)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method private native nativeBeginsWith(J[JLjava/lang/String;Z)V
.end method

.method private native nativeBetween(J[JDD)V
.end method

.method private native nativeBetween(J[JFF)V
.end method

.method private native nativeBetween(J[JJJ)V
.end method

.method private native nativeBetweenTimestamp(J[JJJ)V
.end method

.method protected static native nativeClose(J)V
.end method

.method public static native nativeCloseQueryHandover(J)V
.end method

.method private native nativeContains(J[JLjava/lang/String;Z)V
.end method

.method private native nativeCount(JJJJ)J
.end method

.method private native nativeEndGroup(J)V
.end method

.method private native nativeEndsWith(J[JLjava/lang/String;Z)V
.end method

.method private native nativeEqual(J[JD)V
.end method

.method private native nativeEqual(J[JF)V
.end method

.method private native nativeEqual(J[JJ)V
.end method

.method private native nativeEqual(J[JLjava/lang/String;Z)V
.end method

.method private native nativeEqual(J[JZ)V
.end method

.method private native nativeEqualTimestamp(J[JJ)V
.end method

.method private native nativeFind(JJ)J
.end method

.method private native nativeFindAll(JJJJ)J
.end method

.method public static native nativeFindAllMultiSortedWithHandover(JJJJJ[J[Z)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method public static native nativeFindAllSortedWithHandover(JJJJJJZ)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method public static native nativeFindAllWithHandover(JJJJJ)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method public static native nativeFindWithHandover(JJJ)J
.end method

.method public static native nativeGetDistinctViewWithHandover(JJJ)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method private native nativeGreater(J[JD)V
.end method

.method private native nativeGreater(J[JF)V
.end method

.method private native nativeGreater(J[JJ)V
.end method

.method private native nativeGreaterEqual(J[JD)V
.end method

.method private native nativeGreaterEqual(J[JF)V
.end method

.method private native nativeGreaterEqual(J[JJ)V
.end method

.method private native nativeGreaterEqualTimestamp(J[JJ)V
.end method

.method private native nativeGreaterTimestamp(J[JJ)V
.end method

.method private native nativeGroup(J)V
.end method

.method private native nativeHandoverQuery(JJ)J
.end method

.method public static native nativeImportHandoverRowIntoSharedGroup(JJ)J
.end method

.method private native nativeImportHandoverTableViewIntoSharedGroup(JJ)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method private native nativeIsEmpty(J[J)V
.end method

.method private native nativeIsNotNull(J[J)V
.end method

.method private native nativeIsNull(J[J)V
.end method

.method private native nativeLess(J[JD)V
.end method

.method private native nativeLess(J[JF)V
.end method

.method private native nativeLess(J[JJ)V
.end method

.method private native nativeLessEqual(J[JD)V
.end method

.method private native nativeLessEqual(J[JF)V
.end method

.method private native nativeLessEqual(J[JJ)V
.end method

.method private native nativeLessEqualTimestamp(J[JJ)V
.end method

.method private native nativeLessTimestamp(J[JJ)V
.end method

.method private native nativeMaximumDouble(JJJJJ)Ljava/lang/Double;
.end method

.method private native nativeMaximumFloat(JJJJJ)Ljava/lang/Float;
.end method

.method private native nativeMaximumInt(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeMinimumDouble(JJJJJ)Ljava/lang/Double;
.end method

.method private native nativeMinimumFloat(JJJJJ)Ljava/lang/Float;
.end method

.method private native nativeMinimumInt(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeMinimumTimestamp(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeNot(J)V
.end method

.method private native nativeNotEqual(J[JD)V
.end method

.method private native nativeNotEqual(J[JF)V
.end method

.method private native nativeNotEqual(J[JJ)V
.end method

.method private native nativeNotEqual(J[JLjava/lang/String;Z)V
.end method

.method private native nativeNotEqualTimestamp(J[JJ)V
.end method

.method private native nativeOr(J)V
.end method

.method private native nativeParent(J)V
.end method

.method private native nativeRemove(JJJJ)J
.end method

.method private native nativeSubtable(JJ)V
.end method

.method private native nativeSumDouble(JJJJJ)D
.end method

.method private native nativeSumFloat(JJJJJ)D
.end method

.method private native nativeSumInt(JJJJJ)J
.end method

.method private native nativeTableview(JJ)V
.end method

.method private native nativeValidateQuery(J)Ljava/lang/String;
.end method

.method private throwImmutable()V
    .locals 2

    .prologue
    .line 741
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Mutable method call during read transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private validateQuery()V
    .locals 4

    .prologue
    .line 89
    iget-boolean v1, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    if-nez v1, :cond_0

    .line 90
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/TableQuery;->nativeValidateQuery(J)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "invalidMessage":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    const/4 v1, 0x1

    iput-boolean v1, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 96
    .end local v0    # "invalidMessage":Ljava/lang/String;
    :cond_0
    return-void

    .line 94
    .restart local v0    # "invalidMessage":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public averageDouble(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 650
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 651
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageDouble(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 646
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 647
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 612
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 613
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 608
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 609
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageInt(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 574
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 575
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageInt(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageInt(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 570
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 571
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageInt(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public beginsWith([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 380
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeBeginsWith(J[JLjava/lang/String;Z)V

    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 382
    return-object p0
.end method

.method public beginsWith([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 374
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeBeginsWith(J[JLjava/lang/String;Z)V

    .line 375
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 376
    return-object p0
.end method

.method public between([JDD)Lio/realm/internal/TableQuery;
    .locals 8
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # D
    .param p4, "value2"    # D

    .prologue
    .line 269
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetween(J[JDD)V

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 271
    return-object p0
.end method

.method public between([JFF)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # F
    .param p3, "value2"    # F

    .prologue
    .line 225
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeBetween(J[JFF)V

    .line 226
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 227
    return-object p0
.end method

.method public between([JJJ)Lio/realm/internal/TableQuery;
    .locals 8
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # J
    .param p4, "value2"    # J

    .prologue
    .line 181
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetween(J[JJJ)V

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 183
    return-object p0
.end method

.method public between([JLjava/util/Date;Ljava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 8
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # Ljava/util/Date;
    .param p3, "value2"    # Ljava/util/Date;

    .prologue
    .line 337
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 338
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date values in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :cond_1
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetweenTimestamp(J[JJJ)V

    .line 340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 341
    return-object p0
.end method

.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 64
    iget-object v1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 65
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 66
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-static {v2, v3}, Lio/realm/internal/TableQuery;->nativeClose(J)V

    .line 68
    iget-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 69
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "++++ Query CLOSE, ptr= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 71
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    .line 73
    :cond_1
    monitor-exit v1

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public contains([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 404
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeContains(J[JLjava/lang/String;Z)V

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 406
    return-object p0
.end method

.method public contains([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 398
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeContains(J[JLjava/lang/String;Z)V

    .line 399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 400
    return-object p0
.end method

.method public count()J
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    .line 712
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 713
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeCount(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JJJ)J
    .locals 11
    .param p1, "start"    # J
    .param p3, "end"    # J
    .param p5, "limit"    # J

    .prologue
    .line 707
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 708
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeCount(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public endGroup()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeEndGroup(J)V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 115
    return-object p0
.end method

.method public endSubtable()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeParent(J)V

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 127
    return-object p0
.end method

.method public endsWith([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 392
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEndsWith(J[JLjava/lang/String;Z)V

    .line 393
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 394
    return-object p0
.end method

.method public endsWith([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 386
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEndsWith(J[JLjava/lang/String;Z)V

    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 388
    return-object p0
.end method

.method public equalTo([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 233
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeEqual(J[JD)V

    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 235
    return-object p0
.end method

.method public equalTo([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 189
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeEqual(J[JF)V

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 191
    return-object p0
.end method

.method public equalTo([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndexes"    # [J
    .param p2, "value"    # J

    .prologue
    .line 145
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeEqual(J[JJ)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 147
    return-object p0
.end method

.method public equalTo([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndexes"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 356
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEqual(J[JLjava/lang/String;Z)V

    .line 357
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 358
    return-object p0
.end method

.method public equalTo([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndexes"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 350
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEqual(J[JLjava/lang/String;Z)V

    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 352
    return-object p0
.end method

.method public equalTo([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 287
    if-nez p2, :cond_0

    .line 288
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsNull(J[J)V

    .line 292
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 293
    return-object p0

    .line 290
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeEqualTimestamp(J[JJ)V

    goto :goto_0
.end method

.method public equalTo([JZ)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Z

    .prologue
    .line 277
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeEqual(J[JZ)V

    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 279
    return-object p0
.end method

.method protected finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 77
    iget-object v1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Context;->asyncDisposeQuery(J)V

    .line 80
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    .line 82
    :cond_0
    monitor-exit v1

    .line 83
    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public find()J
    .locals 4

    .prologue
    .line 427
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 428
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableQuery;->nativeFind(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public find(J)J
    .locals 3
    .param p1, "fromTableRow"    # J

    .prologue
    .line 422
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 423
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeFind(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findAll()Lio/realm/internal/TableView;
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    .line 461
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 464
    iget-object v1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 465
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeFindAll(JJJJ)J

    move-result-wide v4

    .line 467
    .local v4, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;JLio/realm/internal/TableQuery;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 468
    :catch_0
    move-exception v0

    .line 469
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v4, v5}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 470
    throw v0
.end method

.method public findAll(JJJ)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "start"    # J
    .param p3, "end"    # J
    .param p5, "limit"    # J

    .prologue
    .line 447
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 450
    iget-object v1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 451
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeFindAll(JJJJ)J

    move-result-wide v4

    .line 453
    .local v4, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    iget-object v3, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;JLio/realm/internal/TableQuery;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v4, v5}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 456
    throw v0
.end method

.method public findAllMultiSortedWithHandover(JJJ[J[Lio/realm/Sort;)J
    .locals 13
    .param p1, "bgSharedGroupPtr"    # J
    .param p3, "nativeReplicationPtr"    # J
    .param p5, "ptrQuery"    # J
    .param p7, "columnIndices"    # [J
    .param p8, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 499
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 501
    iget-object v0, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 502
    invoke-static/range {p8 .. p8}, Lio/realm/internal/TableQuery;->getNativeSortOrderValues([Lio/realm/Sort;)[Z

    move-result-object v11

    .line 503
    .local v11, "ascendings":[Z
    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    move-wide v0, p1

    move-wide/from16 v2, p5

    move-object/from16 v10, p7

    invoke-static/range {v0 .. v11}, Lio/realm/internal/TableQuery;->nativeFindAllMultiSortedWithHandover(JJJJJ[J[Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public findAllSortedWithHandover(JJJJLio/realm/Sort;)J
    .locals 13
    .param p1, "bgSharedGroupPtr"    # J
    .param p3, "nativeReplicationPtr"    # J
    .param p5, "ptrQuery"    # J
    .param p7, "columnIndex"    # J
    .param p9, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 492
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 494
    iget-object v0, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 495
    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual/range {p9 .. p9}, Lio/realm/Sort;->getValue()Z

    move-result v12

    move-wide v0, p1

    move-wide/from16 v2, p5

    move-wide/from16 v10, p7

    invoke-static/range {v0 .. v12}, Lio/realm/internal/TableQuery;->nativeFindAllSortedWithHandover(JJJJJJZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findAllWithHandover(JJJ)J
    .locals 11
    .param p1, "bgSharedGroupPtr"    # J
    .param p3, "nativeReplicationPtr"    # J
    .param p5, "ptrQuery"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 478
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 480
    iget-object v0, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 481
    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    move-wide v0, p1

    move-wide/from16 v2, p5

    invoke-static/range {v0 .. v9}, Lio/realm/internal/TableQuery;->nativeFindAllWithHandover(JJJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findDistinctWithHandover(JJJJ)J
    .locals 7
    .param p1, "bgSharedGroupPtr"    # J
    .param p3, "nativeReplicationPtr"    # J
    .param p5, "ptrQuery"    # J
    .param p7, "columnIndex"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 485
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 487
    iget-object v0, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    move-wide v0, p1

    move-wide v2, p5

    move-wide v4, p7

    .line 488
    invoke-static/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGetDistinctViewWithHandover(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findWithHandover(JJJ)J
    .locals 7
    .param p1, "bgSharedGroupPtr"    # J
    .param p3, "nativeReplicationPtr"    # J
    .param p5, "ptrQuery"    # J

    .prologue
    .line 440
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 442
    iget-object v0, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 443
    const-wide/16 v4, 0x0

    move-wide v0, p1

    move-wide v2, p5

    invoke-static/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeFindWithHandover(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public greaterThan([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 245
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreater(J[JD)V

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 247
    return-object p0
.end method

.method public greaterThan([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 201
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeGreater(J[JF)V

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 203
    return-object p0
.end method

.method public greaterThan([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 157
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreater(J[JJ)V

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 159
    return-object p0
.end method

.method public greaterThan([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 305
    if-nez p2, :cond_0

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterTimestamp(J[JJ)V

    .line 308
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 309
    return-object p0
.end method

.method public greaterThanOrEqual([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 251
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterEqual(J[JD)V

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 253
    return-object p0
.end method

.method public greaterThanOrEqual([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 207
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeGreaterEqual(J[JF)V

    .line 208
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 209
    return-object p0
.end method

.method public greaterThanOrEqual([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 163
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterEqual(J[JJ)V

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 165
    return-object p0
.end method

.method public greaterThanOrEqual([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 313
    if-nez p2, :cond_0

    .line 314
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 315
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterEqualTimestamp(J[JJ)V

    .line 316
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 317
    return-object p0
.end method

.method public group()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeGroup(J)V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 109
    return-object p0
.end method

.method public handoverQuery(J)J
    .locals 3
    .param p1, "callerSharedGroupPtr"    # J

    .prologue
    .line 533
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, p1, p2, v0, v1}, Lio/realm/internal/TableQuery;->nativeHandoverQuery(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public importHandoverTableView(JJ)Lio/realm/internal/TableView;
    .locals 7
    .param p1, "handoverPtr"    # J
    .param p3, "callerSharedGroupPtr"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 515
    invoke-direct {p0, p1, p2, p3, p4}, Lio/realm/internal/TableQuery;->nativeImportHandoverTableViewIntoSharedGroup(JJ)J

    move-result-wide v2

    .line 517
    .local v2, "nativeTvPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v4, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    iget-object v5, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    invoke-direct {v1, v4, v5, v2, v3}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 518
    :catch_0
    move-exception v0

    .line 519
    .local v0, "e":Ljava/lang/RuntimeException;
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 520
    invoke-static {v2, v3}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 522
    :cond_0
    throw v0
.end method

.method public isEmpty([J)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J

    .prologue
    .line 410
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsEmpty(J[J)V

    .line 411
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 412
    return-object p0
.end method

.method public isNotEmpty([J)Lio/realm/internal/TableQuery;
    .locals 1
    .param p1, "columnIndices"    # [J

    .prologue
    .line 416
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->not()Lio/realm/internal/TableQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/internal/TableQuery;->isEmpty([J)Lio/realm/internal/TableQuery;

    move-result-object v0

    return-object v0
.end method

.method public isNotNull([J)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J

    .prologue
    .line 698
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsNotNull(J[J)V

    .line 699
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 700
    return-object p0
.end method

.method public isNull([J)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J

    .prologue
    .line 692
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsNull(J[J)V

    .line 693
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 694
    return-object p0
.end method

.method public lessThan([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 257
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLess(J[JD)V

    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 259
    return-object p0
.end method

.method public lessThan([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 213
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeLess(J[JF)V

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 215
    return-object p0
.end method

.method public lessThan([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 169
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLess(J[JJ)V

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 171
    return-object p0
.end method

.method public lessThan([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 321
    if-nez p2, :cond_0

    .line 322
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessTimestamp(J[JJ)V

    .line 324
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 325
    return-object p0
.end method

.method public lessThanOrEqual([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 263
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessEqual(J[JD)V

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 265
    return-object p0
.end method

.method public lessThanOrEqual([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 219
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeLessEqual(J[JF)V

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 221
    return-object p0
.end method

.method public lessThanOrEqual([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 175
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessEqual(J[JJ)V

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 177
    return-object p0
.end method

.method public lessThanOrEqual([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 329
    if-nez p2, :cond_0

    .line 330
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessEqualTimestamp(J[JJ)V

    .line 332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 333
    return-object p0
.end method

.method public maximumDate(J)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 665
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 666
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 667
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 668
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 670
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public maximumDate(JJJJ)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 657
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 658
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 659
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 660
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 662
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public maximumDouble(J)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 632
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 633
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumDouble(JJJJ)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 628
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 629
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(J)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 594
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 595
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(JJJJ)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 590
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 591
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumInt(J)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 556
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 557
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public maximumInt(JJJJ)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 552
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 553
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumDate(J)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 682
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 683
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 684
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 685
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 687
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public minimumDate(JJJJ)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 674
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 675
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 676
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 677
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 679
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public minimumDouble(J)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 641
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 642
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumDouble(JJJJ)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 637
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 638
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(J)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 603
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 604
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(JJJJ)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 599
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 600
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumInt(J)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 565
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 566
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumInt(JJJJ)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 561
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 562
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public not()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeNot(J)V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 139
    return-object p0
.end method

.method public notEqualTo([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 239
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JD)V

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 241
    return-object p0
.end method

.method public notEqualTo([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 195
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JF)V

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 197
    return-object p0
.end method

.method public notEqualTo([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 151
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JJ)V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 153
    return-object p0
.end method

.method public notEqualTo([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 368
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JLjava/lang/String;Z)V

    .line 369
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 370
    return-object p0
.end method

.method public notEqualTo([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 363
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JLjava/lang/String;Z)V

    .line 364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 365
    return-object p0
.end method

.method public notEqualTo([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 297
    if-nez p2, :cond_0

    .line 298
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeNotEqualTimestamp(J[JJ)V

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 301
    return-object p0
.end method

.method public or()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeOr(J)V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 133
    return-object p0
.end method

.method public remove()J
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    .line 724
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 725
    iget-object v0, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableQuery;->throwImmutable()V

    .line 726
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeRemove(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public remove(JJ)J
    .locals 11
    .param p1, "start"    # J
    .param p3, "end"    # J

    .prologue
    .line 718
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 719
    iget-object v0, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableQuery;->throwImmutable()V

    .line 720
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v8, -0x1

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeRemove(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public subtable(J)Lio/realm/internal/TableQuery;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 119
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeSubtable(JJ)V

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 121
    return-object p0
.end method

.method public sumDouble(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 623
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 624
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumDouble(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 619
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 620
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 585
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 586
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 581
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 582
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumInt(J)J
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 547
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 548
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumInt(JJJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public sumInt(JJJJ)J
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 543
    invoke-direct {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 544
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumInt(JJJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public tableview(Lio/realm/internal/TableView;)Lio/realm/internal/TableQuery;
    .locals 4
    .param p1, "tv"    # Lio/realm/internal/TableView;

    .prologue
    .line 100
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    iget-wide v2, p1, Lio/realm/internal/TableView;->nativePtr:J

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableQuery;->nativeTableview(JJ)V

    .line 101
    return-object p0
.end method
