.class public Lio/realm/internal/SharedGroup;
.super Ljava/lang/Object;
.source "SharedGroup.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/SharedGroup$VersionID;,
        Lio/realm/internal/SharedGroup$Durability;
    }
.end annotation


# static fields
.field private static final CREATE_FILE_NO:Z = true

.field private static final CREATE_FILE_YES:Z = false

.field private static final DISABLE_REPLICATION:Z = false

.field private static final ENABLE_REPLICATION:Z = true

.field public static final EXPLICIT_TRANSACTION:Z = false

.field public static final IMPLICIT_TRANSACTION:Z = true


# instance fields
.field private activeTransaction:Z

.field private final context:Lio/realm/internal/Context;

.field private implicitTransactionsEnabled:Z

.field private nativePtr:J

.field private nativeReplicationPtr:J

.field private final path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "databaseFile"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v3, p0, Lio/realm/internal/SharedGroup;->implicitTransactionsEnabled:Z

    .line 55
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    .line 56
    iput-object p1, p0, Lio/realm/internal/SharedGroup;->path:Ljava/lang/String;

    .line 57
    sget-object v0, Lio/realm/internal/SharedGroup$Durability;->FULL:Lio/realm/internal/SharedGroup$Durability;

    iget v2, v0, Lio/realm/internal/SharedGroup$Durability;->value:I

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/SharedGroup;->nativeCreate(Ljava/lang/String;IZZ[B)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    .line 58
    invoke-direct {p0}, Lio/realm/internal/SharedGroup;->checkNativePtrNotZero()V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lio/realm/internal/SharedGroup$Durability;[B)V
    .locals 6
    .param p1, "canonicalPath"    # Ljava/lang/String;
    .param p2, "durability"    # Lio/realm/internal/SharedGroup$Durability;
    .param p3, "key"    # [B

    .prologue
    const/4 v3, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v3, p0, Lio/realm/internal/SharedGroup;->implicitTransactionsEnabled:Z

    .line 76
    iput-object p1, p0, Lio/realm/internal/SharedGroup;->path:Ljava/lang/String;

    .line 77
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    .line 78
    iget v2, p2, Lio/realm/internal/SharedGroup$Durability;->value:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/SharedGroup;->nativeCreate(Ljava/lang/String;IZZ[B)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    .line 79
    invoke-direct {p0}, Lio/realm/internal/SharedGroup;->checkNativePtrNotZero()V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V
    .locals 6
    .param p1, "canonicalPath"    # Ljava/lang/String;
    .param p2, "enableImplicitTransactions"    # Z
    .param p3, "durability"    # Lio/realm/internal/SharedGroup$Durability;
    .param p4, "key"    # [B

    .prologue
    const/4 v3, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v3, p0, Lio/realm/internal/SharedGroup;->implicitTransactionsEnabled:Z

    .line 62
    if-eqz p2, :cond_0

    .line 63
    invoke-direct {p0, p1, p4}, Lio/realm/internal/SharedGroup;->nativeCreateReplication(Ljava/lang/String;[B)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    .line 64
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    iget v2, p3, Lio/realm/internal/SharedGroup$Durability;->value:I

    invoke-direct {p0, v0, v1, v2, p4}, Lio/realm/internal/SharedGroup;->createNativeWithImplicitTransactions(JI[B)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/SharedGroup;->implicitTransactionsEnabled:Z

    .line 70
    :goto_0
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    .line 71
    iput-object p1, p0, Lio/realm/internal/SharedGroup;->path:Ljava/lang/String;

    .line 72
    invoke-direct {p0}, Lio/realm/internal/SharedGroup;->checkNativePtrNotZero()V

    .line 73
    return-void

    .line 68
    :cond_0
    sget-object v0, Lio/realm/internal/SharedGroup$Durability;->FULL:Lio/realm/internal/SharedGroup$Durability;

    iget v2, v0, Lio/realm/internal/SharedGroup$Durability;->value:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/SharedGroup;->nativeCreate(Ljava/lang/String;IZZ[B)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    goto :goto_0
.end method

.method private checkNativePtrNotZero()V
    .locals 4

    .prologue
    .line 230
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Ljava/io/IOError;

    new-instance v1, Lio/realm/exceptions/RealmIOException;

    const-string v2, "Realm could not be opened"

    invoke-direct {v1, v2}, Lio/realm/exceptions/RealmIOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/IOError;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 233
    :cond_0
    return-void
.end method

.method private native createNativeWithImplicitTransactions(JI[B)J
.end method

.method private native nativeAdvanceRead(J)V
.end method

.method private native nativeAdvanceReadToVersion(JJJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation
.end method

.method private native nativeBeginImplicit(J)J
.end method

.method private native nativeBeginRead(J)J
.end method

.method private native nativeBeginWrite(J)J
.end method

.method protected static native nativeClose(J)V
.end method

.method private native nativeCloseReplication(J)V
.end method

.method private native nativeCommit(J)V
.end method

.method private native nativeCommitAndContinueAsRead(J)V
.end method

.method private native nativeCompact(J)Z
.end method

.method private native nativeCreate(Ljava/lang/String;IZZ[B)J
.end method

.method private native nativeCreateReplication(Ljava/lang/String;[B)J
.end method

.method private native nativeEndRead(J)V
.end method

.method private native nativeGetDefaultReplicationDatabaseFileName()Ljava/lang/String;
.end method

.method private native nativeGetVersionID(J)[J
.end method

.method private native nativeHasChanged(J)Z
.end method

.method private native nativePromoteToWrite(J)V
.end method

.method private native nativeReserve(JJ)V
.end method

.method private native nativeRollback(J)V
.end method

.method private native nativeRollbackAndContinueAsRead(J)V
.end method

.method private native nativeStopWaitForChange(J)V
.end method

.method private native nativeWaitForChange(J)Z
.end method


# virtual methods
.method advanceRead()V
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeAdvanceRead(J)V

    .line 84
    return-void
.end method

.method advanceRead(Lio/realm/internal/SharedGroup$VersionID;)V
    .locals 8
    .param p1, "versionID"    # Lio/realm/internal/SharedGroup$VersionID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 87
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    iget-wide v4, p1, Lio/realm/internal/SharedGroup$VersionID;->version:J

    iget-wide v6, p1, Lio/realm/internal/SharedGroup$VersionID;->index:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/SharedGroup;->nativeAdvanceReadToVersion(JJJ)V

    .line 88
    return-void
.end method

.method public beginImplicitTransaction()Lio/realm/internal/ImplicitTransaction;
    .locals 6

    .prologue
    .line 103
    iget-boolean v3, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    if-eqz v3, :cond_0

    .line 104
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Can\'t beginImplicitTransaction() during another active transaction"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 107
    :cond_0
    iget-wide v4, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v4, v5}, Lio/realm/internal/SharedGroup;->nativeBeginImplicit(J)J

    move-result-wide v0

    .line 108
    .local v0, "nativeGroupPtr":J
    new-instance v2, Lio/realm/internal/ImplicitTransaction;

    iget-object v3, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    invoke-direct {v2, v3, p0, v0, v1}, Lio/realm/internal/ImplicitTransaction;-><init>(Lio/realm/internal/Context;Lio/realm/internal/SharedGroup;J)V

    .line 109
    .local v2, "transaction":Lio/realm/internal/ImplicitTransaction;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    .line 110
    return-object v2
.end method

.method public beginRead()Lio/realm/internal/ReadTransaction;
    .locals 6

    .prologue
    .line 132
    iget-boolean v4, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    if-eqz v4, :cond_0

    .line 133
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Can\'t beginRead() during another active transaction"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 136
    :cond_0
    iget-wide v4, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v4, v5}, Lio/realm/internal/SharedGroup;->nativeBeginRead(J)J

    move-result-wide v2

    .line 139
    .local v2, "nativeReadPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/ReadTransaction;

    iget-object v4, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v4, p0, v2, v3}, Lio/realm/internal/ReadTransaction;-><init>(Lio/realm/internal/Context;Lio/realm/internal/SharedGroup;J)V

    .line 140
    .local v1, "t":Lio/realm/internal/ReadTransaction;
    const/4 v4, 0x1

    iput-boolean v4, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    return-object v1

    .line 142
    .end local v1    # "t":Lio/realm/internal/ReadTransaction;
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/Group;->nativeClose(J)V

    .line 144
    throw v0
.end method

.method public beginWrite()Lio/realm/internal/WriteTransaction;
    .locals 6

    .prologue
    .line 114
    iget-boolean v4, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    if-eqz v4, :cond_0

    .line 115
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Can\'t beginWrite() during another active transaction"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 119
    :cond_0
    iget-wide v4, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v4, v5}, Lio/realm/internal/SharedGroup;->nativeBeginWrite(J)J

    move-result-wide v2

    .line 122
    .local v2, "nativeWritePtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/WriteTransaction;

    iget-object v4, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v4, p0, v2, v3}, Lio/realm/internal/WriteTransaction;-><init>(Lio/realm/internal/Context;Lio/realm/internal/SharedGroup;J)V

    .line 123
    .local v1, "t":Lio/realm/internal/WriteTransaction;
    const/4 v4, 0x1

    iput-boolean v4, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    return-object v1

    .line 125
    .end local v1    # "t":Lio/realm/internal/WriteTransaction;
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/Group;->nativeClose(J)V

    .line 127
    throw v0
.end method

.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 157
    iget-object v1, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 159
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-static {v2, v3}, Lio/realm/internal/SharedGroup;->nativeClose(J)V

    .line 160
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    .line 161
    iget-boolean v0, p0, Lio/realm/internal/SharedGroup;->implicitTransactionsEnabled:Z

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 162
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/SharedGroup;->nativeCloseReplication(J)V

    .line 163
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    .line 166
    :cond_0
    monitor-exit v1

    .line 167
    return-void

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method commit()V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t commit() on closed group. WriteTransaction is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeCommit(J)V

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    .line 188
    return-void
.end method

.method commitAndContinueAsRead()V
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeCommitAndContinueAsRead(J)V

    .line 96
    return-void
.end method

.method public compact()Z
    .locals 2

    .prologue
    .line 217
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeCompact(J)Z

    move-result v0

    return v0
.end method

.method endRead()V
    .locals 2

    .prologue
    .line 149
    invoke-virtual {p0}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t endRead() on closed group. ReadTransaction is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeEndRead(J)V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    .line 154
    return-void
.end method

.method protected finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 170
    iget-object v1, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 171
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lio/realm/internal/SharedGroup;->context:Lio/realm/internal/Context;

    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Context;->asyncDisposeSharedGroup(J)V

    .line 173
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    .line 174
    iget-boolean v0, p0, Lio/realm/internal/SharedGroup;->implicitTransactionsEnabled:Z

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 175
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/SharedGroup;->nativeCloseReplication(J)V

    .line 176
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    .line 179
    :cond_0
    monitor-exit v1

    .line 180
    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNativePointer()J
    .locals 2

    .prologue
    .line 236
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    return-wide v0
.end method

.method public getNativeReplicationPointer()J
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativeReplicationPtr:J

    return-wide v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lio/realm/internal/SharedGroup;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Lio/realm/internal/SharedGroup$VersionID;
    .locals 6

    .prologue
    .line 244
    iget-wide v2, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/SharedGroup;->nativeGetVersionID(J)[J

    move-result-object v0

    .line 245
    .local v0, "versionId":[J
    new-instance v1, Lio/realm/internal/SharedGroup$VersionID;

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    const/4 v4, 0x1

    aget-wide v4, v0, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lio/realm/internal/SharedGroup$VersionID;-><init>(JJ)V

    return-object v1
.end method

.method public hasChanged()Z
    .locals 2

    .prologue
    .line 203
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeHasChanged(J)Z

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 4

    .prologue
    .line 199
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method promoteToWrite()V
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativePromoteToWrite(J)V

    .line 92
    return-void
.end method

.method public reserve(J)V
    .locals 3
    .param p1, "bytes"    # J

    .prologue
    .line 207
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/SharedGroup;->nativeReserve(JJ)V

    .line 208
    return-void
.end method

.method rollback()V
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p0}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t rollback() on closed group. WriteTransaction is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeRollback(J)V

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/SharedGroup;->activeTransaction:Z

    .line 196
    return-void
.end method

.method rollbackAndContinueAsRead()V
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeRollbackAndContinueAsRead(J)V

    .line 100
    return-void
.end method

.method public stopWaitForChange()V
    .locals 2

    .prologue
    .line 309
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeStopWaitForChange(J)V

    .line 310
    return-void
.end method

.method public waitForChange()Z
    .locals 2

    .prologue
    .line 302
    iget-wide v0, p0, Lio/realm/internal/SharedGroup;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/SharedGroup;->nativeWaitForChange(J)Z

    move-result v0

    return v0
.end method
