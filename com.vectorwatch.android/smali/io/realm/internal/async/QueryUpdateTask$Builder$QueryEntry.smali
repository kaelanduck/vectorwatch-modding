.class Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
.super Ljava/lang/Object;
.source "QueryUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/async/QueryUpdateTask$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryEntry"
.end annotation


# instance fields
.field final element:Ljava/lang/ref/WeakReference;

.field handoverQueryPointer:J

.field final queryArguments:Lio/realm/internal/async/ArgumentsHolder;


# direct methods
.method private constructor <init>(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)V
    .locals 0
    .param p1, "element"    # Ljava/lang/ref/WeakReference;
    .param p2, "handoverQueryPointer"    # J
    .param p4, "queryArguments"    # Lio/realm/internal/async/ArgumentsHolder;

    .prologue
    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334
    iput-object p1, p0, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->element:Ljava/lang/ref/WeakReference;

    .line 335
    iput-wide p2, p0, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    .line 336
    iput-object p4, p0, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    .line 337
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;Lio/realm/internal/async/QueryUpdateTask$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/ref/WeakReference;
    .param p2, "x1"    # J
    .param p4, "x2"    # Lio/realm/internal/async/ArgumentsHolder;
    .param p5, "x3"    # Lio/realm/internal/async/QueryUpdateTask$1;

    .prologue
    .line 328
    invoke-direct {p0, p1, p2, p3, p4}, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;-><init>(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)V

    return-void
.end method
