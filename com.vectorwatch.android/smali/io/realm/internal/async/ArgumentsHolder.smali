.class public Lio/realm/internal/async/ArgumentsHolder;
.super Ljava/lang/Object;
.source "ArgumentsHolder.java"


# static fields
.field public static final TYPE_DISTINCT:I = 0x4

.field public static final TYPE_FIND_ALL:I = 0x0

.field public static final TYPE_FIND_ALL_MULTI_SORTED:I = 0x2

.field public static final TYPE_FIND_ALL_SORTED:I = 0x1

.field public static final TYPE_FIND_FIRST:I = 0x3


# instance fields
.field public columnIndex:J

.field public columnIndices:[J

.field public sortOrder:Lio/realm/Sort;

.field public sortOrders:[Lio/realm/Sort;

.field public final type:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lio/realm/internal/async/ArgumentsHolder;->type:I

    .line 39
    return-void
.end method
