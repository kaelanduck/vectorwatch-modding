.class public Lio/realm/internal/async/QueryUpdateTask;
.super Ljava/lang/Object;
.source "QueryUpdateTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/async/QueryUpdateTask$Builder;,
        Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;,
        Lio/realm/internal/async/QueryUpdateTask$Result;
    }
.end annotation


# static fields
.field private static final MODE_UPDATE_REALM_OBJECT:I = 0x1

.field private static final MODE_UPDATE_REALM_RESULTS:I


# instance fields
.field private callerHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private message:I

.field private realmConfiguration:Lio/realm/RealmConfiguration;

.field private realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

.field private realmResultsEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final updateMode:I


# direct methods
.method private constructor <init>(ILio/realm/RealmConfiguration;Ljava/util/List;Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;Ljava/lang/ref/WeakReference;I)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "realmConfiguration"    # Lio/realm/RealmConfiguration;
    .param p4, "realmObject"    # Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
    .param p6, "message"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lio/realm/RealmConfiguration;",
            "Ljava/util/List",
            "<",
            "Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;",
            ">;",
            "Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p3, "listOfRealmResults":Ljava/util/List;, "Ljava/util/List<Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;>;"
    .local p5, "handler":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lio/realm/internal/async/QueryUpdateTask;->updateMode:I

    .line 58
    iput-object p2, p0, Lio/realm/internal/async/QueryUpdateTask;->realmConfiguration:Lio/realm/RealmConfiguration;

    .line 59
    iput-object p3, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    .line 60
    iput-object p4, p0, Lio/realm/internal/async/QueryUpdateTask;->realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    .line 61
    iput-object p5, p0, Lio/realm/internal/async/QueryUpdateTask;->callerHandler:Ljava/lang/ref/WeakReference;

    .line 62
    iput p6, p0, Lio/realm/internal/async/QueryUpdateTask;->message:I

    .line 63
    return-void
.end method

.method synthetic constructor <init>(ILio/realm/RealmConfiguration;Ljava/util/List;Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;Ljava/lang/ref/WeakReference;ILio/realm/internal/async/QueryUpdateTask$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lio/realm/RealmConfiguration;
    .param p3, "x2"    # Ljava/util/List;
    .param p4, "x3"    # Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
    .param p5, "x4"    # Ljava/lang/ref/WeakReference;
    .param p6, "x5"    # I
    .param p7, "x6"    # Lio/realm/internal/async/QueryUpdateTask$1;

    .prologue
    .line 38
    invoke-direct/range {p0 .. p6}, Lio/realm/internal/async/QueryUpdateTask;-><init>(ILio/realm/RealmConfiguration;Ljava/util/List;Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;Ljava/lang/ref/WeakReference;I)V

    return-void
.end method

.method private isAliveHandler(Landroid/os/Handler;)Z
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 203
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTaskCancelled()Z
    .locals 1

    .prologue
    .line 199
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    return v0
.end method

.method public static newBuilder()Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lio/realm/internal/async/QueryUpdateTask$Builder$Steps;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lio/realm/internal/async/QueryUpdateTask$Builder$Steps;-><init>(Lio/realm/internal/async/QueryUpdateTask$1;)V

    return-object v0
.end method

.method private prepareQueriesParameters()Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;
    .locals 12

    .prologue
    .line 114
    iget-object v7, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    new-array v1, v7, [J

    .line 115
    .local v1, "handoverQueries":[J
    iget-object v7, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x6

    filled-new-array {v7, v8}, [I

    move-result-object v7

    sget-object v8, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v8, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [[J

    .line 116
    .local v5, "queriesParameters":[[J
    iget-object v7, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    new-array v3, v7, [[J

    .line 117
    .local v3, "multiSortColumnIndices":[[J
    iget-object v7, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    new-array v4, v7, [[Z

    .line 119
    .local v4, "multiSortOrder":[[Z
    const/4 v2, 0x0

    .line 120
    .local v2, "i":I
    iget-object v7, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    .line 121
    .local v6, "queryEntry":Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
    iget-object v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget v8, v8, Lio/realm/internal/async/ArgumentsHolder;->type:I

    packed-switch v8, :pswitch_data_0

    .line 156
    :pswitch_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Query mode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget v9, v9, Lio/realm/internal/async/ArgumentsHolder;->type:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " not supported"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 123
    :pswitch_1
    iget-wide v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    aput-wide v8, v1, v2

    .line 124
    aget-object v8, v5, v2

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    aput-wide v10, v8, v9

    .line 125
    aget-object v8, v5, v2

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    aput-wide v10, v8, v9

    .line 126
    aget-object v8, v5, v2

    const/4 v9, 0x2

    const-wide/16 v10, -0x1

    aput-wide v10, v8, v9

    .line 127
    aget-object v8, v5, v2

    const/4 v9, 0x3

    const-wide/16 v10, -0x1

    aput-wide v10, v8, v9

    .line 158
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 159
    goto :goto_0

    .line 131
    :pswitch_2
    iget-wide v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    aput-wide v8, v1, v2

    .line 132
    aget-object v8, v5, v2

    const/4 v9, 0x0

    const-wide/16 v10, 0x4

    aput-wide v10, v8, v9

    .line 133
    aget-object v8, v5, v2

    const/4 v9, 0x1

    iget-object v10, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget-wide v10, v10, Lio/realm/internal/async/ArgumentsHolder;->columnIndex:J

    aput-wide v10, v8, v9

    goto :goto_1

    .line 137
    :pswitch_3
    iget-wide v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    aput-wide v8, v1, v2

    .line 138
    aget-object v8, v5, v2

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    aput-wide v10, v8, v9

    .line 139
    aget-object v8, v5, v2

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    aput-wide v10, v8, v9

    .line 140
    aget-object v8, v5, v2

    const/4 v9, 0x2

    const-wide/16 v10, -0x1

    aput-wide v10, v8, v9

    .line 141
    aget-object v8, v5, v2

    const/4 v9, 0x3

    const-wide/16 v10, -0x1

    aput-wide v10, v8, v9

    .line 142
    aget-object v8, v5, v2

    const/4 v9, 0x4

    iget-object v10, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget-wide v10, v10, Lio/realm/internal/async/ArgumentsHolder;->columnIndex:J

    aput-wide v10, v8, v9

    .line 143
    aget-object v10, v5, v2

    const/4 v11, 0x5

    iget-object v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget-object v8, v8, Lio/realm/internal/async/ArgumentsHolder;->sortOrder:Lio/realm/Sort;

    invoke-virtual {v8}, Lio/realm/Sort;->getValue()Z

    move-result v8

    if-eqz v8, :cond_0

    const-wide/16 v8, 0x1

    :goto_2
    aput-wide v8, v10, v11

    goto :goto_1

    :cond_0
    const-wide/16 v8, 0x0

    goto :goto_2

    .line 147
    :pswitch_4
    iget-wide v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    aput-wide v8, v1, v2

    .line 148
    aget-object v8, v5, v2

    const/4 v9, 0x0

    const-wide/16 v10, 0x2

    aput-wide v10, v8, v9

    .line 149
    aget-object v8, v5, v2

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    aput-wide v10, v8, v9

    .line 150
    aget-object v8, v5, v2

    const/4 v9, 0x2

    const-wide/16 v10, -0x1

    aput-wide v10, v8, v9

    .line 151
    aget-object v8, v5, v2

    const/4 v9, 0x3

    const-wide/16 v10, -0x1

    aput-wide v10, v8, v9

    .line 152
    iget-object v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget-object v8, v8, Lio/realm/internal/async/ArgumentsHolder;->columnIndices:[J

    aput-object v8, v3, v2

    .line 153
    iget-object v8, v6, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget-object v8, v8, Lio/realm/internal/async/ArgumentsHolder;->sortOrders:[Lio/realm/Sort;

    invoke-static {v8}, Lio/realm/internal/TableQuery;->getNativeSortOrderValues([Lio/realm/Sort;)[Z

    move-result-object v8

    aput-object v8, v4, v2

    goto/16 :goto_1

    .line 160
    .end local v6    # "queryEntry":Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
    :cond_1
    new-instance v0, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;

    const/4 v7, 0x0

    invoke-direct {v0, v7}, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;-><init>(Lio/realm/internal/async/QueryUpdateTask$1;)V

    .line 162
    .local v0, "alignedParameters":Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;
    iput-object v1, v0, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->handoverQueries:[J

    .line 163
    iput-object v3, v0, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->multiSortColumnIndices:[[J

    .line 164
    iput-object v4, v0, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->multiSortOrder:[[Z

    .line 165
    iput-object v5, v0, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->queriesParameters:[[J

    .line 167
    return-object v0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private swapPointers(Lio/realm/internal/async/QueryUpdateTask$Result;[J)V
    .locals 8
    .param p1, "result"    # Lio/realm/internal/async/QueryUpdateTask$Result;
    .param p2, "handoverTableViewPointer"    # [J

    .prologue
    .line 171
    const/4 v0, 0x0

    .line 172
    .local v0, "i":I
    iget-object v3, p0, Lio/realm/internal/async/QueryUpdateTask;->realmResultsEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    .line 173
    .local v2, "queryEntry":Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
    iget-object v4, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    iget-object v5, v2, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->element:Ljava/lang/ref/WeakReference;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget-wide v6, p2, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 174
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 175
    .end local v2    # "queryEntry":Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;
    :cond_0
    return-void
.end method

.method private updateRealmObjectQuery(Lio/realm/internal/SharedGroup;Lio/realm/internal/async/QueryUpdateTask$Result;)Z
    .locals 8
    .param p1, "sharedGroup"    # Lio/realm/internal/SharedGroup;
    .param p2, "result"    # Lio/realm/internal/async/QueryUpdateTask$Result;

    .prologue
    .line 178
    invoke-direct {p0}, Lio/realm/internal/async/QueryUpdateTask;->isTaskCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lio/realm/internal/async/QueryUpdateTask;->realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    iget-object v0, v0, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget v0, v0, Lio/realm/internal/async/ArgumentsHolder;->type:I

    packed-switch v0, :pswitch_data_0

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Query mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/internal/async/QueryUpdateTask;->realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    iget-object v2, v2, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->queryArguments:Lio/realm/internal/async/ArgumentsHolder;

    iget v2, v2, Lio/realm/internal/async/ArgumentsHolder;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :pswitch_0
    invoke-virtual {p1}, Lio/realm/internal/SharedGroup;->getNativePointer()J

    move-result-wide v0

    iget-object v2, p0, Lio/realm/internal/async/QueryUpdateTask;->realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    iget-wide v2, v2, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    const-wide/16 v4, 0x0

    invoke-static/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeFindWithHandover(JJJ)J

    move-result-wide v6

    .line 184
    .local v6, "handoverRowPointer":J
    iget-object v0, p2, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedRow:Ljava/util/IdentityHashMap;

    iget-object v1, p0, Lio/realm/internal/async/QueryUpdateTask;->realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    iget-object v1, v1, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->element:Ljava/lang/ref/WeakReference;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    const/4 v0, 0x1

    .end local v6    # "handoverRowPointer":J
    :goto_0
    return v0

    .line 191
    :cond_0
    iget-object v0, p0, Lio/realm/internal/async/QueryUpdateTask;->realmObjectEntry:Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;

    iget-wide v0, v0, Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;->handoverQueryPointer:J

    invoke-static {v0, v1}, Lio/realm/internal/TableQuery;->nativeCloseQueryHandover(J)V

    .line 192
    const/4 v0, 0x0

    goto :goto_0

    .line 179
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 71
    const/4 v11, 0x0

    .line 73
    .local v11, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_0
    new-instance v12, Lio/realm/internal/SharedGroup;

    iget-object v0, p0, Lio/realm/internal/async/QueryUpdateTask;->realmConfiguration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lio/realm/internal/async/QueryUpdateTask;->realmConfiguration:Lio/realm/RealmConfiguration;

    .line 75
    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getDurability()Lio/realm/internal/SharedGroup$Durability;

    move-result-object v2

    iget-object v3, p0, Lio/realm/internal/async/QueryUpdateTask;->realmConfiguration:Lio/realm/RealmConfiguration;

    .line 76
    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v3

    invoke-direct {v12, v0, v1, v2, v3}, Lio/realm/internal/SharedGroup;-><init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .local v12, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_1
    iget v0, p0, Lio/realm/internal/async/QueryUpdateTask;->updateMode:I

    if-nez v0, :cond_2

    .line 81
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask$Result;->newRealmResultsResponse()Lio/realm/internal/async/QueryUpdateTask$Result;

    move-result-object v10

    .line 82
    .local v10, "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    invoke-direct {p0}, Lio/realm/internal/async/QueryUpdateTask;->prepareQueriesParameters()Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;

    move-result-object v6

    .line 83
    .local v6, "alignedParameters":Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->getNativePointer()J

    move-result-wide v0

    iget-object v2, v6, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->handoverQueries:[J

    iget-object v3, v6, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->queriesParameters:[[J

    iget-object v4, v6, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->multiSortColumnIndices:[[J

    iget-object v5, v6, Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;->multiSortOrder:[[Z

    invoke-static/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeBatchUpdateQueries(J[J[[J[[J[[Z)[J

    move-result-object v9

    .line 88
    .local v9, "handoverTableViewPointer":[J
    invoke-direct {p0, v10, v9}, Lio/realm/internal/async/QueryUpdateTask;->swapPointers(Lio/realm/internal/async/QueryUpdateTask$Result;[J)V

    .line 89
    const/4 v13, 0x1

    .line 90
    .local v13, "updateSuccessful":Z
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v0

    iput-object v0, v10, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    .line 98
    .end local v6    # "alignedParameters":Lio/realm/internal/async/QueryUpdateTask$AlignedQueriesParameters;
    .end local v9    # "handoverTableViewPointer":[J
    :goto_0
    iget-object v0, p0, Lio/realm/internal/async/QueryUpdateTask;->callerHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    .line 99
    .local v8, "handler":Landroid/os/Handler;
    if-eqz v13, :cond_0

    invoke-direct {p0}, Lio/realm/internal/async/QueryUpdateTask;->isTaskCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v8}, Lio/realm/internal/async/QueryUpdateTask;->isAliveHandler(Landroid/os/Handler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget v0, p0, Lio/realm/internal/async/QueryUpdateTask;->message:I

    invoke-virtual {v8, v0, v10}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 107
    :cond_0
    if-eqz v12, :cond_4

    .line 108
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->close()V

    move-object v11, v12

    .line 111
    .end local v8    # "handler":Landroid/os/Handler;
    .end local v10    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .end local v13    # "updateSuccessful":Z
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_1
    :goto_1
    return-void

    .line 93
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_2
    :try_start_2
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask$Result;->newRealmObjectResponse()Lio/realm/internal/async/QueryUpdateTask$Result;

    move-result-object v10

    .line 94
    .restart local v10    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    invoke-direct {p0, v12, v10}, Lio/realm/internal/async/QueryUpdateTask;->updateRealmObjectQuery(Lio/realm/internal/SharedGroup;Lio/realm/internal/async/QueryUpdateTask$Result;)Z

    move-result v13

    .line 95
    .restart local v13    # "updateSuccessful":Z
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v0

    iput-object v0, v10, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 103
    .end local v10    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    .end local v13    # "updateSuccessful":Z
    :catch_0
    move-exception v7

    move-object v11, v12

    .line 104
    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .local v7, "e":Ljava/lang/Exception;
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :goto_2
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Lio/realm/internal/log/RealmLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 107
    if-eqz v11, :cond_1

    .line 108
    invoke-virtual {v11}, Lio/realm/internal/SharedGroup;->close()V

    goto :goto_1

    .line 107
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v11, :cond_3

    .line 108
    invoke-virtual {v11}, Lio/realm/internal/SharedGroup;->close()V

    :cond_3
    throw v0

    .line 107
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catchall_1
    move-exception v0

    move-object v11, v12

    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_3

    .line 103
    :catch_1
    move-exception v7

    goto :goto_2

    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v8    # "handler":Landroid/os/Handler;
    .restart local v10    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    .restart local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v13    # "updateSuccessful":Z
    :cond_4
    move-object v11, v12

    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_1
.end method
