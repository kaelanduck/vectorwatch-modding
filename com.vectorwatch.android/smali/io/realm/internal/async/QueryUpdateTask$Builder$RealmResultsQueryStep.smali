.class public interface abstract Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;
.super Ljava/lang/Object;
.source "QueryUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/async/QueryUpdateTask$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RealmResultsQueryStep"
.end annotation


# virtual methods
.method public abstract add(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;J",
            "Lio/realm/internal/async/ArgumentsHolder;",
            ")",
            "Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;"
        }
    .end annotation
.end method

.method public abstract sendToHandler(Landroid/os/Handler;I)Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;
.end method
