.class public Lio/realm/internal/async/QueryUpdateTask$Builder;
.super Ljava/lang/Object;
.source "QueryUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/async/QueryUpdateTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/async/QueryUpdateTask$Builder$QueryEntry;,
        Lio/realm/internal/async/QueryUpdateTask$Builder$Steps;,
        Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;,
        Lio/realm/internal/async/QueryUpdateTask$Builder$HandlerStep;,
        Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;,
        Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;,
        Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    return-void
.end method
