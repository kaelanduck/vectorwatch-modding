.class public Lio/realm/internal/SubtableSchema;
.super Ljava/lang/Object;
.source "SubtableSchema.java"

# interfaces
.implements Lio/realm/internal/TableSchema;


# instance fields
.field private parentNativePtr:J

.field private path:[J


# direct methods
.method constructor <init>(J[J)V
    .locals 1
    .param p1, "parentNativePtr"    # J
    .param p3, "path"    # [J

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lio/realm/internal/SubtableSchema;->parentNativePtr:J

    .line 28
    iput-object p3, p0, Lio/realm/internal/SubtableSchema;->path:[J

    .line 29
    return-void
.end method

.method private verifyColumnName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3f

    if-le v0, v1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column names are currently limited to max 63 characters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    return-void
.end method


# virtual methods
.method public addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J
    .locals 7
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p2}, Lio/realm/internal/SubtableSchema;->verifyColumnName(Ljava/lang/String;)V

    .line 48
    iget-wide v2, p0, Lio/realm/internal/SubtableSchema;->parentNativePtr:J

    iget-object v4, p0, Lio/realm/internal/SubtableSchema;->path:[J

    invoke-virtual {p1}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v5

    move-object v1, p0

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/SubtableSchema;->nativeAddColumn(J[JILjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSubtableSchema(J)Lio/realm/internal/SubtableSchema;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    const/4 v3, 0x0

    .line 33
    iget-object v1, p0, Lio/realm/internal/SubtableSchema;->path:[J

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [J

    .line 34
    .local v0, "newPath":[J
    iget-object v1, p0, Lio/realm/internal/SubtableSchema;->path:[J

    iget-object v2, p0, Lio/realm/internal/SubtableSchema;->path:[J

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 35
    iget-object v1, p0, Lio/realm/internal/SubtableSchema;->path:[J

    array-length v1, v1

    aput-wide p1, v0, v1

    .line 36
    new-instance v1, Lio/realm/internal/SubtableSchema;

    iget-wide v2, p0, Lio/realm/internal/SubtableSchema;->parentNativePtr:J

    invoke-direct {v1, v2, v3, v0}, Lio/realm/internal/SubtableSchema;-><init>(J[J)V

    return-object v1
.end method

.method public bridge synthetic getSubtableSchema(J)Lio/realm/internal/TableSchema;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/SubtableSchema;->getSubtableSchema(J)Lio/realm/internal/SubtableSchema;

    move-result-object v0

    return-object v0
.end method

.method protected native nativeAddColumn(J[JILjava/lang/String;)J
.end method

.method protected native nativeRemoveColumn(J[JJ)V
.end method

.method protected native nativeRenameColumn(J[JJLjava/lang/String;)V
.end method

.method public removeColumn(J)V
    .locals 7
    .param p1, "columnIndex"    # J

    .prologue
    .line 58
    iget-wide v1, p0, Lio/realm/internal/SubtableSchema;->parentNativePtr:J

    iget-object v3, p0, Lio/realm/internal/SubtableSchema;->path:[J

    move-object v0, p0

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/SubtableSchema;->nativeRemoveColumn(J[JJ)V

    .line 59
    return-void
.end method

.method public renameColumn(JLjava/lang/String;)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "newName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0, p3}, Lio/realm/internal/SubtableSchema;->verifyColumnName(Ljava/lang/String;)V

    .line 69
    iget-wide v1, p0, Lio/realm/internal/SubtableSchema;->parentNativePtr:J

    iget-object v3, p0, Lio/realm/internal/SubtableSchema;->path:[J

    move-object v0, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/SubtableSchema;->nativeRenameColumn(J[JJLjava/lang/String;)V

    .line 70
    return-void
.end method
