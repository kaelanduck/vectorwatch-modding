.class public interface abstract Lio/realm/internal/TableOrView;
.super Ljava/lang/Object;
.source "TableOrView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/TableOrView$PivotType;
    }
.end annotation


# static fields
.field public static final NO_MATCH:I = -0x1


# virtual methods
.method public abstract averageDouble(J)D
.end method

.method public abstract averageFloat(J)D
.end method

.method public abstract averageLong(J)D
.end method

.method public abstract clear()V
.end method

.method public abstract clearSubtable(JJ)V
.end method

.method public abstract close()V
.end method

.method public abstract count(JLjava/lang/String;)J
.end method

.method public abstract findAllBoolean(JZ)Lio/realm/internal/TableView;
.end method

.method public abstract findAllDouble(JD)Lio/realm/internal/TableView;
.end method

.method public abstract findAllFloat(JF)Lio/realm/internal/TableView;
.end method

.method public abstract findAllLong(JJ)Lio/realm/internal/TableView;
.end method

.method public abstract findAllString(JLjava/lang/String;)Lio/realm/internal/TableView;
.end method

.method public abstract findFirstBoolean(JZ)J
.end method

.method public abstract findFirstDate(JLjava/util/Date;)J
.end method

.method public abstract findFirstDouble(JD)J
.end method

.method public abstract findFirstFloat(JF)J
.end method

.method public abstract findFirstLong(JJ)J
.end method

.method public abstract findFirstString(JLjava/lang/String;)J
.end method

.method public abstract getBinaryByteArray(JJ)[B
.end method

.method public abstract getBoolean(JJ)Z
.end method

.method public abstract getColumnCount()J
.end method

.method public abstract getColumnIndex(Ljava/lang/String;)J
.end method

.method public abstract getColumnName(J)Ljava/lang/String;
.end method

.method public abstract getColumnType(J)Lio/realm/RealmFieldType;
.end method

.method public abstract getDate(JJ)Ljava/util/Date;
.end method

.method public abstract getDouble(JJ)D
.end method

.method public abstract getFloat(JJ)F
.end method

.method public abstract getLink(JJ)J
.end method

.method public abstract getLong(JJ)J
.end method

.method public abstract getMixed(JJ)Lio/realm/internal/Mixed;
.end method

.method public abstract getMixedType(JJ)Lio/realm/RealmFieldType;
.end method

.method public abstract getString(JJ)Ljava/lang/String;
.end method

.method public abstract getSubtable(JJ)Lio/realm/internal/Table;
.end method

.method public abstract getSubtableSize(JJ)J
.end method

.method public abstract getTable()Lio/realm/internal/Table;
.end method

.method public abstract getVersion()J
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract isNullLink(JJ)Z
.end method

.method public abstract lowerBoundLong(JJ)J
.end method

.method public abstract maximumDate(J)Ljava/util/Date;
.end method

.method public abstract maximumDouble(J)Ljava/lang/Double;
.end method

.method public abstract maximumFloat(J)Ljava/lang/Float;
.end method

.method public abstract maximumLong(J)Ljava/lang/Long;
.end method

.method public abstract minimumDate(J)Ljava/util/Date;
.end method

.method public abstract minimumDouble(J)Ljava/lang/Double;
.end method

.method public abstract minimumFloat(J)Ljava/lang/Float;
.end method

.method public abstract minimumLong(J)Ljava/lang/Long;
.end method

.method public abstract nullifyLink(JJ)V
.end method

.method public abstract pivot(JJLio/realm/internal/TableOrView$PivotType;)Lio/realm/internal/Table;
.end method

.method public abstract remove(J)V
.end method

.method public abstract removeFirst()V
.end method

.method public abstract removeLast()V
.end method

.method public abstract rowToString(J)Ljava/lang/String;
.end method

.method public abstract setBinaryByteArray(JJ[B)V
.end method

.method public abstract setBoolean(JJZ)V
.end method

.method public abstract setDate(JJLjava/util/Date;)V
.end method

.method public abstract setDouble(JJD)V
.end method

.method public abstract setFloat(JJF)V
.end method

.method public abstract setLink(JJJ)V
.end method

.method public abstract setLong(JJJ)V
.end method

.method public abstract setMixed(JJLio/realm/internal/Mixed;)V
.end method

.method public abstract setString(JJLjava/lang/String;)V
.end method

.method public abstract size()J
.end method

.method public abstract sourceRowIndex(J)J
.end method

.method public abstract sumDouble(J)D
.end method

.method public abstract sumFloat(J)D
.end method

.method public abstract sumLong(J)J
.end method

.method public abstract syncIfNeeded()J
.end method

.method public abstract toJson()Ljava/lang/String;
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method public abstract toString(J)Ljava/lang/String;
.end method

.method public abstract upperBoundLong(JJ)J
.end method

.method public abstract where()Lio/realm/internal/TableQuery;
.end method
