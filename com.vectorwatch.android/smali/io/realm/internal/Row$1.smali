.class final Lio/realm/internal/Row$1;
.super Ljava/lang/Object;
.source "Row.java"

# interfaces
.implements Lio/realm/internal/Row;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Row;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# static fields
.field private static final UNLOADED_ROW_MESSAGE:Ljava/lang/String; = "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBinaryByteArray(J)[B
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 187
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBoolean(J)Z
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 162
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getColumnCount()J
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getColumnIndex(Ljava/lang/String;)J
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getColumnName(J)Ljava/lang/String;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 132
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getColumnType(J)Lio/realm/RealmFieldType;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDate(J)Ljava/util/Date;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDouble(J)D
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFloat(J)F
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 167
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getIndex()J
    .locals 2

    .prologue
    .line 152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLink(J)J
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 202
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLinkList(J)Lio/realm/internal/LinkView;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 222
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLong(J)J
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 157
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMixed(J)Lio/realm/internal/Mixed;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 192
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMixedType(J)Lio/realm/RealmFieldType;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 197
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getString(J)Ljava/lang/String;
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasColumn(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 282
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isAttached()Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method

.method public isNull(J)Z
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 212
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isNullLink(J)Z
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 207
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nullifyLink(J)V
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 272
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBinaryByteArray(J[B)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "data"    # [B

    .prologue
    .line 257
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBoolean(JZ)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Z

    .prologue
    .line 232
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDate(JLjava/util/Date;)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "date"    # Ljava/util/Date;

    .prologue
    .line 247
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDouble(JD)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 242
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setFloat(JF)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 237
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLink(JJ)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 267
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLong(JJ)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 227
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setMixed(JLio/realm/internal/Mixed;)V
    .locals 1
    .param p1, "columnIndex"    # J
    .param p3, "data"    # Lio/realm/internal/Mixed;

    .prologue
    .line 262
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public setNull(J)V
    .locals 2
    .param p1, "columnIndex"    # J

    .prologue
    .line 217
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setString(JLjava/lang/String;)V
    .locals 2
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 252
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded, make sure the instance is loaded by calling RealmObject.isLoaded()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
