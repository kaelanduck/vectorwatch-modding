.class public Lio/realm/internal/ImplicitTransaction;
.super Lio/realm/internal/Group;
.source "ImplicitTransaction.java"


# instance fields
.field private final parent:Lio/realm/internal/SharedGroup;


# direct methods
.method public constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/SharedGroup;J)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "sharedGroup"    # Lio/realm/internal/SharedGroup;
    .param p3, "nativePtr"    # J

    .prologue
    .line 26
    const/4 v0, 0x1

    invoke-direct {p0, p1, p3, p4, v0}, Lio/realm/internal/Group;-><init>(Lio/realm/internal/Context;JZ)V

    .line 27
    iput-object p2, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    .line 28
    return-void
.end method

.method private assertNotClosed()V
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot use ImplicitTransaction after it or its parent has been closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_1
    return-void
.end method


# virtual methods
.method public advanceRead()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lio/realm/internal/ImplicitTransaction;->assertNotClosed()V

    .line 35
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->advanceRead()V

    .line 36
    return-void
.end method

.method public advanceRead(Lio/realm/internal/SharedGroup$VersionID;)V
    .locals 1
    .param p1, "versionID"    # Lio/realm/internal/SharedGroup$VersionID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Lio/realm/internal/ImplicitTransaction;->assertNotClosed()V

    .line 45
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0, p1}, Lio/realm/internal/SharedGroup;->advanceRead(Lio/realm/internal/SharedGroup$VersionID;)V

    .line 46
    return-void
.end method

.method public commitAndContinueAsRead()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Lio/realm/internal/ImplicitTransaction;->assertNotClosed()V

    .line 59
    iget-boolean v0, p0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not inside a transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->commitAndContinueAsRead()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    .line 64
    return-void
.end method

.method public endRead()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lio/realm/internal/ImplicitTransaction;->assertNotClosed()V

    .line 68
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->endRead()V

    .line 69
    return-void
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public promoteToWrite()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Lio/realm/internal/ImplicitTransaction;->assertNotClosed()V

    .line 50
    iget-boolean v0, p0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nested transactions are not allowed. Use commitTransaction() after each beginTransaction()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    .line 54
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->promoteToWrite()V

    .line 55
    return-void
.end method

.method public rollbackAndContinueAsRead()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lio/realm/internal/ImplicitTransaction;->assertNotClosed()V

    .line 73
    iget-boolean v0, p0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not inside a transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    iget-object v0, p0, Lio/realm/internal/ImplicitTransaction;->parent:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->rollbackAndContinueAsRead()V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    .line 78
    return-void
.end method
