.class public Lio/realm/internal/Version;
.super Ljava/lang/Object;
.source "Version.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/Version$Feature;
    }
.end annotation


# static fields
.field static final CORE_MIN_MAJOR:I = 0x0

.field static final CORE_MIN_MINOR:I = 0x1

.field static final CORE_MIN_PATCH:I = 0x6

.field static final REQUIRED_JNI_VERSION:I = 0x17


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static coreLibVersionCompatible(Z)Z
    .locals 6
    .param p0, "throwIfNot"    # Z

    .prologue
    const/16 v5, 0x17

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lio/realm/internal/Version;->nativeIsAtLeast(III)Z

    move-result v0

    .line 54
    .local v0, "compatible":Z
    if-nez v0, :cond_1

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Version mismatch between realm.jar (0.1.6) and native core library ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 57
    invoke-static {}, Lio/realm/internal/Version;->getCoreVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "errTxt":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 59
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 60
    :cond_0
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 72
    .end local v1    # "errTxt":Ljava/lang/String;
    :goto_0
    return v2

    .line 64
    :cond_1
    invoke-static {}, Lio/realm/internal/Version;->nativeGetAPIVersion()I

    move-result v4

    if-ne v4, v5, :cond_2

    move v0, v3

    .line 65
    :goto_1
    if-nez v0, :cond_4

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Native lib API is version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lio/realm/internal/Version;->nativeGetAPIVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " which is expected by the jar."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    .restart local v1    # "errTxt":Ljava/lang/String;
    if-eqz p0, :cond_3

    .line 69
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .end local v1    # "errTxt":Ljava/lang/String;
    :cond_2
    move v0, v2

    .line 64
    goto :goto_1

    .line 70
    .restart local v1    # "errTxt":Ljava/lang/String;
    :cond_3
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .end local v1    # "errTxt":Ljava/lang/String;
    :cond_4
    move v2, v0

    .line 72
    goto :goto_0
.end method

.method public static getCoreVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lio/realm/internal/Version;->nativeGetVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lio/realm/internal/Version;->getCoreVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasFeature(Lio/realm/internal/Version$Feature;)Z
    .locals 1
    .param p0, "feature"    # Lio/realm/internal/Version$Feature;

    .prologue
    .line 48
    invoke-virtual {p0}, Lio/realm/internal/Version$Feature;->ordinal()I

    move-result v0

    invoke-static {v0}, Lio/realm/internal/Version;->nativeHasFeature(I)Z

    move-result v0

    return v0
.end method

.method static native nativeGetAPIVersion()I
.end method

.method static native nativeGetVersion()Ljava/lang/String;
.end method

.method static native nativeHasFeature(I)Z
.end method

.method static native nativeIsAtLeast(III)Z
.end method
