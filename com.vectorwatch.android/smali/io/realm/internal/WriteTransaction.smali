.class public Lio/realm/internal/WriteTransaction;
.super Lio/realm/internal/Group;
.source "WriteTransaction.java"


# instance fields
.field private committed:Z

.field private final db:Lio/realm/internal/SharedGroup;


# direct methods
.method constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/SharedGroup;J)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "db"    # Lio/realm/internal/SharedGroup;
    .param p3, "nativePtr"    # J

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p3, p4, v0}, Lio/realm/internal/Group;-><init>(Lio/realm/internal/Context;JZ)V

    .line 47
    iput-object p2, p0, Lio/realm/internal/WriteTransaction;->db:Lio/realm/internal/SharedGroup;

    .line 48
    iput-boolean v0, p0, Lio/realm/internal/WriteTransaction;->committed:Z

    .line 49
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lio/realm/internal/WriteTransaction;->committed:Z

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lio/realm/internal/WriteTransaction;->rollback()V

    .line 43
    :cond_0
    return-void
.end method

.method public commit()V
    .locals 2

    .prologue
    .line 25
    iget-boolean v0, p0, Lio/realm/internal/WriteTransaction;->committed:Z

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lio/realm/internal/WriteTransaction;->db:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->commit()V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/WriteTransaction;->committed:Z

    .line 32
    return-void

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can only commit once after a WriteTransaction has been made."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public rollback()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lio/realm/internal/WriteTransaction;->db:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->rollback()V

    .line 36
    return-void
.end method
