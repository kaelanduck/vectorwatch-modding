.class public final enum Lio/realm/internal/Version$Feature;
.super Ljava/lang/Enum;
.source "Version.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Version;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/Version$Feature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/realm/internal/Version$Feature;

.field public static final enum Feature_Debug:Lio/realm/internal/Version$Feature;

.field public static final enum Feature_Replication:Lio/realm/internal/Version$Feature;


# instance fields
.field private final nativeFeature:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lio/realm/internal/Version$Feature;

    const-string v1, "Feature_Debug"

    invoke-direct {v0, v1, v2, v2}, Lio/realm/internal/Version$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Version$Feature;->Feature_Debug:Lio/realm/internal/Version$Feature;

    .line 28
    new-instance v0, Lio/realm/internal/Version$Feature;

    const-string v1, "Feature_Replication"

    invoke-direct {v0, v1, v3, v3}, Lio/realm/internal/Version$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/Version$Feature;->Feature_Replication:Lio/realm/internal/Version$Feature;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lio/realm/internal/Version$Feature;

    sget-object v1, Lio/realm/internal/Version$Feature;->Feature_Debug:Lio/realm/internal/Version$Feature;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/internal/Version$Feature;->Feature_Replication:Lio/realm/internal/Version$Feature;

    aput-object v1, v0, v3

    sput-object v0, Lio/realm/internal/Version$Feature;->$VALUES:[Lio/realm/internal/Version$Feature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nativeValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lio/realm/internal/Version$Feature;->nativeFeature:I

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/Version$Feature;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lio/realm/internal/Version$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Version$Feature;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/Version$Feature;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lio/realm/internal/Version$Feature;->$VALUES:[Lio/realm/internal/Version$Feature;

    invoke-virtual {v0}, [Lio/realm/internal/Version$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/Version$Feature;

    return-object v0
.end method
