.class public Lio/realm/internal/LinkView;
.super Lio/realm/internal/NativeObject;
.source "LinkView.java"


# instance fields
.field final columnIndexInParent:J

.field private final context:Lio/realm/internal/Context;

.field final parent:Lio/realm/internal/Table;


# direct methods
.method public constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;JJ)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "parent"    # Lio/realm/internal/Table;
    .param p3, "columnIndexInParent"    # J
    .param p5, "nativeLinkViewPtr"    # J

    .prologue
    .line 30
    invoke-direct {p0}, Lio/realm/internal/NativeObject;-><init>()V

    .line 31
    iput-object p1, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    .line 32
    iput-object p2, p0, Lio/realm/internal/LinkView;->parent:Lio/realm/internal/Table;

    .line 33
    iput-wide p3, p0, Lio/realm/internal/LinkView;->columnIndexInParent:J

    .line 34
    iput-wide p5, p0, Lio/realm/internal/LinkView;->nativePointer:J

    .line 36
    invoke-virtual {p1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p1, v0, p0}, Lio/realm/internal/Context;->addReference(ILio/realm/internal/NativeObject;)V

    .line 38
    return-void
.end method

.method private checkImmutable()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lio/realm/internal/LinkView;->parent:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Changing Realm data can only be done from inside a write transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    return-void
.end method

.method private native nativeAdd(JJ)V
.end method

.method private native nativeClear(J)V
.end method

.method static native nativeClose(J)V
.end method

.method private native nativeFind(JJ)J
.end method

.method private native nativeGetTargetRowIndex(JJ)J
.end method

.method private native nativeGetTargetTable(J)J
.end method

.method private native nativeInsert(JJJ)V
.end method

.method private native nativeIsAttached(J)Z
.end method

.method private native nativeIsEmpty(J)Z
.end method

.method private native nativeMove(JJJ)V
.end method

.method private native nativeRemove(JJ)V
.end method

.method private native nativeRemoveAllTargetRows(J)V
.end method

.method private native nativeRemoveTargetRow(JJ)V
.end method

.method private native nativeSet(JJJ)V
.end method

.method private native nativeSize(J)J
.end method


# virtual methods
.method public add(J)V
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 70
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 71
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    .line 72
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 96
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeClear(J)V

    .line 97
    return-void
.end method

.method public contains(J)Z
    .locals 5
    .param p1, "tableRowIndex"    # J

    .prologue
    .line 100
    iget-wide v2, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/LinkView;->nativeFind(JJ)J

    move-result-wide v0

    .line 101
    .local v0, "index":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getCheckedRow(J)Lio/realm/internal/CheckedRow;
    .locals 1
    .param p1, "index"    # J

    .prologue
    .line 62
    iget-object v0, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/CheckedRow;->get(Lio/realm/internal/Context;Lio/realm/internal/LinkView;J)Lio/realm/internal/CheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lio/realm/internal/LinkView;->parent:Lio/realm/internal/Table;

    return-object v0
.end method

.method public getTargetRowIndex(J)J
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 66
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/LinkView;->nativeGetTargetRowIndex(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTargetTable()Lio/realm/internal/Table;
    .locals 6

    .prologue
    .line 153
    iget-object v1, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 154
    iget-wide v4, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v4, v5}, Lio/realm/internal/LinkView;->nativeGetTargetTable(J)J

    move-result-wide v2

    .line 157
    .local v2, "nativeTablePointer":J
    :try_start_0
    new-instance v1, Lio/realm/internal/Table;

    iget-object v4, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    iget-object v5, p0, Lio/realm/internal/LinkView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v4, v5, v2, v3}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 160
    throw v0
.end method

.method public getUncheckedRow(J)Lio/realm/internal/UncheckedRow;
    .locals 1
    .param p1, "index"    # J

    .prologue
    .line 49
    iget-object v0, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->getByRowIndex(Lio/realm/internal/Context;Lio/realm/internal/LinkView;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public insert(JJ)V
    .locals 9
    .param p1, "pos"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 75
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 76
    iget-wide v2, p0, Lio/realm/internal/LinkView;->nativePointer:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/LinkView;->nativeInsert(JJJ)V

    .line 77
    return-void
.end method

.method public isAttached()Z
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeIsAttached(J)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 109
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeIsEmpty(J)Z

    move-result v0

    return v0
.end method

.method public move(JJ)V
    .locals 9
    .param p1, "oldPos"    # J
    .param p3, "newPos"    # J

    .prologue
    .line 85
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 86
    iget-wide v2, p0, Lio/realm/internal/LinkView;->nativePointer:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/LinkView;->nativeMove(JJJ)V

    .line 87
    return-void
.end method

.method native nativeGetRow(JJ)J
.end method

.method protected native nativeWhere(J)J
.end method

.method public remove(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 90
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 91
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/LinkView;->nativeRemove(JJ)V

    .line 92
    return-void
.end method

.method public removeAllTargetRows()V
    .locals 2

    .prologue
    .line 139
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 140
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeRemoveAllTargetRows(J)V

    .line 141
    return-void
.end method

.method public removeTargetRow(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 147
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 148
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    int-to-long v2, p1

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/LinkView;->nativeRemoveTargetRow(JJ)V

    .line 149
    return-void
.end method

.method public set(JJ)V
    .locals 9
    .param p1, "pos"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 80
    invoke-direct {p0}, Lio/realm/internal/LinkView;->checkImmutable()V

    .line 81
    iget-wide v2, p0, Lio/realm/internal/LinkView;->nativePointer:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/LinkView;->nativeSet(JJJ)V

    .line 82
    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public where()Lio/realm/internal/TableQuery;
    .locals 6

    .prologue
    .line 114
    iget-object v1, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 115
    iget-wide v4, p0, Lio/realm/internal/LinkView;->nativePointer:J

    invoke-virtual {p0, v4, v5}, Lio/realm/internal/LinkView;->nativeWhere(J)J

    move-result-wide v2

    .line 117
    .local v2, "nativeQueryPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableQuery;

    iget-object v4, p0, Lio/realm/internal/LinkView;->context:Lio/realm/internal/Context;

    iget-object v5, p0, Lio/realm/internal/LinkView;->parent:Lio/realm/internal/Table;

    invoke-direct {v1, v4, v5, v2, v3}, Lio/realm/internal/TableQuery;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/TableQuery;->nativeClose(J)V

    .line 120
    throw v0
.end method
