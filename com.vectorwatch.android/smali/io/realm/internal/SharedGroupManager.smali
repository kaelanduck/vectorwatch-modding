.class public Lio/realm/internal/SharedGroupManager;
.super Ljava/lang/Object;
.source "SharedGroupManager.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private sharedGroup:Lio/realm/internal/SharedGroup;

.field private transaction:Lio/realm/internal/ImplicitTransaction;


# direct methods
.method public constructor <init>(Lio/realm/RealmConfiguration;)V
    .locals 5
    .param p1, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lio/realm/internal/SharedGroup;

    .line 46
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 48
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getDurability()Lio/realm/internal/SharedGroup$Durability;

    move-result-object v3

    .line 49
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lio/realm/internal/SharedGroup;-><init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V

    iput-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    .line 50
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->beginImplicitTransaction()Lio/realm/internal/ImplicitTransaction;

    move-result-object v0

    iput-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    .line 51
    return-void
.end method

.method public static compact(Lio/realm/RealmConfiguration;)Z
    .locals 8
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 170
    const/4 v2, 0x0

    .line 171
    .local v2, "sharedGroup":Lio/realm/internal/SharedGroup;
    const/4 v1, 0x0

    .line 173
    .local v1, "result":Z
    :try_start_0
    new-instance v3, Lio/realm/internal/SharedGroup;

    .line 174
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    sget-object v6, Lio/realm/internal/SharedGroup$Durability;->FULL:Lio/realm/internal/SharedGroup$Durability;

    .line 177
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lio/realm/internal/SharedGroup;-><init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    .end local v2    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .local v3, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_1
    invoke-virtual {v3}, Lio/realm/internal/SharedGroup;->compact()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    .line 183
    if-eqz v3, :cond_0

    .line 184
    invoke-virtual {v3}, Lio/realm/internal/SharedGroup;->close()V

    :cond_0
    move-object v2, v3

    .end local v3    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v2    # "sharedGroup":Lio/realm/internal/SharedGroup;
    move v4, v1

    .line 187
    :cond_1
    :goto_0
    return v4

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lio/realm/internal/log/RealmLog;->i(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    const/4 v4, 0x0

    .line 183
    if-eqz v2, :cond_1

    .line 184
    invoke-virtual {v2}, Lio/realm/internal/SharedGroup;->close()V

    goto :goto_0

    .line 183
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v2, :cond_2

    .line 184
    invoke-virtual {v2}, Lio/realm/internal/SharedGroup;->close()V

    :cond_2
    throw v4

    .line 183
    .end local v2    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v3    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v2    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_2

    .line 179
    .end local v2    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v3    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v2    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_1
.end method


# virtual methods
.method public advanceRead()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0}, Lio/realm/internal/ImplicitTransaction;->advanceRead()V

    .line 77
    return-void
.end method

.method public advanceRead(Lio/realm/internal/SharedGroup$VersionID;)V
    .locals 1
    .param p1, "version"    # Lio/realm/internal/SharedGroup$VersionID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/realm/internal/async/BadVersionException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0, p1}, Lio/realm/internal/ImplicitTransaction;->advanceRead(Lio/realm/internal/SharedGroup$VersionID;)V

    .line 84
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->close()V

    .line 59
    iput-object v1, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    .line 60
    iput-object v1, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    .line 61
    return-void
.end method

.method public commitAndContinueAsRead()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0}, Lio/realm/internal/ImplicitTransaction;->commitAndContinueAsRead()V

    .line 119
    return-void
.end method

.method public copyToFile(Ljava/io/File;[B)V
    .locals 1
    .param p1, "destination"    # Ljava/io/File;
    .param p2, "key"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0, p1, p2}, Lio/realm/internal/ImplicitTransaction;->writeToFile(Ljava/io/File;[B)V

    .line 142
    return-void
.end method

.method public getNativePointer()J
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->getNativePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSharedGroup()Lio/realm/internal/SharedGroup;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    return-object v0
.end method

.method public getTable(Ljava/lang/String;)Lio/realm/internal/Table;
    .locals 1
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 90
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0, p1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    return-object v0
.end method

.method public getTransaction()Lio/realm/internal/ImplicitTransaction;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    return-object v0
.end method

.method public getVersion()Lio/realm/internal/SharedGroup$VersionID;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v0

    return-object v0
.end method

.method public hasChanged()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->hasChanged()Z

    move-result v0

    return v0
.end method

.method public hasTable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 134
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0, p1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isImmutable()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    iget-boolean v0, v0, Lio/realm/internal/ImplicitTransaction;->immutable:Z

    return v0
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->sharedGroup:Lio/realm/internal/SharedGroup;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public promoteToWrite()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0}, Lio/realm/internal/ImplicitTransaction;->promoteToWrite()V

    .line 112
    return-void
.end method

.method public rollbackAndContinueAsRead()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lio/realm/internal/SharedGroupManager;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v0}, Lio/realm/internal/ImplicitTransaction;->rollbackAndContinueAsRead()V

    .line 126
    return-void
.end method
