.class public Lio/realm/internal/Table;
.super Ljava/lang/Object;
.source "Table.java"

# interfaces
.implements Lio/realm/internal/TableOrView;
.implements Lio/realm/internal/TableSchema;
.implements Ljava/io/Closeable;


# static fields
.field private static final DEBUG:Z = false

.field public static final INFINITE:J = -0x1L

.field public static final INTEGER_DEFAULT_VALUE:J = 0x0L

.field public static final METADATA_TABLE_NAME:Ljava/lang/String; = "metadata"

.field public static final NOT_NULLABLE:Z = false

.field private static final NO_PRIMARY_KEY:J = -0x2L

.field public static final NULLABLE:Z = true

.field private static final PRIMARY_KEY_CLASS_COLUMN_INDEX:J = 0x0L

.field private static final PRIMARY_KEY_CLASS_COLUMN_NAME:Ljava/lang/String; = "pk_table"

.field private static final PRIMARY_KEY_FIELD_COLUMN_INDEX:J = 0x1L

.field private static final PRIMARY_KEY_FIELD_COLUMN_NAME:Ljava/lang/String; = "pk_property"

.field private static final PRIMARY_KEY_TABLE_NAME:Ljava/lang/String; = "pk"

.field public static final STRING_DEFAULT_VALUE:Ljava/lang/String; = ""

.field public static final TABLE_MAX_LENGTH:I = 0x38

.field public static final TABLE_PREFIX:Ljava/lang/String;

.field static tableCount:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private cachedPrimaryKeyColumnIndex:J

.field private final context:Lio/realm/internal/Context;

.field protected nativePtr:J

.field protected final parent:Ljava/lang/Object;

.field protected tableNo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Lio/realm/internal/Util;->getTablePrefix()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lio/realm/internal/Table;->tableCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 64
    invoke-static {}, Lio/realm/internal/RealmCore;->loadLibrary()V

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    .line 73
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    .line 77
    invoke-virtual {p0}, Lio/realm/internal/Table;->createNative()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    .line 78
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Out of native memory."

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    return-void
.end method

.method constructor <init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    .locals 3
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "parent"    # Ljava/lang/Object;
    .param p3, "nativePointer"    # J

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 88
    iput-object p1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    .line 89
    iput-object p2, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    .line 90
    iput-wide p3, p0, Lio/realm/internal/Table;->nativePtr:J

    .line 95
    return-void
.end method

.method private checkHasPrimaryKey()V
    .locals 3

    .prologue
    .line 1079
    invoke-virtual {p0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1080
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no primary key defined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1082
    :cond_0
    return-void
.end method

.method private getPrimaryKeyTable()Lio/realm/internal/Table;
    .locals 6

    .prologue
    .line 1012
    invoke-virtual {p0}, Lio/realm/internal/Table;->getTableGroup()Lio/realm/internal/Group;

    move-result-object v0

    .line 1013
    .local v0, "group":Lio/realm/internal/Group;
    if-nez v0, :cond_0

    .line 1014
    const/4 v1, 0x0

    .line 1025
    :goto_0
    return-object v1

    .line 1017
    :cond_0
    const-string v2, "pk"

    invoke-virtual {v0, v2}, Lio/realm/internal/Group;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 1018
    .local v1, "pkTable":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1019
    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v3, "pk_table"

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    .line 1020
    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v3, "pk_property"

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    goto :goto_0

    .line 1022
    :cond_1
    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->migratePrimaryKeyTableIfNeeded(Lio/realm/internal/Group;Lio/realm/internal/Table;)V

    goto :goto_0
.end method

.method private getSubtableDuringInsert(JJ)Lio/realm/internal/Table;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 806
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 807
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetSubtableDuringInsert(JJJ)J

    move-result-wide v8

    .line 809
    .local v8, "nativeSubtablePtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/Table;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 811
    :catch_0
    move-exception v0

    .line 812
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 813
    throw v0
.end method

.method private insertSubTable(JJLjava/lang/Object;)V
    .locals 5
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Ljava/lang/Object;

    .prologue
    .line 969
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 970
    if-eqz p5, :cond_0

    .line 972
    invoke-direct {p0, p1, p2, p3, p4}, Lio/realm/internal/Table;->getSubtableDuringInsert(JJ)Lio/realm/internal/Table;

    move-result-object v3

    .local v3, "subtable":Lio/realm/internal/Table;
    move-object v4, p5

    .line 973
    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    array-length v2, v4

    .line 974
    .local v2, "rows":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    move-object v4, p5

    .line 975
    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    aget-object v1, v4, v0

    .line 976
    .local v1, "rowArr":Ljava/lang/Object;
    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "rowArr":Ljava/lang/Object;
    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v3, v1}, Lio/realm/internal/Table;->add([Ljava/lang/Object;)J

    .line 974
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 979
    .end local v0    # "i":I
    .end local v2    # "rows":I
    .end local v3    # "subtable":Lio/realm/internal/Table;
    :cond_0
    return-void
.end method

.method public static isMetaTable(Ljava/lang/String;)Z
    .locals 1
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 1422
    const-string v0, "metadata"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pk"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPrimaryKeyColumn(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 555
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private migratePrimaryKeyTableIfNeeded(Lio/realm/internal/Group;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "group"    # Lio/realm/internal/Group;
    .param p2, "pkTable"    # Lio/realm/internal/Table;

    .prologue
    .line 1038
    iget-wide v0, p1, Lio/realm/internal/Group;->nativePtr:J

    iget-wide v2, p2, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/Table;->nativeMigratePrimaryKeyTableIfNeeded(JJ)V

    .line 1039
    return-void
.end method

.method private native nativeAddColumn(JILjava/lang/String;Z)J
.end method

.method private native nativeAddColumnLink(JILjava/lang/String;J)J
.end method

.method private native nativeAddEmptyRow(JJ)J
.end method

.method private native nativeAddSearchIndex(JJ)V
.end method

.method private native nativeAverageDouble(JJ)D
.end method

.method private native nativeAverageFloat(JJ)D
.end method

.method private native nativeAverageInt(JJ)D
.end method

.method private native nativeClear(J)V
.end method

.method private native nativeClearSubtable(JJJ)V
.end method

.method static native nativeClose(J)V
.end method

.method private native nativeConvertColumnToNotNullable(JJ)V
.end method

.method private native nativeConvertColumnToNullable(JJ)V
.end method

.method private native nativeCountDouble(JJD)J
.end method

.method private native nativeCountFloat(JJF)J
.end method

.method private native nativeCountLong(JJJ)J
.end method

.method private native nativeCountString(JJLjava/lang/String;)J
.end method

.method private native nativeFindAllBool(JJZ)J
.end method

.method private native nativeFindAllDouble(JJD)J
.end method

.method private native nativeFindAllFloat(JJF)J
.end method

.method private native nativeFindAllInt(JJJ)J
.end method

.method private native nativeFindAllString(JJLjava/lang/String;)J
.end method

.method private native nativeFindAllTimestamp(JJJ)J
.end method

.method private native nativeFindFirstBool(JJZ)J
.end method

.method private native nativeFindFirstDouble(JJD)J
.end method

.method private native nativeFindFirstFloat(JJF)J
.end method

.method private native nativeFindFirstInt(JJJ)J
.end method

.method private native nativeFindFirstNull(JJ)J
.end method

.method private native nativeFindFirstString(JJLjava/lang/String;)J
.end method

.method private native nativeFindFirstTimestamp(JJJ)J
.end method

.method private native nativeGetBoolean(JJJ)Z
.end method

.method private native nativeGetByteArray(JJJ)[B
.end method

.method private native nativeGetColumnCount(J)J
.end method

.method private native nativeGetColumnIndex(JLjava/lang/String;)J
.end method

.method private native nativeGetColumnName(JJ)Ljava/lang/String;
.end method

.method private native nativeGetColumnType(JJ)I
.end method

.method private native nativeGetDistinctView(JJ)J
.end method

.method private native nativeGetDouble(JJJ)D
.end method

.method private native nativeGetFloat(JJJ)F
.end method

.method private native nativeGetLink(JJJ)J
.end method

.method private native nativeGetLinkTarget(JJ)J
.end method

.method private native nativeGetLong(JJJ)J
.end method

.method private native nativeGetMixed(JJJ)Lio/realm/internal/Mixed;
.end method

.method private native nativeGetMixedType(JJJ)I
.end method

.method private native nativeGetName(J)Ljava/lang/String;
.end method

.method private native nativeGetSortedView(JJZ)J
.end method

.method private native nativeGetSortedViewMulti(J[J[Z)J
.end method

.method private native nativeGetString(JJJ)Ljava/lang/String;
.end method

.method private native nativeGetSubtable(JJJ)J
.end method

.method private native nativeGetSubtableDuringInsert(JJJ)J
.end method

.method private native nativeGetSubtableSize(JJJ)J
.end method

.method private native nativeGetTableSpec(J)Lio/realm/internal/TableSpec;
.end method

.method private native nativeGetTimestamp(JJJ)J
.end method

.method private native nativeHasSameSchema(JJ)Z
.end method

.method private native nativeHasSearchIndex(JJ)Z
.end method

.method private native nativeIsColumnNullable(JJ)Z
.end method

.method private native nativeIsNullLink(JJJ)Z
.end method

.method private native nativeIsRootTable(J)Z
.end method

.method private native nativeIsValid(J)Z
.end method

.method private native nativeLowerBoundInt(JJJ)J
.end method

.method private native nativeMaximumDouble(JJ)D
.end method

.method private native nativeMaximumFloat(JJ)F
.end method

.method private native nativeMaximumInt(JJ)J
.end method

.method private native nativeMaximumTimestamp(JJ)J
.end method

.method private native nativeMigratePrimaryKeyTableIfNeeded(JJ)V
.end method

.method private native nativeMinimumDouble(JJ)D
.end method

.method private native nativeMinimumFloat(JJ)F
.end method

.method private native nativeMinimumInt(JJ)J
.end method

.method private native nativeMinimumTimestamp(JJ)J
.end method

.method private native nativeMoveLastOver(JJ)V
.end method

.method private native nativeNullifyLink(JJJ)V
.end method

.method private native nativeOptimize(J)V
.end method

.method private native nativePivot(JJJIJ)V
.end method

.method private native nativeRemove(JJ)V
.end method

.method private native nativeRemoveColumn(JJ)V
.end method

.method private native nativeRemoveLast(J)V
.end method

.method private native nativeRemoveSearchIndex(JJ)V
.end method

.method private native nativeRenameColumn(JJLjava/lang/String;)V
.end method

.method private native nativeRowToString(JJ)Ljava/lang/String;
.end method

.method private native nativeSetBoolean(JJJZ)V
.end method

.method private native nativeSetByteArray(JJJ[B)V
.end method

.method private native nativeSetDouble(JJJD)V
.end method

.method private native nativeSetFloat(JJJF)V
.end method

.method private native nativeSetLink(JJJJ)V
.end method

.method private native nativeSetLong(JJJJ)V
.end method

.method private native nativeSetMixed(JJJLio/realm/internal/Mixed;)V
.end method

.method private native nativeSetNull(JJJ)V
.end method

.method private native nativeSetPrimaryKey(JJLjava/lang/String;)J
.end method

.method private native nativeSetString(JJJLjava/lang/String;)V
.end method

.method private native nativeSetTimestamp(JJJJ)V
.end method

.method private native nativeSize(J)J
.end method

.method private native nativeSumDouble(JJ)D
.end method

.method private native nativeSumFloat(JJ)D
.end method

.method private native nativeSumInt(JJ)J
.end method

.method private native nativeToJson(J)Ljava/lang/String;
.end method

.method private native nativeToString(JJ)Ljava/lang/String;
.end method

.method private native nativeUpdateFromSpec(JLio/realm/internal/TableSpec;)V
.end method

.method private native nativeUpperBoundInt(JJJ)J
.end method

.method private native nativeVersion(J)J
.end method

.method private native nativeWhere(J)J
.end method

.method public static tableNameToClassName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 1437
    sget-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1440
    .end local p0    # "tableName":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "tableName":Ljava/lang/String;
    :cond_0
    sget-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 684
    new-instance v0, Lio/realm/exceptions/RealmPrimaryKeyConstraintException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value already exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmPrimaryKeyConstraintException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private throwImmutable()V
    .locals 2

    .prologue
    .line 1402
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Changing Realm data can only be done from inside a transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private verifyColumnName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3f

    if-le v0, v1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column names are currently limited to max 63 characters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs add([Ljava/lang/Object;)J
    .locals 30
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 465
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->addEmptyRow()J

    move-result-wide v8

    .line 467
    .local v8, "rowIndex":J
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 470
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v28, v0

    .line 471
    .local v28, "columns":I
    move-object/from16 v0, p1

    array-length v3, v0

    move/from16 v0, v28

    if-eq v0, v3, :cond_0

    .line 472
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The number of value parameters ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    array-length v5, v0

    .line 473
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") does not match the number of columns in the table ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 475
    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 477
    :cond_0
    move/from16 v0, v28

    new-array v12, v0, [Lio/realm/RealmFieldType;

    .line 478
    .local v12, "colTypes":[Lio/realm/RealmFieldType;
    const/4 v6, 0x0

    .local v6, "columnIndex":I
    :goto_0
    move/from16 v0, v28

    if-ge v6, v0, :cond_3

    .line 479
    aget-object v24, p1, v6

    .line 480
    .local v24, "value":Ljava/lang/Object;
    int-to-long v4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v2

    .line 481
    .local v2, "colType":Lio/realm/RealmFieldType;
    aput-object v2, v12, v6

    .line 482
    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Lio/realm/RealmFieldType;->isValid(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 485
    if-nez v24, :cond_1

    .line 486
    const-string v29, "null"

    .line 491
    .local v29, "providedType":Ljava/lang/String;
    :goto_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid argument no "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v6, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Expected a value compatible with column type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", but got "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 488
    .end local v29    # "providedType":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v29

    .restart local v29    # "providedType":Ljava/lang/String;
    goto :goto_1

    .line 478
    .end local v29    # "providedType":Ljava/lang/String;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 497
    .end local v2    # "colType":Lio/realm/RealmFieldType;
    .end local v24    # "value":Ljava/lang/Object;
    :cond_3
    const-wide/16 v6, 0x0

    .local v6, "columnIndex":J
    :goto_2
    move/from16 v0, v28

    int-to-long v4, v0

    cmp-long v3, v6, v4

    if-gez v3, :cond_9

    .line 498
    long-to-int v3, v6

    aget-object v24, p1, v3

    .line 499
    .restart local v24    # "value":Ljava/lang/Object;
    sget-object v3, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    long-to-int v4, v6

    aget-object v4, v12, v4

    invoke-virtual {v4}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 548
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected columnType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    long-to-int v5, v6

    aget-object v5, v12, v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 501
    :pswitch_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    check-cast v24, Ljava/lang/Boolean;

    .end local v24    # "value":Ljava/lang/Object;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZ)V

    .line 497
    :goto_3
    const-wide/16 v4, 0x1

    add-long/2addr v6, v4

    goto :goto_2

    .line 504
    .restart local v24    # "value":Ljava/lang/Object;
    :pswitch_1
    if-nez v24, :cond_4

    .line 505
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v8, v9}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 506
    move-object/from16 v0, p0

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lio/realm/internal/Table;->nativeSetNull(JJJ)V

    goto :goto_3

    .line 508
    :cond_4
    check-cast v24, Ljava/lang/Number;

    .end local v24    # "value":Ljava/lang/Object;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Number;->longValue()J

    move-result-wide v10

    .local v10, "intValue":J
    move-object/from16 v5, p0

    .line 509
    invoke-virtual/range {v5 .. v11}, Lio/realm/internal/Table;->checkIntValueIsLegal(JJJ)V

    .line 510
    move-object/from16 v0, p0

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v11}, Lio/realm/internal/Table;->nativeSetLong(JJJJ)V

    goto :goto_3

    .line 514
    .end local v10    # "intValue":J
    .restart local v24    # "value":Ljava/lang/Object;
    :pswitch_2
    move-object/from16 v0, p0

    iget-wide v14, v0, Lio/realm/internal/Table;->nativePtr:J

    check-cast v24, Ljava/lang/Float;

    .end local v24    # "value":Ljava/lang/Object;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Float;->floatValue()F

    move-result v20

    move-object/from16 v13, p0

    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    invoke-direct/range {v13 .. v20}, Lio/realm/internal/Table;->nativeSetFloat(JJJF)V

    goto :goto_3

    .line 517
    .restart local v24    # "value":Ljava/lang/Object;
    :pswitch_3
    move-object/from16 v0, p0

    iget-wide v14, v0, Lio/realm/internal/Table;->nativePtr:J

    check-cast v24, Ljava/lang/Double;

    .end local v24    # "value":Ljava/lang/Object;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v20

    move-object/from16 v13, p0

    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    invoke-direct/range {v13 .. v21}, Lio/realm/internal/Table;->nativeSetDouble(JJJD)V

    goto :goto_3

    .line 520
    .restart local v24    # "value":Ljava/lang/Object;
    :pswitch_4
    if-nez v24, :cond_5

    .line 521
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v8, v9}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 522
    move-object/from16 v0, p0

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lio/realm/internal/Table;->nativeSetNull(JJJ)V

    goto :goto_3

    :cond_5
    move-object/from16 v18, v24

    .line 524
    check-cast v18, Ljava/lang/String;

    .local v18, "stringValue":Ljava/lang/String;
    move-object/from16 v13, p0

    move-wide v14, v6

    move-wide/from16 v16, v8

    .line 525
    invoke-virtual/range {v13 .. v18}, Lio/realm/internal/Table;->checkStringValueIsLegal(JJLjava/lang/String;)V

    .line 526
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v20, v0

    move-object/from16 v26, v24

    check-cast v26, Ljava/lang/String;

    move-object/from16 v19, p0

    move-wide/from16 v22, v6

    move-wide/from16 v24, v8

    invoke-direct/range {v19 .. v26}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;)V

    goto/16 :goto_3

    .line 530
    .end local v18    # "stringValue":Ljava/lang/String;
    :pswitch_5
    if-nez v24, :cond_6

    .line 531
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Null Date is not allowed."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 532
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v20, v0

    check-cast v24, Ljava/util/Date;

    .end local v24    # "value":Ljava/lang/Object;
    invoke-virtual/range {v24 .. v24}, Ljava/util/Date;->getTime()J

    move-result-wide v26

    move-object/from16 v19, p0

    move-wide/from16 v22, v6

    move-wide/from16 v24, v8

    invoke-direct/range {v19 .. v27}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJ)V

    goto/16 :goto_3

    .line 535
    .restart local v24    # "value":Ljava/lang/Object;
    :pswitch_6
    if-nez v24, :cond_7

    .line 536
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Null Mixed data is not allowed"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 537
    :cond_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v20, v0

    invoke-static/range {v24 .. v24}, Lio/realm/internal/Mixed;->mixedValue(Ljava/lang/Object;)Lio/realm/internal/Mixed;

    move-result-object v26

    move-object/from16 v19, p0

    move-wide/from16 v22, v6

    move-wide/from16 v24, v8

    invoke-direct/range {v19 .. v26}, Lio/realm/internal/Table;->nativeSetMixed(JJJLio/realm/internal/Mixed;)V

    goto/16 :goto_3

    .line 540
    :pswitch_7
    if-nez v24, :cond_8

    .line 541
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Null Array is not allowed"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 542
    :cond_8
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v20, v0

    check-cast v24, [B

    .end local v24    # "value":Ljava/lang/Object;
    move-object/from16 v26, v24

    check-cast v26, [B

    move-object/from16 v19, p0

    move-wide/from16 v22, v6

    move-wide/from16 v24, v8

    invoke-direct/range {v19 .. v26}, Lio/realm/internal/Table;->nativeSetByteArray(JJJ[B)V

    goto/16 :goto_3

    .restart local v24    # "value":Ljava/lang/Object;
    :pswitch_8
    move-object/from16 v19, p0

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    .line 545
    invoke-direct/range {v19 .. v24}, Lio/realm/internal/Table;->insertSubTable(JJLjava/lang/Object;)V

    goto/16 :goto_3

    .line 551
    .end local v24    # "value":Ljava/lang/Object;
    :cond_9
    return-wide v8

    .line 499
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J
    .locals 2
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 180
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J
    .locals 7
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "isNullable"    # Z

    .prologue
    .line 169
    invoke-direct {p0, p2}, Lio/realm/internal/Table;->verifyColumnName(Ljava/lang/String;)V

    .line 170
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v4

    move-object v1, p0

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeAddColumn(JILjava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J
    .locals 8
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 189
    invoke-direct {p0, p2}, Lio/realm/internal/Table;->verifyColumnName(Ljava/lang/String;)V

    .line 190
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v4

    iget-wide v6, p3, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeAddColumnLink(JILjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public addEmptyRow()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v6, -0x1

    .line 359
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 360
    invoke-virtual {p0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 361
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    .line 362
    .local v0, "primaryKeyColumnIndex":J
    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v2

    .line 363
    .local v2, "type":Lio/realm/RealmFieldType;
    sget-object v3, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v2}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 375
    new-instance v3, Lio/realm/exceptions/RealmException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 365
    :pswitch_0
    const-string v3, ""

    invoke-virtual {p0, v0, v1, v3}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 366
    const-string v3, ""

    invoke-direct {p0, v3}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 379
    .end local v0    # "primaryKeyColumnIndex":J
    .end local v2    # "type":Lio/realm/RealmFieldType;
    :cond_0
    :goto_0
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v6, 0x1

    invoke-direct {p0, v4, v5, v6, v7}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v4

    return-wide v4

    .line 370
    .restart local v0    # "primaryKeyColumnIndex":J
    .restart local v2    # "type":Lio/realm/RealmFieldType;
    :pswitch_1
    invoke-virtual {p0, v0, v1, v8, v9}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 371
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v3}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public addEmptyRowWithPrimaryKey(Ljava/lang/Object;)J
    .locals 14
    .param p1, "primaryKeyValue"    # Ljava/lang/Object;

    .prologue
    .line 383
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 384
    invoke-direct {p0}, Lio/realm/internal/Table;->checkHasPrimaryKey()V

    .line 386
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 387
    .local v4, "primaryKeyColumnIndex":J
    invoke-virtual {p0, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    .line 392
    .local v8, "type":Lio/realm/RealmFieldType;
    if-nez p1, :cond_1

    .line 393
    sget-object v9, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v8}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 405
    new-instance v9, Lio/realm/exceptions/RealmException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 396
    :pswitch_0
    invoke-virtual {p0, v4, v5}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_0

    .line 397
    const-string v9, "null"

    invoke-direct {p0, v9}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 399
    :cond_0
    iget-wide v10, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v12, 0x1

    invoke-direct {p0, v10, v11, v12, v13}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v6

    .line 400
    .local v6, "rowIndex":J
    invoke-virtual {p0, v6, v7}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v1

    .line 401
    .local v1, "row":Lio/realm/internal/UncheckedRow;
    invoke-virtual {v1, v4, v5}, Lio/realm/internal/UncheckedRow;->setNull(J)V

    .line 441
    .end local p1    # "primaryKeyValue":Ljava/lang/Object;
    :goto_0
    return-wide v6

    .line 409
    .end local v1    # "row":Lio/realm/internal/UncheckedRow;
    .end local v6    # "rowIndex":J
    .restart local p1    # "primaryKeyValue":Ljava/lang/Object;
    :cond_1
    sget-object v9, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v8}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_1

    .line 438
    new-instance v9, Lio/realm/exceptions/RealmException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 411
    :pswitch_1
    instance-of v9, p1, Ljava/lang/String;

    if-nez v9, :cond_2

    .line 412
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Primary key value is not a String: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_2
    move-object v9, p1

    .line 414
    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v4, v5, v9}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_3

    .line 415
    invoke-direct {p0, p1}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 417
    :cond_3
    iget-wide v10, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v12, 0x1

    invoke-direct {p0, v10, v11, v12, v13}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v6

    .line 418
    .restart local v6    # "rowIndex":J
    invoke-virtual {p0, v6, v7}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v1

    .line 419
    .restart local v1    # "row":Lio/realm/internal/UncheckedRow;
    check-cast p1, Ljava/lang/String;

    .end local p1    # "primaryKeyValue":Ljava/lang/Object;
    invoke-virtual {v1, v4, v5, p1}, Lio/realm/internal/UncheckedRow;->setString(JLjava/lang/String;)V

    goto :goto_0

    .line 425
    .end local v1    # "row":Lio/realm/internal/UncheckedRow;
    .end local v6    # "rowIndex":J
    .restart local p1    # "primaryKeyValue":Ljava/lang/Object;
    :pswitch_2
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 429
    .local v2, "pkValue":J
    invoke-virtual {p0, v4, v5, v2, v3}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_4

    .line 430
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-direct {p0, v9}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 432
    :cond_4
    iget-wide v10, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v12, 0x1

    invoke-direct {p0, v10, v11, v12, v13}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v6

    .line 433
    .restart local v6    # "rowIndex":J
    invoke-virtual {p0, v6, v7}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v1

    .line 434
    .restart local v1    # "row":Lio/realm/internal/UncheckedRow;
    invoke-virtual {v1, v4, v5, v2, v3}, Lio/realm/internal/UncheckedRow;->setLong(JJ)V

    goto/16 :goto_0

    .line 426
    .end local v1    # "row":Lio/realm/internal/UncheckedRow;
    .end local v2    # "pkValue":J
    .end local v6    # "rowIndex":J
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Ljava/lang/RuntimeException;
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Primary key value is not a long: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 393
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 409
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addEmptyRows(J)J
    .locals 5
    .param p1, "rows"    # J

    .prologue
    const-wide/16 v2, 0x1

    .line 445
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 446
    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    .line 447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'rows\' must be > 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 450
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    .line 451
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Multiple empty rows cannot be created if a primary key is defined for the table."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 453
    :cond_1
    invoke-virtual {p0}, Lio/realm/internal/Table;->addEmptyRow()J

    move-result-wide v0

    .line 455
    :goto_0
    return-wide v0

    :cond_2
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public addSearchIndex(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 982
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 983
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAddSearchIndex(JJ)V

    .line 984
    return-void
.end method

.method public averageDouble(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1148
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAverageDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1127
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAverageFloat(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageLong(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1106
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAverageInt(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method checkDuplicatedNullForPrimaryKeyValue(JJ)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "rowToUpdate"    # J

    .prologue
    .line 666
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->isPrimaryKeyColumn(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 667
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v2

    .line 668
    .local v2, "type":Lio/realm/RealmFieldType;
    sget-object v3, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v2}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 681
    .end local v2    # "type":Lio/realm/RealmFieldType;
    :cond_0
    :goto_0
    return-void

    .line 671
    .restart local v2    # "type":Lio/realm/RealmFieldType;
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v0

    .line 672
    .local v0, "rowIndex":J
    cmp-long v3, v0, p3

    if-eqz v3, :cond_0

    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 673
    const-string v3, "null"

    invoke-direct {p0, v3}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_0

    .line 668
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method checkImmutable()V
    .locals 1

    .prologue
    .line 1073
    invoke-virtual {p0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1074
    invoke-direct {p0}, Lio/realm/internal/Table;->throwImmutable()V

    .line 1076
    :cond_0
    return-void
.end method

.method checkIntValueIsLegal(JJJ)V
    .locals 5
    .param p1, "columnIndex"    # J
    .param p3, "rowToUpdate"    # J
    .param p5, "value"    # J

    .prologue
    .line 656
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->isPrimaryKeyColumn(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 657
    invoke-virtual {p0, p1, p2, p5, p6}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v0

    .line 658
    .local v0, "rowIndex":J
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 659
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {p0, v2}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 662
    .end local v0    # "rowIndex":J
    :cond_0
    return-void
.end method

.method checkStringValueIsLegal(JJLjava/lang/String;)V
    .locals 5
    .param p1, "columnIndex"    # J
    .param p3, "rowToUpdate"    # J
    .param p5, "value"    # Ljava/lang/String;

    .prologue
    .line 647
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->isPrimaryKey(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 648
    invoke-virtual {p0, p1, p2, p5}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v0

    .line 649
    .local v0, "rowIndex":J
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 650
    invoke-direct {p0, p5}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 653
    .end local v0    # "rowIndex":J
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 274
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeClear(J)V

    .line 275
    return-void
.end method

.method public clearSubtable(JJ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 822
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 823
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeClearSubtable(JJJ)V

    .line 824
    return-void
.end method

.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 106
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    monitor-enter v1

    .line 107
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 108
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static {v2, v3}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 113
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    .line 115
    :cond_0
    monitor-exit v1

    .line 116
    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public convertColumnToNotNullable(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 235
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeConvertColumnToNotNullable(JJ)V

    .line 236
    return-void
.end method

.method public convertColumnToNullable(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 226
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeConvertColumnToNullable(JJ)V

    .line 227
    return-void
.end method

.method public count(JD)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 1176
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeCountDouble(JJD)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JF)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 1172
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeCountFloat(JJF)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1168
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeCountLong(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JLjava/lang/String;)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1181
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeCountString(JJLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected native createNative()J
.end method

.method protected finalize()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 120
    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    monitor-enter v2

    .line 121
    :try_start_0
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 123
    .local v0, "isRoot":Z
    :goto_0
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {v1, v4, v5, v0}, Lio/realm/internal/Context;->asyncDisposeTable(JZ)V

    .line 124
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    .line 126
    .end local v0    # "isRoot":Z
    :cond_0
    monitor-exit v2

    .line 130
    return-void

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public findAllBoolean(JZ)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Z

    .prologue
    .line 1276
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1277
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindAllBool(JJZ)J

    move-result-wide v8

    .line 1279
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1280
    :catch_0
    move-exception v0

    .line 1281
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 1282
    throw v0
.end method

.method public findAllDouble(JD)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 1302
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1303
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindAllDouble(JJD)J

    move-result-wide v8

    .line 1305
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1306
    :catch_0
    move-exception v0

    .line 1307
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 1308
    throw v0
.end method

.method public findAllFloat(JF)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 1289
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1290
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindAllFloat(JJF)J

    move-result-wide v8

    .line 1292
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1293
    :catch_0
    move-exception v0

    .line 1294
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 1295
    throw v0
.end method

.method public findAllLong(JJ)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1263
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1264
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindAllInt(JJJ)J

    move-result-wide v8

    .line 1266
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1267
    :catch_0
    move-exception v0

    .line 1268
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 1269
    throw v0
.end method

.method public findAllString(JLjava/lang/String;)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1315
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1316
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindAllString(JJLjava/lang/String;)J

    move-result-wide v8

    .line 1318
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1319
    :catch_0
    move-exception v0

    .line 1320
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 1321
    throw v0
.end method

.method public findFirstBoolean(JZ)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Z

    .prologue
    .line 1222
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindFirstBool(JJZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstDate(JLjava/util/Date;)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "date"    # Ljava/util/Date;

    .prologue
    .line 1237
    if-nez p3, :cond_0

    .line 1238
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1240
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindFirstTimestamp(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstDouble(JD)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 1232
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindFirstDouble(JJD)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstFloat(JF)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 1227
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindFirstFloat(JJF)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1217
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstNull(J)J
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1258
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstString(JLjava/lang/String;)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1245
    if-nez p3, :cond_0

    .line 1246
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1248
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getBinaryByteArray(JJ)[B
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 746
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetByteArray(JJJ)[B

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 698
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetBoolean(JJJ)Z

    move-result v0

    return v0
.end method

.method public getCheckedRow(J)Lio/realm/internal/CheckedRow;
    .locals 1
    .param p1, "index"    # J

    .prologue
    .line 858
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/CheckedRow;->get(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/CheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getColumnCount()J
    .locals 2

    .prologue
    .line 285
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetColumnCount(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getColumnIndex(Ljava/lang/String;)J
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 311
    if-nez p1, :cond_0

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column name can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/Table;->nativeGetColumnIndex(JLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getColumnName(J)Ljava/lang/String;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 300
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumnType(J)Lio/realm/RealmFieldType;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 325
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnType(JJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public getDate(JJ)Ljava/util/Date;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 713
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetTimestamp(JJJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getDistinctView(J)Lio/realm/internal/TableView;
    .locals 7
    .param p1, "columnIndex"    # J

    .prologue
    .line 1350
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1351
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v4, v5, p1, p2}, Lio/realm/internal/Table;->nativeGetDistinctView(JJ)J

    move-result-wide v2

    .line 1353
    .local v2, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v4, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v4, p0, v2, v3}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1354
    :catch_0
    move-exception v0

    .line 1355
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 1356
    throw v0
.end method

.method public getDouble(JJ)D
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 708
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetDouble(JJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(JJ)F
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 703
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetFloat(JJJ)F

    move-result v0

    return v0
.end method

.method public getLink(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 760
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetLink(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLinkTarget(J)Lio/realm/internal/Table;
    .locals 7
    .param p1, "columnIndex"    # J

    .prologue
    .line 765
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 766
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v4, v5, p1, p2}, Lio/realm/internal/Table;->nativeGetLinkTarget(JJ)J

    move-result-wide v2

    .line 769
    .local v2, "nativeTablePointer":J
    :try_start_0
    new-instance v1, Lio/realm/internal/Table;

    iget-object v4, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    iget-object v5, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    invoke-direct {v1, v4, v5, v2, v3}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 771
    :catch_0
    move-exception v0

    .line 772
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 773
    throw v0
.end method

.method public getLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 693
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetLong(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMixed(JJ)Lio/realm/internal/Mixed;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 751
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetMixed(JJJ)Lio/realm/internal/Mixed;

    move-result-object v0

    return-object v0
.end method

.method public getMixedType(JJ)Lio/realm/RealmFieldType;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 756
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetMixedType(JJJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1366
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryKey()J
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v6, -0x2

    .line 606
    iget-wide v8, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    cmp-long v3, v8, v10

    if-gez v3, :cond_0

    iget-wide v8, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    cmp-long v3, v8, v6

    if-nez v3, :cond_2

    .line 607
    :cond_0
    iget-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 623
    :cond_1
    :goto_0
    return-wide v6

    .line 609
    :cond_2
    invoke-direct {p0}, Lio/realm/internal/Table;->getPrimaryKeyTable()Lio/realm/internal/Table;

    move-result-object v2

    .line 610
    .local v2, "pkTable":Lio/realm/internal/Table;
    if-eqz v2, :cond_1

    .line 614
    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lio/realm/internal/Table;->tableNameToClassName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 615
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {v2, v10, v11, v0}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v4

    .line 616
    .local v4, "rowIndex":J
    const-wide/16 v8, -0x1

    cmp-long v3, v4, v8

    if-eqz v3, :cond_3

    .line 617
    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    const-wide/16 v6, 0x1

    invoke-virtual {v3, v6, v7}, Lio/realm/internal/UncheckedRow;->getString(J)Ljava/lang/String;

    move-result-object v1

    .line 618
    .local v1, "pkColumnName":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 623
    .end local v1    # "pkColumnName":Ljava/lang/String;
    :goto_1
    iget-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    goto :goto_0

    .line 620
    :cond_3
    iput-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    goto :goto_1
.end method

.method public getSortedView(J)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J

    .prologue
    .line 585
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v0}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 586
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeGetSortedView(JJZ)J

    move-result-wide v8

    .line 587
    .local v8, "nativeViewPtr":J
    new-instance v0, Lio/realm/internal/TableView;

    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v0, v1, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V

    return-object v0
.end method

.method public getSortedView(JLio/realm/Sort;)Lio/realm/internal/TableView;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "sortOrder"    # Lio/realm/Sort;

    .prologue
    .line 567
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 568
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Sort;->getValue()Z

    move-result v6

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeGetSortedView(JJZ)J

    move-result-wide v8

    .line 570
    .local v8, "nativeViewPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableView;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/TableView;->nativeClose(J)V

    .line 573
    throw v0
.end method

.method public getSortedView([J[Lio/realm/Sort;)Lio/realm/internal/TableView;
    .locals 6
    .param p1, "columnIndices"    # [J
    .param p2, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 591
    iget-object v4, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v4}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 592
    array-length v4, p2

    new-array v1, v4, [Z

    .line 593
    .local v1, "nativeSortOrder":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_0

    .line 594
    aget-object v4, p2, v0

    invoke-virtual {v4}, Lio/realm/Sort;->getValue()Z

    move-result v4

    aput-boolean v4, v1, v0

    .line 593
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 596
    :cond_0
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v4, v5, p1, v1}, Lio/realm/internal/Table;->nativeGetSortedViewMulti(J[J[Z)J

    move-result-wide v2

    .line 597
    .local v2, "nativeViewPtr":J
    new-instance v4, Lio/realm/internal/TableView;

    iget-object v5, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v4, v5, p0, v2, v3}, Lio/realm/internal/TableView;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V

    return-object v4
.end method

.method public getString(JJ)Ljava/lang/String;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 725
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetString(JJJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubtable(JJ)Lio/realm/internal/Table;
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 789
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 790
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetSubtable(JJJ)J

    move-result-wide v8

    .line 793
    .local v8, "nativeSubtablePtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/Table;

    iget-object v2, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v2, p0, v8, v9}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Context;Ljava/lang/Object;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 795
    :catch_0
    move-exception v0

    .line 796
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v8, v9}, Lio/realm/internal/Table;->nativeClose(J)V

    .line 797
    throw v0
.end method

.method public getSubtableSchema(J)Lio/realm/internal/TableSchema;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 151
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/Table;->nativeIsRootTable(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "This is a subtable. Can only be called on root table."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 155
    :cond_0
    const/4 v1, 0x1

    new-array v0, v1, [J

    .line 156
    .local v0, "newPath":[J
    const/4 v1, 0x0

    aput-wide p1, v0, v1

    .line 157
    new-instance v1, Lio/realm/internal/SubtableSchema;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {v1, v2, v3, v0}, Lio/realm/internal/SubtableSchema;-><init>(J[J)V

    return-object v1
.end method

.method public getSubtableSize(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 818
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetSubtableSize(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 0

    .prologue
    .line 99
    return-object p0
.end method

.method getTableGroup()Lio/realm/internal/Group;
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    instance-of v0, v0, Lio/realm/internal/Group;

    if-eqz v0, :cond_0

    .line 1044
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/Group;

    .line 1048
    :goto_0
    return-object v0

    .line 1045
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    instance-of v0, v0, Lio/realm/internal/Table;

    if-eqz v0, :cond_1

    .line 1046
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->getTableGroup()Lio/realm/internal/Group;

    move-result-object v0

    goto :goto_0

    .line 1048
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTableSpec()Lio/realm/internal/TableSpec;
    .locals 2

    .prologue
    .line 289
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetTableSpec(J)Lio/realm/internal/TableSpec;

    move-result-object v0

    return-object v0
.end method

.method public getUncheckedRow(J)Lio/realm/internal/UncheckedRow;
    .locals 1
    .param p1, "index"    # J

    .prologue
    .line 834
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->getByRowIndex(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getUncheckedRowByPointer(J)Lio/realm/internal/UncheckedRow;
    .locals 1
    .param p1, "nativeRowPointer"    # J

    .prologue
    .line 845
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->getByRowPointer(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 1433
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeVersion(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public hasPrimaryKey()Z
    .locals 4

    .prologue
    .line 643
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSameSchema(Lio/realm/internal/Table;)Z
    .locals 4
    .param p1, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 1412
    if-nez p1, :cond_0

    .line 1413
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1415
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    iget-wide v2, p1, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/Table;->nativeHasSameSchema(JJ)Z

    move-result v0

    return v0
.end method

.method public hasSearchIndex(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1053
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeHasSearchIndex(JJ)Z

    move-result v0

    return v0
.end method

.method public isColumnNullable(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 217
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeIsColumnNullable(JJ)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    .line 265
    invoke-virtual {p0}, Lio/realm/internal/Table;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isImmutable()Z
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    instance-of v0, v0, Lio/realm/internal/Table;

    if-nez v0, :cond_1

    .line 1066
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/Group;

    iget-boolean v0, v0, Lio/realm/internal/Group;->immutable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1068
    :goto_0
    return v0

    .line 1066
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1068
    :cond_1
    iget-object v0, p0, Lio/realm/internal/Table;->parent:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    goto :goto_0
.end method

.method public isNullLink(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 1057
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeIsNullLink(JJJ)Z

    move-result v0

    return v0
.end method

.method public isPrimaryKey(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 634
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 4

    .prologue
    .line 140
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeIsValid(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lowerBoundLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1328
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeLowerBoundInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public maximumDate(J)Ljava/util/Date;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 1155
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/Table;->nativeMaximumTimestamp(JJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public maximumDouble(J)Ljava/lang/Double;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1138
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMaximumDouble(JJ)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(J)Ljava/lang/Float;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1117
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMaximumFloat(JJ)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumLong(J)Ljava/lang/Long;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1096
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMaximumInt(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumDate(J)Ljava/util/Date;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 1160
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/Table;->nativeMinimumTimestamp(JJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public minimumDouble(J)Ljava/lang/Double;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1143
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMinimumDouble(JJ)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(J)Ljava/lang/Float;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1122
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMinimumFloat(JJ)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumLong(J)Ljava/lang/Long;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1101
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMinimumInt(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public moveLastOver(J)V
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 354
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 355
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMoveLastOver(JJ)V

    .line 356
    return-void
.end method

.method native nativeGetRowPtr(JJ)J
.end method

.method public nullifyLink(JJ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 1061
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    .line 1062
    return-void
.end method

.method public optimize()V
    .locals 2

    .prologue
    .line 1372
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 1373
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeOptimize(J)V

    .line 1374
    return-void
.end method

.method public pivot(JJLio/realm/internal/TableOrView$PivotType;)Lio/realm/internal/Table;
    .locals 13
    .param p1, "stringCol"    # J
    .param p3, "intCol"    # J
    .param p5, "pivotType"    # Lio/realm/internal/TableOrView$PivotType;

    .prologue
    .line 1337
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {v3, v4}, Lio/realm/RealmFieldType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1338
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Group by column must be of type String"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1339
    :cond_0
    move-wide/from16 v0, p3

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {v3, v4}, Lio/realm/RealmFieldType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1340
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Aggregation column must be of type Int"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1341
    :cond_1
    new-instance v2, Lio/realm/internal/Table;

    invoke-direct {v2}, Lio/realm/internal/Table;-><init>()V

    .line 1342
    .local v2, "result":Lio/realm/internal/Table;
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v0, p5

    iget v10, v0, Lio/realm/internal/TableOrView$PivotType;->value:I

    iget-wide v11, v2, Lio/realm/internal/Table;->nativePtr:J

    move-object v3, p0

    move-wide v6, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v3 .. v12}, Lio/realm/internal/Table;->nativePivot(JJJIJ)V

    .line 1343
    return-object v2
.end method

.method public remove(J)V
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 337
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 338
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeRemove(JJ)V

    .line 339
    return-void
.end method

.method public removeColumn(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 198
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeRemoveColumn(JJ)V

    .line 199
    return-void
.end method

.method public removeFirst()V
    .locals 2

    .prologue
    .line 343
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 344
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Table;->remove(J)V

    .line 345
    return-void
.end method

.method public removeLast()V
    .locals 2

    .prologue
    .line 349
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 350
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeRemoveLast(J)V

    .line 351
    return-void
.end method

.method public removeSearchIndex(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 987
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 988
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeRemoveSearchIndex(JJ)V

    .line 989
    return-void
.end method

.method public renameColumn(JLjava/lang/String;)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "newName"    # Ljava/lang/String;

    .prologue
    .line 206
    invoke-direct {p0, p3}, Lio/realm/internal/Table;->verifyColumnName(Ljava/lang/String;)V

    .line 207
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeRenameColumn(JJLjava/lang/String;)V

    .line 208
    return-void
.end method

.method public rowToString(J)Ljava/lang/String;
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 1393
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeRowToString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setBinaryByteArray(JJ[B)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "data"    # [B

    .prologue
    .line 943
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 944
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/Table;->nativeSetByteArray(JJJ[B)V

    .line 945
    return-void
.end method

.method public setBoolean(JJZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Z

    .prologue
    .line 874
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 875
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZ)V

    .line 876
    return-void
.end method

.method public setDate(JJLjava/util/Date;)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "date"    # Ljava/util/Date;

    .prologue
    .line 892
    if-nez p5, :cond_0

    .line 893
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null Date is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 894
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 895
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual/range {p5 .. p5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJ)V

    .line 896
    return-void
.end method

.method public setDouble(JJD)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # D

    .prologue
    .line 886
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 887
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/Table;->nativeSetDouble(JJJD)V

    .line 888
    return-void
.end method

.method public setFloat(JJF)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # F

    .prologue
    .line 880
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 881
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/Table;->nativeSetFloat(JJJF)V

    .line 882
    return-void
.end method

.method public setLink(JJJ)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # J

    .prologue
    .line 963
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 964
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/Table;->nativeSetLink(JJJJ)V

    .line 965
    return-void
.end method

.method public setLong(JJJ)V
    .locals 11
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # J

    .prologue
    .line 867
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 868
    invoke-virtual/range {p0 .. p6}, Lio/realm/internal/Table;->checkIntValueIsLegal(JJJ)V

    .line 869
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/Table;->nativeSetLong(JJJJ)V

    .line 870
    return-void
.end method

.method public setMixed(JJLio/realm/internal/Mixed;)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "data"    # Lio/realm/internal/Mixed;

    .prologue
    .line 956
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 957
    if-nez p5, :cond_0

    .line 958
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 959
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/Table;->nativeSetMixed(JJJLio/realm/internal/Mixed;)V

    .line 960
    return-void
.end method

.method public setPrimaryKey(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1008
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 1009
    return-void
.end method

.method public setPrimaryKey(Ljava/lang/String;)V
    .locals 7
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 1000
    invoke-direct {p0}, Lio/realm/internal/Table;->getPrimaryKeyTable()Lio/realm/internal/Table;

    move-result-object v0

    .line 1001
    .local v0, "pkTable":Lio/realm/internal/Table;
    if-nez v0, :cond_0

    .line 1002
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Primary keys are only supported if Table is part of a Group"

    invoke-direct {v1, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1004
    :cond_0
    iget-wide v2, v0, Lio/realm/internal/Table;->nativePtr:J

    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeSetPrimaryKey(JJLjava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 1005
    return-void
.end method

.method public setString(JJLjava/lang/String;)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Ljava/lang/String;

    .prologue
    .line 907
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 908
    if-nez p5, :cond_0

    .line 909
    invoke-virtual {p0, p1, p2, p3, p4}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 910
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeSetNull(JJJ)V

    .line 915
    :goto_0
    return-void

    .line 912
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lio/realm/internal/Table;->checkStringValueIsLegal(JJLjava/lang/String;)V

    .line 913
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;)V

    goto :goto_0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 255
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public sourceRowIndex(J)J
    .locals 1
    .param p1, "rowIndex"    # J

    .prologue
    .line 1212
    return-wide p1
.end method

.method public sumDouble(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1133
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeSumDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1112
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeSumFloat(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumLong(J)J
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1091
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeSumInt(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public syncIfNeeded()J
    .locals 2

    .prologue
    .line 1398
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not supported for tables"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toJson()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1378
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeToJson(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1383
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v2, -0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/Table;->nativeToString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(J)Ljava/lang/String;
    .locals 3
    .param p1, "maxRows"    # J

    .prologue
    .line 1388
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeToString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateFromSpec(Lio/realm/internal/TableSpec;)V
    .locals 2
    .param p1, "tableSpec"    # Lio/realm/internal/TableSpec;

    .prologue
    .line 242
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 243
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/Table;->nativeUpdateFromSpec(JLio/realm/internal/TableSpec;)V

    .line 244
    return-void
.end method

.method public upperBoundLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1332
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeUpperBoundInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public where()Lio/realm/internal/TableQuery;
    .locals 6

    .prologue
    .line 1191
    iget-object v1, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v1}, Lio/realm/internal/Context;->executeDelayedDisposal()V

    .line 1192
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v4, v5}, Lio/realm/internal/Table;->nativeWhere(J)J

    move-result-wide v2

    .line 1195
    .local v2, "nativeQueryPtr":J
    :try_start_0
    new-instance v1, Lio/realm/internal/TableQuery;

    iget-object v4, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v1, v4, p0, v2, v3}, Lio/realm/internal/TableQuery;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1196
    :catch_0
    move-exception v0

    .line 1197
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-static {v2, v3}, Lio/realm/internal/TableQuery;->nativeClose(J)V

    .line 1198
    throw v0
.end method
