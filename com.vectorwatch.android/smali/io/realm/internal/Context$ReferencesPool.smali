.class Lio/realm/internal/Context$ReferencesPool;
.super Ljava/lang/Object;
.source "Context.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Context;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReferencesPool"
.end annotation


# instance fields
.field freeIndexList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field pool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lio/realm/internal/NativeObjectReference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Context$ReferencesPool;->pool:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Context$ReferencesPool;->freeIndexList:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lio/realm/internal/Context$1;)V
    .locals 0
    .param p1, "x0"    # Lio/realm/internal/Context$1;

    .prologue
    .line 31
    invoke-direct {p0}, Lio/realm/internal/Context$ReferencesPool;-><init>()V

    return-void
.end method


# virtual methods
.method add(Lio/realm/internal/NativeObjectReference;)V
    .locals 2
    .param p1, "ref"    # Lio/realm/internal/NativeObjectReference;

    .prologue
    .line 36
    iget-object v0, p0, Lio/realm/internal/Context$ReferencesPool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p1, Lio/realm/internal/NativeObjectReference;->refIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 37
    iget-object v0, p0, Lio/realm/internal/Context$ReferencesPool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Context$ReferencesPool;->pool:Ljava/util/ArrayList;

    iget-object v1, p1, Lio/realm/internal/NativeObjectReference;->refIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method getFreeIndex()Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 45
    iget-object v2, p0, Lio/realm/internal/Context$ReferencesPool;->freeIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 46
    .local v0, "freeIndexListSize":I
    if-nez v0, :cond_0

    .line 47
    iget-object v2, p0, Lio/realm/internal/Context$ReferencesPool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 51
    .local v1, "index":Ljava/lang/Integer;
    :goto_0
    return-object v1

    .line 49
    .end local v1    # "index":Ljava/lang/Integer;
    :cond_0
    iget-object v2, p0, Lio/realm/internal/Context$ReferencesPool;->freeIndexList:Ljava/util/ArrayList;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .restart local v1    # "index":Ljava/lang/Integer;
    goto :goto_0
.end method
