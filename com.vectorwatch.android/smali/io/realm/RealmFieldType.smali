.class public final enum Lio/realm/RealmFieldType;
.super Ljava/lang/Enum;
.source "RealmFieldType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/RealmFieldType;",
        ">;"
    }
.end annotation

.annotation build Lio/realm/internal/Keep;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/realm/RealmFieldType;

.field public static final enum BINARY:Lio/realm/RealmFieldType;

.field public static final enum BOOLEAN:Lio/realm/RealmFieldType;

.field public static final enum DATE:Lio/realm/RealmFieldType;

.field public static final enum DOUBLE:Lio/realm/RealmFieldType;

.field public static final enum FLOAT:Lio/realm/RealmFieldType;

.field public static final enum INTEGER:Lio/realm/RealmFieldType;

.field public static final enum LIST:Lio/realm/RealmFieldType;

.field public static final enum OBJECT:Lio/realm/RealmFieldType;

.field public static final enum STRING:Lio/realm/RealmFieldType;

.field public static final enum UNSUPPORTED_DATE:Lio/realm/RealmFieldType;

.field public static final enum UNSUPPORTED_MIXED:Lio/realm/RealmFieldType;

.field public static final enum UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

.field private static typeList:[Lio/realm/RealmFieldType;


# instance fields
.field private final nativeValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 34
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "INTEGER"

    invoke-direct {v3, v4, v7, v7}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    .line 35
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "BOOLEAN"

    invoke-direct {v3, v4, v8, v8}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    .line 36
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "STRING"

    invoke-direct {v3, v4, v9, v9}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    .line 37
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "BINARY"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5, v10}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->BINARY:Lio/realm/RealmFieldType;

    .line 38
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "UNSUPPORTED_TABLE"

    invoke-direct {v3, v4, v10, v11}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    .line 39
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "UNSUPPORTED_MIXED"

    const/4 v5, 0x6

    invoke-direct {v3, v4, v11, v5}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->UNSUPPORTED_MIXED:Lio/realm/RealmFieldType;

    .line 40
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "UNSUPPORTED_DATE"

    const/4 v5, 0x6

    const/4 v6, 0x7

    invoke-direct {v3, v4, v5, v6}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->UNSUPPORTED_DATE:Lio/realm/RealmFieldType;

    .line 41
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "DATE"

    const/4 v5, 0x7

    const/16 v6, 0x8

    invoke-direct {v3, v4, v5, v6}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    .line 42
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "FLOAT"

    const/16 v5, 0x8

    const/16 v6, 0x9

    invoke-direct {v3, v4, v5, v6}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    .line 43
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "DOUBLE"

    const/16 v5, 0x9

    const/16 v6, 0xa

    invoke-direct {v3, v4, v5, v6}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    .line 44
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "OBJECT"

    const/16 v5, 0xa

    const/16 v6, 0xc

    invoke-direct {v3, v4, v5, v6}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    .line 45
    new-instance v3, Lio/realm/RealmFieldType;

    const-string v4, "LIST"

    const/16 v5, 0xb

    const/16 v6, 0xd

    invoke-direct {v3, v4, v5, v6}, Lio/realm/RealmFieldType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    .line 31
    const/16 v3, 0xc

    new-array v3, v3, [Lio/realm/RealmFieldType;

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v4, v3, v7

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v4, v3, v8

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v4, v3, v9

    const/4 v4, 0x3

    sget-object v5, Lio/realm/RealmFieldType;->BINARY:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    sget-object v4, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    aput-object v4, v3, v10

    sget-object v4, Lio/realm/RealmFieldType;->UNSUPPORTED_MIXED:Lio/realm/RealmFieldType;

    aput-object v4, v3, v11

    const/4 v4, 0x6

    sget-object v5, Lio/realm/RealmFieldType;->UNSUPPORTED_DATE:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    const/4 v4, 0x7

    sget-object v5, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    const/16 v4, 0x8

    sget-object v5, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    const/16 v4, 0x9

    sget-object v5, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    const/16 v4, 0xa

    sget-object v5, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    const/16 v4, 0xb

    sget-object v5, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    aput-object v5, v3, v4

    sput-object v3, Lio/realm/RealmFieldType;->$VALUES:[Lio/realm/RealmFieldType;

    .line 49
    const/16 v3, 0xf

    new-array v3, v3, [Lio/realm/RealmFieldType;

    sput-object v3, Lio/realm/RealmFieldType;->typeList:[Lio/realm/RealmFieldType;

    .line 51
    invoke-static {}, Lio/realm/RealmFieldType;->values()[Lio/realm/RealmFieldType;

    move-result-object v0

    .line 52
    .local v0, "columnTypes":[Lio/realm/RealmFieldType;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 53
    aget-object v3, v0, v1

    iget v2, v3, Lio/realm/RealmFieldType;->nativeValue:I

    .line 54
    .local v2, "v":I
    sget-object v3, Lio/realm/RealmFieldType;->typeList:[Lio/realm/RealmFieldType;

    aget-object v4, v0, v1

    aput-object v4, v3, v2

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "v":I
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nativeValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput p3, p0, Lio/realm/RealmFieldType;->nativeValue:I

    .line 62
    return-void
.end method

.method public static fromNativeValue(I)Lio/realm/RealmFieldType;
    .locals 4
    .param p0, "value"    # I

    .prologue
    .line 112
    if-ltz p0, :cond_0

    sget-object v1, Lio/realm/RealmFieldType;->typeList:[Lio/realm/RealmFieldType;

    array-length v1, v1

    if-ge p0, v1, :cond_0

    .line 113
    sget-object v1, Lio/realm/RealmFieldType;->typeList:[Lio/realm/RealmFieldType;

    aget-object v0, v1, p0

    .line 114
    .local v0, "e":Lio/realm/RealmFieldType;
    if-eqz v0, :cond_0

    .line 115
    return-object v0

    .line 118
    .end local v0    # "e":Lio/realm/RealmFieldType;
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid native Realm type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/RealmFieldType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lio/realm/RealmFieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/RealmFieldType;

    return-object v0
.end method

.method public static values()[Lio/realm/RealmFieldType;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lio/realm/RealmFieldType;->$VALUES:[Lio/realm/RealmFieldType;

    invoke-virtual {v0}, [Lio/realm/RealmFieldType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/RealmFieldType;

    return-object v0
.end method


# virtual methods
.method public getNativeValue()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lio/realm/RealmFieldType;->nativeValue:I

    return v0
.end method

.method public isValid(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 79
    iget v2, p0, Lio/realm/RealmFieldType;->nativeValue:I

    packed-switch v2, :pswitch_data_0

    .line 100
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported Realm type:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :pswitch_1
    instance-of v2, p1, Ljava/lang/Long;

    if-nez v2, :cond_0

    instance-of v2, p1, Ljava/lang/Integer;

    if-nez v2, :cond_0

    instance-of v2, p1, Ljava/lang/Short;

    if-nez v2, :cond_0

    instance-of v2, p1, Ljava/lang/Byte;

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 99
    :cond_1
    :goto_0
    :pswitch_2
    return v0

    .line 81
    :pswitch_3
    instance-of v0, p1, Ljava/lang/Boolean;

    goto :goto_0

    .line 82
    :pswitch_4
    instance-of v0, p1, Ljava/lang/String;

    goto :goto_0

    .line 83
    :pswitch_5
    instance-of v2, p1, [B

    if-nez v2, :cond_2

    instance-of v2, p1, Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    :cond_2
    move v0, v1

    goto :goto_0

    .line 84
    :pswitch_6
    if-eqz p1, :cond_3

    instance-of v2, p1, [[Ljava/lang/Object;

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 85
    :pswitch_7
    instance-of v2, p1, Lio/realm/internal/Mixed;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Long;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Integer;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Short;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Byte;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Boolean;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Float;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/Double;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/lang/String;

    if-nez v2, :cond_4

    instance-of v2, p1, [B

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/nio/ByteBuffer;

    if-nez v2, :cond_4

    if-eqz p1, :cond_4

    instance-of v2, p1, [[Ljava/lang/Object;

    if-nez v2, :cond_4

    instance-of v2, p1, Ljava/util/Date;

    if-eqz v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0

    .line 93
    :pswitch_8
    instance-of v0, p1, Ljava/util/Date;

    goto :goto_0

    .line 94
    :pswitch_9
    instance-of v0, p1, Ljava/util/Date;

    goto :goto_0

    .line 95
    :pswitch_a
    instance-of v0, p1, Ljava/lang/Float;

    goto :goto_0

    .line 96
    :pswitch_b
    instance-of v0, p1, Ljava/lang/Double;

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
