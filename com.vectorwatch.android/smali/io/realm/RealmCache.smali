.class final Lio/realm/RealmCache;
.super Ljava/lang/Object;
.source "RealmCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RealmCache$RealmCacheType;,
        Lio/realm/RealmCache$RefAndCount;,
        Lio/realm/RealmCache$Callback0;,
        Lio/realm/RealmCache$Callback;
    }
.end annotation


# static fields
.field private static final DIFFERENT_KEY_MESSAGE:Ljava/lang/String; = "Wrong key used to decrypt Realm."

.field private static final WRONG_REALM_CLASS_MESSAGE:Ljava/lang/String; = "The type of Realm class must be Realm or DynamicRealm."

.field private static cachesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/RealmCache;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final configuration:Lio/realm/RealmConfiguration;

.field private final refAndCountMap:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lio/realm/RealmCache$RealmCacheType;",
            "Lio/realm/RealmCache$RefAndCount;",
            ">;"
        }
    .end annotation
.end field

.field private typedColumnIndices:Lio/realm/internal/ColumnIndices;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lio/realm/RealmConfiguration;)V
    .locals 7
    .param p1, "config"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    .line 88
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, Lio/realm/RealmCache$RealmCacheType;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    .line 89
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 90
    .local v0, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v4, p0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    new-instance v5, Lio/realm/RealmCache$RefAndCount;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lio/realm/RealmCache$RefAndCount;-><init>(Lio/realm/RealmCache$1;)V

    invoke-virtual {v4, v0, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v0    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_0
    return-void
.end method

.method private static copyAssetFileIfNeeded(Lio/realm/RealmConfiguration;)V
    .locals 10
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 294
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->hasAssetFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 295
    new-instance v6, Ljava/io/File;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getRealmFolder()Ljava/io/File;

    move-result-object v7

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getRealmFileName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 296
    .local v6, "realmFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 333
    .end local v6    # "realmFile":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 300
    .restart local v6    # "realmFile":Ljava/io/File;
    :cond_1
    const/4 v3, 0x0

    .line 301
    .local v3, "inputStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 303
    .local v4, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getAssetFile()Ljava/io/InputStream;

    move-result-object v3

    .line 304
    if-nez v3, :cond_4

    .line 305
    new-instance v7, Lio/realm/exceptions/RealmIOException;

    const-string v8, "Invalid input stream to asset file."

    invoke-direct {v7, v8}, Lio/realm/exceptions/RealmIOException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :catch_0
    move-exception v2

    .line 315
    .local v2, "e":Ljava/io/IOException;
    :goto_1
    :try_start_1
    new-instance v7, Lio/realm/exceptions/RealmIOException;

    const-string v8, "Could not resolve the path to the Realm asset file."

    invoke-direct {v7, v8, v2}, Lio/realm/exceptions/RealmIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 317
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v3, :cond_2

    .line 319
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 324
    :cond_2
    :goto_3
    if-eqz v4, :cond_3

    .line 326
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 328
    :cond_3
    throw v7

    .line 308
    :cond_4
    :try_start_4
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 309
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .local v5, "outputStream":Ljava/io/FileOutputStream;
    const/16 v7, 0x1000

    :try_start_5
    new-array v0, v7, [B

    .line 311
    .local v0, "buf":[B
    :goto_4
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    const/4 v7, -0x1

    if-le v1, v7, :cond_5

    .line 312
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4

    .line 314
    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    :catch_1
    move-exception v2

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 317
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buf":[B
    .restart local v1    # "bytesRead":I
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :cond_5
    if-eqz v3, :cond_6

    .line 319
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 324
    :cond_6
    :goto_5
    if-eqz v5, :cond_0

    .line 326
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    .line 327
    :catch_2
    move-exception v2

    .line 328
    .restart local v2    # "e":Ljava/io/IOException;
    new-instance v7, Lio/realm/exceptions/RealmIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid output stream to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Lio/realm/exceptions/RealmIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 327
    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    .line 328
    .restart local v2    # "e":Ljava/io/IOException;
    new-instance v7, Lio/realm/exceptions/RealmIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid output stream to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Lio/realm/exceptions/RealmIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 320
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buf":[B
    .restart local v1    # "bytesRead":I
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v7

    goto :goto_5

    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v8

    goto :goto_3

    .line 317
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method static declared-synchronized createRealmOrGetFromCache(Lio/realm/RealmConfiguration;Ljava/lang/Class;)Lio/realm/BaseRealm;
    .locals 9
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lio/realm/BaseRealm;",
            ">(",
            "Lio/realm/RealmConfiguration;",
            "Ljava/lang/Class",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "realmClass":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const-class v7, Lio/realm/RealmCache;

    monitor-enter v7

    const/4 v1, 0x1

    .line 104
    .local v1, "isCacheInMap":Z
    :try_start_0
    sget-object v6, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 105
    .local v0, "cache":Lio/realm/RealmCache;
    if-nez v0, :cond_4

    .line 107
    new-instance v0, Lio/realm/RealmCache;

    .end local v0    # "cache":Lio/realm/RealmCache;
    invoke-direct {v0, p0}, Lio/realm/RealmCache;-><init>(Lio/realm/RealmConfiguration;)V

    .line 109
    .restart local v0    # "cache":Lio/realm/RealmCache;
    const/4 v1, 0x0

    .line 111
    invoke-static {p0}, Lio/realm/RealmCache;->copyAssetFileIfNeeded(Lio/realm/RealmConfiguration;)V

    .line 117
    :goto_0
    iget-object v6, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-static {p1}, Lio/realm/RealmCache$RealmCacheType;->valueOf(Ljava/lang/Class;)Lio/realm/RealmCache$RealmCacheType;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/RealmCache$RefAndCount;

    .line 119
    .local v4, "refAndCount":Lio/realm/RealmCache$RefAndCount;
    # getter for: Lio/realm/RealmCache$RefAndCount;->localRealm:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 124
    const-class v6, Lio/realm/Realm;

    if-ne p1, v6, :cond_5

    .line 126
    iget-object v6, v0, Lio/realm/RealmCache;->typedColumnIndices:Lio/realm/internal/ColumnIndices;

    invoke-static {p0, v6}, Lio/realm/Realm;->createInstance(Lio/realm/RealmConfiguration;Lio/realm/internal/ColumnIndices;)Lio/realm/Realm;

    move-result-object v2

    .line 136
    .local v2, "realm":Lio/realm/BaseRealm;
    :goto_1
    if-nez v1, :cond_0

    .line 137
    sget-object v6, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_0
    # getter for: Lio/realm/RealmCache$RefAndCount;->localRealm:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 140
    # getter for: Lio/realm/RealmCache$RefAndCount;->localCount:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 143
    .end local v2    # "realm":Lio/realm/BaseRealm;
    :cond_1
    # getter for: Lio/realm/RealmCache$RefAndCount;->localCount:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 144
    .local v5, "refCount":Ljava/lang/Integer;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_3

    .line 145
    const-class v6, Lio/realm/Realm;

    if-ne p1, v6, :cond_2

    # getter for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)I

    move-result v6

    if-nez v6, :cond_2

    .line 146
    # getter for: Lio/realm/RealmCache$RefAndCount;->localRealm:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/realm/BaseRealm;

    iget-object v6, v6, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    iget-object v6, v6, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    iput-object v6, v0, Lio/realm/RealmCache;->typedColumnIndices:Lio/realm/internal/ColumnIndices;

    .line 149
    :cond_2
    # operator++ for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$308(Lio/realm/RealmCache$RefAndCount;)I

    .line 151
    :cond_3
    # getter for: Lio/realm/RealmCache$RefAndCount;->localCount:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 154
    # getter for: Lio/realm/RealmCache$RefAndCount;->localRealm:Ljava/lang/ThreadLocal;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/BaseRealm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    .local v3, "realm":Lio/realm/BaseRealm;, "TE;"
    monitor-exit v7

    return-object v3

    .line 114
    .end local v3    # "realm":Lio/realm/BaseRealm;, "TE;"
    .end local v4    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .end local v5    # "refCount":Ljava/lang/Integer;
    :cond_4
    :try_start_1
    invoke-direct {v0, p0}, Lio/realm/RealmCache;->validateConfiguration(Lio/realm/RealmConfiguration;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 103
    .end local v0    # "cache":Lio/realm/RealmCache;
    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    .line 127
    .restart local v0    # "cache":Lio/realm/RealmCache;
    .restart local v4    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    :cond_5
    :try_start_2
    const-class v6, Lio/realm/DynamicRealm;

    if-ne p1, v6, :cond_6

    .line 128
    invoke-static {p0}, Lio/realm/DynamicRealm;->createInstance(Lio/realm/RealmConfiguration;)Lio/realm/DynamicRealm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/BaseRealm;
    goto :goto_1

    .line 130
    .end local v2    # "realm":Lio/realm/BaseRealm;
    :cond_6
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v8, "The type of Realm class must be Realm or DynamicRealm."

    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method static declared-synchronized invokeWithGlobalRefCount(Lio/realm/RealmConfiguration;Lio/realm/RealmCache$Callback;)V
    .locals 8
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "callback"    # Lio/realm/RealmCache$Callback;

    .prologue
    const/4 v3, 0x0

    .line 265
    const-class v5, Lio/realm/RealmCache;

    monitor-enter v5

    :try_start_0
    sget-object v4, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 266
    .local v0, "cache":Lio/realm/RealmCache;
    if-nez v0, :cond_0

    .line 267
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Lio/realm/RealmCache$Callback;->onResult(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :goto_0
    monitor-exit v5

    return-void

    .line 270
    :cond_0
    const/4 v1, 0x0

    .line 271
    .local v1, "totalRefCount":I
    :try_start_1
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v6

    array-length v7, v6

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_1

    aget-object v2, v6, v4

    .line 272
    .local v2, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v3, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {v3, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmCache$RefAndCount;

    # getter for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v3}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)I

    move-result v3

    add-int/2addr v1, v3

    .line 271
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 274
    .end local v2    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_1
    invoke-interface {p1, v1}, Lio/realm/RealmCache$Callback;->onResult(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 265
    .end local v0    # "cache":Lio/realm/RealmCache;
    .end local v1    # "totalRefCount":I
    :catchall_0
    move-exception v3

    monitor-exit v5

    throw v3
.end method

.method static declared-synchronized invokeWithLock(Lio/realm/RealmCache$Callback0;)V
    .locals 2
    .param p0, "callback"    # Lio/realm/RealmCache$Callback0;

    .prologue
    .line 283
    const-class v0, Lio/realm/RealmCache;

    monitor-enter v0

    :try_start_0
    invoke-interface {p0}, Lio/realm/RealmCache$Callback0;->onCall()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    monitor-exit v0

    return-void

    .line 283
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static declared-synchronized release(Lio/realm/BaseRealm;)V
    .locals 11
    .param p0, "realm"    # Lio/realm/BaseRealm;

    .prologue
    const/4 v6, 0x0

    .line 165
    const-class v8, Lio/realm/RealmCache;

    monitor-enter v8

    :try_start_0
    invoke-virtual {p0}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "canonicalPath":Ljava/lang/String;
    sget-object v7, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 167
    .local v0, "cache":Lio/realm/RealmCache;
    const/4 v3, 0x0

    .line 168
    .local v3, "refCount":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 170
    .local v2, "refAndCount":Lio/realm/RealmCache$RefAndCount;
    if-eqz v0, :cond_0

    .line 171
    iget-object v7, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9}, Lio/realm/RealmCache$RealmCacheType;->valueOf(Ljava/lang/Class;)Lio/realm/RealmCache$RealmCacheType;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    check-cast v2, Lio/realm/RealmCache$RefAndCount;

    .line 172
    .restart local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    # getter for: Lio/realm/RealmCache$RefAndCount;->localCount:Ljava/lang/ThreadLocal;
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "refCount":Ljava/lang/Integer;
    check-cast v3, Ljava/lang/Integer;

    .line 174
    .restart local v3    # "refCount":Ljava/lang/Integer;
    :cond_0
    if-nez v3, :cond_1

    .line 175
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 178
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-gtz v7, :cond_2

    .line 179
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Realm "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " has been closed already."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lio/realm/internal/log/RealmLog;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :goto_0
    monitor-exit v8

    return-void

    .line 184
    :cond_2
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 186
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_7

    .line 189
    # getter for: Lio/realm/RealmCache$RefAndCount;->localCount:Ljava/lang/ThreadLocal;
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 190
    # getter for: Lio/realm/RealmCache$RefAndCount;->localRealm:Ljava/lang/ThreadLocal;
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 193
    # operator-- for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$310(Lio/realm/RealmCache$RefAndCount;)I

    .line 194
    # getter for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    if-gez v7, :cond_3

    .line 196
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Global reference counter of Realm"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " got corrupted."

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    .end local v0    # "cache":Lio/realm/RealmCache;
    .end local v1    # "canonicalPath":Ljava/lang/String;
    .end local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .end local v3    # "refCount":Ljava/lang/Integer;
    :catchall_0
    move-exception v6

    monitor-exit v8

    throw v6

    .line 201
    .restart local v0    # "cache":Lio/realm/RealmCache;
    .restart local v1    # "canonicalPath":Ljava/lang/String;
    .restart local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .restart local v3    # "refCount":Ljava/lang/Integer;
    :cond_3
    :try_start_2
    instance-of v7, p0, Lio/realm/Realm;

    if-eqz v7, :cond_4

    # getter for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    if-nez v7, :cond_4

    .line 203
    const/4 v7, 0x0

    iput-object v7, v0, Lio/realm/RealmCache;->typedColumnIndices:Lio/realm/internal/ColumnIndices;

    .line 206
    :cond_4
    const/4 v4, 0x0

    .line 207
    .local v4, "totalRefCount":I
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v9

    array-length v10, v9

    move v7, v6

    :goto_1
    if-ge v7, v10, :cond_5

    aget-object v5, v9, v7

    .line 208
    .local v5, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v6, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {v6, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/realm/RealmCache$RefAndCount;

    # getter for: Lio/realm/RealmCache$RefAndCount;->globalCount:I
    invoke-static {v6}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)I

    move-result v6

    add-int/2addr v4, v6

    .line 207
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 211
    .end local v5    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_5
    if-nez v4, :cond_6

    .line 212
    sget-object v6, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    :cond_6
    invoke-virtual {p0}, Lio/realm/BaseRealm;->doClose()V

    goto :goto_0

    .line 218
    .end local v4    # "totalRefCount":I
    :cond_7
    # getter for: Lio/realm/RealmCache$RefAndCount;->localCount:Ljava/lang/ThreadLocal;
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private validateConfiguration(Lio/realm/RealmConfiguration;)V
    .locals 5
    .param p1, "newConfiguration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 229
    iget-object v2, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2, p1}, Lio/realm/RealmConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 231
    return-void

    .line 235
    :cond_0
    iget-object v2, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v2

    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1

    .line 236
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Wrong key used to decrypt Realm."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 240
    :cond_1
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v0

    .line 241
    .local v0, "newMigration":Lio/realm/RealmMigration;
    iget-object v2, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v1

    .line 242
    .local v1, "oldMigration":Lio/realm/RealmMigration;
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 244
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 246
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Configurations cannot be different if used to open the same file. The most likely cause is that equals() and hashCode() are not overridden in the migration class: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 248
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 251
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Configurations cannot be different if used to open the same file. \nCached configuration: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nNew configuration: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
