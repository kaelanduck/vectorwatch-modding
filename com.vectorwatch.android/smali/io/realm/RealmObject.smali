.class public abstract Lio/realm/RealmObject;
.super Ljava/lang/Object;
.source "RealmObject.java"

# interfaces
.implements Lio/realm/RealmModel;


# annotations
.annotation build Lio/realm/annotations/RealmClass;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addChangeListener(Lio/realm/RealmModel;Lio/realm/RealmChangeListener;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;",
            "Lio/realm/RealmChangeListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<TE;>;"
    if-nez p0, :cond_0

    .line 237
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Object should not be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 239
    :cond_0
    if-nez p1, :cond_1

    .line 240
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Listener should not be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 242
    :cond_1
    instance-of v3, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v3, :cond_5

    move-object v1, p0

    .line 243
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 244
    .local v1, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    .line 245
    .local v2, "realm":Lio/realm/BaseRealm;
    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 246
    iget-object v3, v2, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    if-nez v3, :cond_2

    .line 247
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "You can\'t register a listener from a non-Looper thread "

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 249
    :cond_2
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getListeners$realm()Ljava/util/List;

    move-result-object v0

    .line 250
    .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lio/realm/RealmChangeListener;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 251
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    :cond_3
    invoke-static {v1}, Lio/realm/RealmObject;->isLoaded(Lio/realm/RealmModel;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 257
    iget-object v3, v2, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v3, v1}, Lio/realm/HandlerController;->addToRealmObjects(Lio/realm/internal/RealmObjectProxy;)V

    .line 262
    :cond_4
    return-void

    .line 260
    .end local v0    # "listeners":Ljava/util/List;, "Ljava/util/List<Lio/realm/RealmChangeListener;>;"
    .end local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local v2    # "realm":Lio/realm/BaseRealm;
    :cond_5
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Cannot add listener from this unmanaged RealmObject (created outside of Realm)"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static asObservable(Lio/realm/RealmModel;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)",
            "Lrx/Observable",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 386
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    instance-of v5, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_2

    move-object v3, p0

    .line 387
    check-cast v3, Lio/realm/internal/RealmObjectProxy;

    .line 388
    .local v3, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v3}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    .line 389
    .local v4, "realm":Lio/realm/BaseRealm;
    instance-of v5, v4, Lio/realm/Realm;

    if-eqz v5, :cond_0

    .line 390
    iget-object v5, v4, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v5}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v5

    check-cast v4, Lio/realm/Realm;

    .end local v4    # "realm":Lio/realm/BaseRealm;
    invoke-interface {v5, v4, p0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/Realm;Lio/realm/RealmModel;)Lrx/Observable;

    move-result-object v2

    .line 396
    :goto_0
    return-object v2

    .line 391
    .restart local v4    # "realm":Lio/realm/BaseRealm;
    :cond_0
    instance-of v5, v4, Lio/realm/DynamicRealm;

    if-eqz v5, :cond_1

    move-object v1, v4

    .line 392
    check-cast v1, Lio/realm/DynamicRealm;

    .local v1, "dynamicRealm":Lio/realm/DynamicRealm;
    move-object v0, p0

    .line 393
    check-cast v0, Lio/realm/DynamicRealmObject;

    .line 395
    .local v0, "dynamicObject":Lio/realm/DynamicRealmObject;
    iget-object v5, v4, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v5}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v5

    invoke-interface {v5, v1, v0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/DynamicRealm;Lio/realm/DynamicRealmObject;)Lrx/Observable;

    move-result-object v2

    .line 396
    .local v2, "observable":Lrx/Observable;, "Lrx/Observable<TE;>;"
    goto :goto_0

    .line 398
    .end local v0    # "dynamicObject":Lio/realm/DynamicRealmObject;
    .end local v1    # "dynamicRealm":Lio/realm/DynamicRealm;
    .end local v2    # "observable":Lrx/Observable;, "Lrx/Observable<TE;>;"
    :cond_1
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " does not support RxJava."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " See https://realm.io/docs/java/latest/#rxjava for more details."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 403
    .end local v3    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local v4    # "realm":Lio/realm/BaseRealm;
    :cond_2
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Cannot create Observables from unmanaged RealmObjects"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public static deleteFromRealm(Lio/realm/RealmModel;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    instance-of v2, p0, Lio/realm/internal/RealmObjectProxy;

    if-nez v2, :cond_0

    .line 95
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Object not managed by Realm, so it cannot be removed."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v0, p0

    .line 98
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 99
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    if-nez v2, :cond_1

    .line 100
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Object malformed: missing object in Realm. Make sure to instantiate RealmObjects with Realm.createObject()"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 102
    :cond_1
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-nez v2, :cond_2

    .line 103
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Object malformed: missing Realm. Make sure to instantiate RealmObjects with Realm.createObject()"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 106
    :cond_2
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 107
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    .line 108
    .local v1, "row":Lio/realm/internal/Row;
    invoke-interface {v1}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    invoke-interface {v1}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->moveLastOver(J)V

    .line 109
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    sget-object v3, Lio/realm/internal/InvalidRow;->INSTANCE:Lio/realm/internal/InvalidRow;

    invoke-virtual {v2, v3}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 110
    return-void
.end method

.method public static isLoaded(Lio/realm/RealmModel;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)Z"
        }
    .end annotation

    .prologue
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    const/4 v1, 0x1

    .line 173
    instance-of v2, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v2, :cond_0

    move-object v0, p0

    .line 174
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 175
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 176
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getPendingQuery$realm()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->isCompleted$realm()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_0
    :goto_0
    return v1

    .line 176
    .restart local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValid(Lio/realm/RealmModel;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)Z"
        }
    .end annotation

    .prologue
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    const/4 v2, 0x0

    .line 142
    instance-of v3, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v3, :cond_0

    move-object v0, p0

    .line 143
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 144
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    .line 145
    .local v1, "row":Lio/realm/internal/Row;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lio/realm/internal/Row;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 147
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local v1    # "row":Lio/realm/internal/Row;
    :cond_0
    return v2
.end method

.method public static load(Lio/realm/RealmModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)Z"
        }
    .end annotation

    .prologue
    .line 202
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    invoke-static {p0}, Lio/realm/RealmObject;->isLoaded(Lio/realm/RealmModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const/4 v0, 0x1

    .line 210
    .end local p0    # "object":Lio/realm/RealmModel;, "TE;"
    :goto_0
    return v0

    .line 205
    .restart local p0    # "object":Lio/realm/RealmModel;, "TE;"
    :cond_0
    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_1

    .line 208
    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "object":Lio/realm/RealmModel;, "TE;"
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/ProxyState;->onCompleted$realm()Z

    move-result v0

    goto :goto_0

    .line 210
    .restart local p0    # "object":Lio/realm/RealmModel;, "TE;"
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static removeChangeListener(Lio/realm/RealmModel;Lio/realm/RealmChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lio/realm/RealmChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;",
            "Lio/realm/RealmChangeListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    if-nez p0, :cond_0

    .line 287
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Object should not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 289
    :cond_0
    if-nez p1, :cond_1

    .line 290
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Listener should not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 292
    :cond_1
    instance-of v1, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v1, :cond_2

    move-object v0, p0

    .line 293
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 294
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 295
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getListeners$realm()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 299
    return-void

    .line 297
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot remove listener from this unmanaged RealmObject (created outside of Realm)"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static removeChangeListeners(Lio/realm/RealmModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p0, "object":Lio/realm/RealmModel;, "TE;"
    instance-of v1, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v1, :cond_0

    move-object v0, p0

    .line 316
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 317
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 318
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getListeners$realm()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 322
    return-void

    .line 320
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot remove listeners from this unmanaged RealmObject (created outside of Realm)"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final addChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/RealmChangeListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<TE;>;"
    invoke-static {p0, p1}, Lio/realm/RealmObject;->addChangeListener(Lio/realm/RealmModel;Lio/realm/RealmChangeListener;)V

    .line 224
    return-void
.end method

.method public final asObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lio/realm/RealmObject;",
            ">()",
            "Lrx/Observable",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 357
    invoke-static {p0}, Lio/realm/RealmObject;->asObservable(Lio/realm/RealmModel;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final deleteFromRealm()V
    .locals 0

    .prologue
    .line 80
    invoke-static {p0}, Lio/realm/RealmObject;->deleteFromRealm(Lio/realm/RealmModel;)V

    .line 81
    return-void
.end method

.method public final isLoaded()Z
    .locals 1

    .prologue
    .line 160
    invoke-static {p0}, Lio/realm/RealmObject;->isLoaded(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public final isValid()Z
    .locals 1

    .prologue
    .line 131
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public final load()Z
    .locals 1

    .prologue
    .line 190
    invoke-static {p0}, Lio/realm/RealmObject;->load(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public final removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lio/realm/RealmChangeListener;

    .prologue
    .line 273
    invoke-static {p0, p1}, Lio/realm/RealmObject;->removeChangeListener(Lio/realm/RealmModel;Lio/realm/RealmChangeListener;)V

    .line 274
    return-void
.end method

.method public final removeChangeListeners()V
    .locals 0

    .prologue
    .line 305
    invoke-static {p0}, Lio/realm/RealmObject;->removeChangeListeners(Lio/realm/RealmModel;)V

    .line 306
    return-void
.end method
