.class Lio/realm/BaseRealm$1;
.super Ljava/lang/Object;
.source "BaseRealm.java"

# interfaces
.implements Lio/realm/RealmCache$Callback0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/BaseRealm;->stopWaitForChange()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/BaseRealm;


# direct methods
.method constructor <init>(Lio/realm/BaseRealm;)V
    .locals 0
    .param p1, "this$0"    # Lio/realm/BaseRealm;

    .prologue
    .line 292
    iput-object p1, p0, Lio/realm/BaseRealm$1;->this$0:Lio/realm/BaseRealm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCall()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lio/realm/BaseRealm$1;->this$0:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/BaseRealm$1;->this$0:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/BaseRealm$1;->this$0:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->getSharedGroup()Lio/realm/internal/SharedGroup;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This Realm instance has already been closed, making it unusable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_1
    iget-object v0, p0, Lio/realm/BaseRealm$1;->this$0:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->getSharedGroup()Lio/realm/internal/SharedGroup;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/SharedGroup;->stopWaitForChange()V

    .line 300
    return-void
.end method
