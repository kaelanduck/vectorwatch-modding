.class final Lio/realm/HandlerController;
.super Ljava/lang/Object;
.source "HandlerController.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field static final COMPLETED_ASYNC_REALM_OBJECT:I = 0x3c50ea2

.field static final COMPLETED_ASYNC_REALM_RESULTS:I = 0x2547029

.field static final COMPLETED_UPDATE_ASYNC_QUERIES:I = 0x1709e79

.field private static final NO_REALM_QUERY:Ljava/lang/Boolean;

.field static final REALM_ASYNC_BACKGROUND_EXCEPTION:I = 0x6197ecb

.field static final REALM_CHANGED:I = 0xe3d1b0


# instance fields
.field final asyncRealmResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;",
            "Lio/realm/RealmQuery",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private autoRefresh:Z

.field final changeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;>;"
        }
    .end annotation
.end field

.field final emptyAsyncRealmObject:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;",
            "Lio/realm/RealmQuery",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field

.field final realm:Lio/realm/BaseRealm;

.field final realmObjects:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final referenceQueueAsyncRealmResults:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field

.field final referenceQueueRealmObject:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Lio/realm/RealmModel;",
            ">;"
        }
    .end annotation
.end field

.field private final referenceQueueSyncRealmResults:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field

.field final syncRealmResults:Lio/realm/internal/IdentitySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/internal/IdentitySet",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private updateAsyncQueriesTask:Ljava/util/concurrent/Future;

.field final weakChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lio/realm/HandlerController;->NO_REALM_QUERY:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lio/realm/BaseRealm;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/BaseRealm;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->changeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 62
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->referenceQueueAsyncRealmResults:Ljava/lang/ref/ReferenceQueue;

    .line 73
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->referenceQueueSyncRealmResults:Ljava/lang/ref/ReferenceQueue;

    .line 75
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->referenceQueueRealmObject:Ljava/lang/ref/ReferenceQueue;

    .line 81
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    .line 85
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    .line 90
    new-instance v0, Lio/realm/internal/IdentitySet;

    invoke-direct {v0}, Lio/realm/internal/IdentitySet;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->syncRealmResults:Lio/realm/internal/IdentitySet;

    .line 97
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    .line 101
    iput-object p1, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    .line 102
    return-void
.end method

.method private completedAsyncQueriesUpdate(Lio/realm/internal/async/QueryUpdateTask$Result;)V
    .locals 12
    .param p1, "result"    # Lio/realm/internal/async/QueryUpdateTask$Result;

    .prologue
    .line 457
    iget-object v8, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v8, v8, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v8}, Lio/realm/internal/SharedGroupManager;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v1

    .line 458
    .local v1, "callerVersionID":Lio/realm/internal/SharedGroup$VersionID;
    iget-object v8, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    invoke-virtual {v1, v8}, Lio/realm/internal/SharedGroup$VersionID;->compareTo(Lio/realm/internal/SharedGroup$VersionID;)I

    move-result v2

    .line 459
    .local v2, "compare":I
    if-lez v2, :cond_0

    .line 461
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "COMPLETED_UPDATE_ASYNC_QUERIES realm:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " caller is more advanced, Looper will updates queries"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 519
    :goto_0
    return-void

    .line 467
    :cond_0
    if-eqz v2, :cond_1

    .line 472
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "COMPLETED_UPDATE_ASYNC_QUERIES realm:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " caller is behind  advance_read"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 477
    :try_start_0
    iget-object v8, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v8, v8, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    iget-object v9, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    invoke-virtual {v8, v9}, Lio/realm/internal/SharedGroupManager;->advanceRead(Lio/realm/internal/SharedGroup$VersionID;)V
    :try_end_0
    .catch Lio/realm/internal/async/BadVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v8, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    invoke-virtual {v8}, Ljava/util/IdentityHashMap;->size()I

    move-result v8

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 487
    .local v0, "callbacksToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    iget-object v8, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    invoke-virtual {v8}, Ljava/util/IdentityHashMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 488
    .local v5, "query":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Ljava/lang/Long;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/ref/WeakReference;

    .line 489
    .local v7, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/realm/RealmResults;

    .line 490
    .local v6, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    if-nez v6, :cond_2

    .line 492
    iget-object v8, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 478
    .end local v0    # "callbacksToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    .end local v5    # "query":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Ljava/lang/Long;>;"
    .end local v6    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .end local v7    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :catch_0
    move-exception v3

    .line 481
    .local v3, "e":Lio/realm/internal/async/BadVersionException;
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Failed to advance Caller Realm to Worker Realm version"

    invoke-direct {v8, v9, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 496
    .end local v3    # "e":Lio/realm/internal/async/BadVersionException;
    .restart local v0    # "callbacksToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    .restart local v5    # "query":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Ljava/lang/Long;>;"
    .restart local v6    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .restart local v7    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_2
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lio/realm/RealmResults;->swapTableViewPointer(J)V

    .line 501
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "COMPLETED_UPDATE_ASYNC_QUERIES realm:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " updating RealmResults "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 507
    .end local v5    # "query":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Ljava/lang/Long;>;"
    .end local v6    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .end local v7    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/RealmResults;

    .line 508
    .local v4, "query":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    invoke-virtual {v4}, Lio/realm/RealmResults;->notifyChangeListeners()V

    goto :goto_2

    .line 513
    .end local v4    # "query":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    :cond_4
    invoke-direct {p0}, Lio/realm/HandlerController;->notifyGlobalListeners()V

    .line 514
    invoke-direct {p0}, Lio/realm/HandlerController;->notifySyncRealmResultsCallbacks()V

    .line 515
    invoke-direct {p0}, Lio/realm/HandlerController;->notifyRealmObjectCallbacks()V

    .line 517
    const/4 v8, 0x0

    iput-object v8, p0, Lio/realm/HandlerController;->updateAsyncQueriesTask:Ljava/util/concurrent/Future;

    goto/16 :goto_0
.end method

.method private completedAsyncRealmObject(Lio/realm/internal/async/QueryUpdateTask$Result;)V
    .locals 14
    .param p1, "result"    # Lio/realm/internal/async/QueryUpdateTask$Result;

    .prologue
    .line 522
    iget-object v10, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedRow:Ljava/util/IdentityHashMap;

    invoke-virtual {v10}, Ljava/util/IdentityHashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    .line 523
    .local v8, "updatedRowKey":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;>;"
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v10

    if-lez v10, :cond_1

    .line 524
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 525
    .local v4, "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    .line 527
    .local v2, "proxy":Lio/realm/internal/RealmObjectProxy;
    if-eqz v2, :cond_1

    .line 528
    iget-object v10, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v10, v10, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v10}, Lio/realm/internal/SharedGroupManager;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v0

    .line 529
    .local v0, "callerVersionID":Lio/realm/internal/SharedGroup$VersionID;
    iget-object v10, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    invoke-virtual {v0, v10}, Lio/realm/internal/SharedGroup$VersionID;->compareTo(Lio/realm/internal/SharedGroup$VersionID;)I

    move-result v1

    .line 532
    .local v1, "compare":I
    if-nez v1, :cond_2

    .line 533
    iget-object v10, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedRow:Ljava/util/IdentityHashMap;

    invoke-virtual {v10, v4}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 534
    .local v6, "rowPointer":J
    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    invoke-interface {v10, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 536
    iget-object v10, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    invoke-interface {v10, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    iget-object v10, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v11, Lio/realm/HandlerController;->NO_REALM_QUERY:Ljava/lang/Boolean;

    invoke-virtual {v10, v4, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    :cond_0
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Lio/realm/ProxyState;->onCompleted$realm(J)V

    .line 540
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/ProxyState;->notifyChangeListeners$realm()V

    .line 579
    .end local v0    # "callerVersionID":Lio/realm/internal/SharedGroup$VersionID;
    .end local v1    # "compare":I
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local v4    # "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    .end local v6    # "rowPointer":J
    :cond_1
    :goto_0
    return-void

    .line 542
    .restart local v0    # "callerVersionID":Lio/realm/internal/SharedGroup$VersionID;
    .restart local v1    # "compare":I
    .restart local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .restart local v4    # "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    :cond_2
    if-lez v1, :cond_6

    .line 545
    invoke-static {v2}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 546
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[COMPLETED_ASYNC_REALM_OBJECT "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] , realm:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " RealmObject is already loaded, just notify it."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 548
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/ProxyState;->notifyChangeListeners$realm()V

    goto :goto_0

    .line 551
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[COMPLETED_ASYNC_REALM_OBJECT "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] , realm:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " RealmObject is not loaded yet. Rerun the query."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 553
    iget-object v10, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 555
    .local v9, "value":Ljava/lang/Object;
    if-eqz v9, :cond_4

    sget-object v10, Lio/realm/HandlerController;->NO_REALM_QUERY:Ljava/lang/Boolean;

    if-ne v9, v10, :cond_5

    .line 556
    :cond_4
    iget-object v10, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    invoke-interface {v10, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lio/realm/RealmQuery;

    .line 562
    .local v5, "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    :goto_1
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask;->newBuilder()Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;

    move-result-object v10

    iget-object v11, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    .line 563
    invoke-virtual {v11}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v11

    invoke-interface {v10, v11}, Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;->realmConfiguration(Lio/realm/RealmConfiguration;)Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;

    move-result-object v10

    .line 565
    invoke-virtual {v5}, Lio/realm/RealmQuery;->handoverQueryPointer()J

    move-result-wide v12

    .line 566
    invoke-virtual {v5}, Lio/realm/RealmQuery;->getArgument()Lio/realm/internal/async/ArgumentsHolder;

    move-result-object v11

    .line 564
    invoke-interface {v10, v4, v12, v13, v11}, Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;->addObject(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)Lio/realm/internal/async/QueryUpdateTask$Builder$HandlerStep;

    move-result-object v10

    iget-object v11, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v11, v11, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    const v12, 0x3c50ea2

    .line 567
    invoke-interface {v10, v11, v12}, Lio/realm/internal/async/QueryUpdateTask$Builder$HandlerStep;->sendToHandler(Landroid/os/Handler;I)Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;

    move-result-object v10

    .line 568
    invoke-interface {v10}, Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;->build()Lio/realm/internal/async/QueryUpdateTask;

    move-result-object v3

    .line 570
    .local v3, "queryUpdateTask":Lio/realm/internal/async/QueryUpdateTask;
    sget-object v10, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    invoke-virtual {v10, v3}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .end local v3    # "queryUpdateTask":Lio/realm/internal/async/QueryUpdateTask;
    .end local v5    # "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    :cond_5
    move-object v5, v9

    .line 559
    check-cast v5, Lio/realm/RealmQuery;

    .restart local v5    # "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    goto :goto_1

    .line 575
    .end local v5    # "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    .end local v9    # "value":Ljava/lang/Object;
    :cond_6
    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "Caller thread behind the Worker thread"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10
.end method

.method private completedAsyncRealmResults(Lio/realm/internal/async/QueryUpdateTask$Result;)V
    .locals 11
    .param p1, "result"    # Lio/realm/internal/async/QueryUpdateTask$Result;

    .prologue
    .line 390
    iget-object v7, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    invoke-virtual {v7}, Ljava/util/IdentityHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 391
    .local v5, "updatedTableViewsKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;>;"
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 392
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    .line 394
    .local v6, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/RealmResults;

    .line 395
    .local v4, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    if-nez v4, :cond_1

    .line 396
    iget-object v7, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[COMPLETED_ASYNC_REALM_RESULTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " RealmResults GC\'d ignore results"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 454
    .end local v4    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .end local v6    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_0
    :goto_0
    return-void

    .line 400
    .restart local v4    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .restart local v6    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_1
    iget-object v7, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v7, v7, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v7}, Lio/realm/internal/SharedGroupManager;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v0

    .line 401
    .local v0, "callerVersionID":Lio/realm/internal/SharedGroup$VersionID;
    iget-object v7, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    invoke-virtual {v0, v7}, Lio/realm/internal/SharedGroup$VersionID;->compareTo(Lio/realm/internal/SharedGroup$VersionID;)I

    move-result v1

    .line 402
    .local v1, "compare":I
    if-nez v1, :cond_3

    .line 405
    invoke-virtual {v4}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v7

    if-nez v7, :cond_2

    .line 406
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[COMPLETED_ASYNC_REALM_RESULTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] , realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " same versions, using results (RealmResults is not loaded)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 408
    iget-object v7, p1, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    invoke-virtual {v7, v6}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lio/realm/RealmResults;->swapTableViewPointer(J)V

    .line 410
    invoke-virtual {v4}, Lio/realm/RealmResults;->notifyChangeListeners()V

    goto :goto_0

    .line 412
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[COMPLETED_ASYNC_REALM_RESULTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] , realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ignoring result the RealmResults (is already loaded)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 415
    :cond_3
    if-lez v1, :cond_5

    .line 425
    invoke-virtual {v4}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v7

    if-nez v7, :cond_4

    .line 427
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[COMPLETED_ASYNC_REALM_RESULTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] , realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " caller is more advanced & RealmResults is not loaded, rerunning the query against the latest version"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 429
    iget-object v7, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmQuery;

    .line 430
    .local v2, "query":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<*>;"
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask;->newBuilder()Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;

    move-result-object v7

    iget-object v8, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    .line 431
    invoke-virtual {v8}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v8

    invoke-interface {v7, v8}, Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;->realmConfiguration(Lio/realm/RealmConfiguration;)Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;

    move-result-object v7

    .line 433
    invoke-virtual {v2}, Lio/realm/RealmQuery;->handoverQueryPointer()J

    move-result-wide v8

    .line 434
    invoke-virtual {v2}, Lio/realm/RealmQuery;->getArgument()Lio/realm/internal/async/ArgumentsHolder;

    move-result-object v10

    .line 432
    invoke-interface {v7, v6, v8, v9, v10}, Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;->add(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;

    move-result-object v7

    iget-object v8, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v8, v8, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    const v9, 0x2547029

    .line 435
    invoke-interface {v7, v8, v9}, Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;->sendToHandler(Landroid/os/Handler;I)Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;

    move-result-object v7

    .line 436
    invoke-interface {v7}, Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;->build()Lio/realm/internal/async/QueryUpdateTask;

    move-result-object v3

    .line 438
    .local v3, "queryUpdateTask":Lio/realm/internal/async/QueryUpdateTask;
    sget-object v7, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    invoke-virtual {v7, v3}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 442
    .end local v2    # "query":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<*>;"
    .end local v3    # "queryUpdateTask":Lio/realm/internal/async/QueryUpdateTask;
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[COMPLETED_ASYNC_REALM_RESULTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] , realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " caller is more advanced & RealmResults is loaded ignore the outdated result"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 450
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[COMPLETED_ASYNC_REALM_RESULTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] , realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " caller thread behind worker thread, ignore results (a batch update will update everything including this query)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private deleteWeakReferences()V
    .locals 3

    .prologue
    .line 627
    :goto_0
    iget-object v2, p0, Lio/realm/HandlerController;->referenceQueueAsyncRealmResults:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v2}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v1

    .local v1, "weakReferenceResults":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<+Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    if-eqz v1, :cond_0

    .line 628
    iget-object v2, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 630
    :cond_0
    :goto_1
    iget-object v2, p0, Lio/realm/HandlerController;->referenceQueueSyncRealmResults:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v2}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 631
    iget-object v2, p0, Lio/realm/HandlerController;->syncRealmResults:Lio/realm/internal/IdentitySet;

    invoke-virtual {v2, v1}, Lio/realm/internal/IdentitySet;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 633
    :cond_1
    :goto_2
    iget-object v2, p0, Lio/realm/HandlerController;->referenceQueueRealmObject:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v2}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    .local v0, "weakReferenceObject":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<+Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_2

    .line 634
    iget-object v2, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 636
    :cond_2
    return-void
.end method

.method private notifyAsyncRealmResultsCallbacks()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/HandlerController;->notifyRealmResultsCallbacks(Ljava/util/Iterator;)V

    .line 281
    return-void
.end method

.method private notifyGlobalListeners()V
    .locals 6

    .prologue
    .line 215
    iget-object v5, p0, Lio/realm/HandlerController;->changeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 216
    .local v0, "iteratorStrongListeners":Ljava/util/Iterator;, "Ljava/util/Iterator<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 217
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmChangeListener;

    .line 218
    .local v2, "listener":Lio/realm/RealmChangeListener;
    iget-object v5, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-interface {v2, v5}, Lio/realm/RealmChangeListener;->onChange(Ljava/lang/Object;)V

    goto :goto_0

    .line 221
    .end local v2    # "listener":Lio/realm/RealmChangeListener;
    :cond_0
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 222
    .local v1, "iteratorWeakListeners":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    const/4 v3, 0x0

    .line 223
    .local v3, "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    .line 224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 225
    .local v4, "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmChangeListener;

    .line 226
    .restart local v2    # "listener":Lio/realm/RealmChangeListener;
    if-nez v2, :cond_2

    .line 227
    if-nez v3, :cond_1

    .line 228
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 230
    .restart local v3    # "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    :cond_1
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 232
    :cond_2
    iget-object v5, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-interface {v2, v5}, Lio/realm/RealmChangeListener;->onChange(Ljava/lang/Object;)V

    goto :goto_1

    .line 235
    .end local v2    # "listener":Lio/realm/RealmChangeListener;
    .end local v4    # "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    :cond_3
    if-eqz v3, :cond_4

    .line 236
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 238
    :cond_4
    return-void
.end method

.method private notifyRealmObjectCallbacks()V
    .locals 7

    .prologue
    .line 308
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 309
    .local v2, "objectsToBeNotified":Ljava/util/List;, "Ljava/util/List<Lio/realm/internal/RealmObjectProxy;>;"
    iget-object v5, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 310
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 311
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 312
    .local v4, "weakRealmObject":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/RealmObjectProxy;

    .line 313
    .local v3, "realmObject":Lio/realm/internal/RealmObjectProxy;
    if-nez v3, :cond_1

    .line 314
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 317
    :cond_1
    invoke-interface {v3}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v5

    invoke-interface {v5}, Lio/realm/internal/Row;->isAttached()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 319
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 320
    :cond_2
    invoke-interface {v3}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v5

    sget-object v6, Lio/realm/internal/Row;->EMPTY_ROW:Lio/realm/internal/Row;

    if-eq v5, v6, :cond_0

    .line 321
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 326
    .end local v3    # "realmObject":Lio/realm/internal/RealmObjectProxy;
    .end local v4    # "weakRealmObject":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lio/realm/internal/RealmObjectProxy;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v5

    if-nez v5, :cond_4

    .line 327
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/RealmObjectProxy;

    .line 328
    .restart local v3    # "realmObject":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v3}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->notifyChangeListeners$realm()V

    goto :goto_1

    .line 330
    .end local v3    # "realmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_4
    return-void
.end method

.method private notifyRealmResultsCallbacks(Ljava/util/Iterator;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v2, "resultsToBeNotified":Ljava/util/List;, "Ljava/util/List<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 291
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 292
    .local v3, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmResults;

    .line 293
    .local v1, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    if-nez v1, :cond_0

    .line 294
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 297
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 301
    .end local v1    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .end local v3    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v4}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v4

    if-nez v4, :cond_2

    .line 302
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmResults;

    .line 303
    .restart local v1    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->notifyChangeListeners()V

    goto :goto_1

    .line 305
    .end local v1    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    :cond_2
    return-void
.end method

.method private notifySyncRealmResultsCallbacks()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lio/realm/HandlerController;->syncRealmResults:Lio/realm/internal/IdentitySet;

    invoke-virtual {v0}, Lio/realm/internal/IdentitySet;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/HandlerController;->notifyRealmResultsCallbacks(Ljava/util/Iterator;)V

    .line 285
    return-void
.end method

.method private notifyTypeBasedListeners()V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Lio/realm/HandlerController;->notifyAsyncRealmResultsCallbacks()V

    .line 275
    invoke-direct {p0}, Lio/realm/HandlerController;->notifySyncRealmResultsCallbacks()V

    .line 276
    invoke-direct {p0}, Lio/realm/HandlerController;->notifyRealmObjectCallbacks()V

    .line 277
    return-void
.end method

.method private realmChanged()V
    .locals 2

    .prologue
    .line 378
    invoke-direct {p0}, Lio/realm/HandlerController;->deleteWeakReferences()V

    .line 379
    invoke-direct {p0}, Lio/realm/HandlerController;->threadContainsAsyncQueries()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    invoke-direct {p0}, Lio/realm/HandlerController;->updateAsyncQueries()V

    .line 387
    :goto_0
    return-void

    .line 383
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REALM_CHANGED realm:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " no async queries, advance_read"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->advanceRead()V

    .line 385
    invoke-virtual {p0}, Lio/realm/HandlerController;->notifyAllListeners()V

    goto :goto_0
.end method

.method private threadContainsAsyncQueries()Z
    .locals 4

    .prologue
    .line 589
    const/4 v0, 0x1

    .line 590
    .local v0, "isEmpty":Z
    iget-object v3, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 591
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Lio/realm/RealmQuery<*>;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 592
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 593
    .local v2, "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Lio/realm/RealmQuery<*>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 594
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 596
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 600
    .end local v2    # "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Lio/realm/RealmQuery<*>;>;"
    :cond_1
    if-nez v0, :cond_2

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private updateAsyncEmptyRealmObject()V
    .locals 8

    .prologue
    .line 241
    iget-object v2, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 242
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;Lio/realm/RealmQuery<*>;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 243
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 244
    .local v1, "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;Lio/realm/RealmQuery<*>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 245
    sget-object v4, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    .line 246
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask;->newBuilder()Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;

    move-result-object v2

    iget-object v3, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    .line 247
    invoke-virtual {v3}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;->realmConfiguration(Lio/realm/RealmConfiguration;)Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;

    move-result-object v5

    .line 248
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 249
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmQuery;

    invoke-virtual {v3}, Lio/realm/RealmQuery;->handoverQueryPointer()J

    move-result-wide v6

    .line 250
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmQuery;

    invoke-virtual {v3}, Lio/realm/RealmQuery;->getArgument()Lio/realm/internal/async/ArgumentsHolder;

    move-result-object v3

    .line 248
    invoke-interface {v5, v2, v6, v7, v3}, Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;->addObject(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)Lio/realm/internal/async/QueryUpdateTask$Builder$HandlerStep;

    move-result-object v2

    iget-object v3, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    const v5, 0x3c50ea2

    .line 251
    invoke-interface {v2, v3, v5}, Lio/realm/internal/async/QueryUpdateTask$Builder$HandlerStep;->sendToHandler(Landroid/os/Handler;I)Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;

    move-result-object v2

    .line 252
    invoke-interface {v2}, Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;->build()Lio/realm/internal/async/QueryUpdateTask;

    move-result-object v2

    .line 246
    invoke-virtual {v4, v2}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 255
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 258
    .end local v1    # "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;Lio/realm/RealmQuery<*>;>;"
    :cond_1
    return-void
.end method

.method private updateAsyncQueries()V
    .locals 10

    .prologue
    .line 333
    iget-object v7, p0, Lio/realm/HandlerController;->updateAsyncQueriesTask:Ljava/util/concurrent/Future;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lio/realm/HandlerController;->updateAsyncQueriesTask:Ljava/util/concurrent/Future;

    invoke-interface {v7}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v7

    if-nez v7, :cond_0

    .line 335
    iget-object v7, p0, Lio/realm/HandlerController;->updateAsyncQueriesTask:Ljava/util/concurrent/Future;

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 336
    sget-object v7, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    invoke-virtual {v7}, Lio/realm/internal/async/RealmThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v7

    iget-object v8, p0, Lio/realm/HandlerController;->updateAsyncQueriesTask:Ljava/util/concurrent/Future;

    invoke-interface {v7, v8}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 337
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "REALM_CHANGED realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cancelling pending COMPLETED_UPDATE_ASYNC_QUERIES updates"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 339
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "REALM_CHANGED realm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " updating async queries, total: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    .line 341
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask;->newBuilder()Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;

    move-result-object v7

    iget-object v8, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    .line 342
    invoke-virtual {v8}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v8

    invoke-interface {v7, v8}, Lio/realm/internal/async/QueryUpdateTask$Builder$RealmConfigurationStep;->realmConfiguration(Lio/realm/RealmConfiguration;)Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;

    move-result-object v5

    .line 343
    .local v5, "updateQueryStep":Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;
    const/4 v4, 0x0

    .line 346
    .local v4, "realmResultsQueryStep":Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;
    iget-object v7, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 347
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Lio/realm/RealmQuery<*>;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 348
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 349
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Lio/realm/RealmQuery<*>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    .line 350
    .local v6, "weakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmResults;

    .line 351
    .local v3, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    if-nez v3, :cond_1

    .line 353
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 357
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lio/realm/RealmQuery;

    invoke-virtual {v7}, Lio/realm/RealmQuery;->handoverQueryPointer()J

    move-result-wide v8

    .line 358
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lio/realm/RealmQuery;

    invoke-virtual {v7}, Lio/realm/RealmQuery;->getArgument()Lio/realm/internal/async/ArgumentsHolder;

    move-result-object v7

    .line 356
    invoke-interface {v5, v6, v8, v9, v7}, Lio/realm/internal/async/QueryUpdateTask$Builder$UpdateQueryStep;->add(Ljava/lang/ref/WeakReference;JLio/realm/internal/async/ArgumentsHolder;)Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;

    move-result-object v4

    goto :goto_0

    .line 369
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;Lio/realm/RealmQuery<*>;>;"
    .end local v3    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .end local v6    # "weakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_2
    if-eqz v4, :cond_3

    .line 370
    iget-object v7, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v7, v7, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    const v8, 0x1709e79

    .line 371
    invoke-interface {v4, v7, v8}, Lio/realm/internal/async/QueryUpdateTask$Builder$RealmResultsQueryStep;->sendToHandler(Landroid/os/Handler;I)Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;

    move-result-object v7

    .line 372
    invoke-interface {v7}, Lio/realm/internal/async/QueryUpdateTask$Builder$BuilderStep;->build()Lio/realm/internal/async/QueryUpdateTask;

    move-result-object v2

    .line 373
    .local v2, "queryUpdateTask":Lio/realm/internal/async/QueryUpdateTask;
    sget-object v7, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    invoke-virtual {v7, v2}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v7

    iput-object v7, p0, Lio/realm/HandlerController;->updateAsyncQueriesTask:Ljava/util/concurrent/Future;

    .line 375
    .end local v2    # "queryUpdateTask":Lio/realm/internal/async/QueryUpdateTask;
    :cond_3
    return-void
.end method


# virtual methods
.method addChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    iget-object v0, p0, Lio/realm/HandlerController;->changeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 148
    return-void
.end method

.method addChangeListenerAsWeakReference(Lio/realm/RealmChangeListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 160
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    const/4 v2, 0x0

    .line 161
    .local v2, "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    const/4 v0, 0x1

    .line 162
    .local v0, "addListener":Z
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 163
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 164
    .local v4, "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmChangeListener;

    .line 167
    .local v3, "weakListener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    if-nez v3, :cond_2

    .line 168
    if-nez v2, :cond_1

    .line 169
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 171
    .restart local v2    # "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    :cond_1
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_2
    if-ne v3, p1, :cond_0

    .line 176
    const/4 v0, 0x0

    goto :goto_0

    .line 179
    .end local v3    # "weakListener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    .end local v4    # "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    :cond_3
    if-eqz v2, :cond_4

    .line 180
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 182
    :cond_4
    if-eqz v0, :cond_5

    .line 183
    iget-object v5, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    :cond_5
    return-void
.end method

.method addToAsyncRealmObject(Lio/realm/internal/RealmObjectProxy;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/internal/RealmObjectProxy;",
            ">(TE;",
            "Lio/realm/RealmQuery",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 665
    .local p1, "realmObject":Lio/realm/internal/RealmObjectProxy;, "TE;"
    .local p2, "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lio/realm/HandlerController;->referenceQueueRealmObject:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p1, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 666
    .local v0, "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    iget-object v1, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    return-object v0
.end method

.method addToAsyncRealmResults(Lio/realm/RealmResults;Lio/realm/RealmQuery;)Ljava/lang/ref/WeakReference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/RealmQuery",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 639
    .local p1, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .local p2, "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lio/realm/HandlerController;->referenceQueueAsyncRealmResults:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p1, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 641
    .local v0, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    iget-object v1, p0, Lio/realm/HandlerController;->asyncRealmResults:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    return-object v0
.end method

.method addToEmptyAsyncRealmObject(Ljava/lang/ref/WeakReference;Lio/realm/RealmQuery;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;",
            "Lio/realm/RealmQuery",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 675
    .local p1, "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    .local p2, "realmQuery":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<+Lio/realm/RealmModel;>;"
    iget-object v0, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    return-void
.end method

.method addToRealmObjects(Lio/realm/internal/RealmObjectProxy;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/internal/RealmObjectProxy;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 654
    .local p1, "realmObject":Lio/realm/internal/RealmObjectProxy;, "TE;"
    iget-object v2, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 655
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 662
    .end local v1    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    :goto_0
    return-void

    .line 659
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lio/realm/HandlerController;->referenceQueueRealmObject:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 661
    .local v0, "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    iget-object v2, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v3, Lio/realm/HandlerController;->NO_REALM_QUERY:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method addToRealmResults(Lio/realm/RealmResults;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmResults",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 646
    .local p1, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lio/realm/HandlerController;->referenceQueueSyncRealmResults:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p1, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 648
    .local v0, "realmResultsWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    iget-object v1, p0, Lio/realm/HandlerController;->syncRealmResults:Lio/realm/internal/IdentitySet;

    invoke-virtual {v1, v0}, Lio/realm/internal/IdentitySet;->add(Ljava/lang/Object;)V

    .line 649
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 111
    iget-object v1, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v1, v1, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    if-eqz v1, :cond_0

    .line 113
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 140
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 116
    :sswitch_0
    invoke-direct {p0}, Lio/realm/HandlerController;->realmChanged()V

    .line 143
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 120
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/async/QueryUpdateTask$Result;

    .line 121
    .local v0, "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    invoke-direct {p0, v0}, Lio/realm/HandlerController;->completedAsyncRealmResults(Lio/realm/internal/async/QueryUpdateTask$Result;)V

    goto :goto_0

    .line 125
    .end local v0    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/async/QueryUpdateTask$Result;

    .line 126
    .restart local v0    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    invoke-direct {p0, v0}, Lio/realm/HandlerController;->completedAsyncRealmObject(Lio/realm/internal/async/QueryUpdateTask$Result;)V

    goto :goto_0

    .line 131
    .end local v0    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lio/realm/internal/async/QueryUpdateTask$Result;

    .line 132
    .restart local v0    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    invoke-direct {p0, v0}, Lio/realm/HandlerController;->completedAsyncQueriesUpdate(Lio/realm/internal/async/QueryUpdateTask$Result;)V

    goto :goto_0

    .line 137
    .end local v0    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    :sswitch_4
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Error;

    throw v1

    .line 113
    :sswitch_data_0
    .sparse-switch
        0xe3d1b0 -> :sswitch_0
        0x1709e79 -> :sswitch_3
        0x2547029 -> :sswitch_1
        0x3c50ea2 -> :sswitch_2
        0x6197ecb -> :sswitch_4
    .end sparse-switch
.end method

.method public isAutoRefreshEnabled()Z
    .locals 1

    .prologue
    .line 707
    iget-boolean v0, p0, Lio/realm/HandlerController;->autoRefresh:Z

    return v0
.end method

.method notifyAllListeners()V
    .locals 1

    .prologue
    .line 261
    invoke-direct {p0}, Lio/realm/HandlerController;->notifyGlobalListeners()V

    .line 262
    invoke-direct {p0}, Lio/realm/HandlerController;->notifyTypeBasedListeners()V

    .line 268
    iget-object v0, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lio/realm/HandlerController;->threadContainsAsyncEmptyRealmObject()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    invoke-direct {p0}, Lio/realm/HandlerController;->updateAsyncEmptyRealmObject()V

    .line 271
    :cond_0
    return-void
.end method

.method public notifyCurrentThreadRealmChanged()V
    .locals 2

    .prologue
    .line 714
    iget-object v0, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lio/realm/HandlerController;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    const v1, 0xe3d1b0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 717
    :cond_0
    return-void
.end method

.method public refreshSynchronousTableViews()V
    .locals 4

    .prologue
    .line 687
    iget-object v3, p0, Lio/realm/HandlerController;->syncRealmResults:Lio/realm/internal/IdentitySet;

    invoke-virtual {v3}, Lio/realm/internal/IdentitySet;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 688
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 689
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 690
    .local v2, "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmResults;

    .line 691
    .local v1, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    if-nez v1, :cond_0

    .line 692
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 694
    :cond_0
    invoke-virtual {v1}, Lio/realm/RealmResults;->syncIfNeeded()V

    goto :goto_0

    .line 697
    .end local v1    # "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<+Lio/realm/RealmModel;>;"
    .end local v2    # "weakRealmResults":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmResults<+Lio/realm/RealmModel;>;>;"
    :cond_1
    return-void
.end method

.method removeAllChangeListeners()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lio/realm/HandlerController;->changeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 211
    return-void
.end method

.method removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    iget-object v0, p0, Lio/realm/HandlerController;->changeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 207
    return-void
.end method

.method removeFromAsyncRealmObject(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 671
    .local p1, "realmObjectWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;"
    iget-object v0, p0, Lio/realm/HandlerController;->realmObjects:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    return-void
.end method

.method removeWeakChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    const/4 v1, 0x0

    .line 189
    .local v1, "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 190
    iget-object v4, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 191
    .local v3, "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmChangeListener;

    .line 194
    .local v2, "weakListener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    if-eqz v2, :cond_0

    if-ne v2, p1, :cond_2

    .line 195
    :cond_0
    if-nez v1, :cond_1

    .line 196
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    iget-object v4, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 198
    .restart local v1    # "toRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;>;"
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    .end local v2    # "weakListener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    .end local v3    # "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;>;"
    :cond_3
    iget-object v4, p0, Lio/realm/HandlerController;->weakChangeListeners:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 203
    return-void
.end method

.method public setAutoRefresh(Z)V
    .locals 2
    .param p1, "autoRefresh"    # Z

    .prologue
    .line 700
    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 701
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot enabled autorefresh on a non-looper thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 703
    :cond_0
    iput-boolean p1, p0, Lio/realm/HandlerController;->autoRefresh:Z

    .line 704
    return-void
.end method

.method threadContainsAsyncEmptyRealmObject()Z
    .locals 4

    .prologue
    .line 610
    const/4 v0, 0x1

    .line 611
    .local v0, "isEmpty":Z
    iget-object v3, p0, Lio/realm/HandlerController;->emptyAsyncRealmObject:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 612
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;Lio/realm/RealmQuery<*>;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 613
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 614
    .local v2, "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;Lio/realm/RealmQuery<*>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 615
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 617
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 621
    .end local v2    # "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/ref/WeakReference<Lio/realm/internal/RealmObjectProxy;>;Lio/realm/RealmQuery<*>;>;"
    :cond_1
    if-nez v0, :cond_2

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
