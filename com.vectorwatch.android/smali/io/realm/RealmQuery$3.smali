.class Lio/realm/RealmQuery$3;
.super Ljava/lang/Object;
.source "RealmQuery.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/RealmQuery;->findAllSortedAsync(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/RealmQuery;

.field final synthetic val$fieldName:Ljava/lang/String;

.field final synthetic val$handoverQueryPointer:J

.field final synthetic val$realmConfiguration:Lio/realm/RealmConfiguration;

.field final synthetic val$sortOrder:Lio/realm/Sort;

.field final synthetic val$weakHandler:Ljava/lang/ref/WeakReference;

.field final synthetic val$weakRealmResults:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;Ljava/lang/String;JLio/realm/Sort;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 0
    .param p1, "this$0"    # Lio/realm/RealmQuery;

    .prologue
    .line 1591
    .local p0, "this":Lio/realm/RealmQuery$3;, "Lio/realm/RealmQuery$3;"
    iput-object p1, p0, Lio/realm/RealmQuery$3;->this$0:Lio/realm/RealmQuery;

    iput-object p2, p0, Lio/realm/RealmQuery$3;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    iput-object p3, p0, Lio/realm/RealmQuery$3;->val$fieldName:Ljava/lang/String;

    iput-wide p4, p0, Lio/realm/RealmQuery$3;->val$handoverQueryPointer:J

    iput-object p6, p0, Lio/realm/RealmQuery$3;->val$sortOrder:Lio/realm/Sort;

    iput-object p7, p0, Lio/realm/RealmQuery$3;->val$weakRealmResults:Ljava/lang/ref/WeakReference;

    iput-object p8, p0, Lio/realm/RealmQuery$3;->val$weakHandler:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Long;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1594
    .local p0, "this":Lio/realm/RealmQuery$3;, "Lio/realm/RealmQuery$3;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1595
    const/16 v16, 0x0

    .line 1598
    .local v16, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_0
    new-instance v17, Lio/realm/internal/SharedGroup;

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery$3;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/RealmQuery$3;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    .line 1600
    invoke-virtual {v5}, Lio/realm/RealmConfiguration;->getDurability()Lio/realm/internal/SharedGroup$Durability;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/RealmQuery$3;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    .line 1601
    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v6

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4, v5, v6}, Lio/realm/internal/SharedGroup;-><init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V
    :try_end_0
    .catch Lio/realm/internal/async/BadVersionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1603
    .end local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .local v17, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery$3;->this$0:Lio/realm/RealmQuery;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery$3;->val$fieldName:Ljava/lang/String;

    # invokes: Lio/realm/RealmQuery;->getColumnIndexForSort(Ljava/lang/String;)J
    invoke-static {v3, v4}, Lio/realm/RealmQuery;->access$300(Lio/realm/RealmQuery;Ljava/lang/String;)J

    move-result-wide v10

    .line 1606
    .local v10, "columnIndex":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery$3;->this$0:Lio/realm/RealmQuery;

    # getter for: Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;
    invoke-static {v3}, Lio/realm/RealmQuery;->access$000(Lio/realm/RealmQuery;)Lio/realm/internal/TableQuery;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/SharedGroup;->getNativePointer()J

    move-result-wide v4

    .line 1607
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/SharedGroup;->getNativeReplicationPointer()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/RealmQuery$3;->val$handoverQueryPointer:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lio/realm/RealmQuery$3;->val$sortOrder:Lio/realm/Sort;

    .line 1606
    invoke-virtual/range {v3 .. v12}, Lio/realm/internal/TableQuery;->findAllSortedWithHandover(JJJJLio/realm/Sort;)J

    move-result-wide v14

    .line 1609
    .local v14, "handoverTableViewPointer":J
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask$Result;->newRealmResultsResponse()Lio/realm/internal/async/QueryUpdateTask$Result;

    move-result-object v13

    .line 1610
    .local v13, "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    iget-object v3, v13, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery$3;->val$weakRealmResults:Ljava/lang/ref/WeakReference;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1611
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/SharedGroup;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v3

    iput-object v3, v13, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    .line 1612
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery$3;->this$0:Lio/realm/RealmQuery;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery$3;->val$weakHandler:Ljava/lang/ref/WeakReference;

    const v5, 0x2547029

    move-object/from16 v0, v17

    # invokes: Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    invoke-static {v3, v0, v4, v5, v13}, Lio/realm/RealmQuery;->access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V

    .line 1615
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Lio/realm/internal/async/BadVersionException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 1627
    if-eqz v17, :cond_0

    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1628
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/SharedGroup;->close()V

    .line 1635
    .end local v10    # "columnIndex":J
    .end local v13    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    .end local v14    # "handoverTableViewPointer":J
    .end local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_0
    :goto_0
    return-object v3

    .line 1616
    .restart local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_0
    move-exception v2

    .line 1618
    .local v2, "e":Lio/realm/internal/async/BadVersionException;
    :goto_1
    :try_start_2
    const-string v3, "findAllSortedAsync handover could not complete due to a BadVersionException. Retry is scheduled by a REALM_CHANGED event."

    invoke-static {v3}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1627
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1628
    invoke-virtual/range {v16 .. v16}, Lio/realm/internal/SharedGroup;->close()V

    .line 1635
    .end local v2    # "e":Lio/realm/internal/async/BadVersionException;
    .end local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_1
    :goto_2
    # getter for: Lio/realm/RealmQuery;->INVALID_NATIVE_POINTER:Ljava/lang/Long;
    invoke-static {}, Lio/realm/RealmQuery;->access$200()Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1621
    .restart local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_1
    move-exception v2

    .line 1622
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lio/realm/internal/log/RealmLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1623
    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/RealmQuery$3;->this$0:Lio/realm/RealmQuery;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/RealmQuery$3;->val$weakHandler:Ljava/lang/ref/WeakReference;

    const v5, 0x6197ecb

    new-instance v6, Ljava/lang/Error;

    invoke-direct {v6, v2}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    move-object/from16 v0, v16

    # invokes: Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    invoke-static {v3, v0, v4, v5, v6}, Lio/realm/RealmQuery;->access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1627
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1628
    invoke-virtual/range {v16 .. v16}, Lio/realm/internal/SharedGroup;->close()V

    goto :goto_2

    .line 1627
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :goto_4
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1628
    invoke-virtual/range {v16 .. v16}, Lio/realm/internal/SharedGroup;->close()V

    :cond_2
    throw v3

    .line 1632
    .end local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lio/realm/RealmQuery$3;->val$handoverQueryPointer:J

    invoke-static {v4, v5}, Lio/realm/internal/TableQuery;->nativeCloseQueryHandover(J)V

    goto :goto_2

    .line 1627
    .restart local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catchall_1
    move-exception v3

    move-object/from16 v16, v17

    .end local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_4

    .line 1621
    .end local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_2
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_3

    .line 1616
    .end local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_3
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v16    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1591
    .local p0, "this":Lio/realm/RealmQuery$3;, "Lio/realm/RealmQuery$3;"
    invoke-virtual {p0}, Lio/realm/RealmQuery$3;->call()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
