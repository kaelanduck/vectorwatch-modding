.class abstract Lio/realm/BaseRealm;
.super Ljava/lang/Object;
.source "BaseRealm.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/BaseRealm$MigrationCallback;
    }
.end annotation


# static fields
.field private static final CANNOT_REFRESH_INSIDE_OF_TRANSACTION_MESSAGE:Ljava/lang/String; = "Cannot refresh inside of a transaction."

.field private static final CLOSED_REALM_MESSAGE:Ljava/lang/String; = "This Realm instance has already been closed, making it unusable."

.field private static final INCORRECT_THREAD_CLOSE_MESSAGE:Ljava/lang/String; = "Realm access from incorrect thread. Realm instance can only be closed on the thread it was created."

.field private static final INCORRECT_THREAD_MESSAGE:Ljava/lang/String; = "Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created."

.field protected static final UNVERSIONED:J = -0x1L

.field static final asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

.field protected static final handlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/Handler;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected configuration:Lio/realm/RealmConfiguration;

.field handler:Landroid/os/Handler;

.field handlerController:Lio/realm/HandlerController;

.field schema:Lio/realm/RealmSchema;

.field protected sharedGroupManager:Lio/realm/internal/SharedGroupManager;

.field final threadId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    .line 63
    invoke-static {}, Lio/realm/internal/async/RealmThreadPoolExecutor;->newDefaultExecutor()Lio/realm/internal/async/RealmThreadPoolExecutor;

    move-result-object v0

    sput-object v0, Lio/realm/BaseRealm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    .line 73
    new-instance v0, Lio/realm/internal/android/ReleaseAndroidLogger;

    invoke-direct {v0}, Lio/realm/internal/android/ReleaseAndroidLogger;-><init>()V

    invoke-static {v0}, Lio/realm/internal/log/RealmLog;->add(Lio/realm/internal/log/Logger;)V

    .line 74
    return-void
.end method

.method protected constructor <init>(Lio/realm/RealmConfiguration;Z)V
    .locals 2
    .param p1, "configuration"    # Lio/realm/RealmConfiguration;
    .param p2, "autoRefresh"    # Z

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/BaseRealm;->threadId:J

    .line 78
    iput-object p1, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    .line 79
    new-instance v0, Lio/realm/internal/SharedGroupManager;

    invoke-direct {v0, p1}, Lio/realm/internal/SharedGroupManager;-><init>(Lio/realm/RealmConfiguration;)V

    iput-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    .line 80
    new-instance v0, Lio/realm/RealmSchema;

    iget-object v1, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v1}, Lio/realm/internal/SharedGroupManager;->getTransaction()Lio/realm/internal/ImplicitTransaction;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lio/realm/RealmSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/ImplicitTransaction;)V

    iput-object v0, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    .line 81
    new-instance v0, Lio/realm/HandlerController;

    invoke-direct {v0, p0}, Lio/realm/HandlerController;-><init>(Lio/realm/BaseRealm;)V

    iput-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    .line 82
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 83
    if-eqz p2, :cond_1

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set auto-refresh in a Thread without a Looper"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    invoke-virtual {p0, p2}, Lio/realm/BaseRealm;->setAutoRefresh(Z)V

    .line 89
    :cond_1
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-static {p0, p1, p2}, Lio/realm/BaseRealm;->deletes(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static compactRealm(Lio/realm/RealmConfiguration;)Z
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 659
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 660
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot currently compact an encrypted Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 663
    :cond_0
    invoke-static {p0}, Lio/realm/internal/SharedGroupManager;->compact(Lio/realm/RealmConfiguration;)Z

    move-result v0

    return v0
.end method

.method static deleteRealm(Lio/realm/RealmConfiguration;)Z
    .locals 3
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 617
    const-string v0, ".management"

    .line 618
    .local v0, "management":Ljava/lang/String;
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 620
    .local v1, "realmDeleted":Ljava/util/concurrent/atomic/AtomicBoolean;
    new-instance v2, Lio/realm/BaseRealm$2;

    invoke-direct {v2, p0, v1}, Lio/realm/BaseRealm$2;-><init>(Lio/realm/RealmConfiguration;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-static {p0, v2}, Lio/realm/RealmCache;->invokeWithGlobalRefCount(Lio/realm/RealmConfiguration;Lio/realm/RealmCache$Callback;)V

    .line 648
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    return v2
.end method

.method private static deletes(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z
    .locals 10
    .param p0, "canonicalPath"    # Ljava/lang/String;
    .param p1, "rootFolder"    # Ljava/io/File;
    .param p2, "realmFileName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 591
    new-instance v3, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v3, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 593
    .local v3, "realmDeleted":Ljava/util/concurrent/atomic/AtomicBoolean;
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/io/File;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v5, v4, v9

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".lock"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v5, v4, v8

    const/4 v5, 0x2

    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".log_a"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x3

    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".log_b"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x4

    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".log"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x5

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v6, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 601
    .local v2, "filesToDelete":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 602
    .local v1, "fileToDelete":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 603
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 604
    .local v0, "deleteResult":Z
    if-nez v0, :cond_0

    .line 605
    invoke-virtual {v3, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 606
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not delete the file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lio/realm/internal/log/RealmLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 610
    .end local v0    # "deleteResult":Z
    .end local v1    # "fileToDelete":Ljava/io/File;
    :cond_1
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    return v4
.end method

.method static getHandlers()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Landroid/os/Handler;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    sget-object v0, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    return-object v0
.end method

.method protected static migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/RealmMigration;Lio/realm/BaseRealm$MigrationCallback;)V
    .locals 4
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "migration"    # Lio/realm/RealmMigration;
    .param p2, "callback"    # Lio/realm/BaseRealm$MigrationCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 676
    if-nez p0, :cond_0

    .line 677
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "RealmConfiguration must be provided"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 679
    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v1

    if-nez v1, :cond_1

    .line 680
    new-instance v1, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RealmMigration must be provided"

    invoke-direct {v1, v2, v3}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 683
    :cond_1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 685
    .local v0, "fileNotFound":Ljava/util/concurrent/atomic/AtomicBoolean;
    new-instance v1, Lio/realm/BaseRealm$3;

    invoke-direct {v1, p0, v0, p1, p2}, Lio/realm/BaseRealm$3;-><init>(Lio/realm/RealmConfiguration;Ljava/util/concurrent/atomic/AtomicBoolean;Lio/realm/RealmMigration;Lio/realm/BaseRealm$MigrationCallback;)V

    invoke-static {p0, v1}, Lio/realm/RealmCache;->invokeWithGlobalRefCount(Lio/realm/RealmConfiguration;Lio/realm/RealmCache$Callback;)V

    .line 722
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 723
    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot migrate a Realm file which doesn\'t exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 724
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 726
    :cond_2
    return-void
.end method


# virtual methods
.method protected addListener(Lio/realm/RealmChangeListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    if-nez p1, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 141
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->isAutoRefreshEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can\'t register a listener from a non-Looper thread "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0, p1}, Lio/realm/HandlerController;->addChangeListener(Lio/realm/RealmChangeListener;)V

    .line 145
    return-void
.end method

.method public abstract asObservable()Lrx/Observable;
.end method

.method public beginTransaction()V
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 317
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->promoteToWrite()V

    .line 318
    return-void
.end method

.method public cancelTransaction()V
    .locals 1

    .prologue
    .line 380
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 381
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->rollbackAndContinueAsRead()V

    .line 382
    return-void
.end method

.method protected checkAllObjectsSortedParameters([Ljava/lang/String;[Lio/realm/Sort;)V
    .locals 2
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 518
    if-nez p1, :cond_0

    .line 519
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fieldNames must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_0
    if-nez p2, :cond_1

    .line 521
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sortOrders must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 523
    :cond_1
    return-void
.end method

.method protected checkIfValid()V
    .locals 4

    .prologue
    .line 389
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 390
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This Realm instance has already been closed, making it unusable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 394
    :cond_1
    iget-wide v0, p0, Lio/realm/BaseRealm;->threadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 395
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_2
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    .line 441
    iget-wide v0, p0, Lio/realm/BaseRealm;->threadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 442
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm access from incorrect thread. Realm instance can only be closed on the thread it was created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 445
    :cond_0
    invoke-static {p0}, Lio/realm/RealmCache;->release(Lio/realm/BaseRealm;)V

    .line 446
    return-void
.end method

.method public commitTransaction()V
    .locals 2

    .prologue
    .line 327
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lio/realm/BaseRealm;->commitTransaction(ZLjava/lang/Runnable;)V

    .line 328
    return-void
.end method

.method commitTransaction(ZLjava/lang/Runnable;)V
    .locals 6
    .param p1, "notifyLocalThread"    # Z
    .param p2, "runAfterCommit"    # Ljava/lang/Runnable;

    .prologue
    const v5, 0xe3d1b0

    .line 339
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 340
    iget-object v3, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v3}, Lio/realm/internal/SharedGroupManager;->commitAndContinueAsRead()V

    .line 342
    if-eqz p2, :cond_0

    .line 343
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 346
    :cond_0
    sget-object v3, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 347
    .local v1, "handlerIntegerEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/os/Handler;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 348
    .local v0, "handler":Landroid/os/Handler;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 352
    .local v2, "realmPath":Ljava/lang/String;
    if-nez p1, :cond_2

    iget-object v4, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 360
    :cond_2
    iget-object v4, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    .line 361
    invoke-virtual {v4}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 362
    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 363
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 364
    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 365
    const-string v4, "Cannot update Looper threads when the Looper has quit. Use realm.setAutoRefresh(false) to prevent this."

    invoke-static {v4}, Lio/realm/internal/log/RealmLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 369
    .end local v0    # "handler":Landroid/os/Handler;
    .end local v1    # "handlerIntegerEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/os/Handler;Ljava/lang/String;>;"
    .end local v2    # "realmPath":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public deleteAll()V
    .locals 4

    .prologue
    .line 584
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 585
    iget-object v1, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v1}, Lio/realm/RealmSchema;->getAll()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmObjectSchema;

    .line 586
    .local v0, "objectSchema":Lio/realm/RealmObjectSchema;
    iget-object v2, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0}, Lio/realm/RealmObjectSchema;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/realm/RealmSchema;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/internal/Table;->clear()V

    goto :goto_0

    .line 588
    .end local v0    # "objectSchema":Lio/realm/RealmObjectSchema;
    :cond_0
    return-void
.end method

.method doClose()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->close()V

    .line 454
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    .line 456
    :cond_0
    iget-object v0, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 457
    invoke-virtual {p0}, Lio/realm/BaseRealm;->removeHandler()V

    .line 459
    :cond_1
    return-void
.end method

.method protected doMultiFieldSort([Ljava/lang/String;[Lio/realm/Sort;Lio/realm/internal/Table;)Lio/realm/internal/TableView;
    .locals 9
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .param p3, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 504
    array-length v5, p1

    new-array v2, v5, [J

    .line 505
    .local v2, "columnIndices":[J
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, p1

    if-ge v4, v5, :cond_1

    .line 506
    aget-object v3, p1, v4

    .line 507
    .local v3, "fieldName":Ljava/lang/String;
    invoke-virtual {p3, v3}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 508
    .local v0, "columnIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v0, v6

    if-nez v5, :cond_0

    .line 509
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Field name \'%s\' does not exist."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 511
    :cond_0
    aput-wide v0, v2, v4

    .line 505
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 514
    .end local v0    # "columnIndex":J
    .end local v3    # "fieldName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p3, v2, p2}, Lio/realm/internal/Table;->getSortedView([J[Lio/realm/Sort;)Lio/realm/internal/TableView;

    move-result-object v5

    return-object v5
.end method

.method get(Ljava/lang/Class;J)Lio/realm/RealmModel;
    .locals 6
    .param p2, "rowIndex"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;J)TE;"
        }
    .end annotation

    .prologue
    .line 540
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    iget-object v4, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v4, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 541
    .local v3, "table":Lio/realm/internal/Table;
    invoke-virtual {v3, p2, p3}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v2

    .line 542
    .local v2, "row":Lio/realm/internal/UncheckedRow;
    iget-object v4, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v4}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v4

    iget-object v5, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v5, p1}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lio/realm/internal/RealmProxyMediator;->newInstance(Ljava/lang/Class;Lio/realm/internal/ColumnInfo;)Lio/realm/RealmModel;

    move-result-object v1

    .local v1, "result":Lio/realm/RealmModel;, "TE;"
    move-object v0, v1

    .line 543
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 544
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 545
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 546
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->setTableVersion$realm()V

    .line 548
    return-object v1
.end method

.method get(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/RealmModel;
    .locals 7
    .param p2, "dynamicClassName"    # Ljava/lang/String;
    .param p3, "rowIndex"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            "J)TE;"
        }
    .end annotation

    .prologue
    .line 556
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p2, :cond_0

    .line 557
    iget-object v4, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v4, p2}, Lio/realm/RealmSchema;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    .line 559
    .local v3, "table":Lio/realm/internal/Table;
    new-instance v0, Lio/realm/DynamicRealmObject;

    invoke-direct {v0}, Lio/realm/DynamicRealmObject;-><init>()V

    .line 560
    .local v0, "dynamicObj":Lio/realm/RealmModel;, "TE;"
    move-object v2, v0

    .end local v0    # "dynamicObj":Lio/realm/RealmModel;, "TE;"
    .local v2, "result":Lio/realm/RealmModel;, "TE;"
    :goto_0
    move-object v1, v2

    .line 566
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 567
    .local v1, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 568
    const-wide/16 v4, -0x1

    cmp-long v4, p3, v4

    if-eqz v4, :cond_1

    .line 569
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v3, p3, p4}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 570
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->setTableVersion$realm()V

    .line 575
    :goto_1
    return-object v2

    .line 562
    .end local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local v2    # "result":Lio/realm/RealmModel;, "TE;"
    .end local v3    # "table":Lio/realm/internal/Table;
    :cond_0
    iget-object v4, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v4, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 563
    .restart local v3    # "table":Lio/realm/internal/Table;
    iget-object v4, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v4}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v4

    iget-object v5, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v5, p1}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lio/realm/internal/RealmProxyMediator;->newInstance(Ljava/lang/Class;Lio/realm/internal/ColumnInfo;)Lio/realm/RealmModel;

    move-result-object v2

    .restart local v2    # "result":Lio/realm/RealmModel;, "TE;"
    goto :goto_0

    .line 572
    .restart local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_1
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    sget-object v5, Lio/realm/internal/InvalidRow;->INSTANCE:Lio/realm/internal/InvalidRow;

    invoke-virtual {v4, v5}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    goto :goto_1
.end method

.method public getConfiguration()Lio/realm/RealmConfiguration;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSchema()Lio/realm/RealmSchema;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    return-object v0
.end method

.method public getVersion()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 424
    iget-object v1, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    const-string v2, "metadata"

    invoke-virtual {v1, v2}, Lio/realm/internal/SharedGroupManager;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 425
    const-wide/16 v2, -0x1

    .line 428
    :goto_0
    return-wide v2

    .line 427
    :cond_0
    iget-object v1, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    const-string v2, "metadata"

    invoke-virtual {v1, v2}, Lio/realm/internal/SharedGroupManager;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 428
    .local v0, "metadataTable":Lio/realm/internal/Table;
    invoke-virtual {v0, v4, v5, v4, v5}, Lio/realm/internal/Table;->getLong(JJ)J

    move-result-wide v2

    goto :goto_0
.end method

.method hasChanged()Z
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->hasChanged()Z

    move-result v0

    return v0
.end method

.method public isAutoRefresh()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->isAutoRefreshEnabled()Z

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 4

    .prologue
    .line 468
    iget-wide v0, p0, Lio/realm/BaseRealm;->threadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 469
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_0
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->isOpen()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 482
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->getTransaction()Lio/realm/internal/ImplicitTransaction;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/ImplicitTransaction;->isObjectTablesEmpty()Z

    move-result v0

    return v0
.end method

.method public isInTransaction()Z
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 133
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0}, Lio/realm/internal/SharedGroupManager;->isImmutable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAllChangeListeners()V
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 194
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->isAutoRefreshEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can\'t remove listeners from a non-Looper thread "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->removeAllChangeListeners()V

    .line 198
    return-void
.end method

.method public removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<+",
            "Lio/realm/BaseRealm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<+Lio/realm/BaseRealm;>;"
    if-nez p1, :cond_0

    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 160
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->isAutoRefreshEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can\'t remove a listener from a non-Looper thread "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_1
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0, p1}, Lio/realm/HandlerController;->removeChangeListener(Lio/realm/RealmChangeListener;)V

    .line 164
    return-void
.end method

.method protected removeHandler()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 213
    sget-object v0, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    iget-object v1, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 216
    iput-object v2, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    .line 217
    return-void
.end method

.method public setAutoRefresh(Z)V
    .locals 3
    .param p1, "autoRefresh"    # Z

    .prologue
    .line 103
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 104
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set auto-refresh in a Thread without a Looper"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->isAutoRefreshEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 109
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    .line 110
    sget-object v0, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    iget-object v1, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_1
    :goto_0
    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0, p1}, Lio/realm/HandlerController;->setAutoRefresh(Z)V

    .line 115
    return-void

    .line 111
    :cond_2
    if-nez p1, :cond_1

    iget-object v0, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v0}, Lio/realm/HandlerController;->isAutoRefreshEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {p0}, Lio/realm/BaseRealm;->removeHandler()V

    goto :goto_0
.end method

.method setHandler(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 204
    sget-object v0, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    iget-object v1, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lio/realm/BaseRealm;->handlers:Ljava/util/Map;

    iget-object v1, p0, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iput-object p1, p0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    .line 207
    return-void
.end method

.method setVersion(J)V
    .locals 9
    .param p1, "version"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 491
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    const-string v4, "metadata"

    invoke-virtual {v0, v4}, Lio/realm/internal/SharedGroupManager;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 492
    .local v1, "metadataTable":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_0

    .line 493
    sget-object v0, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v4, "version"

    invoke-virtual {v1, v0, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    .line 494
    invoke-virtual {v1}, Lio/realm/internal/Table;->addEmptyRow()J

    :cond_0
    move-wide v4, v2

    move-wide v6, p1

    .line 496
    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->setLong(JJJ)V

    .line 497
    return-void
.end method

.method public stopWaitForChange()V
    .locals 1

    .prologue
    .line 292
    new-instance v0, Lio/realm/BaseRealm$1;

    invoke-direct {v0, p0}, Lio/realm/BaseRealm$1;-><init>(Lio/realm/BaseRealm;)V

    invoke-static {v0}, Lio/realm/RealmCache;->invokeWithLock(Lio/realm/RealmCache$Callback0;)V

    .line 302
    return-void
.end method

.method public waitForChange()Z
    .locals 3

    .prologue
    .line 266
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 267
    invoke-virtual {p0}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot wait for changes inside of a transaction."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 270
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 271
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot wait for changes inside a Looper thread. Use RealmChangeListeners instead."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 273
    :cond_1
    iget-object v1, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v1}, Lio/realm/internal/SharedGroupManager;->getSharedGroup()Lio/realm/internal/SharedGroup;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/internal/SharedGroup;->waitForChange()Z

    move-result v0

    .line 274
    .local v0, "hasChanged":Z
    if-eqz v0, :cond_2

    .line 276
    iget-object v1, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v1}, Lio/realm/internal/SharedGroupManager;->advanceRead()V

    .line 277
    iget-object v1, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v1}, Lio/realm/HandlerController;->refreshSynchronousTableViews()V

    .line 279
    :cond_2
    return v0
.end method

.method public writeCopyTo(Ljava/io/File;)V
    .locals 1
    .param p1, "destination"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lio/realm/BaseRealm;->writeEncryptedCopyTo(Ljava/io/File;[B)V

    .line 232
    return-void
.end method

.method public writeEncryptedCopyTo(Ljava/io/File;[B)V
    .locals 2
    .param p1, "destination"    # Ljava/io/File;
    .param p2, "key"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    if-nez p1, :cond_0

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The destination argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_0
    invoke-virtual {p0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 253
    iget-object v0, p0, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v0, p1, p2}, Lio/realm/internal/SharedGroupManager;->copyToFile(Ljava/io/File;[B)V

    .line 254
    return-void
.end method
