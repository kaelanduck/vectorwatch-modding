.class public final Lio/realm/RealmList;
.super Ljava/util/AbstractList;
.source "RealmList.java"

# interfaces
.implements Lio/realm/OrderedRealmCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RealmList$RealmListItr;,
        Lio/realm/RealmList$RealmItr;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "Lio/realm/OrderedRealmCollection",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final NULL_OBJECTS_NOT_ALLOWED_MESSAGE:Ljava/lang/String; = "RealmList does not accept null values"

.field private static final ONLY_IN_MANAGED_MODE_MESSAGE:Ljava/lang/String; = "This method is only available in managed mode"

.field public static final REMOVE_OUTSIDE_TRANSACTION_ERROR:Ljava/lang/String; = "Objects can only be removed from inside a write transaction"


# instance fields
.field protected className:Ljava/lang/String;

.field protected clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final managedMode:Z

.field protected realm:Lio/realm/BaseRealm;

.field private unmanagedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected view:Lio/realm/internal/LinkView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    .line 75
    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/BaseRealm;)V
    .locals 1
    .param p2, "linkView"    # Lio/realm/internal/LinkView;
    .param p3, "realm"    # Lio/realm/BaseRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/internal/LinkView;",
            "Lio/realm/BaseRealm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    .line 104
    iput-object p1, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    .line 105
    iput-object p2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    .line 106
    iput-object p3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    .line 107
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lio/realm/internal/LinkView;Lio/realm/BaseRealm;)V
    .locals 1
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "linkView"    # Lio/realm/internal/LinkView;
    .param p3, "realm"    # Lio/realm/BaseRealm;

    .prologue
    .line 109
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    .line 111
    iput-object p2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    .line 112
    iput-object p3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    .line 113
    iput-object p1, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public varargs constructor <init>([Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "objects":[Lio/realm/RealmModel;, "[TE;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 87
    if-nez p1, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The objects argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    .line 92
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method static synthetic access$100(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 52
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$200(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 52
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$300(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 52
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$400(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 52
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$500(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 52
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method private checkIndex(I)V
    .locals 4
    .param p1, "location"    # I

    .prologue
    .line 738
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    .line 739
    .local v0, "size":I
    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 740
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 742
    :cond_1
    return-void
.end method

.method private checkValidObject(Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 732
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    if-nez p1, :cond_0

    .line 733
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RealmList does not accept null values"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 735
    :cond_0
    return-void
.end method

.method private checkValidView()V
    .locals 2

    .prologue
    .line 745
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 746
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_1

    .line 747
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm instance has been closed or this object or its parent has been deleted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 749
    :cond_1
    return-void
.end method

.method private copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 234
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    instance-of v4, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v4, :cond_5

    move-object v2, p1

    .line 235
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    .line 237
    .local v2, "proxy":Lio/realm/internal/RealmObjectProxy;
    instance-of v4, v2, Lio/realm/DynamicRealmObject;

    if-eqz v4, :cond_4

    .line 238
    iget-object v4, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v4}, Lio/realm/internal/LinkView;->getTargetTable()Lio/realm/internal/Table;

    move-result-object v4

    invoke-static {v4}, Lio/realm/RealmSchema;->getSchemaForTable(Lio/realm/internal/Table;)Ljava/lang/String;

    move-result-object v0

    .local v0, "listClassName":Ljava/lang/String;
    move-object v4, p1

    .line 239
    check-cast v4, Lio/realm/DynamicRealmObject;

    invoke-virtual {v4}, Lio/realm/DynamicRealmObject;->getType()Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "objectClassName":Ljava/lang/String;
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    iget-object v5, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    if-ne v4, v5, :cond_2

    .line 241
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 273
    .end local v0    # "listClassName":Ljava/lang/String;
    .end local v1    # "objectClassName":Ljava/lang/String;
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "object":Lio/realm/RealmModel;, "TE;"
    :cond_0
    :goto_0
    return-object p1

    .line 246
    .restart local v0    # "listClassName":Ljava/lang/String;
    .restart local v1    # "objectClassName":Ljava/lang/String;
    .restart local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "object":Lio/realm/RealmModel;, "TE;"
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "The object has a different type from list\'s. Type of the list is \'%s\', type of object is \'%s\'."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 249
    :cond_2
    iget-object v4, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-wide v4, v4, Lio/realm/BaseRealm;->threadId:J

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    iget-wide v6, v6, Lio/realm/BaseRealm;->threadId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 253
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Cannot copy DynamicRealmObject between Realm instances."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 255
    :cond_3
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Cannot copy an object to a Realm instance created in another thread."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 259
    .end local v0    # "listClassName":Ljava/lang/String;
    .end local v1    # "objectClassName":Ljava/lang/String;
    :cond_4
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 260
    iget-object v4, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eq v4, v5, :cond_0

    .line 261
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Cannot copy an object from another Realm instance."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 269
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_5
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    check-cast v3, Lio/realm/Realm;

    .line 270
    .local v3, "realm":Lio/realm/Realm;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 271
    invoke-virtual {v3, p1}, Lio/realm/Realm;->copyToRealmOrUpdate(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    goto/16 :goto_0

    .line 273
    :cond_6
    invoke-virtual {v3, p1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    goto/16 :goto_0
.end method

.method private isAttached()Z
    .locals 1

    .prologue
    .line 131
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(ILio/realm/RealmModel;)V
    .locals 6
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p2}, Lio/realm/RealmList;->checkValidObject(Lio/realm/RealmModel;)V

    .line 156
    iget-boolean v1, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v1, :cond_2

    .line 157
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 158
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v1

    if-le p1, v1, :cond_1

    .line 159
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 161
    :cond_1
    invoke-direct {p0, p2}, Lio/realm/RealmList;->copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 162
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lio/realm/internal/LinkView;->insert(JJ)V

    .line 166
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :goto_0
    iget v1, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/RealmList;->modCount:I

    .line 167
    return-void

    .line 164
    :cond_2
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 52
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/RealmList;->add(ILio/realm/RealmModel;)V

    return-void
.end method

.method public add(Lio/realm/RealmModel;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 187
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p1}, Lio/realm/RealmList;->checkValidObject(Lio/realm/RealmModel;)V

    .line 188
    iget-boolean v1, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v1, :cond_0

    .line 189
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 190
    invoke-direct {p0, p1}, Lio/realm/RealmList;->copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 191
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/LinkView;->add(J)V

    .line 195
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :goto_0
    iget v1, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/RealmList;->modCount:I

    .line 196
    const/4 v1, 0x1

    return v1

    .line 193
    :cond_0
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    check-cast p1, Lio/realm/RealmModel;

    invoke-virtual {p0, p1}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public average(Ljava/lang/String;)D
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 606
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 607
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->average(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0

    .line 609
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 313
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 315
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->clear()V

    .line 319
    :goto_0
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 320
    return-void

    .line 317
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 684
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    .line 685
    .local v0, "contains":Z
    iget-boolean v2, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v2, :cond_1

    .line 686
    iget-object v2, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 687
    instance-of v2, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 688
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 689
    .local v1, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    sget-object v3, Lio/realm/internal/InvalidRow;->INSTANCE:Lio/realm/internal/InvalidRow;

    if-eq v2, v3, :cond_0

    .line 690
    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v3

    invoke-interface {v3}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/LinkView;->contains(J)Z

    move-result v0

    .line 696
    .end local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_0
    :goto_0
    return v0

    .line 694
    :cond_1
    iget-object v2, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public deleteAllFromRealm()Z
    .locals 2

    .prologue
    .line 642
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_1

    .line 643
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 644
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 645
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->removeAllTargetRows()V

    .line 646
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 647
    const/4 v0, 0x1

    .line 649
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 652
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteFirstFromRealm()Z
    .locals 2

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    .line 398
    iget-boolean v1, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v1, :cond_1

    .line 399
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 400
    invoke-virtual {p0, v0}, Lio/realm/RealmList;->deleteFromRealm(I)V

    .line 401
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 402
    const/4 v0, 0x1

    .line 404
    :cond_0
    return v0

    .line 407
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteFromRealm(I)V
    .locals 2
    .param p1, "location"    # I

    .prologue
    .line 523
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 524
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 525
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0, p1}, Lio/realm/internal/LinkView;->removeTargetRow(I)V

    .line 526
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 530
    return-void

    .line 528
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteLastFromRealm()Z
    .locals 2

    .prologue
    .line 416
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_1

    .line 417
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 418
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lio/realm/RealmList;->deleteFromRealm(I)V

    .line 419
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 420
    const/4 v0, 0x1

    .line 422
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 425
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public first()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v1, 0x0

    .line 452
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 453
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 454
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 455
    invoke-virtual {p0, v1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 458
    :goto_0
    return-object v0

    .line 457
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 458
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 460
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "The list is empty."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(I)Lio/realm/RealmModel;
    .locals 6
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 439
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v2, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v2, :cond_0

    .line 440
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 441
    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/LinkView;->getTargetRowIndex(J)J

    move-result-wide v0

    .line 442
    .local v0, "rowIndex":J
    iget-object v2, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v3, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    iget-object v4, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/RealmModel;

    move-result-object v2

    .line 444
    .end local v0    # "rowIndex":J
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;

    goto :goto_0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 661
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 124
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    const/4 v0, 0x0

    .line 127
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lio/realm/RealmList;->isAttached()Z

    move-result v0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 704
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 705
    new-instance v0, Lio/realm/RealmList$RealmItr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/realm/RealmList$RealmItr;-><init>(Lio/realm/RealmList;Lio/realm/RealmList$1;)V

    .line 707
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public last()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 467
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 468
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 469
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 470
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->size()J

    move-result-wide v0

    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 473
    :goto_0
    return-object v0

    .line 472
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 473
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 475
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "The list is empty."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 716
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/realm/RealmList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 724
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 725
    new-instance v0, Lio/realm/RealmList$RealmListItr;

    invoke-direct {v0, p0, p1}, Lio/realm/RealmList$RealmListItr;-><init>(Lio/realm/RealmList;I)V

    .line 727
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public load()Z
    .locals 1

    .prologue
    .line 669
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public max(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 582
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 583
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->max(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0

    .line 585
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public maxDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 618
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 619
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->maximumDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0

    .line 621
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public min(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 570
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 571
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->min(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0

    .line 573
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public minDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 630
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 631
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->minimumDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0

    .line 633
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public move(II)V
    .locals 6
    .param p1, "oldPos"    # I
    .param p2, "newPos"    # I

    .prologue
    .line 288
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v1, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v1, :cond_0

    .line 289
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 290
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    int-to-long v4, p2

    invoke-virtual {v1, v2, v3, v4, v5}, Lio/realm/internal/LinkView;->move(JJ)V

    .line 301
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-direct {p0, p1}, Lio/realm/RealmList;->checkIndex(I)V

    .line 293
    invoke-direct {p0, p2}, Lio/realm/RealmList;->checkIndex(I)V

    .line 294
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 295
    .local v0, "object":Lio/realm/RealmModel;, "TE;"
    if-le p2, p1, :cond_1

    .line 296
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    add-int/lit8 v2, p2, -0x1

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 298
    :cond_1
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public remove(I)Lio/realm/RealmModel;
    .locals 4
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 333
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v1, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v1, :cond_0

    .line 334
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 335
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 336
    .local v0, "removedItem":Lio/realm/RealmModel;, "TE;"
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/LinkView;->remove(J)V

    .line 340
    :goto_0
    iget v1, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/RealmList;->modCount:I

    .line 341
    return-object v0

    .line 338
    .end local v0    # "removedItem":Lio/realm/RealmModel;, "TE;"
    :cond_0
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .restart local v0    # "removedItem":Lio/realm/RealmModel;, "TE;"
    goto :goto_0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->remove(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 363
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects can only be removed from inside a write transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 387
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects can only be removed from inside a write transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 390
    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public set(ILio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 8
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p2}, Lio/realm/RealmList;->checkValidObject(Lio/realm/RealmModel;)V

    .line 220
    iget-boolean v3, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v3, :cond_0

    .line 221
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 222
    invoke-direct {p0, p2}, Lio/realm/RealmList;->copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    .line 223
    .local v2, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 224
    .local v0, "oldObject":Lio/realm/RealmModel;, "TE;"
    iget-object v3, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v4, p1

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lio/realm/internal/LinkView;->set(JJ)V

    move-object v1, v0

    .line 229
    .end local v0    # "oldObject":Lio/realm/RealmModel;, "TE;"
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .local v1, "oldObject":Lio/realm/RealmModel;, "TE;"
    :goto_0
    return-object v1

    .line 227
    .end local v1    # "oldObject":Lio/realm/RealmModel;, "TE;"
    :cond_0
    iget-object v3, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v3, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .restart local v0    # "oldObject":Lio/realm/RealmModel;, "TE;"
    move-object v1, v0

    .line 229
    .end local v0    # "oldObject":Lio/realm/RealmModel;, "TE;"
    .restart local v1    # "oldObject":Lio/realm/RealmModel;, "TE;"
    goto :goto_0
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/RealmList;->set(ILio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 540
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v2, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v2, :cond_1

    .line 541
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 542
    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v2}, Lio/realm/internal/LinkView;->size()J

    move-result-wide v0

    .line 543
    .local v0, "size":J
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    long-to-int v2, v0

    .line 545
    .end local v0    # "size":J
    :goto_0
    return v2

    .line 543
    .restart local v0    # "size":J
    :cond_0
    const v2, 0x7fffffff

    goto :goto_0

    .line 545
    .end local v0    # "size":J
    :cond_1
    iget-object v2, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0
.end method

.method public sort(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 483
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    sget-object v0, Lio/realm/Sort;->ASCENDING:Lio/realm/Sort;

    invoke-virtual {p0, p1, v0}, Lio/realm/RealmList;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 491
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 492
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0

    .line 494
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 503
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/RealmList;->sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 2
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 511
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->findAllSorted([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0

    .line 514
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sum(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 594
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->sum(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0

    .line 597
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 753
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 754
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-boolean v2, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 755
    const-string v2, "@["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    iget-boolean v2, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lio/realm/RealmList;->isAttached()Z

    move-result v2

    if-nez v2, :cond_2

    .line 757
    const-string v2, "invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 770
    :cond_0
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 771
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 754
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 759
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 760
    iget-boolean v2, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v2, :cond_4

    .line 761
    invoke-virtual {p0, v0}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 765
    :goto_2
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 766
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 759
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 763
    :cond_4
    invoke-virtual {p0, v0}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public where()Lio/realm/RealmQuery;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 557
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-boolean v0, p0, Lio/realm/RealmList;->managedMode:Z

    if-eqz v0, :cond_0

    .line 558
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 559
    invoke-static {p0}, Lio/realm/RealmQuery;->createQueryFromList(Lio/realm/RealmList;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0

    .line 561
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
