.class Lio/realm/RealmQuery$5;
.super Ljava/lang/Object;
.source "RealmQuery.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/RealmQuery;->findFirstAsync()Lio/realm/RealmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/RealmQuery;

.field final synthetic val$handoverQueryPointer:J

.field final synthetic val$realmConfiguration:Lio/realm/RealmConfiguration;

.field final synthetic val$realmObjectWeakReference:Ljava/lang/ref/WeakReference;

.field final synthetic val$weakHandler:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;JLjava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 1
    .param p1, "this$0"    # Lio/realm/RealmQuery;

    .prologue
    .line 1906
    .local p0, "this":Lio/realm/RealmQuery$5;, "Lio/realm/RealmQuery$5;"
    iput-object p1, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    iput-object p2, p0, Lio/realm/RealmQuery$5;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    iput-wide p3, p0, Lio/realm/RealmQuery$5;->val$handoverQueryPointer:J

    iput-object p5, p0, Lio/realm/RealmQuery$5;->val$realmObjectWeakReference:Ljava/lang/ref/WeakReference;

    iput-object p6, p0, Lio/realm/RealmQuery$5;->val$weakHandler:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Long;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1909
    .local p0, "this":Lio/realm/RealmQuery$5;, "Lio/realm/RealmQuery$5;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1910
    const/4 v11, 0x0

    .line 1913
    .local v11, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_0
    new-instance v12, Lio/realm/internal/SharedGroup;

    iget-object v1, p0, Lio/realm/RealmQuery$5;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lio/realm/RealmQuery$5;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    .line 1915
    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getDurability()Lio/realm/internal/SharedGroup$Durability;

    move-result-object v3

    iget-object v4, p0, Lio/realm/RealmQuery$5;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    .line 1916
    invoke-virtual {v4}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v4

    invoke-direct {v12, v1, v2, v3, v4}, Lio/realm/internal/SharedGroup;-><init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1918
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .local v12, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_1
    iget-object v1, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    # getter for: Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;
    invoke-static {v1}, Lio/realm/RealmQuery;->access$000(Lio/realm/RealmQuery;)Lio/realm/internal/TableQuery;

    move-result-object v1

    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->getNativePointer()J

    move-result-wide v2

    .line 1919
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->getNativeReplicationPointer()J

    move-result-wide v4

    iget-wide v6, p0, Lio/realm/RealmQuery$5;->val$handoverQueryPointer:J

    .line 1918
    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/TableQuery;->findWithHandover(JJJ)J

    move-result-wide v8

    .line 1920
    .local v8, "handoverRowPointer":J
    const-wide/16 v2, 0x0

    cmp-long v1, v8, v2

    if-nez v1, :cond_0

    .line 1921
    iget-object v1, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    # getter for: Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;
    invoke-static {v1}, Lio/realm/RealmQuery;->access$400(Lio/realm/RealmQuery;)Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v1, v1, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    iget-object v2, p0, Lio/realm/RealmQuery$5;->val$realmObjectWeakReference:Ljava/lang/ref/WeakReference;

    iget-object v3, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    invoke-virtual {v1, v2, v3}, Lio/realm/HandlerController;->addToEmptyAsyncRealmObject(Ljava/lang/ref/WeakReference;Lio/realm/RealmQuery;)V

    .line 1922
    iget-object v1, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    # getter for: Lio/realm/RealmQuery;->realm:Lio/realm/BaseRealm;
    invoke-static {v1}, Lio/realm/RealmQuery;->access$400(Lio/realm/RealmQuery;)Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v1, v1, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    iget-object v2, p0, Lio/realm/RealmQuery$5;->val$realmObjectWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1, v2}, Lio/realm/HandlerController;->removeFromAsyncRealmObject(Ljava/lang/ref/WeakReference;)V

    .line 1925
    :cond_0
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask$Result;->newRealmObjectResponse()Lio/realm/internal/async/QueryUpdateTask$Result;

    move-result-object v10

    .line 1926
    .local v10, "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    iget-object v1, v10, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedRow:Ljava/util/IdentityHashMap;

    iget-object v2, p0, Lio/realm/RealmQuery$5;->val$realmObjectWeakReference:Ljava/lang/ref/WeakReference;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1927
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v1

    iput-object v1, v10, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    .line 1928
    iget-object v1, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    iget-object v2, p0, Lio/realm/RealmQuery$5;->val$weakHandler:Ljava/lang/ref/WeakReference;

    const v3, 0x3c50ea2

    # invokes: Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    invoke-static {v1, v12, v2, v3, v10}, Lio/realm/RealmQuery;->access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V

    .line 1931
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1940
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1941
    invoke-virtual {v12}, Lio/realm/internal/SharedGroup;->close()V

    .line 1948
    .end local v8    # "handoverRowPointer":J
    .end local v10    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_1
    :goto_0
    return-object v1

    .line 1933
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_0
    move-exception v0

    .line 1934
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lio/realm/internal/log/RealmLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1936
    iget-object v1, p0, Lio/realm/RealmQuery$5;->this$0:Lio/realm/RealmQuery;

    iget-object v2, p0, Lio/realm/RealmQuery$5;->val$weakHandler:Ljava/lang/ref/WeakReference;

    const v3, 0x6197ecb

    new-instance v4, Ljava/lang/Error;

    invoke-direct {v4, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    # invokes: Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    invoke-static {v1, v11, v2, v3, v4}, Lio/realm/RealmQuery;->access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1940
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1941
    invoke-virtual {v11}, Lio/realm/internal/SharedGroup;->close()V

    .line 1948
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_2
    :goto_2
    # getter for: Lio/realm/RealmQuery;->INVALID_NATIVE_POINTER:Ljava/lang/Long;
    invoke-static {}, Lio/realm/RealmQuery;->access$200()Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1940
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catchall_0
    move-exception v1

    :goto_3
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1941
    invoke-virtual {v11}, Lio/realm/internal/SharedGroup;->close()V

    :cond_3
    throw v1

    .line 1945
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_4
    iget-wide v2, p0, Lio/realm/RealmQuery$5;->val$handoverQueryPointer:J

    invoke-static {v2, v3}, Lio/realm/internal/TableQuery;->nativeCloseQueryHandover(J)V

    goto :goto_2

    .line 1940
    .restart local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catchall_1
    move-exception v1

    move-object v11, v12

    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_3

    .line 1933
    .end local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_1
    move-exception v0

    move-object v11, v12

    .end local v12    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v11    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1906
    .local p0, "this":Lio/realm/RealmQuery$5;, "Lio/realm/RealmQuery$5;"
    invoke-virtual {p0}, Lio/realm/RealmQuery$5;->call()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
