.class public final Lio/realm/RealmAsyncTask;
.super Ljava/lang/Object;
.source "RealmAsyncTask.java"


# instance fields
.field private volatile isCancelled:Z

.field private final pendingQuery:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Future;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/RealmAsyncTask;->isCancelled:Z

    .line 33
    iput-object p1, p0, Lio/realm/RealmAsyncTask;->pendingQuery:Ljava/util/concurrent/Future;

    .line 34
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 40
    iget-object v0, p0, Lio/realm/RealmAsyncTask;->pendingQuery:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 41
    iput-boolean v1, p0, Lio/realm/RealmAsyncTask;->isCancelled:Z

    .line 52
    sget-object v0, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    invoke-virtual {v0}, Lio/realm/internal/async/RealmThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RealmAsyncTask;->pendingQuery:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lio/realm/RealmAsyncTask;->isCancelled:Z

    return v0
.end method
