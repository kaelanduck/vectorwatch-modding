.class Lio/realm/RealmQuery$4;
.super Ljava/lang/Object;
.source "RealmQuery.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/RealmQuery;->findAllSortedAsync([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/RealmQuery;

.field final synthetic val$handoverQueryPointer:J

.field final synthetic val$indices:[J

.field final synthetic val$realmConfiguration:Lio/realm/RealmConfiguration;

.field final synthetic val$sortOrders:[Lio/realm/Sort;

.field final synthetic val$weakHandler:Ljava/lang/ref/WeakReference;

.field final synthetic val$weakRealmResults:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lio/realm/RealmQuery;Lio/realm/RealmConfiguration;J[J[Lio/realm/Sort;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 1
    .param p1, "this$0"    # Lio/realm/RealmQuery;

    .prologue
    .line 1766
    .local p0, "this":Lio/realm/RealmQuery$4;, "Lio/realm/RealmQuery$4;"
    iput-object p1, p0, Lio/realm/RealmQuery$4;->this$0:Lio/realm/RealmQuery;

    iput-object p2, p0, Lio/realm/RealmQuery$4;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    iput-wide p3, p0, Lio/realm/RealmQuery$4;->val$handoverQueryPointer:J

    iput-object p5, p0, Lio/realm/RealmQuery$4;->val$indices:[J

    iput-object p6, p0, Lio/realm/RealmQuery$4;->val$sortOrders:[Lio/realm/Sort;

    iput-object p7, p0, Lio/realm/RealmQuery$4;->val$weakRealmResults:Ljava/lang/ref/WeakReference;

    iput-object p8, p0, Lio/realm/RealmQuery$4;->val$weakHandler:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Long;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1769
    .local p0, "this":Lio/realm/RealmQuery$4;, "Lio/realm/RealmQuery$4;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1770
    const/4 v13, 0x0

    .line 1773
    .local v13, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_0
    new-instance v14, Lio/realm/internal/SharedGroup;

    iget-object v1, p0, Lio/realm/RealmQuery$4;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lio/realm/RealmQuery$4;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    .line 1775
    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getDurability()Lio/realm/internal/SharedGroup$Durability;

    move-result-object v3

    iget-object v4, p0, Lio/realm/RealmQuery$4;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    .line 1776
    invoke-virtual {v4}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v4

    invoke-direct {v14, v1, v2, v3, v4}, Lio/realm/internal/SharedGroup;-><init>(Ljava/lang/String;ZLio/realm/internal/SharedGroup$Durability;[B)V
    :try_end_0
    .catch Lio/realm/internal/async/BadVersionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1779
    .end local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .local v14, "sharedGroup":Lio/realm/internal/SharedGroup;
    :try_start_1
    iget-object v1, p0, Lio/realm/RealmQuery$4;->this$0:Lio/realm/RealmQuery;

    # getter for: Lio/realm/RealmQuery;->query:Lio/realm/internal/TableQuery;
    invoke-static {v1}, Lio/realm/RealmQuery;->access$000(Lio/realm/RealmQuery;)Lio/realm/internal/TableQuery;

    move-result-object v1

    invoke-virtual {v14}, Lio/realm/internal/SharedGroup;->getNativePointer()J

    move-result-wide v2

    .line 1780
    invoke-virtual {v14}, Lio/realm/internal/SharedGroup;->getNativeReplicationPointer()J

    move-result-wide v4

    iget-wide v6, p0, Lio/realm/RealmQuery$4;->val$handoverQueryPointer:J

    iget-object v8, p0, Lio/realm/RealmQuery$4;->val$indices:[J

    iget-object v9, p0, Lio/realm/RealmQuery$4;->val$sortOrders:[Lio/realm/Sort;

    .line 1779
    invoke-virtual/range {v1 .. v9}, Lio/realm/internal/TableQuery;->findAllMultiSortedWithHandover(JJJ[J[Lio/realm/Sort;)J

    move-result-wide v10

    .line 1782
    .local v10, "handoverTableViewPointer":J
    invoke-static {}, Lio/realm/internal/async/QueryUpdateTask$Result;->newRealmResultsResponse()Lio/realm/internal/async/QueryUpdateTask$Result;

    move-result-object v12

    .line 1783
    .local v12, "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    iget-object v1, v12, Lio/realm/internal/async/QueryUpdateTask$Result;->updatedTableViews:Ljava/util/IdentityHashMap;

    iget-object v2, p0, Lio/realm/RealmQuery$4;->val$weakRealmResults:Ljava/lang/ref/WeakReference;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1784
    invoke-virtual {v14}, Lio/realm/internal/SharedGroup;->getVersion()Lio/realm/internal/SharedGroup$VersionID;

    move-result-object v1

    iput-object v1, v12, Lio/realm/internal/async/QueryUpdateTask$Result;->versionID:Lio/realm/internal/SharedGroup$VersionID;

    .line 1785
    iget-object v1, p0, Lio/realm/RealmQuery$4;->this$0:Lio/realm/RealmQuery;

    iget-object v2, p0, Lio/realm/RealmQuery$4;->val$weakHandler:Ljava/lang/ref/WeakReference;

    const v3, 0x2547029

    # invokes: Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    invoke-static {v1, v14, v2, v3, v12}, Lio/realm/RealmQuery;->access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V

    .line 1788
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Lio/realm/internal/async/BadVersionException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1800
    if-eqz v14, :cond_0

    invoke-virtual {v14}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1801
    invoke-virtual {v14}, Lio/realm/internal/SharedGroup;->close()V

    .line 1808
    .end local v10    # "handoverTableViewPointer":J
    .end local v12    # "result":Lio/realm/internal/async/QueryUpdateTask$Result;
    .end local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_0
    :goto_0
    return-object v1

    .line 1789
    .restart local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_0
    move-exception v0

    .line 1791
    .local v0, "e":Lio/realm/internal/async/BadVersionException;
    :goto_1
    :try_start_2
    const-string v1, "findAllSortedAsync handover could not complete due to a BadVersionException. Retry is scheduled by a REALM_CHANGED event."

    invoke-static {v1}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1800
    if-eqz v13, :cond_1

    invoke-virtual {v13}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1801
    invoke-virtual {v13}, Lio/realm/internal/SharedGroup;->close()V

    .line 1808
    .end local v0    # "e":Lio/realm/internal/async/BadVersionException;
    .end local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_1
    :goto_2
    # getter for: Lio/realm/RealmQuery;->INVALID_NATIVE_POINTER:Ljava/lang/Long;
    invoke-static {}, Lio/realm/RealmQuery;->access$200()Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1794
    .restart local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_1
    move-exception v0

    .line 1795
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lio/realm/internal/log/RealmLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1796
    iget-object v1, p0, Lio/realm/RealmQuery$4;->this$0:Lio/realm/RealmQuery;

    iget-object v2, p0, Lio/realm/RealmQuery$4;->val$weakHandler:Ljava/lang/ref/WeakReference;

    const v3, 0x6197ecb

    new-instance v4, Ljava/lang/Error;

    invoke-direct {v4, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    # invokes: Lio/realm/RealmQuery;->closeSharedGroupAndSendMessageToHandler(Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    invoke-static {v1, v13, v2, v3, v4}, Lio/realm/RealmQuery;->access$100(Lio/realm/RealmQuery;Lio/realm/internal/SharedGroup;Ljava/lang/ref/WeakReference;ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1800
    if-eqz v13, :cond_1

    invoke-virtual {v13}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1801
    invoke-virtual {v13}, Lio/realm/internal/SharedGroup;->close()V

    goto :goto_2

    .line 1800
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :goto_4
    if-eqz v13, :cond_2

    invoke-virtual {v13}, Lio/realm/internal/SharedGroup;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1801
    invoke-virtual {v13}, Lio/realm/internal/SharedGroup;->close()V

    :cond_2
    throw v1

    .line 1805
    .end local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :cond_3
    iget-wide v2, p0, Lio/realm/RealmQuery$4;->val$handoverQueryPointer:J

    invoke-static {v2, v3}, Lio/realm/internal/TableQuery;->nativeCloseQueryHandover(J)V

    goto :goto_2

    .line 1800
    .restart local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catchall_1
    move-exception v1

    move-object v13, v14

    .end local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_4

    .line 1794
    .end local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_2
    move-exception v0

    move-object v13, v14

    .end local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_3

    .line 1789
    .end local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    :catch_3
    move-exception v0

    move-object v13, v14

    .end local v14    # "sharedGroup":Lio/realm/internal/SharedGroup;
    .restart local v13    # "sharedGroup":Lio/realm/internal/SharedGroup;
    goto :goto_1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1766
    .local p0, "this":Lio/realm/RealmQuery$4;, "Lio/realm/RealmQuery$4;"
    invoke-virtual {p0}, Lio/realm/RealmQuery$4;->call()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
