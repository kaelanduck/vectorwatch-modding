.class Lio/realm/RealmResults$RealmResultsIterator;
.super Ljava/lang/Object;
.source "RealmResults.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/RealmResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RealmResultsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field pos:I

.field tableViewVersion:J

.field final synthetic this$0:Lio/realm/RealmResults;


# direct methods
.method constructor <init>(Lio/realm/RealmResults;)V
    .locals 2

    .prologue
    .line 699
    .local p0, "this":Lio/realm/RealmResults$RealmResultsIterator;, "Lio/realm/RealmResults<TE;>.RealmResultsIterator;"
    iput-object p1, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 696
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->tableViewVersion:J

    .line 697
    const/4 v0, -0x1

    iput v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    .line 700
    # getter for: Lio/realm/RealmResults;->currentTableViewVersion:J
    invoke-static {p1}, Lio/realm/RealmResults;->access$000(Lio/realm/RealmResults;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->tableViewVersion:J

    .line 701
    return-void
.end method


# virtual methods
.method protected checkRealmIsStable()V
    .locals 6

    .prologue
    .line 734
    .local p0, "this":Lio/realm/RealmResults$RealmResultsIterator;, "Lio/realm/RealmResults<TE;>.RealmResultsIterator;"
    iget-object v2, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    # getter for: Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;
    invoke-static {v2}, Lio/realm/RealmResults;->access$100(Lio/realm/RealmResults;)Lio/realm/internal/TableOrView;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/TableOrView;->getVersion()J

    move-result-wide v0

    .line 740
    .local v0, "version":J
    iget-object v2, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    iget-object v2, v2, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Lio/realm/RealmResults$RealmResultsIterator;->tableViewVersion:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-wide v2, p0, Lio/realm/RealmResults$RealmResultsIterator;->tableViewVersion:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 741
    new-instance v2, Ljava/util/ConcurrentModificationException;

    const-string v3, "No outside changes to a Realm is allowed while iterating a RealmResults. Don\'t call Realm.refresh() while iterating."

    invoke-direct {v2, v3}, Ljava/util/ConcurrentModificationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 743
    :cond_0
    iput-wide v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->tableViewVersion:J

    .line 744
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 707
    .local p0, "this":Lio/realm/RealmResults$RealmResultsIterator;, "Lio/realm/RealmResults<TE;>.RealmResultsIterator;"
    iget v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    invoke-virtual {v1}, Lio/realm/RealmResults;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lio/realm/RealmModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 714
    .local p0, "this":Lio/realm/RealmResults$RealmResultsIterator;, "Lio/realm/RealmResults<TE;>.RealmResultsIterator;"
    iget-object v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    iget-object v0, v0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 715
    invoke-virtual {p0}, Lio/realm/RealmResults$RealmResultsIterator;->checkRealmIsStable()V

    .line 716
    iget v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    .line 717
    iget v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    iget-object v1, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    invoke-virtual {v1}, Lio/realm/RealmResults;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 718
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot access index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    invoke-virtual {v2}, Lio/realm/RealmResults;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Remember to check hasNext() before using next()."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 720
    :cond_0
    iget-object v0, p0, Lio/realm/RealmResults$RealmResultsIterator;->this$0:Lio/realm/RealmResults;

    iget v1, p0, Lio/realm/RealmResults$RealmResultsIterator;->pos:I

    invoke-virtual {v0, v1}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 695
    .local p0, "this":Lio/realm/RealmResults$RealmResultsIterator;, "Lio/realm/RealmResults<TE;>.RealmResultsIterator;"
    invoke-virtual {p0}, Lio/realm/RealmResults$RealmResultsIterator;->next()Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 730
    .local p0, "this":Lio/realm/RealmResults$RealmResultsIterator;, "Lio/realm/RealmResults<TE;>.RealmResultsIterator;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() is not supported by RealmResults iterators."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
