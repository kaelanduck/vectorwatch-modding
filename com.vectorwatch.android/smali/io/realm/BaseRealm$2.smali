.class final Lio/realm/BaseRealm$2;
.super Ljava/lang/Object;
.source "BaseRealm.java"

# interfaces
.implements Lio/realm/RealmCache$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/BaseRealm;->deleteRealm(Lio/realm/RealmConfiguration;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$configuration:Lio/realm/RealmConfiguration;

.field final synthetic val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lio/realm/RealmConfiguration;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Lio/realm/BaseRealm$2;->val$configuration:Lio/realm/RealmConfiguration;

    iput-object p2, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(I)V
    .locals 12
    .param p1, "count"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 623
    if-eqz p1, :cond_0

    .line 624
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "It\'s not allowed to delete the file associated with an open Realm. Remember to close() all the instances of the Realm before deleting its file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lio/realm/BaseRealm$2;->val$configuration:Lio/realm/RealmConfiguration;

    .line 625
    invoke-virtual {v8}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 628
    :cond_0
    iget-object v6, p0, Lio/realm/BaseRealm$2;->val$configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 629
    .local v0, "canonicalPath":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/BaseRealm$2;->val$configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->getRealmFolder()Ljava/io/File;

    move-result-object v5

    .line 630
    .local v5, "realmFolder":Ljava/io/File;
    iget-object v6, p0, Lio/realm/BaseRealm$2;->val$configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->getRealmFileName()Ljava/lang/String;

    move-result-object v4

    .line 631
    .local v4, "realmFileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ".management"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 635
    .local v3, "managementFolder":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 636
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_2

    .line 637
    array-length v10, v2

    move v9, v8

    :goto_0
    if-ge v9, v10, :cond_2

    aget-object v1, v2, v9

    .line 638
    .local v1, "file":Ljava/io/File;
    iget-object v11, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v6, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v7

    :goto_1
    invoke-virtual {v11, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 637
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    goto :goto_0

    :cond_1
    move v6, v8

    .line 638
    goto :goto_1

    .line 641
    .end local v1    # "file":Ljava/io/File;
    :cond_2
    iget-object v9, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v6, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_3

    move v6, v7

    :goto_2
    invoke-virtual {v9, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 644
    iget-object v6, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v9, p0, Lio/realm/BaseRealm$2;->val$realmDeleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v9

    if-eqz v9, :cond_4

    # invokes: Lio/realm/BaseRealm;->deletes(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z
    invoke-static {v0, v5, v4}, Lio/realm/BaseRealm;->access$000(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    :goto_3
    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 645
    return-void

    :cond_3
    move v6, v8

    .line 641
    goto :goto_2

    :cond_4
    move v7, v8

    .line 644
    goto :goto_3
.end method
