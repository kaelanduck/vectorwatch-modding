.class public final Lio/realm/RealmResults;
.super Ljava/util/AbstractList;
.source "RealmResults.java"

# interfaces
.implements Lio/realm/OrderedRealmCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RealmResults$RealmResultsListIterator;,
        Lio/realm/RealmResults$RealmResultsIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "Lio/realm/OrderedRealmCollection",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final NOT_SUPPORTED_MESSAGE:Ljava/lang/String; = "This method is not supported by RealmResults."

.field private static final TABLE_VIEW_VERSION_NONE:J = -0x1L

.field private static final TYPE_MISMATCH:Ljava/lang/String; = "Field \'%s\': type mismatch - %s expected."


# instance fields
.field private asyncQueryCompleted:Z

.field className:Ljava/lang/String;

.field classSpec:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field private currentTableViewVersion:J

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;>;"
        }
    .end annotation
.end field

.field private pendingQuery:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final query:Lio/realm/internal/TableQuery;

.field realm:Lio/realm/BaseRealm;

.field private table:Lio/realm/internal/TableOrView;

.field private viewUpdated:Z


# direct methods
.method private constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "table"    # Lio/realm/internal/TableOrView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/TableOrView;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p3, "classSpec":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 76
    iput-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 81
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 83
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    .line 85
    iput-boolean v3, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    .line 88
    iput-boolean v3, p0, Lio/realm/RealmResults;->viewUpdated:Z

    .line 124
    iput-object p1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    .line 125
    iput-object p3, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    .line 126
    iput-object p2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 128
    iput-object v2, p0, Lio/realm/RealmResults;->pendingQuery:Ljava/util/concurrent/Future;

    .line 129
    iput-object v2, p0, Lio/realm/RealmResults;->query:Lio/realm/internal/TableQuery;

    .line 130
    invoke-interface {p2}, Lio/realm/internal/TableOrView;->syncIfNeeded()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 131
    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)V
    .locals 2
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "table"    # Lio/realm/internal/TableOrView;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    .line 142
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-direct {p0, p1, p3}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Ljava/lang/String;)V

    .line 143
    iput-object p2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 144
    invoke-interface {p2}, Lio/realm/internal/TableOrView;->syncIfNeeded()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 145
    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)V
    .locals 3
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "query"    # Lio/realm/internal/TableQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/TableQuery;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const/4 v2, 0x0

    .line 111
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 81
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 83
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    .line 85
    iput-boolean v2, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    .line 88
    iput-boolean v2, p0, Lio/realm/RealmResults;->viewUpdated:Z

    .line 112
    iput-object p1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    .line 113
    iput-object p3, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    .line 114
    iput-object p2, p0, Lio/realm/RealmResults;->query:Lio/realm/internal/TableQuery;

    .line 115
    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)V
    .locals 3
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "query"    # Lio/realm/internal/TableQuery;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v2, 0x0

    .line 117
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 81
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 83
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    .line 85
    iput-boolean v2, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    .line 88
    iput-boolean v2, p0, Lio/realm/RealmResults;->viewUpdated:Z

    .line 118
    iput-object p1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    .line 119
    iput-object p2, p0, Lio/realm/RealmResults;->query:Lio/realm/internal/TableQuery;

    .line 120
    iput-object p3, p0, Lio/realm/RealmResults;->className:Ljava/lang/String;

    .line 121
    return-void
.end method

.method private constructor <init>(Lio/realm/BaseRealm;Ljava/lang/String;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 76
    iput-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 81
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 83
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    .line 85
    iput-boolean v3, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    .line 88
    iput-boolean v3, p0, Lio/realm/RealmResults;->viewUpdated:Z

    .line 134
    iput-object p1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    .line 135
    iput-object p2, p0, Lio/realm/RealmResults;->className:Ljava/lang/String;

    .line 137
    iput-object v2, p0, Lio/realm/RealmResults;->pendingQuery:Ljava/util/concurrent/Future;

    .line 138
    iput-object v2, p0, Lio/realm/RealmResults;->query:Lio/realm/internal/TableQuery;

    .line 139
    return-void
.end method

.method static synthetic access$000(Lio/realm/RealmResults;)J
    .locals 2
    .param p0, "x0"    # Lio/realm/RealmResults;

    .prologue
    .line 69
    iget-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    return-wide v0
.end method

.method static synthetic access$100(Lio/realm/RealmResults;)Lio/realm/internal/TableOrView;
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmResults;

    .prologue
    .line 69
    iget-object v0, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    return-object v0
.end method

.method static createFromDynamicClass(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p0, "realm"    # Lio/realm/BaseRealm;
    .param p1, "query"    # Lio/realm/internal/TableQuery;
    .param p2, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/TableQuery;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Lio/realm/RealmResults;

    invoke-direct {v0, p0, p1, p2}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/String;)V

    return-object v0
.end method

.method static createFromDynamicTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 2
    .param p0, "realm"    # Lio/realm/BaseRealm;
    .param p1, "table"    # Lio/realm/internal/TableOrView;
    .param p2, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/TableOrView;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<",
            "Lio/realm/DynamicRealmObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Lio/realm/RealmResults;

    invoke-direct {v0, p0, p1, p2}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/String;)V

    .line 107
    .local v0, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    iget-object v1, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v1, v0}, Lio/realm/HandlerController;->addToRealmResults(Lio/realm/RealmResults;)V

    .line 108
    return-object v0
.end method

.method static createFromTableOrView(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)Lio/realm/RealmResults;
    .locals 2
    .param p0, "realm"    # Lio/realm/BaseRealm;
    .param p1, "table"    # Lio/realm/internal/TableOrView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/TableOrView;",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    new-instance v0, Lio/realm/RealmResults;

    invoke-direct {v0, p0, p1, p2}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/TableOrView;Ljava/lang/Class;)V

    .line 97
    .local v0, "realmResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v1, p0, Lio/realm/BaseRealm;->handlerController:Lio/realm/HandlerController;

    invoke-virtual {v1, v0}, Lio/realm/HandlerController;->addToRealmResults(Lio/realm/RealmResults;)V

    .line 98
    return-object v0
.end method

.method static createFromTableQuery(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)Lio/realm/RealmResults;
    .locals 1
    .param p0, "realm"    # Lio/realm/BaseRealm;
    .param p1, "query"    # Lio/realm/internal/TableQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/TableQuery;",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    new-instance v0, Lio/realm/RealmResults;

    invoke-direct {v0, p0, p1, p2}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/TableQuery;Ljava/lang/Class;)V

    return-object v0
.end method

.method private getColumnIndexForSort(Ljava/lang/String;)J
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 316
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 317
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Non-empty field name required."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 319
    :cond_1
    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 320
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sorting using child object fields is not supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 322
    :cond_2
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, p1}, Lio/realm/internal/TableOrView;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 323
    .local v0, "columnIndex":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 324
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\' does not exist."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 326
    :cond_3
    return-wide v0
.end method

.method private notifyChangeListeners(ZZ)V
    .locals 3
    .param p1, "syncBeforeNotifying"    # Z
    .param p2, "forceNotify"    # Z

    .prologue
    .line 1000
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    if-eqz p1, :cond_0

    .line 1001
    invoke-virtual {p0}, Lio/realm/RealmResults;->syncIfNeeded()V

    .line 1003
    :cond_0
    iget-object v1, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1006
    iget-object v1, p0, Lio/realm/RealmResults;->pendingQuery:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    if-nez v1, :cond_2

    .line 1013
    :cond_1
    return-void

    .line 1007
    :cond_2
    iget-boolean v1, p0, Lio/realm/RealmResults;->viewUpdated:Z

    if-nez v1, :cond_3

    if-eqz p2, :cond_1

    .line 1008
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lio/realm/RealmResults;->viewUpdated:Z

    .line 1009
    iget-object v1, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmChangeListener;

    .line 1010
    .local v0, "listener":Lio/realm/RealmChangeListener;
    invoke-interface {v0, p0}, Lio/realm/RealmChangeListener;->onChange(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private onAsyncQueryCompleted()Z
    .locals 8

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 893
    :try_start_0
    iget-object v1, p0, Lio/realm/RealmResults;->pendingQuery:Ljava/util/concurrent/Future;

    invoke-interface {v1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 897
    .local v2, "tvHandover":J
    iget-object v1, p0, Lio/realm/RealmResults;->query:Lio/realm/internal/TableQuery;

    iget-object v6, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v6, v6, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v6}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v6

    invoke-virtual {v1, v2, v3, v6, v7}, Lio/realm/internal/TableQuery;->importHandoverTableView(JJ)Lio/realm/internal/TableView;

    move-result-object v1

    iput-object v1, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 898
    const/4 v1, 0x1

    iput-boolean v1, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    .line 899
    const/4 v1, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, v1, v6}, Lio/realm/RealmResults;->notifyChangeListeners(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v4

    .line 904
    .end local v2    # "tvHandover":J
    :goto_0
    return v1

    .line 900
    :catch_0
    move-exception v0

    .line 901
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/realm/internal/log/RealmLog;->d(Ljava/lang/String;)V

    move v1, v5

    .line 902
    goto :goto_0
.end method


# virtual methods
.method public add(ILio/realm/RealmModel;)V
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 669
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p2, "element":Lio/realm/RealmModel;, "TE;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/RealmResults;->add(ILio/realm/RealmModel;)V

    return-void
.end method

.method public add(Lio/realm/RealmModel;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 658
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "element":Lio/realm/RealmModel;, "TE;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    check-cast p1, Lio/realm/RealmModel;

    invoke-virtual {p0, p1}, Lio/realm/RealmResults;->add(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 2
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 680
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p2, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 691
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 915
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/RealmResults<TE;>;>;"
    if-nez p1, :cond_0

    .line 916
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 918
    :cond_0
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 919
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->handler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 920
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can\'t register a listener from a non-Looper thread "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 922
    :cond_1
    iget-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 923
    iget-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 925
    :cond_2
    return-void
.end method

.method public asObservable()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 979
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    instance-of v3, v3, Lio/realm/Realm;

    if-eqz v3, :cond_0

    .line 980
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v4

    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    check-cast v3, Lio/realm/Realm;

    invoke-interface {v4, v3, p0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/Realm;Lio/realm/RealmResults;)Lrx/Observable;

    move-result-object v2

    .line 986
    :goto_0
    return-object v2

    .line 981
    :cond_0
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    instance-of v3, v3, Lio/realm/DynamicRealm;

    if-eqz v3, :cond_1

    .line 982
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    check-cast v0, Lio/realm/DynamicRealm;

    .line 983
    .local v0, "dynamicRealm":Lio/realm/DynamicRealm;
    move-object v1, p0

    .line 985
    .local v1, "dynamicResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v3

    invoke-interface {v3, v0, v1}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/DynamicRealm;Lio/realm/RealmResults;)Lrx/Observable;

    move-result-object v2

    .line 986
    .local v2, "results":Lrx/Observable;
    goto :goto_0

    .line 988
    .end local v0    # "dynamicRealm":Lio/realm/DynamicRealm;
    .end local v1    # "dynamicResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    .end local v2    # "results":Lrx/Observable;
    :cond_1
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " does not support RxJava."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public average(Ljava/lang/String;)D
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 472
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 473
    invoke-direct {p0, p1}, Lio/realm/RealmResults;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 474
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmResults$1;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 482
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 476
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->averageLong(J)D

    move-result-wide v2

    .line 480
    :goto_0
    return-wide v2

    .line 478
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->averageDouble(J)D

    move-result-wide v2

    goto :goto_0

    .line 480
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->averageFloat(J)D

    move-result-wide v2

    goto :goto_0

    .line 474
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public clear()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 647
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 181
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v0, 0x0

    .line 182
    .local v0, "contains":Z
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 183
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 184
    .local v1, "proxy":Lio/realm/internal/RealmObjectProxy;
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    sget-object v3, Lio/realm/internal/InvalidRow;->INSTANCE:Lio/realm/internal/InvalidRow;

    if-eq v2, v3, :cond_0

    .line 185
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v3

    invoke-interface {v3}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/internal/TableOrView;->sourceRowIndex(J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 188
    .end local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_0
    :goto_0
    return v0

    .line 185
    .restart local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteAllFromRealm()Z
    .locals 2

    .prologue
    .line 252
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 253
    invoke-virtual {p0}, Lio/realm/RealmResults;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 254
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    .line 255
    .local v0, "table":Lio/realm/internal/TableOrView;
    invoke-interface {v0}, Lio/realm/internal/TableOrView;->clear()V

    .line 256
    const/4 v1, 0x1

    .line 258
    .end local v0    # "table":Lio/realm/internal/TableOrView;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteFirstFromRealm()Z
    .locals 2

    .prologue
    .line 630
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 631
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    .line 632
    .local v0, "table":Lio/realm/internal/TableOrView;
    invoke-interface {v0}, Lio/realm/internal/TableOrView;->removeFirst()V

    .line 633
    const/4 v1, 0x1

    .line 635
    .end local v0    # "table":Lio/realm/internal/TableOrView;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteFromRealm(I)V
    .locals 4
    .param p1, "location"    # I

    .prologue
    .line 242
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 243
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    .line 244
    .local v0, "table":Lio/realm/internal/TableOrView;
    int-to-long v2, p1

    invoke-interface {v0, v2, v3}, Lio/realm/internal/TableOrView;->remove(J)V

    .line 245
    return-void
.end method

.method public deleteLastFromRealm()Z
    .locals 2

    .prologue
    .line 607
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v1, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 608
    invoke-virtual {p0}, Lio/realm/RealmResults;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 609
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v0

    .line 610
    .local v0, "table":Lio/realm/internal/TableOrView;
    invoke-interface {v0}, Lio/realm/internal/TableOrView;->removeLast()V

    .line 611
    const/4 v1, 0x1

    .line 613
    .end local v0    # "table":Lio/realm/internal/TableOrView;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public distinct(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 497
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v3}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 498
    iget-object v3, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3}, Lio/realm/internal/TableOrView;->getTable()Lio/realm/internal/Table;

    move-result-object v3

    invoke-static {p1, v3}, Lio/realm/RealmQuery;->getAndValidateDistinctColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;)J

    move-result-wide v0

    .line 500
    .local v0, "columnIndex":J
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v2

    .line 501
    .local v2, "tableOrView":Lio/realm/internal/TableOrView;
    instance-of v3, v2, Lio/realm/internal/Table;

    if-eqz v3, :cond_0

    .line 502
    check-cast v2, Lio/realm/internal/Table;

    .end local v2    # "tableOrView":Lio/realm/internal/TableOrView;
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->getDistinctView(J)Lio/realm/internal/TableView;

    move-result-object v3

    iput-object v3, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 506
    :goto_0
    return-object p0

    .line 504
    .restart local v2    # "tableOrView":Lio/realm/internal/TableOrView;
    :cond_0
    check-cast v2, Lio/realm/internal/TableView;

    .end local v2    # "tableOrView":Lio/realm/internal/TableOrView;
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableView;->distinct(J)V

    goto :goto_0
.end method

.method public varargs distinct(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "firstFieldName"    # Ljava/lang/String;
    .param p2, "remainingFieldNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 538
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->distinct(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public distinctAsync(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 522
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->distinctAsync(Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public first()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0

    .line 220
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "No results were found."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(I)Lio/realm/RealmModel;
    .locals 8
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 202
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v1

    .line 203
    .local v1, "table":Lio/realm/internal/TableOrView;
    instance-of v2, v1, Lio/realm/internal/TableView;

    if-eqz v2, :cond_0

    .line 204
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v3, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    iget-object v4, p0, Lio/realm/RealmResults;->className:Ljava/lang/String;

    check-cast v1, Lio/realm/internal/TableView;

    .end local v1    # "table":Lio/realm/internal/TableOrView;
    int-to-long v6, p1

    invoke-virtual {v1, v6, v7}, Lio/realm/internal/TableView;->getSourceRowIndex(J)J

    move-result-wide v6

    invoke-virtual {v2, v3, v4, v6, v7}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/RealmModel;

    move-result-object v0

    .line 209
    .local v0, "obj":Lio/realm/RealmModel;, "TE;"
    :goto_0
    return-object v0

    .line 206
    .end local v0    # "obj":Lio/realm/RealmModel;, "TE;"
    .restart local v1    # "table":Lio/realm/internal/TableOrView;
    :cond_0
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v3, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    iget-object v4, p0, Lio/realm/RealmResults;->className:Ljava/lang/String;

    int-to-long v6, p1

    invoke-virtual {v2, v3, v4, v6, v7}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/RealmModel;

    move-result-object v0

    .restart local v0    # "obj":Lio/realm/RealmModel;, "TE;"
    goto :goto_0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method getTable()Lio/realm/internal/TableOrView;
    .locals 2

    .prologue
    .line 148
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    iget-object v1, p0, Lio/realm/RealmResults;->classSpec:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    goto :goto_0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 863
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 864
    iget-object v0, p0, Lio/realm/RealmResults;->pendingQuery:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 159
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 271
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/realm/RealmResults$RealmResultsIterator;

    invoke-direct {v0, p0}, Lio/realm/RealmResults$RealmResultsIterator;-><init>(Lio/realm/RealmResults;)V

    goto :goto_0
.end method

.method public last()Lio/realm/RealmModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 229
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->size()I

    move-result v0

    .line 230
    .local v0, "size":I
    if-lez v0, :cond_0

    .line 231
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v1

    return-object v1

    .line 233
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    const-string v2, "No results were found."

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 287
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 291
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/realm/RealmResults$RealmResultsListIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/realm/RealmResults$RealmResultsListIterator;-><init>(Lio/realm/RealmResults;I)V

    goto :goto_0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 305
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 309
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/realm/RealmResults$RealmResultsListIterator;

    invoke-direct {v0, p0, p1}, Lio/realm/RealmResults$RealmResultsListIterator;-><init>(Lio/realm/RealmResults;I)V

    goto :goto_0
.end method

.method public load()Z
    .locals 1

    .prologue
    .line 876
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    const/4 v0, 0x1

    .line 881
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lio/realm/RealmResults;->onAsyncQueryCompleted()Z

    move-result v0

    goto :goto_0
.end method

.method public max(Ljava/lang/String;)Ljava/lang/Number;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 414
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 415
    invoke-direct {p0, p1}, Lio/realm/RealmResults;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 416
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmResults$1;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 424
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 418
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->maximumLong(J)Ljava/lang/Long;

    move-result-object v2

    .line 422
    :goto_0
    return-object v2

    .line 420
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->maximumFloat(J)Ljava/lang/Float;

    move-result-object v2

    goto :goto_0

    .line 422
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->maximumDouble(J)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 416
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public maxDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 439
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 440
    invoke-direct {p0, p1}, Lio/realm/RealmResults;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 441
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v2

    sget-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    if-ne v2, v3, :cond_0

    .line 442
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->maximumDate(J)Ljava/util/Date;

    move-result-object v2

    return-object v2

    .line 445
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "Date"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public min(Ljava/lang/String;)Ljava/lang/Number;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 382
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 383
    invoke-direct {p0, p1}, Lio/realm/RealmResults;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 384
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmResults$1;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 392
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 386
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->minimumLong(J)Ljava/lang/Long;

    move-result-object v2

    .line 390
    :goto_0
    return-object v2

    .line 388
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->minimumFloat(J)Ljava/lang/Float;

    move-result-object v2

    goto :goto_0

    .line 390
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->minimumDouble(J)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public minDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 400
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 401
    invoke-direct {p0, p1}, Lio/realm/RealmResults;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 402
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v2

    sget-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    if-ne v2, v3, :cond_0

    .line 403
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->minimumDate(J)Ljava/util/Date;

    move-result-object v2

    return-object v2

    .line 406
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "Date"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method notifyChangeListeners()V
    .locals 2

    .prologue
    .line 996
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/RealmResults;->notifyChangeListeners(ZZ)V

    .line 997
    return-void
.end method

.method public remove(I)Lio/realm/RealmModel;
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 551
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/RealmResults;->remove(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 562
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 573
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lio/realm/RealmChangeListener;

    .prologue
    .line 935
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    if-nez p1, :cond_0

    .line 936
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 938
    :cond_0
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 939
    iget-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 940
    return-void
.end method

.method public removeChangeListeners()V
    .locals 1

    .prologue
    .line 946
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 947
    iget-object v0, p0, Lio/realm/RealmResults;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 948
    return-void
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 597
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public set(ILio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 2
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 584
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by RealmResults."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/RealmResults;->set(ILio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method setPendingQuery(Ljava/util/concurrent/Future;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 844
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "pendingQuery":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Long;>;"
    iput-object p1, p0, Lio/realm/RealmResults;->pendingQuery:Ljava/util/concurrent/Future;

    .line 845
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 849
    invoke-direct {p0}, Lio/realm/RealmResults;->onAsyncQueryCompleted()Z

    .line 851
    :cond_0
    return-void
.end method

.method public size()I
    .locals 4

    .prologue
    .line 370
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->isLoaded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 371
    const/4 v2, 0x0

    .line 374
    :goto_0
    return v2

    .line 373
    :cond_0
    invoke-virtual {p0}, Lio/realm/RealmResults;->getTable()Lio/realm/internal/TableOrView;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/TableOrView;->size()J

    move-result-wide v0

    .line 374
    .local v0, "size":J
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    const v2, 0x7fffffff

    goto :goto_0

    :cond_1
    long-to-int v2, v0

    goto :goto_0
.end method

.method public sort(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 334
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    sget-object v0, Lio/realm/Sort;->ASCENDING:Lio/realm/Sort;

    invoke-virtual {p0, p1, v0}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 342
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/RealmResults;->sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 350
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->findAllSorted([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sum(Ljava/lang/String;)Ljava/lang/Number;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 454
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 455
    invoke-direct {p0, p1}, Lio/realm/RealmResults;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 456
    .local v0, "columnIndex":J
    sget-object v2, Lio/realm/RealmResults$1;->$SwitchMap$io$realm$RealmFieldType:[I

    iget-object v3, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v3, v0, v1}, Lio/realm/internal/TableOrView;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 464
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\': type mismatch - %s expected."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "int, float or double"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 458
    :pswitch_0
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->sumLong(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 462
    :goto_0
    return-object v2

    .line 460
    :pswitch_1
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->sumFloat(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 462
    :pswitch_2
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2, v0, v1}, Lio/realm/internal/TableOrView;->sumDouble(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 456
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method swapTableViewPointer(J)V
    .locals 5
    .param p1, "handoverTableViewPointer"    # J

    .prologue
    .line 830
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :try_start_0
    iget-object v1, p0, Lio/realm/RealmResults;->query:Lio/realm/internal/TableQuery;

    iget-object v2, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v2, v2, Lio/realm/BaseRealm;->sharedGroupManager:Lio/realm/internal/SharedGroupManager;

    invoke-virtual {v2}, Lio/realm/internal/SharedGroupManager;->getNativePointer()J

    move-result-wide v2

    invoke-virtual {v1, p1, p2, v2, v3}, Lio/realm/internal/TableQuery;->importHandoverTableView(JJ)Lio/realm/internal/TableView;

    move-result-object v1

    iput-object v1, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    .line 831
    const/4 v1, 0x1

    iput-boolean v1, p0, Lio/realm/RealmResults;->asyncQueryCompleted:Z
    :try_end_0
    .catch Lio/realm/internal/async/BadVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 835
    return-void

    .line 832
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Lio/realm/internal/async/BadVersionException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Caller and Worker Realm should have been at the same version"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method syncIfNeeded()V
    .locals 4

    .prologue
    .line 618
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->table:Lio/realm/internal/TableOrView;

    invoke-interface {v2}, Lio/realm/internal/TableOrView;->syncIfNeeded()J

    move-result-wide v0

    .line 619
    .local v0, "newVersion":J
    iget-wide v2, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lio/realm/RealmResults;->viewUpdated:Z

    .line 620
    iput-wide v0, p0, Lio/realm/RealmResults;->currentTableViewVersion:J

    .line 621
    return-void

    .line 619
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public where()Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 167
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 169
    invoke-static {p0}, Lio/realm/RealmQuery;->createQueryFromResult(Lio/realm/RealmResults;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method
