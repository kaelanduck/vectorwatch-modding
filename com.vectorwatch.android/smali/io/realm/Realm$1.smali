.class Lio/realm/Realm$1;
.super Ljava/lang/Object;
.source "Realm.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;Lio/realm/Realm$Transaction$OnError;)Lio/realm/RealmAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/Realm;

.field final synthetic val$onError:Lio/realm/Realm$Transaction$OnError;

.field final synthetic val$onSuccess:Lio/realm/Realm$Transaction$OnSuccess;

.field final synthetic val$realmConfiguration:Lio/realm/RealmConfiguration;

.field final synthetic val$transaction:Lio/realm/Realm$Transaction;


# direct methods
.method constructor <init>(Lio/realm/Realm;Lio/realm/RealmConfiguration;Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;Lio/realm/Realm$Transaction$OnError;)V
    .locals 0
    .param p1, "this$0"    # Lio/realm/Realm;

    .prologue
    .line 1050
    iput-object p1, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iput-object p2, p0, Lio/realm/Realm$1;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    iput-object p3, p0, Lio/realm/Realm$1;->val$transaction:Lio/realm/Realm$Transaction;

    iput-object p4, p0, Lio/realm/Realm$1;->val$onSuccess:Lio/realm/Realm$Transaction$OnSuccess;

    iput-object p5, p0, Lio/realm/Realm$1;->val$onError:Lio/realm/Realm$Transaction$OnError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1053
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1141
    :cond_0
    :goto_0
    return-void

    .line 1057
    :cond_1
    const/4 v4, 0x0

    .line 1058
    .local v4, "transactionCommitted":Z
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/Throwable;

    .line 1059
    .local v3, "exception":[Ljava/lang/Throwable;
    iget-object v5, p0, Lio/realm/Realm$1;->val$realmConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v5}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v1

    .line 1060
    .local v1, "bgRealm":Lio/realm/Realm;
    invoke-virtual {v1}, Lio/realm/Realm;->beginTransaction()V

    .line 1062
    :try_start_0
    iget-object v5, p0, Lio/realm/Realm$1;->val$transaction:Lio/realm/Realm$Transaction;

    invoke-interface {v5, v1}, Lio/realm/Realm$Transaction;->execute(Lio/realm/Realm;)V

    .line 1064
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1065
    const/4 v5, 0x0

    new-instance v6, Lio/realm/Realm$1$1;

    invoke-direct {v6, p0, v1}, Lio/realm/Realm$1$1;-><init>(Lio/realm/Realm$1;Lio/realm/Realm;)V

    invoke-virtual {v1, v5, v6}, Lio/realm/Realm;->commitTransaction(ZLjava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1074
    const/4 v4, 0x1

    .line 1079
    :cond_2
    invoke-virtual {v1}, Lio/realm/Realm;->isClosed()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1080
    invoke-virtual {v1}, Lio/realm/Realm;->isInTransaction()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1081
    invoke-virtual {v1}, Lio/realm/Realm;->cancelTransaction()V

    .line 1085
    :cond_3
    :goto_1
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 1088
    :cond_4
    aget-object v0, v3, v7

    .line 1090
    .local v0, "backgroundException":Ljava/lang/Throwable;
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    if-eqz v5, :cond_8

    .line 1091
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    .line 1092
    invoke-virtual {v5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1093
    iget-object v5, p0, Lio/realm/Realm$1;->val$onSuccess:Lio/realm/Realm$Transaction$OnSuccess;

    if-eqz v5, :cond_5

    if-eqz v4, :cond_5

    .line 1094
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v6, Lio/realm/Realm$1$2;

    invoke-direct {v6, p0}, Lio/realm/Realm$1$2;-><init>(Lio/realm/Realm$1;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1102
    :cond_5
    if-eqz v0, :cond_0

    .line 1103
    iget-object v5, p0, Lio/realm/Realm$1;->val$onError:Lio/realm/Realm$Transaction$OnError;

    if-eqz v5, :cond_7

    .line 1104
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v6, Lio/realm/Realm$1$3;

    invoke-direct {v6, p0, v0}, Lio/realm/Realm$1$3;-><init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1082
    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_6
    aget-object v5, v3, v7

    if-eqz v5, :cond_3

    .line 1083
    const-string v5, "Could not cancel transaction, not currently in a transaction."

    invoke-static {v5}, Lio/realm/internal/log/RealmLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 1111
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_7
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v6, Lio/realm/Realm$1$4;

    invoke-direct {v6, p0, v0}, Lio/realm/Realm$1$4;-><init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1127
    :cond_8
    if-eqz v0, :cond_0

    .line 1128
    instance-of v5, v0, Ljava/lang/RuntimeException;

    if-eqz v5, :cond_9

    .line 1130
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    throw v0

    .line 1131
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_9
    instance-of v5, v0, Ljava/lang/Exception;

    if-eqz v5, :cond_a

    .line 1133
    new-instance v5, Lio/realm/exceptions/RealmException;

    const-string v6, "Async transaction failed"

    invoke-direct {v5, v6, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1134
    :cond_a
    instance-of v5, v0, Ljava/lang/Error;

    if-eqz v5, :cond_0

    .line 1136
    check-cast v0, Ljava/lang/Error;

    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    throw v0

    .line 1076
    :catch_0
    move-exception v2

    .line 1077
    .local v2, "e":Ljava/lang/Throwable;
    const/4 v5, 0x0

    :try_start_1
    aput-object v2, v3, v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1079
    invoke-virtual {v1}, Lio/realm/Realm;->isClosed()Z

    move-result v5

    if-nez v5, :cond_c

    .line 1080
    invoke-virtual {v1}, Lio/realm/Realm;->isInTransaction()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1081
    invoke-virtual {v1}, Lio/realm/Realm;->cancelTransaction()V

    .line 1085
    :cond_b
    :goto_2
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 1088
    :cond_c
    aget-object v0, v3, v7

    .line 1090
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    if-eqz v5, :cond_10

    .line 1091
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-nez v5, :cond_10

    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    .line 1092
    invoke-virtual {v5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1093
    iget-object v5, p0, Lio/realm/Realm$1;->val$onSuccess:Lio/realm/Realm$Transaction$OnSuccess;

    if-eqz v5, :cond_d

    if-eqz v4, :cond_d

    .line 1094
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v6, Lio/realm/Realm$1$2;

    invoke-direct {v6, p0}, Lio/realm/Realm$1$2;-><init>(Lio/realm/Realm$1;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1102
    :cond_d
    if-eqz v0, :cond_0

    .line 1103
    iget-object v5, p0, Lio/realm/Realm$1;->val$onError:Lio/realm/Realm$Transaction$OnError;

    if-eqz v5, :cond_f

    .line 1104
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v6, Lio/realm/Realm$1$3;

    invoke-direct {v6, p0, v0}, Lio/realm/Realm$1$3;-><init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1082
    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_e
    aget-object v5, v3, v7

    if-eqz v5, :cond_b

    .line 1083
    const-string v5, "Could not cancel transaction, not currently in a transaction."

    invoke-static {v5}, Lio/realm/internal/log/RealmLog;->w(Ljava/lang/String;)V

    goto :goto_2

    .line 1111
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_f
    iget-object v5, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v5, v5, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v6, Lio/realm/Realm$1$4;

    invoke-direct {v6, p0, v0}, Lio/realm/Realm$1$4;-><init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1127
    :cond_10
    if-eqz v0, :cond_0

    .line 1128
    instance-of v5, v0, Ljava/lang/RuntimeException;

    if-eqz v5, :cond_11

    .line 1130
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    throw v0

    .line 1131
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_11
    instance-of v5, v0, Ljava/lang/Exception;

    if-eqz v5, :cond_12

    .line 1133
    new-instance v5, Lio/realm/exceptions/RealmException;

    const-string v6, "Async transaction failed"

    invoke-direct {v5, v6, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1134
    :cond_12
    instance-of v5, v0, Ljava/lang/Error;

    if-eqz v5, :cond_0

    .line 1136
    check-cast v0, Ljava/lang/Error;

    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    throw v0

    .line 1079
    .end local v2    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Lio/realm/Realm;->isClosed()Z

    move-result v6

    if-nez v6, :cond_14

    .line 1080
    invoke-virtual {v1}, Lio/realm/Realm;->isInTransaction()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 1081
    invoke-virtual {v1}, Lio/realm/Realm;->cancelTransaction()V

    .line 1085
    :cond_13
    :goto_3
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 1088
    :cond_14
    aget-object v0, v3, v7

    .line 1090
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    iget-object v6, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v6, v6, Lio/realm/Realm;->handler:Landroid/os/Handler;

    if-eqz v6, :cond_19

    .line 1091
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v6

    if-nez v6, :cond_19

    iget-object v6, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v6, v6, Lio/realm/Realm;->handler:Landroid/os/Handler;

    .line 1092
    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->isAlive()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1093
    iget-object v6, p0, Lio/realm/Realm$1;->val$onSuccess:Lio/realm/Realm$Transaction$OnSuccess;

    if-eqz v6, :cond_15

    if-eqz v4, :cond_15

    .line 1094
    iget-object v6, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v6, v6, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v7, Lio/realm/Realm$1$2;

    invoke-direct {v7, p0}, Lio/realm/Realm$1$2;-><init>(Lio/realm/Realm$1;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1102
    :cond_15
    if-eqz v0, :cond_16

    .line 1103
    iget-object v6, p0, Lio/realm/Realm$1;->val$onError:Lio/realm/Realm$Transaction$OnError;

    if-eqz v6, :cond_18

    .line 1104
    iget-object v6, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v6, v6, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v7, Lio/realm/Realm$1$3;

    invoke-direct {v7, p0, v0}, Lio/realm/Realm$1$3;-><init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1140
    :cond_16
    :goto_4
    throw v5

    .line 1082
    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_17
    aget-object v6, v3, v7

    if-eqz v6, :cond_13

    .line 1083
    const-string v6, "Could not cancel transaction, not currently in a transaction."

    invoke-static {v6}, Lio/realm/internal/log/RealmLog;->w(Ljava/lang/String;)V

    goto :goto_3

    .line 1111
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_18
    iget-object v6, p0, Lio/realm/Realm$1;->this$0:Lio/realm/Realm;

    iget-object v6, v6, Lio/realm/Realm;->handler:Landroid/os/Handler;

    new-instance v7, Lio/realm/Realm$1$4;

    invoke-direct {v7, p0, v0}, Lio/realm/Realm$1$4;-><init>(Lio/realm/Realm$1;Ljava/lang/Throwable;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4

    .line 1127
    :cond_19
    if-eqz v0, :cond_16

    .line 1128
    instance-of v6, v0, Ljava/lang/RuntimeException;

    if-eqz v6, :cond_1a

    .line 1130
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    throw v0

    .line 1131
    .restart local v0    # "backgroundException":Ljava/lang/Throwable;
    :cond_1a
    instance-of v6, v0, Ljava/lang/Exception;

    if-eqz v6, :cond_1b

    .line 1133
    new-instance v5, Lio/realm/exceptions/RealmException;

    const-string v6, "Async transaction failed"

    invoke-direct {v5, v6, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1134
    :cond_1b
    instance-of v6, v0, Ljava/lang/Error;

    if-eqz v6, :cond_16

    .line 1136
    check-cast v0, Ljava/lang/Error;

    .end local v0    # "backgroundException":Ljava/lang/Throwable;
    throw v0
.end method
