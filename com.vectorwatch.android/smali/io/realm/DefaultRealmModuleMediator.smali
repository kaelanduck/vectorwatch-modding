.class Lio/realm/DefaultRealmModuleMediator;
.super Lio/realm/internal/RealmProxyMediator;
.source "DefaultRealmModuleMediator.java"


# annotations
.annotation runtime Lio/realm/annotations/RealmModule;
.end annotation


# static fields
.field private static final MODEL_CLASSES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 31
    .local v0, "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 32
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lio/realm/DefaultRealmModuleMediator;->MODEL_CLASSES:Ljava/util/Set;

    .line 37
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lio/realm/internal/RealmProxyMediator;-><init>()V

    return-void
.end method


# virtual methods
.method public copyOrUpdate(Lio/realm/Realm;Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .param p3, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "TE;Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 143
    .local p2, "obj":Lio/realm/RealmModel;, "TE;"
    .local p4, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v1, p2, Lio/realm/internal/RealmObjectProxy;

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 145
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    :goto_0
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/StringObjectRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 154
    :goto_1
    return-object v1

    .line 143
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 147
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    :cond_1
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 148
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/ActivityAlertRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 149
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_2
    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 150
    check-cast p2, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/CloudActivityDayRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 151
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_3
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 152
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/AlarmRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 153
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_4
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 154
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/LongObjectRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 156
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_5
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v1

    throw v1
.end method

.method public createDetachedCopy(Lio/realm/RealmModel;ILjava/util/Map;)Lio/realm/RealmModel;
    .locals 3
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;I",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)TE;"
        }
    .end annotation

    .prologue
    .local p1, "realmObject":Lio/realm/RealmModel;, "TE;"
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    const/4 v2, 0x0

    .line 204
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 206
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/StringObjectRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 215
    :goto_0
    return-object v1

    .line 208
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_0
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/ActivityAlertRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 210
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1
    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211
    check-cast p1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/CloudActivityDayRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;IILjava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 212
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_2
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 213
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/AlarmRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 214
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_3
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 215
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/LongObjectRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 217
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_4
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v1

    throw v1
.end method

.method public createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    .locals 1
    .param p2, "realm"    # Lio/realm/Realm;
    .param p3, "json"    # Lorg/json/JSONObject;
    .param p4, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/Realm;",
            "Lorg/json/JSONObject;",
            "Z)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 165
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-static {p2, p3, p4}, Lio/realm/StringObjectRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 174
    :goto_0
    return-object v0

    .line 167
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-static {p2, p3, p4}, Lio/realm/ActivityAlertRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 169
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    invoke-static {p2, p3, p4}, Lio/realm/CloudActivityDayRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 171
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 172
    invoke-static {p2, p3, p4}, Lio/realm/AlarmRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 173
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 174
    invoke-static {p2, p3, p4}, Lio/realm/LongObjectRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 176
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public createTable(Ljava/lang/Class;Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;
    .locals 1
    .param p2, "transaction"    # Lio/realm/internal/ImplicitTransaction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/internal/ImplicitTransaction;",
            ")",
            "Lio/realm/internal/Table;"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 43
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {p2}, Lio/realm/StringObjectRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    .line 45
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    invoke-static {p2}, Lio/realm/ActivityAlertRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 47
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 48
    invoke-static {p2}, Lio/realm/CloudActivityDayRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50
    invoke-static {p2}, Lio/realm/AlarmRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 52
    invoke-static {p2}, Lio/realm/LongObjectRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public createUsingJsonStream(Ljava/lang/Class;Lio/realm/Realm;Landroid/util/JsonReader;)Lio/realm/RealmModel;
    .locals 1
    .param p2, "realm"    # Lio/realm/Realm;
    .param p3, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/Realm;",
            "Landroid/util/JsonReader;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 185
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    invoke-static {p2, p3}, Lio/realm/StringObjectRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 194
    :goto_0
    return-object v0

    .line 187
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    invoke-static {p2, p3}, Lio/realm/ActivityAlertRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 189
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    invoke-static {p2, p3}, Lio/realm/CloudActivityDayRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 191
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    invoke-static {p2, p3}, Lio/realm/AlarmRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 193
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 194
    invoke-static {p2, p3}, Lio/realm/LongObjectRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 196
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public getFieldNames(Ljava/lang/Class;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 81
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-static {}, Lio/realm/StringObjectRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    .line 83
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    invoke-static {}, Lio/realm/ActivityAlertRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    invoke-static {}, Lio/realm/CloudActivityDayRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    invoke-static {}, Lio/realm/AlarmRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 90
    invoke-static {}, Lio/realm/LongObjectRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 92
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public getModelClasses()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lio/realm/DefaultRealmModuleMediator;->MODEL_CLASSES:Ljava/util/Set;

    return-object v0
.end method

.method public getTableName(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 100
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {}, Lio/realm/StringObjectRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 102
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    invoke-static {}, Lio/realm/ActivityAlertRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    invoke-static {}, Lio/realm/CloudActivityDayRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    invoke-static {}, Lio/realm/AlarmRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 109
    invoke-static {}, Lio/realm/LongObjectRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 111
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public newInstance(Ljava/lang/Class;Lio/realm/internal/ColumnInfo;)Lio/realm/RealmModel;
    .locals 1
    .param p2, "columnInfo"    # Lio/realm/internal/ColumnInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/internal/ColumnInfo;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 119
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Lio/realm/StringObjectRealmProxy;

    invoke-direct {v0, p2}, Lio/realm/StringObjectRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 128
    :goto_0
    return-object v0

    .line 121
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    new-instance v0, Lio/realm/ActivityAlertRealmProxy;

    invoke-direct {v0, p2}, Lio/realm/ActivityAlertRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 123
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    new-instance v0, Lio/realm/CloudActivityDayRealmProxy;

    invoke-direct {v0, p2}, Lio/realm/CloudActivityDayRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 125
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    new-instance v0, Lio/realm/AlarmRealmProxy;

    invoke-direct {v0, p2}, Lio/realm/AlarmRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 127
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 128
    new-instance v0, Lio/realm/LongObjectRealmProxy;

    invoke-direct {v0, p2}, Lio/realm/LongObjectRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 130
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public transformerApplied()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public validateTable(Ljava/lang/Class;Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/ColumnInfo;
    .locals 1
    .param p2, "transaction"    # Lio/realm/internal/ImplicitTransaction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/internal/ImplicitTransaction;",
            ")",
            "Lio/realm/internal/ColumnInfo;"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 62
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-static {p2}, Lio/realm/StringObjectRealmProxy;->validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    .line 64
    :cond_0
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    invoke-static {p2}, Lio/realm/ActivityAlertRealmProxy;->validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_1
    const-class v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    invoke-static {p2}, Lio/realm/CloudActivityDayRealmProxy;->validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_2
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    invoke-static {p2}, Lio/realm/AlarmRealmProxy;->validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_3
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 71
    invoke-static {p2}, Lio/realm/LongObjectRealmProxy;->validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_4
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method
