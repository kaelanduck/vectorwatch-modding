.class public final Lio/realm/RealmSchema;
.super Ljava/lang/Object;
.source "RealmSchema.java"


# static fields
.field private static final EMPTY_STRING_MSG:Ljava/lang/String; = "Null or empty class names are not allowed"

.field private static final TABLE_PREFIX:Ljava/lang/String;


# instance fields
.field private final classToSchema:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/RealmObjectSchema;",
            ">;"
        }
    .end annotation
.end field

.field private final classToTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/internal/Table;",
            ">;"
        }
    .end annotation
.end field

.field columnIndices:Lio/realm/internal/ColumnIndices;

.field private final dynamicClassToSchema:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/RealmObjectSchema;",
            ">;"
        }
    .end annotation
.end field

.field private final dynamicClassToTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/internal/Table;",
            ">;"
        }
    .end annotation
.end field

.field private final realm:Lio/realm/BaseRealm;

.field private final transaction:Lio/realm/internal/ImplicitTransaction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    sput-object v0, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/ImplicitTransaction;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/RealmSchema;->dynamicClassToTable:Ljava/util/Map;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/RealmSchema;->classToTable:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/RealmSchema;->classToSchema:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/RealmSchema;->dynamicClassToSchema:Ljava/util/Map;

    .line 60
    iput-object p1, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    .line 61
    iput-object p2, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    .line 62
    return-void
.end method

.method private checkEmpty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 188
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_1
    return-void
.end method

.method private checkHasTable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 194
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "internalTableName":Ljava/lang/String;
    iget-object v1, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v1, v0}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :cond_0
    return-void
.end method

.method static getSchemaForTable(Lio/realm/internal/Table;)Ljava/lang/String;
    .locals 2
    .param p0, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 262
    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .locals 3
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 184
    iget-object v0, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    .locals 6
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 110
    const-string v3, "Null or empty class names are not allowed"

    invoke-direct {p0, p1, v3}, Lio/realm/RealmSchema;->checkEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "internalTableName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x38

    if-le v3, v4, :cond_0

    .line 113
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Class name is to long. Limit is 57 characters: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 115
    :cond_0
    iget-object v3, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v3, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Class already exists: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 118
    :cond_1
    iget-object v3, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v3, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 119
    .local v2, "table":Lio/realm/internal/Table;
    new-instance v0, Lio/realm/RealmObjectSchema$DynamicColumnMap;

    invoke-direct {v0, v2}, Lio/realm/RealmObjectSchema$DynamicColumnMap;-><init>(Lio/realm/internal/Table;)V

    .line 120
    .local v0, "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    new-instance v3, Lio/realm/RealmObjectSchema;

    iget-object v4, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    invoke-direct {v3, v4, v2, v0}, Lio/realm/RealmObjectSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Table;Ljava/util/Map;)V

    return-object v3
.end method

.method public get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    .locals 5
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 72
    const-string v3, "Null or empty class names are not allowed"

    invoke-direct {p0, p1, v3}, Lio/realm/RealmSchema;->checkEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "internalClassName":Ljava/lang/String;
    iget-object v3, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v3, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v3, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v3, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 76
    .local v2, "table":Lio/realm/internal/Table;
    new-instance v0, Lio/realm/RealmObjectSchema$DynamicColumnMap;

    invoke-direct {v0, v2}, Lio/realm/RealmObjectSchema$DynamicColumnMap;-><init>(Lio/realm/internal/Table;)V

    .line 77
    .local v0, "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    new-instance v3, Lio/realm/RealmObjectSchema;

    iget-object v4, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    invoke-direct {v3, v4, v2, v0}, Lio/realm/RealmObjectSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Table;Ljava/util/Map;)V

    .line 79
    .end local v0    # "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    .end local v2    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getAll()Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmObjectSchema;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v6, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v6}, Lio/realm/internal/ImplicitTransaction;->size()J

    move-result-wide v6

    long-to-int v4, v6

    .line 90
    .local v4, "tableCount":I
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2, v4}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 91
    .local v2, "schemas":Ljava/util/Set;, "Ljava/util/Set<Lio/realm/RealmObjectSchema;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_1

    .line 92
    iget-object v6, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v6, v1}, Lio/realm/internal/ImplicitTransaction;->getTableName(I)Ljava/lang/String;

    move-result-object v5

    .line 93
    .local v5, "tableName":Ljava/lang/String;
    invoke-static {v5}, Lio/realm/internal/Table;->isMetaTable(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 91
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_0
    iget-object v6, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v6, v5}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    .line 97
    .local v3, "table":Lio/realm/internal/Table;
    new-instance v0, Lio/realm/RealmObjectSchema$DynamicColumnMap;

    invoke-direct {v0, v3}, Lio/realm/RealmObjectSchema$DynamicColumnMap;-><init>(Lio/realm/internal/Table;)V

    .line 98
    .local v0, "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    new-instance v6, Lio/realm/RealmObjectSchema;

    iget-object v7, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    invoke-direct {v6, v7, v3, v0}, Lio/realm/RealmObjectSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Table;Ljava/util/Map;)V

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 100
    .end local v0    # "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    .end local v3    # "table":Lio/realm/internal/Table;
    .end local v5    # "tableName":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Lio/realm/internal/ColumnInfo;"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    iget-object v1, p0, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    invoke-virtual {v1, p1}, Lio/realm/internal/ColumnIndices;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v0

    .line 202
    .local v0, "columnInfo":Lio/realm/internal/ColumnInfo;
    if-nez v0, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No validated schema information found for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v3

    invoke-virtual {v3, p1}, Lio/realm/internal/RealmProxyMediator;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 205
    :cond_0
    return-object v0
.end method

.method getSchemaForClass(Ljava/lang/Class;)Lio/realm/RealmObjectSchema;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Lio/realm/RealmObjectSchema;"
        }
    .end annotation

    .prologue
    .line 232
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    iget-object v2, p0, Lio/realm/RealmSchema;->classToSchema:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmObjectSchema;

    .line 233
    .local v0, "classSchema":Lio/realm/RealmObjectSchema;
    if-nez v0, :cond_0

    .line 234
    invoke-static {p1}, Lio/realm/internal/Util;->getOriginalModelClass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p1

    .line 235
    iget-object v2, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    iget-object v3, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v3

    invoke-virtual {v3, p1}, Lio/realm/internal/RealmProxyMediator;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 236
    .local v1, "table":Lio/realm/internal/Table;
    new-instance v0, Lio/realm/RealmObjectSchema;

    .end local v0    # "classSchema":Lio/realm/RealmObjectSchema;
    iget-object v2, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    iget-object v3, p0, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    invoke-virtual {v3, p1}, Lio/realm/internal/ColumnIndices;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/internal/ColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Lio/realm/RealmObjectSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Table;Ljava/util/Map;)V

    .line 237
    .restart local v0    # "classSchema":Lio/realm/RealmObjectSchema;
    iget-object v2, p0, Lio/realm/RealmSchema;->classToSchema:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    .end local v1    # "table":Lio/realm/internal/Table;
    :cond_0
    return-object v0
.end method

.method getSchemaForClass(Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    .locals 6
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 243
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 244
    iget-object v3, p0, Lio/realm/RealmSchema;->dynamicClassToSchema:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmObjectSchema;

    .line 245
    .local v1, "dynamicSchema":Lio/realm/RealmObjectSchema;
    if-nez v1, :cond_1

    .line 246
    iget-object v3, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v3, p1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 247
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The class "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " doesn\'t exist in this Realm."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 249
    :cond_0
    iget-object v3, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v3, p1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 250
    .local v2, "table":Lio/realm/internal/Table;
    new-instance v0, Lio/realm/RealmObjectSchema$DynamicColumnMap;

    invoke-direct {v0, v2}, Lio/realm/RealmObjectSchema$DynamicColumnMap;-><init>(Lio/realm/internal/Table;)V

    .line 251
    .local v0, "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    new-instance v1, Lio/realm/RealmObjectSchema;

    .end local v1    # "dynamicSchema":Lio/realm/RealmObjectSchema;
    iget-object v3, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    invoke-direct {v1, v3, v2, v0}, Lio/realm/RealmObjectSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Table;Ljava/util/Map;)V

    .line 252
    .restart local v1    # "dynamicSchema":Lio/realm/RealmObjectSchema;
    iget-object v3, p0, Lio/realm/RealmSchema;->dynamicClassToSchema:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    .end local v0    # "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    .end local v2    # "table":Lio/realm/internal/Table;
    :cond_1
    return-object v1
.end method

.method getTable(Ljava/lang/Class;)Lio/realm/internal/Table;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Lio/realm/internal/Table;"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    iget-object v1, p0, Lio/realm/RealmSchema;->classToTable:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Table;

    .line 223
    .local v0, "table":Lio/realm/internal/Table;
    if-nez v0, :cond_0

    .line 224
    invoke-static {p1}, Lio/realm/internal/Util;->getOriginalModelClass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p1

    .line 225
    iget-object v1, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    iget-object v2, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    iget-object v2, v2, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v2

    invoke-virtual {v2, p1}, Lio/realm/internal/RealmProxyMediator;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 226
    iget-object v1, p0, Lio/realm/RealmSchema;->classToTable:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    :cond_0
    return-object v0
.end method

.method getTable(Ljava/lang/String;)Lio/realm/internal/Table;
    .locals 4
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 210
    iget-object v1, p0, Lio/realm/RealmSchema;->dynamicClassToTable:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Table;

    .line 211
    .local v0, "table":Lio/realm/internal/Table;
    if-nez v0, :cond_1

    .line 212
    iget-object v1, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v1, p1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 213
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t exist in this Realm."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    :cond_0
    iget-object v1, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v1, p1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lio/realm/RealmSchema;->dynamicClassToTable:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_1
    return-object v0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 4
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 130
    const-string v2, "Null or empty class names are not allowed"

    invoke-direct {p0, p1, v2}, Lio/realm/RealmSchema;->checkEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "internalTableName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot remove class because it is not in this Realm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lio/realm/RealmSchema;->checkHasTable(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 134
    .local v1, "table":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 137
    :cond_0
    iget-object v2, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v2, v0}, Lio/realm/internal/ImplicitTransaction;->removeTable(Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    .locals 9
    .param p1, "oldClassName"    # Ljava/lang/String;
    .param p2, "newClassName"    # Ljava/lang/String;

    .prologue
    .line 148
    const-string v6, "Class names cannot be empty or null"

    invoke-direct {p0, p1, v6}, Lio/realm/RealmSchema;->checkEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v6, "Class names cannot be empty or null"

    invoke-direct {p0, p2, v6}, Lio/realm/RealmSchema;->checkEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "oldInternalName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lio/realm/RealmSchema;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "newInternalName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot rename class because it doesn\'t exist in this Realm: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lio/realm/RealmSchema;->checkHasTable(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v6, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v6, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 154
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cannot be renamed because the new class already exists: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 158
    :cond_0
    invoke-virtual {p0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    .line 159
    .local v3, "oldTable":Lio/realm/internal/Table;
    const/4 v4, 0x0

    .line 160
    .local v4, "pkField":Ljava/lang/String;
    invoke-virtual {v3}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 161
    invoke-virtual {v3}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v4

    .line 162
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 165
    :cond_1
    iget-object v6, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v6, v2, v1}, Lio/realm/internal/ImplicitTransaction;->renameTable(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v6, p0, Lio/realm/RealmSchema;->transaction:Lio/realm/internal/ImplicitTransaction;

    invoke-virtual {v6, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v5

    .line 169
    .local v5, "table":Lio/realm/internal/Table;
    if-eqz v4, :cond_2

    .line 170
    invoke-virtual {v5, v4}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 173
    :cond_2
    new-instance v0, Lio/realm/RealmObjectSchema$DynamicColumnMap;

    invoke-direct {v0, v5}, Lio/realm/RealmObjectSchema$DynamicColumnMap;-><init>(Lio/realm/internal/Table;)V

    .line 174
    .local v0, "columnIndices":Lio/realm/RealmObjectSchema$DynamicColumnMap;
    new-instance v6, Lio/realm/RealmObjectSchema;

    iget-object v7, p0, Lio/realm/RealmSchema;->realm:Lio/realm/BaseRealm;

    invoke-direct {v6, v7, v5, v0}, Lio/realm/RealmObjectSchema;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Table;Ljava/util/Map;)V

    return-object v6
.end method

.method setColumnIndices(Lio/realm/internal/ColumnIndices;)V
    .locals 0
    .param p1, "columnIndices"    # Lio/realm/internal/ColumnIndices;

    .prologue
    .line 258
    iput-object p1, p0, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    .line 259
    return-void
.end method
