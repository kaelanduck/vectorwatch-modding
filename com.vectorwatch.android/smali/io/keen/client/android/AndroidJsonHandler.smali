.class public Lio/keen/client/android/AndroidJsonHandler;
.super Ljava/lang/Object;
.source "AndroidJsonHandler.java"

# interfaces
.implements Lio/keen/client/java/KeenJsonHandler;


# static fields
.field private static final COPY_BUFFER_SIZE:I = 0x1000


# instance fields
.field private isWrapNestedMapsAndCollections:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lio/keen/client/android/AndroidJsonHandler;->isWrapNestedMapsAndCollections:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private convertCollectionToJSONArray(Ljava/util/Collection;)Lorg/json/JSONArray;
    .locals 5
    .param p1, "collection"    # Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    const/4 v1, 0x0

    .line 151
    .local v1, "newCollection":Ljava/util/Collection;
    iget-boolean v4, p0, Lio/keen/client/android/AndroidJsonHandler;->isWrapNestedMapsAndCollections:Z

    if-eqz v4, :cond_2

    invoke-static {p1}, Lio/keen/client/android/AndroidJsonHandler;->requiresWrap(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 152
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "newCollection":Ljava/util/Collection;
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .restart local v1    # "newCollection":Ljava/util/Collection;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 156
    .local v3, "value":Ljava/lang/Object;
    move-object v2, v3

    .line 159
    .local v2, "newValue":Ljava/lang/Object;
    instance-of v4, v3, Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 160
    check-cast v3, Ljava/util/Map;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-direct {p0, v3}, Lio/keen/client/android/AndroidJsonHandler;->convertMapToJSONObject(Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v2

    .line 166
    .end local v2    # "newValue":Ljava/lang/Object;
    :cond_0
    :goto_1
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    .restart local v2    # "newValue":Ljava/lang/Object;
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v4, v3, Ljava/util/Collection;

    if-eqz v4, :cond_0

    .line 162
    check-cast v3, Ljava/util/Collection;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-direct {p0, v3}, Lio/keen/client/android/AndroidJsonHandler;->convertCollectionToJSONArray(Ljava/util/Collection;)Lorg/json/JSONArray;

    move-result-object v2

    .local v2, "newValue":Lorg/json/JSONArray;
    goto :goto_1

    .line 170
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "newValue":Lorg/json/JSONArray;
    :cond_2
    move-object v1, p1

    .line 174
    :cond_3
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    return-object v4
.end method

.method private convertMapToJSONObject(Ljava/util/Map;)Lorg/json/JSONObject;
    .locals 6
    .param p1, "map"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    const/4 v2, 0x0

    .line 110
    .local v2, "newMap":Ljava/util/Map;
    iget-boolean v5, p0, Lio/keen/client/android/AndroidJsonHandler;->isWrapNestedMapsAndCollections:Z

    if-eqz v5, :cond_2

    invoke-static {p1}, Lio/keen/client/android/AndroidJsonHandler;->requiresWrap(Ljava/util/Map;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 111
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "newMap":Ljava/util/Map;
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 114
    .restart local v2    # "newMap":Ljava/util/Map;
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 115
    .local v1, "key":Ljava/lang/Object;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 116
    .local v4, "value":Ljava/lang/Object;
    move-object v3, v4

    .line 119
    .local v3, "newValue":Ljava/lang/Object;
    instance-of v5, v4, Ljava/util/Map;

    if-eqz v5, :cond_1

    .line 120
    check-cast v4, Ljava/util/Map;

    .end local v4    # "value":Ljava/lang/Object;
    invoke-direct {p0, v4}, Lio/keen/client/android/AndroidJsonHandler;->convertMapToJSONObject(Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v3

    .line 126
    .end local v3    # "newValue":Ljava/lang/Object;
    :cond_0
    :goto_1
    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 121
    .restart local v3    # "newValue":Ljava/lang/Object;
    .restart local v4    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v5, v4, Ljava/util/Collection;

    if-eqz v5, :cond_0

    .line 122
    check-cast v4, Ljava/util/Collection;

    .end local v4    # "value":Ljava/lang/Object;
    invoke-direct {p0, v4}, Lio/keen/client/android/AndroidJsonHandler;->convertCollectionToJSONArray(Ljava/util/Collection;)Lorg/json/JSONArray;

    move-result-object v3

    .local v3, "newValue":Lorg/json/JSONArray;
    goto :goto_1

    .line 130
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/Object;
    .end local v3    # "newValue":Lorg/json/JSONArray;
    :cond_2
    move-object v2, p1

    .line 134
    :cond_3
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    return-object v5
.end method

.method private static readerToString(Ljava/io/Reader;)Ljava/lang/String;
    .locals 4
    .param p0, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 188
    .local v2, "writer":Ljava/io/StringWriter;
    const/16 v3, 0x1000

    :try_start_0
    new-array v0, v3, [C

    .line 190
    .local v0, "buffer":[C
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/Reader;->read([C)I

    move-result v1

    .line 191
    .local v1, "bytesRead":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 197
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 199
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    return-object v3

    .line 194
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v2, v0, v3, v1}, Ljava/io/StringWriter;->write([CII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 199
    .end local v0    # "buffer":[C
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v3

    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    throw v3
.end method

.method private static requiresWrap(Ljava/util/Collection;)Z
    .locals 3
    .param p0, "collection"    # Ljava/util/Collection;

    .prologue
    .line 229
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 230
    .local v1, "value":Ljava/lang/Object;
    instance-of v2, v1, Ljava/util/Collection;

    if-nez v2, :cond_1

    instance-of v2, v1, Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 231
    :cond_1
    const/4 v2, 0x1

    .line 234
    .end local v1    # "value":Ljava/lang/Object;
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static requiresWrap(Ljava/util/Map;)Z
    .locals 3
    .param p0, "map"    # Ljava/util/Map;

    .prologue
    .line 212
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 213
    .local v1, "value":Ljava/lang/Object;
    instance-of v2, v1, Ljava/util/Collection;

    if-nez v2, :cond_1

    instance-of v2, v1, Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 214
    :cond_1
    const/4 v2, 0x1

    .line 217
    .end local v1    # "value":Ljava/lang/Object;
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public readJson(Ljava/io/Reader;)Ljava/util/Map;
    .locals 5
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Reader must not be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 40
    :cond_0
    invoke-static {p1}, Lio/keen/client/android/AndroidJsonHandler;->readerToString(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "json":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 43
    .local v2, "jsonObject":Lorg/json/JSONObject;
    invoke-static {v2}, Lio/keen/client/android/JsonHelper;->toMap(Lorg/json/JSONObject;)Ljava/util/Map;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 44
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public setWrapNestedMapsAndCollections(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lio/keen/client/android/AndroidJsonHandler;->isWrapNestedMapsAndCollections:Z

    .line 79
    return-void
.end method

.method public writeJson(Ljava/io/Writer;Ljava/util/Map;)V
    .locals 3
    .param p1, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Writer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "value":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-nez p1, :cond_0

    .line 55
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Writer must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_0
    invoke-direct {p0, p2}, Lio/keen/client/android/AndroidJsonHandler;->convertMapToJSONObject(Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v0

    .line 59
    .local v0, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Ljava/io/Writer;->close()V

    .line 61
    return-void
.end method
