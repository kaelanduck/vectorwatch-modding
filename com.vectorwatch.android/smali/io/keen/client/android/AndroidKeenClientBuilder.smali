.class public Lio/keen/client/android/AndroidKeenClientBuilder;
.super Lio/keen/client/java/KeenClient$Builder;
.source "AndroidKeenClientBuilder.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Lio/keen/client/java/KeenClient$Builder;-><init>()V

    .line 39
    iput-object p1, p0, Lio/keen/client/android/AndroidKeenClientBuilder;->context:Landroid/content/Context;

    .line 40
    return-void
.end method


# virtual methods
.method protected getDefaultEventStore()Lio/keen/client/java/KeenEventStore;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lio/keen/client/java/FileEventStore;

    iget-object v1, p0, Lio/keen/client/android/AndroidKeenClientBuilder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/keen/client/java/FileEventStore;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method protected getDefaultJsonHandler()Lio/keen/client/java/KeenJsonHandler;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lio/keen/client/android/AndroidJsonHandler;

    invoke-direct {v0}, Lio/keen/client/android/AndroidJsonHandler;-><init>()V

    return-object v0
.end method

.method protected getDefaultNetworkStatusHandler()Lio/keen/client/java/KeenNetworkStatusHandler;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lio/keen/client/android/AndroidNetworkStatusHandler;

    iget-object v1, p0, Lio/keen/client/android/AndroidKeenClientBuilder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lio/keen/client/android/AndroidNetworkStatusHandler;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
