.class public Lio/keen/client/android/AndroidNetworkStatusHandler;
.super Ljava/lang/Object;
.source "AndroidNetworkStatusHandler.java"

# interfaces
.implements Lio/keen/client/java/KeenNetworkStatusHandler;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lio/keen/client/android/AndroidNetworkStatusHandler;->context:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public isNetworkConnected()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 30
    iget-object v5, p0, Lio/keen/client/android/AndroidNetworkStatusHandler;->context:Landroid/content/Context;

    const-string v6, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v5, v6}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    move v1, v3

    .line 33
    .local v1, "canCheckNetworkState":Z
    :goto_0
    if-nez v1, :cond_2

    .line 39
    :cond_0
    :goto_1
    return v3

    .end local v1    # "canCheckNetworkState":Z
    :cond_1
    move v1, v4

    .line 30
    goto :goto_0

    .line 36
    .restart local v1    # "canCheckNetworkState":Z
    :cond_2
    iget-object v5, p0, Lio/keen/client/android/AndroidNetworkStatusHandler;->context:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 38
    .local v2, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 39
    .local v0, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method
