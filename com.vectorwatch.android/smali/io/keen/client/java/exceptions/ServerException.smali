.class public Lio/keen/client/java/exceptions/ServerException;
.super Lio/keen/client/java/exceptions/KeenException;
.source "ServerException.java"


# static fields
.field private static final serialVersionUID:J = 0x3650adb883e4c6d6L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lio/keen/client/java/exceptions/KeenException;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lio/keen/client/java/exceptions/KeenException;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lio/keen/client/java/exceptions/KeenException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lio/keen/client/java/exceptions/KeenException;-><init>(Ljava/lang/Throwable;)V

    .line 18
    return-void
.end method
