.class public Lio/keen/client/java/KeenClient;
.super Ljava/lang/Object;
.source "KeenClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/keen/client/java/KeenClient$ClientSingleton;,
        Lio/keen/client/java/KeenClient$Builder;
    }
.end annotation


# static fields
.field private static final ENCODING:Ljava/lang/String; = "UTF-8"

.field private static final ISO_8601_FORMAT:Ljava/text/DateFormat;


# instance fields
.field private final attemptsLock:Ljava/lang/Object;

.field private baseUrl:Ljava/lang/String;

.field private defaultProject:Lio/keen/client/java/KeenProject;

.field private final eventStore:Lio/keen/client/java/KeenEventStore;

.field private globalProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private globalPropertiesEvaluator:Lio/keen/client/java/GlobalPropertiesEvaluator;

.field private final httpHandler:Lio/keen/client/java/http/HttpHandler;

.field private isActive:Z

.field private isDebugMode:Z

.field private final jsonHandler:Lio/keen/client/java/KeenJsonHandler;

.field private maxAttempts:I

.field private final networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

.field private proxy:Ljava/net/Proxy;

.field private final publishExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1126
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lio/keen/client/java/KeenClient;->ISO_8601_FORMAT:Ljava/text/DateFormat;

    return-void
.end method

.method protected constructor <init>(Lio/keen/client/java/KeenClient$Builder;)V
    .locals 1
    .param p1, "builder"    # Lio/keen/client/java/KeenClient$Builder;

    .prologue
    .line 1007
    new-instance v0, Lio/keen/client/java/Environment;

    invoke-direct {v0}, Lio/keen/client/java/Environment;-><init>()V

    invoke-direct {p0, p1, v0}, Lio/keen/client/java/KeenClient;-><init>(Lio/keen/client/java/KeenClient$Builder;Lio/keen/client/java/Environment;)V

    .line 1008
    return-void
.end method

.method constructor <init>(Lio/keen/client/java/KeenClient$Builder;Lio/keen/client/java/Environment;)V
    .locals 2
    .param p1, "builder"    # Lio/keen/client/java/KeenClient$Builder;
    .param p2, "env"    # Lio/keen/client/java/Environment;

    .prologue
    const/4 v1, 0x0

    .line 1019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->attemptsLock:Ljava/lang/Object;

    .line 1138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    .line 1140
    const/4 v0, 0x3

    iput v0, p0, Lio/keen/client/java/KeenClient;->maxAttempts:I

    .line 1021
    # getter for: Lio/keen/client/java/KeenClient$Builder;->httpHandler:Lio/keen/client/java/http/HttpHandler;
    invoke-static {p1}, Lio/keen/client/java/KeenClient$Builder;->access$000(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/http/HttpHandler;

    move-result-object v0

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    .line 1022
    # getter for: Lio/keen/client/java/KeenClient$Builder;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;
    invoke-static {p1}, Lio/keen/client/java/KeenClient$Builder;->access$100(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/KeenJsonHandler;

    move-result-object v0

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    .line 1023
    # getter for: Lio/keen/client/java/KeenClient$Builder;->eventStore:Lio/keen/client/java/KeenEventStore;
    invoke-static {p1}, Lio/keen/client/java/KeenClient$Builder;->access$200(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/KeenEventStore;

    move-result-object v0

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    .line 1024
    # getter for: Lio/keen/client/java/KeenClient$Builder;->publishExecutor:Ljava/util/concurrent/Executor;
    invoke-static {p1}, Lio/keen/client/java/KeenClient$Builder;->access$300(Lio/keen/client/java/KeenClient$Builder;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->publishExecutor:Ljava/util/concurrent/Executor;

    .line 1025
    # getter for: Lio/keen/client/java/KeenClient$Builder;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;
    invoke-static {p1}, Lio/keen/client/java/KeenClient$Builder;->access$400(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/KeenNetworkStatusHandler;

    move-result-object v0

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

    .line 1028
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/keen/client/java/KeenClient;->publishExecutor:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_1

    .line 1030
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/keen/client/java/KeenClient;->setActive(Z)V

    .line 1034
    :cond_1
    const-string v0, "https://api.keen.io"

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->baseUrl:Ljava/lang/String;

    .line 1035
    iput-object v1, p0, Lio/keen/client/java/KeenClient;->globalPropertiesEvaluator:Lio/keen/client/java/GlobalPropertiesEvaluator;

    .line 1036
    iput-object v1, p0, Lio/keen/client/java/KeenClient;->globalProperties:Ljava/util/Map;

    .line 1039
    invoke-virtual {p2}, Lio/keen/client/java/Environment;->getKeenProjectId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1040
    new-instance v0, Lio/keen/client/java/KeenProject;

    invoke-direct {v0, p2}, Lio/keen/client/java/KeenProject;-><init>(Lio/keen/client/java/Environment;)V

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    .line 1042
    :cond_2
    return-void
.end method

.method static synthetic access$500(Lio/keen/client/java/KeenClient;)Lio/keen/client/java/KeenJsonHandler;
    .locals 1
    .param p0, "x0"    # Lio/keen/client/java/KeenClient;

    .prologue
    .line 54
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    return-object v0
.end method

.method private buildEventMap(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;
    .locals 18
    .param p1, "projectId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1243
    .local p2, "eventHandles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 1245
    .local v14, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1246
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1247
    .local v6, "eventCollection":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 1250
    .local v10, "handles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v15

    if-eqz v15, :cond_0

    .line 1255
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v15

    invoke-direct {v7, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 1258
    .local v7, "events":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    instance-of v15, v15, Lio/keen/client/java/KeenAttemptCountingEventStore;

    if-eqz v15, :cond_5

    .line 1259
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/keen/client/java/KeenClient;->attemptsLock:Ljava/lang/Object;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 1261
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lio/keen/client/java/KeenClient;->getAttemptsMap(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1270
    .local v2, "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :goto_1
    :try_start_1
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 1271
    .local v9, "handle":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lio/keen/client/java/KeenClient;->getEvent(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    .line 1273
    .local v5, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/lang/Object;->hashCode()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1274
    .local v3, "attemptsKey":Ljava/lang/String;
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 1275
    .local v13, "remainingAttempts":Ljava/lang/Integer;
    if-nez v13, :cond_1

    .line 1277
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 1281
    :cond_1
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 1282
    invoke-interface {v2, v3, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1284
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-ltz v15, :cond_2

    .line 1286
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1303
    .end local v2    # "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "attemptsKey":Ljava/lang/String;
    .end local v5    # "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v9    # "handle":Ljava/lang/Object;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "remainingAttempts":Ljava/lang/Integer;
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v15

    .line 1262
    :catch_0
    move-exception v8

    .line 1265
    .local v8, "ex":Ljava/io/IOException;
    :try_start_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1266
    .restart local v2    # "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Failed to read attempt counts map. Events will still be POSTed. Exception: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 1289
    .end local v8    # "ex":Ljava/io/IOException;
    .restart local v3    # "attemptsKey":Ljava/lang/String;
    .restart local v5    # "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v9    # "handle":Ljava/lang/Object;
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "remainingAttempts":Ljava/lang/Integer;
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    invoke-interface {v15, v9}, Lio/keen/client/java/KeenEventStore;->remove(Ljava/lang/Object;)V

    .line 1293
    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1298
    .end local v3    # "attemptsKey":Ljava/lang/String;
    .end local v5    # "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v9    # "handle":Ljava/lang/Object;
    .end local v13    # "remainingAttempts":Ljava/lang/Integer;
    :cond_3
    :try_start_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v2}, Lio/keen/client/java/KeenClient;->setAttemptsMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1303
    :goto_3
    :try_start_4
    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1311
    .end local v2    # "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_4
    invoke-interface {v14, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1299
    .restart local v2    # "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catch_1
    move-exception v8

    .line 1300
    .restart local v8    # "ex":Ljava/io/IOException;
    :try_start_5
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Failed to update event POST attempts counts while sending queued events. Events will still be POSTed. Exception: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 1305
    .end local v2    # "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v8    # "ex":Ljava/io/IOException;
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 1306
    .restart local v9    # "handle":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lio/keen/client/java/KeenClient;->getEvent(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v15

    invoke-interface {v7, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1313
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    .end local v6    # "eventCollection":Ljava/lang/String;
    .end local v7    # "events":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v9    # "handle":Ljava/lang/Object;
    .end local v10    # "handles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_6
    return-object v14
.end method

.method public static client()Lio/keen/client/java/KeenClient;
    .locals 2

    .prologue
    .line 64
    sget-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    iget-object v0, v0, Lio/keen/client/java/KeenClient$ClientSingleton;->client:Lio/keen/client/java/KeenClient;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Please call KeenClient.initialize() before requesting the client."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    sget-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    iget-object v0, v0, Lio/keen/client/java/KeenClient$ClientSingleton;->client:Lio/keen/client/java/KeenClient;

    return-object v0
.end method

.method private getAttemptsMap(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 9
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1650
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1651
    .local v1, "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v7, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    instance-of v7, v7, Lio/keen/client/java/KeenAttemptCountingEventStore;

    if-eqz v7, :cond_1

    .line 1652
    iget-object v6, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    check-cast v6, Lio/keen/client/java/KeenAttemptCountingEventStore;

    .line 1653
    .local v6, "res":Lio/keen/client/java/KeenAttemptCountingEventStore;
    invoke-interface {v6, p1, p2}, Lio/keen/client/java/KeenAttemptCountingEventStore;->getAttempts(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1654
    .local v2, "attemptsJSON":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1655
    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1656
    .local v5, "reader":Ljava/io/StringReader;
    iget-object v7, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    invoke-interface {v7, v5}, Lio/keen/client/java/KeenJsonHandler;->readJson(Ljava/io/Reader;)Ljava/util/Map;

    move-result-object v0

    .line 1657
    .local v0, "attemptTmp":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1658
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/lang/Number;

    if-eqz v7, :cond_0

    .line 1659
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1665
    .end local v0    # "attemptTmp":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "attemptsJSON":Ljava/lang/String;
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "reader":Ljava/io/StringReader;
    .end local v6    # "res":Lio/keen/client/java/KeenAttemptCountingEventStore;
    :cond_1
    return-object v1
.end method

.method private getEvent(Ljava/lang/Object;)Ljava/util/Map;
    .locals 4
    .param p1, "handle"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1632
    iget-object v3, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    invoke-interface {v3, p1}, Lio/keen/client/java/KeenEventStore;->get(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1635
    .local v1, "jsonEvent":Ljava/lang/String;
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1636
    .local v2, "reader":Ljava/io/StringReader;
    iget-object v3, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    invoke-interface {v3, v2}, Lio/keen/client/java/KeenJsonHandler;->readJson(Ljava/io/Reader;)Ljava/util/Map;

    move-result-object v0

    .line 1637
    .local v0, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {v2}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 1638
    return-object v0
.end method

.method private handleAddEventsResponse(Ljava/util/Map;Ljava/lang/String;)V
    .locals 22
    .param p2, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1441
    .local p1, "handles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    new-instance v14, Ljava/io/StringReader;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1443
    .local v14, "reader":Ljava/io/StringReader;
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Lio/keen/client/java/KeenJsonHandler;->readJson(Ljava/io/Reader;)Ljava/util/Map;

    move-result-object v16

    .line 1449
    .local v16, "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1450
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1453
    .local v2, "collectionName":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1456
    .local v1, "collectionHandles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 1457
    .local v9, "eventResults":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    const/4 v13, 0x0

    .line 1458
    .local v13, "index":I
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map;

    .line 1460
    .local v8, "eventResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v15, 0x1

    .line 1461
    .local v15, "removeCacheEntry":Z
    const-string v18, "success"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Boolean;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    .line 1462
    .local v17, "success":Z
    if-nez v17, :cond_2

    .line 1464
    const-string v18, "error"

    move-object/from16 v0, v18

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    .line 1465
    .local v7, "errorDict":Ljava/util/Map;
    const-string v18, "name"

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1466
    .local v6, "errorCode":Ljava/lang/String;
    const-string v18, "InvalidCollectionNameError"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "InvalidPropertyNameError"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "InvalidPropertyValueError"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1469
    :cond_1
    const/4 v15, 0x1

    .line 1470
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "An invalid event was found. Deleting it. Error: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "description"

    move-object/from16 v0, v19

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1484
    .end local v6    # "errorCode":Ljava/lang/String;
    .end local v7    # "errorDict":Ljava/util/Map;
    :cond_2
    :goto_1
    if-eqz v15, :cond_3

    .line 1485
    invoke-interface {v1, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    .line 1489
    .local v10, "handle":Ljava/lang/Object;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Lio/keen/client/java/KeenEventStore;->remove(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1494
    .end local v10    # "handle":Ljava/lang/Object;
    :cond_3
    :goto_2
    add-int/lit8 v13, v13, 0x1

    .line 1495
    goto/16 :goto_0

    .line 1473
    .restart local v6    # "errorCode":Ljava/lang/String;
    .restart local v7    # "errorDict":Ljava/util/Map;
    :cond_4
    const-string v18, "description"

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1474
    .local v3, "description":Ljava/lang/String;
    const/4 v15, 0x0

    .line 1475
    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v19, "The event could not be inserted for some reason. Error name and description: %s %s"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v6, v20, v21

    const/16 v21, 0x1

    aput-object v3, v20, v21

    invoke-static/range {v18 .. v20}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 1490
    .end local v3    # "description":Ljava/lang/String;
    .end local v6    # "errorCode":Ljava/lang/String;
    .end local v7    # "errorDict":Ljava/util/Map;
    .restart local v10    # "handle":Ljava/lang/Object;
    :catch_0
    move-exception v4

    .line 1491
    .local v4, "e":Ljava/io/IOException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Failed to remove object \'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\' from cache"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_2

    .line 1497
    .end local v1    # "collectionHandles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v2    # "collectionName":Ljava/lang/String;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8    # "eventResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v9    # "eventResults":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v10    # "handle":Ljava/lang/Object;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "index":I
    .end local v15    # "removeCacheEntry":Z
    .end local v17    # "success":Z
    :cond_5
    return-void
.end method

.method private handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V
    .locals 7
    .param p1, "callback"    # Lio/keen/client/java/KeenCallback;
    .param p2, "project"    # Lio/keen/client/java/KeenProject;
    .param p3, "eventCollection"    # Ljava/lang/String;
    .param p6, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenCallback;",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1590
    .local p4, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p5, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-boolean v1, p0, Lio/keen/client/java/KeenClient;->isDebugMode:Z

    if-eqz v1, :cond_1

    .line 1591
    instance-of v1, p6, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    .line 1592
    check-cast p6, Ljava/lang/RuntimeException;

    .end local p6    # "e":Ljava/lang/Exception;
    throw p6

    .line 1594
    .restart local p6    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1597
    :cond_1
    invoke-direct {p0, p1, p6}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V

    .line 1599
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Encountered error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1600
    if-eqz p1, :cond_2

    .line 1602
    :try_start_0
    instance-of v1, p1, Lio/keen/client/java/KeenDetailedCallback;

    if-eqz v1, :cond_2

    .line 1603
    move-object v0, p1

    check-cast v0, Lio/keen/client/java/KeenDetailedCallback;

    move-object v1, v0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v1 .. v6}, Lio/keen/client/java/KeenDetailedCallback;->onFailure(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1610
    :cond_2
    :goto_0
    return-void

    .line 1605
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "callback"    # Lio/keen/client/java/KeenCallback;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 1553
    iget-boolean v0, p0, Lio/keen/client/java/KeenClient;->isDebugMode:Z

    if-eqz v0, :cond_1

    .line 1554
    instance-of v0, p2, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 1555
    check-cast p2, Ljava/lang/RuntimeException;

    .end local p2    # "e":Ljava/lang/Exception;
    throw p2

    .line 1557
    .restart local p2    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 1560
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Encountered error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1561
    if-eqz p1, :cond_2

    .line 1563
    :try_start_0
    invoke-interface {p1, p2}, Lio/keen/client/java/KeenCallback;->onFailure(Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1569
    :cond_2
    :goto_0
    return-void

    .line 1564
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private handleLibraryInactive(Lio/keen/client/java/KeenCallback;)V
    .locals 2
    .param p1, "callback"    # Lio/keen/client/java/KeenCallback;

    .prologue
    .line 1619
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The Keen library failed to initialize properly and is inactive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V

    .line 1621
    return-void
.end method

.method private handleSuccess(Lio/keen/client/java/KeenCallback;)V
    .locals 1
    .param p1, "callback"    # Lio/keen/client/java/KeenCallback;

    .prologue
    .line 1506
    if-eqz p1, :cond_0

    .line 1508
    :try_start_0
    invoke-interface {p1}, Lio/keen/client/java/KeenCallback;->onSuccess()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1513
    :cond_0
    :goto_0
    return-void

    .line 1509
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private handleSuccess(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .param p1, "callback"    # Lio/keen/client/java/KeenCallback;
    .param p2, "project"    # Lio/keen/client/java/KeenProject;
    .param p3, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenCallback;",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1531
    .local p4, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p5, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1}, Lio/keen/client/java/KeenClient;->handleSuccess(Lio/keen/client/java/KeenCallback;)V

    .line 1532
    if-eqz p1, :cond_0

    .line 1534
    :try_start_0
    instance-of v0, p1, Lio/keen/client/java/KeenDetailedCallback;

    if-eqz v0, :cond_0

    .line 1535
    check-cast p1, Lio/keen/client/java/KeenDetailedCallback;

    .end local p1    # "callback":Lio/keen/client/java/KeenCallback;
    invoke-interface {p1, p2, p3, p4, p5}, Lio/keen/client/java/KeenDetailedCallback;->onSuccess(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1541
    :cond_0
    :goto_0
    return-void

    .line 1537
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static initialize(Lio/keen/client/java/KeenClient;)V
    .locals 2
    .param p0, "client"    # Lio/keen/client/java/KeenClient;

    .prologue
    .line 78
    if-nez p0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Client must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    sget-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    iget-object v0, v0, Lio/keen/client/java/KeenClient$ClientSingleton;->client:Lio/keen/client/java/KeenClient;

    if-eqz v0, :cond_1

    .line 88
    :goto_0
    return-void

    .line 87
    :cond_1
    sget-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    iput-object p0, v0, Lio/keen/client/java/KeenClient$ClientSingleton;->client:Lio/keen/client/java/KeenClient;

    goto :goto_0
.end method

.method public static isInitialized()Z
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    iget-object v0, v0, Lio/keen/client/java/KeenClient$ClientSingleton;->client:Lio/keen/client/java/KeenClient;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNetworkConnected()Z
    .locals 1

    .prologue
    .line 1420
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

    invoke-interface {v0}, Lio/keen/client/java/KeenNetworkStatusHandler;->isNetworkConnected()Z

    move-result v0

    return v0
.end method

.method private publish(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1328
    .local p3, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s/%s/projects/%s/events/%s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lio/keen/client/java/KeenClient;->getBaseUrl()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "3.0"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lio/keen/client/java/KeenProject;->getProjectId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object p2, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1330
    .local v1, "urlString":Ljava/lang/String;
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1331
    .local v0, "url":Ljava/net/URL;
    invoke-direct {p0, p1, v0, p3}, Lio/keen/client/java/KeenClient;->publishObject(Lio/keen/client/java/KeenProject;Ljava/net/URL;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private publishAll(Lio/keen/client/java/KeenProject;Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1345
    .local p2, "events":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;>;"
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s/%s/projects/%s/events"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lio/keen/client/java/KeenClient;->getBaseUrl()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "3.0"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lio/keen/client/java/KeenProject;->getProjectId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1347
    .local v1, "urlString":Ljava/lang/String;
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1348
    .local v0, "url":Ljava/net/URL;
    invoke-direct {p0, p1, v0, p2}, Lio/keen/client/java/KeenClient;->publishObject(Lio/keen/client/java/KeenProject;Ljava/net/URL;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private declared-synchronized publishObject(Lio/keen/client/java/KeenProject;Ljava/net/URL;Ljava/util/Map;)Ljava/lang/String;
    .locals 11
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/net/URL;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1366
    .local p3, "requestData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    monitor-enter p0

    if-eqz p3, :cond_0

    :try_start_0
    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1367
    :cond_0
    const-string v1, "No API calls were made because there were no events to upload"

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1368
    const/4 v1, 0x0

    .line 1408
    :goto_0
    monitor-exit p0

    return-object v1

    .line 1372
    :cond_1
    :try_start_1
    new-instance v4, Lio/keen/client/java/KeenClient$3;

    invoke-direct {v4, p0, p3}, Lio/keen/client/java/KeenClient$3;-><init>(Lio/keen/client/java/KeenClient;Ljava/util/Map;)V

    .line 1381
    .local v4, "source":Lio/keen/client/java/http/OutputSource;
    invoke-static {}, Lio/keen/client/java/KeenLogging;->isLoggingEnabled()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 1383
    :try_start_2
    new-instance v8, Ljava/io/StringWriter;

    invoke-direct {v8}, Ljava/io/StringWriter;-><init>()V

    .line 1384
    .local v8, "writer":Ljava/io/StringWriter;
    iget-object v1, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    invoke-interface {v1, v8, p3}, Lio/keen/client/java/KeenJsonHandler;->writeJson(Ljava/io/Writer;Ljava/util/Map;)V

    .line 1385
    invoke-virtual {v8}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1386
    .local v0, "request":Ljava/lang/String;
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Sent request \'%s\' to URL \'%s\'"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v5, v9

    const/4 v9, 0x1

    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    invoke-static {v1, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1395
    .end local v0    # "request":Ljava/lang/String;
    .end local v8    # "writer":Ljava/io/StringWriter;
    :cond_2
    :goto_1
    :try_start_3
    invoke-virtual {p1}, Lio/keen/client/java/KeenProject;->getWriteKey()Ljava/lang/String;

    move-result-object v3

    .line 1396
    .local v3, "writeKey":Ljava/lang/String;
    new-instance v0, Lio/keen/client/java/http/Request;

    const-string v2, "POST"

    iget-object v5, p0, Lio/keen/client/java/KeenClient;->proxy:Ljava/net/Proxy;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lio/keen/client/java/http/Request;-><init>(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Lio/keen/client/java/http/OutputSource;Ljava/net/Proxy;)V

    .line 1397
    .local v0, "request":Lio/keen/client/java/http/Request;
    iget-object v1, p0, Lio/keen/client/java/KeenClient;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    invoke-interface {v1, v0}, Lio/keen/client/java/http/HttpHandler;->execute(Lio/keen/client/java/http/Request;)Lio/keen/client/java/http/Response;

    move-result-object v7

    .line 1400
    .local v7, "response":Lio/keen/client/java/http/Response;
    invoke-static {}, Lio/keen/client/java/KeenLogging;->isLoggingEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1401
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Received response: \'%s\' (%d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v7, Lio/keen/client/java/http/Response;->body:Ljava/lang/String;

    aput-object v10, v5, v9

    const/4 v9, 0x1

    iget v10, v7, Lio/keen/client/java/http/Response;->statusCode:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v9

    invoke-static {v1, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1407
    :cond_3
    invoke-virtual {v7}, Lio/keen/client/java/http/Response;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1408
    iget-object v1, v7, Lio/keen/client/java/http/Response;->body:Ljava/lang/String;

    goto :goto_0

    .line 1388
    .end local v0    # "request":Lio/keen/client/java/http/Request;
    .end local v3    # "writeKey":Ljava/lang/String;
    .end local v7    # "response":Lio/keen/client/java/http/Response;
    :catch_0
    move-exception v6

    .line 1389
    .local v6, "e":Ljava/io/IOException;
    const-string v1, "Couldn\'t log event written to file: "

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1390
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1366
    .end local v4    # "source":Lio/keen/client/java/http/OutputSource;
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1410
    .restart local v0    # "request":Lio/keen/client/java/http/Request;
    .restart local v3    # "writeKey":Ljava/lang/String;
    .restart local v4    # "source":Lio/keen/client/java/http/OutputSource;
    .restart local v7    # "response":Lio/keen/client/java/http/Response;
    :cond_4
    :try_start_4
    new-instance v1, Lio/keen/client/java/exceptions/ServerException;

    iget-object v2, v7, Lio/keen/client/java/http/Response;->body:Ljava/lang/String;

    invoke-direct {v1, v2}, Lio/keen/client/java/exceptions/ServerException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private setAttemptsMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 5
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1677
    .local p3, "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v4, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    instance-of v4, v4, Lio/keen/client/java/KeenAttemptCountingEventStore;

    if-eqz v4, :cond_0

    .line 1678
    iget-object v1, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    check-cast v1, Lio/keen/client/java/KeenAttemptCountingEventStore;

    .line 1679
    .local v1, "res":Lio/keen/client/java/KeenAttemptCountingEventStore;
    const/4 v2, 0x0

    .line 1681
    .local v2, "writer":Ljava/io/StringWriter;
    :try_start_0
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1682
    .end local v2    # "writer":Ljava/io/StringWriter;
    .local v3, "writer":Ljava/io/StringWriter;
    :try_start_1
    iget-object v4, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    invoke-interface {v4, v3, p3}, Lio/keen/client/java/KeenJsonHandler;->writeJson(Ljava/io/Writer;Ljava/util/Map;)V

    .line 1683
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1684
    .local v0, "attemptsJSON":Ljava/lang/String;
    invoke-interface {v1, p1, p2, v0}, Lio/keen/client/java/KeenAttemptCountingEventStore;->setAttempts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1686
    invoke-static {v3}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 1689
    .end local v0    # "attemptsJSON":Ljava/lang/String;
    .end local v1    # "res":Lio/keen/client/java/KeenAttemptCountingEventStore;
    .end local v3    # "writer":Ljava/io/StringWriter;
    :cond_0
    return-void

    .line 1686
    .restart local v1    # "res":Lio/keen/client/java/KeenAttemptCountingEventStore;
    .restart local v2    # "writer":Ljava/io/StringWriter;
    :catchall_0
    move-exception v4

    :goto_0
    invoke-static {v2}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v4

    .end local v2    # "writer":Ljava/io/StringWriter;
    .restart local v3    # "writer":Ljava/io/StringWriter;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/StringWriter;
    .restart local v2    # "writer":Ljava/io/StringWriter;
    goto :goto_0
.end method

.method private validateEvent(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1170
    .local p1, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lio/keen/client/java/KeenClient;->validateEvent(Ljava/util/Map;I)V

    .line 1171
    return-void
.end method

.method private validateEvent(Ljava/util/Map;I)V
    .locals 5
    .param p2, "depth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1182
    .local p1, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p2, :cond_2

    .line 1183
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 1184
    :cond_0
    new-instance v3, Lio/keen/client/java/exceptions/InvalidEventException;

    const-string v4, "You must specify a non-null, non-empty event."

    invoke-direct {v3, v4}, Lio/keen/client/java/exceptions/InvalidEventException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1186
    :cond_1
    const-string v3, "keen"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1187
    new-instance v3, Lio/keen/client/java/exceptions/InvalidEventException;

    const-string v4, "An event cannot contain a root-level property named \'keen\'."

    invoke-direct {v3, v4}, Lio/keen/client/java/exceptions/InvalidEventException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1189
    :cond_2
    const/16 v3, 0x3e8

    if-le p2, v3, :cond_3

    .line 1190
    new-instance v3, Lio/keen/client/java/exceptions/InvalidEventException;

    const-string v4, "An event\'s depth (i.e. layers of nesting) cannot exceed 1000"

    invoke-direct {v3, v4}, Lio/keen/client/java/exceptions/InvalidEventException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1194
    :cond_3
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1195
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1196
    .local v2, "key":Ljava/lang/String;
    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1197
    new-instance v3, Lio/keen/client/java/exceptions/InvalidEventException;

    const-string v4, "An event cannot contain a property with the period (.) character in it."

    invoke-direct {v3, v4}, Lio/keen/client/java/exceptions/InvalidEventException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1200
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x100

    if-le v3, v4, :cond_5

    .line 1201
    new-instance v3, Lio/keen/client/java/exceptions/InvalidEventException;

    const-string v4, "An event cannot contain a property name longer than 256 characters."

    invoke-direct {v3, v4}, Lio/keen/client/java/exceptions/InvalidEventException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1204
    :cond_5
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lio/keen/client/java/KeenClient;->validateEventValue(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1206
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "key":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method private validateEventCollection(Ljava/lang/String;)V
    .locals 3
    .param p1, "eventCollection"    # Ljava/lang/String;

    .prologue
    .line 1156
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1157
    :cond_0
    new-instance v0, Lio/keen/client/java/exceptions/InvalidEventCollectionException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "You must specify a non-null, non-empty event collection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/keen/client/java/exceptions/InvalidEventCollectionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1160
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x100

    if-le v0, v1, :cond_2

    .line 1161
    new-instance v0, Lio/keen/client/java/exceptions/InvalidEventCollectionException;

    const-string v1, "An event collection name cannot be longer than 256 characters."

    invoke-direct {v0, v1}, Lio/keen/client/java/exceptions/InvalidEventCollectionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1163
    :cond_2
    return-void
.end method

.method private validateEventValue(Ljava/lang/Object;I)V
    .locals 5
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "depth"    # I

    .prologue
    .line 1217
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 1218
    check-cast v2, Ljava/lang/String;

    .line 1219
    .local v2, "strValue":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x2710

    if-lt v3, v4, :cond_1

    .line 1220
    new-instance v3, Lio/keen/client/java/exceptions/InvalidEventException;

    const-string v4, "An event cannot contain a string property value longer than 10,000 characters."

    invoke-direct {v3, v4}, Lio/keen/client/java/exceptions/InvalidEventException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1223
    .end local v2    # "strValue":Ljava/lang/String;
    :cond_0
    instance-of v3, p1, Ljava/util/Map;

    if-eqz v3, :cond_2

    .line 1224
    check-cast p1, Ljava/util/Map;

    .end local p1    # "value":Ljava/lang/Object;
    add-int/lit8 v3, p2, 0x1

    invoke-direct {p0, p1, v3}, Lio/keen/client/java/KeenClient;->validateEvent(Ljava/util/Map;I)V

    .line 1230
    :cond_1
    return-void

    .line 1225
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v3, p1, Ljava/lang/Iterable;

    if-eqz v3, :cond_1

    .line 1226
    check-cast p1, Ljava/lang/Iterable;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1227
    .local v1, "listElement":Ljava/lang/Object;
    invoke-direct {p0, v1, p2}, Lio/keen/client/java/KeenClient;->validateEventValue(Ljava/lang/Object;I)V

    goto :goto_0
.end method


# virtual methods
.method public addEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V
    .locals 9
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .param p5, "callback"    # Lio/keen/client/java/KeenCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lio/keen/client/java/KeenCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    .local p3, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p4, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    if-nez v0, :cond_0

    .line 146
    invoke-direct {p0, p5}, Lio/keen/client/java/KeenClient;->handleLibraryInactive(Lio/keen/client/java/KeenCallback;)V

    .line 168
    :goto_0
    return-void

    .line 150
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    if-nez v0, :cond_1

    .line 151
    const/4 v1, 0x0

    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v0, "No project specified, but no default project found"

    invoke-direct {v6, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0

    .line 155
    :cond_1
    if-nez p1, :cond_2

    iget-object v8, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    .line 159
    .local v8, "useProject":Lio/keen/client/java/KeenProject;
    :goto_1
    :try_start_0
    invoke-virtual {p0, v8, p2, p3, p4}, Lio/keen/client/java/KeenClient;->validateAndBuildEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v7

    .line 163
    .local v7, "newEvent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, v8, p2, v7}, Lio/keen/client/java/KeenClient;->publish(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 164
    invoke-direct/range {v0 .. v5}, Lio/keen/client/java/KeenClient;->handleSuccess(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    .end local v7    # "newEvent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v6

    .local v6, "e":Ljava/lang/Exception;
    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 166
    invoke-direct/range {v0 .. v6}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0

    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "useProject":Lio/keen/client/java/KeenProject;
    :cond_2
    move-object v8, p1

    .line 155
    goto :goto_1
.end method

.method public addEvent(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/keen/client/java/KeenClient;->addEvent(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 111
    return-void
.end method

.method public addEvent(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 6
    .param p1, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p3, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .line 125
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lio/keen/client/java/KeenClient;->addEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V

    .line 126
    return-void
.end method

.method public addEventAsync(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V
    .locals 10
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .param p5, "callback"    # Lio/keen/client/java/KeenCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lio/keen/client/java/KeenCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216
    .local p3, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p4, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    if-nez v0, :cond_0

    .line 217
    invoke-direct {p0, p5}, Lio/keen/client/java/KeenClient;->handleLibraryInactive(Lio/keen/client/java/KeenCallback;)V

    .line 239
    :goto_0
    return-void

    .line 221
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    if-nez v0, :cond_1

    .line 222
    const/4 v1, 0x0

    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v0, "No project specified, but no default project found"

    invoke-direct {v6, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0

    .line 225
    :cond_1
    if-nez p1, :cond_2

    iget-object v2, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    .line 230
    .local v2, "useProject":Lio/keen/client/java/KeenProject;
    :goto_1
    :try_start_0
    iget-object v7, p0, Lio/keen/client/java/KeenClient;->publishExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lio/keen/client/java/KeenClient$1;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lio/keen/client/java/KeenClient$1;-><init>(Lio/keen/client/java/KeenClient;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/lang/Exception;
    move-object v3, p0

    move-object v4, p5

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    .line 237
    invoke-direct/range {v3 .. v9}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0

    .end local v2    # "useProject":Lio/keen/client/java/KeenProject;
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v2, p1

    .line 225
    goto :goto_1
.end method

.method public addEventAsync(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p2, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/keen/client/java/KeenClient;->addEventAsync(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 180
    return-void
.end method

.method public addEventAsync(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 6
    .param p1, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p3, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .line 194
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lio/keen/client/java/KeenClient;->addEventAsync(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V

    .line 195
    return-void
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->baseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultProject()Lio/keen/client/java/KeenProject;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    return-object v0
.end method

.method public getEventStore()Lio/keen/client/java/KeenEventStore;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    return-object v0
.end method

.method public getGlobalProperties()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->globalProperties:Ljava/util/Map;

    return-object v0
.end method

.method public getGlobalPropertiesEvaluator()Lio/keen/client/java/GlobalPropertiesEvaluator;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->globalPropertiesEvaluator:Lio/keen/client/java/GlobalPropertiesEvaluator;

    return-object v0
.end method

.method public getJsonHandler()Lio/keen/client/java/KeenJsonHandler;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    return-object v0
.end method

.method public getMaxAttempts()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lio/keen/client/java/KeenClient;->maxAttempts:I

    return v0
.end method

.method public getProxy()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->proxy:Ljava/net/Proxy;

    return-object v0
.end method

.method public getPublishExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lio/keen/client/java/KeenClient;->publishExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 660
    iget-boolean v0, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    return v0
.end method

.method public isDebugMode()Z
    .locals 1

    .prologue
    .line 640
    iget-boolean v0, p0, Lio/keen/client/java/KeenClient;->isDebugMode:Z

    return v0
.end method

.method public queueEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V
    .locals 19
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .param p5, "callback"    # Lio/keen/client/java/KeenCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lio/keen/client/java/KeenCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    .local p3, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p4, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lio/keen/client/java/KeenClient;->isActive:Z

    if-nez v5, :cond_0

    .line 287
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lio/keen/client/java/KeenClient;->handleLibraryInactive(Lio/keen/client/java/KeenCallback;)V

    .line 327
    :goto_0
    return-void

    .line 291
    :cond_0
    if-nez p1, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    if-nez v5, :cond_1

    .line 292
    const/4 v6, 0x0

    new-instance v11, Ljava/lang/IllegalStateException;

    const-string v5, "No project specified, but no default project found"

    invoke-direct {v11, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object/from16 v5, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    invoke-direct/range {v5 .. v11}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0

    .line 295
    :cond_1
    if-nez p1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    move-object/from16 v17, v0

    .line 299
    .local v17, "useProject":Lio/keen/client/java/KeenProject;
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lio/keen/client/java/KeenClient;->validateAndBuildEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v16

    .line 303
    .local v16, "newEvent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v18, Ljava/io/StringWriter;

    invoke-direct/range {v18 .. v18}, Ljava/io/StringWriter;-><init>()V

    .line 304
    .local v18, "writer":Ljava/io/StringWriter;
    move-object/from16 v0, p0

    iget-object v5, v0, Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-interface {v5, v0, v1}, Lio/keen/client/java/KeenJsonHandler;->writeJson(Ljava/io/Writer;Ljava/util/Map;)V

    .line 305
    invoke-virtual/range {v18 .. v18}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v15

    .line 306
    .local v15, "jsonEvent":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    invoke-virtual/range {v17 .. v17}, Lio/keen/client/java/KeenProject;->getProjectId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-interface {v5, v6, v0, v15}, Lio/keen/client/java/KeenEventStore;->store(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    .line 312
    .local v14, "handle":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v5, v0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    instance-of v5, v5, Lio/keen/client/java/KeenAttemptCountingEventStore;

    if-eqz v5, :cond_2

    .line 313
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/keen/client/java/KeenClient;->attemptsLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 314
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Lio/keen/client/java/KeenProject;->getProjectId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v5, v1}, Lio/keen/client/java/KeenClient;->getAttemptsMap(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v12

    .line 315
    .local v12, "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget v7, v0, Lio/keen/client/java/KeenClient;->maxAttempts:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v12, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    invoke-virtual/range {v17 .. v17}, Lio/keen/client/java/KeenProject;->getProjectId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v5, v1, v12}, Lio/keen/client/java/KeenClient;->setAttemptsMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 317
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v12    # "attempts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v14    # "handle":Ljava/lang/Object;
    :cond_2
    :goto_2
    move-object/from16 v5, p0

    move-object/from16 v6, p5

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    .line 323
    :try_start_3
    invoke-direct/range {v5 .. v10}, Lio/keen/client/java/KeenClient;->handleSuccess(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 324
    .end local v15    # "jsonEvent":Ljava/lang/String;
    .end local v16    # "newEvent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v18    # "writer":Ljava/io/StringWriter;
    :catch_0
    move-exception v11

    .local v11, "e":Ljava/lang/Exception;
    move-object/from16 v5, p0

    move-object/from16 v6, p5

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    .line 325
    invoke-direct/range {v5 .. v11}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .end local v11    # "e":Ljava/lang/Exception;
    .end local v17    # "useProject":Lio/keen/client/java/KeenProject;
    :cond_3
    move-object/from16 v17, p1

    .line 295
    goto/16 :goto_1

    .line 317
    .restart local v14    # "handle":Ljava/lang/Object;
    .restart local v15    # "jsonEvent":Ljava/lang/String;
    .restart local v16    # "newEvent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v17    # "useProject":Lio/keen/client/java/KeenProject;
    .restart local v18    # "writer":Ljava/io/StringWriter;
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 319
    .end local v14    # "handle":Ljava/lang/Object;
    :catch_1
    move-exception v13

    .line 320
    .local v13, "ex":Ljava/io/IOException;
    :try_start_6
    const-string v5, "Failed to set the event POST attempt count. The event was still queued and will we POSTed."

    invoke-static {v5}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_2
.end method

.method public queueEvent(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 250
    .local p2, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/keen/client/java/KeenClient;->queueEvent(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 251
    return-void
.end method

.method public queueEvent(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 6
    .param p1, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p3, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .line 265
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lio/keen/client/java/KeenClient;->queueEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lio/keen/client/java/KeenCallback;)V

    .line 266
    return-void
.end method

.method public sendQueuedEvents()V
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/keen/client/java/KeenClient;->sendQueuedEvents(Lio/keen/client/java/KeenProject;)V

    .line 336
    return-void
.end method

.method public sendQueuedEvents(Lio/keen/client/java/KeenProject;)V
    .locals 1
    .param p1, "project"    # Lio/keen/client/java/KeenProject;

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lio/keen/client/java/KeenClient;->sendQueuedEvents(Lio/keen/client/java/KeenProject;Lio/keen/client/java/KeenCallback;)V

    .line 348
    return-void
.end method

.method public declared-synchronized sendQueuedEvents(Lio/keen/client/java/KeenProject;Lio/keen/client/java/KeenCallback;)V
    .locals 9
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "callback"    # Lio/keen/client/java/KeenCallback;

    .prologue
    .line 361
    monitor-enter p0

    :try_start_0
    iget-boolean v6, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    if-nez v6, :cond_0

    .line 362
    invoke-direct {p0, p2}, Lio/keen/client/java/KeenClient;->handleLibraryInactive(Lio/keen/client/java/KeenCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :goto_0
    monitor-exit p0

    return-void

    .line 366
    :cond_0
    if-nez p1, :cond_1

    :try_start_1
    iget-object v6, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    if-nez v6, :cond_1

    .line 367
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "No project specified, but no default project found"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v6, v7}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 361
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 371
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lio/keen/client/java/KeenClient;->isNetworkConnected()Z

    move-result v6

    if-nez v6, :cond_2

    .line 372
    const-string v6, "Not sending events because there is no network connection. Events will be retried next time `sendQueuedEvents` is called."

    invoke-static {v6}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 374
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "Network not connected."

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v6}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V

    goto :goto_0

    .line 378
    :cond_2
    if-nez p1, :cond_4

    iget-object v5, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 381
    .local v5, "useProject":Lio/keen/client/java/KeenProject;
    :goto_1
    :try_start_3
    invoke-virtual {v5}, Lio/keen/client/java/KeenProject;->getProjectId()Ljava/lang/String;

    move-result-object v3

    .line 382
    .local v3, "projectId":Ljava/lang/String;
    iget-object v6, p0, Lio/keen/client/java/KeenClient;->eventStore:Lio/keen/client/java/KeenEventStore;

    invoke-interface {v6, v3}, Lio/keen/client/java/KeenEventStore;->getHandles(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 383
    .local v1, "eventHandles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    invoke-direct {p0, v3, v1}, Lio/keen/client/java/KeenClient;->buildEventMap(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 384
    .local v2, "events":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;>;"
    invoke-direct {p0, v5, v2}, Lio/keen/client/java/KeenClient;->publishAll(Lio/keen/client/java/KeenProject;Ljava/util/Map;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 385
    .local v4, "response":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 387
    :try_start_4
    invoke-direct {p0, v1, v4}, Lio/keen/client/java/KeenClient;->handleAddEventsResponse(Ljava/util/Map;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 393
    :cond_3
    :goto_2
    :try_start_5
    invoke-direct {p0, p2}, Lio/keen/client/java/KeenClient;->handleSuccess(Lio/keen/client/java/KeenCallback;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 394
    .end local v1    # "eventHandles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    .end local v2    # "events":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;>;"
    .end local v3    # "projectId":Ljava/lang/String;
    .end local v4    # "response":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-direct {p0, p2, v0}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v5    # "useProject":Lio/keen/client/java/KeenProject;
    :cond_4
    move-object v5, p1

    .line 378
    goto :goto_1

    .line 388
    .restart local v1    # "eventHandles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    .restart local v2    # "events":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;>;"
    .restart local v3    # "projectId":Ljava/lang/String;
    .restart local v4    # "response":Ljava/lang/String;
    .restart local v5    # "useProject":Lio/keen/client/java/KeenProject;
    :catch_1
    move-exception v0

    .line 390
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error handling response to batch publish: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2
.end method

.method public sendQueuedEventsAsync()V
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/keen/client/java/KeenClient;->sendQueuedEventsAsync(Lio/keen/client/java/KeenProject;)V

    .line 406
    return-void
.end method

.method public sendQueuedEventsAsync(Lio/keen/client/java/KeenProject;)V
    .locals 1
    .param p1, "project"    # Lio/keen/client/java/KeenProject;

    .prologue
    .line 417
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lio/keen/client/java/KeenClient;->sendQueuedEventsAsync(Lio/keen/client/java/KeenProject;Lio/keen/client/java/KeenCallback;)V

    .line 418
    return-void
.end method

.method public sendQueuedEventsAsync(Lio/keen/client/java/KeenProject;Lio/keen/client/java/KeenCallback;)V
    .locals 5
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "callback"    # Lio/keen/client/java/KeenCallback;

    .prologue
    .line 431
    iget-boolean v2, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    if-nez v2, :cond_0

    .line 432
    invoke-direct {p0, p2}, Lio/keen/client/java/KeenClient;->handleLibraryInactive(Lio/keen/client/java/KeenCallback;)V

    .line 454
    :goto_0
    return-void

    .line 436
    :cond_0
    if-nez p1, :cond_1

    iget-object v2, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    if-nez v2, :cond_1

    .line 437
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No project specified, but no default project found"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2, v3}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V

    goto :goto_0

    .line 440
    :cond_1
    if-nez p1, :cond_2

    iget-object v1, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    .line 445
    .local v1, "useProject":Lio/keen/client/java/KeenProject;
    :goto_1
    :try_start_0
    iget-object v2, p0, Lio/keen/client/java/KeenClient;->publishExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lio/keen/client/java/KeenClient$2;

    invoke-direct {v3, p0, v1, p2}, Lio/keen/client/java/KeenClient$2;-><init>(Lio/keen/client/java/KeenClient;Lio/keen/client/java/KeenProject;Lio/keen/client/java/KeenCallback;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, p2, v0}, Lio/keen/client/java/KeenClient;->handleFailure(Lio/keen/client/java/KeenCallback;Ljava/lang/Exception;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "useProject":Lio/keen/client/java/KeenProject;
    :cond_2
    move-object v1, p1

    .line 440
    goto :goto_1
.end method

.method protected setActive(Z)V
    .locals 2
    .param p1, "isActive"    # Z

    .prologue
    .line 1054
    iput-boolean p1, p0, Lio/keen/client/java/KeenClient;->isActive:Z

    .line 1055
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Keen Client set to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string v0, "active"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1056
    return-void

    .line 1055
    :cond_0
    const-string v0, "inactive"

    goto :goto_0
.end method

.method public setBaseUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "baseUrl"    # Ljava/lang/String;

    .prologue
    .line 519
    if-nez p1, :cond_0

    .line 520
    const-string v0, "https://api.keen.io"

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->baseUrl:Ljava/lang/String;

    .line 524
    :goto_0
    return-void

    .line 522
    :cond_0
    iput-object p1, p0, Lio/keen/client/java/KeenClient;->baseUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public setDebugMode(Z)V
    .locals 0
    .param p1, "isDebugMode"    # Z

    .prologue
    .line 651
    iput-boolean p1, p0, Lio/keen/client/java/KeenClient;->isDebugMode:Z

    .line 652
    return-void
.end method

.method public setDefaultProject(Lio/keen/client/java/KeenProject;)V
    .locals 0
    .param p1, "defaultProject"    # Lio/keen/client/java/KeenProject;

    .prologue
    .line 498
    iput-object p1, p0, Lio/keen/client/java/KeenClient;->defaultProject:Lio/keen/client/java/KeenProject;

    .line 499
    return-void
.end method

.method public setGlobalProperties(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 631
    .local p1, "globalProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iput-object p1, p0, Lio/keen/client/java/KeenClient;->globalProperties:Ljava/util/Map;

    .line 632
    return-void
.end method

.method public setGlobalPropertiesEvaluator(Lio/keen/client/java/GlobalPropertiesEvaluator;)V
    .locals 0
    .param p1, "globalPropertiesEvaluator"    # Lio/keen/client/java/GlobalPropertiesEvaluator;

    .prologue
    .line 587
    iput-object p1, p0, Lio/keen/client/java/KeenClient;->globalPropertiesEvaluator:Lio/keen/client/java/GlobalPropertiesEvaluator;

    .line 588
    return-void
.end method

.method public setMaxAttempts(I)V
    .locals 0
    .param p1, "maxAttempts"    # I

    .prologue
    .line 532
    iput p1, p0, Lio/keen/client/java/KeenClient;->maxAttempts:I

    .line 533
    return-void
.end method

.method public setProxy(Ljava/lang/String;I)V
    .locals 3
    .param p1, "proxyHost"    # Ljava/lang/String;
    .param p2, "proxyPort"    # I

    .prologue
    .line 670
    new-instance v0, Ljava/net/Proxy;

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v2, Ljava/net/InetSocketAddress;

    invoke-direct {v2, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v0, v1, v2}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v0, p0, Lio/keen/client/java/KeenClient;->proxy:Ljava/net/Proxy;

    .line 671
    return-void
.end method

.method public setProxy(Ljava/net/Proxy;)V
    .locals 0
    .param p1, "proxy"    # Ljava/net/Proxy;

    .prologue
    .line 679
    iput-object p1, p0, Lio/keen/client/java/KeenClient;->proxy:Ljava/net/Proxy;

    .line 680
    return-void
.end method

.method protected validateAndBuildEvent(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 11
    .param p1, "project"    # Lio/keen/client/java/KeenProject;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1071
    .local p3, "event":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p4, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lio/keen/client/java/KeenProject;->getWriteKey()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    .line 1072
    new-instance v7, Lio/keen/client/java/exceptions/NoWriteKeyException;

    const-string v8, "You can\'t send events to Keen IO if you haven\'t set a write key."

    invoke-direct {v7, v8}, Lio/keen/client/java/exceptions/NoWriteKeyException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1075
    :cond_0
    invoke-direct {p0, p2}, Lio/keen/client/java/KeenClient;->validateEventCollection(Ljava/lang/String;)V

    .line 1076
    invoke-direct {p0, p3}, Lio/keen/client/java/KeenClient;->validateEvent(Ljava/util/Map;)V

    .line 1078
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Adding event to collection: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 1081
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1083
    .local v4, "newEvent":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1084
    .local v0, "currentTime":Ljava/util/Calendar;
    sget-object v7, Lio/keen/client/java/KeenClient;->ISO_8601_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 1085
    .local v6, "timestamp":Ljava/lang/String;
    if-nez p4, :cond_4

    .line 1086
    new-instance p4, Ljava/util/HashMap;

    .end local p4    # "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    .line 1087
    .restart local p4    # "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v7, "timestamp"

    invoke-interface {p4, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1094
    :cond_1
    :goto_0
    const-string v7, "keen"

    invoke-interface {v4, v7, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient;->getGlobalProperties()Ljava/util/Map;

    move-result-object v1

    .line 1098
    .local v1, "globalProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v1, :cond_2

    .line 1099
    invoke-interface {v4, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1101
    :cond_2
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient;->getGlobalPropertiesEvaluator()Lio/keen/client/java/GlobalPropertiesEvaluator;

    move-result-object v2

    .line 1102
    .local v2, "globalPropertiesEvaluator":Lio/keen/client/java/GlobalPropertiesEvaluator;
    if-eqz v2, :cond_3

    .line 1103
    invoke-interface {v2, p2}, Lio/keen/client/java/GlobalPropertiesEvaluator;->getGlobalProperties(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 1104
    .local v5, "props":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v5, :cond_3

    .line 1105
    invoke-interface {v4, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1110
    .end local v5    # "props":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_3
    invoke-interface {v4, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1111
    return-object v4

    .line 1088
    .end local v1    # "globalProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "globalPropertiesEvaluator":Lio/keen/client/java/GlobalPropertiesEvaluator;
    :cond_4
    const-string v7, "timestamp"

    invoke-interface {p4, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1091
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, p4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1092
    .end local p4    # "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local v3, "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v7, "timestamp"

    invoke-interface {v3, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p4, v3

    .end local v3    # "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local p4    # "keenProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_0
.end method
