.class public Lio/keen/client/java/ScopedKeys;
.super Ljava/lang/Object;
.source "ScopedKeys.java"


# static fields
.field private static final BLOCK_SIZE:I = 0x20


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decrypt(Lio/keen/client/java/KeenClient;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 12
    .param p0, "client"    # Lio/keen/client/java/KeenClient;
    .param p1, "apiKey"    # Ljava/lang/String;
    .param p2, "scopedKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenClient;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/keen/client/java/exceptions/ScopedKeyException;
        }
    .end annotation

    .prologue
    .line 140
    :try_start_0
    invoke-static {p1}, Lio/keen/client/java/ScopedKeys;->padApiKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 143
    .local v7, "paddedApiKey":Ljava/lang/String;
    const/4 v10, 0x0

    const/16 v11, 0x20

    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 146
    .local v4, "hexedIv":Ljava/lang/String;
    const/16 v10, 0x20

    invoke-virtual {p2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "hexedCipherText":Ljava/lang/String;
    invoke-static {v4}, Lio/keen/client/java/KeenUtils;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v5

    .line 150
    .local v5, "iv":[B
    invoke-static {v3}, Lio/keen/client/java/KeenUtils;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 153
    .local v1, "cipherText":[B
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const-string v10, "UTF-8"

    invoke-virtual {v7, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    const-string v11, "AES"

    invoke-direct {v9, v10, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 156
    .local v9, "secret":Ljavax/crypto/SecretKey;
    const-string v10, "AES/CBC/PKCS5Padding"

    invoke-static {v10}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 159
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v6, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v6, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 160
    .local v6, "ivParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 163
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v10

    const-string v11, "UTF-8"

    invoke-direct {v8, v10, v11}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 166
    .local v8, "plainText":Ljava/lang/String;
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient;->getJsonHandler()Lio/keen/client/java/KeenJsonHandler;

    move-result-object v10

    new-instance v11, Ljava/io/StringReader;

    invoke-direct {v11, v8}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v10, v11}, Lio/keen/client/java/KeenJsonHandler;->readJson(Ljava/io/Reader;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    return-object v10

    .line 167
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v1    # "cipherText":[B
    .end local v3    # "hexedCipherText":Ljava/lang/String;
    .end local v4    # "hexedIv":Ljava/lang/String;
    .end local v5    # "iv":[B
    .end local v6    # "ivParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v7    # "paddedApiKey":Ljava/lang/String;
    .end local v8    # "plainText":Ljava/lang/String;
    .end local v9    # "secret":Ljavax/crypto/SecretKey;
    :catch_0
    move-exception v2

    .line 168
    .local v2, "e":Ljava/lang/Exception;
    new-instance v10, Lio/keen/client/java/exceptions/ScopedKeyException;

    const-string v11, "An error occurred while attempting to decrypt a Scoped Key"

    invoke-direct {v10, v11, v2}, Lio/keen/client/java/exceptions/ScopedKeyException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
.end method

.method public static decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p0, "apiKey"    # Ljava/lang/String;
    .param p1, "scopedKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/keen/client/java/exceptions/ScopedKeyException;
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {}, Lio/keen/client/java/KeenClient;->client()Lio/keen/client/java/KeenClient;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lio/keen/client/java/ScopedKeys;->decrypt(Lio/keen/client/java/KeenClient;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static encrypt(Lio/keen/client/java/KeenClient;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 12
    .param p0, "client"    # Lio/keen/client/java/KeenClient;
    .param p1, "apiKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenClient;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/keen/client/java/exceptions/ScopedKeyException;
        }
    .end annotation

    .prologue
    .line 82
    .local p2, "options":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p2, :cond_0

    .line 83
    :try_start_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .end local p2    # "options":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local v5, "options":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object p2, v5

    .line 87
    .end local v5    # "options":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local p2    # "options":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    invoke-static {p1}, Lio/keen/client/java/ScopedKeys;->padApiKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 90
    .local v6, "paddedApiKey":Ljava/lang/String;
    new-instance v9, Ljava/io/StringWriter;

    invoke-direct {v9}, Ljava/io/StringWriter;-><init>()V

    .line 91
    .local v9, "writer":Ljava/io/StringWriter;
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient;->getJsonHandler()Lio/keen/client/java/KeenJsonHandler;

    move-result-object v10

    invoke-interface {v10, v9, p2}, Lio/keen/client/java/KeenJsonHandler;->writeJson(Ljava/io/Writer;Ljava/util/Map;)V

    .line 92
    invoke-virtual {v9}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, "jsonOptions":Ljava/lang/String;
    new-instance v8, Ljavax/crypto/spec/SecretKeySpec;

    const-string v10, "UTF-8"

    invoke-virtual {v6, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    const-string v11, "AES"

    invoke-direct {v8, v10, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 98
    .local v8, "secret":Ljavax/crypto/SecretKey;
    const-string v10, "AES/CBC/PKCS5Padding"

    invoke-static {v10}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 99
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 101
    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getParameters()Ljava/security/AlgorithmParameters;

    move-result-object v7

    .line 103
    .local v7, "params":Ljava/security/AlgorithmParameters;
    const-class v10, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v7, v10}, Ljava/security/AlgorithmParameters;->getParameterSpec(Ljava/lang/Class;)Ljava/security/spec/AlgorithmParameterSpec;

    move-result-object v10

    check-cast v10, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v10}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v3

    .line 105
    .local v3, "iv":[B
    const-string v10, "UTF-8"

    invoke-virtual {v4, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    invoke-virtual {v0, v10}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 108
    .local v1, "cipherText":[B
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lio/keen/client/java/KeenUtils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v1}, Lio/keen/client/java/KeenUtils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    return-object v10

    .line 109
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v1    # "cipherText":[B
    .end local v3    # "iv":[B
    .end local v4    # "jsonOptions":Ljava/lang/String;
    .end local v6    # "paddedApiKey":Ljava/lang/String;
    .end local v7    # "params":Ljava/security/AlgorithmParameters;
    .end local v8    # "secret":Ljavax/crypto/SecretKey;
    .end local v9    # "writer":Ljava/io/StringWriter;
    :catch_0
    move-exception v2

    .line 110
    .local v2, "e":Ljava/lang/Exception;
    new-instance v10, Lio/keen/client/java/exceptions/ScopedKeyException;

    const-string v11, "An error occurred while attempting to encrypt a Scoped Key"

    invoke-direct {v10, v11, v2}, Lio/keen/client/java/exceptions/ScopedKeyException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
.end method

.method public static encrypt(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 1
    .param p0, "apiKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/keen/client/java/exceptions/ScopedKeyException;
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "options":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {}, Lio/keen/client/java/KeenClient;->client()Lio/keen/client/java/KeenClient;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lio/keen/client/java/ScopedKeys;->encrypt(Lio/keen/client/java/KeenClient;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static pad(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    rem-int/lit8 v3, v3, 0x20

    rsub-int/lit8 v2, v3, 0x20

    .line 184
    .local v2, "paddingSize":I
    const-string v1, ""

    .line 185
    .local v1, "padding":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 186
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    int-to-char v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static padApiKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "apiKey"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    .line 176
    .end local p0    # "apiKey":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "apiKey":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lio/keen/client/java/ScopedKeys;->pad(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
