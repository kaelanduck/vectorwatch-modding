.class public Lio/keen/client/java/FileEventStore;
.super Ljava/lang/Object;
.source "FileEventStore.java"

# interfaces
.implements Lio/keen/client/java/KeenAttemptCountingEventStore;


# static fields
.field private static final ATTEMPTS_JSON_FILE_NAME:Ljava/lang/String; = "__attempts.json"

.field private static final ENCODING:Ljava/lang/String; = "UTF-8"

.field private static final MAX_EVENTS_PER_COLLECTION:I = 0x2710

.field private static final NUMBER_EVENTS_TO_FORGET:I = 0x64


# instance fields
.field private final root:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1, "root"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Event store root \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' must exist and be a directory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_1
    iput-object p1, p0, Lio/keen/client/java/FileEventStore;->root:Ljava/io/File;

    .line 43
    return-void
.end method

.method private getCollectionDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lio/keen/client/java/FileEventStore;->getProjectDir(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 298
    .local v0, "collectionDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache directory for event collection \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' doesn\'t exist. Creating it."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 301
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 302
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create collection cache directory \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 306
    :cond_0
    return-object v0
.end method

.method private getFileForEvent(Ljava/io/File;Ljava/util/Calendar;)Ljava/io/File;
    .locals 3
    .param p1, "collectionDir"    # Ljava/io/File;
    .param p2, "timestamp"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    const/4 v0, 0x0

    .line 320
    .local v0, "counter":I
    invoke-direct {p0, p1, p2, v0}, Lio/keen/client/java/FileEventStore;->getNextFileForEvent(Ljava/io/File;Ljava/util/Calendar;I)Ljava/io/File;

    move-result-object v1

    .line 321
    .local v1, "eventFile":Ljava/io/File;
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    invoke-direct {p0, p1, p2, v0}, Lio/keen/client/java/FileEventStore;->getNextFileForEvent(Ljava/io/File;Ljava/util/Calendar;I)Ljava/io/File;

    move-result-object v1

    .line 323
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 325
    :cond_0
    return-object v1
.end method

.method private getFilesInDir(Ljava/io/File;)[Ljava/io/File;
    .locals 1
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 260
    new-instance v0, Lio/keen/client/java/FileEventStore$2;

    invoke-direct {v0, p0}, Lio/keen/client/java/FileEventStore$2;-><init>(Lio/keen/client/java/FileEventStore;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private getHandlesFromProjectDirectory(Ljava/io/File;)Ljava/util/Map;
    .locals 11
    .param p1, "projectDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lio/keen/client/java/FileEventStore;->getSubDirectories(Ljava/io/File;)[Ljava/io/File;

    move-result-object v1

    .line 201
    .local v1, "collectionDirs":[Ljava/io/File;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 202
    .local v6, "handleMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/Object;>;>;"
    if-eqz v1, :cond_1

    .line 204
    move-object v0, v1

    .local v0, "arr$":[Ljava/io/File;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v3, v0, v7

    .line 205
    .local v3, "directory":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 206
    .local v2, "collectionName":Ljava/lang/String;
    invoke-direct {p0, v3}, Lio/keen/client/java/FileEventStore;->getFilesInDir(Ljava/io/File;)[Ljava/io/File;

    move-result-object v4

    .line 207
    .local v4, "files":[Ljava/io/File;
    if-eqz v4, :cond_0

    .line 208
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v5, "handleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 210
    invoke-interface {v6, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    .end local v5    # "handleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 212
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Directory was null while getting event handles: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 217
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "collectionName":Ljava/lang/String;
    .end local v3    # "directory":Ljava/io/File;
    .end local v4    # "files":[Ljava/io/File;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :cond_1
    return-object v6
.end method

.method private getKeenCacheDirectory()Ljava/io/File;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lio/keen/client/java/FileEventStore;->root:Ljava/io/File;

    const-string v3, "keen"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 229
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 230
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v0

    .line 231
    .local v0, "dirMade":Z
    if-nez v0, :cond_0

    .line 232
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not make keen cache directory at: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 235
    .end local v0    # "dirMade":Z
    :cond_0
    return-object v1
.end method

.method private getMaxEventsPerCollection()I
    .locals 1

    .prologue
    .line 349
    const/16 v0, 0x2710

    return v0
.end method

.method private getNextFileForEvent(Ljava/io/File;Ljava/util/Calendar;I)Ljava/io/File;
    .locals 6
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "timestamp"    # Ljava/util/Calendar;
    .param p3, "counter"    # I

    .prologue
    .line 338
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 339
    .local v2, "timestampInMillis":J
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "name":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private getNumberEventsToForget()I
    .locals 1

    .prologue
    .line 358
    const/16 v0, 0x64

    return v0
.end method

.method private getProjectDir(Ljava/lang/String;Z)Ljava/io/File;
    .locals 4
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "create"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lio/keen/client/java/FileEventStore;->getKeenCacheDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 277
    .local v0, "projectDir":Ljava/io/File;
    if-eqz p2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache directory for project \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' doesn\'t exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Creating it."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 280
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create project cache directory \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 285
    :cond_0
    return-object v0
.end method

.method private getSubDirectories(Ljava/io/File;)[Ljava/io/File;
    .locals 1
    .param p1, "parent"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    new-instance v0, Lio/keen/client/java/FileEventStore$1;

    invoke-direct {v0, p0}, Lio/keen/client/java/FileEventStore$1;-><init>(Lio/keen/client/java/FileEventStore;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private prepareCollectionDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 11
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 373
    invoke-direct {p0, p1, p2}, Lio/keen/client/java/FileEventStore;->getCollectionDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 377
    .local v0, "collectionDir":Ljava/io/File;
    invoke-direct {p0, v0}, Lio/keen/client/java/FileEventStore;->getFilesInDir(Ljava/io/File;)[Ljava/io/File;

    move-result-object v1

    .line 378
    .local v1, "eventFiles":[Ljava/io/File;
    array-length v5, v1

    invoke-direct {p0}, Lio/keen/client/java/FileEventStore;->getMaxEventsPerCollection()I

    move-result v6

    if-lt v5, v6, :cond_1

    .line 380
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Too many events in cache for %s, aging out old data"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object p2, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 382
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Count: %d and Max: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    array-length v8, v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-direct {p0}, Lio/keen/client/java/FileEventStore;->getMaxEventsPerCollection()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 386
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 387
    .local v3, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v5, Lio/keen/client/java/FileEventStore$3;

    invoke-direct {v5, p0}, Lio/keen/client/java/FileEventStore$3;-><init>(Lio/keen/client/java/FileEventStore;)V

    invoke-static {v3, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 393
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-direct {p0}, Lio/keen/client/java/FileEventStore;->getNumberEventsToForget()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 394
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 395
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_0

    .line 396
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "CRITICAL: can\'t delete file %s, cache is going to be too big"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 393
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 403
    .end local v2    # "f":Ljava/io/File;
    .end local v3    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v4    # "i":I
    :cond_1
    return-object v0
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/String;
    .locals 4
    .param p1, "handle"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    instance-of v1, p1, Ljava/io/File;

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected File, but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    .line 83
    check-cast v0, Ljava/io/File;

    .line 84
    .local v0, "eventFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    invoke-static {v0}, Lio/keen/client/java/KeenUtils;->convertFileToString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 87
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAttempts(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lio/keen/client/java/FileEventStore;->getProjectDir(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v2

    .line 135
    .local v2, "projectDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 136
    .local v1, "collectionDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v3, "__attempts.json"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 137
    .local v0, "attemptsFile":Ljava/io/File;
    invoke-virtual {p0, v0}, Lio/keen/client/java/FileEventStore;->get(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getHandles(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p1, "projectId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lio/keen/client/java/FileEventStore;->getProjectDir(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    .line 122
    .local v0, "projectDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    invoke-direct {p0, v0}, Lio/keen/client/java/FileEventStore;->getHandlesFromProjectDirectory(Ljava/io/File;)Ljava/util/Map;

    move-result-object v1

    .line 125
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 6
    .param p1, "handle"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 96
    instance-of v1, p1, Ljava/io/File;

    if-nez v1, :cond_0

    .line 97
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected File, but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    .line 100
    check-cast v0, Ljava/io/File;

    .line 101
    .local v0, "eventFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 102
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Successfully deleted file: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    .line 114
    :goto_0
    return-void

    .line 106
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "CRITICAL ERROR: Could not remove event at %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "WARNING: no event found at %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAttempts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .param p3, "attemptsString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Lio/keen/client/java/FileEventStore;->prepareCollectionDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 149
    .local v1, "collectionCacheDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v5, "__attempts.json"

    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 152
    .local v0, "cacheFile":Ljava/io/File;
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 153
    .local v2, "out":Ljava/io/OutputStream;
    const/4 v3, 0x0

    .line 155
    .local v3, "writer":Ljava/io/Writer;
    :try_start_0
    new-instance v4, Ljava/io/OutputStreamWriter;

    const-string v5, "UTF-8"

    invoke-direct {v4, v2, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    .end local v3    # "writer":Ljava/io/Writer;
    .local v4, "writer":Ljava/io/Writer;
    :try_start_1
    invoke-virtual {v4, p3}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 158
    invoke-static {v4}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 160
    return-void

    .line 158
    .end local v4    # "writer":Ljava/io/Writer;
    .restart local v3    # "writer":Ljava/io/Writer;
    :catchall_0
    move-exception v5

    :goto_0
    invoke-static {v3}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v5

    .end local v3    # "writer":Ljava/io/Writer;
    .restart local v4    # "writer":Ljava/io/Writer;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/Writer;
    .restart local v3    # "writer":Ljava/io/Writer;
    goto :goto_0
.end method

.method public store(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 7
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "eventCollection"    # Ljava/lang/String;
    .param p3, "event"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lio/keen/client/java/FileEventStore;->prepareCollectionDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 57
    .local v1, "collectionCacheDir":Ljava/io/File;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 58
    .local v3, "timestamp":Ljava/util/Calendar;
    invoke-direct {p0, v1, v3}, Lio/keen/client/java/FileEventStore;->getFileForEvent(Ljava/io/File;Ljava/util/Calendar;)Ljava/io/File;

    move-result-object v0

    .line 61
    .local v0, "cacheFile":Ljava/io/File;
    const/4 v4, 0x0

    .line 63
    .local v4, "writer":Ljava/io/Writer;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 64
    .local v2, "out":Ljava/io/OutputStream;
    new-instance v5, Ljava/io/OutputStreamWriter;

    const-string v6, "UTF-8"

    invoke-direct {v5, v2, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    .end local v4    # "writer":Ljava/io/Writer;
    .local v5, "writer":Ljava/io/Writer;
    :try_start_1
    invoke-virtual {v5, p3}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 67
    invoke-static {v5}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 71
    return-object v0

    .line 67
    .end local v2    # "out":Ljava/io/OutputStream;
    .end local v5    # "writer":Ljava/io/Writer;
    .restart local v4    # "writer":Ljava/io/Writer;
    :catchall_0
    move-exception v6

    :goto_0
    invoke-static {v4}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v6

    .end local v4    # "writer":Ljava/io/Writer;
    .restart local v2    # "out":Ljava/io/OutputStream;
    .restart local v5    # "writer":Ljava/io/Writer;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "writer":Ljava/io/Writer;
    .restart local v4    # "writer":Ljava/io/Writer;
    goto :goto_0
.end method
