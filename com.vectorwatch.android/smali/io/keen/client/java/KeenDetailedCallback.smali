.class public interface abstract Lio/keen/client/java/KeenDetailedCallback;
.super Ljava/lang/Object;
.source "KeenDetailedCallback.java"

# interfaces
.implements Lio/keen/client/java/KeenCallback;


# virtual methods
.method public abstract onFailure(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Exception;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onSuccess(Lio/keen/client/java/KeenProject;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/keen/client/java/KeenProject;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method
