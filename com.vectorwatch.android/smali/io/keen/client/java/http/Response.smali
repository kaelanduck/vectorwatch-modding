.class public final Lio/keen/client/java/http/Response;
.super Ljava/lang/Object;
.source "Response.java"


# instance fields
.field public final body:Ljava/lang/String;

.field public final statusCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "statusCode"    # I
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lio/keen/client/java/http/Response;->statusCode:I

    .line 20
    iput-object p2, p0, Lio/keen/client/java/http/Response;->body:Ljava/lang/String;

    .line 21
    return-void
.end method

.method private static isSuccessCode(I)Z
    .locals 2
    .param p0, "statusCode"    # I

    .prologue
    .line 38
    div-int/lit8 v0, p0, 0x64

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isSuccess()Z
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lio/keen/client/java/http/Response;->statusCode:I

    invoke-static {v0}, Lio/keen/client/java/http/Response;->isSuccessCode(I)Z

    move-result v0

    return v0
.end method
