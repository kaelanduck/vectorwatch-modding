.class public final Lio/keen/client/java/http/Request;
.super Ljava/lang/Object;
.source "Request.java"


# instance fields
.field public final authorization:Ljava/lang/String;

.field public final body:Lio/keen/client/java/http/OutputSource;

.field public final method:Ljava/lang/String;

.field public final proxy:Ljava/net/Proxy;

.field public final url:Ljava/net/URL;


# direct methods
.method public constructor <init>(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Lio/keen/client/java/http/OutputSource;)V
    .locals 6
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "authorization"    # Ljava/lang/String;
    .param p4, "body"    # Lio/keen/client/java/http/OutputSource;

    .prologue
    .line 25
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lio/keen/client/java/http/Request;-><init>(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Lio/keen/client/java/http/OutputSource;Ljava/net/Proxy;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Lio/keen/client/java/http/OutputSource;Ljava/net/Proxy;)V
    .locals 0
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "authorization"    # Ljava/lang/String;
    .param p4, "body"    # Lio/keen/client/java/http/OutputSource;
    .param p5, "proxy"    # Ljava/net/Proxy;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lio/keen/client/java/http/Request;->url:Ljava/net/URL;

    .line 30
    iput-object p2, p0, Lio/keen/client/java/http/Request;->method:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lio/keen/client/java/http/Request;->authorization:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lio/keen/client/java/http/Request;->body:Lio/keen/client/java/http/OutputSource;

    .line 33
    iput-object p5, p0, Lio/keen/client/java/http/Request;->proxy:Ljava/net/Proxy;

    .line 34
    return-void
.end method
