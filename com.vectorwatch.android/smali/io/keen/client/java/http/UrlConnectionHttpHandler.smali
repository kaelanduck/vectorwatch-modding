.class public Lio/keen/client/java/http/UrlConnectionHttpHandler;
.super Ljava/lang/Object;
.source "UrlConnectionHttpHandler.java"

# interfaces
.implements Lio/keen/client/java/http/HttpHandler;


# static fields
.field private static final DEFAULT_CONNECT_TIMEOUT:I = 0x7530

.field private static final DEFAULT_READ_TIMEOUT:I = 0x7530


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/keen/client/java/http/Request;)Lio/keen/client/java/http/Response;
    .locals 2
    .param p1, "request"    # Lio/keen/client/java/http/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lio/keen/client/java/http/UrlConnectionHttpHandler;->openConnection(Lio/keen/client/java/http/Request;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 29
    .local v0, "connection":Ljava/net/HttpURLConnection;
    invoke-virtual {p0, v0, p1}, Lio/keen/client/java/http/UrlConnectionHttpHandler;->sendRequest(Ljava/net/HttpURLConnection;Lio/keen/client/java/http/Request;)V

    .line 30
    invoke-virtual {p0, v0}, Lio/keen/client/java/http/UrlConnectionHttpHandler;->readResponse(Ljava/net/HttpURLConnection;)Lio/keen/client/java/http/Response;

    move-result-object v1

    return-object v1
.end method

.method protected openConnection(Lio/keen/client/java/http/Request;)Ljava/net/HttpURLConnection;
    .locals 4
    .param p1, "request"    # Lio/keen/client/java/http/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x7530

    .line 47
    iget-object v1, p1, Lio/keen/client/java/http/Request;->proxy:Ljava/net/Proxy;

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p1, Lio/keen/client/java/http/Request;->url:Ljava/net/URL;

    iget-object v2, p1, Lio/keen/client/java/http/Request;->proxy:Ljava/net/Proxy;

    invoke-virtual {v1, v2}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 52
    .local v0, "result":Ljava/net/HttpURLConnection;
    :goto_0
    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 53
    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 54
    return-object v0

    .line 50
    .end local v0    # "result":Ljava/net/HttpURLConnection;
    :cond_0
    iget-object v1, p1, Lio/keen/client/java/http/Request;->url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .restart local v0    # "result":Ljava/net/HttpURLConnection;
    goto :goto_0
.end method

.method protected readResponse(Ljava/net/HttpURLConnection;)Lio/keen/client/java/http/Response;
    .locals 5
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 98
    .local v2, "in":Ljava/io/InputStream;
    :goto_0
    const-string v0, ""

    .line 99
    .local v0, "body":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 101
    :try_start_1
    invoke-static {v2}, Lio/keen/client/java/KeenUtils;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 103
    invoke-static {v2}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 108
    :cond_0
    new-instance v3, Lio/keen/client/java/http/Response;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    invoke-direct {v3, v4, v0}, Lio/keen/client/java/http/Response;-><init>(ILjava/lang/String;)V

    return-object v3

    .line 93
    .end local v0    # "body":Ljava/lang/String;
    .end local v2    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v2

    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_0

    .line 103
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "body":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lio/keen/client/java/KeenUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v3
.end method

.method protected sendRequest(Ljava/net/HttpURLConnection;Lio/keen/client/java/http/Request;)V
    .locals 2
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .param p2, "request"    # Lio/keen/client/java/http/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p2, Lio/keen/client/java/http/Request;->method:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 67
    const-string v0, "Accept"

    const-string v1, "application/json"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v0, "Authorization"

    iget-object v1, p2, Lio/keen/client/java/http/Request;->authorization:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p2, Lio/keen/client/java/http/Request;->body:Lio/keen/client/java/http/OutputSource;

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 73
    const-string v0, "Content-Type"

    const-string v1, "application/json"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p2, Lio/keen/client/java/http/Request;->body:Lio/keen/client/java/http/OutputSource;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/keen/client/java/http/OutputSource;->writeTo(Ljava/io/OutputStream;)V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    goto :goto_0
.end method
