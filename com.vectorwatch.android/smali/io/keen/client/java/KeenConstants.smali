.class Lio/keen/client/java/KeenConstants;
.super Ljava/lang/Object;
.source "KeenConstants.java"


# static fields
.field static final API_VERSION:Ljava/lang/String; = "3.0"

.field static final DEFAULT_MAX_ATTEMPTS:I = 0x3

.field static final DESCRIPTION_PARAM:Ljava/lang/String; = "description"

.field static final ERROR_PARAM:Ljava/lang/String; = "error"

.field static final INVALID_COLLECTION_NAME_ERROR:Ljava/lang/String; = "InvalidCollectionNameError"

.field static final INVALID_PROPERTY_NAME_ERROR:Ljava/lang/String; = "InvalidPropertyNameError"

.field static final INVALID_PROPERTY_VALUE_ERROR:Ljava/lang/String; = "InvalidPropertyValueError"

.field static final MAX_EVENT_DEPTH:I = 0x3e8

.field static final NAME_PARAM:Ljava/lang/String; = "name"

.field static final SERVER_ADDRESS:Ljava/lang/String; = "https://api.keen.io"

.field static final SUCCESS_PARAM:Ljava/lang/String; = "success"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
