.class public Lio/keen/client/java/KeenProject;
.super Ljava/lang/Object;
.source "KeenProject.java"


# instance fields
.field private final projectId:Ljava/lang/String;

.field private final readKey:Ljava/lang/String;

.field private final writeKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lio/keen/client/java/Environment;

    invoke-direct {v0}, Lio/keen/client/java/Environment;-><init>()V

    invoke-direct {p0, v0}, Lio/keen/client/java/KeenProject;-><init>(Lio/keen/client/java/Environment;)V

    .line 15
    return-void
.end method

.method constructor <init>(Lio/keen/client/java/Environment;)V
    .locals 3
    .param p1, "env"    # Lio/keen/client/java/Environment;

    .prologue
    .line 18
    invoke-virtual {p1}, Lio/keen/client/java/Environment;->getKeenProjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lio/keen/client/java/Environment;->getKeenWriteKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lio/keen/client/java/Environment;->getKeenReadKey()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lio/keen/client/java/KeenProject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "writeKey"    # Ljava/lang/String;
    .param p3, "readKey"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 32
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid project id specified: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    iput-object p1, p0, Lio/keen/client/java/KeenProject;->projectId:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lio/keen/client/java/KeenProject;->writeKey:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lio/keen/client/java/KeenProject;->readKey:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public getProjectId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lio/keen/client/java/KeenProject;->projectId:Ljava/lang/String;

    return-object v0
.end method

.method public getReadKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lio/keen/client/java/KeenProject;->readKey:Ljava/lang/String;

    return-object v0
.end method

.method public getWriteKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lio/keen/client/java/KeenProject;->writeKey:Ljava/lang/String;

    return-object v0
.end method
