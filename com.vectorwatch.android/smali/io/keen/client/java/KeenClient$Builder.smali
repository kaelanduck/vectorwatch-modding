.class public abstract Lio/keen/client/java/KeenClient$Builder;
.super Ljava/lang/Object;
.source "KeenClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/keen/client/java/KeenClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# instance fields
.field private eventStore:Lio/keen/client/java/KeenEventStore;

.field private httpHandler:Lio/keen/client/java/http/HttpHandler;

.field private jsonHandler:Lio/keen/client/java/KeenJsonHandler;

.field private networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

.field private publishExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/http/HttpHandler;
    .locals 1
    .param p0, "x0"    # Lio/keen/client/java/KeenClient$Builder;

    .prologue
    .line 707
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    return-object v0
.end method

.method static synthetic access$100(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/KeenJsonHandler;
    .locals 1
    .param p0, "x0"    # Lio/keen/client/java/KeenClient$Builder;

    .prologue
    .line 707
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    return-object v0
.end method

.method static synthetic access$200(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/KeenEventStore;
    .locals 1
    .param p0, "x0"    # Lio/keen/client/java/KeenClient$Builder;

    .prologue
    .line 707
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->eventStore:Lio/keen/client/java/KeenEventStore;

    return-object v0
.end method

.method static synthetic access$300(Lio/keen/client/java/KeenClient$Builder;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lio/keen/client/java/KeenClient$Builder;

    .prologue
    .line 707
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->publishExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$400(Lio/keen/client/java/KeenClient$Builder;)Lio/keen/client/java/KeenNetworkStatusHandler;
    .locals 1
    .param p0, "x0"    # Lio/keen/client/java/KeenClient$Builder;

    .prologue
    .line 707
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

    return-object v0
.end method


# virtual methods
.method public build()Lio/keen/client/java/KeenClient;
    .locals 3

    .prologue
    .line 944
    :try_start_0
    iget-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    if-nez v1, :cond_0

    .line 945
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient$Builder;->getDefaultHttpHandler()Lio/keen/client/java/http/HttpHandler;

    move-result-object v1

    iput-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->httpHandler:Lio/keen/client/java/http/HttpHandler;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 952
    :cond_0
    :goto_0
    :try_start_1
    iget-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    if-nez v1, :cond_1

    .line 953
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient$Builder;->getDefaultJsonHandler()Lio/keen/client/java/KeenJsonHandler;

    move-result-object v1

    iput-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 960
    :cond_1
    :goto_1
    :try_start_2
    iget-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->eventStore:Lio/keen/client/java/KeenEventStore;

    if-nez v1, :cond_2

    .line 961
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient$Builder;->getDefaultEventStore()Lio/keen/client/java/KeenEventStore;

    move-result-object v1

    iput-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->eventStore:Lio/keen/client/java/KeenEventStore;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 968
    :cond_2
    :goto_2
    :try_start_3
    iget-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->publishExecutor:Ljava/util/concurrent/Executor;

    if-nez v1, :cond_3

    .line 969
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient$Builder;->getDefaultPublishExecutor()Ljava/util/concurrent/Executor;

    move-result-object v1

    iput-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->publishExecutor:Ljava/util/concurrent/Executor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 976
    :cond_3
    :goto_3
    :try_start_4
    iget-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

    if-nez v1, :cond_4

    .line 977
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient$Builder;->getDefaultNetworkStatusHandler()Lio/keen/client/java/KeenNetworkStatusHandler;

    move-result-object v1

    iput-object v1, p0, Lio/keen/client/java/KeenClient$Builder;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 983
    :cond_4
    :goto_4
    invoke-virtual {p0}, Lio/keen/client/java/KeenClient$Builder;->buildInstance()Lio/keen/client/java/KeenClient;

    move-result-object v1

    return-object v1

    .line 947
    :catch_0
    move-exception v0

    .line 948
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception building HTTP handler: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 955
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 956
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception building JSON handler: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 963
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 964
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception building event store: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_2

    .line 971
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 972
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception building publish executor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto :goto_3

    .line 979
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v0

    .line 980
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception building network status handler: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/keen/client/java/KeenLogging;->log(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method protected buildInstance()Lio/keen/client/java/KeenClient;
    .locals 1

    .prologue
    .line 994
    new-instance v0, Lio/keen/client/java/KeenClient;

    invoke-direct {v0, p0}, Lio/keen/client/java/KeenClient;-><init>(Lio/keen/client/java/KeenClient$Builder;)V

    return-object v0
.end method

.method protected getDefaultEventStore()Lio/keen/client/java/KeenEventStore;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 811
    new-instance v0, Lio/keen/client/java/RamEventStore;

    invoke-direct {v0}, Lio/keen/client/java/RamEventStore;-><init>()V

    return-object v0
.end method

.method protected getDefaultHttpHandler()Lio/keen/client/java/http/HttpHandler;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 727
    new-instance v0, Lio/keen/client/java/http/UrlConnectionHttpHandler;

    invoke-direct {v0}, Lio/keen/client/java/http/UrlConnectionHttpHandler;-><init>()V

    return-object v0
.end method

.method protected abstract getDefaultJsonHandler()Lio/keen/client/java/KeenJsonHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected getDefaultNetworkStatusHandler()Lio/keen/client/java/KeenNetworkStatusHandler;
    .locals 1

    .prologue
    .line 902
    new-instance v0, Lio/keen/client/java/AlwaysConnectedNetworkStatusHandler;

    invoke-direct {v0}, Lio/keen/client/java/AlwaysConnectedNetworkStatusHandler;-><init>()V

    return-object v0
.end method

.method protected getDefaultPublishExecutor()Ljava/util/concurrent/Executor;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 858
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    .line 859
    .local v0, "procCount":I
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    return-object v1
.end method

.method public getEventStore()Lio/keen/client/java/KeenEventStore;
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->eventStore:Lio/keen/client/java/KeenEventStore;

    return-object v0
.end method

.method public getHttpHandler()Lio/keen/client/java/http/HttpHandler;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    return-object v0
.end method

.method public getJsonHandler()Lio/keen/client/java/KeenJsonHandler;
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    return-object v0
.end method

.method public getNetworkStatusHandler()Lio/keen/client/java/KeenNetworkStatusHandler;
    .locals 1

    .prologue
    .line 912
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

    return-object v0
.end method

.method public getPublishExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lio/keen/client/java/KeenClient$Builder;->publishExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public setEventStore(Lio/keen/client/java/KeenEventStore;)V
    .locals 0
    .param p1, "eventStore"    # Lio/keen/client/java/KeenEventStore;

    .prologue
    .line 831
    iput-object p1, p0, Lio/keen/client/java/KeenClient$Builder;->eventStore:Lio/keen/client/java/KeenEventStore;

    .line 832
    return-void
.end method

.method public setHttpHandler(Lio/keen/client/java/http/HttpHandler;)V
    .locals 0
    .param p1, "httpHandler"    # Lio/keen/client/java/http/HttpHandler;

    .prologue
    .line 746
    iput-object p1, p0, Lio/keen/client/java/KeenClient$Builder;->httpHandler:Lio/keen/client/java/http/HttpHandler;

    .line 747
    return-void
.end method

.method public setJsonHandler(Lio/keen/client/java/KeenJsonHandler;)V
    .locals 0
    .param p1, "jsonHandler"    # Lio/keen/client/java/KeenJsonHandler;

    .prologue
    .line 786
    iput-object p1, p0, Lio/keen/client/java/KeenClient$Builder;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;

    .line 787
    return-void
.end method

.method public setNetworkStatusHandler(Lio/keen/client/java/KeenNetworkStatusHandler;)V
    .locals 0
    .param p1, "networkStatusHandler"    # Lio/keen/client/java/KeenNetworkStatusHandler;

    .prologue
    .line 921
    iput-object p1, p0, Lio/keen/client/java/KeenClient$Builder;->networkStatusHandler:Lio/keen/client/java/KeenNetworkStatusHandler;

    .line 922
    return-void
.end method

.method public setPublishExecutor(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "publishExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 878
    iput-object p1, p0, Lio/keen/client/java/KeenClient$Builder;->publishExecutor:Ljava/util/concurrent/Executor;

    .line 879
    return-void
.end method

.method public withEventStore(Lio/keen/client/java/KeenEventStore;)Lio/keen/client/java/KeenClient$Builder;
    .locals 0
    .param p1, "eventStore"    # Lio/keen/client/java/KeenEventStore;

    .prologue
    .line 842
    invoke-virtual {p0, p1}, Lio/keen/client/java/KeenClient$Builder;->setEventStore(Lio/keen/client/java/KeenEventStore;)V

    .line 843
    return-object p0
.end method

.method public withHttpHandler(Lio/keen/client/java/http/HttpHandler;)Lio/keen/client/java/KeenClient$Builder;
    .locals 0
    .param p1, "httpHandler"    # Lio/keen/client/java/http/HttpHandler;

    .prologue
    .line 756
    invoke-virtual {p0, p1}, Lio/keen/client/java/KeenClient$Builder;->setHttpHandler(Lio/keen/client/java/http/HttpHandler;)V

    .line 757
    return-object p0
.end method

.method public withJsonHandler(Lio/keen/client/java/KeenJsonHandler;)Lio/keen/client/java/KeenClient$Builder;
    .locals 0
    .param p1, "jsonHandler"    # Lio/keen/client/java/KeenJsonHandler;

    .prologue
    .line 796
    invoke-virtual {p0, p1}, Lio/keen/client/java/KeenClient$Builder;->setJsonHandler(Lio/keen/client/java/KeenJsonHandler;)V

    .line 797
    return-object p0
.end method

.method public withNetworkStatusHandler(Lio/keen/client/java/KeenNetworkStatusHandler;)Lio/keen/client/java/KeenClient$Builder;
    .locals 0
    .param p1, "networkStatusHandler"    # Lio/keen/client/java/KeenNetworkStatusHandler;

    .prologue
    .line 931
    invoke-virtual {p0, p1}, Lio/keen/client/java/KeenClient$Builder;->setNetworkStatusHandler(Lio/keen/client/java/KeenNetworkStatusHandler;)V

    .line 932
    return-object p0
.end method

.method public withPublishExecutor(Ljava/util/concurrent/Executor;)Lio/keen/client/java/KeenClient$Builder;
    .locals 0
    .param p1, "publishExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 888
    invoke-virtual {p0, p1}, Lio/keen/client/java/KeenClient$Builder;->setPublishExecutor(Ljava/util/concurrent/Executor;)V

    .line 889
    return-object p0
.end method
