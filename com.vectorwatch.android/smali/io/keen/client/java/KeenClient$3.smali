.class Lio/keen/client/java/KeenClient$3;
.super Ljava/lang/Object;
.source "KeenClient.java"

# interfaces
.implements Lio/keen/client/java/http/OutputSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/keen/client/java/KeenClient;->publishObject(Lio/keen/client/java/KeenProject;Ljava/net/URL;Ljava/util/Map;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/keen/client/java/KeenClient;

.field final synthetic val$requestData:Ljava/util/Map;


# direct methods
.method constructor <init>(Lio/keen/client/java/KeenClient;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1372
    iput-object p1, p0, Lio/keen/client/java/KeenClient$3;->this$0:Lio/keen/client/java/KeenClient;

    iput-object p2, p0, Lio/keen/client/java/KeenClient$3;->val$requestData:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1375
    new-instance v0, Ljava/io/OutputStreamWriter;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1376
    .local v0, "writer":Ljava/io/OutputStreamWriter;
    iget-object v1, p0, Lio/keen/client/java/KeenClient$3;->this$0:Lio/keen/client/java/KeenClient;

    # getter for: Lio/keen/client/java/KeenClient;->jsonHandler:Lio/keen/client/java/KeenJsonHandler;
    invoke-static {v1}, Lio/keen/client/java/KeenClient;->access$500(Lio/keen/client/java/KeenClient;)Lio/keen/client/java/KeenJsonHandler;

    move-result-object v1

    iget-object v2, p0, Lio/keen/client/java/KeenClient$3;->val$requestData:Ljava/util/Map;

    invoke-interface {v1, v0, v2}, Lio/keen/client/java/KeenJsonHandler;->writeJson(Ljava/io/Writer;Ljava/util/Map;)V

    .line 1377
    return-void
.end method
