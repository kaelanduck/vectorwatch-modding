.class public interface abstract Lio/keen/client/java/KeenAttemptCountingEventStore;
.super Ljava/lang/Object;
.source "KeenAttemptCountingEventStore.java"

# interfaces
.implements Lio/keen/client/java/KeenEventStore;


# virtual methods
.method public abstract getAttempts(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setAttempts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
