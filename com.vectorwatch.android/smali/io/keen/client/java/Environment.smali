.class Lio/keen/client/java/Environment;
.super Ljava/lang/Object;
.source "Environment.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-static {}, Ljava/lang/System;->getenv()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getKeenProjectId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const-string v0, "KEEN_PROJECT_ID"

    invoke-direct {p0, v0}, Lio/keen/client/java/Environment;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeenReadKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "KEEN_READ_KEY"

    invoke-direct {p0, v0}, Lio/keen/client/java/Environment;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeenWriteKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const-string v0, "KEEN_WRITE_KEY"

    invoke-direct {p0, v0}, Lio/keen/client/java/Environment;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
