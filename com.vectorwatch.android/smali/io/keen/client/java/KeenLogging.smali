.class public Lio/keen/client/java/KeenLogging;
.super Ljava/lang/Object;
.source "KeenLogging.java"


# static fields
.field private static final HANDLER:Ljava/util/logging/StreamHandler;

.field private static final LOGGER:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const-class v0, Lio/keen/client/java/KeenLogging;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    .line 23
    new-instance v0, Ljava/util/logging/StreamHandler;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/util/logging/SimpleFormatter;

    invoke-direct {v2}, Ljava/util/logging/SimpleFormatter;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/logging/StreamHandler;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    sput-object v0, Lio/keen/client/java/KeenLogging;->HANDLER:Ljava/util/logging/StreamHandler;

    .line 24
    sget-object v0, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    sget-object v1, Lio/keen/client/java/KeenLogging;->HANDLER:Ljava/util/logging/StreamHandler;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    .line 25
    invoke-static {}, Lio/keen/client/java/KeenLogging;->disableLogging()V

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableDefaultLogHandler()V
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    sget-object v1, Lio/keen/client/java/KeenLogging;->HANDLER:Ljava/util/logging/StreamHandler;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->removeHandler(Ljava/util/logging/Handler;)V

    .line 55
    return-void
.end method

.method public static disableLogging()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-static {v0}, Lio/keen/client/java/KeenLogging;->setLogLevel(Ljava/util/logging/Level;)V

    .line 47
    return-void
.end method

.method public static enableLogging()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-static {v0}, Lio/keen/client/java/KeenLogging;->setLogLevel(Ljava/util/logging/Level;)V

    .line 40
    return-void
.end method

.method public static isLoggingEnabled()Z
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static log(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-static {}, Lio/keen/client/java/KeenLogging;->isLoggingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    sget-object v0, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v0, v1, p0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lio/keen/client/java/KeenLogging;->HANDLER:Ljava/util/logging/StreamHandler;

    invoke-virtual {v0}, Ljava/util/logging/StreamHandler;->flush()V

    .line 33
    :cond_0
    return-void
.end method

.method private static setLogLevel(Ljava/util/logging/Level;)V
    .locals 5
    .param p0, "newLevel"    # Ljava/util/logging/Level;

    .prologue
    .line 67
    sget-object v4, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    invoke-virtual {v4, p0}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 68
    sget-object v4, Lio/keen/client/java/KeenLogging;->LOGGER:Ljava/util/logging/Logger;

    invoke-virtual {v4}, Ljava/util/logging/Logger;->getHandlers()[Ljava/util/logging/Handler;

    move-result-object v0

    .local v0, "arr$":[Ljava/util/logging/Handler;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 70
    .local v1, "handler":Ljava/util/logging/Handler;
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/util/logging/Handler;->setLevel(Ljava/util/logging/Level;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 71
    :catch_0
    move-exception v4

    goto :goto_1

    .line 75
    .end local v1    # "handler":Ljava/util/logging/Handler;
    :cond_0
    return-void
.end method
