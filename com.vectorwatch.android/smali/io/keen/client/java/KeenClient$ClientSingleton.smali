.class final enum Lio/keen/client/java/KeenClient$ClientSingleton;
.super Ljava/lang/Enum;
.source "KeenClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/keen/client/java/KeenClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ClientSingleton"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/keen/client/java/KeenClient$ClientSingleton;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/keen/client/java/KeenClient$ClientSingleton;

.field public static final enum INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;


# instance fields
.field client:Lio/keen/client/java/KeenClient;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1120
    new-instance v0, Lio/keen/client/java/KeenClient$ClientSingleton;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lio/keen/client/java/KeenClient$ClientSingleton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    .line 1119
    const/4 v0, 0x1

    new-array v0, v0, [Lio/keen/client/java/KeenClient$ClientSingleton;

    sget-object v1, Lio/keen/client/java/KeenClient$ClientSingleton;->INSTANCE:Lio/keen/client/java/KeenClient$ClientSingleton;

    aput-object v1, v0, v2

    sput-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->$VALUES:[Lio/keen/client/java/KeenClient$ClientSingleton;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/keen/client/java/KeenClient$ClientSingleton;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1119
    const-class v0, Lio/keen/client/java/KeenClient$ClientSingleton;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/keen/client/java/KeenClient$ClientSingleton;

    return-object v0
.end method

.method public static values()[Lio/keen/client/java/KeenClient$ClientSingleton;
    .locals 1

    .prologue
    .line 1119
    sget-object v0, Lio/keen/client/java/KeenClient$ClientSingleton;->$VALUES:[Lio/keen/client/java/KeenClient$ClientSingleton;

    invoke-virtual {v0}, [Lio/keen/client/java/KeenClient$ClientSingleton;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/keen/client/java/KeenClient$ClientSingleton;

    return-object v0
.end method
