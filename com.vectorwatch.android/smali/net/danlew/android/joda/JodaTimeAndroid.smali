.class public final Lnet/danlew/android/joda/JodaTimeAndroid;
.super Ljava/lang/Object;
.source "JodaTimeAndroid.java"


# static fields
.field private static sInitCalled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lnet/danlew/android/joda/JodaTimeAndroid;->sInitCalled:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    sget-boolean v1, Lnet/danlew/android/joda/JodaTimeAndroid;->sInitCalled:Z

    if-eqz v1, :cond_0

    .line 47
    :goto_0
    return-void

    .line 36
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lnet/danlew/android/joda/JodaTimeAndroid;->sInitCalled:Z

    .line 39
    :try_start_0
    new-instance v1, Lnet/danlew/android/joda/ResourceZoneInfoProvider;

    invoke-direct {v1, p0}, Lnet/danlew/android/joda/ResourceZoneInfoProvider;-><init>(Landroid/content/Context;)V

    invoke-static {v1}, Lorg/joda/time/DateTimeZone;->setProvider(Lorg/joda/time/tz/Provider;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lnet/danlew/android/joda/TimeZoneChangedReceiver;

    invoke-direct {v2}, Lnet/danlew/android/joda/TimeZoneChangedReceiver;-><init>()V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.TIMEZONE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 46
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not read ZoneInfoMap"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
