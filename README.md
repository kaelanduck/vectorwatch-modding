# Vector app modding #

Efforts for modding the Vector Watch app to add features.

## Features ##

* Spotify support in the music watchapp
* The persistent notification is now a minimum priority notification
* There is a switch in Settings->Notifications that enables dismissing notifications from the phone when dismissed on the watch (default is yes)

## Compiling ##

Prerequisites

* Java
* Commonsense

To build an apk, run `./build.sh` or `build.bat` for the relevant system and an output apk is generated as com.vectorwatch.android-mod1.apk

(note: you may need to `chmod +x build.sh`)

## Installing ##

The Play Store version of the app needs to be uninstalled before installing this version (watchfaces and stream setup is not affected, but you have to login again)

You can use `adb install -r com.vectorwatch.android-mod1.apk` to install the app if you have the Android platform tools installed.

## Downloads ##

Downloads are [here](../../downloads)